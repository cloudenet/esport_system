import 'package:esport_system/helper/all_binding.dart';
import 'package:esport_system/locale/my_translations.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_value/shared_value.dart';
import 'package:flutter/services.dart';

import 'helper/constants.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Load SharedPreferences or any other asynchronous operations
  accessToken.load();
  loadSharedValue();


  // // Initialize the LanguageController
  // final languageController = Get.put(LanguageController());
  //
  // // Load the saved language before running the app
  // await languageController.loadLanguage();  // Await loading the language

  runApp(SharedValue.wrapApp(
      GetMaterialApp(
      theme: ThemeData.dark().copyWith(
        scaffoldBackgroundColor: bgColor,
        textTheme: GoogleFonts.poppinsTextTheme(ThemeData.dark().textTheme).
        apply(bodyColor: Colors.white), canvasColor: secondaryColor,),
        themeMode: ThemeMode.light,
        defaultTransition: Transition.fadeIn,
        opaqueRoute: Get.isOpaqueRouteDefault,
        popGesture: Get.isPopGestureEnable,
        navigatorKey: Get.key,
        translations: MyTranslations(),
        locale: const Locale('en', 'US'), // Set the locale from LanguageController
        fallbackLocale: const Locale('en', 'US'), // Fallback to English if locale not found
        initialRoute: RouteHelper.initial,
        initialBinding: AllBinding(),
        getPages: RouteHelper.routes,
        debugShowCheckedModeBanner: false,
        builder: EasyLoading.init(),
  )));
}

var cPrimary = const Color.fromARGB(255, 7, 118, 11);
var cPrimary2 = const Color.fromARGB(255, 2, 89, 5);
var cWhite = const Color.fromARGB(255, 238, 248, 252);

ThemeData get googleFormTheme => ThemeData(
  appBarTheme: AppBarTheme(
    centerTitle: false,
    scrolledUnderElevation: 10.0,
    titleTextStyle: const TextStyle(
        color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.normal),
    shadowColor: Colors.transparent,
    elevation: 0.0,
    color: cPrimary,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20)),
    ),
    systemOverlayStyle: const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.light,
      statusBarBrightness: Brightness.dark,
    ),
    iconTheme: const IconThemeData(color: Colors.white, size: 16.0),
    actionsIconTheme: const IconThemeData(color: Colors.white, size: 16.0),
  ),
  inputDecorationTheme: InputDecorationTheme(
    floatingLabelBehavior: FloatingLabelBehavior.auto,
    border: OutlineInputBorder(
      borderSide:
      BorderSide(color: Colors.grey.withOpacity(0.4), width: 1.0),
      borderRadius: const BorderRadius.all(Radius.circular(4.0)),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide:
      BorderSide(color: Colors.grey.withOpacity(0.4), width: 1.0),
      borderRadius: const BorderRadius.all(Radius.circular(4.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: cPrimary, width: 2.0),
      borderRadius: const BorderRadius.all(Radius.circular(4.0)),
    ),
    errorBorder: const OutlineInputBorder(
      borderSide: BorderSide(color: Colors.red, width: 2.0),
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
    focusedErrorBorder: const OutlineInputBorder(
      borderSide: BorderSide(color: Colors.red, width: 2.0),
      borderRadius: BorderRadius.all(Radius.circular(4.0)),
    ),
    disabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: cPrimary2, width: 1.0),
      borderRadius: const BorderRadius.all(Radius.circular(4.0)),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ElevatedButton.styleFrom(
        elevation: 8.0,
        shadowColor: cPrimary,
        backgroundColor: cPrimary,
        disabledBackgroundColor: Colors.green.withOpacity(0.4),
        disabledForegroundColor: Colors.grey,
        padding:
        const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
        textStyle:
        const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)),
        foregroundColor: Colors.white),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      side: const BorderSide(color: Colors.green, width: 1.5),
      disabledForegroundColor: Colors.grey,
      foregroundColor: Colors.green,
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 16.0),
      textStyle:
      const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
      shape:
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    ),
  ),
);
