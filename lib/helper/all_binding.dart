import 'package:esport_system/data/controllers/arbitrator_controller.dart';
import 'package:esport_system/data/controllers/coach_controller.dart';
import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/item_controller.dart';
import 'package:esport_system/data/controllers/language_controller.dart';
import 'package:esport_system/data/controllers/league_controller.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/controllers/medal_controller.dart';
import 'package:esport_system/data/controllers/monthly_test_controller.dart';
import 'package:esport_system/data/controllers/pitch_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/controllers/score_keeper_controller.dart';
import 'package:esport_system/data/controllers/subject_controller.dart';
import 'package:esport_system/data/controllers/umpire_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:get/get.dart';

import '../data/controllers/category_controller.dart';
import '../data/controllers/request_item_controller.dart';

class AllBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
    Get.lazyPut(() => TeamController());
    Get.lazyPut(() => UserController());
    Get.lazyPut(() => PlayerController());
    Get.lazyPut(() => CoachController());
    Get.lazyPut(() => PositionController());
    Get.lazyPut(() => EventController());
    Get.lazyPut(() => MatchesController());
    Get.lazyPut(() => LeagueController());
    Get.lazyPut(() => PitchController());
    Get.lazyPut(() => MedalsPlayerController());
    Get.lazyPut(() => RankController());
    Get.lazyPut(() => ArbitratorController());
    Get.lazyPut(() => LanguageController());
    Get.lazyPut(() => MonthlyTestController());
    Get.lazyPut(() => ItemController());
    Get.lazyPut(() => RequestItemController());
    Get.lazyPut(() => CategoryController());
    Get.lazyPut(() => MonthlyTestController());
    Get.lazyPut(() => RefereesController());
    Get.lazyPut(() => ScoreKeeperController());
    Get.lazyPut(() => SubjectController());
  }
}
