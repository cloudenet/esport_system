

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ConfirmationDialog extends StatelessWidget {
  final String message;
  final String cancelText;
  final String confirmText;
  final VoidCallback onCancel;
  final VoidCallback onConfirm;
  final Color backgroundColor;

  const ConfirmationDialog({
    Key? key,
    required this.message,
    required this.cancelText,
    required this.confirmText,
    required this.onCancel,
    required this.onConfirm,
    required this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: backgroundColor,
      content: Container(
        width:  300,
        height: Get.height * 0.2,
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Image.asset(
                "assets/images/warning.png",
                width: 50,
                height: 50,
              ),
            ),
            const SizedBox(height: 8),
            Text(message,
              textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyMedium,
            ),
            const Spacer(),
            Align(
              alignment: Alignment.bottomRight,
              // child: Row(
              //   crossAxisAlignment: CrossAxisAlignment.end,
              //   mainAxisAlignment: MainAxisAlignment.end,
              //   children: [
              //     TextButton(
              //       onPressed: onCancel,
              //       child: Text(cancelText),
              //     ),
              //     const SizedBox(width: 10),
              //     ElevatedButton(
              //       onPressed: onConfirm,
              //       child: Text(confirmText),
              //     ),
              //   ],
              // ),

              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: onCancel,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red, // Set the background color to red
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5), // Set the border radius to 5
                      ),
                    ),
                    child: Text( cancelText ,style: const TextStyle(color: Colors.white)),
                  ),
                  const SizedBox(width: 16.0),
                  ElevatedButton(
                    onPressed: onConfirm,
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue, // Set the background color to red
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5), // Set the border radius to 5
                      ),
                    ),
                    child: Text( confirmText ,style: const TextStyle(color: Colors.white)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}