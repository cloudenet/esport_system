import 'package:flutter/material.dart';

import 'constants.dart';

class HoverInSizebar extends StatefulWidget {
  final Widget child;

  const HoverInSizebar({super.key, required this.child});

  @override
  _HoverInSizebarState createState() => _HoverInSizebarState();
}

class _HoverInSizebarState extends State<HoverInSizebar> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) => setState(() {
        _isHovered = true;
      }),
      onExit: (event) => setState(() {
        _isHovered = false;
      }),
      child: AnimatedContainer(

        //margin: const EdgeInsets.symmetric(horizontal: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: _isHovered ? bgColor : Colors.transparent,
        ),
        duration: const Duration(milliseconds: 0),
        child: AnimatedScale(
          scale: _isHovered ? 1.05 : 1.0,
          duration: const Duration(milliseconds: 200),
          child: widget.child,
        ),
      ),
    );
  }
}
class HoverZoomWidget extends StatefulWidget {
  final Widget child;

  const HoverZoomWidget({super.key, required this.child});

  @override
  _HoverInSizebarState createState() => _HoverInSizebarState();
}

class _HoverZoomWidgetState extends State<HoverInSizebar> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) => setState(() {
        _isHovered = true;
      }),
      onExit: (_) => setState(() {
        _isHovered = false;
      }),
      child: AnimatedScale(
        scale: _isHovered ? 1.1 : 1.0,
        duration: const Duration(milliseconds: 200),
        child: widget.child,
      ),
    );
  }
}
