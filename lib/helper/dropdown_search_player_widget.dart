import 'package:dropdown_search/dropdown_search.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../data/controllers/matches_controller.dart';

final playerController = Get.find<PlayerController>();

class PlayerDropdownSearch extends StatefulWidget {
  final PlayerModel? selectedItem;
  final Color? colorBg;

  final ValueChanged<PlayerModel?> onChanged;

  const PlayerDropdownSearch({
    Key? key,
    required this.selectedItem,
    required this.onChanged,
    this.colorBg
  }) : super(key: key);

  @override
  PlayerDropdownSearchState createState() => PlayerDropdownSearchState();
}

class PlayerDropdownSearchState extends State<PlayerDropdownSearch> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PlayerController>(
        builder: (logic) {
          return Theme(
            data: Theme.of(context).copyWith(
              hoverColor: Colors.transparent,
            ),
            child: DropdownSearch<PlayerModel>(
              items: logic.dataPlayerForDropdownSearch,
              selectedItem: widget.selectedItem,
              dropdownDecoratorProps: DropDownDecoratorProps(
                dropdownSearchDecoration: InputDecoration(
                  hintText: "select_player".tr,
                  filled: true,
                  fillColor: widget.colorBg ?? bgColor,
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),
                  ),

                ),
              ),
              onChanged: widget.onChanged,
              popupProps: PopupProps.menu(
                showSearchBox: true,
                searchFieldProps: TextFieldProps(
                  decoration: InputDecoration(
                    labelText: 'search'.tr,
                    filled: true,
                    fillColor: secondaryColor,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  style: const TextStyle(color: Colors.white),
                ),
                menuProps: MenuProps(
                  backgroundColor: bgColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                fit: FlexFit.loose,
                constraints: const BoxConstraints(maxHeight: 400),
                itemBuilder: (context, user, isSelected) =>
                    ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.blue,
                        child: Text(
                          (user.name?.substring(0, 1) ?? 'N').toUpperCase(),
                          // Display the first letter of the name
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                      title: Text(
                        user.name?.toUpperCase() ?? 'N/A',
                      ),
                      subtitle: Text(
                        user.typePlayer ?? 'N/A', // Display the name normally
                        style: TextStyle(color: Colors.grey[600]),
                      ),
                    ),
              ),
              itemAsString: (PlayerModel player) => player.name ?? 'N/A',
              dropdownBuilder: (context, selectedItem) {
                if (selectedItem == null) {
                  return Text(
                    "search_player_name".tr,
                    style: TextStyle(color: Colors.grey[300]),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.zero,
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 12, // Size of the avatar
                          child: Text(
                            (selectedItem.name?.substring(0, 1) ?? 'N').toUpperCase(),
                            style: const TextStyle(color: Colors.white, fontSize: 12), // Font size
                          ),
                        ),
                        const SizedBox(width: 8), // Adjust spacing
                        Expanded(
                          child: Text(
                            selectedItem.name?.toUpperCase() ?? 'N/A',
                            style: const TextStyle(fontSize: 14), // Font size
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  );
                }
              },
            ),
          );
        });
  }
}


class PlayerDropDownInMatch extends StatefulWidget {
  final PlayerModel? selectedItem;
  final Color? colorBg;
  final ValueChanged<PlayerModel?> onChanged;
  const PlayerDropDownInMatch({
    super.key,
    required this.selectedItem,
    required this.onChanged,
    this.colorBg,});

  @override
  State<PlayerDropDownInMatch> createState() => _PlayerDropDownInMatchState();
}

class _PlayerDropDownInMatchState extends State<PlayerDropDownInMatch> {
  var m = Get.find<MatchesController>();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PlayerController>(
      builder: (logic) {

        List<PlayerModel> availablePlayers = logic.dataPlayerForDropdownSearch
            .where((player) => !m.selectedPlayers.contains(player) || player == widget.selectedItem)
            .toList();
        return Theme(
          data: Theme.of(context).copyWith(hoverColor: Colors.transparent),
          child: DropdownSearch<PlayerModel>(
            items: availablePlayers,
            selectedItem: widget.selectedItem,
            dropdownDecoratorProps: DropDownDecoratorProps(
              dropdownSearchDecoration: InputDecoration(
                hintText: "select_player".tr,
                filled: true,
                fillColor: secondaryColor,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(10),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(10),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            onChanged: (PlayerModel? value) {
              setState(() {
                widget.onChanged(value);
                if (value != null) {
                  m.selectPlayer(value);
                }
              });
            },
            popupProps: PopupProps.menu(
              showSearchBox: true,
              searchFieldProps: TextFieldProps(
                decoration: InputDecoration(
                  labelText: 'search'.tr,
                  filled: true,
                  fillColor: secondaryColor,
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                style: const TextStyle(color: Colors.white),
              ),
              menuProps: MenuProps(
                backgroundColor: bgColor,
                borderRadius: BorderRadius.circular(10),
              ),
              fit: FlexFit.loose,
              constraints: const BoxConstraints(maxHeight: 400),
              itemBuilder: (context, user, isSelected) {
                return ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.blue,
                    child: Text(
                      (user.name?.substring(0, 1) ?? 'N').toUpperCase(),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  title: Text(user.name?.toUpperCase() ?? 'N/A'),
                  subtitle: Text(
                    user.typePlayer ?? 'N/A',
                    style: TextStyle(color: Colors.grey[600]),
                  ),
                );
              },
            ),
            itemAsString: (PlayerModel player) => player.name ?? 'N/A',
            dropdownBuilder: (context, selectedItem) {
              if (selectedItem == null) {
                return Text(
                  "search_player_name".tr,
                  style: TextStyle(color: Colors.grey[300]),
                );
              } else {
                return Padding(
                  padding: EdgeInsets.zero,
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.blue,
                        radius: 12,
                        child: Text(
                          (selectedItem.name?.substring(0, 1) ?? 'N').toUpperCase(),
                          style: const TextStyle(color: Colors.white, fontSize: 12),
                        ),
                      ),
                      const SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          selectedItem.name?.toUpperCase() ?? 'N/A',
                          style: const TextStyle(fontSize: 14),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                );
              }
            },
          ),
        );
      },
    );
  }
}

