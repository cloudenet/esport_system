

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class CustomFormBuilderTextField extends StatelessWidget {
  final String name;
  final TextEditingController controller;
  final String hintText;
  final String? errorText;
  final IconData? icon;
  final Color fillColor;
  final VoidCallback? onTap;
  final bool? readOnly;

  const CustomFormBuilderTextField({
    Key? key,
    required this.name,
    required this.controller,
    required this.hintText,
    this.errorText,
    this.icon,
    required this.fillColor,
    this.onTap,
    this.readOnly,

  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      onTap: onTap,
      name: name,
      controller: controller,
      readOnly: readOnly ?? false,
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        hintText: hintText,
        fillColor: fillColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(errorText: errorText),
      ]),
    );
  }
}

class CustomFormNote extends StatelessWidget {
  final String name;
  final TextEditingController controller;
  final String hintText;
  final String? errorText;
  final IconData? icon;
  final int? minLine;
  final Color fillColor;
  final VoidCallback? onTap;
  final bool? readOnly;

  const CustomFormNote({
    Key? key,
    required this.name,
    required this.controller,
    required this.hintText,
    this.errorText,
    this.icon,
    this.minLine,
    required this.fillColor,
    this.onTap,
    this.readOnly,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      onTap: onTap,
      name: name,
      controller: controller,
      readOnly: readOnly ?? false,
      maxLines: null,
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        hintText: hintText,
        fillColor: fillColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
      validator: FormBuilderValidators.compose([
        if (errorText != null) FormBuilderValidators.required(errorText: errorText),
      ]),
    );
  }
}