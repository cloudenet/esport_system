import 'package:flutter/material.dart';

const primaryColor = Color(0xFF2697FF);
const secondaryColor = Color(0xFF2A2D3E);
const bgColor = Color(0xFF212332);
const activeColor = Color(0xff38b000);
const pendingColor = Colors.amberAccent;
final completeColor = Colors.green.withOpacity(0.1);
final cancelColor = Colors.grey.withOpacity(0.1);
const defaultPadding = 16.0;
const mobileSize = 800;
