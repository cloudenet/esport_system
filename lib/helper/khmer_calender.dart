
import 'package:intl/intl.dart';

Map<String, String> khmerMonths = {
  'January': 'មករា',
  'February': 'កុម្ភៈ',
  'March': 'មិនា',
  'April': 'មេសា',
  'May': 'ឧសភា',
  'June': 'មិថុនា',
  'July': 'កក្កដា',
  'August': 'សីហា',
  'September': 'កញ្ញា',
  'October': 'តុលា',
  'November': 'វិច្ឆិកា',
  'December': 'ធ្នូ',
};

Map<String, String> khmerDays = {
  'Monday': 'ច័ន្ទ',
  'Tuesday': 'អង្គារ',
  'Wednesday': 'ពុធ',
  'Thursday': 'ព្រហស្បតិ៍',
  'Friday': 'សុក្រ',
  'Saturday': 'សៅរ៍',
  'Sunday': 'អាទិត្យ',
};

Map<String, String> khmerDigits = {
  '0': '០',
  '1': '១',
  '2': '២',
  '3': '៣',
  '4': '៤',
  '5': '៥',
  '6': '៦',
  '7': '៧',
  '8': '៨',
  '9': '៩',
};


String convertNumberToKhmer(String number) {
  String khmerNumber = '';
  for (int i = 0; i < number.length; i++) {
    khmerNumber += khmerDigits[number[i]] ?? number[i];
  }
  return khmerNumber;
}

String convertToKhmerDate(DateTime date) {
  String formattedDate = DateFormat('d / MMMM / yyyy').format(date);

  formattedDate = formattedDate.replaceAllMapped(
    RegExp(r'(January|February|March|April|May|June|July|August|September|October|November|December)'),
        (match) => khmerMonths[match[0]] ?? match[0]!,
  );
  formattedDate = formattedDate.replaceAllMapped(
    RegExp(r'(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)'),
        (match) => khmerDays[match[0]] ?? match[0]!,
  );

  String year = DateFormat('yyyy').format(date);
  String dayNum = DateFormat('d').format(date);
  String khmerYear = convertNumberToKhmer(year);
  String dayNumKhmer = convertNumberToKhmer(dayNum);
  formattedDate = formattedDate.replaceAll(year, khmerYear);
  formattedDate = formattedDate.replaceAll(dayNum, dayNumKhmer);
  return formattedDate;
}

String convertToKhmerDateAndDayKhmer(DateTime date) {
  // Get the formatted English date
  String formattedDate = DateFormat('EEEE, d MMMM yyyy').format(date);

  // Translate the formatted English date to Khmer
  formattedDate = formattedDate.replaceAllMapped(
    RegExp(r'(January|February|March|April|May|June|July|August|September|October|November|December)'),
        (match) => khmerMonths[match[0]] ?? match[0]!,
  );
  formattedDate = formattedDate.replaceAllMapped(
    RegExp(r'(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)'),
        (match) => khmerDays[match[0]] ?? match[0]!,
  );

  // Convert the year to Khmer digits
  String year = DateFormat('yyyy').format(date);
  String khmerYear = convertNumberToKhmer(year);

  // Replace the English year with the Khmer year
  formattedDate = formattedDate.replaceAll(year, khmerYear);

  return formattedDate;
}