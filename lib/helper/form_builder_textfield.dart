import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:responsive_grid/responsive_grid.dart';

ResponsiveGridCol buildFormBuilderTextField({
  required String name,
  required TextEditingController controller,
  required String labelText,
  bool readOnly = false,
  void Function()? onTap,
  int md = 6,
  int lg = 4,
}) {
  return ResponsiveGridCol(
    md: md,
    lg: lg,
    child: Padding(
      padding: const EdgeInsets.all(10),
      child: FormBuilderTextField(
        name: name,
        controller: controller,
        decoration: InputDecoration(
          labelText: labelText,
        ),
        validator: FormBuilderValidators.compose([
          FormBuilderValidators.required(),
        ]),
        readOnly: readOnly,
        onTap: onTap,
      ),
    ),
  );
}
