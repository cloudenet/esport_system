import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';

Widget buildText(
    {required String title,
      required String textData,
      required double height,
      double? padding,
      Alignment alignment = Alignment.topLeft,
      double? fontSizeText,  // Changed to double instead of String
      TextAlign textAlign = TextAlign.start}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Text(
        title,
        style: const TextStyle(fontSize: 16),
      ),
      Container(
        padding: EdgeInsets.all(padding ?? 0),
        width: double.infinity,
        alignment: alignment,
        height: height,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Text(
          textData,
          overflow: TextOverflow.ellipsis,
          textAlign: textAlign,  // Added textAlign
          maxLines: 5,
          style: TextStyle(
            fontSize: fontSizeText ?? 14,  // Set fontSize
          ),
        ),
      ),
    ],
  );
}
