import 'dart:convert';

import 'package:http/http.dart'as http;

import 'app_contrain.dart';

Future<String> globalGenerateToken() async {
  try {
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/auth/generate-access-token'));
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var res = await response.stream.bytesToString();
      var newToken = jsonDecode(res);
      print("############ After generate token : ${newToken['accessToken']}");
      return newToken["accessToken"];
    } else {
      print(response.reasonPhrase);
    }
  } catch (e) {
    return "";
  }
  return "";
}


  Future<String> localGenerateToken() async {
    try{
      var request = http.Request('POST', Uri.parse('http://localhost:1120/auth/generate-access-token'));
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        return res["accessToken"];
      }
      else {
        print(response.reasonPhrase);
        return "";
      }
    }catch(e){
      print("Error : $e");
      return "";
    }
  }

