import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';



class SR {

  static String toDateNoHour(DateTime today) {
    return "${today.year.toString()}-${today.month.toString().padLeft(2, '0')}-${today.day.toString().padLeft(2, '0')}";
  }

  static String toDateNoHourFMonth(DateTime today) {
    return "${today.year.toString()}-${today.month.toString().padLeft(2, '0')}-01";
  }

  static String toDateNoHourPrevMonth(DateTime today) {
    return "${today.year.toString()}-${(today.month - 1).toString().padLeft(2, '0')}-${today.day.toString().padLeft(2, '0')}";
  }

  static String formatAmount(price) {
    String priceInText = "";
    int counter = 0;
    for (int i = (price.length - 1); i >= 0; i--) {
      counter++;
      String str = price[i];
      if ((counter % 3) != 0 && i != 0) {
        priceInText = "$str$priceInText";
      } else if (i == 0) {
        priceInText = "$str$priceInText";
      } else {
        priceInText = "$str$priceInText";
      }
    }
    return priceInText.trim();
  }

  static double toDouble(dynamic n) {
    if (n == null) return 0.0;
    if (n.toString() == 'null') return 0.0;

    //var r = (n.toString()).replaceAll(RegExp("\\D"), "");
    var r = (n.toString()).replaceAll(RegExp("[^\\d.-]"), "").toString();
    r = r.replaceAll(",", "");
    try {
      return double.parse(r);
    } catch (e) {
      return 0.0;
    }
  }

  static int toInt(dynamic n) {
    if (n == null) return 0;
    if (n.toString() == 'null') return 0;

    try {
      var r = toDouble(n);
      return r.toInt();
    } catch (e) {
      return 0;
    }
  }

  static bool toBool(dynamic n) {
    if (n == null) return false;
    if (n.toString() == 'null') return false;
    try {
      return n;
    } catch (e) {
      return false;
    }
  }

  static String? getYoutubeVideoIdByURL(String url) {
    final regex = RegExp(r'.*\?v=(.+?)($|[\&])', caseSensitive: false);

    try {
      if (regex.hasMatch(url)) {
        return regex.firstMatch(url)!.group(1);
      } else {
        return url;
      }
    } catch (_) {}
    return null;
  }

  static showAlertDialog(
      {required BuildContext context,
      required Widget widget,
      required String tittle}) {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          title: Center(child: Text(tittle)),
          content: widget,
          actions: <Widget>[
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }



  static Widget noDataWidget() {
    return Container(
        alignment: Alignment.center, child: const Text("NO DATA FOUND"));
  }

  static alertParam(BuildContext context, String title) async {
    showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(title,
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.normal)),
              //content: new Text("This is my content"),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: const Text("okay"),
                  onPressed: () => Navigator.pop(context),
                )
              ],
            ));
  }



  static Future<void> showMyDialog(
      {required BuildContext context, String? title, Widget? widget}) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            contentPadding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            content: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 3,
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                // color: Colors.white,
              ),
              child: Column(
                children: [
                  Container(
                      height: 50,
                      width: double.infinity,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            topLeft: Radius.circular(10)),
                        color: Colors.blue,
                      ),
                      child: Center(
                          child: Text(
                        title ?? "",
                        style: const TextStyle(color: Colors.white),
                      ))),
                  if (widget != null) ...[
                    Expanded(child: SingleChildScrollView(child: widget))
                  ],
                ],
              ),
            ));
      },
    );
  }

  static bool isNull(String? str) {
    try {
      if (str == null) return true;
      str = str.toString();
      if (str == 'null' || str == '') {
        return true;
      }
      return false;
    } catch (_) {
      return true;
    }
  }

  static String timeAgoCustom(String str) {
    // final date = DateTime.now();
    var createAt = DateTime.now();
    try {
      createAt = DateTime.parse(str);
    } catch (_) {
      createAt = DateTime.now();
    }
    // print("=======createAt $createAt");
    // final diff = date.difference(createAt);
    // <-- Custom method Time Show  (Display Example  ==> 'Today 7:00 PM')     // WhatsApp Time Show Status Shimila
    Duration diff = DateTime.now().difference(createAt);
    if (diff.inDays > 365) {
      return "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? "year" : "years"} ago";
    }
    if (diff.inDays > 30) {
      return "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "month" : "months"} ago";
    }
    if (diff.inDays > 7) {
      return "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? "week" : "weeks"} ago";
    }
    if (diff.inDays > 0) {
      return DateFormat.E().add_jm().format(createAt);
    }
    if (diff.inHours > 0) {
      return "Today ${DateFormat('jm').format(createAt)}";
    }
    if (diff.inMinutes > 0) {
      return "${diff.inMinutes} ${diff.inMinutes == 1 ? "minute" : "minutes"} ago";
    }
    if (diff.inSeconds > 0) {
      return "${diff.inSeconds} ${diff.inMinutes == 1 ? "second" : "seconds"} ago";
    }
    return "just now";
  }

  static String forMatteDate(String d) {
    if (d.isNotEmpty) {
      try {
        var dateTime = DateTime.parse(d);
        return DateFormat().add_yMMMd().format(dateTime);
      } catch (_) {
        return "";
      }
    }
    return "";
  }

  static bool isNotNull(String? str) {
    try {
      if (str == null) return false;
      str = str.toString();
      if (str == 'null' || str == '') {
        return false;
      }
      return true;
    } catch (_) {
      return false;
    }
  }

  static String getPrettyCount(int value) {
    String postfix = '';
    double finalValue = value.toDouble();

    if (value < 0) {
      throw 'Invalid value';
    } else if (value < 1000) {
      return value.toString();
    } else if (value < 1000000) {
      postfix = 'K';
      finalValue = value / 1000;
    } else if (value < 1000000000) {
      postfix = 'M';
      finalValue = value / 1000000;
    } else if (value < 1000000000000) {
      postfix = 'B';
      finalValue = value / 1000000000;
    }

    return finalValue.toStringAsFixed(1) + postfix;
  }

  static String firstUpperCase(String str) {
    if (str.isNotEmpty) {
      return "${str[0].toUpperCase()}${str.substring(1).toLowerCase()}";
    }
    return '';
  }

}
