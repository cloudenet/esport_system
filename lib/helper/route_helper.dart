import 'dart:convert';
import 'package:esport_system/helper/all_binding.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/auth_section/screen/login_page.dart';
import 'package:esport_system/views/auth_section/screen/register_page.dart';
import 'package:esport_system/views/setting_section/screen/general_system_setting.dart';
import 'package:esport_system/views/splash_section/screen/splash.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'package:shared_value/shared_value.dart';
import 'package:http/http.dart' as http;

import '../views/home_section/screen/new_home_dash.dart';

class RouteHelper {
  static const String initial = '/';
  static const String splash = '/splash';
  static const String loginPage = '/login';
  static const String registerPage = '/register';
  static const String changeProfile = '/change-profile';

  static List<GetPage> routes = [
    GetPage(
        name: initial,
        page: () => getRoute( HomePage(),),
        binding: AllBinding()),
    GetPage(
        name: splash,
        page: () => getRoute(const AnimatedSplashScreen(), isTrue: true),
        binding: AllBinding()),
    GetPage(
        name: loginPage,
        page: () => getRoute(const LoginPage(), isTrue: true),
        binding: AllBinding()),
    GetPage(
        name: registerPage,
        page: () => getRoute(const RegisterPage(), isTrue: true),
        binding: AllBinding()),
  ];

  static getRoute(Widget navigateTo, {bool isTrue = false}) {
    try {
      accessToken.load();
    } catch (_) {}
    if (isTrue) {
      return navigateTo;
    }
    if (accessToken.$ != "") {
      return navigateTo;
    } else {
      return LoginPage();
    }
  }
}

UserModel? userInfo;
final SharedValue<String> accessToken = SharedValue(
  value: "",
  key: "accessToken",
  autosave: true,
);
final SharedValue<String> user = SharedValue(
  value: "",
  key: "userInfo",
  autosave: true,
);
void toggleTheme() {
  isDarkMode.$ = !isDarkMode.$;
  isDarkMode.save();
  Get.changeThemeMode(isDarkMode.$ ? ThemeMode.dark : ThemeMode.light);
}



final SharedValue<bool> isDarkMode = SharedValue(
  value: false,
  key: "isDarkMode",
  autosave: true,
);


Future<void> loadSharedValue() async {
  await accessToken.load();
  await user.load();

  if (user.$ != "") {
    try {
      userInfo = UserModel.fromJson(jsonDecode(user.$));
      if (kDebugMode) {
        print(userInfo!.toJson());
      }
    } catch (_) {}
  }
}

Future<UserModel?> getMe() async {
  if (userInfo != null) {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await getAccessToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('http://167.172.81.224:8287/api/get-detail-user'));
    request.body = json.encode({"user_id": userInfo!.id.toString()});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var source = (await response.stream.bytesToString());
      var data = jsonDecode(source);
      if (data['data'] != null) {
        if (data['data']['user'] != null) {
          userInfo = UserModel.fromJson(data['data']['user']);
          var bcrypt = data['data']['bcrypt'];
          accessToken.$ = bcrypt;
          user.$ = jsonEncode(data['data']['user']);
          accessToken.save();
          user.save();
          print('=======================${userInfo!.name}');

          return userInfo;
        }
      }
    } else {
      if (kDebugMode) {
        print('============user getMe${response.reasonPhrase}');
      }
    }
  }
  return null;
}

final f = DateFormat('yyyy-MM-dd HH:mm');
final f2 = DateFormat('dd-MM-yyyy HH:mm');

class UserModel {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;
  String? emailVerifiedAt;
  String? photo;
  String? gender;
  String? dob;
  String? address;
  String? userStatus;
  String? role;
  String? devicesToken;
  String? lastLoginAt;
  String? rememberToken;
  String? createdBy;
  String? updatedBy;
  String? identityNo;
  String? identityPhoto;
  String? nationality;
  String? createdAt;
  String? updatedAt;

  UserModel(
      {this.id,
      this.cardNumber,
      this.name,
      this.username,
      this.phone,
      this.email,
      this.emailVerifiedAt,
      this.photo,
      this.gender,
      this.dob,
      this.address,
      this.userStatus,
      this.role,
      this.devicesToken,
      this.lastLoginAt,
      this.rememberToken,
      this.createdBy,
      this.updatedBy,
      this.identityNo,
      this.identityPhoto,
      this.nationality,
      this.createdAt,
      this.updatedAt});

  UserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
    emailVerifiedAt = json['email_verified_at'].toString();
    photo = json['photo'].toString();
    gender = json['gender'].toString();
    dob = json['dob'].toString();
    address = json['address'].toString();
    userStatus = json['user_status'].toString();
    role = json['role'].toString();
    devicesToken = json['devices_token'].toString();
    lastLoginAt = json['last_login_at'].toString();
    rememberToken = json['remember_token'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    identityNo = json['identity_no'].toString();
    identityPhoto = json['identity_photo'].toString();
    nationality = json['nationality'].toString();

    createdAt = "";
    try {
      if (json['created_at'] != null) {
        var d = DateTime.parse(json['created_at'].toString());
        createdAt = f2.format(d);
      }
    } catch (_) {
      createdAt = "";
    }

    updatedAt = "";
    try {
      if (json['updated_at'] != null) {
        var d = DateTime.parse(json['updated_at'].toString());
        updatedAt = f2.format(d);
      }
    } catch (_) {
      updatedAt = "";
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    data['email_verified_at'] = emailVerifiedAt;
    data['photo'] = photo;
    data['gender'] = gender;
    data['dob'] = dob;
    data['address'] = address;
    data['user_status'] = userStatus;
    data['role'] = role;
    data['devices_token'] = devicesToken;
    data['last_login_at'] = lastLoginAt;
    data['remember_token'] = rememberToken;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['identity_no'] = identityNo;
    data['identity_photo'] = identityPhoto;
    data['nationality'] = nationality;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

Future<String> getAccessToken() async {
  if (userInfo == null) return "";

  var headers = {
    'Content-Type': 'application/json',
  };
  var request = http.Request('POST',
      Uri.parse('http://167.172.81.224:8287/auth/generate-access-token'));
  request.body = json.encode(
      {"user_id": userInfo!.id.toString(), "accessToken": accessToken.$});
  request.headers.addAll(headers);

  http.StreamedResponse response = await request.send();

  if (response.statusCode == 200) {
    var txt = (await response.stream.bytesToString());
    var data = jsonDecode(txt);
    if (data["accessToken"] != null) {
      return data["accessToken"].toString();
    }
  } else {
    if (kDebugMode) {
      print(response.reasonPhrase);
    }
  }

  return "";
}
