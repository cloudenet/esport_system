import 'package:dropdown_search/dropdown_search.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

final controller = Get.find<UserController>();

class UserDropdownSearch extends StatefulWidget {
  final ListUserModel? selectedItem;
  final bool? checkUpdate ;
  final List<ListUserModel>? allUserList;
  final ValueChanged<ListUserModel?> onChanged;

  const UserDropdownSearch({
    Key? key,
    required this.selectedItem,
    required this.onChanged,
    this.checkUpdate,
    this.allUserList
  }) : super(key: key);

  @override
  UserDropdownSearchState createState() => UserDropdownSearchState();
}

class UserDropdownSearchState extends State<UserDropdownSearch> {
  @override
  Widget build(BuildContext context) {
    return DropdownSearch<ListUserModel>(
      items: widget.checkUpdate! ?  widget.allUserList! : controller.listUserDropdown,
      selectedItem: widget.selectedItem,
      enabled: !widget.checkUpdate!,
      validator: FormBuilderValidators.required(),
      dropdownDecoratorProps: DropDownDecoratorProps(
        dropdownSearchDecoration: InputDecoration(
          hoverColor: secondaryColor,
          hintText: "Select User",
          filled: true,
          fillColor: secondaryColor, // Background color for dropdown field
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
      onChanged: widget.onChanged,
      popupProps: PopupProps.menu(
        showSearchBox: true,
        searchFieldProps: TextFieldProps(
          decoration: InputDecoration(
            labelText: 'Search',
            filled: true,
            fillColor: secondaryColor,
            border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          style: const TextStyle(color: Colors.white),
        ),
        menuProps: const MenuProps(
          backgroundColor: bgColor,
        ),
        fit: FlexFit.loose,
        constraints: const BoxConstraints(maxHeight: 400),
        itemBuilder: (context, user, isSelected) => ListTile(
          leading: CircleAvatar(
            backgroundColor: Colors.blue,
            child: Text(
              (user.name?.substring(0, 1) ?? 'N').toUpperCase(), // Display the first letter of the name
              style: const TextStyle(color: Colors.white),
            ),
          ),
          title: Text(
            user.name?.toUpperCase() ?? 'N/A', // Display the name in uppercase
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          subtitle: Text(
            user.role ?? 'N/A', // Display the name normally
            style: TextStyle(color: Colors.grey[600]),
          ),
        ),
      ),
      itemAsString: (ListUserModel user) => user.name ?? 'N/A',
      dropdownBuilder: (context, selectedItem) {
        if (selectedItem == null) {
          return Text(
            "Select User",
            style: TextStyle(color: Colors.grey[300]),
          );
        } else {
          return Padding(
            padding: EdgeInsets.zero,
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: Colors.blue,
                  radius: 12, // Size of the avatar
                  child: Text(
                    (selectedItem.name?.substring(0, 1) ?? 'N').toUpperCase(),
                    style: const TextStyle(color: Colors.white, fontSize: 12), // Font size
                  ),
                ),
                const SizedBox(width: 8), // Adjust spacing
                Expanded(
                  child: Text(
                    selectedItem.name?.toUpperCase() ?? 'N/A',
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14), // Font size
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
