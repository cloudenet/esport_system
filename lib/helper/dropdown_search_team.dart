import 'package:dropdown_search/dropdown_search.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/team_model.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

final teamController = Get.find<TeamController>();

class TeamDropdownSearch extends StatefulWidget {
  final TeamModel? selectedItem;
  final ValueChanged<TeamModel?> onChanged;

  const TeamDropdownSearch({
    Key? key,
    required this.selectedItem,
    required this.onChanged,
  }) : super(key: key);

  @override
  TeamDropdownSearchState createState() => TeamDropdownSearchState();
}

class TeamDropdownSearchState extends State<TeamDropdownSearch> {
  @override
  Widget build(BuildContext context) {
    return FormField<TeamModel>(
      validator: (value) {
        if (value == null) {
          return "Please select a team";
        }
        return null;
      },
      builder: (FormFieldState<TeamModel> field) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            DropdownSearch<TeamModel>(
              items: teamController.listTeamDropdown,
              selectedItem: widget.selectedItem,
              dropdownDecoratorProps: DropDownDecoratorProps(
                dropdownSearchDecoration: InputDecoration(
                  hoverColor: bgColor,
                  hintText: "Select User",
                  filled: true,
                  fillColor: secondaryColor,
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  errorText: field.errorText, // Display validation error
                ),
              ),
              onChanged: (team) {
                field.didChange(team); // Update field state
                widget.onChanged(team);
              },
              popupProps: PopupProps.menu(
                showSearchBox: true,
                searchFieldProps: TextFieldProps(
                  decoration: InputDecoration(
                    labelText: 'Search',
                    filled: true,
                    fillColor: secondaryColor,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  style: const TextStyle(color: Colors.white),
                ),
                menuProps: const MenuProps(
                  backgroundColor: bgColor,
                ),
                fit: FlexFit.loose,
                constraints: const BoxConstraints(maxHeight: 400),
                itemBuilder: (context, team, isSelected) => ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.blue,
                    child: Text(
                      (team.teamName?.substring(0, 1) ?? 'N').toUpperCase(),
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                  title: Text(
                    team.teamName?.toUpperCase() ?? 'N/A',
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    team.teamName ?? 'N/A',
                    style: TextStyle(color: Colors.grey[600]),
                  ),
                ),
              ),
              itemAsString: (TeamModel team) => team.teamName ?? 'N/A',
              dropdownBuilder: (context, selectedItem) {
                if (selectedItem == null) {
                  return Text(
                    "Search user name",
                    style: TextStyle(color: Colors.grey[300]),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.zero,
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 12,
                          child: Text(
                            (selectedItem.teamName?.substring(0, 1) ?? 'N').toUpperCase(),
                            style: const TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Text(
                            selectedItem.teamName?.toUpperCase() ?? 'N/A',
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  );
                }
              },
            ),
          ],
        );
      },
    );
  }
}
