import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter/foundation.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

import '../data/model/address_model.dart';
import 'constants.dart'; // For kIsWeb
// Ensure FormBuilderValidators is imported

class LocationSelector extends StatelessWidget {
  final String label;
  final List<Address> data;
  final String currentValue;
  final Function(Address) onSelect;
  final bool enabled;
  final IconData? icon;

  const LocationSelector({
    Key? key,
    required this.label,
    required this.data,
    required this.currentValue,
    required this.onSelect,
    this.enabled = true,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () => _showSelectionDialog(
          context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: bgColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
    kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding:
          const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
