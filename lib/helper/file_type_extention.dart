


import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;

IconData getFileIcon(String fileName) {
  final Map<String, IconData> fileIcons = {
    'pdf': Icons.picture_as_pdf,
    'xlsx': Icons.description_outlined,
    'doc': Icons.description,
    'docx': Icons.description,
    'jpg': Icons.image,
    'jpeg': Icons.image,
    'png': Icons.image,
    'mp4': Icons.videocam,
    'mp3': Icons.audiotrack,
  };
  String fileExtension =
  p.extension(fileName).toLowerCase().replaceAll('.', ''); // Get extension
  return fileIcons[fileExtension] ?? Icons.insert_drive_file;
}

Color getFileIconColor(String fileName) {
  final Map<String, Color> fileColors = {
    'pdf': Colors.red,
    'xlsx': Colors.green,
    'doc': Colors.blue,
    'docx': Colors.blue,
    'jpg': Colors.green,
    'jpeg': Colors.green,
    'png': Colors.green,
    'mp4': Colors.orange,
    'mp3': Colors.purple,
  };
  String fileExtension =
  p.extension(fileName).toLowerCase().replaceAll('.', ''); // Get extension
  return fileColors[fileExtension] ?? Colors.grey;
}