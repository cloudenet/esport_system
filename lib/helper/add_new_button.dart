import 'package:esport_system/helper/responsive_helper.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

class AddNewButton extends StatelessWidget {
  final String btnName;
   final Function() onPress;
  const AddNewButton({super.key,required this.btnName,required this.onPress});

  @override
  Widget build(BuildContext context) {
    return  ElevatedButton.icon(
      style: TextButton.styleFrom(
        shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.circular(8)
        ),
        backgroundColor: Colors.blue,
        padding: EdgeInsets.symmetric(
          horizontal: defaultPadding * 1.5,
          vertical:
          defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
        ),
      ),
      onPressed: onPress,
      icon: const Icon(Icons.add,color: Colors.white,),
      label: Text(btnName,style: const TextStyle(color: Colors.white,fontSize: 16),),
    );
  }
}

class CancelButton extends StatelessWidget {
  final String btnName;
   final Function() onPress;
  final Color? colorRStatus;

  const CancelButton({super.key,required this.btnName,required this.onPress,this.colorRStatus});

  @override
  Widget build(BuildContext context) {
    return  ElevatedButton(
      style: TextButton.styleFrom(
        shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.circular(8)
        ),
        backgroundColor: colorRStatus ?? Colors.red,
        padding: EdgeInsets.symmetric(
          horizontal: defaultPadding * 1.5,
          vertical:
          defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
        ),
      ),
      onPressed: onPress,

      child: Text(btnName,style: const TextStyle(color: Colors.white,fontSize: 16),),
    );
  }
}

class OKButton extends StatelessWidget {
  final String btnName;
  final Function() onPress;
  final Color? colorRStatus;

  const OKButton({
    super.key,
    required this.btnName,
    required this.onPress,
    this.colorRStatus,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: TextButton.styleFrom(
        shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        backgroundColor: colorRStatus ?? Colors.blue,
        padding: EdgeInsets.symmetric(
          horizontal: defaultPadding * 1.5,
          vertical: defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
        ),
      ),
      onPressed: onPress,
      child: Text(
        btnName,
        style: const TextStyle(color: Colors.white, fontSize: 16),
      ),
    );
  }
}

class DownloadButton extends StatelessWidget {
  final String btnName;
  final Color colors;
  final Color textColor;
  final IconData icons;
  final Color iconColor;
  final Function() onPress;

  const DownloadButton({
    Key? key,
    required this.btnName,
    required this.onPress,
    required this.colors,
    required this.textColor,
    required this.icons,
    required this.iconColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      style: ElevatedButton.styleFrom(
        shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        backgroundColor: colors,
        padding: EdgeInsets.symmetric(
          horizontal: defaultPadding * 1.5,
          vertical: defaultPadding / (Responsive.isMobile(context) ? 2 : 1),
        ),
      ),
      onPressed: onPress,
      icon: Icon(
        icons,
        color: iconColor,
      ),
      label: Text(
        btnName,
        style: TextStyle(
          color: textColor,
          fontSize: 16,
        ),
      ),
    );
  }
}


