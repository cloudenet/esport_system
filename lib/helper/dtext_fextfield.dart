import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
 dtextField({
  TextEditingController? controller,
  Key? formKey,
  String? lableText,
  String? Function(String?)? validator,
}) {
  return FormBuilderTextField(
      name: lableText!,
      controller: controller,
      decoration: InputDecoration(labelText: lableText.tr),
      validator: validator);
}
