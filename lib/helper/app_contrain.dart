class AppConstants {
  static const String theme = 'app_theme';

  static const String languageCode = 'language_code';
  static const String countryCode = 'country_code';

  //static const String baseUrl = 'http://167.172.81.224:8287';
  static const String baseUrl = 'http://localhost:1120';  // user local server

  static const String createEvent = '/api/create-event';
  static const String getEvent = '/api/none/get-list-events';
  static const String updateEvent = '/api/update-events';
  static const String deleteEvent = '/api/delete-event';
  static const String getEventDetail = '/api/get-detail-event';
  static const String updateTeam = '/api/update-team';
  static const String createCoach = '/api/create-coach';
  static const String getCoach = '/api/get-coach';
  static const String deleteCoach = '/api/delete-coach';
  static const String deleteTeam = '/api/delete-team';
  static const String updateCoach = '/api/update-coach';
  static const String createPlayer = '/api/create-player';
  static const String getPlayer = '/api/get-player';
  static const String deletePlayer = '/api/delete-player';
  static const String updatePlayer = '/api/update-player';
  static const String createUser = '/api/create-user';
  static const String getUser = '/api/get-list-user';
  static const String getUserDetail = '/api/get-detail-user';
  static const String updateUser = '/api/update-info-user';
  static const String assignControlUser = '/api/assign-control';
  static const String deleteUser = '/api/delete-user';
  static const String createPosition = '/api/create-position';
  static const String getPosition = '/api/get-positions';
  static const String updatePosition = '/api/update-position';
  static const String deletePosition = '/api/delete-position';
  static const String createMatches = '/api/create-matches';
  static const String getMatches = '/api/get-list-matches';
  static const String updateMatches = '/api/update-match';
  static const String getListRequestBowl = '/api/get-list-request-bowl';
  static const String createRequestBowl = '/api/create-request-bowl';
  static const String updateRequestBowl = '/api/update-request-bowl';
  static const String deleteRequestBowl = '/api/delete-request-bowl';
  static const String requestPitch = '/api/request-pitch';
  static const String getRequestPitch = '/api/get-list-request-pitch';
  static const String reject = 'reject';
  static const String approve = 'approve';

  static const String pending = 'pending';
  static const String completed = 'completed';
  static const String received = 'received';
  static const String cancelled = 'cancelled';

  static const String vs = 'assets/icons/avatar.png';
  static const String event = 'assets/icons/calendar.png';
  static const String coach = 'assets/icons/coach.png';
  static const String player = 'assets/icons/group.png';
  static const String user = 'assets/icons/user.png';

//  =================== Status ======================//

  // static List<LanguageModel> languages = [
  //   LanguageModel(
  //       imageUrl: "assets/flag/khmer.jpg",
  //       languageName: 'ខ្មែរ',
  //       countryCode: 'KH',
  //       languageCode: 'km'),
  //   LanguageModel(
  //       imageUrl: "assets/flag/english.jpg",
  //       languageName: 'English',
  //       countryCode: 'US',
  //       languageCode: 'en'),
  //   LanguageModel(
  //       imageUrl: "assets/flag/china.png",
  //       languageName: '中文',
  //       countryCode: 'ZH',
  //       languageCode: 'zh'),
  // ];
}
