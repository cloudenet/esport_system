import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

Widget searchField({required Function(String) onChange, required String hintText}) {
  return SizedBox(
    height: 48, // Set the desired height
    child: TextField(
      onChanged: onChange,
      decoration: InputDecoration(
        hintText: hintText,
        fillColor: secondaryColor,
        hintStyle: const TextStyle(color: Colors.white),
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide.none,
        ),
        contentPadding:
            const EdgeInsets.only(left: 12), // Set the vertical padding
        suffixIcon: InkWell(
          onTap: () {},
          child: Container(
            padding: const EdgeInsets.all(defaultPadding * 0.95),
            margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: SvgPicture.asset("assets/icons/Search.svg"),
          ),
        ),
      ),
    ),
  );
}
