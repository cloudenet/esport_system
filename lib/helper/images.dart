



class Images {

  static const String logoBoules = 'assets/images/Boules.png';
  static const String fontKhmerMoul = 'assets/fonts/khmer_moul_regular.ttf';
  static const String locationIcon = 'assets/images/icons8_location_96.png';
  static const String telegramIcon = 'assets/images/telegram.png';
  static const String mailIcon = 'assets/images/mail.png';
  static const String facebookIcon = 'assets/images/facebook.png';
  static const String phoneIcon = 'assets/images/telephone.png';
  static const String noImage = 'assets/images/no-image.png';
  static const String matchIcon = 'assets/images/match.png';
  static const String userCardImage = 'assets/images/user_card_image.png';
  static const String goldMedalImage = 'assets/images/gold_medal.png';
  static const String silverMedalImage = 'assets/images/silever_medal.png';
  static const String brownCardImage = 'assets/images/brown_medal.png';
  static const String sign = 'assets/images/sign.png';

}