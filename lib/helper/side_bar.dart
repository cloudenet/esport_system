import 'package:esport_system/helper/hover_widget.dart';
import 'package:flutter/material.dart';

import 'constants.dart';
//
// class SideMenu extends StatelessWidget {
//    SideMenu({
//     Key? key,
//   }) : super(key: key,);
//
//   @override
//   Widget build(BuildContext context) {
//     return Drawer(
//       child: ListView(
//         children: [
//           DrawerHeader(
//             child: Image.asset("assets/images/logo.png"),
//           ),
//     DrawerListTile(
//     title: "Dashboard",
//     svgSrc: "assets/icons/dashboard.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(HomeView());
//     },
//    // isSelected: false,
//   ), DrawerListTile(
//     title: "Payments",
//             svgSrc: "assets/icons/group.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(ListTeamScreen());
//     },
//    // isSelected: false,
//   ), DrawerListTile(
//     title: "Register",
//     svgSrc: "assets/icons/group.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(ListTeamScreen());
//     },
//   //  isSelected: false,
//   ), DrawerListTile(
//     title: "Setting",
//     svgSrc: "assets/icons/settings.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(ListTeamScreen());
//     },
//    // isSelected: false,
//   ), DrawerListTile(
//     title: "Team",
//     svgSrc: "assets/icons/group.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(ListTeamScreen());
//     },
//     //isSelected: false,
//   ),
//
//   DrawerListTile(
//     title: "User",
//     svgSrc: "assets/icons/user.png",
//     press: () {
//
//       Get.find<HomeController>()
//           .updateWidgetHome(GetUserList());
//     },
//    // isSelected: false,
//   ),
//
//   DrawerListTile(
//      title: "Players",
//      svgSrc: "assets/icons/group.png",
//      press: () {
//
//        Get.find<HomeController>().updateWidgetHome(const ListPlayerScreen());
//      },
//   //  isSelected: false,
//    ),
//
//   DrawerListTile(
//     title: "Coach",
//     svgSrc: "assets/icons/coach.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(const GetCoachScreen());
//     },
//    // isSelected: false
//   ),
//
//   DrawerListTile(
//     title: "Positions",
//     svgSrc: "assets/icons/user.png",
//     press: () {
//
//       Get.find<HomeController>()
//           .updateWidgetHome(GetPositionScreen());
//     },
//   //  isSelected:false,
//   ),
//
//   DrawerListTile(
//     title: "Event",
//     svgSrc: "assets/icons/calendar.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(ListEvent());
//     },
//   //  isSelected: false,
//   ),
//
//   DrawerListTile(
//     title: "Matches",
//     svgSrc: "assets/icons/avatar.png",
//     press: () {
//
//       Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
//     },
//    // isSelected: false,
//   )
//         ],
//       ),
//     );
//   }
// }


// class DrawerListTile extends StatelessWidget {
//   const DrawerListTile({
//     Key? key,
//     // For selecting those three line once press "Command+D"
//     required this.title,
//     required this.svgSrc,
//     this.press,
//     required this.isSelected,
//
//   }) : super(key: key);
//
//   final String title, svgSrc;
//   final VoidCallback? press;
//   final bool isSelected;
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//      margin: const EdgeInsets.symmetric(horizontal: 8),
//       decoration: BoxDecoration(
//
//         borderRadius: BorderRadius.circular(8),
//         color: isSelected?bgColor:null),
//       child: HoverInSizebar(
//         child: GestureDetector(
//           onTap: ()=>press?.call(),
//           child: ListTile(
//
//             horizontalTitleGap: 0.0,
//             leading: Container(
//
//                 height: 45,
//                 width: 45,
//                 margin: const EdgeInsets.all(8),
//                 padding: const EdgeInsets.all(8),
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(8),
//                     color:isSelected? secondaryColor:null
//                   // image: DecorationImage(image: AssetImage(icon)),
//                 ),
//                 child: ColorFiltered(
//                     colorFilter: const ColorFilter.mode(
//                       Colors.white,
//                       BlendMode.srcIn,
//                     ),
//                     child: Image.asset(svgSrc))),
//             title: Text(
//               title,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

class DrawerListTile extends StatelessWidget {
  const DrawerListTile({
    Key? key,
    required this.title,
    required this.svgSrc,
    this.press,
    required this.isSelected,
    this.hasSubMenu = false,
    this.children,
    this.parentIndex,
    this.subIndex,
    this.selectSub = false
  }) : super(key: key);

  final String title, svgSrc;
  final VoidCallback? press;
  final bool isSelected;
  final bool hasSubMenu;
  final List<Widget>? children;
  final int? parentIndex ;
  final int? subIndex ;
  final bool selectSub;


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: isSelected ? bgColor : null,
      ),
      child: hasSubMenu
          ? ExpansionTile(
        leading: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: isSelected ? secondaryColor : null, // Use selectSubmenu
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ColorFiltered(
                colorFilter: const ColorFilter.mode(
                  Colors.white,
                  BlendMode.srcIn,
                ),
                child: Image.asset(svgSrc, width: 20, height: 20)),
          ),
        ),
        title: Text(
          title,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
            color: Colors.white,
            fontSize: 16,
          ),
        ),
        shape: const RoundedRectangleBorder(
          side: BorderSide.none,
        ),
        children: children ?? [],
      )
          : HoverInSizebar(
        child: GestureDetector(
          onTap: () => press?.call(),
          child: ListTile(

            leading: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: isSelected ? secondaryColor : null,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ColorFiltered(
                    colorFilter: const ColorFilter.mode(
                      Colors.white,
                      BlendMode.srcIn,
                    ),
                    child: Image.asset(svgSrc,width: 20,height: 20,)
                ),
              ),
            ),
            title: Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: isSelected ? Colors.white : null, // Ensure color is consistent
                fontSize: 16, // Ensure font size is consistent
              ),
            ),
          ),
        ),
      ),
    );
  }
}
