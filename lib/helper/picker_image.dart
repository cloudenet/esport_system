import 'package:image_picker/image_picker.dart';

class ImagePickerHelper {
  final ImagePicker _picker = ImagePicker();

  Future<XFile?> pickImageFromCamera() async {
    try {
      final XFile? imageFile = await _picker.pickImage(source: ImageSource.camera);
      return imageFile;
    } catch (e) {
      // Handle any errors here
      print("Error picking image: $e");
      return null;
    }
  }

  Future<XFile?> pickImageFromGallery() async {
    try {
      final XFile? imageFile = await _picker.pickImage(source: ImageSource.gallery);
      return imageFile;
    } catch (e) {
      // Handle any errors here
      print("Error picking image: $e");
      return null;
    }
  }
}
