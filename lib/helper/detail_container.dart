import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';

Widget buildContainer(
    {required String title,
      required String data,
      required IconData icon,
      required BuildContext context}) {
  return Container(
    width: double.infinity,
    padding: const EdgeInsets.all(10),
    height: 60,
    decoration:
    BoxDecoration(borderRadius: BorderRadius.circular(5), color: bgColor),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(
          icon,
          color: Colors.blue,
        ),
        const SizedBox(
          width: 10,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(title),
            Text(
              data,
              style: Theme.of(context).textTheme.bodyMedium?.copyWith(
                fontWeight: FontWeight.bold,
                fontSize: 11,
              ),
            )
          ],
        )
      ],
    ),
  );
}