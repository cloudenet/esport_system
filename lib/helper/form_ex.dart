import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/main.dart';
import 'package:responsive_grid/responsive_grid.dart';

class FormEx extends StatelessWidget {
  FormEx({super.key});

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: InkWell(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: GetBuilder<HomeController>(builder: (logic) {
          return Container(
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(begin: Alignment.topCenter, colors: [
              cPrimary,
              cPrimary2,
              cWhite,
            ])),
            padding: const EdgeInsets.all(5.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20))),
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.all(30),
                            child: FormBuilder(
                              key: _formKey,
                              child: ResponsiveGridRow(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // phone,name, username, password, role_id
                                  ResponsiveGridCol(
                                    md: 6,
                                    lg: 4,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8, right: 4, left: 4),
                                      child: FormBuilderTextField(
                                        name: 'name',
                                        controller: logic.nameController,
                                        decoration: InputDecoration(
                                            labelText: 'name'.tr),
                                        validator:
                                            FormBuilderValidators.compose([
                                          FormBuilderValidators.required(),
                                          FormBuilderValidators.minLength(3)
                                        ]),
                                      ),
                                    ),
                                  ),
                                  ResponsiveGridCol(
                                    md: 6,
                                    lg: 4,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8, right: 4, left: 4),
                                      child: FormBuilderTextField(
                                        name: 'phone',
                                        controller: logic.phoneController,
                                        decoration: InputDecoration(
                                            labelText: 'phone'.tr),
                                        validator:
                                            FormBuilderValidators.compose([
                                          FormBuilderValidators.required(),
                                          FormBuilderValidators.minLength(9),
                                          FormBuilderValidators.maxLength(15),
                                        ]),
                                      ),
                                    ),
                                  ),
                                  ResponsiveGridCol(
                                    md: 6,
                                    lg: 4,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8, right: 4, left: 4),
                                      child: FormBuilderTextField(
                                        name: 'password',
                                        controller: logic.passwordController,
                                        decoration: InputDecoration(
                                            labelText: 'Password'.tr),
                                        obscureText: true,
                                        validator:
                                            FormBuilderValidators.compose([
                                          FormBuilderValidators.required(),
                                        ]),
                                      ),
                                    ),
                                  ),
                                  ResponsiveGridCol(
                                    md: 6,
                                    lg: 4,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8, right: 4, left: 4),
                                      child: FormBuilderTextField(
                                        name: 'passwordConfirmation',
                                        controller: logic
                                            .passwordConfirmationController,
                                        decoration: InputDecoration(
                                            labelText: 'Confirmation'.tr),
                                        obscureText: true,
                                        validator:
                                            FormBuilderValidators.compose([
                                          FormBuilderValidators.required(),
                                        ]),
                                      ),
                                    ),
                                  ),

                                  ResponsiveGridCol(
                                    child: Center(
                                      child: ElevatedButton(
                                        onPressed: () {
                                          if (_formKey.currentState != null &&
                                              _formKey.currentState!
                                                  .saveAndValidate()) {
                                            // EasyLoading.show(status: 'loading...',dismissOnTap: true);
                                            // logic.registerRepo().then((result) {
                                            //   if(result){
                                            //     EasyLoading.showSuccess('Great Success!');
                                            //     Get.offAllNamed(RouteHelper.initial);
                                            //   }else{
                                            //     EasyLoading.showError('Failed with Error');
                                            //   }
                                            //   EasyLoading.dismiss();
                                            // });
                                          }
                                        },
                                        child: Text('Register'.tr),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ))),
              ],
            ),
          );
        }),
      ),
    );
  }
}
