

import 'package:flutter/material.dart';

class EmptyData extends StatelessWidget {
  const EmptyData({super.key});


  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/images/notfound.png',height: 120,width: 120,),
          const Text('No found data.'),
        ],
      ),
    );
  }
}
