import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String formatDate(String date) {
  DateTime parsedDate = DateTime.parse(date);
  String formattedDate = DateFormat('EEEE, MMMM d, yyyy').format(parsedDate);
  return formattedDate;
}
String shortFormatDate(String date) {
  DateTime parsedDate = DateTime.parse(date);
  String formattedDate = DateFormat('EEE, dd, yyyy').format(parsedDate);
  return formattedDate;
}

Future<void> selectDate(BuildContext context, TextEditingController controller) async {
  DateTime? picked = await showDatePicker(
    context: context,
    initialDate: DateTime.now(),
    firstDate: DateTime(1900),
    lastDate: DateTime(2100),
  );

  if (picked != null) {
    controller.text = DateFormat('yyyy-MM-dd').format(picked);
  }
}

Future<FilePickerResult?> pickFileGlobal() async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: FileType.any,
  );
  return result;
}

String getFileNameFromUrl(String url) {
  List<String> parts = url.split('/');
  return parts.isNotEmpty ? parts.last : '';
}

String convertToEnglishDateFormatted(DateTime date) {
  String formattedDate = DateFormat('d / MMM / yyyy').format(date);
  return formattedDate;
}