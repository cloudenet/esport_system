

import 'dart:convert';

import 'package:esport_system/data/model/monthly_test_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart'as http;
import '../../helper/generate_token.dart';
class MonthLyTestRepo{


  void searchPlayerMonthly({
    required String monthYear,
    required String playerCate,
    required String playerId,
}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/search-player-monthly-exam?player_cate_id=$playerCate&player_id=$playerId'));
      request.body = json.encode({
        "m_month_year": monthYear,
        "created_by": userInfo!.id.toString()
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      }
      else {
        print(response.reasonPhrase);
      }
    }catch(e){
      print("catch  search player monthly$e");
    }
  }


}