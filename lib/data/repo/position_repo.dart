import 'dart:convert';
import 'package:esport_system/data/model/position_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:http/http.dart' as http;

class PositionRepo {
  Future<Map<String, dynamic>> createPosition(
      {required String title, required String description}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.createPosition}'));
    request.body = json.encode({"title": title, "description": description});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Coach created successfully'};
    } else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }

  Future<List<PositionModel>> getPosition(
      {String? search, page, int limit = 8}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getPosition}'));
      request.body = json.encode(
          {"position_title": search, "page": page, "limit": limit});
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> getPositionData = res['data']['position'];
        print("data : $getPositionData");
        List<PositionModel> getPosition =
            getPositionData.map((e) => PositionModel.fromJson(e)).toList();
        print("Position data: ${getPositionData.length}");
        return getPosition;
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (error) {
      print("=====Error Position : $error");
      return [];
    }
  }

  Future<bool> updatePosition(
      {required String positionId,
      required String title,
      required String description}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.updatePosition}'));
      request.body = json.encode({
        "position_id": positionId,
        "title": title,
        "description": description
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true ;
      } else {
        print(response.reasonPhrase);
        return false ;
      }
    } catch (error) {
      print("======= Failed to Update Position $error");
      return false;
    }
  }

  Future<Map<String , dynamic>> deletePosition({required String positionId}) async{
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.deletePosition}'));
    request.body = json.encode({
      "position_id": positionId
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Event delete successfully'};
    }
    else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }
}
