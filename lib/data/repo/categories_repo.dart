


import 'dart:convert';
import 'package:esport_system/data/model/category_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart' as http;
import '../../helper/generate_token.dart';

class CategoriesRepo {

  Future<Map<String, dynamic>> createCategory({
    required String title ,
    required String subTitle ,
    required String fromAge ,
    required String toAge ,
  }) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/store-player-categories'));
      request.body = json.encode({
        "title": title,
        "other_title": subTitle,
        "parent": "",
        "form_age": fromAge,
        "end_age": toAge,
        "created_by": userInfo!.id
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Categories created successfully'};
      }
      else {
        return {'Failed create categories': false, 'message': "${response.reasonPhrase}"};
      }
    }catch(e){
      return {'Failed create categories': false, 'message': e};
    }
  }
  Future<Map<String, dynamic>> updateCategory({
    required String title ,
    required String pcID ,
    required String subTitle ,
    required String fromAge ,
    required String toAge ,
  }) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };

      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-player-categories'));
      request.body = json.encode({
        "pcate_id": pcID,
        "title": title,
        "other_title": subTitle,
        "parent": "",
        "form_age": fromAge,
        "end_age": toAge,
        "updated_by": userInfo!.id
      });

      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Categories created successfully'};
      }
      else {
        return {'Failed create categories': false, 'message': "${response.reasonPhrase}"};
      }
    }catch(e){
      return {'Failed create categories': false, 'message': e};
    }
  }

  Future<Map<String, dynamic>> deletedCategory({required String pcId}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/delete-player-categories'));
      request.body = json.encode({
        "pcate_id": pcId
      });

      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Categories deleted successfully'};
      }
      else {
        return {'Failed deleted categories': false, 'message': "${response.reasonPhrase}"};
      }
    }catch(e){
      return {'Failed deleted categories': false, 'message': e};
    }
  }





  Future<List<CategoryModel>> getCategory({
    required String searchTitle ,
    required int limit ,
    required int page ,
}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-list-data-player-categories'));
      request.body = json.encode({
        "search_term": searchTitle,
        "limit": limit,
        "page": page
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var a = jsonDecode(await response.stream.bytesToString());
        List<dynamic> listData = a['data']['player_categories'];
        return listData.map((value)=>CategoryModel.fromJson(value)).toList();
      }
      else {
        print(response.reasonPhrase);
        return[];
      }
    }catch(ee){
      print(" Catch in repo $ee");
      return[];
    }
  }
}