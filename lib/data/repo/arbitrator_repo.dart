import 'dart:convert';
import 'package:esport_system/data/model/arbitrator_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';


class ArbitratorRepo {
  Future<List<ArbitratorModel>> getArbitrator(
      {String? search, int limit = 6, page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-list-arbitrators'));
      request.body = json.encode({"search_term": search, "limit": limit, "page": page});
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> arbitratorData = res['data']['arbitrators'];
        List<ArbitratorModel> dataArbitrator =
            arbitratorData.map((e) => ArbitratorModel.fromJson(e)).toList();
        return dataArbitrator;
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (e) {
      print("Error Get arbitrator $e");
      return [];
    }
  }

  Future<Map<String,dynamic>> createArbitrator({
    required String assignUserID,
    required String rank,
    required String expYear,
    required String trainingDate,
    required XFile? bioFile, // Single bio file
    required List<PlatformFile> certificateFiles,
}) async {

    // print("in repo assignUserID ${assignUserID.toString()}");
    // print("in repo rank ${rank}");
    // print("in repo expYear ${expYear}");
    // print("in repo trainingDate ${trainingDate}");
    //
    // if (bioFile != null) {
    //   print("File name: ${bioFile.name}");
    //   print("File path: ${bioFile.path}");
    // }
    // for (var file in certificateFiles) {
    //   debugPrint('Picked file Repo: ${file.name}');
    // }

    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/create-arbitrators'));
      request.fields.addAll({
        "user_id": userInfo!.id.toString(),
        "assign_user_id": assignUserID,
        "rank": rank,
        "ex_year": expYear,
        "training_date": trainingDate,
      });
      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'bio_file',
          await bioFile!.readAsBytes(),
          filename: bioFile.path + bioFile.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('bio_file', bioFile!.path));
      }
      for (var file in certificateFiles) {
        request.files.add(http.MultipartFile.fromBytes(
          'certificate_file',
          file.bytes!,
          filename: file.name,
        ));
      }
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Arbitrator created successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Arbitrator created false'};
      }
    }catch(e){
      return {'success': false, 'message': 'Arbitrator created false:$e'};
    }

  }

  Future<Map<String,dynamic>> updateArbitrator({
    required String rankID,
    required String experienceYear,
    required String trainingDate,
    required String arbitratorsID,
    required XFile? bioFile, // Single bio file
    required List<PlatformFile> certificateFiles,
  }) async {

    // print("in repo assignUserID ${arbitratorsID.toString()}");
    // print("in repo rank ${rankID}");
    // print("in repo expYear ${experienceYear}");
    // print("in repo trainingDate ${trainingDate}");
    //
    // if (bioFile != null) {
    //   print("File name: ${bioFile.name}");
    //   print("File path: ${bioFile.path}");
    // }
    // for (var file in certificateFiles) {
    //   debugPrint('Picked file Repo: ${file.name}');
    // }

    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('http://167.172.81.224:8287/api/update-arbitrators'));
      request.fields.addAll({
        "user_id": userInfo!.id.toString(),
        "arbitrators_id": arbitratorsID,
        "rank": rankID,
        "ex_year": experienceYear,
        "training_date": trainingDate,
      });

      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'bio_file',
          await bioFile!.readAsBytes(),
          filename: bioFile.path + bioFile.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('bio_file', bioFile!.path));
      }

      for (var file in certificateFiles) {
        if (file.bytes != null) {
          request.files.add(http.MultipartFile.fromBytes(
            'certificate_file',
            file.bytes!,
            filename: file.name,
          ));
        } else {
          print("File bytes are null for ${file.name}----${file.bytes}");
        }
      }

      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Arbitrator update successfully'};
      }else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Arbitrator update false'};
      }
    }catch(e){
      print("Arbitrator update false: $e");
      return {'success': false, 'message': 'Arbitrator update false: $e'};
    }
  }

  Future<bool> deleteArbitrator({
    required String arbitratorsID}) async{
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/delete-arbitrators'));
      request.body = json.encode({
        "arbitrators_id": arbitratorsID
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }
    }catch(e){
      print("Error delete arbitrator $e");
      return false;
    }
  }


}
