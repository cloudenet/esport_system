import 'dart:convert';

import 'package:esport_system/data/model/item_model.dart';
import 'package:esport_system/data/model/item_request_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/request_item_section/list_request_item.dart';
import 'package:http/http.dart' as http;

import '../model/cart_item_model.dart';

class ItemRepo{

  Future<List<ItemModel>> getItem({required String? search, int limit = 6, page,
}) async{
    try{
      var headers = {
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-items?type_select=&search_term=active&limit=6&page=$page'));
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> itemData = res['data']['items'];
        List<ItemModel> getItemData = itemData.map((e) => ItemModel.fromJson(e)).toList();
        return getItemData;
      }
      else {
        print(response.reasonPhrase);
      }
      return [];

    }catch(e){
      print("Error get item $e");
      return [];
    }
  }

  Future<List<ItemsHistory>> itemsHistory({
    required String? itemId,
    required String? trainType,
    required String? fromDate,
    required String? toDate,
    required int limit,
    page
  }) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-detail-items-and-history?from_date=$fromDate&to_date=$toDate&limit=$limit&page=$page'));
      request.body = json.encode({
        "items_id": itemId,
        "train_type": trainType
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> historyList = res['data']['items_history'];
        List<ItemsHistory> itemsHistoryList = historyList.map((json) => ItemsHistory.fromJson(json)).toList();
        return itemsHistoryList;
      } else {
        print(response.reasonPhrase);
        return [];
      }
    }catch(e){
      print("issue catch: $e");
      return [];
    }
  }

  Future<bool> createItem({
    required String itemTitle,
    required String subTitle,
    required String status,
}) async{
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/create-items'));
    request.body = json.encode({
      "item_title": itemTitle,
      "other_title": subTitle,
      "is_status": status,
      "created_by": userInfo!.id.toString()
    });
    request.headers.addAll(headers);

    try{
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }

    }catch(e){
      print("Error : $e");
      return false;
    }
  }

  Future<bool> addQtyItem({required String itemID, required String qtyStock, required String noted}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/receive-items-stock'));
      request.body = json.encode({
        "item_id": itemID,
        "qty": qtyStock,
        "noted": noted,
        "created_by": userInfo!.id.toString()
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }
    }catch(e){
      print("Error : $e");
      return false;
    }
  }

  Future<bool> updateItem({
    required String itemID,
    required String itemTitle,
    required String otherTitle,
    required String isStatus,
    required String updatedBy,
}) async{
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-items'));
      request.body = json.encode({
        "items_id": itemID,
        "item_title": itemTitle,
        "other_title": otherTitle,
        "is_status": isStatus,
        "updated_by": updatedBy
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }
    }catch(e){
      print('Error update item $e');
      return false;
    }
  }

  Future<bool> deleteItem({
    required String itemID
}) async{
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/delete-items'));
      request.body = json.encode({
        "items_id": itemID
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }
    }catch(e){
      print("Error Delete Item $e");
      return false;
    }
  }

  Future<List<ItemRequestModel>> getListItemRequest({int? limit , int? page , String? search}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-list-request-items'));
    request.body = json.encode({
      "search_term": search,
      "limit": limit,
      "page": page
    });
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<dynamic> listItem = res['data']['request_items'];
      return listItem.map((e)=>ItemRequestModel.fromJson(e)).toList();
    }
    else {
      print(response.reasonPhrase);
      return [];
    }
  }

  Future<bool> createRequestItem({
    required String? refNum,
    required String? location,
    required String? teamId,
    required String? note,
    required List<Map<String, dynamic>> items,
}) async {
    // print("refNumberController repo $refNum");
    // print("selectLocationCode $location");
    // print("selectTeamId $teamId");
    // print("noteController $note");
    // if(items.isNotEmpty){
    //   for(int i=0 ; i<items.length ; i++){
    //     var a = items[i];
    //     //print("Item in cart ${a.items?.itemTitle} --- id: ${a.items?.id} ---${a.quantity} ---${a.subNote}");
    //   }
    // }
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/create-request-items'));
    request.body = json.encode({
      "ref_number": refNum,
      "location": location,
      "team_id": teamId,
      "is_status": "pending",
      "note": note,
      "created_by": userInfo?.id.toString(),
      "item_details": items
    });
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }
  }

  Future<bool> updateRequestItem({
    required String? rqId,
    required String? refNum,
    required String? location,
    required String? teamId,
    required String? note,
    required List<Map<String, dynamic>> items,
  }) async {

    print("rqId $rqId");
    print("refNumberController repo $refNum");
    print("selectLocationCode $location");
    print("selectTeamId $teamId");
    print("noteController $note");

    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-request-items'));
    request.body = json.encode({
      "req_item_id": rqId,
      "ref_number": refNum,
      "location": location,
      "team_id": teamId,
      "is_status": "pending",
      "note": note,
      "updated_by": userInfo!.id.toString(),
      "item_details": items
    });
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }
  }

  Future<bool> rejectedRequestItem({required String? rqId}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/authorization-cancelled-request-items'));
      request.body = json.encode({
        "req_item_id": rqId,
        "cancelled_by": userInfo?.id.toString()
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      } else {
        print('Request failed with status: ${response.statusCode}. Reason: ${response.reasonPhrase}');
        return false;
      }
    } catch (e) {
      print('An error occurred: $e');
      return false;
    }
  }

  Future<Map<String, dynamic>> receivedItem({
    required String? rqId,
    required String? refNum,
    required String? location,
    required String? teamId,
    required String? note,
    required List<Map<String, dynamic>> items,
}) async {

    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/authorization-received-request-items'));
      request.body = json.encode({
        "req_item_id": rqId,
        "ref_number": refNum,
        "location": location,
        "team_id": teamId,
        "is_status": "pending",
        "note": note,
        "created_by": userInfo!.id.toString(),
        "item_details": items
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Received items successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Failed items successfully ${response.reasonPhrase}'};
      }
    }catch(e){
      return {'success': false, 'message': 'Catch items successfully $e'};
    }
  }

}