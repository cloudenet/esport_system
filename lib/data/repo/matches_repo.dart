import 'dart:convert';
import 'package:esport_system/data/model/matches_model.dart';
import 'package:esport_system/data/model/registration_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart' as http;

class MatchesRepo {
  Future<bool> createMatches({
    required String eventId,
    required String homeTeamId,
    required String awayTeamId,
    required String startTime,
    required String endTime,
  }) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };

    var request = http.Request('POST',
        Uri.parse('${AppConstants.baseUrl}${AppConstants.createMatches}'));

    request.body = json.encode({
      "event_id": eventId,
      "home_team_id": homeTeamId,
      "away_team_id": awayTeamId,
      "start_time": startTime,
      "end_time": endTime,
      "created_by": userInfo!.id.toString()
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      // Assume a 200 status code means success
      print(await response.stream.bytesToString());
      return true;
    } else {
      // Print error reason and body
      print("Error: ${response.reasonPhrase}");
      String responseBody = await response.stream.bytesToString();
      print("Error body: $responseBody");
      return false;
    }
  }


  Future<void> updateStatusMatche({
    required String matcheId,
    required String? matcheStatus,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-status-match'));
      request.body = json.encode({
        "match_id": matcheId.toString(),
        "match_status": matcheStatus.toString(),
        "updated_by": userInfo!.id.toString(),
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      } else {
        // Print error reason and body
        print("Error: ${response.reasonPhrase}");
        String responseBody = await response.stream.bytesToString();
        print("Error body: $responseBody");
      }
    } catch (e) {
      print("Exception caught: $e");
    }
  }

  Future<void> updateModifyMatch({
    required String matcheId,
    required String winningId,
    required String losingId,
    required String? winningScore,
    required String losingScore,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/modify-finish-match'));
      request.body = json.encode({
        "match_id": matcheId.toString(),
        "winning_team_id": winningId.toString(),
        "losing_team_id": losingId.toString(),
        "home_team_score": winningScore.toString(),
        "away_team_score": losingScore.toString(),
        "updated_by": userInfo!.id.toString()
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      } else {
        print("Error: ${response.reasonPhrase}");
        String responseBody = await response.stream.bytesToString();
        print("Error body: $responseBody");
      }
    } catch (e) {
      print("Exception caught: $e");
    }
  }

  Future<List<MatchesModel>> getMatches({
    required String matchStatus,
    required int limit,
    required int page,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-list-matches'));
      request.body = json.encode({
        "match_status": matchStatus,
        "limit": limit,
        "page": page,
      });

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> getMatchData = res['data']['match'];
        List<MatchesModel> matches = getMatchData.map((e) => MatchesModel.fromJson(e)).toList();
        return matches;
      } else {
        print('Failed to fetch matches: ${response.reasonPhrase}');
        return [];
      }
    } catch (e) {
      print('Failed to fetch matches: $e');
      return [];
    }
  }

  Future<void> updateMatches({
    required String eventId,
    required String homeTeamId,
    required String awayTeamId,
    required String startTime,
    required String endTime,
    required String matchId,
    required String updatedBy,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-match'));
      request.body = json.encode({
        "event_id": eventId.toString(),
        "home_team_id": homeTeamId.toString(),
        "away_team_id": awayTeamId.toString(),
        "start_time": startTime.toString(),
        "end_time": endTime.toString(),
        "match_id": matchId.toString(),
        "updated_by": updatedBy.toString()
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      } else {
        // Print error reason and body
        print("Error: ${response.reasonPhrase}");
        String responseBody = await response.stream.bytesToString();
        print("Error body: $responseBody");
      }
    } catch (e) {
      print("Exception caught: $e");
    }
  }

  Future<Map<String,dynamic>> registerMatch({
    required String? teamId,
    required String? eventId,
    required String? des,
    required String? matchType,
    required String? venue,
    required List<Map<String, dynamic>> matchDetails
}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/create-registration-matches'));
      request.body = json.encode({
        "team_id": teamId,
        "event_id": eventId,
        "description": des,
        "match_type": matchType,
        "venue": venue,
        "created_by": userInfo!.id.toString(),
        "match_details": matchDetails
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        return {'success': true, 'message': 'successfully:${await response.stream.bytesToString()}'};
      }
      else {
        return {'success': false, 'message': 'Issue: ${response.reasonPhrase}'};
      }
    }catch(e){
      return {'success': false, 'message': 'Created false:$e'};
    }

  }
  Future<List<RegistrationMatchesModel>> getRegistrationMatch({required String? status, required int limit,
    required int page,}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/lists/get-registration-matches'));
    request.body = json.encode({
      "registration_status": status,
      "limit": limit,
      "page": page
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<dynamic> data = res['data'];
      List<RegistrationMatchesModel> listData = data.map((e) => RegistrationMatchesModel.fromJson(e)).toList();
      print("sdfdfdfdf:${listData.length}");
      return listData;
    }
    else {
      print(response.reasonPhrase);
      return [];
    }

  }

  Future<Map<String,dynamic>> updateRegistrationMatch({
    required String id,
    required String? teamId,
    required String? eventId,
    required String? des,
    required String? matchType,
    required String? venue,
    required List<Map<String, dynamic>> matchDetails
}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/edit-registration-matches?registration_id=$id'));
      request.body = json.encode({
        "team_id": teamId,
        "event_id": eventId,
        "description": des,
        "match_type": matchType,
        "venue": venue,
        "updated_by": userInfo!.id,
        "match_details": matchDetails
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        return {'success': true, 'message': 'successfully:${await response.stream.bytesToString()}'};
      }
      else {
        return {'success': false, 'message': 'Issue: ${response.reasonPhrase}'};
      }
    }catch(e){
      return {'success': false, 'message': 'Created false:$e'};
    }

  }

  Future<Map<String,dynamic>> authorizeRegistration({
    required String id,
    required String status,
    required String approvedNote,
  }) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/authorize-registration-matches?registration_id=$id'));
      request.body = json.encode({
        "approved_by": userInfo!.id,
        "registration_status": status,
        "approved_note": approvedNote
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        return {'success': true, 'message': 'successfully:${await response.stream.bytesToString()}'};
      }
      else {
        return {'success': false, 'message': 'Issue: ${response.reasonPhrase}'};
      }
    }catch(e){
      return {'success': false, 'message': 'Authorize false:$e'};
    }

  }


}
