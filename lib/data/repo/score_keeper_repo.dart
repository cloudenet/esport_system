import 'dart:convert';

import 'package:esport_system/data/model/scorekepper_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class ScoreKeeperRepo {
  Future<Map<String, dynamic>> createScoreKeeper({
    required String assignUserID,
    required String rank,
    required String experienceYear,
    required String trainingDate,
    required XFile? bioFile,
    required List<PlatformFile> certificateFiles,
  }) async {
    try {
      var headers = {
        'Authorization': 'Bearer ${await globalGenerateToken()}',
      };
      var request = http.MultipartRequest(
        'POST',
        Uri.parse(
            '${AppConstants.baseUrl}/api/v1/officials/create-scorekeepers'),
      );
      request.fields.addAll({
        'assign_user_id': assignUserID,
        'user_id': userInfo!.id.toString(),
        'rank': rank,
        'ex_year': experienceYear,
        'training_date': trainingDate,
      });

      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'bio_file',
          await bioFile!.readAsBytes(),
          filename: bioFile.path + bioFile.name,
        ));
      } else {
        request.files
            .add(await http.MultipartFile.fromPath('bio_file', bioFile!.path));
      }

      for (var file in certificateFiles) {
        request.files.add(http.MultipartFile.fromBytes(
          'certificate_file',
          file.bytes!,
          filename: file.name,
        ));
      }
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        final responseData = await response.stream.bytesToString();
        print(responseData);
        return {'success': true, 'message': 'Scorekeeper created successfully'};
      } else {
        final error = response.reasonPhrase ?? 'Unknown error';
        print("Failed during create scorekeeper: $error");
        return {'success': false, 'message': error};
      }
    } catch (e) {
      print("Error during create scorekeeper in catch: $e");
      return {'success': false, 'message': e.toString()};
    }
  }

  Future<List<ScorekeeperModel>> getListScorekeeper(
      {String? search, int limit = 6, page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST',
          Uri.parse(
              '${AppConstants.baseUrl}/api/v1/officials/get-list-scorekeepers'));
      request.body =
          json.encode({"search_term": search, "limit": limit, "page": page});
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> data = res['data']['scorekeepers'];
        print("Scorekeeper Date: $data");
        return data.map((value) => ScorekeeperModel.fromJson(value)).toList();
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (e) {
      print("Error get list scorekeeper in catch $e");
      return [];
    }
  }

  Future<Map<String, dynamic>> updateScorekeeper({
    required String scorekeeperId,
    required String rankId,
    required String exYears,
    required String trainDate,
    required XFile? bioFile,
    required List<PlatformFile> certificateFiles,
  })async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/officials/update-scorekeepers'));
      request.fields.addAll({
        'scorekeepers_id': scorekeeperId,
        'user_id': userInfo!.id.toString(),
        'rank': rankId,
        'ex_year': exYears,
        'training_date': trainDate
      });
      if (bioFile != null) {
        if (kIsWeb) {
          request.files.add(http.MultipartFile.fromBytes(
            'bio_file',
            await bioFile.readAsBytes(),
            filename: bioFile.name,
          ));
        } else {
          request.files.add(await http.MultipartFile.fromPath('bio_file', bioFile.path));
        }
      }
      for (var file in certificateFiles) {
        if (file.bytes != null) {
          request.files.add(http.MultipartFile.fromBytes(
            'certificate_file',
            file.bytes!,
            filename: file.name,
          ));
        } else {
          print("Skipping file ${file.name}, as bytes are null");
        }
      }

      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Scorekeeper updated successfully'};
      } else {
        print("Response failed with status code: ${response.reasonPhrase}");
        return {'success': false, 'message': 'Scorekeeper update failed'};
      }

    } catch (e) {
      print("Error in catch block: $e");
      return {'success': false, 'message': 'Scorekeeper update failed'};
    }
  }

  Future<Map<String,dynamic>> deleteScoreKeeper({required String scoreId}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/delete-scorekeepers'));
      request.body = json.encode({
        "scorekeepers_id": scoreId
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Umpires deleted successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Umpires deleted failed ${response.reasonPhrase}'};
      }
    }catch(e){
      print("Issue catch delete Umpire:$e");
      return {'success': false, 'message': 'Umpires deleted failed'};
    }

  }

}
