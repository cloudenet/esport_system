import 'dart:convert';

import 'package:esport_system/data/model/event_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:http/http.dart' as http;

class EventRepo {
  Future<void> createEvent(
      {required String title,
      required String address,
      required String description,
      required String startDate,
      required String endDate,
      required String createdBy}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse("${AppConstants.baseUrl}${AppConstants.createEvent}"));
    request.body = json.encode({
      "title": title,
      "address": address,
      "description": description,
      "start_date": startDate,
      "end_date": endDate,
      "created_by": createdBy
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      String responseBody = await response.stream.bytesToString();
      var jsonResponse = json.decode(responseBody);
      print("$jsonResponse");
    } else {
      print(response.reasonPhrase);
    }
  }

  Future<List<EventModel>> getEvent({String? search, page, int limit = 6}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getEvent}'));
      request.body = json.encode({
        "searchTerm": search,
        "page": page,
        "limit": limit
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> dataEvent = res['data']['event'];
        print("Data : $dataEvent");
        List<EventModel> getListEvent =
            dataEvent.map((e) => EventModel.fromJson(e)).toList();
        print("Data length : ${getListEvent.length}");
        return getListEvent;
      } else {
        print(response.reasonPhrase);
      }
    } catch (e) {
      print("Error : $e");
    }
    return [];
  }

  Future<void> updateEvent(
      {required String eventID,
      required String title,
      required String address,
      required String description,
      required String startDate,
      required String endDate,
      required String updatedBy}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.updateEvent}'));
      request.body = json.encode({
        "event_id": eventID,
        "title": title,
        "address": address,
        "description": description,
        "start_date": startDate,
        "end_date": endDate,
        "updated_by": updatedBy
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      } else {
        print(response.reasonPhrase);
      }
    } catch (error) {
      print("failed to update event: $error");
    }
  }

  Future<Map<String , dynamic>> deleteEven({required String eventID}) async{
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.deleteEvent}'));
    request.body = json.encode({
      "event_id": eventID
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Event delete successfully'};
    }
    else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }

  Future<List<EventModel>> getEventDetail({required String eventID}) async{
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getEventDetail}'));
      request.body = json.encode({
        "event_id": eventID
      });
      request.headers.addAll(headers);

      http.StreamedResponse streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);

      if (response.statusCode == 200) {
        var res = jsonDecode(response.body);
        if (res['data'] != null && res['data']['event'] != null) {
          List<dynamic> eventJson = [res['data']['event']];
          print("-------HHH $eventJson");
          List<EventModel> dataEventJson = eventJson.map((event) => EventModel.fromJson(event)).toList();
          return dataEventJson;
        } else {
          print('No user data found in the response.');
          return [];
        }
      } else {
        print('Request failed with status: ${response.statusCode}.');
        print('Reason: ${response.reasonPhrase}');
        return [];
      }
    }catch(e){
      print("Detail Event Error : $e");
    }
    return [];
  }
}
