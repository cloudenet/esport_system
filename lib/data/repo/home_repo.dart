import 'dart:convert';

import 'package:esport_system/data/model/total_item_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:http/http.dart' as http;

class HomeRepo {
  Future<void> createTeam() async {
    var headers = {
      'Authorization':
          'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE3MTYwMDgzMDgsImV4cCI6MTcxNjAxODMwOH0.ktxIg1qXVAWpr6ImtnNgjE-5GZAdUZdFsBt0ij72HpE'
    };
    var request = http.MultipartRequest(
        'POST', Uri.parse('http://167.172.81.224:8287/api/create-team'));
    request.fields.addAll({
      'user_id': '1',
      'team_name': 'Rean AI',
      'founded_date': '',
      'stadium': '',
      'province_code': '03',
      'district_code': '302',
      'village_code': '30206',
      'commune_code': '3020603',
      'created_at': '2024-05-16 19:58:52',
      'updated_at': '2024-05-16 19:58:52',
      'province': '',
      'district': '',
      'commune': '',
      'village': ''
    });
    request.files
        .add(await http.MultipartFile.fromPath('myLogo', '/path/to/file'));
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
    } else {
      print(response.reasonPhrase);
    }
  }

  Future<TotalItem> fetchItemCounts() async {
    var headers = {'Authorization': 'Bearer ${await globalGenerateToken()}'};

    var url = Uri.parse('${AppConstants.baseUrl}/api/get-total-items');
    var response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      var jsonData = jsonDecode(response.body);
      var data = TotalItem.fromJson(jsonData['data']);
      print('${data.totalMatches}');
      return data;
    } else {
      throw Exception('Failed to load item counts');
    }
  }
}
