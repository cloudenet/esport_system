import 'dart:convert';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/data/model/team_model.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

import '../model/player_in_team_model.dart';

class TeamRepository {
  Future<List<Address>> getAddresses() async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": ""});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
  }

  Future<List<Address>> getDistricts(String provinceCode) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": provinceCode});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
    // Implementation for fetching districts based on provinceCode
  }

  Future<List<Address>> getCommunes(String districtCode) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": districtCode});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
    // Implementation for fetching communes based on districtCode
  }

  Future<List<Address>> getVillages(String villageCode) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": villageCode});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
    // Implementation for fetching communes based on districtCode
  }

  Future<void> createTeam({
    required String teamName,
    required String foundDate,
    required String stadium,
    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String district,
    required String commune,
    required String villages,
    required String championshipsWon,
    required String leagueId,
    required XFile? logo,
  }) async {
    try {
      var headers = {'Authorization': 'Bearer ${await globalGenerateToken()}'};
      var request = http.MultipartRequest(
        'POST',
        Uri.parse('${AppConstants.baseUrl}/api/create-team'),
      );
      request.fields.addAll({
        'user_id': userInfo!.id.toString(),
        'team_name': teamName,
        'founded_date': foundDate,
        'stadium': stadium,
        'province_code': provinceCode,
        'district_code': districtCode,
        'village_code': villageCode,
        'commune_code': communeCode,
        'province': province,
        'district': district,
        'commune': commune,
        'village': villages,
        'championships_won': championshipsWon,
        'parent_id': leagueId
      });

      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
            'myLogo',
            await logo!.readAsBytes().then((value) {
              return value.cast();
            }),
            filename: logo.path.toString() + logo.name));
      } else {
        request.files
            .add(await http.MultipartFile.fromPath('myLogo', logo!.path));
      }
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(
            '==================success${await response.stream.bytesToString()}');
      } else {
        print("=================error${await response.stream.bytesToString()}");
      }
    } catch (e) {
      print("=================error eeeee $e");
    }
  }

  Future<List<TeamModel>> getTeam(
      {required String userId, String? search, page, dynamic limit = 6}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request =
        http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-team'));
    request.body = json.encode({
      "user_id": userId,
      "team_id": "",
      "team_name": search,
      "page": page,
      "limit": limit
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<dynamic> getListTeam = res['data']['team'];
      print('==========Data : $getListTeam');
      List<TeamModel> getTeamList =
          getListTeam.map((json) => TeamModel.fromJson(json)).toList();
      return getTeamList;
    } else {
      print("ERROR");
      return [];
    }
  }

  Future<void> updateTeam({
    required String teamId,
    required String teamName,
    required String foundedDate,
    required String stadium,
    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String district,
    required String commune,
    required String village,
    required String championshipsWon,
    required String leagueId,
    required XFile? logo,
  }) async {
    try {
      var headers = {'Authorization': 'Bearer ${await globalGenerateToken()}'};
      var request = http.MultipartRequest('POST',
          Uri.parse('${AppConstants.baseUrl}${AppConstants.updateTeam}'));
      request.fields.addAll({
        'team_id': teamId,
        'team_name': teamName,
        'founded_date': foundedDate,
        'stadium': stadium,
        'province_code': provinceCode,
        'district_code': districtCode,
        'village_code': villageCode,
        'commune_code': communeCode,
        'province': province,
        'district': district,
        'commune': commune,
        'village': village,
        'championships_won': championshipsWon,
        'parent_id': leagueId
      });

      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'myLogo',
          await logo!.readAsBytes().then((value) {
            return value.cast();
          }),
          filename: logo.path.toString() + logo.name,
        ));
      } else {
        request.files
            .add(await http.MultipartFile.fromPath('myLogo', logo!.path));
      }

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print('Team updated successfully');
      } else {
        String errorMessage = await response.stream.bytesToString();
        Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
        String error = errorResponse['message'] ?? 'An error occurred';
        print('Error: $error');
      }
    } catch (e) {
      print('Exception: $e');
    }
  }

  Future<Map<String, dynamic>> deleteTeam({required String teamID}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.deleteTeam}'));
    request.body = json.encode({"team_id": teamID});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Team delete successfully'};
    } else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }

  Future<List<PlayerInTeamModel>> fetchPlayersInTeam(String teamId) async {
    const String apiUrl = '${AppConstants.baseUrl}/api/get-player-in-team';
    final Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse(apiUrl));
    request.body = json.encode({"team_id": teamId});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      String responseBody = await response.stream.bytesToString();
      List<dynamic> jsonData = json.decode(responseBody)['data']['player'];
      List<PlayerInTeamModel> players = jsonData.map((jsonItem) => PlayerInTeamModel.fromJson(jsonItem)).toList();
      return players;
    } else {
      print('Failed to load players: ${response.reasonPhrase}');
      return [];
    }
  }
}
