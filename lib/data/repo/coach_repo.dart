import 'dart:convert';
import 'package:esport_system/data/model/coach_data_model.dart';
import 'package:esport_system/data/model/coach_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class CoachRepo {

  static const String baseLocal = "http://localhost:1120/api/" ;

  Future<Map<String, dynamic>> createCoach({
    required String assignUserId,
    required String teamId,
    required String experienceYear,
    required String specialty,
    required String hireDate,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String championshipWon,
    required String matchesWon,
    required String matchesLost,
    required String matchesDrawn,
    required String biography,
    required String userId,
  })
  async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}',
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.createCoach}'));
    request.body = json.encode({
      "assign_user_id": assignUserId,
      "team_id": teamId,
      "experience_years": experienceYear,
      "specialty": specialty,
      "hire_date": hireDate,
      "contract_start_date": contractStartDate,
      "contract_end_date": contractEndDate,
      "salary": salary,
      "championships_won": championshipWon,
      "matches_won": matchesWon,
      "matches_lost": matchesLost,
      "matches_drawn": matchesDrawn,
      "biography": biography,
      "user_id": userId,
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Coach created successfully'};
    } else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }

  Future<Map<String, dynamic>> createCoachV2({
    required String assignUserId,
    required String teamId,
    required String experienceYear,
    required String specialty,
    required String hireDate,
    required String startDate,
    required String endDate,
    required String salary,
    required String championshipsWon,
    required String matchesWon,
    required String matchesLost,
    required String rankId,
    required XFile? bioFile,
  })
  async {
    try {
      var headers = {
        'Authorization': 'Bearer ${await globalGenerateToken()}',
      };

      var request = http.MultipartRequest(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/create-coach'),
      );

      request.fields.addAll({
        'user_id': userInfo!.id.toString(),
        'assign_user_id': assignUserId,
        'team_id': teamId,
        'experience_years': experienceYear,
        'specialty': specialty,
        'hire_date': hireDate,
        'contract_start_date': startDate,
        'contract_end_date': endDate,
        'salary': salary,
        'championships_won': championshipsWon,
        'matches_won': matchesWon,
        'matches_lost': matchesLost,
        'rank_id': rankId,
      });

      // Handle file upload for both web and mobile
      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'biography_coaches', await bioFile!.readAsBytes(),
          filename: bioFile.path + bioFile.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('biography_coaches', bioFile!.path));
      }

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        final responseData = await response.stream.bytesToString();
        print(responseData);
        return {'success': true, 'message': 'Coach created successfully'};
      } else {
        final error = response.reasonPhrase ?? 'Unknown error';
        print("Failed during create coach: $error");
        return {'success': false, 'message': error};
      }
    } catch (e) {
      print("Error during create coach: $e");
      return {'success': false, 'message': e.toString()};
    }
  }

  Future<bool> updateCoachV2({
    required String coachId,
    required String rankId,
    required String teamId,
    required String experienceYear,
    required String specialty,
    required String hireDate,
    required String startDate,
    required String endDate,
    required String salary,
    required String championshipWon,
    required String matchWon,
    required String matchLost,
    required XFile? bioFile
})
  async{
    try{
      var headers = {
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-coach'));
      request.fields.addAll({
        'coach_id': coachId,
        'rank_id': rankId,
        'team_id': teamId,
        'experience_years': experienceYear,
        'specialty': specialty,
        'hire_date': hireDate,
        'contract_start_date': startDate,
        'contract_end_date': endDate,
        'salary': salary,
        'championships_won': championshipWon,
        'matches_won': matchWon,
        'matches_lost': matchLost
      });
      if (kIsWeb) {
        if(bioFile!=null){
          request.files.add(http.MultipartFile.fromBytes(
            'biography_coaches', await bioFile.readAsBytes(),
            filename: bioFile.path + bioFile.name,
          ));
        }

      } else {
        request.files.add(await http.MultipartFile.fromPath('biography_coaches', bioFile!.path));
      }

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }

    }catch(e){
      print("Update coach error in repo $e");
      return false;
    }
  }

  Future<List<CoachModel>> getCoach({int limit = 6, page, String? search}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getCoach}'));
      request.body = json.encode({
        "user_id": userInfo!.id.toString(),
        "coach_name": search,
        "limit": limit,
        "page": page
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> getCoachData = res['data']['coach'];
        print('==========DataCoach : $getCoachData');
        List<CoachModel> getCoach = getCoachData.map((e) => CoachModel.fromJson(e)).toList();
        print("Data : ${getCoach.length}");
        return getCoach;
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (e) {
      print("=====Error Coach : $e");
      return [];
    }
  }


  Future<List<CoachDataModel>> getListCoachAndAssistant({
    required String typeCoach,
    String? coachName ,
    int? page ,
    int? limit ,
  })
  async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await localGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('$baseLocal$typeCoach/get-list'));
      request.body = json.encode({
        "user_id": userInfo!.id.toString(),
        "coach_name": coachName??"",
        "limit": limit ?? "",
        "page": page ?? ""
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> listRes = res['data'];
        return listRes.map((v)=>CoachDataModel.fromJson(v)).toList();
      }
      else {
        print(response.reasonPhrase);
        return [];
      }
    }catch(e){
      print("Error deleting coach: $e");
      return [];
    }
  }

  Future<Map<String,dynamic>> assignCoachAndAssistant({
    required String assignUserId,
    required String teamId,
    required String rankId,
    required String experienceYears,
    required String specialty,
    required String joinDate,
    required String salary,
    required String championshipsWon,
    required String coachesType,
    required String type,
    required XFile? biographyCoaches
})
  async {
    try{
      var headers = {
        'Authorization': 'Bearer ${await localGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('$baseLocal$type/create'));
      request.fields.addAll({
        'user_id': userInfo!.id.toString(),
        'assign_user_id': assignUserId,
        'team_id': teamId,
        'rank_id': rankId,
        'experience_years': experienceYears,
        'specialty': specialty,
        'join_date': joinDate,
        'salary': salary,
        'championships_won': championshipsWon,
        'coaches_type': coachesType
      });

      print("log body: ${request.fields}");

      if (kIsWeb) {
        if(biographyCoaches!=null){
          request.files.add(http.MultipartFile.fromBytes(
            'biography_coaches', await biographyCoaches.readAsBytes(),
            filename: biographyCoaches.path + biographyCoaches.name,
          ));
        }

      } else {
        request.files.add(await http.MultipartFile.fromPath('biography_coaches', biographyCoaches!.path));
      }
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Coach created successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Coach created unsuccessfully'};
      }
    }catch(e){
      return {'success': false, 'message': e.toString()};
    }
  }

  Future<Map<String,dynamic>> updateAssignCoachAndAssistant({
    required String id,
    required String assignUserId,
    required String teamId,
    required String rankId,
    required String experienceYears,
    required String specialty,
    required String joinDate,
    required String salary,
    required String championshipsWon,
    required String coachesType,
    required String type,
    required XFile? biographyCoaches
})
  async {
    try{
      var headers = {
        'Authorization': 'Bearer ${await localGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('$baseLocal$type/update'));
      request.fields.addAll({
        'id': id,
        'user_id': userInfo!.id.toString(),
        'assign_user_id': assignUserId,
        'team_id': teamId,
        'rank_id': rankId,
        'experience_years': experienceYears,
        'specialty': specialty,
        'join_date': joinDate,
        'salary': salary,
        'championships_won': championshipsWon,
        'coaches_type': championshipsWon
      });
      if (kIsWeb) {
        if(biographyCoaches!=null){
          request.files.add(http.MultipartFile.fromBytes(
            'biography_coaches', await biographyCoaches.readAsBytes(),
            filename: biographyCoaches.path + biographyCoaches.name,
          ));
        }

      } else {
        request.files.add(await http.MultipartFile.fromPath('biography_coaches', biographyCoaches!.path));
      }
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Coach updated successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Coach updated unsuccessfully'};
      }
    }catch(e){
      return {'success': false, 'message': e.toString()};
    }
  }

  Future<Map<String,dynamic>> deleteAssignCoachAndAssistant({required String type,required String coachId}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await localGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('$baseLocal$type/delete'));
      request.body = json.encode({
        "coach_id": coachId
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Coach deleted successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Coach deleted unsuccessfully'};
      }
    }catch(e){
      return {'success': false, 'message': e.toString()};
    }
  }

  Future<void> deleteCoach({required String coachId}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.deleteCoach}'));
      request.body = json.encode({"coach_id": coachId});
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print('Coach deleted successfully');
      } else {
        String errorMessage = await response.stream.bytesToString();
        Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
        String error = errorResponse['message'] ?? 'An error occurred';
        throw Exception(error);
      }
    } catch (error) {
      print("Error deleting coach: $error");
    }
  }

  Future<Map<String, dynamic>> updateCoach({
    required String coachId,
    required String teamId,
    required String experinceYear,
    required String specialty,
    required String hireDate,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String championhshipsWon,
    required String matchesWon,
    required String matchesLost,
    required String matchesDrawn,
    required String biography,
  })
  async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.updateCoach}'));
    request.body = json.encode({
      "coach_id": coachId,
      "team_id": teamId,
      "experience_years": experinceYear,
      "specialty": specialty,
      "hire_date": hireDate,
      "contract_start_date": contractStartDate,
      "contract_end_date": contractEndDate,
      "salary": salary,
      "championships_won": championhshipsWon,
      "matches_won": matchesWon,
      "matches_lost": matchesLost,
      "matches_drawn": matchesDrawn,
      "biography": biography
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Coach created successfully'};
    } else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }
}
