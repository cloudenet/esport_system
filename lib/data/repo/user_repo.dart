import 'dart:convert';
import 'package:esport_system/data/model/address_assign.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class UserRepo {

  Future<Map<String, dynamic>> createUser({
    required String cardNumber,
    required String userName,
    required String phone,
    required String email,
    required String password,
    required String gender,
    required String dob,
    required String address,
    required String provinceCode,
    required String districtCode,
    required String communeCode,
    required String villageCode,
    required String provinceName,
    required String districtName,
    required String communeName,
    required String villageName,
    required String name,
    required String nameKhmer,
    required XFile? profile,
    required List<XFile> myIdentity,
  }) async {
    var headers = {
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/create-user'));
    request.fields.addAll({
      'card_number': cardNumber,
      'username': userName,
      'phone': phone,
      'email': email,
      'password': password,
      'gender': gender,
      'dob': dob,
      'address': address,
      'created_by': userInfo!.id.toString(),
      'province_code': provinceCode,
      'district_code': districtCode,
      'commune_code': communeCode,
      'village_code': villageCode,
      'name': name,
      'province': provinceName,
      'district': districtName,
      'commune': communeName,
      'village': villageName,
      'name_khmer':nameKhmer
    });

    if (kIsWeb) {
      request.files.add(http.MultipartFile.fromBytes(
        'myProfile',
        await profile!.readAsBytes(),
        filename: profile.path + profile.name,
      ));
    } else {
      request.files.add(await http.MultipartFile.fromPath('myProfile', profile!.path));
    }
    for (var file in myIdentity) {
      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'myIdentity',
          await file.readAsBytes(),
          filename: file.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('myIdentity', file.path));
      }
    }

    request.headers.addAll(headers);
    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var jsonResponse = json.decode(await response.stream.bytesToString());
        return {'success': true, 'message': jsonResponse['message']};
      }
      else {
        var jsonResponse = json.decode(await response.stream.bytesToString());
        return {'success': false, 'message': jsonResponse['message']};
      }
    }catch(e){
      print("Issue in catch $e");
      return {'success': false, 'message': "Failed in catch"};
    }
  }




  Future<List<ListUserModel>> getListUser({dynamic limit = 6, String search = '', page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };

      var request = http.Request(
          'POST',
          Uri.parse('${AppConstants.baseUrl}${AppConstants.getUser}')
      );
      request.body = json.encode({
        "user_id": userInfo!.id,
        "searchTerm": search,
        "limit": limit,
        "page": page
      });

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> getListUser = res['data']['users'];
        List<ListUserModel> getUserList = getListUser.map((e) => ListUserModel.fromJson(e)).toList();
        return getUserList;
      } else {
        print(response.reasonPhrase);
        return [];
      }
    } catch (e) {
      print("============ Error GetListUser : $e");
      return [];
    }
  }

  Future<List<ListUserModel>> getAllUser({dynamic limit = 6, String search = '', page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };

      var request = http.Request(
          'POST',
          Uri.parse('${AppConstants.baseUrl}${AppConstants.getUser}')
      );
      request.body = json.encode({
        "user_id": userInfo!.id,
        "searchTerm": search,
        "limit": "",
        "page": ""
      });

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> getListUser = res['data']['users'];
        List<ListUserModel> getUserList = getListUser.map((e) => ListUserModel.fromJson(e)).toList();
        return getUserList;
      } else {
        print(response.reasonPhrase);
        return [];
      }
    } catch (e) {
      print("============ Error GetListUser : $e");
      return [];
    }
  }

  Future<List<ListUserModel>> getListUserDropDown({dynamic limit = 6, String search = '', page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-list-user-normal')
      );
      request.body = json.encode({
        "user_id": userInfo!.id,
        "searchTerm": search,
        "limit": limit,
        "page": page
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> getListUser = res['data']['users'];
        List<ListUserModel> getUserList = getListUser.map((e) => ListUserModel.fromJson(e)).toList();
        return getUserList;
      } else {
        print(response.reasonPhrase);
        return [];
      }
    } catch (e) {
      print("============ Error GetListUser : $e");
      return [];
    }
  }

  // Service Get User Detail
  Future<List<ListUserModel>> getUserDetail() async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}',
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getUserDetail}'));
      request.body = json.encode({"user_id": 8});
      request.headers.addAll(headers);

      http.StreamedResponse streamedResponse = await request.send();
      http.Response response = await http.Response.fromStream(streamedResponse);


      if (response.statusCode == 200) {
        var res = jsonDecode(response.body);
        if (res['data'] != null && res['data']['user'] != null) {
          List<dynamic> usersJson = [res['data']['user']];
          List<ListUserModel> dataUserJson =
          usersJson.map((user) => ListUserModel.fromJson(user)).toList();
          print("=========== UserDetail : ${dataUserJson.length}");
          return dataUserJson;
        } else {
          print('No user data found in the response.');
          return [];
        }
      } else {
        print('Request failed with status: ${response.statusCode}.');
        print('Reason: ${response.reasonPhrase}');
        return [];
      }
    } catch (e) {
      print("========== GetUser Detail Error: $e");
      return [];
    }
  }

  Future<bool> updateUserInfo({
    required String userId,
    required String name,
    required String email,
    required String identityNo,
    required String nationality,
    required String gender,
    required String dob,
    required String address,
    required String updatedBy,
    required String cardNumber,
    required XFile identityImagePath,
  }) async {
    var headers = {
      'Authorization': 'Bearer ${await globalGenerateToken()}',
    };

    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.updateUser}'),
    );

    request.fields.addAll({
      'user_id': userId,
      'name': name,
      'email': email,
      'identity_no': identityNo,
      'nationality': nationality,
      'gender': gender,
      'dob': dob,
      'address': address,
      'updated_by': updatedBy,
      'card_number': cardNumber,
    });
    if (kIsWeb) {
      request.files.add(http.MultipartFile.fromBytes(
          'myIdentity',
          await identityImagePath.readAsBytes().then((value) {
            return value.cast();
          }),
          filename:
          identityImagePath.path.toString() + identityImagePath.name));
    } else {
      request.files.add(await http.MultipartFile.fromPath(
          'myIdentity', identityImagePath.path));
    }

    request.headers.addAll(headers);
    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true ;
      } else {
        print(response.reasonPhrase);
        return false ;
      }
    }catch(e){
      return false;
    }
  }

  Future<bool> updateUser({
    required String userId,
    required String name,
    required String khmerName,
    required String email,
    required String identityNo,
    required String nationality,
    required String gender,
    required String dob,
    required String address,
    required String updatedBy,
    required String cardNumber,
    required String provinceCode,
    required String districtCode,
    required String communeCode,
    required String villageCode,
    required String provinceName,
    required String districtName,
    required String communeName,
    required String villageName,
    required XFile identityImagePath,
  }) async {

    // print("in repo cardNumber ${cardNumber.toString()}");
    // print("in repo userName ${name}");
    // print("in repo email ${email}");
    // print("in repo email ${identityNo}");
    // print("in repo email ${nationality}");
    // print("in repo email ${email}");
    // print("in repo gender ${gender}");
    // print("in repo dob ${dob}");
    // print("in repo address ${address}");
    // print("in repo userInfo ${userInfo!.id.toString()}");
    // print("in repo provinceCode ${provinceCode} -- ${provinceName}");
    // print("in repo districtCode ${districtCode} -- ${districtName}");
    // print("in repo communeCode ${communeCode} -- ${communeName}");
    // print("in repo villageCode ${villageCode} -- ${villageName}");
    var headers = {
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-info-user'));
    request.fields.addAll({
      'user_id': userId,
      'name': name,
      'email': email,
      'identity_no': identityNo,
      'nationality': nationality,
      'gender': gender,
      'dob': dob,
      'address': address,
      'updated_by': userInfo!.id.toString(),
      'card_number': cardNumber,
      'province_code': provinceCode,
      'district_code': districtCode,
      'commune_code': communeCode,
      'village_code': villageCode,
      'province': provinceName,
      'district': districtName,
      'commune': communeName,
      'village': villageName,
      'name_khmer':khmerName
    });
    //request.files.add(await http.MultipartFile.fromPath('myIdentity', '/C:/Users/Wathana/Pictures/Bac1.avif'));
    if (kIsWeb) {
      request.files.add(http.MultipartFile.fromBytes(
          'myIdentity',
          await identityImagePath.readAsBytes().then((value) {
            return value.cast();
          }),
          filename: identityImagePath.path.toString() + identityImagePath.name));
    } else {
      request.files.add(await http.MultipartFile.fromPath(
          'myIdentity', identityImagePath.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }

  }


  Future<bool> updateUserPassword(
      {required String userId, required String newPassword}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/change-password-user'));
      request.body = json.encode({"user_id": userId, "new_password": newPassword});
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true ;
      } else {
        print(response.reasonPhrase);
        return false;
      }
    } catch (e) {
      print("========= Password change error $e");
      return false;
    }
  }

  Future<Map<String, dynamic>> upDateProfileUser({required XFile? imagePath}) async{
    var headers = {
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-profile-user'));
    request.fields.addAll({
      'user_id': userInfo!.id.toString()
    });
    if (kIsWeb) {
      request.files.add(http.MultipartFile.fromBytes(
          'myProfile',
          await imagePath!.readAsBytes().then((value) {
            return value.cast();
          }),
          filename: imagePath.path.toString() + imagePath.name)
      );
    } else {
      request.files.add(await http.MultipartFile.fromPath('myProfile', imagePath!.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(await response.stream.bytesToString());
      print("success change : $jsonResponse");
      return {'success': true, 'message': jsonResponse['message']};
    }
    else {
      print(response.reasonPhrase);
      return {'success': false, 'message': response.reasonPhrase};
    }

  }
  Future<Map<String , dynamic>> deleteUser  ({required String userid}) async{
    var headers = {
      'Content-Type': 'application/json',
    'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.deleteUser}'));
    request.body = json.encode({
      "user_id": userid
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Team delete successfully'};
    }
    else {
    print(response.reasonPhrase);
    String errorMessage = await response.stream.bytesToString();
    Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
    String error = errorResponse['message'] ?? 'An error occurred';
    return {'success': false, 'message': error};
    }
  }

  Future<Map<String , dynamic>> assignControlUser({
    required String level,
    required String userID,
    required String province,
    required String district,
    required String commune,
    required String village,
  }) async{
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.assignControlUser}'));
    request.body = json.encode({
      "level": level,
      "user_id": userID,
      "province": province,
      "district": district,
      "commune": commune,
      "village": village
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Assign user control successfully'};
    }
    else {
      print(response.reasonPhrase);
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }

  Future<List<AddressAssign>> addressAfterAssign({
    required String controlOf
}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-address-when-assign'));
    request.body = json.encode({
      "code": controlOf
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<AddressAssign> addresses = [];

      var data = res['data'];
      if (data != null) {
        var addressData = data['address'];
        if (addressData != null) {
          addresses.add(AddressAssign.fromJson(addressData));
        }
      }
      // for(var i=0 ;i<addresses.length ; i++){
      //   var a=addresses[i];
      //   print("-------- provinceCode: ${a.provinceCode!.name}");
      //   print("-------- districtCode: ${a.districtCode!.name}");
      //   print("-------- communeCode: ${a.communeCode!.name}");
      //   print("-------- villageCode: ${a.villageCode!.name}");
      // }
      return addresses;
    }
    else {
      print(response.reasonPhrase);
      return [];
    }
  }

  Future<Map<String , dynamic>> updateMySelf({
    required String cardNumber,
    required String name,
    required String nameKh,
    required String email,
    required String identityNum,
    required String nationality,
    required String gender,
    required String dob,
    required String address,
    required String provinceCode,
    required String districtCode,
    required String communeCode,
    required String villageCode,
    required String province,
    required String district,
    required String commune,
    required String village,
  }) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-myself-info-user'));
      request.fields.addAll({
        'user_id': userInfo!.id.toString(),
        'card_number': cardNumber,
        'name': name,
        'name_khmer': nameKh,
        'email': email,
        'identity_no': identityNum,
        'nationality': nationality,
        'gender': gender,
        'dob': dob,
        'address': address,
        'province_code': provinceCode,
        'district_code': districtCode,
        'commune_code': communeCode,
        'village_code': villageCode,
        'province': province,
        'district': district,
        'commune': commune,
        'village': village
      });

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Assign user control successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Errors : ${response.reasonPhrase}'};
      }
    }catch(e){
      return {'success': false, 'message': 'Errors : $e'};
    }

  }

  Future<Map<String , dynamic>> updateUsers({
    required String userId,
    required String cardNumber,
    required String name,
    required String nameKh,
    required String email,
    required String identityNum,
    required String nationality,
    required String gender,
    required String dob,
    required String address,
    required String provinceCode,
    required String districtCode,
    required String communeCode,
    required String villageCode,
    required String province,
    required String district,
    required String commune,
    required String village,
    required List<XFile> myIdentity,
}) async {
    // print("in repo userId ${userId}");
    // print("in repo cardNumber ${cardNumber.toString()}");
    // print("in repo userName ${name}");
    // print("in repo nameKh ${nameKh}");
    // print("in repo email ${email}");
    // print("in repo identityNum ${identityNum}");
    // print("in repo nationality ${nationality}");
    // print("in repo gender ${gender}");
    // print("in repo dob ${dob}");
    // print("in repo address ${address}");
    // print("in repo provinceCode ${provinceCode} -- ${province}");
    // print("in repo districtCode ${districtCode} -- ${district}");
    // print("in repo communeCode ${communeCode} -- ${commune}");
    // print("in repo villageCode ${villageCode} -- ${village}");

    for(var a in myIdentity){
      print("imdID:  ${a.name}");
    }
    var headers = {
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-myself-info-user'));
    request.fields.addAll({
      'user_id': userId,
      'card_number': cardNumber,
      'name': name,
      'name_khmer': nameKh,
      'email': email,
      'identity_no': identityNum,
      'nationality': nationality,
      'gender': gender,
      'dob': dob,
      'address': address,
      'province_code': provinceCode,
      'district_code': districtCode,
      'commune_code': communeCode,
      'village_code': villageCode,
      'province': province,
      'district': district,
      'commune': commune,
      'village': village
    });

    for (var file in myIdentity) {
      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'myIdentity',
          await file.readAsBytes(),
          filename: file.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('myIdentity', file.path));
      }
    }
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Assign user control successfully'};
    }
    else {
      print(response.reasonPhrase);
      return {'success': false, 'message': 'Errors : ${response.reasonPhrase}'};
    }
  }

  Future<Map<String , dynamic>> updateImageProfile({
    required String userId,
    required XFile? imagePath
  }) async {
    try{
      var headers = {
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-profile-user'));
      request.fields.addAll({
        'user_id': userId
      });
      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
            'myProfile',
            await imagePath!.readAsBytes().then((value) {
              return value.cast();
            }),
            filename: imagePath.path.toString() + imagePath.name)
        );
      } else {
        request.files.add(await http.MultipartFile.fromPath('myProfile', imagePath!.path));
      }
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        return {'success': true, 'message': await response.stream.bytesToString()};
      }
      else {
        return {'success': false, 'message': 'Errors : ${response.reasonPhrase}'};
      }
    }catch(e){
      return {'success': false, 'message': 'Errors : $e'};
    }

  }




}

