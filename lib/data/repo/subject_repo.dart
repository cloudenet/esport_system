import 'dart:convert';

import 'package:esport_system/data/model/subject_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart' as http;

class SubjectRepo {
  Future<bool> createSubject({
    required String title,
    required String maxPoint,
    required String des, // Make sure this can be an empty string
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST', Uri.parse('${AppConstants.baseUrl}/api/v1/store-subjects'));
      request.body = json.encode({
        "title": title,
        "max_point": maxPoint,
        "description":
            des.isEmpty ? '' : des, // Ensure empty strings are handled
        "created_by": userInfo!.id.toString()
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      } else {
        print(response.reasonPhrase);
      }
      return false;
    } catch (e) {
      print("Error create subject in catch in repo $e");
      return false;
    }
  }

  Future<List<SubjectModel>> getListSubject(
      {String? search, int? limit, page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST',
          Uri.parse('${AppConstants.baseUrl}/api/v1/get-list-subjects'));
      request.body =
          json.encode({"search_term": search, "limit": limit, "page": page});
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> data = res['data']['subjects'];
        print("Date list Subject $data");
        return data.map((e) => SubjectModel.fromJson(e)).toList();
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (error) {
      print("Error get subject in catch in repo $error");
      return [];
    }
  }

  Future<bool> updateSubject({
    required String subId,
    required String title,
    required String maxPoint,
    required String des,
    required String updatedBy,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST', Uri.parse('${AppConstants.baseUrl}/api/v1/update-subject'));
      request.body = json.encode({
        "subject_id": subId,
        "title": title,
        "max_point": maxPoint,
        "description": des,
        "updated_by": updatedBy
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      } else {
        print(response.reasonPhrase);
      }
      return false;
    } catch (e) {
      print("Error update sub in catch in repo $e");
      return false;
    }
  }

  Future<bool> deleteSubject({
    required String subId,
    required String deleteBy,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST', Uri.parse('${AppConstants.baseUrl}/api/v1/delete-subjects'));
      request.body = json.encode({
        "subject_id": subId,
        "deleted_by": deleteBy,
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      } else {
        print(await response.stream.bytesToString());
      }
      return false;
    } catch (e) {
      print("Error update sub in catch in repo $e");
      return false;
    }
  }
}
