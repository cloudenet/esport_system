import 'dart:convert';
import 'package:esport_system/data/model/category_player.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';


class PlayerRepo {
  Future<Map<String, dynamic>> createPlayer({
    required String assignUserId,
    required String userId,
    required String teamId,
    required String positionId,
    required String height,
    required String weight,
    required String joinDate,
    required String jerseyNumber,
    required String previousTeam,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String goalsScored,
    required String assists,
    required String matchesPlayed,
    required String yellowCards,
    required String redCards,
    required String injuries,
  }) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.createPlayer}'));

    request.body = json.encode({
      "assign_user_id": assignUserId,
      "user_id": userId,
      "team_id": teamId,
      "position_id": positionId,
      "height": height,
      "weight": weight,
      "join_date": joinDate,
      "jersey_number": jerseyNumber,
      "previous_team": previousTeam,
      "contract_start_date": contractStartDate,
      "contract_end_date": contractEndDate,
      "salary": salary,
      "goals_scored": goalsScored,
      "assists": assists,
      "matches_played": matchesPlayed,
      "yellow_cards": yellowCards,
      "red_cards": redCards,
      "injuries": injuries
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Coach created successfully'};
    } else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }



  Future<Map<String, dynamic>> createPlayerV2({
    required String assignUserId,
    required String teamId,
    required String positionId,
    required String height,
    required String weight,
    required String joinDate,
    required String contractStartDate,
    required String contractEndDate,
    required String previousTeam,
    required String salary,
    required String? matchesPlayed,
    required String playerCategoryId,
    required XFile? bioPlayer,
}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/create-player'));
    // print("createPlayer: user_id : ${userInfo!.id.toString()}");
    // print("createPlayer: assign_user : $assignUserId");
    // print("createPlayer: teamId : $teamId");
    // print("createPlayer: positionId : $positionId");
    // print("createPlayer: height : $height");
    // print("createPlayer: weight : $weight");
    // print("createPlayer: joinDate : $joinDate");
    // print("createPlayer: contractStartDate : $contractStartDate");
    // print("createPlayer: contractEndDate : $contractEndDate");
    // print("createPlayer: jerseyNumber : $jerseyNumber");
    // print("createPlayer: previousTeam : $previousTeam");
    // print("createPlayer: salary : $salary");
    // print("createPlayer: matchesPlayed : $matchesPlayed");
    // print("createPlayer: playerCategoryId : $playerCategoryId");
    // print("createPlayer: bioPlayer : $bioPlayer");
    request.fields.addAll({
      'user_id': userInfo!.id.toString(),
      'assign_user_id': assignUserId,
      'team_id': teamId,
      'position_id': positionId,
      'height': height,
      'weight': weight,
      'join_date': joinDate,
      'contract_start_date': contractStartDate,
      'contract_end_date': contractEndDate,
      'jersey_number': "",
      'previous_team': previousTeam,
      'salary': salary,
      'matches_played': matchesPlayed ?? "",
      'player_category_id': playerCategoryId
    });
    //request.files.add(await http.MultipartFile.fromPath('biography_players', '/C:/List work place/CloudNet-Weekly(1) 30-04-Nove-2023.docx'));
    if (kIsWeb) {
      request.files.add(http.MultipartFile.fromBytes(
        'biography_players', await bioPlayer!.readAsBytes(),
        filename: bioPlayer.path + bioPlayer.name,
      ));
    } else {
      request.files.add(await http.MultipartFile.fromPath('biography_players', bioPlayer!.path));
    }
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Player created successfully'};
    }
    else {
      print("Failed during create player Repo ${response.reasonPhrase}");
      return {'success': false, 'message': response.reasonPhrase};
    }

  }




  Future<List<PlayerModel>> getPlayer({dynamic limit = 6, page,String? search}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getPlayer}'));
      request.body = json.encode({
            "user_id": userInfo!.id.toString(),
            "player_name": search,
            "limit": limit,
            "page": page
          });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> playerData = res['data']['player'];
        List<PlayerModel> getPlayerData = playerData.map((e) => PlayerModel.fromJson(e)).toList();
        return getPlayerData;
      } else {
        print(response.reasonPhrase);
      }
    } catch (e) {
      print('error $e');
    }
    return [];
  }
  Future<Map<String, dynamic>> updatePlayerV2({
    required String playerId,
    required String teamId,
    required String positoinId,
    required String height,
    required String weight,
    required String joinDate,
    required String previousTeam,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String? matchesPlay,
    required String playerCatId,
    required XFile? bioPlayer,
}) async {
    var headers = {
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/update-player'));
    request.fields.addAll({
      'player_id': playerId,
      'team_id': teamId,
      'position_id': positoinId,
      'height': height,
      'weight': weight,
      'join_date': joinDate,
      'jersey_number': "",
      'previous_team': previousTeam,
      'contract_start_date': contractStartDate,
      'contract_end_date': contractEndDate,
      'salary': salary,
      'matches_played': matchesPlay ?? "",
      'player_category_id': playerCatId
    });
    //request.files.add(await http.MultipartFile.fromPath('biography_players', '/path/to/file'));
    if (kIsWeb) {
      request.files.add(http.MultipartFile.fromBytes(
        'biography_players', await bioPlayer!.readAsBytes(),
        filename: bioPlayer.path + bioPlayer.name,
      ));
    } else {
      request.files.add(await http.MultipartFile.fromPath('biography_players', bioPlayer!.path));
    }

    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();
    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Player updated successfully'};
    }
    else {
      print(response.reasonPhrase);
      return {'success': false, 'message': '${response.reasonPhrase}'};
    }

  }

  Future<Map<String, dynamic>> updatePlayer({
    required String playerId,
    required String teamId,
    required String positoinId,
    required String height,
    required String weight,
    required String joinDate,
    required String jerseyNumber,
    required String previousTeam,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String goalScore,
    required String assists,
    required String matchesPlay,
    required String yellowCard,
    required String redCard,
    required String injeries,
  }) async {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request =
          http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.updatePlayer}'));
      request.body = json.encode({
        "player_id": playerId,
        "team_id": teamId,
        "position_id": positoinId,
        "height": height,
        "weight": weight,
        "join_date": joinDate,
        "jersey_number": jerseyNumber,
        "previous_team": previousTeam,
        "contract_start_date": contractStartDate,
        "contract_end_date": contractEndDate,
        "salary": salary,
        "goals_scored": goalScore,
        "assists": assists,
        "matches_played": matchesPlay,
        "yellow_cards": yellowCard,
        "red_cards": redCard,
        "injuries": injeries
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Player update successfully'};
      } else {
       String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
      }
  }

  Future<Map<String, dynamic>> deletePlayer({required String playerID}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.deletePlayer}'));
    request.body = json.encode({"player_id": playerID});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'Player delete successfully'};
    } else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }

  Future<List<CategoryPlayer>> getCategoryPlayer({String? searchTerm, int limit = 10, int page = 1}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-list-data-player-categories'));
    request.body = json.encode({
      "search_term": searchTerm,
      "limit": limit,
      "page": page
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<dynamic> list = res['data']['player_categories'];
      return list.map((e)=> CategoryPlayer.fromJson(e)).toList();
    }
    else {
      print(response.reasonPhrase);
      return [];
    }
  }
}
