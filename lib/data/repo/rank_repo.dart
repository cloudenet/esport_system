import 'dart:convert';

import 'package:esport_system/data/model/rank_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart' as http;

class RankRepo {
  Future<bool> createRank({
    required String type,
    required String title,
  }) async {
    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/create-data-ranks'));
    request.body = json.encode(
        {"type": type, "title_rank": title, "created_by": userInfo!.id});

    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return true;
    } else {
      print(response.reasonPhrase);
      return false;
    }
  }

  Future<List<RankModel>> getRank({String? search, int limit = 6, page}) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-data-list-ranks'));
      request.body = json.encode({
        "type": search,
        "limit": limit,
        "page": page
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> rankData = res['data']['ranks'];
        List<RankModel> getRankData = rankData.map((e) => RankModel.fromJson(e)).toList();
        return getRankData;
      } else {
        print(response.reasonPhrase);
        return [];
      }
    } catch (e) {
      print("---Error get rank $e");
      return [];
    }
  }

  Future<bool> updateRank({
    required String rankID,
    required String type,
    required String titleRank,
  }) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/update-data-ranks'));
    request.body = json.encode({
      "rank_id": rankID,
      "type": type,
      "title_rank": titleRank,
      "updated_by": userInfo!.id
    });
    request.headers.addAll(headers);
    try {
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      } else {
        print("Not found ${response.reasonPhrase}");
        return false;
      }
    } catch (e) {
      print("----Error update rank $e");
      return false;
    }

  }

  Future<bool> deleteRank({required String rankID}) async{
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/delete-data-ranks'));
    request.body = json.encode({
      "rank_id": rankID
    });
    request.headers.addAll(headers);

    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
    }
    else {
    print(response.reasonPhrase);
    return false;
    }
    }catch(e){
      print("Delete rank error $e");
      return false;
    }
  }
}
