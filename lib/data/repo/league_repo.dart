import 'dart:convert';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:esport_system/data/model/league_model.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart' as http;


class LeagueRepository {

  Future<void> createLeague({
    required String leagueName,
    required String createDate,
    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String district,
    required String commune,
    required String address,
    required String villages,
  }) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/create-league'));
    request.body = json.encode({
      "title": leagueName,
      "founded_date": createDate,
      "province_code": provinceCode,
      "district_code": districtCode,
      "village_code": villageCode,
      "commune_code": communeCode,
      "province": province,
      "district": district,
      "village": villages,
      "commune": commune,
      "address": address,
      "user_id": userInfo!.id.toString()
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
    } else {
      throw Exception(await response.stream.bytesToString());
    }
  }
  Future<void> updateLeague({
    required String leagueName,
    required String createDate,

    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String district,
    required String commune,
    required String leaugeId,
    required String villages,
  }) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-league'));
    request.body = json.encode({
      "title": leagueName,
      "founded_date": createDate,
      "province_code": provinceCode,
      "district_code": districtCode,
      "village_code": villageCode,
      "commune_code": communeCode,
      "province": province,
      "district": district,
      "village": villages,
      "commune": commune,
      "league_id": leaugeId
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();


    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
    } else {
      throw Exception(await response.stream.bytesToString());
    }
  }



  Future<List<LeagueModel>> getleague(
      {
        required String userId,
        required String search, page, int limit = 8}) async {
        var headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ${await globalGenerateToken()}'
        };

      // var request = http.Request( 'POST', Uri.parse('http://167.172.81.224:8287/api/get-league'));

      var request = http.Request( 'POST', Uri.parse('${AppConstants.baseUrl}/api/get-league'));
      request.body = json.encode({
        "user_id": userId,
        "league_id": "",
        "league_name": search,
        "page": page,
        "limit": limit
      });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<dynamic> getListLeague = res['data']['league'];
      print('==========Data : $getListLeague');
      List<LeagueModel> getLeagueList = getListLeague.map((json) => LeagueModel.fromJson(json)).toList();
      return getLeagueList;
    } else {
      print("ERROR ${response.reasonPhrase}");
      return [];
    }
    return [];
  }

  Future<List<Address>> getAddresses() async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request( 'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": ""});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      print("===================Address ${data}");
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      print("Something wrong fetch province");
      return [];
    }
  }
  Future<List<Address>> getDistricts(String provinceCode) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": provinceCode});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
    // Implementation for fetching districts based on provinceCode
  }
  Future<List<Address>> getCommunes(String districtCode) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": districtCode});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
    // Implementation for fetching communes based on districtCode
  }
  Future<List<Address>> getVillages(String villageCode) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request(
        'POST', Uri.parse('${AppConstants.baseUrl}/api/get-address'));
    request.body = json.encode({"code": villageCode});
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var responseBody = await response.stream.bytesToString();
      var data = json.decode(responseBody)['data']['addresses'] as List;
      return data.map((json) => Address.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load addresses');
    }
    // Implementation for fetching communes based on districtCode
  }
  Future<Map<String , dynamic>> deleteLeague({required String leagueID}) async{
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/delete-league'));
    request.body = json.encode({
      "league_id": leagueID
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return {'success': true, 'message': 'League delete successfully'};
    }
    else {
      String errorMessage = await response.stream.bytesToString();
      Map<String, dynamic> errorResponse = jsonDecode(errorMessage);
      String error = errorResponse['message'] ?? 'An error occurred';
      return {'success': false, 'message': error};
    }
  }


}