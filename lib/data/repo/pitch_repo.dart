
import 'dart:convert';
import 'package:esport_system/helper/route_helper.dart';
import 'package:http/http.dart' as http;
import '../../helper/app_contrain.dart';
import '../../helper/generate_token.dart';
import '../model/pitch_model.dart';
import '../model/team_model.dart';

class PitchRepo{

  Future<bool> requestPitch({
  required String? proId ,
  required String? teamId ,
  required String? amountPitch ,
  required String? name ,
  required String? phone ,
  required String? amountTable ,
  required String? location,
  required String? matchDate,
  required String? note,
}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.requestPitch}'));
    request.body = json.encode({
      "province": proId,
      "team_id": teamId,
      "num_of_pitches": amountPitch,//replace for amount club
      "requester_name": name,
      "requester_phone": phone,
      "num_of_tables": amountTable,// replace for place amount
      "location_pitches": location,
      "match_date": matchDate,
      "remark": note,
      "created_by": userInfo!.id
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      return true;
    }
    else {
      print(response.reasonPhrase);
      return false;
    }
  }

  Future<bool> updateRequestPitch({
    required String rqPitchID,
    required String provinceID,
    required String teamID,
    required String amountPitch,
    required String rqName,
    required String rqPhone,
    required String location,
    required String amountTable,
    required String matchDate,
    required String remark,
}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/update-pitch'));
    request.body = json.encode({
      "request_pitch_id": rqPitchID,
      "province": provinceID,
      "team_id": teamID,
      "requester_name": rqName,
      "requester_phone": rqPhone,
      "num_of_pitches": amountPitch,
      "num_of_tables": amountTable,
      "location_pitches": location,
      "match_date": matchDate,
      "remark": remark,
      "updated_by": userInfo!.id
    });
    request.headers.addAll(headers);
    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print("Not found ${response.reasonPhrase}");
        return false;
      }
    }catch(e){
      print("Errors in catch update$e");
      return false;
    }

  }


  Future<bool> approveRequestPitch({
  required String rqPitchID,
}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/approve-request-pitch'));
    request.body = json.encode({
      "request_pitch_id": rqPitchID,
      "user_id": userInfo!.id
    });
    request.headers.addAll(headers);
    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }
    }catch(e){
      print("Errors in catch approve $e");
      return false;
    }

  }


  Future<List<PitchModel>> getListRequestPitch({
    required String? searchTerm,
    required String? r_status,
    required int? limit,
    required int? page,
}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}${AppConstants.getRequestPitch}'));
    request.body = json.encode({
      "searchTerm": searchTerm,
      "r_status": r_status,
      "limit": limit,
      "page": page
    });
    request.headers.addAll(headers);
    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> list = res['data']['request_pitch'];
        List<PitchModel> listRequestPitch = list.map((e) => PitchModel.fromJson(e)).toList();
        return listRequestPitch;
      }
      else {
        print('Something wrong in repo: ${response.reasonPhrase}');
        return [];
      }
    }catch(e){
      print('Something wrong in catch:  $e');
      return [];
    }
  }

  Future<List<TeamModel>> getTeams() async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/get-team'));
    request.body = json.encode({
      "user_id": userInfo?.id
    });
    request.headers.addAll(headers);
    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var res = jsonDecode(await response.stream.bytesToString());
      List<dynamic> list = res['data']['team'];
      print("======Team$list");
      return list.map((e)=> TeamModel.fromJson(e)).toList();
    }
    else {
      print(response.reasonPhrase);
      return[];
    }
  }

  Future<bool> deleteRequest({required String rqId}) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${await globalGenerateToken()}'
    };
    var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/delete-request-pitch'));
    request.body = json.encode({
      "request_pitch_id": rqId,
    });
    request.headers.addAll(headers);
    try{
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      }
      else {
        print(response.reasonPhrase);
        return false;
      }
    }catch(e){
      print('Something wrong in repo: $e');
      return false;
    }
  }


}