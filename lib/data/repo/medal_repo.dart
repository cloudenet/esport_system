import 'dart:convert';
import 'package:esport_system/data/model/medal_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/generate_token.dart';
import 'package:http/http.dart' as http;

class MedalsPlayerRepo {
  Future<List<MedalModel>> getMedalPlayer({
    required String? playerCatId,
    required String? playerId,
}) async {
    try {
      var headers = {'Authorization': 'Bearer ${await globalGenerateToken()}'};
      var request = http.Request(
          'POST',
          Uri.parse(
              '${AppConstants.baseUrl}/api/get-list-medal-players?player_cate_id=$playerCatId&player_id=$playerId'));

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> medalPlayer = res['data']['player_medals'];
        print("========Medal Player Data$medalPlayer");
        List<MedalModel> medalPlayerData =
            medalPlayer.map((e) => MedalModel.fromJson(e)).toList();
        print("Medal Player Data length ${medalPlayerData.length}");
        return medalPlayerData;
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (e) {
      print("----Error get medal in repo $e");
      return [];
    }
  }

  Future<bool> createMedalPlayer({
    required String playerId,
    required String typeMedal,
    required String eventId,
    required String qtyMedal,
    required String dateWin,
    required String createdBy,
  }) async {
    try {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request(
          'POST',
          Uri.parse(
              '${AppConstants.baseUrl}/api/add-medal-to-players?player_id=$playerId'
              ''));
      request.body = json.encode({
        "type_medals": typeMedal,
        "event_id": eventId,
        "qty_medal": qtyMedal,
        "date_win": dateWin,
        "created_by": createdBy
      });
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return true;
      } else {
        print(response.reasonPhrase);
        return false;
      }
    } catch (e) {
      print("Error create medal in repo $e");
      return false;
    }
  }

  Future<List<MedalDetailModel>> getDetailMedal(
      {required String playerId}) async {
    try {
      var headers = {'Authorization': 'Bearer ${await globalGenerateToken()}'};
      var request = http.Request(
          'POST',
          Uri.parse(
              '${AppConstants.baseUrl}/api/get-detail-medal-player?player_id=$playerId'));

      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var a = jsonDecode(await response.stream.bytesToString());
        List<dynamic> medalDetailData = a['data']['player_medals'];
        print("Data === > $medalDetailData");
        return medalDetailData
            .map((e) => MedalDetailModel.fromJson(e))
            .toList();
      } else {
        print(response.reasonPhrase);
      }
      return [];
    } catch (ee) {
      print("Error get detail medal $ee");
      return [];
    }
  }
}
