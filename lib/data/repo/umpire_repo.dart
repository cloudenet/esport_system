

import 'dart:convert';

import 'package:esport_system/data/model/referees_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart'as http;
import 'package:image_picker/image_picker.dart';

import '../../helper/generate_token.dart';

class UmpiresRepo{

  Future<List<UmpiresModel>> getUmpires({String? search, int limit = 6, page}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/officials/get-list-umpires'));
      request.body = json.encode({
        "search_term": search,
        "limit": limit,
        "page": page
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        var res = jsonDecode(await response.stream.bytesToString());
        List<dynamic> data = res['data']['umpires'];
        print("Data Referees: $data");
        return data.map((v)=>UmpiresModel.fromJson(v)).toList();
      }
      else {
        print(response.reasonPhrase);
        return [];
      }
    }catch(e){
      print("issue repo : $e");
      return [];
    }

  }



  Future<Map<String,dynamic>> createUmpires({
    required String assignUserId,
    required String rankId,
    required String exYears,
    required String trainDate,
    required XFile? bioFile, // Single bio file
    required List<PlatformFile> certificateFiles,
  }) async {
    //
    // print("in repo cardNumber ${assignUserId.toString()}");
    // print("in repo userName ${rankId}");
    // print("in repo phone ${exYears}");
    // print("in repo email ${trainDate}");

    // if (bioFile != null) {
    //   print("File name: ${bioFile.name}");
    //   print("File path: ${bioFile.path}");
    // }
    // for (var file in certificateFiles) {
    //   debugPrint('Picked file Repo: ${file.name}');
    // }
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/officials/create-umpires'));
      request.fields.addAll({
        'user_id': userInfo!.id.toString(),
        'assign_user_id': assignUserId,
        'rank': rankId,
        'ex_year': exYears,
        'training_date': trainDate
      });

      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'bio_file',
          await bioFile!.readAsBytes(),
          filename: bioFile.path + bioFile.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('bio_file', bioFile!.path));
      }
      for (var file in certificateFiles) {
        request.files.add(http.MultipartFile.fromBytes(
          'certificate_file',
          file.bytes!,
          filename: file.name,
        ));
      }
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Umpires created successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Umpires created failed'};
      }
    }catch(e){
      print("Issue catch :$e");
      return {'success': false, 'message': 'Umpires created failed'};
    }
  }


  Future<Map<String,dynamic>> updateUmpires({
    required String umpireId,
    required String rankId,
    required String exYears,
    required String trainDate,
    required XFile? bioFile, // Single bio file
    required List<PlatformFile> certificateFiles,
}) async {
    // print("in repo umpireId ${umpireId.toString()}");
    // print("in repo rankId ${rankId}");
    // print("in repo exYears ${exYears}");
    // print("in repo trainDate ${trainDate}");
    // print("in repo bioFile ${bioFile!.path}");
    //
    // if (bioFile != null) {
    //   print("File name: ${bioFile.name}");
    //   print("File path: ${bioFile.path}");
    // }
    // for (var file in certificateFiles) {
    //   debugPrint('Picked file Repo: ${file.name}');
    // }
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.MultipartRequest('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/officials/update-umpires'));
      request.fields.addAll({
        'umpires_id': umpireId,
        'user_id': userInfo!.id.toString(),
        'rank': rankId,
        'ex_year': exYears,
        'training_date': trainDate
      });
      if (kIsWeb) {
        request.files.add(http.MultipartFile.fromBytes(
          'bio_file',
          await bioFile!.readAsBytes(),
          filename: bioFile.path + bioFile.name,
        ));
      } else {
        request.files.add(await http.MultipartFile.fromPath('bio_file', bioFile!.path));
      }

      for (var file in certificateFiles) {
        if (file.bytes != null) {
          request.files.add(http.MultipartFile.fromBytes(
            'certificate_file',
            file.bytes!,
            filename: file.name,
          ));
        } else {
          print("File bytes are null for ${file.name}----${file.bytes}");
        }
      }

      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Umpires updated successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Umpires updated failed'};
      }
    }catch(e){
      print("Issue catch :$e");
      return {'success': false, 'message': 'Umpires updated failed'};
    }
  }


  Future<Map<String,dynamic>> deleteUmpire({required String umpireId}) async {
    try{
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${await globalGenerateToken()}'
      };
      var request = http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/v1/delete-umpires'));
      request.body = json.encode({
        "umpires_id": umpireId
      });
      request.headers.addAll(headers);
      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
        return {'success': true, 'message': 'Umpires deleted successfully'};
      }
      else {
        print(response.reasonPhrase);
        return {'success': false, 'message': 'Umpires deleted failed ${response.reasonPhrase}'};
      }
    }catch(e){
      print("Issue catch delete Umpire:$e");
      return {'success': false, 'message': 'Umpires deleted failed'};
    }

  }


}