

class CategoryModel {
  String? id;
  String? title;
  String? otherTitle;
  String? fromAge;
  String? endAge;
  CreatedBy? createdBy;
  CreatedBy? updatedBy;
  String? createdAt;
  String? updatedAt;

  CategoryModel(
      {
        this.id,
        this.title,
        this.otherTitle,
        this.fromAge,
        this.endAge,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt});

  CategoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    otherTitle = json['other_title'].toString();
    fromAge = json['from_age'].toString();
    endAge = json['end_age'].toString();

    if (json['created_by'] != null &&
        json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    if (json['updated_by'] != null &&
        json['updated_by'] is Map<String, dynamic>) {
      updatedBy = CreatedBy.fromJson(json['updated_by']);
    }

    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['other_title'] = otherTitle;
    data['from_age'] = fromAge;
    data['end_age'] = endAge;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    if (updatedBy != null) {
      data['updated_by'] = updatedBy!.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}