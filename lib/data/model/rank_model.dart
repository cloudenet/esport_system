class RankModel {
  String? id;
  String? typeRank;
  String? titleRank;
  CreatedBy? createdBy;
  List<CreatedBy>? updatedBy;
  String? createdAt;
  String? updatedAt;

  RankModel(
      {this.id,
        this.typeRank,
        this.titleRank,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt});

  RankModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    typeRank = json['type_rank'].toString();
    titleRank = json['title_rank'].toString();
    createdBy = json['created_by'] != null
        ? CreatedBy.fromJson(json['created_by'])
        : null;
    // if (json['updated_by'] != null) {
    //   updatedBy = <CreatedBy>[];
    //   json['updated_by'].forEach((v) {
    //     updatedBy!.add(CreatedBy.fromJson(v));
    //   });
    // }
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type_rank'] = typeRank;
    data['title_rank'] = titleRank;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    // if (updatedBy != null) {
    //   data['updated_by'] = updatedBy!.map((v) => v.toJson()).toList();
    // }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}
