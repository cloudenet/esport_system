
import 'package:flutter/material.dart';

class CloudStorageInfo {
  final String? svgSrc, title, totalStorage;
  final int? numOfFiles, percentage;
  final Color? color;

  CloudStorageInfo({
    this.svgSrc,
    this.title,
    this.totalStorage,
    this.numOfFiles,
    this.percentage,
    this.color,
  });
}

// List demoMyFiles = [
//   CloudStorageInfo(
//     title: "Matches",
//     numOfFiles: 1328,
//     svgSrc: "assets/icons/avatar.png",
//     totalStorage: "100",
//     color: primaryColor,
//     percentage: 35,
//   ),
//   CloudStorageInfo(
//     title: "Events",
//     numOfFiles: 1328,
//     svgSrc: "assets/icons/calendar.png",
//     totalStorage: "303",
//     color: const Color(0xFFFFA113),
//     percentage: 35,
//   ),
//   CloudStorageInfo(
//     title: "User",
//     numOfFiles: 1328,
//     svgSrc: "assets/icons/group.png",
//     totalStorage: "400",
//     color: const Color(0xFFA4CDFF),
//     percentage: 10,
//   ),
//   CloudStorageInfo(
//     title: "Players",
//     numOfFiles: 5328,
//     svgSrc: "assets/icons/user.png",
//     totalStorage: "200",
//     color: const Color(0xFF007EE5),
//     percentage: 78,
//   ),
// ];
