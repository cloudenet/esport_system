import 'package:esport_system/data/model/referees_model.dart';

class ScorekeeperModel {
  String? username;
  String? name;
  String? id;
  String? officialType;
  UserId? userId;
  RankId? rankId;
  String? exYear;
  String? trainingDate;
  String? bioFile;
  List<String>? certificate;
  CreatedBy? createdBy;
  List<dynamic>? updatedBy; // Changed to List<dynamic> or specify a type
  String? createdAt;
  String? updatedAt;

  ScorekeeperModel({
    this.username,
    this.name,
    this.id,
    this.officialType,
    this.userId,
    this.rankId,
    this.exYear,
    this.trainingDate,
    this.bioFile,
    this.certificate,
    this.createdBy,
    this.updatedBy,
    this.createdAt,
    this.updatedAt,
  });

  ScorekeeperModel.fromJson(Map<String, dynamic> json) {
    username = json['username'].toString();
    name = json['name'].toString();
    id = json['id'].toString();
    officialType = json['official_type'].toString();
    if (json['user_id'] != null &&
        json['user_id'] is Map<String, dynamic>) {
      userId = UserId.fromJson(json['user_id']);
    }
    if (json['rank_id'] != null &&
        json['rank_id'] is Map<String, dynamic>) {
      rankId = RankId.fromJson(json['rank_id']);
    }
    exYear = json['ex_year'].toString();
    trainingDate = json['training_date'].toString();
    bioFile = json['bio_file'];
    certificate = json['certificate'].cast<String>();
    if (json['created_by'] != null &&
        json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    if (json['updated_by'] != null &&
        json['updated_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['updated_by']);
    }
    // Adjusted to List<dynamic>
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['username'] = username;
    data['name'] = name;
    data['id'] = id;
    data['official_type'] = officialType;
    if (userId != null) {
      data['user_id'] = userId!.toJson();
    }

    if (rankId != null) {
      data['rank_id'] = rankId!.toJson();
    }

    data['ex_year'] = exYear;
    data['training_date'] = trainingDate;
    data['bio_file'] = bioFile;
    data['certificate'] = certificate;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }

    if (updatedBy != null) {
      data['updated_by'] = updatedBy; // List<dynamic> does not need further transformation
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class UserId {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;
  String? gender;
  String? dob;
  String? photo;

  UserId({
    this.id,
    this.cardNumber,
    this.name,
    this.username,
    this.phone,
    this.email,
    this.gender,
    this.dob,
    this.photo,
  });

  UserId.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
    gender = json['gender'].toString();
    dob = json['dob'].toString();
    photo = json['photo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    data['gender'] = gender;
    data['dob'] = dob;
    data['photo'] = photo;
    return data;
  }
}



class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy({
    this.id,
    this.cardNumber,
    this.name,
    this.username,
    this.phone,
    this.email,
  });

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}
