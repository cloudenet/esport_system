class LeagueModel {
  String? id;
  String? leagueName;
  String? foundedDate;
  String? logo;
  String? stadium;
  String? address;
  String? province;
  String? district;
  String? commune;
  String? village;
  String? provinceCode;
  String? districtCode;
  String? communeCode;
  String? villageCode;
  String? championshipsWon;
  String? parentId;
  String? createdBy;
  String? createdAt;
  String? updatedAt;

  LeagueModel(
      {this.id,
        this.leagueName,
        this.foundedDate,
        this.stadium,
        this.logo,
        this.address,
        this.province,
        this.district,
        this.commune,
        this.village,
        this.provinceCode,
        this.districtCode,
        this.communeCode,
        this.villageCode,
        this.championshipsWon,
        this.parentId,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
      //  this.parentLeague
  }
  );

  LeagueModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    leagueName = json['team_name'].toString();
    foundedDate = json['founded_date'].toString();
    stadium = json['stadium'].toString();
    logo = json['logo'].toString();
    address = json['address'].toString();
    province = json['province'].toString();
    district = json['district'].toString().toString();
    commune = json['commune'];
    village = json['village'].toString();
    provinceCode = json['province_code'].toString();
    districtCode = json['district_code'].toString();
    communeCode = json['commune_code'].toString();
    villageCode = json['village_code'].toString();
    championshipsWon = json['championships_won'].toString();
    parentId = json['parent_id'].toString();
    createdBy = json['created_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    // if (json['parent_league'] != null) {
    //   parentLeague = <Null>[];
    //   json['parent_league'].forEach((v) {
    //     parentLeague!.add(new Null.fromJson(v));
    //   });
    // }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = leagueName;
    data['founded_date'] = foundedDate;
    data['stadium'] = stadium;
    data['logo'] = logo;
    data['address'] = address;
    data['province'] = province;
    data['district'] = district;
    data['commune'] = commune;
    data['village'] = village;
    data['province_code'] = provinceCode;
    data['district_code'] = districtCode;
    data['commune_code'] = communeCode;
    data['village_code'] = villageCode;
    data['championships_won'] = championshipsWon;
    data['parent_id'] = parentId;
    data['created_by'] = createdBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    // if (this.parentLeague != null) {
    //   data['parent_league'] =
    //       this.parentLeague!.map((v) => v.toJson()).toList();
    // }
    return data;
  }
}
