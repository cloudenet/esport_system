

class AddressAssign {
  ProvinceCode? provinceCode;
  ProvinceCode? districtCode;
  ProvinceCode? communeCode;
  ProvinceCode? villageCode;
  String? level;

  AddressAssign(
      {this.provinceCode,
        this.districtCode,
        this.communeCode,
        this.villageCode,
        this.level});

  AddressAssign.fromJson(Map<String, dynamic> json) {

    if (json['province_code'] != null &&
        json['province_code'] is Map<String, dynamic>) {
      provinceCode = ProvinceCode.fromJson(json['province_code']);
    }
    if (json['district_code'] != null &&
        json['district_code'] is Map<String, dynamic>) {
      districtCode = ProvinceCode.fromJson(json['district_code']);
    }
    if (json['commune_code'] != null &&
        json['commune_code'] is Map<String, dynamic>) {
      communeCode = ProvinceCode.fromJson(json['commune_code']);
    }
    if (json['village_code'] != null &&
        json['village_code'] is Map<String, dynamic>) {
      villageCode = ProvinceCode.fromJson(json['village_code']);
    }
    level = json['level'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (provinceCode != null) {
      data['province_code'] = provinceCode!.toJson();
    }

    if (districtCode != null) {
      data['district_code'] = districtCode!.toJson();
    }
    if (communeCode != null) {
      data['commune_code'] = communeCode!.toJson();
    }
    if (villageCode != null) {
      data['village_code'] = villageCode!.toJson();
    }
    data['level'] = level;
    return data;
  }
}

class ProvinceCode {
  String? id;
  String? code;
  String? name;
  String? description;
  String? type;
  String? parentCode;
  String? reference;
  String? officialNote;
  String? noteByChecker;

  ProvinceCode(
      {this.id,
        this.code,
        this.name,
        this.description,
        this.type,
        this.parentCode,
        this.reference,
        this.officialNote,
        this.noteByChecker});

  ProvinceCode.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    code = json['code'].toString();
    name = json['name'].toString();
    description = json['description'].toString();
    type = json['type'].toString();
    parentCode = json['parent_code'].toString();
    reference = json['reference'].toString();
    officialNote = json['official_note'].toString();
    noteByChecker = json['note by_checker'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['name'] = name;
    data['description'] = description;
    data['type'] = type;
    data['parent_code'] = parentCode;
    data['reference'] = reference;
    data['official_note'] = officialNote;
    data['note by_checker'] = noteByChecker;
    return data;
  }
}