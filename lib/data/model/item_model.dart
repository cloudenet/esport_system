import 'package:esport_system/data/model/arbitrator_model.dart';

class ItemModel {
  String? id;
  String? itemTitle;
  String? otherTitle;
  String? isStatus;
  CreatedBy? createdBy;
  UpdatedBy? updatedBy;
  String? createdAt;
  String? updatedAt;
  String? stockQty;
  bool? isInCart;

  ItemModel({
    this.id,
    this.itemTitle,
    this.otherTitle,
    this.isStatus,
    this.createdBy,
    this.updatedBy,
    this.createdAt,
    this.updatedAt,
    this.stockQty,
    this.isInCart, // Initialize new field
  });

  ItemModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    itemTitle = json['item_title'].toString();
    otherTitle = json['other_title'].toString();
    isStatus = json['is_status'].toString();
    createdBy = json['created_by'] != null
        ? CreatedBy.fromJson(json['created_by'])
        : null;
    if (json['updated_by'] != null &&
        json['updated_by'] is Map<String, dynamic>) {
      updatedBy = UpdatedBy.fromJson(json['updated_by']);
    }
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    stockQty = json['stock_qty'].toString();
    isInCart = json['is_in_cart'] != null ? json['is_in_cart'] as bool? : null; // Handle new field
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['item_title'] = itemTitle;
    data['other_title'] = otherTitle;
    data['is_status'] = isStatus;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    if (updatedBy != null) {
      data['updated_by'] = updatedBy!.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['stock_qty'] = stockQty;
    data['is_in_cart'] = isInCart; // Include new field
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy({
    this.id,
    this.cardNumber,
    this.name,
    this.username,
    this.phone,
    this.email,
  });

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}


class ItemsHistory {
  String? id;
  String? itemId;
  String? trainType;
  String? qtyItems;
  String? trainDate;
  String? createdBy;
  String? noted;
  String? createdAt;

  ItemsHistory(
      {this.id,
        this.itemId,
        this.trainType,
        this.qtyItems,
        this.trainDate,
        this.createdBy,
        this.noted,
        this.createdAt});

  ItemsHistory.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    itemId = json['item_id'].toString();
    trainType = json['train_type'].toString();
    qtyItems = json['qty_items'].toString();
    trainDate = json['train_date'].toString();
    createdBy = json['created_by'].toString();
    noted = json['noted'].toString();
    createdAt = json['created_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['item_id'] = itemId;
    data['train_type'] = trainType;
    data['qty_items'] =qtyItems;
    data['train_date'] = trainDate;
    data['created_by'] = createdBy;
    data['noted'] = noted;
    data['created_at'] = createdAt;
    return data;
  }
}