class EventModel {
  String? id;
  String? title;
  String? address;
  String? description;
  String? startDate;
  String? endDate;
  String? createdBy;
  String? updatedBy;
  String? createdAt;
  String? updatedAt;

  EventModel(
      {this.id,
        this.title,
        this.address,
        this.description,
        this.startDate,
        this.endDate,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt});

  EventModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    address = json['address'].toString();
    description = json['description'].toString();
    startDate = json['start_date'].toString();
    endDate = json['end_date'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['address'] = address;
    data['description'] = description;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
