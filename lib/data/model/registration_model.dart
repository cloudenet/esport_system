



class RegistrationMatchesModel {
  String? id;
  String? registrationDate;
  CreatedBy? createdBy;
  String? description;
  String? matchType;
  String? venue;
  CreatedBy? approvedBy;
  String? approvedDate;
  String? approvedNote;
  String? registrationStatus;
  String? createdAt;
  String? updatedAt;
  Team? team;
  Event? event;
  List<MatchDetails>? matchDetails;

  RegistrationMatchesModel(
      {this.id,
        this.registrationDate,
        this.createdBy,
        this.description,
        this.matchType,
        this.venue,
        this.approvedBy,
        this.approvedDate,
        this.approvedNote,
        this.registrationStatus,
        this.createdAt,
        this.updatedAt,
        this.team,
        this.event,
        this.matchDetails});

  RegistrationMatchesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    registrationDate = json['registration_date'].toString();
    if (json['created_by'] != null && json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    description = json['description'].toString();
    matchType = json['match_type'].toString();
    venue = json['venue'].toString();
    if (json['approved_by'] != null && json['approved_by'] is Map<String, dynamic>) {
      approvedBy = CreatedBy.fromJson(json['approved_by']);
    }
    approvedDate = json['approved_date'].toString();
    approvedNote = json['approved_note'].toString();
    registrationStatus = json['registration_status'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    if (json['team'] != null && json['team'] is Map<String, dynamic>) {
      team = Team.fromJson(json['team']);
    }
    if (json['event'] != null && json['event'] is Map<String, dynamic>) {
      event = Event.fromJson(json['event']);
    }
    if (json['match_details'] != null) {
      matchDetails = <MatchDetails>[];
      json['match_details'].forEach((v) {
        matchDetails!.add(MatchDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['registration_date'] = registrationDate;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    data['description'] = description;
    data['match_type'] = matchType;
    data['venue'] = venue;
    if (approvedBy != null) {
      data['approved_by'] = approvedBy!.toJson();
    }
    data['approved_date'] = approvedDate;
    data['approved_note'] = approvedNote;
    data['registration_status'] = registrationStatus;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (team != null) {
      data['team'] = team!.toJson();
    }
    if (event != null) {
      data['event'] = event!.toJson();
    }
    if (matchDetails != null) {
      data['match_details'] = matchDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? nameKhmer;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.nameKhmer,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    nameKhmer = json['name_khmer'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['name_khmer'] = nameKhmer;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}

class Team {
  String? id;
  String? teamName;
  String? address;
  String? province;

  Team({this.id, this.teamName, this.address, this.province});

  Team.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    address = json['address'].toString();
    province = json['province'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['address'] = address;
    data['province'] = province;
    return data;
  }
}

class Event {
  String? id;
  String? title;
  String? address;
  String? eventType;

  Event({this.id, this.title, this.address, this.eventType});

  Event.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    address = json['address'].toString();
    eventType = json['event_type'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['address'] = address;
    data['event_type'] = eventType;
    return data;
  }
}

class MatchDetails {
  String? id;
  String? registrationId;
  String? description;
  String? createdAt;
  String? updatedAt;
  Player? player;
  Subject? subject;

  MatchDetails(
      {this.id,
        this.registrationId,
        this.description,
        this.createdAt,
        this.updatedAt,
        this.player,
        this.subject});

  MatchDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    registrationId = json['registration_id'].toString();
    description = json['description'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    if (json['player'] != null && json['player'] is Map<String, dynamic>) {
      player = Player.fromJson(json['player']);
    }
    if (json['subject'] != null && json['subject'] is Map<String, dynamic>) {
      subject = Subject.fromJson(json['subject']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['registration_id'] = registrationId;
    data['description'] = description;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (player != null) {
      data['player'] = player!.toJson();
    }
    if (subject != null) {
      data['subject'] = subject!.toJson();
    }
    return data;
  }
}

class Player {
  String? id;
  String? userId;
  String? playerUsername;
  String? playerName;
  String? playerNameKh;

  Player(
      {this.id,
        this.userId,
        this.playerUsername,
        this.playerName,
        this.playerNameKh});

  Player.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    userId = json['user_id'].toString();
    playerUsername = json['player_username'].toString();
    playerName = json['player_name'].toString();
    playerNameKh = json['player_name_kh'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['player_username'] = playerUsername;
    data['player_name'] = playerName;
    data['player_name_kh'] = playerNameKh;
    return data;
  }
}

class Subject {
  String? id;
  String? title;
  String? maxPoint;
  String? description;
  String? createdBy;
  String? updatedBy;
  String? createdAt;
  String? updatedAt;

  Subject(
      {this.id,
        this.title,
        this.maxPoint,
        this.description,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt});

  Subject.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    maxPoint = json['max_point'].toString();
    description = json['description'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['max_point'] = maxPoint;
    data['description'] = description;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

