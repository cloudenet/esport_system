


class PitchModel {
  String? id;
  String? location;
  String? teamId;
  String? amountClub;
  String? requesterName;
  String? requesterPhone;
  String? numOfPitches;
  String? numOfTables;
  String? locationPitches;
  String? requestDate;
  String? placeAmount;
  String? place;
  String? remark;
  String? rStatus;
  String? createdBy;
  String? approvedBy;
  String? updatedBy;
  String? createdAt;
  String? updatedAt;
  Province? province;
  Team? team;
  UserApproved? userApproved;
  UserCreated? userCreated;
  UserUpdated? userUpdated;

  PitchModel(
      {this.id,
        this.location,
        this.teamId,
        this.amountClub,
        this.requesterName,
        this.requesterPhone,
        this.numOfPitches,
        this.numOfTables,
        this.locationPitches,
        this.requestDate,
        this.placeAmount,
        this.place,
        this.remark,
        this.rStatus,
        this.createdBy,
        this.approvedBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.province,
        this.team,
        this.userApproved,
        this.userCreated,
        this.userUpdated});

  PitchModel.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toString() ?? '';
    location = json['location']?.toString() ?? '';
    teamId = json['team_id']?.toString() ?? '';
    amountClub = json['amount_club']?.toString() ?? '';
    requesterName = json['requester_name']?.toString() ?? '';
    requesterPhone = json['requester_phone']?.toString() ?? '';
    numOfPitches = json['num_of_pitches']?.toString() ?? '';
    numOfTables = json['num_of_tables']?.toString() ?? '';
    locationPitches = json['location_pitches']?.toString() ?? '';
    requestDate = json['request_date']?.toString() ?? '';
    placeAmount = json['place_amount']?.toString() ?? '';
    place = json['place']?.toString() ?? '';
    remark = json['remark']?.toString() ?? '';
    rStatus = json['r_status']?.toString() ?? '';
    createdBy = json['created_by']?.toString() ?? '';
    approvedBy = json['approved_by']?.toString() ?? '';
    updatedBy = json['updated_by']?.toString() ?? '';
    createdAt = json['created_at']?.toString() ?? '';
    updatedAt = json['updated_at']?.toString() ?? '';

    if (json['province'] != null &&
        json['province'] is Map<String, dynamic>) {
      province = Province.fromJson(json['province']);
    }
    if (json['team'] != null &&
        json['team'] is Map<String, dynamic>) {
      team = Team.fromJson(json['team']);
    }
    if (json['user_approved'] != null &&
        json['user_approved'] is Map<String, dynamic>) {
      userApproved = UserApproved.fromJson(json['user_approved']);
    }
    if (json['user_created'] != null &&
        json['user_created'] is Map<String, dynamic>) {
      userCreated = UserCreated.fromJson(json['user_created']);
    }

    if (json['user_updated'] != null &&
        json['user_updated'] is Map<String, dynamic>) {
      userUpdated = UserUpdated.fromJson(json['user_updated']);
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['location'] = location;
    data['team_id'] = teamId;
    data['amount_club'] = amountClub;
    data['requester_name'] = requesterName;
    data['requester_phone'] = requesterPhone;
    data['num_of_pitches'] = numOfPitches;
    data['num_of_tables'] = numOfTables;
    data['location_pitches'] = locationPitches;
    data['request_date'] = requestDate;
    data['place_amount'] = placeAmount;
    data['place'] = place;
    data['remark'] = remark;
    data['r_status'] = rStatus;
    data['created_by'] = createdBy;
    data['approved_by'] = approvedBy;
    data['updated_by'] = updatedBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (province != null) {
      data['province'] = province!.toJson();
    }
    if (team != null) {
      data['team'] = team!.toJson();
    }
    if (userApproved != null) {
      data['user_approved'] = userApproved!.toJson();
    }
    if (userCreated != null) {
      data['user_created'] = userCreated!.toJson();
    }
    if (userUpdated != null) {
      data['user_updated'] = userUpdated!.toJson();
    }
    return data;
  }
}

class Province {
  String? code;
  String? name;
  String? parentCode;

  Province({this.code, this.name, this.parentCode});

  Province.fromJson(Map<String, dynamic> json) {
    code = json['code'].toString();
    name = json['name'].toString();
    parentCode = json['parent_code'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['name'] = name;
    data['parent_code'] = parentCode;
    return data;
  }
}

class Team {
  String? id;
  String? teamName;
  String? logo;
  String? address;

  Team({this.id, this.teamName, this.logo, this.address});

  Team.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    logo = json['logo'].toString();
    address = json['address'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['logo'] = logo;
    data['address'] = address;
    return data;
  }
}

class UserApproved {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? photo;

  UserApproved(
      {this.id, this.cardNumber, this.name, this.username, this.photo});

  UserApproved.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    photo = json['photo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['photo'] = photo;
    return data;
  }
}

class UserUpdated {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? photo;

  UserUpdated(
      {this.id, this.cardNumber, this.name, this.username, this.photo});

  UserUpdated.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    photo = json['photo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['photo'] = photo;
    return data;
  }
}

class UserCreated {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? photo;

  UserCreated(
      {this.id, this.cardNumber, this.name, this.username, this.photo});

  UserCreated.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    photo = json['photo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['photo'] = photo;
    return data;
  }
}