class MatchesModel {
  String? id;
  String? eventId;
  String? homeTeamId;
  String? awayTeamId;
  String? homeTeamScore;
  String? awayTeamScore;
  String? winningTeamId;
  String? losingTeamId;
  String? startTime;
  String? endTime;
  String? matchStatus;
  String? createdBy;
  String? updatedBy;
  String? createdAt;
  String? updatedAt;
  Event? event;
  HomeTeam? homeTeam;
  HomeTeam? awayTeam;
  List<String>? winningTeam;
  List<String>? losingTeam;

  MatchesModel(
      {
        this.id,
        this.eventId,
        this.homeTeamId,
        this.awayTeamId,
        this.homeTeamScore,
        this.awayTeamScore,
        this.winningTeamId,
        this.losingTeamId,
        this.startTime,
        this.endTime,
        this.matchStatus,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.event,
        this.homeTeam,
        this.awayTeam,
        this.winningTeam,
        this.losingTeam});

  MatchesModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    eventId = json['event_id'].toString();
    homeTeamId = json['home_team_id'].toString();
    awayTeamId = json['away_team_id'].toString();
    homeTeamScore = json['home_team_score'].toString();
    awayTeamScore = json['away_team_score'].toString();
    winningTeamId = json['winning_team_id'].toString();
    losingTeamId = json['losing_team_id'].toString();
    startTime = json['start_time'].toString();
    endTime = json['end_time'].toString();
    matchStatus = json['match_status'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    event = json['event'] != null ?  Event.fromJson(json['event']) : null;
    homeTeam = json['home_team'] != null
        ?  HomeTeam.fromJson(json['home_team'])
        : null;
    awayTeam = json['away_team'] != null
        ?  HomeTeam.fromJson(json['away_team'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id.toString();
    data['event_id'] = eventId.toString();
    data['home_team_id'] = homeTeamId.toString();
    data['away_team_id'] = awayTeamId.toString();
    data['home_team_score'] = homeTeamScore.toString();
    data['away_team_score'] = awayTeamScore.toString();
    data['winning_team_id'] = winningTeamId.toString();
    data['losing_team_id'] = losingTeamId.toString();
    data['start_time'] = startTime.toString();
    data['end_time'] = endTime.toString();
    data['match_status'] = matchStatus.toString();
    data['created_by'] = createdBy.toString();
    data['updated_by'] = updatedBy.toString();
    data['created_at'] = createdAt.toString();
    data['updated_at'] = updatedAt.toString();
    if (event != null) {
      data['event'] = event!.toJson();
    }
    if (homeTeam != null) {
      data['home_team'] = homeTeam!.toJson();
    }
    if (awayTeam != null) {
      data['away_team'] = awayTeam!.toJson();
    }
    return data;
  }
}
class Event {
  String? id;
  String? title;
  String? address;
  String? description;
  String? startDate;
  String? endDate;

  Event(
      {this.id,
        this.title,
        this.address,
        this.description,
        this.startDate,
        this.endDate});

  Event.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    address = json['address'].toString();
    description = json['description'].toString();
    startDate = json['start_date'].toString();
    endDate = json['end_date'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['id'] = id.toString();
    data['title'] = title.toString();
    data['address'] = address.toString();
    data['description'] = description.toString();
    data['start_date'] = startDate.toString();
    data['end_date'] = endDate.toString();
    return data;
  }
}

class HomeTeam {
  String? id;
  String? teamName;
  String? stadium;
  String? logo;
  String? address;

  HomeTeam({this.id, this.teamName, this.stadium, this.logo, this.address});

  HomeTeam.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    stadium = json['stadium'].toString();
    logo = json['logo'].toString();
    address = json['address'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  Map<String, dynamic>();
    data['id'] = id.toString();
    data['team_name'] = teamName.toString();
    data['stadium'] = stadium.toString();
    data['logo'] = logo.toString();
    data['address'] = address.toString();
    return data;
  }
}
