class PlayerModel {
  String? username;
  String? photo;
  String? name;
  String? name_khmer;
  String? teamName;
  String? typePlayer;
  String? otherTypePlayer;
  String? id;
  String? userId;
  String? playerCateId;
  String? teamId;
  String? positionId;
  String? height;
  String? weight;
  String? joinDate;
  String? jerseyNumber;
  String? previousTeam;
  String? contractStartDate;
  String? contractEndDate;
  String? salary;
  String? fileBioPlayer;
  String? goalsScored;
  String? assists;
  String? matchesPlayed;
  String? yellowCards;
  String? redCards;
  String? injuries;
  String? createdBy;
  Position? position;

  PlayerModel(
      {this.username,
      this.photo,
      this.name,
      this.name_khmer,
      this.teamName,
      this.typePlayer,
      this.otherTypePlayer,
      this.id,
      this.userId,
      this.playerCateId,
      this.teamId,
      this.positionId,
      this.height,
      this.weight,
      this.joinDate,
      this.jerseyNumber,
      this.previousTeam,
      this.contractStartDate,
      this.contractEndDate,
      this.salary,
      this.fileBioPlayer,
      this.goalsScored,
      this.assists,
      this.matchesPlayed,
      this.yellowCards,
      this.redCards,
      this.injuries,
      this.createdBy,
      this.position});

  PlayerModel.fromJson(Map<String, dynamic> json) {
    username = json['username'].toString();
    photo = json['photo'].toString();
    name = json['name'].toString();
    name_khmer = json['name_khmer'].toString();
    teamName = json['team_name'].toString();
    typePlayer = json['type_player'].toString();
    otherTypePlayer = json['other_type_player'].toString();
    id = json['id'].toString();
    userId = json['user_id'].toString();
    playerCateId = json['player_cate_id'].toString();
    teamId = json['team_id'].toString();
    positionId = json['position_id'].toString();
    height = json['height'].toString();
    weight = json['weight'].toString();
    joinDate = json['join_date'].toString();
    jerseyNumber = json['jersey_number'].toString();
    previousTeam = json['previous_team'].toString();
    contractStartDate = json['contract_start_date'].toString();
    contractEndDate = json['contract_end_date'].toString();
    salary = json['salary'].toString();
    fileBioPlayer = json['file_bio'].toString();
    goalsScored = json['goals_scored'].toString();
    assists = json['assists'].toString();
    matchesPlayed = json['matches_played'].toString();
    yellowCards = json['yellow_cards'].toString();
    redCards = json['red_cards'].toString();
    injuries = json['injuries'].toString();
    createdBy = json['created_by'].toString();
    position = json['position'] != null
        ? Position.fromJson(json['position'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['username'] = username;
    data['photo'] = photo;
    data['name'] = name;
    data['name_khmer'] = name_khmer;
    data['team_name'] = teamName;
    data['id'] = id;
    data['user_id'] = userId;
    data['team_id'] = teamId;
    data['position_id'] = positionId;
    data['height'] = height;
    data['weight'] = weight;
    data['join_date'] = joinDate;
    data['jersey_number'] = jerseyNumber;
    data['previous_team'] = previousTeam;
    data['contract_start_date'] = contractStartDate;
    data['contract_end_date'] = contractEndDate;
    data['salary'] = salary;
    data['file_bio'] = fileBioPlayer;
    data['goals_scored'] = goalsScored;
    data['assists'] = assists;
    data['matches_played'] = matchesPlayed;
    data['yellow_cards'] = yellowCards;
    data['red_cards'] = redCards;
    data['injuries'] = injuries;
    data['created_by'] = createdBy;
    if (position != null) {
      data['position'] = position!.toJson();
    }
    return data;
  }
}

class Position {
  String? id;
  String? title;
  String? description;

  Position({this.id, this.title, this.description});

  Position.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    description = json['description'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}
