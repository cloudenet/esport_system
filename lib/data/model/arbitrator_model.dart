import 'dart:core';

class ArbitratorModel {
  String? username;
  String? name;
  String? teamName;
  String? id;
  UserId? userId;
  TeamId? teamId;
  RankModel? rankModel;
  String? exYear;
  String? trainingDate;
  CreatedBy? createdBy;
  UpdatedBy? updatedBy;
  String? createdAt;
  String? updatedAt;
  String? bioFile;
  String? officialType;
  List<String>? certificate;

  ArbitratorModel(
      {this.username,
        this.name,
        this.teamName,
        this.id,
        this.userId,
        this.teamId,
        this.rankModel,
        this.exYear,
        this.trainingDate,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.bioFile,
        this.officialType,
        this.certificate,
      });

  ArbitratorModel.fromJson(Map<String, dynamic> json) {
    username = json['username'].toString();
    name = json['name'].toString();
    teamName = json['team_name'].toString();
    id = json['id'].toString();
    userId = json['user_id'] != null ? UserId.fromJson(json['user_id']) : null;
    teamId = json['team_id'] != null ? TeamId.fromJson(json['team_id']) : null;
    if (json['rank_id'] != null &&
        json['rank_id'] is Map<String, dynamic>) {
      rankModel = RankModel.fromJson(json['rank_id']);
    }
    exYear = json['ex_year'].toString();
    trainingDate = json['training_date'].toString();
    createdBy = json['created_by'] != null ? CreatedBy.fromJson(json['created_by']) : null;
    if (json['updated_by'] != null &&
        json['updated_by'] is Map<String, dynamic>) {
      updatedBy = UpdatedBy.fromJson(json['updated_by']);
    }
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    bioFile = json['bio_file'];
    officialType = json['official_type'];
    certificate = json['certificate'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['username'] = username;
    data['name'] = name;
    data['team_name'] = teamName;
    data['id'] = id;
    if (userId != null) {
      data['user_id'] = userId!.toJson();
    }
    if (teamId != null) {
      data['team_id'] = teamId!.toJson();
    }
    if (rankModel != null) {
      data['rank_id'] = rankModel!.toJson();
    }
    data['ex_year'] = exYear;
    data['training_date'] = trainingDate;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    if (updatedBy != null) {
      data['updated_by'] = updatedBy!.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['bio_file'] = bioFile;
    data['official_type'] = officialType;
    data['certificate'] = certificate;
    return data;
  }
}

class UserId {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;
  String? gender;
  String? dob;
  String? photo;

  UserId(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email,
        this.gender,
        this.dob,
        this.photo});

  UserId.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
    gender = json['gender'].toString();
    dob = json['dob'].toString();
    photo = json['photo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    data['gender'] = gender;
    data['dob'] = dob;
    data['photo'] = photo;
    return data;
  }
}

class TeamId {
  String? id;
  String? teamName;
  String? logo;

  TeamId({this.id, this.teamName, this.logo});

  TeamId.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    logo = json['logo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['logo'] = logo;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }

}

class RankModel {
  String? id;
  String? typeRank;
  String? titleRank;

  RankModel({this.id, this.typeRank, this.titleRank});

  RankModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    typeRank = json['type_rank'].toString();
    titleRank = json['title_rank'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type_rank'] = typeRank;
    data['title_rank'] = titleRank;
    return data;
  }
}
class UpdatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  UpdatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  UpdatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}

