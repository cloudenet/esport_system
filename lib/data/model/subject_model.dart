import 'arbitrator_model.dart';

class SubjectModel {
  String? id;
  String? title;
  String? maxPoint;
  String? description;
  CreatedBy? createdBy;
  UpdatedBy? updatedBy;
  String? createdAt;
  String? updatedAt;

  SubjectModel({
    this.id,
    this.title,
    this.maxPoint,
    this.description,
    this.createdBy,
    this.updatedBy,
    this.createdAt,
    this.updatedAt,
  });

  SubjectModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    maxPoint = json['max_point'].toString();
    description = json['description'].toString();
    if (json['created_by'] != null &&
        json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    if (json['updated_by'] != null &&
        json['updated_by'] is Map<String, dynamic>) {
      updatedBy = UpdatedBy.fromJson(json['updated_by']);
    }
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['max_point'] = maxPoint;
    data['description'] = description;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    if (updatedBy != null) {
      data['updated_by'] = updatedBy!.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy({
    this.id,
    this.cardNumber,
    this.name,
    this.username,
    this.phone,
    this.email,
  });

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}

