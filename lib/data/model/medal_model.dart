class MedalModel {
  String? id;
  String? parent;
  String? title;
  String? otherTitle;
  String? createdBy;
  String? updatedBy;
  String? createdAt;
  String? updatedAt;
  List<Players>? players;

  MedalModel(
      {this.id,
        this.parent,
        this.title,
        this.otherTitle,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt,
        this.players});

  MedalModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    parent = json['parent'].toString();
    title = json['title'].toString();
    otherTitle = json['other_title'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    if (json['players'] != null) {
      players = <Players>[];
      json['players'].forEach((v) {
        players!.add(Players.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['parent'] = parent;
    data['title'] = title;
    data['other_title'] = otherTitle;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (players != null) {
      data['players'] = players!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Players {
  String? id;
  String? playerCateId;
  String? positionId;
  String? playerUsername;
  String? playerPhoto;
  String? playerName;
  String? playerNameKh;
  Medals? medals;

  Players(
      {this.id,
        this.playerCateId,
        this.positionId,
        this.playerUsername,
        this.playerPhoto,
        this.playerName,
        this.playerNameKh,
        this.medals});

  Players.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    playerCateId = json['player_cate_id'].toString();
    positionId = json['position_id'].toString();
    playerUsername = json['player_username'].toString();
    playerPhoto = json['player_photo'].toString();
    playerName = json['player_name'].toString();
    playerNameKh = json['player_name_kh'].toString();
    medals =
    json['medals'] != null ? Medals.fromJson(json['medals']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['player_cate_id'] = playerCateId;
    data['position_id'] = positionId;
    data['player_username'] = playerUsername;
    data['player_photo'] = playerPhoto;
    data['player_name'] = playerName;
    data['player_name_kh'] = playerNameKh;
    if (medals != null) {
      data['medals'] = medals!.toJson();
    }
    return data;
  }
}

class Medals {
  String? goldMedals;
  String? silverMedals;
  String? bronzeMedals;
  String? totalMedals;

  Medals(
      {this.goldMedals,
        this.silverMedals,
        this.bronzeMedals,
        this.totalMedals});

  Medals.fromJson(Map<String, dynamic> json) {
    goldMedals = json['gold_medals'].toString();
    silverMedals = json['silver_medals'].toString();
    bronzeMedals = json['bronze_medals'].toString();
    totalMedals = json['total_medals'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['gold_medals'] = goldMedals;
    data['silver_medals'] = silverMedals;
    data['bronze_medals'] = bronzeMedals;
    data['total_medals'] = totalMedals;
    return data;
  }
}

class MedalDetailModel {
  String? id;
  String? typeMedals;
  String? qtyMedal;
  String? dateWin;
  CreatedBy? createdBy;
  String? createdAt;
  String? updatedAt;
  Player? player;
  Event? event;

  MedalDetailModel(
      {this.id,
        this.typeMedals,
        this.qtyMedal,
        this.dateWin,
        this.createdBy,
        this.createdAt,
        this.updatedAt,
        this.player,
        this.event});

  MedalDetailModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    typeMedals = json['type_medals'].toString();
    qtyMedal = json['qty_medal'].toString();
    dateWin = json['date_win'].toString();
    if (json['created_by'] != null &&
        json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    if (json['player'] != null &&
        json['player'] is Map<String, dynamic>) {
      player = Player.fromJson(json['player']);
    }
    if (json['event'] != null &&
        json['event'] is Map<String, dynamic>) {
      event = Event.fromJson(json['event']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type_medals'] = typeMedals;
    data['qty_medal'] = qtyMedal;
    data['date_win'] = dateWin;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (player != null) {
      data['player'] = player!.toJson();
    }
    if (event != null) {
      data['event'] = event!.toJson();
    }
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}

class Player {
  String? id;
  String? playerUsername;
  String? playerName;
  String? playerNameKh;
  String? teamName;
  String? typePlayer;
  String? otherTypePlayer;

  Player(
      {this.id,
        this.playerUsername,
        this.playerName,
        this.playerNameKh,
        this.teamName,
        this.typePlayer,
        this.otherTypePlayer});

  Player.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    playerUsername = json['player_username'].toString();
    playerName = json['player_name'].toString();
    playerNameKh = json['player_name_kh'].toString();
    teamName = json['team_name'].toString();
    typePlayer = json['type_player'].toString();
    otherTypePlayer = json['other_type_player'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['player_username'] = playerUsername;
    data['player_name'] = playerName;
    data['player_name_kh'] = playerNameKh;
    data['team_name'] = teamName;
    data['type_player'] = typePlayer;
    data['other_type_player'] = otherTypePlayer;
    return data;
  }
}

class Event {
  String? id;
  String? title;
  String? address;
  String? description;
  String? startDate;
  String? endDate;

  Event(
      {this.id,
        this.title,
        this.address,
        this.description,
        this.startDate,
        this.endDate});

  Event.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    address = json['address'].toString();
    description = json['description'].toString();
    startDate = json['start_date'].toString();
    endDate = json['end_date'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['address'] = address;
    data['description'] = description;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    return data;
  }
}

