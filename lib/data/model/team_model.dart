class TeamModel {
  String? id;
  String? teamName;
  String? stadium;
  String? logo;
  String? address;
  String? province;
  String? district;
  String? commune;
  String? village;
  String? provinceCode;
  String? districtCode;
  String? communeCode;
  String? villageCode;
  String? championshipsWon;
  String? createdBy;
  String? createdAt;
  String? updatedAt;
  ParentTeam? parentTeam;

  TeamModel(
      {this.id,
      this.teamName,
      this.stadium,
      this.logo,
      this.address,
      this.province,
      this.district,
      this.commune,
      this.village,
      this.provinceCode,
      this.districtCode,
      this.communeCode,
      this.villageCode,
      this.championshipsWon,
      this.createdBy,
      this.createdAt,
      this.updatedAt,
      this.parentTeam});

  TeamModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    stadium = json['stadium'].toString();
    logo = json['logo'].toString();
    address = json['address'].toString();
    province = json['province'].toString();
    district = json['district'].toString();
    commune = json['commune'].toString();
    village = json['village'].toString();
    provinceCode = json['province_code'].toString();
    districtCode = json['district_code'].toString();
    communeCode = json['commune_code'].toString();
    villageCode = json['village_code'].toString();
    championshipsWon = json['championships_won'].toString();
    createdBy = json['created_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    if (json['parent_team'] != null &&
        json['parent_team'] is Map<String, dynamic>) {
      parentTeam = ParentTeam.fromJson(json['parent_team']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['stadium'] = stadium;
    data['logo'] = logo;
    data['address'] = address;
    data['province'] = province;
    data['district'] = district;
    data['commune'] = commune;
    data['village'] = village;
    data['province_code'] = provinceCode;
    data['district_code'] = districtCode;
    data['commune_code'] = communeCode;
    data['village_code'] = villageCode;
    data['championships_won'] = championshipsWon;
    data['created_by'] = createdBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class ParentTeam {
  String? id;
  String? teamName;
  String? foundedDate;
  String? stadium;
  String? logo;
  String? address;

  ParentTeam(
      {this.id,
      this.teamName,
      this.foundedDate,
      this.stadium,
      this.logo,
      this.address});

  ParentTeam.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    foundedDate = json['founded_date'].toString();
    stadium = json['stadium'].toString();
    logo = json['logo'].toString();
    address = json['address'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['founded_date'] = foundedDate;
    data['stadium'] = stadium;
    data['logo'] = logo;
    data['address'] = address;
    return data;
  }
}
