

class items_history {
  String? id;
  String? itemId;
  String? trainType;
  String? qtyItems;
  String? trainDate;
  String? createdBy;
  String? noted;
  String? createdAt;

  items_history(
      {this.id,
        this.itemId,
        this.trainType,
        this.qtyItems,
        this.trainDate,
        this.createdBy,
        this.noted,
        this.createdAt});

  items_history.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    itemId = json['item_id'].toString();
    trainType = json['train_type'].toString();
    qtyItems = json['qty_items'].toString();
    trainDate = json['train_date'].toString();
    createdBy = json['created_by'].toString();
    noted = json['noted'].toString();
    createdAt = json['created_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['item_id'] = itemId;
    data['train_type'] = trainType;
    data['qty_items'] =qtyItems;
    data['train_date'] = trainDate;
    data['created_by'] = createdBy;
    data['noted'] = noted;
    data['created_at'] = createdAt;
    return data;
  }
}


class ItemRequestModel {
  String? id;
  String? refNumber;
  Location? location;
  Team? team;
  String? isStatus;
  String? reqRef;
  String? note;
  String? approvedStatus;
  String? approvedDate;
  CreatedBy? approvedBy;
  CreatedBy? createdBy;
  String? updatedBy;
  String? isReceived;
  String? createdAt;
  String? updatedAt;
  List<ItemDetails>? itemDetails;

  ItemRequestModel(
      {this.id,
        this.refNumber,
        this.location,
        this.team,
        this.isStatus,
        this.reqRef,
        this.note,
        this.approvedStatus,
        this.approvedDate,
        this.approvedBy,
        this.createdBy,
        this.updatedBy,
        this.isReceived,
        this.createdAt,
        this.updatedAt,
        this.itemDetails});

  ItemRequestModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    refNumber = json['ref_number'].toString();

    if (json['location'] != null &&
        json['location'] is Map<String, dynamic>) {
      location = Location.fromJson(json['location']);
    }

    if (json['team'] != null &&
        json['team'] is Map<String, dynamic>) {
      team = Team.fromJson(json['team']);
    }

    isStatus = json['is_status'].toString();
    reqRef = json['req_ref'].toString();
    note = json['note'].toString();
    approvedStatus = json['approved_status'].toString();
    approvedDate = json['approved_date'].toString();

    if (json['approved_by'] != null &&
        json['approved_by'] is Map<String, dynamic>) {
      approvedBy = CreatedBy.fromJson(json['approved_by']);
    }
    if (json['created_by'] != null &&
        json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    updatedBy = json['updated_by'].toString();
    isReceived = json['is_received'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();

    if (json['item_details'] != null) {
      itemDetails = <ItemDetails>[];
      json['item_details'].forEach((v) {
        itemDetails!.add(ItemDetails.fromJson(v));
      });
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['ref_number'] = refNumber;

    if (location != null) {
      data['location'] = location!.toJson();
    }
    if (team != null) {
      data['team'] = team!.toJson();
    }
    data['is_status'] = isStatus;
    data['req_ref'] = reqRef;
    data['note'] = note;
    data['approved_status'] = approvedStatus;
    data['approved_date'] = approvedDate;
    if (approvedBy != null) {
      data['approved_by'] = approvedBy!.toJson();
    }
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    data['updated_by'] = updatedBy;
    data['is_received'] = isReceived;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    if (itemDetails != null) {
      data['item_details'] = itemDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Team {
  String? id;
  String? teamName;
  String? foundedDate;
  String? stadium;
  String? logo;

  Team({this.id, this.teamName, this.foundedDate, this.stadium, this.logo});

  Team.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString().toString();
    foundedDate = json['founded_date'].toString();
    stadium = json['stadium'].toString();
    logo = json['logo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['founded_date'] = foundedDate;
    data['stadium'] = stadium;
    data['logo'] = logo;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}

class ItemDetails {
  String? id;
  String? requestId;
  Item? item;
  String? qty;
  String? qtyRemaining;
  String? qtyReceived;
  String? note;
  String? createdAt;
  String? updatedAt;

  ItemDetails(
      {this.id,
        this.requestId,
        this.item,
        this.qty,
        this.note,
        this.qtyRemaining,
        this.qtyReceived,
        this.createdAt,
        this.updatedAt});

  ItemDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    requestId = json['request_id'].toString();

    if (json['item'] != null &&
        json['item'] is Map<String, dynamic>) {
      item = Item.fromJson(json['item']);
    }

    qty = json['qty'].toString();
    qtyRemaining = json['qty_remaining'].toString();
    qtyReceived = json['qty_received'].toString();
    note = json['note'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['request_id'] = requestId;
    if (item != null) {
      data['item'] = item!.toJson();
    }
    data['qty'] = qty;
    data['qty_remaining'] = qtyRemaining;
    data['qty_received'] = qtyReceived;
    data['note'] = note;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class Item {
  String? id;
  String? itemTitle;
  String? otherTitle;

  Item({this.id, this.itemTitle, this.otherTitle});

  Item.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    itemTitle = json['item_title'].toString();
    otherTitle = json['other_title'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['item_title'] = itemTitle;
    data['other_title'] = otherTitle;
    return data;
  }
}
class Location {
  String? id;
  String? code;
  String? name;
  String? description;

  Location({this.id, this.code, this.name, this.description});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    code = json['code'].toString();
    name = json['name'].toString();
    description = json['description'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['name'] = name;
    data['description'] = description;
    return data;
  }
}
