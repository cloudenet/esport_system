


class Monthly {
  String? id;
  String? playerCateId;
  String? playerId;
  String? mMonth;
  String? mYear;
  String? exDate;
  String? totalPoint;
  String? ratePoint;
  String? grade;
  String? createdBy;
  String? updatedBy;
  String? createdAt;
  String? updatedAt;

  Monthly(
      {this.id,
        this.playerCateId,
        this.playerId,
        this.mMonth,
        this.mYear,
        this.exDate,
        this.totalPoint,
        this.ratePoint,
        this.grade,
        this.createdBy,
        this.updatedBy,
        this.createdAt,
        this.updatedAt});

  Monthly.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    playerCateId = json['player_cate_id'].toString();
    playerId = json['player_id'].toString();
    mMonth = json['m_month'].toString();
    mYear = json['m_year'].toString();
    exDate = json['ex_date'].toString();
    totalPoint = json['total_point'].toString();
    ratePoint = json['rate_point'].toString();
    grade = json['grade'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['player_cate_id'] = playerCateId;
    data['player_id'] = playerId;
    data['m_month'] = mMonth;
    data['m_year'] = mYear;
    data['ex_date'] = exDate;
    data['total_point'] = totalPoint;
    data['rate_point'] = ratePoint;
    data['grade'] = grade;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class Results {
  List<Players>? players;
  List<Subjects>? subjects;

  Results({this.players, this.subjects});

  Results.fromJson(Map<String, dynamic> json) {
    if (json['players'] != null) {
      players = <Players>[];
      json['players'].forEach((v) {
        players!.add(Players.fromJson(v));
      });
    }
    if (json['subjects'] != null) {
      subjects = <Subjects>[];
      json['subjects'].forEach((v) {
        subjects!.add(Subjects.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (players != null) {
      data['players'] = players!.map((v) => v.toJson()).toList();
    }
    if (subjects != null) {
      data['subjects'] = subjects!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Players {
  String? id;
  String? playerCateId;
  String? name;
  Subjects? subjects;

  Players({this.id, this.playerCateId, this.name, this.subjects});

  Players.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    playerCateId = json['player_cate_id'].toString();
    name = json['name'].toString();
    if(json['subjects'] != null && json['subjects'] is Map<String, dynamic>){
      subjects = Subjects.fromJson(json['subjects']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['player_cate_id'] = playerCateId;
    data['name'] = name;
    if (subjects != null) {
      data['subjects'] = subjects!.toJson();
    }
    return data;
  }
}

class Subjects {
  String? nODEJS;
  String? fLUTTER;
  String? c;
  String? jAVA;
  String? rEACTJS;

  Subjects({this.nODEJS, this.fLUTTER, this.c, this.jAVA, this.rEACTJS});

  Subjects.fromJson(Map<String, dynamic> json) {
    nODEJS = json['NODE JS'];
    fLUTTER = json['FLUTTER'];
    c = json['C++'];
    jAVA = json['JAVA'];
    rEACTJS = json['REACT JS'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['NODE JS'] = this.nODEJS;
    data['FLUTTER'] = this.fLUTTER;
    data['C++'] = this.c;
    data['JAVA'] = this.jAVA;
    data['REACT JS'] = this.rEACTJS;
    return data;
  }
}

class Subject {
  String? id;
  String? title;
  String? maxPoint;
  String? description;

  Subject({this.id, this.title, this.maxPoint, this.description});

  Subject.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    maxPoint = json['max_point'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['max_point'] = this.maxPoint;
    data['description'] = this.description;
    return data;
  }
}