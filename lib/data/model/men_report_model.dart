class MenReportModel {
  final int index;
  final String name;
  final double round1;
  final double round2;
  final double round3;
  final double round4;
  final double totalScore;
  final int rank;

  MenReportModel({
    required this.index,
    required this.name,
    required this.round1,
    required this.round2,
    required this.round3,
    required this.round4,
    required this.totalScore,
    required this.rank,
  });
}
