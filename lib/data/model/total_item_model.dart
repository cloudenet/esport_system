class TotalItem {
  String? totalPlayers;
  String? totalCoaches;
  String? totalMatches;
  String? totalEvents;

  TotalItem(
      {this.totalPlayers,
      this.totalCoaches,
      this.totalMatches,
      this.totalEvents});

  TotalItem.fromJson(Map<String, dynamic> json) {
    totalPlayers = json['total_players'].toString();
    totalCoaches = json['total_coaches'].toString();
    totalMatches = json['total_matches'].toString();
    totalEvents = json['total_events'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['total_players'] = totalPlayers;
    data['total_coaches'] = totalCoaches;
    data['total_matches'] = totalMatches;
    data['total_events'] = totalEvents;
    return data;
  }
}
