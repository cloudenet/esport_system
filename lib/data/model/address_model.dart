class Address {
  final String? id;
  final String? code;
   String? name;
  final String? description;
  final String? type;
  final String? parentCode;
  final String? reference;
  final String officialNote;
  final String noteByChecker;

  Address({
     this.id,
     this.code,
     this.name,
     this.description,
     this.type,
     this.parentCode,
     this.reference,
    this.officialNote = "",
    this.noteByChecker = "",
  });

  // Named constructor to create an Address object from a JSON map
  Address.fromJson(Map<String, dynamic> json)
      : id = json['id'].toString(),
        code = json['code'].toString(),
        name = json['name'].toString(),
        description = json['description'].toString(),
        type = json['type'].toString(),
        parentCode = json['parent_code'].toString(),
        reference = json['reference'].toString(),
        officialNote = json['official_note'].toString(),
        noteByChecker = json['note by_checker'].toString();

  // Method to convert an Address object to a JSON map
  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'code': code,
      'name': name,
      'description': description,
      'type': type,
      'parent_code': parentCode,
      'reference': reference,
      'official_note': officialNote,
      'note by_checker': noteByChecker,
    };
  }
}
