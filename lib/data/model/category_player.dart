

class CategoryPlayer {
  String? id;
  String? title;
  String? otherTitle;
  CreatedBy? createdBy;
  String? createdAt;
  String? updatedAt;

  CategoryPlayer(
      {this.id,
        this.title,
        this.otherTitle,
        this.createdBy,
        this.createdAt,
        this.updatedAt});

  CategoryPlayer.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    title = json['title'].toString();
    otherTitle = json['other_title'].toString();
    if (json['created_by'] != null &&
        json['created_by'] is Map<String, dynamic>) {
      createdBy = CreatedBy.fromJson(json['created_by']);
    }
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['other_title'] = this.otherTitle;
    if (createdBy != null) {
      data['created_by'] = createdBy!.toJson();
    }
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}

class CreatedBy {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? phone;
  String? email;

  CreatedBy(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.phone,
        this.email});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}
