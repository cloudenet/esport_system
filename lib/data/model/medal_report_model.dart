class FetchMedalReportModel {
  final int serialNumber;
  final String event;
  final int goldMedals;
  final int silverMedals;
  final int bronzeMedals;
  final int totalMedals;

  FetchMedalReportModel({
    required this.serialNumber,
    required this.event,
    required this.goldMedals,
    required this.silverMedals,
    required this.bronzeMedals,
    required this.totalMedals,
  });
}
