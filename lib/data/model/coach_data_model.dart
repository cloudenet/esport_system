class CoachDataModel {
  String? id;
  String? code;
  String? coacheType;
  String? experienceYears;
  String? specialty;
  String? hireDate;
  String? joinDate;
  String? contractStartDate;
  String? contractEndDate;
  String? salary;
  String? championshipsWon;
  String? matchesWon;
  String? matchesLost;
  String? matchesDrawn;
  String? biographyFile;
  String? biography;
  String? type;
  String? username;
  User? user;
  Rank? rank;
  Team? team;

  CoachDataModel(
      {this.id,
        this.code,
        this.coacheType,
        this.experienceYears,
        this.specialty,
        this.hireDate,
        this.joinDate,
        this.contractStartDate,
        this.contractEndDate,
        this.salary,
        this.championshipsWon,
        this.matchesWon,
        this.matchesLost,
        this.matchesDrawn,
        this.biographyFile,
        this.biography,
        this.type,
        this.username,
        this.user,
        this.rank,
        this.team});

  CoachDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    code = json['code'].toString();
    coacheType = json['coache_type'].toString();
    experienceYears = json['experience_years'].toString();
    specialty = json['specialty'].toString();
    hireDate = json['hire_date'].toString();
    joinDate = json['join_date'].toString();
    contractStartDate = json['contract_start_date'].toString();
    contractEndDate = json['contract_end_date'].toString();
    salary = json['salary'].toString();
    championshipsWon = json['championships_won'].toString();
    matchesWon = json['matches_won'].toString();
    matchesLost = json['matches_lost'].toString();
    matchesDrawn = json['matches_drawn'].toString();
    biographyFile = json['biography_file'].toString();
    biography = json['biography'].toString();
    type = json['type'].toString();
    username = json['username'].toString();

    if (json['user'] != null &&
        json['user'] is Map<String, dynamic>) {
      user = User.fromJson(json['user']);
    }
    if (json['rank'] != null &&
        json['rank'] is Map<String, dynamic>) {
      rank = Rank.fromJson(json['rank']);
    }
    if (json['team'] != null &&
        json['team'] is Map<String, dynamic>) {
      team = Team.fromJson(json['team']);
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['coache_type'] = coacheType;
    data['experience_years'] = experienceYears;
    data['specialty'] = specialty;
    data['hire_date'] = hireDate;
    data['join_date'] = joinDate;
    data['contract_start_date'] = contractStartDate;
    data['contract_end_date'] = contractEndDate;
    data['salary'] = salary;
    data['championships_won'] = championshipsWon;
    data['matches_won'] = matchesWon;
    data['matches_lost'] = matchesLost;
    data['matches_drawn'] = matchesDrawn;
    data['biography_file'] = biographyFile;
    data['biography'] = biography;
    data['type'] = type;
    data['username'] = username;
    if (user != null) {
      data['user'] = user!.toJson();
    }
    if (rank != null) {
      data['rank'] = rank!.toJson();
    }
    if (team != null) {
      data['team'] = team!.toJson();
    }
    return data;
  }
}

class User {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? nameKhmer;
  String? phone;
  String? email;

  User(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.nameKhmer,
        this.phone,
        this.email});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    nameKhmer = json['name_khmer'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['name_khmer'] = nameKhmer;
    data['phone'] = phone;
    data['email'] = email;
    return data;
  }
}

class Rank {
  String? id;
  String? titleRank;

  Rank({this.id, this.titleRank});

  Rank.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    titleRank = json['title_rank'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title_rank'] = titleRank;
    return data;
  }
}

class Team {
  String? id;
  String? teamName;
  String? foundedDate;
  String? logo;
  String? address;

  Team({this.id, this.teamName, this.foundedDate, this.logo, this.address});

  Team.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    teamName = json['team_name'].toString();
    foundedDate = json['founded_date'].toString();
    logo = json['logo'].toString();
    address = json['address'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['team_name'] = teamName;
    data['founded_date'] = foundedDate;
    data['logo'] = logo;
    data['address'] = address;
    return data;
  }
}
