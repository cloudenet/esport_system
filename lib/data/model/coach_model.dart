class CoachModel {
  String? username;
  String? name_khmer;
  String? photo;
  String? name;
  String? teamName;
  String? id;
  String? userId;
  String? rankId;
  String? teamId;
  String? experienceYears;
  String? specialty;
  String? hireDate;
  String? contractStartDate;
  String? contractEndDate;
  String? salary;
  String? championshipsWon;
  String? matchesWon;
  String? matchesLost;
  String? matchesDrawn;
  String? biographyFile;
  String? biography; // Changed from Null? to String?
  String? createdBy;
  Rank? rank;

  CoachModel({
    this.username,
    this.name_khmer,
    this.photo,
    this.name,
    this.teamName,
    this.id,
    this.userId,
    this.rankId,
    this.teamId,
    this.experienceYears,
    this.specialty,
    this.hireDate,
    this.contractStartDate,
    this.contractEndDate,
    this.salary,
    this.championshipsWon,
    this.matchesWon,
    this.matchesLost,
    this.matchesDrawn,
    this.biographyFile,
    this.biography,
    this.createdBy,
    this.rank,
  });

  CoachModel.fromJson(Map<String, dynamic> json) {
    username = json['username'].toString();
    name_khmer = json['name_khmer'].toString();
    photo = json['photo'].toString();
    name = json['name'].toString();
    teamName = json['team_name'].toString();
    id = json['id'].toString();
    userId = json['user_id'].toString();
    rankId = json['rank_id'].toString();
    teamId = json['team_id'].toString();
    experienceYears = json['experience_years'].toString();
    specialty = json['specialty'].toString();
    hireDate = json['hire_date'].toString();
    contractStartDate = json['contract_start_date'].toString();
    contractEndDate = json['contract_end_date'].toString();
    salary = json['salary'].toString();
    championshipsWon = json['championships_won'].toString();
    matchesWon = json['matches_won'].toString();
    matchesLost = json['matches_lost'].toString();
    matchesDrawn = json['matches_drawn'].toString();
    biographyFile = json['biography_file'].toString();
    biography = json['biography'].toString();
    createdBy = json['created_by'].toString();
    if (json['rank'] != null &&
        json['rank'] is Map<String, dynamic>) {
      rank = Rank.fromJson(json['rank']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['username'] = username;
    data['name_khmer'] = name_khmer;
    data['photo'] = photo;
    data['name'] = name;
    data['team_name'] = teamName;
    data['id'] = id;
    data['user_id'] = userId;
    data['rank_id'] = rankId;
    data['team_id'] = teamId;
    data['experience_years'] = experienceYears;
    data['specialty'] = specialty;
    data['hire_date'] = hireDate;
    data['contract_start_date'] = contractStartDate;
    data['contract_end_date'] = contractEndDate;
    data['salary'] = salary;
    data['championships_won'] = championshipsWon;
    data['matches_won'] = matchesWon;
    data['matches_lost'] = matchesLost;
    data['matches_drawn'] = matchesDrawn;
    data['biography_file'] = biographyFile;
    data['biography'] = biography;
    data['created_by'] = createdBy;
    if (rank != null) {
      data['rank'] = rank!.toJson();
    }
    return data;
  }
}

class Rank {
  String? id;
  String? typeRank;
  String? titleRank;

  Rank({this.id, this.typeRank, this.titleRank});

  Rank.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    typeRank = json['type_rank'].toString();
    titleRank = json['title_rank'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['type_rank'] = typeRank;
    data['title_rank'] = titleRank;
    return data;
  }
}




