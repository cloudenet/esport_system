class PlayerInTeamModel {
  String? username;
  String? photo;
  String? name;
  String? teamName;
  String? id;
  String? userId;
  String? teamId;
  String? positionId;
  String? height;
  String? weight;
  String? joinDate;
  String? jerseyNumber;
  String? previousTeam;
  String? contractStartDate;
  String? contractEndDate;
  String? salary;
  String? goalsScored;
  String? assists;
  String? matchesPlayed;
  String? yellowCards;
  String? redCards;
  String? injuries;
  String? createdBy;
  Position? position;

  PlayerInTeamModel(
      {this.username,
        this.photo,
        this.name,
        this.teamName,
        this.id,
        this.userId,
        this.teamId,
        this.positionId,
        this.height,
        this.weight,
        this.joinDate,
        this.jerseyNumber,
        this.previousTeam,
        this.contractStartDate,
        this.contractEndDate,
        this.salary,
        this.goalsScored,
        this.assists,
        this.matchesPlayed,
        this.yellowCards,
        this.redCards,
        this.injuries,
        this.createdBy,
        this.position});
  PlayerInTeamModel.fromJson(Map<String, dynamic> json) {
    username = json['username']?.toString();
    photo = json['photo']?.toString();
    name = json['name'].toString();
    teamName = json['team_name'].toString();
    id = json['id']?.toString();
    userId = json['user_id']?.toString();
    teamId = json['team_id']?.toString();
    positionId = json['position_id']?.toString();
    height = json['height']?.toString();
    weight = json['weight']?.toString();
    joinDate = json['join_date'];
    jerseyNumber = json['jersey_number']?.toString();
    previousTeam = json['previous_team'];
    contractStartDate = json['contract_start_date'];
    contractEndDate = json['contract_end_date'];
    salary = json['salary']?.toString();
    goalsScored = json['goals_scored']?.toString();
    assists = json['assists']?.toString();
    matchesPlayed = json['matches_played']?.toString();
    yellowCards = json['yellow_cards']?.toString();
    redCards = json['red_cards']?.toString();
    injuries = json['injuries'];
    createdBy = json['created_by']?.toString();
    position = json['position'] != null ? Position.fromJson(json['position']) : null;
  }


  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['username'] = username;
    data['photo'] = photo;
    data['name'] = name;
    data['team_name'] = teamName;
    data['id'] = id;
    data['user_id'] = userId;
    data['team_id'] = teamId;
    data['position_id'] = positionId;
    data['height'] = height;
    data['weight'] = weight;
    data['join_date'] = joinDate;
    data['jersey_number'] = jerseyNumber;
    data['previous_team'] = previousTeam;
    data['contract_start_date'] = contractStartDate;
    data['contract_end_date'] = contractEndDate;
    data['salary'] = salary;
    data['goals_scored'] = goalsScored;
    data['assists'] = assists;
    data['matches_played'] = matchesPlayed;
    data['yellow_cards'] = yellowCards;
    data['red_cards'] = redCards;
    data['injuries'] = injuries;
    data['created_by'] = createdBy;
    if (position != null) {
      data['position'] = position!.toJson();
    }
    return data;
  }
}

class Position {
  int? id;
  String? title;
  String? description;

  Position({this.id, this.title, this.description});

  Position.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    return data;
  }
}
