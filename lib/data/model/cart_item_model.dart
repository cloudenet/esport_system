

import 'package:esport_system/data/model/item_model.dart';

class CartItemModel {
  ItemModel? items;
  int? quantity;
  String? subNote;

  CartItemModel({this.items, this.quantity, this.subNote});

  CartItemModel.fromJson(Map<String, dynamic> json) {
    if (json['item_id'] != null &&
        json['item_id'] is Map<String, dynamic>) {
      items = ItemModel.fromJson(json['item_id']);
    }
    quantity = json['qty'];
    subNote = json['sub_note'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    if (items != null) {
      data['item_id'] = items!.toJson();
    }
    data['qty'] = quantity;
    data['sub_note'] = subNote;
    return data;
  }

}
