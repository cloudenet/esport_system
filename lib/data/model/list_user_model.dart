class ListUserModel {
  String? id;
  String? cardNumber;
  String? name;
  String? username;
  String? nameKhmer;
  String? phone;
  String? email;
  String? emailVerifiedAt;
  String? photo;
  String? gender;
  String? dob;
  String? address;
  String? userStatus;
  String? role;
  String? devicesToken;
  String? lastLoginAt;
  String? rememberToken;
  String? createdBy;
  String? updatedBy;
  String? identityNo;
  List<String>? identityPhoto;
  String? nationality;
  String? villageCode;
  String? communeCode;
  String? districtCode;
  String? provinceCode;
  String? villageName;
  String? communeName;
  String? districtName;
  String? provinceName;
  String? controlOf;
  String? createdAt;
  String? updatedAt;
  String? level;
  AddressControlOf? addressControlOf;

  ListUserModel(
      {this.id,
        this.cardNumber,
        this.name,
        this.username,
        this.nameKhmer,
        this.phone,
        this.email,
        this.emailVerifiedAt,
        this.photo,
        this.gender,
        this.dob,
        this.address,
        this.userStatus,
        this.role,
        this.devicesToken,
        this.lastLoginAt,
        this.rememberToken,
        this.createdBy,
        this.updatedBy,
        this.identityNo,
        this.identityPhoto,
        this.nationality,
        this.villageCode,
        this.communeCode,
        this.districtCode,
        this.provinceCode,
        this.villageName,
        this.communeName,
        this.districtName,
        this.provinceName,
        this.controlOf,
        this.createdAt,
        this.updatedAt,
        this.addressControlOf,
        this.level,
      });

  ListUserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    cardNumber = json['card_number'].toString();
    name = json['name'].toString();
    username = json['username'].toString();
    nameKhmer = json['name_khmer'].toString();
    phone = json['phone'].toString();
    email = json['email'].toString();
    emailVerifiedAt = json['email_verified_at'].toString();
    photo = json['photo'].toString();
    gender = json['gender'].toString();
    dob = json['dob'].toString();
    address = json['address'].toString();
    userStatus = json['user_status'].toString();
    role = json['role'].toString();
    devicesToken = json['devices_token'].toString();
    lastLoginAt = json['last_login_at'].toString();
    rememberToken = json['remember_token'].toString();
    createdBy = json['created_by'].toString();
    updatedBy = json['updated_by'].toString();
    identityNo = json['identity_no'].toString();
    identityPhoto = json['identity_photo'] is List
        ? List<String>.from(json['identity_photo'])
        : null;
    nationality = json['nationality'].toString();
    villageCode = json['village_code'].toString();
    communeCode = json['commune_code'].toString();
    districtCode = json['district_code'].toString();
    provinceCode = json['province_code'].toString();
    villageName = json['village'].toString();
    communeName = json['commune'].toString();
    districtName = json['district'].toString();
    provinceName = json['province'].toString();
    controlOf = json['control_of'].toString();
    createdAt = json['created_at'].toString();
    level = json['level'].toString();
    updatedAt = json['updated_at'].toString();
    if (json['address'] != null &&
        json['address'] is Map<String, dynamic>) {
      addressControlOf = AddressControlOf.fromJson(json['address']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['card_number'] = cardNumber;
    data['name'] = name;
    data['username'] = username;
    data['name_khmer'] = nameKhmer;
    data['phone'] = phone;
    data['email'] = email;
    data['email_verified_at'] = emailVerifiedAt;
    data['photo'] = photo;
    data['gender'] = gender;
    data['dob'] = dob;
    data['address'] = address;
    data['user_status'] = userStatus;
    data['role'] = role;
    data['devices_token'] = devicesToken;
    data['last_login_at'] = lastLoginAt;
    data['remember_token'] = rememberToken;
    data['created_by'] = createdBy;
    data['updated_by'] = updatedBy;
    data['identity_no'] = identityNo;
    data['identity_photo'] = identityPhoto;
    data['nationality'] = nationality;
    data['village_code'] = villageCode;
    data['commune_code'] = communeCode;
    data['district_code'] = districtCode;
    data['province_code'] = provinceCode;
    data['village'] = villageName;
    data['commune'] = communeName;
    data['district'] = districtName;
    data['province'] = provinceName;
    data['control_of'] = controlOf;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['level'] = level;
    if (addressControlOf != null) {
      data['address'] = addressControlOf!.toJson();
    }
    return data;
  }
}

class AddressControlOf {
  String? province;
  String? district;
  String? commune;
  String? village;
  String? level;

  AddressControlOf({this.province, this.district, this.commune, this.village, this.level});

  AddressControlOf.fromJson(Map<String, dynamic> json) {
    province = json['province'].toString();
    district = json['district'].toString();
    commune = json['commune'].toString();
    village = json['village'].toString();
    level = json['level'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['province'] = province;
    data['district'] = district;
    data['commune'] = commune;
    data['village'] = village;
    data['level'] = level;
    return data;
  }
}
