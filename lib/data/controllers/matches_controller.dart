import 'dart:convert';
import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/subject_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/registration_model.dart';
import 'package:esport_system/data/repo/matches_repo.dart';
import 'package:esport_system/views/matches_section/screen/get_registration_matches.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../helper/sr.dart';
import '../../views/matches_section/screen/register_match.dart';
import '../model/event_model.dart';
import '../model/matches_model.dart';
import '../model/player_model.dart';
import '../model/subject_model.dart';
import '../model/team_model.dart';
import 'home_controller.dart';

class MatchesController extends GetxController implements GetxService {
  final MatchesRepo _matchesRepo = MatchesRepo();
  var homeController = Get.find<HomeController>();
  var teamController = Get.find<TeamController>();
  var eventController = Get.find<EventController>();
  var  playerController= Get.find<PlayerController>();

  TextEditingController eventIdController = TextEditingController();
  TextEditingController homeTeamIdController = TextEditingController();
  TextEditingController awayTeamIdController = TextEditingController();
  TextEditingController startTimeController = TextEditingController();
  TextEditingController endTimeController = TextEditingController();
  TextEditingController createdByController = TextEditingController();
  TextEditingController losingController = TextEditingController();
  TextEditingController winningController = TextEditingController();
  TextEditingController startDateController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController venueController = TextEditingController();
  TextEditingController approvedNoteController = TextEditingController();


  List<MatchesModel> matchesList = [];
  Map<String, dynamic> data = {};
  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  var selectedStatus = '';
  String matchSearch = "" ;
  String? selectedValue;
  String? selectedTeamId;
  TeamModel? selectTeam ;
  EventModel? selectEvent;
  String? selectedEventId;
  String? selectedEventTitle;
  String? selectedMatchType = "None";
  String? selectedSubId ;
  String? filterStatus;
  SubjectModel? selectSubject ;
  final List<String> dropdownItems = ['completed'.tr, 'cancelled'.tr, 'pending'.tr];
  List<String> listMatchType = ["provinces","international","national","none"];
  List<String> listStatus = ["approved","rejected","pending"];
  List<PlayerModel> selectedPlayers = [];
  PlayerModel? previousSelectPlayer;
  List<PlayerModel>? selectedPlayer ;
  List<SubjectModel> selectSubjects = [];
  List<MatchDetail> matchDetail = [];

  @override
  void onInit() {
    super.onInit();
    teamController.getTeam();
    eventController.getEvent();
  }


  void selectDropdownValue(String value){
    selectedValue = value;
  }

  void selectStatus(String status) {
    selectedStatus = status;

    update();
  }
  void search(String query) {
    matchSearch = query ;
    getMatches();
  }

  void setPage(int page) {
    currentPage = page;
    getMatches();
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getMatches();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getMatches();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    getMatches();
  }

  void searchRegisterMatch(String query) {
    matchSearch = query ;
    getRegistrationMatch();
  }

  void setPageRegisterMatch(int page) {
    currentPage = page;
    getRegistrationMatch();
  }

  void  nextPageRegisterMatch() {
    if (currentPage < totalPages) {
      currentPage++;
      getRegistrationMatch();
    }
  }

  void previousPageRegisterMatch() {
    if (currentPage > 1) {
      currentPage--;
      getRegistrationMatch();
    }
  }

  void setLimitRegisterMatch(int newLimit) {
    limit = newLimit;
    getRegistrationMatch();
  }

  Future<void> selectDate(BuildContext context, TextEditingController controller) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );

    if (picked != null) {
      controller.text = DateFormat('yyyy-MM-dd').format(picked);
    }
  }

  var subjectController = Get.find<SubjectController>();
  void setOldValue(RegistrationMatchesModel? data){
    selectTeam = teamController.listTeam.firstWhere((team) => team.id == data!.team?.id,);

    selectEvent = eventController.eventData
        .where((event) => event.id == data?.event?.id)
        .isNotEmpty
        ? eventController.eventData.firstWhere((event) => event.id == data!.event!.id)
        : null;

    print("Why This event: ${selectEvent?.toJson()}");
    selectedMatchType = listMatchType.firstWhere((type) => type == data!.matchType,);
    descriptionController.text = data!.description.toString();
    venueController.text = data.venue.toString();
    if(data.matchDetails!=null){
      matchDetail = (data.matchDetails ?? []).map((data) {
        return MatchDetail(
          playerId: SR.toInt(data.player?.id ?? 0), // Ensure playerId is not null
          subjectId: SR.toInt(data.subject?.id ?? 0), // Ensure subjectId is not null
          description: "", // Ensure description is not null
        );
      }).toList();

    }
    selectedPlayers = (data.matchDetails ?? []).map((data) => PlayerModel(
      id: data.player?.id ?? "0",
      name: data.player?.playerName ?? '',
    )).toList();

    selectSubjects = (data.matchDetails ?? []).map((data) {
      return subjectController.subjectData.firstWhere(
            (subject) => subject.id == data.subject?.id,
        orElse: () => SubjectModel(id: "0", title: ""), // Default if no match
      );
    }).toList();
    for(int i=0 ; i<selectSubjects.length ;i++){
      var b = selectSubjects[i];
      print("-----SubID: ${b.id} ----SubTitle: ${b.title}");
    }
    update();
  }

  void clearData() {
    selectedTeamId = null;
    selectedEventId = null;
    selectedMatchType = null;
    descriptionController.clear();
    venueController.clear();
    matchDetail.clear();
    selectedPlayers.clear();
    selectTeam = null ;
    selectEvent = null ;
    update();
  }

  Future<void> registerMatch() async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      // final body = {
      //   'team_id': selectedTeamId,
      //   'event_id': selectedEventId,
      //   'description': descriptionController.text,
      //   'match_type': selectedMatchType,
      //   'venue': venueController.text,
      //   'created_by': 1,  // Assuming the logged-in user ID is 1
      //   'match_details': getMatchDetailsJson(),
      // };
      String jsonBody = jsonEncode({"match_details": getMatchDetailsJson()});
      print("BodyCont: $jsonBody");

      await _matchesRepo.registerMatch(
          teamId: selectTeam!.id,
          eventId: selectEvent!.id,
          des: descriptionController.text,
          matchType: selectedMatchType,
          venue: venueController.text,
          matchDetails: getMatchDetailsJson()
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            "register_match_create_successfully".tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearData();
          homeController.updateWidgetHome(const GetRegistrationMatches());
        }else{
          EasyLoading.showError(
            "failed_to_create_match".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("Failed to create",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }

  }

  Future<void> updateRegisterMatch({required String id}) async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      // final body = {
      //   'team_id': selectedTeamId,
      //   'event_id': selectedEventId,
      //   'description': descriptionController.text,
      //   'match_type': selectedMatchType,
      //   'venue': venueController.text,
      //   'created_by': 1,  // Assuming the logged-in user ID is 1
      //   'match_details': getMatchDetailsJson(),
      // };
      // String jsonBody = jsonEncode({"match_details": getMatchDetailsJson()});
      // print("BodyCont: $jsonBody");

      await _matchesRepo.updateRegistrationMatch(
          id: id,
          teamId: selectTeam!.id,
          eventId: selectEvent!.id,
          des: descriptionController.text,
          matchType: selectedMatchType,
          venue: venueController.text,
          matchDetails: getMatchDetailsJson()
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            "updated_match_successfully".tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearData();
          homeController.updateWidgetHome(const GetRegistrationMatches());
        }else{
          EasyLoading.showError(
            "failed_to_update_match".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("failed_to_update_match",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }

  }

  void selectPlayer(PlayerModel player) {
    selectedPlayers.add(player);
    update();
  }

  Future<bool> createMatches({
    required String eventId,
    required String homeTeamId,
    required String awayTeamId,
    required String startTime,
    required String endTime,
  })
  async {
    isLoading = true;
    update();
    try {
      bool success = await _matchesRepo.createMatches(
        eventId: eventId,
        homeTeamId: homeTeamId,
        awayTeamId: awayTeamId,
        startTime: startTime,
        endTime: endTime,
      );

      if (success) {
        getMatches(); // Refresh matches list after creating a match
        isLoading = false;
        update();
        return true;
      } else {
        isLoading = false;
        update();
        return false;
      }
    } catch (e) {
      print('Error creating match: $e');
      isLoading = false;
      update();
      return false;
    }
  }


  Future<bool> updateStatusMatche({
    required String matcheId,
    required String statusMatche,
  })
  async {
    try {
      isLoading = true;
      update();
      await _matchesRepo.updateStatusMatche(
          matcheId: matcheId, matcheStatus: statusMatche);
      isLoading = false;
      getMatches(); // Refresh matches list after creating a match
      return true;
    } catch (e) {
      print('Error creating match: $e');
      isLoading = false;
      update();
      return false;
    }
  }

  Future<bool> updateModifyMatch({
    required String matcheId,
    required String winningId,
    required String losingId,
    required String? winningScore,
    required String losingScore,
  })
  async {
    try {
      update();
      await _matchesRepo.updateModifyMatch(
          matcheId: matcheId,
          winningId: winningId,
          losingId: losingId,
          winningScore: winningScore,
          losingScore: losingScore);
      getMatches(); // Refresh matches list after creating a match
      return true;
    } catch (e) {
      print('Error creating match: $e');
      update();
      return false;
    }
  }

  Future<void> getMatches() async {
    isLoading = true;
    update();
    try {
      matchesList = await _matchesRepo.getMatches(
        matchStatus: matchSearch, // Adjust the parameter as necessary
        page: currentPage,
        limit: limit,
      );
      totalPages = (matchesList.length == limit) ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (error) {
      print("Error fetching matches: $error");
      isLoading = false;
      update();
    }
  }

  Future<bool> updateMatches({
    required String eventId,
    required String homeTeamId,
    required String awayTeamId,
    required String startTime,
    required String endTime,
    required String matchId,
    required String updatedBy,
  })
  async {
    try {
      isLoading = true;
      update();
      await _matchesRepo.updateMatches(
        eventId: eventId,
        homeTeamId: homeTeamId,
        awayTeamId: awayTeamId,
        startTime: startTime,
        endTime: endTime,
        matchId: matchId,
        updatedBy: updatedBy,
      );
      isLoading = false;
      getMatches(); // Refresh matches list after updating a match
      return true;
    } catch (e) {
      print('Error updating match: $e');
      isLoading = false;
      update();
      return false;
    }
  }


  void addMatchDetail(MatchDetail detail) {
    matchDetail.add(detail);
    update();
  }

  void updateMatchDetail(int index, MatchDetail detail) {
    if (index < matchDetail.length) {
      matchDetail[index] = detail;
    } else {
      matchDetail.add(detail);
    }
    update();
  }

  void removeMatchDetail(int index) {
    if (index >= 0 && index < matchDetail.length) {
      matchDetail.removeAt(index);
      update();
    } else {
      print("Index out of bounds. Cannot remove match detail.");
    }
  }

  List<Map<String, dynamic>> getMatchDetailsJson() {
    return matchDetail.map((detail) => detail.toJson()).toList();
  }

  List<RegistrationMatchesModel> listRegistrationMatches = [];

  Future<void> getRegistrationMatch()async{
    isLoading = true;
    update();
    try{
      await _matchesRepo.getRegistrationMatch(
          status: filterStatus,
          page: currentPage,
          limit: limit,).then((v){
        if(v.isNotEmpty){
          listRegistrationMatches = v;
          isLoading = false;
        }else{
          listRegistrationMatches = [];
          isLoading = false;
        }
      });
      totalPages = (matchesList.length == limit) ? currentPage + 1 : currentPage;
    }catch(e){
      listRegistrationMatches = [];
      isLoading = false;
    }
    update();
  }


  Future<void> authorizeRegistration({
    required String id,
    required String status,
  }) async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _matchesRepo.authorizeRegistration(
          id: id,
          status: status,
          approvedNote: approvedNoteController.text
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            "authorize_match_successfully".tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearData();
          homeController.updateWidgetHome(const GetRegistrationMatches());
        }else{
          EasyLoading.showError(
            "failed_to_authorize_match".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("failed_to_authorize_match".tr,
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }

  }
}
