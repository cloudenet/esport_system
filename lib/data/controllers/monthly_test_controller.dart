

import 'package:esport_system/data/model/subject_model.dart';
import 'package:esport_system/data/repo/monthly_test_repo.dart';
import 'package:esport_system/data/repo/player_repo.dart';
import 'package:esport_system/data/repo/subject_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../model/monthly_test_model.dart';
import '../model/player_model.dart';

class MonthlyTestController extends GetxController implements GetxService{

  TextEditingController monthYearTestController = TextEditingController();
  var monthlyTestRepo = MonthLyTestRepo();
  final _playerRepo = PlayerRepo();
  var subjectRepo = SubjectRepo();
  String? selectCategoryPlayer ;
  PlayerModel? selectPlayer ;
  String? selectedPlayerIdOne;
  bool isLoading = false;
  List<PlayerModel> dataPlayerBaseCat = [];
  List<SubjectModel> subjectData = [];

  Future<void> selectDateMonth(BuildContext context, TextEditingController controller) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );

    if (picked != null) {
      //controller.text = DateFormat('MMMM yyyy').format(picked);
      controller.text = DateFormat('MM-yyyy').format(picked);
      print("Debug month/Year  :${controller.text}");
    }
  }
  void getAllSubject() async {
    try{
      await subjectRepo.getListSubject().then((v) {
        if (v.isNotEmpty) {
          subjectData = v;
        } else {
          subjectData = [];
        }
        update();
      });
    }catch(e){
      print("Error During Get Player search in controller $e");
      subjectData = [];
      update();
    }
  }

  Future<void> getPlayerDropdownSearch(String? searchQuery) async {
    isLoading = true;
    update();
    try {
      dataPlayerBaseCat = await _playerRepo.getPlayer(
          limit: "",
          page: "",
          search: searchQuery
      );
      isLoading = false;
      update();
    } catch (e) {
      print("Error During Get Player search in controller $e");
      isLoading = false;
      update();
    }
  }

}