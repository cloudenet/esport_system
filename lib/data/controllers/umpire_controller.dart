


import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/repo/rank_repo.dart';
import 'package:esport_system/data/repo/umpire_repo.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import '../../views/home_section/screen/home.dart';
import '../../views/umpire_section/get_list_umpire.dart';
import '../model/list_user_model.dart';
import '../model/referees_model.dart';
import 'package:esport_system/data/model/rank_model.dart';

import 'home_controller.dart';


class RefereesController extends GetxController implements GetxService{

  UmpiresRepo umpireRepo = UmpiresRepo();
  final RankRepo _rankRepo = RankRepo();
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();

  ListUserModel? selectedUser;
  String? selectedUserId;
  XFile? fileBio ;
  TextEditingController trainingDateController = TextEditingController();
  TextEditingController certificateController = TextEditingController();
  TextEditingController fileBioRefereeController = TextEditingController();
  TextEditingController yearOfExperienceController = TextEditingController();
  TextEditingController rankController = TextEditingController();
  bool isLoading = false;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  String? umpiresId ;
  List<UmpiresModel> listReferees = [];
  List<RankModel> rankReferees = [];
  String? selectedRefereesRank;
  String? previousBioFilePath;
  List<String>? previousCertificateFilePaths;

  void isWaiting(status){
    isLoading = status ;
    update();
  }

  List<PlatformFile>? certificateFiles = [];
  bool isFirstFilePick = true;

  // Future<List<PlatformFile>?> pickCertificateFiles() async {
  //   FilePickerResult? certificateFilesResult = await FilePicker.platform.pickFiles(
  //     allowMultiple: true,
  //     type: FileType.any,
  //   );
  //   if (certificateFilesResult != null) {
  //     certificateFiles?.addAll(certificateFilesResult.files);
  //     if(certificateFiles!.isNotEmpty){
  //       for (var file in certificateFiles!) {
  //         certificateController.text = file.name.toString();
  //       }
  //       for (var file in certificateFiles!) {
  //         if (file.bytes != null) {
  //           print('Picked file with bytes: ${file.name}');
  //         } else {
  //           print('Picked file with no bytes: ${file.name}');
  //         }
  //       }
  //     }
  //     update();
  //     return certificateFilesResult.files;
  //   } else {
  //     return null;
  //   }
  // }

  Future<List<PlatformFile>?> pickCertificateFiles() async {
    if (isFirstFilePick) {
      certificateFiles?.clear();
      isFirstFilePick = false;
      update();
    }
    FilePickerResult? certificateFilesResult = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.any,
    );

    if (certificateFilesResult != null) {
      certificateFiles?.addAll(certificateFilesResult.files);
      update();
      return certificateFilesResult.files;
    } else {
      return null;
    }
  }
  void resetFilePickState() {
    isFirstFilePick = true;
    update();
  }

  void removeCertificateFile(String? file ,int index) {
    print("file index remove : $file -- $index");
    if (index >= 0 && index < certificateFiles!.length) {
      certificateFiles!.removeAt(index);
      update();
    }
  }



  void addPreviousFiles(List<String> serverFiles) {
    certificateFiles = [];
    for (String filePath in serverFiles) {
      String fileName = filePath.split('/').last;
      certificateFiles?.add(PlatformFile(
        name: fileName,
        path: filePath,
        size: 0,
      ));
      for (var file in certificateFiles!) {
        debugPrint('Picked file ctr: ${file.name} -- length ${certificateFiles!.length}');
      }
    }
    update(); // Notify listeners or update the UI
  }

  void setPreviousValue(UmpiresModel? data){
    selectedRefereesRank = data!.rankId!.id.toString();
    selectedUserId = data.userId!.id.toString();
    yearOfExperienceController.text = data.exYear.toString();
    trainingDateController.text = data.trainingDate.toString();
    if(data.certificate != null){
      addPreviousFiles(data.certificate!);
    }
    if (data.bioFile != null && data.bioFile!.isNotEmpty) {
      fileBio = XFile(data.bioFile!.split('/').last);
      fileBioRefereeController.text = fileBio!.path;
      print('File exists: ${fileBio?.path}');
    }
    selectedUser = userController.listAllUser.firstWhere((user) =>
    user.id.toString() == data.userId!.id.toString(),);
    update();
  }

  void clearData(){
    selectedUserId = null ;
    selectedRefereesRank = null ;
    yearOfExperienceController.text = "" ;
    trainingDateController.text = "";
    fileBio = null ;
    certificateFiles = null ;
  }


  Future<void> getRankReferees() async {
    isWaiting(false);
    try {
      await _rankRepo.getRank(search: '',).then((v){
        if(v.isNotEmpty){
          rankReferees = v ;
        }else{
          rankReferees = [];
        }
        isWaiting(false);
      });
    } catch (e) {
      rankReferees = [];
      isWaiting(false);
      print("-------Error during get position : $e");
    }
  }


  Future<void> createUmpire()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await umpireRepo.createUmpires(
          assignUserId: selectedUserId.toString(),
          rankId: selectedRefereesRank.toString(),
          exYears: yearOfExperienceController.text,
          trainDate: trainingDateController.text,
          bioFile: fileBio,
          certificateFiles: certificateFiles!
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'umpire_create_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearData();
          getUmpires();
          homeController.updateWidgetHome(const GetListUmpires());
        }else{
          EasyLoading.showError("failed_to_create_umpire".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("Failed to create $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> updateUmpire({required String umpireId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await umpireRepo.updateUmpires(
          umpireId: umpireId,
          rankId: selectedRefereesRank.toString(),
          exYears: yearOfExperienceController.text,
          trainDate: trainingDateController.text,
          bioFile: fileBio,
          certificateFiles: certificateFiles!
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'umpire_updated_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearData();
          getUmpires();
          homeController.updateWidgetHome(const GetListUmpires());
        }else{
          EasyLoading.showError("failed_to_update_umpire".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("Failed to updated $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> deleteUmpire({required String umpireId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await umpireRepo.deleteUmpire(umpireId: umpireId).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'umpire_deleted_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          getUmpires();
          homeController.updateWidgetHome(const GetListUmpires());
        }else{
          EasyLoading.showError("failed_to_deleted_umpire".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });

    }catch(e){
      EasyLoading.showError("Failed to deleted $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> getUmpires()async{
    isWaiting(true);
    try{
      await umpireRepo.getUmpires(limit: limit,page: currentPage,search: searchQuery).then((v){
        if(v.isNotEmpty){
          listReferees = v;
          totalPages = listReferees.length == limit ? currentPage + 1 : currentPage;
          isWaiting(false);
        }else{
          listReferees = [];
          isWaiting(false);
        }
      });
    }catch(e){
      isWaiting(false);
      print("Issue controller : $e");
    }
  }

  void setPage(int page) {
    currentPage = page;
    getUmpires();
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getUmpires();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getUmpires();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getUmpires();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getUmpires();
    update();
  }
}