import 'dart:convert';

import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/data/repo/league_repo.dart';
import 'package:esport_system/data/repo/user_repo.dart';
import 'package:esport_system/helper/picker_image.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/auth_section/screen/login_page.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/setting_section/screen/setting.dart';
import 'package:esport_system/views/user_section/screen/get_list_user.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh_flutter3/pull_to_refresh_flutter3.dart';

import 'package:flutter/rendering.dart';
import 'dart:ui' as ui;
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:uuid/uuid.dart';


import 'dart:html' as html;

import 'home_controller.dart';

class UserController extends GetxController implements GetxService {
  final UserRepo _userRepo = UserRepo();
  final LeagueRepository _leagueRepository = LeagueRepository();
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();

  ScrollController scrollController = ScrollController();
  RefreshController refreshController = RefreshController(initialRefresh: false);


  TextEditingController cardNameController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController khmerNameController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController identityNoController = TextEditingController();
  TextEditingController nationalityController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String selectGender = 'male';
  TextEditingController dayofbirthdayController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController cfmPasswordController = TextEditingController();

  List<ListUserModel> listUser = [];
  List<ListUserModel> listUserDropdown = [];
  List<ListUserModel> listAllUser = [];
  List<ListUserModel> userDetail = [];
  ListUserModel? selectUserModel;

  List<Address> provinces = [];
  List<Address> districts = [];
  List<Address> communes = [];
  List<Address> villages = [];
  Address? selectedProvince;
  Address? selectedDistrict;
  Address? selectedCommune;
  Address? selectedVillage;
  bool isLoadingProvinces = false;
  bool isLoadingDistricts = false;
  bool isLoadingCommunes = false;
  bool isLoadingVillages = false;

  List<ListUserModel> filteredUsers = [];
  bool isLoading = true;
  String searchQuery = '';
  ValueNotifier<bool> isButtonEnabled = ValueNotifier(false);

  // Pagination variables
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  Uint8List? imageData;
  Map<String, Uint8List?> imageDataMap = {};
  bool isAllow = false;
  List<ListUserModel> displayedUsers = [];
  bool get isImageSelected => imageData != null;
  GlobalKey globalKey = GlobalKey();
  GlobalKey globalKeyBack = GlobalKey();
  GlobalKey globalKeyReport = GlobalKey();

  final List<Map<String, String>> typeGender = [
    {'value': 'male', 'text': 'Male'},
    {'value': 'female', 'text': 'Female'},
  ];


  Future<void> renderWidgetsToImagesX(List<GlobalKey> keys) async {
    if (kIsWeb) {
      for (var key in keys) {
        await Future.delayed(const Duration(milliseconds: 200));
        RenderRepaintBoundary boundary = key.currentContext!.findRenderObject() as RenderRepaintBoundary;

        // Capture image with pixelRatio
        ui.Image image = await boundary.toImage(pixelRatio: 5.0);
        ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
        Uint8List pngBytes = byteData!.buffer.asUint8List();

        // Convert image to base64
        final base64data = base64Encode(pngBytes);

        // Create a downloadable link
        final html.AnchorElement anchor = html.AnchorElement(
            href: 'data:image/png;base64,$base64data')
          ..setAttribute("download", "${const Uuid().v4()}.png")
          ..click();
      }
      return;
    }

    try {
      for (var key in keys) {
        RenderRepaintBoundary? boundary =
        key.currentContext?.findRenderObject() as RenderRepaintBoundary?;
        if (boundary != null) {
          ui.Image image = await boundary.toImage();
          ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
          if (byteData != null) {
            final pngBytes = byteData.buffer.asUint8List();

            final directory = (await getApplicationDocumentsDirectory()).path;
            File imgFile = File('$directory/${const Uuid().v4()}.png');
            await imgFile.writeAsBytes(pngBytes);

            await ImageGallerySaver.saveFile(imgFile.path);
            Get.snackbar(
              barBlur: 10,
              'Success', // title
              'QR Code saved to gallery!', // message
              backgroundColor: Colors.greenAccent,
              icon: const Icon(Icons.save_alt_sharp, color: Colors.white), // icon
            );
          }
        }
      }
    } catch (e) {
      print(e);
      Get.snackbar('Error', 'Failed to save QR Codes');
    }
  }

  Future<void> renderWidgetsToImages(List<GlobalKey> keys) async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    if (kIsWeb) {
      for (var key in keys) {
        await Future.delayed(const Duration(milliseconds: 200));
        RenderRepaintBoundary boundary = key.currentContext!.findRenderObject() as RenderRepaintBoundary;

        // Capture image with pixelRatio
        ui.Image image = await boundary.toImage(pixelRatio: 5.0);
        ByteData? byteData = await image.toByteData(format: ui.ImageByteFormat.png);
        Uint8List pngBytes = byteData!.buffer.asUint8List();

        // Convert image to base64
        final base64data = base64Encode(pngBytes);

        // Create a downloadable link
        final html.AnchorElement anchor = html.AnchorElement(
            href: 'data:image/png;base64,$base64data')
          ..setAttribute("download", "${const Uuid().v4()}.png")
          ..click();
      }
      EasyLoading.dismiss();
      return;
    }
  }
  Uint8List? getImageDataForUser(String userId) {
    return imageDataMap[userId];
  }


  void isWait(value){
    isLoading = value;
    update();
  }

  @override
  void onClose() {
    newPasswordController.dispose();
    isButtonEnabled.dispose();
    super.onClose();
  }

  void clearTextField(){
    cardNameController.text = "" ;
    userNameController.text = "" ;
    phoneController.text = "" ;
    emailController.text = "" ;
    passwordController.text = "" ;
    dayofbirthdayController.text = "" ;
    khmerNameController.text = "" ;
    addressController.text = "" ;
    selectedProvince = null;
    selectedDistrict = null;
    selectedCommune = null;
    selectedVillage = null;
    nameController.text = "" ;
    selectedImageUser = null ;
    nationalityController.text = "";
    identityNoController.text = "";
    listIdentity = [];
    selectedFrontId = null ;
    selectedBackID = null ;
    imageDataX = null;
  }

  void clearPass() {
    newPasswordController.text = "";
    cfmPasswordController.text = "";
  }

  void selectUser(ListUserModel userModel){
    selectUserModel = userModel ;
    update();
  }

  void fetchProvinces() async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getAddresses();
      provinces.assignAll(result.where((address) => address.type == 'province').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchDistricts(String provinceCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getDistricts(provinceCode);
      districts.assignAll(
          result.where((address) => address.type == 'district').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchCommunes(String districtCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getCommunes(districtCode);
      communes.assignAll(
          result.where((address) => address.type == 'commune').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchVillages(String communeCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getVillages(communeCode);
      villages.assignAll(
          result.where((address) => address.type == 'village').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void selectProvince(Address? province) {
    selectedProvince = province;

    if (province != null) {

      if (selectedProvince!.code == province.code) {
        selectedDistrict = null;
        selectedCommune = null;
        selectedVillage = null;
      }
      fetchDistricts(province.code!);
    } else {
      districts.clear();
      communes.clear();
      villages.clear();
      selectedDistrict = null;
      selectedCommune = null;
      selectedVillage = null;
    }
    update();
  }

  void selectDistrict(Address? district) {
    selectedDistrict = district;
    if (district != null) {
      fetchCommunes(district.code!);
      selectedCommune = null;
    } else {
      communes.clear();
      selectedCommune = null;
    }
    update();
  }

  void selectCommune(Address? commune) {
    selectedCommune = commune;
    if (commune != null) {
      fetchVillages(commune.code!);
      selectedVillage = null;
    } else {
      villages.clear();
      selectedVillage = null;
    }
    update();
  }

  void selectVillage(Address? village) {
    selectedVillage = village;
    update();
  }


  void loadData() {
    cardNameController.toString();
    userNameController.toString();
    nameController.toString();
    phoneController.toString();
    identityNoController.toString();
    nationalityController.toString();
    emailController.toString();
    passwordController.toString();
    dayofbirthdayController.toString();
  }

  Future<void> createUser() async {
    isWait(true);
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try {
      await _userRepo.createUser(
          cardNumber: cardNameController.text,
          userName: userNameController.text,
          phone: phoneController.text,
          email: emailController.text,
          password: passwordController.text,
          gender: selectGender,
          dob: dayofbirthdayController.text,
          address: addressController.text,
          provinceCode: selectedProvince!.code.toString(),
          districtCode: selectedDistrict!.code.toString(),
          communeCode: selectedCommune!.code.toString(),
          villageCode: selectedVillage!.code.toString(),
          provinceName: selectedProvince!.name.toString(),
          districtName: selectedDistrict!.name.toString(),
          communeName: selectedCommune!.name.toString(),
          villageName: selectedVillage!.name.toString(),
          name: nameController.text,
          nameKhmer: khmerNameController.text,
          profile: selectedImageUser,
          myIdentity: listIdentity ).then((a){
        if(a['success']){
          EasyLoading.showSuccess('${a['message']}',
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          clearTextField();
          homeController.updateWidgetHome(const GetUserList());
          isWait(false);
        }else{
          EasyLoading.showError('Error ${a['message']}', duration: const Duration(seconds: 2));
          isWait(false);
        }
      });
      EasyLoading.dismiss();
    } catch (e) {
      isWait(false);
      EasyLoading.showError('Error', duration: const Duration(seconds: 2));
    }
    EasyLoading.dismiss();
  }

  void setPage(int page) {
    currentPage = page;
    getListUser();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getListUser();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getListUser();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getListUser();
  }

  Future<void> getListUser() async {
    try {
      isLoading = true;
      update();
      listUser = await _userRepo.getListUser(
        limit: limit, // Simplified the condition
        search: searchQuery,
        page: currentPage,
      );
      filteredUsers = listUser;
      totalPages = listUser.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      print('Error fetching user list: $e');
      isLoading = false;
      update();
    }
  }

  Future<void> getAllUser()async{
    isLoading = true;
    update();
    try{
      await _userRepo.getAllUser().then((v){
        if(v.isNotEmpty){
          listAllUser = v ;
          isLoading = false;
          update();
        }else{
          listAllUser = [];
          isLoading = false;
          update();
        }
      });
    }catch(e){
      print('Error fetching all user list dropdown in controller: $e');
      isLoading = false;
      update();
    }
  }


  Future<void> getListUserDropdown() async {
    try {
      isLoading = true;
      update();
      listUserDropdown = await _userRepo.getListUserDropDown(
        limit: "",
        search: searchQuery,
        page: "",
      );
      isLoading = false;
      update();
    } catch (e) {
      print('Error fetching user list dropdown in controller: $e');
      isLoading = false;
      update();
    }
  }

  // Future<void> getListUserDropDown({bool isRefresh = false}) async {
  //   if (isRefresh) {
  //     currentPage = 1;
  //   } else {
  //     currentPage++;
  //   }
  //   try {
  //     List<ListUserModel> user = await _userRepo.getListUser(
  //       limit: 0,
  //       search: searchQuery,
  //       page: currentPage,
  //     );
  //     if (isRefresh) {
  //       listUserDropDown = user;
  //       refreshController.refreshCompleted();
  //       update();
  //     } else {
  //       listUserDropDown.addAll(user);
  //       refreshController.loadComplete();
  //       update();
  //     }
  //   } catch (e) {
  //     print("Something went wrong: $e");
  //     if (isRefresh) {
  //       refreshController.refreshFailed();
  //     } else {
  //       refreshController.loadFailed();
  //     }
  //   }
  // }

  void search(String query) {
    searchQuery = query;
    // currentPage = 1;
    getListUser();
  }

  Future<void> getUserDetail() async {
    userDetail = await _userRepo.getUserDetail();
    update();
  }

  XFile? selectedImageId;
  final ImagePickerHelper _imagePickerHelper = ImagePickerHelper();

  Future<void> selectDate(BuildContext context, TextEditingController controller) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );

    if (picked != null) {
      controller.text = DateFormat('yyyy-MM-dd').format(picked);
    }
  }
  void setOldValue(ListUserModel userModel){
    cardNameController.text= userModel.cardNumber != "null" ? userModel.cardNumber.toString() : "N/A";
    nameController.text= userModel.name != "null" ? userModel.name.toString() : "N/A";
    khmerNameController.text = userModel.nameKhmer != "null" ? userModel.nameKhmer.toString() : "N/A";
    userNameController.text= userModel.username != "null" ? userModel.username.toString() : "N/A";
    phoneController.text= userModel.phone != "null" ? userModel.phone.toString() : "N/A";
    emailController.text= userModel.email != "null" ? userModel.email.toString() : "N/A";
    dayofbirthdayController.text= userModel.dob != "null" ? userModel.dob.toString() : "N/A";
    addressController.text= userModel.provinceName != "null" ? userModel.provinceName.toString() : "N/A";
    selectGender = userModel.gender != "null" ? userModel.gender.toString() : "N/A";
    identityNoController.text = userModel.identityNo != "null" ? userModel.identityNo.toString() : "N/A";
    nationalityController.text = userModel.nationality != "null" ?userModel.nationality.toString() : "N/A";

    selectProvince(Address(
        code: userModel.provinceCode,
        name: userModel.provinceName
    ));
    selectDistrict(Address(
        code: userModel.districtCode,
        name: userModel.districtName
    ));
    selectCommune(Address(
        code: userModel.communeCode,
        name: userModel.communeName
    ));
    selectVillage(Address(
        code: userModel.villageCode,
        name: userModel.villageName
    ));

  }

  Future<bool> updateUserInfo({
    required String userId,
    required String name,
    required String email,
    required String identityNo,
    required String nationality,
    required String gender,
    required String dob,
    required String address,
    required String updatedBy,
    required String cardNumber,
    required XFile? identityImage,
  }) async {
    isLoading = true;
    update(); // Trigger UI update
    try {
      await _userRepo.updateUserInfo(
        userId: userId,
        name: name,
        email: email,
        identityNo: identityNo,
        nationality: nationality,
        gender: gender,
        dob: dob,
        address: address,
        updatedBy: updatedBy,
        cardNumber: cardNumber,
        identityImagePath: identityImage!,
      );
      getListUser();
      isLoading = false;
      update(); // Trigger UI update
      return true;
    } catch (e) {
      isLoading = false;
      update(); // Trigger UI update
      return false;
    }
  }

  XFile? selectedImageUser;
  XFile? selectedFrontId;
  XFile? selectedBackID;
  List<XFile> listIdentity = [];


  Future<XFile?> pickImage() async {
    return await _imagePickerHelper.pickImageFromGallery(); // Or pickImageFromCamera
  }



  Future<void> pickID() async {
    // Choose either from camera or gallery
    XFile? imageFile = await _imagePickerHelper
        .pickImageFromGallery(); // Or pickImageFromCamera
    if (imageFile != null) {
      selectedImageId = imageFile;
      update();
    }
  }

  Future<void> changeUserPassword(
      {required String userId, required String newPassword}) async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try {
      await _userRepo
          .updateUserPassword(userId: userId, newPassword: newPassword)
          .then((value) {
        if (value) {
          EasyLoading.showSuccess(
            'password_change_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          clearPass();
        } else {
          EasyLoading.showError('Error', duration: const Duration(seconds: 2));
          clearPass();
        }
      });
      EasyLoading.dismiss();
      update();
    } catch (e) {
      EasyLoading.showError('Error', duration: const Duration(seconds: 2));
      clearPass();
    }
    update();
  }

  Future<bool> updateProfileUser({required String userId, required Uint8List? imageData}) async {
    isLoading = true;
    update(); // Trigger UI update
    try {
      XFile? imageFile;
      if (imageData != null) {
        imageFile = XFile.fromData(imageData, mimeType: 'image/jpeg'); // assuming JPEG image type
      }
      await _userRepo.upDateProfileUser(imagePath: imageFile);
      getListUser();
      isLoading = false;
      update(); // Trigger UI update
      return true;
    } catch (e) {
      isLoading = false;
      update(); // Trigger UI update
      return false;
    }
  }

  void changeProfile ()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      XFile? xFileImage = imageDataX != null ? XFile.fromData(imageDataX!) : null;
      await _userRepo.upDateProfileUser(imagePath: xFileImage).then((v) {
        if(v['success']){
          EasyLoading.showSuccess(
            'profile_change_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          getMe();
          clearPickedImage();
        }else{
          EasyLoading.showError('Error', duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError('An error occurred: $e',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  Future<void> pickImageFromFilePicker() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.image,
    );

    if (result != null && result.files.isNotEmpty) {
      imageData = result.files.first.bytes;
    }
    update();
  }


  Future<Map<String, dynamic>> deleteUser({required String userId}) async {
    var result = await _userRepo.deleteUser(userid: userId);
    isLoading = false;
    update();
    return result;
  }

  Future<void> assignControlUser({
    required String level,
    required String userID,
    required String province,
    required String district,
    required String commune,
    required String village,
  }) async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _userRepo.assignControlUser(
          level: level,
          userID: userID,
          province: province,
          district: district,
          commune: commune,
          village: village).then((v){
        if(v['success']){
          EasyLoading.showSuccess('assign_control_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          teamController.clearAddress();
          homeController.updateWidgetHome(const GetUserList());
        }else{
          EasyLoading.showError(v['message'],
            duration: const Duration(seconds: 2),
          );
          EasyLoading.dismiss();
        }
        update();
      });
    }catch(e){
      EasyLoading.showError('An error occurred: $e',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  void logOut() {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    accessToken.$ = "";
    accessToken.save();
    Future.delayed(const Duration(milliseconds: 300));
    EasyLoading.showSuccess('success_log_out'.tr);
    Get.offAll(const LoginPage());
  }


  Uint8List? imageDataX;
  XFile? profileImg;

  Future<void> pickImageX() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(type: FileType.image);
    if (result != null) {
      print("File selected: ${result.files.single.name}");  // Debug print
      imageDataX = result.files.single.bytes;
      profileImg = XFile.fromData(
        imageDataX!,
        name: result.files.single.name,
        mimeType: result.files.single.extension,
      );
      print("XFile created: ${profileImg!.name}");
      update();
    } else {
      print("No file selected.");
    }
  }

  void clearPickedImage() {
    imageDataX = null;
    update();
  }



  Future<void> updateUser({
    required String userId,
    required XFile? identityImage,
  })
  async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try {
      await _userRepo.updateUser(
          userId: userId,
          name: nameController.text,
          khmerName: khmerNameController.text,
          email: emailController.text,
          identityNo: identityNoController.text,
          nationality: nationalityController.text,
          gender: selectGender,
          dob: dayofbirthdayController.text,
          address: addressController.text,
          cardNumber: cardNameController.text,
          identityImagePath: identityImage!,
          updatedBy: userInfo!.id.toString(),
          provinceCode: selectedProvince!.code.toString(),
          provinceName: selectedProvince!.name.toString(),
          districtCode: selectedDistrict!.code.toString(),
          districtName: selectedDistrict!.name.toString(),
          communeCode: selectedCommune!.code.toString(),
          communeName: selectedCommune!.name.toString(),
          villageCode: selectedVillage!.code.toString(),
          villageName: selectedVillage!.name.toString(),
      ).then((value){
        if(value){
          EasyLoading.showSuccess(
            'update_user_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          getListUser();
          homeController.updateWidgetHome(const GetUserList());
          clearTextField();
        }else{
          EasyLoading.showError('Error', duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
      });
    } catch (e) {
      EasyLoading.showError('Error', duration: const Duration(seconds: 2));
      EasyLoading.dismiss();
    }
  }

  Future<void> updateMySelf() async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try {
      // print("cardNameController : ${cardNameController.text}");
      // print("nameController : ${nameController.text}");
      // print("khmerNameController : ${khmerNameController.text}");
      // print("emailController : ${emailController.text}");
      // print("identityNoController : ${identityNoController.text}");
      // print("nationalityController : ${nationalityController.text}");
      // print("nationalityController : ${nationalityController.text}");
      // print("selectGender : ${selectGender}");
      // print("dayofbirthdayController : ${dayofbirthdayController.text}");
      // print("addressController : ${addressController.text}");
      // print("selectedProvince : ${selectedProvince!.name}");
      // print("selectedDistrict : ${selectedDistrict!.name}");
      // print("selectedCommune : ${selectedCommune!.name}");
      // print("selectedVillage : ${selectedVillage!.name}");
      await _userRepo.updateMySelf(
          cardNumber: cardNameController.text,
          name: nameController.text,
          nameKh: khmerNameController.text,
          email: emailController.text,
          identityNum: identityNoController.text,
          nationality: nationalityController.text,
          gender: selectGender,
          dob: dayofbirthdayController.text,
          address: addressController.text,
          provinceCode: selectedProvince!.code.toString(),
          districtCode: selectedDistrict!.code.toString(),
          communeCode: selectedCommune!.code.toString(),
          villageCode: selectedVillage!.code.toString(),
          province: selectedProvince!.name.toString(),
          district: selectedDistrict!.name.toString(),
          commune: selectedCommune!.name.toString(),
          village: selectedVillage!.name.toString()
      ).then((value){
        if(value['success']){
          EasyLoading.showSuccess(
            'update_mySelf_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          homeController.updateWidgetHome(const SettingScreen());
        }else{
          EasyLoading.showError('Error ${value['message']}', duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
      });
    } catch (e) {
      EasyLoading.showError('Error', duration: const Duration(seconds: 2));
      EasyLoading.dismiss();
    }
  }

  Future<void> updateUsers({required String userId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try{
      await _userRepo.updateUsers(
          userId: userId,
          cardNumber: cardNameController.text,
          name: nameController.text,
          nameKh: khmerNameController.text,
          email: emailController.text,
          identityNum: identityNoController.text,
          nationality: nationalityController.text,
          gender: selectGender,
          dob: dayofbirthdayController.text,
          address: addressController.text,
          provinceCode: selectedProvince!.code.toString(),
          districtCode: selectedDistrict!.code.toString(),
          communeCode: selectedCommune!.code.toString(),
          villageCode: selectedVillage!.code.toString(),
          province: selectedProvince!.name.toString(),
          district: selectedDistrict!.name.toString(),
          commune: selectedCommune!.name.toString(),
          village: selectedVillage!.name.toString(),
          myIdentity: listIdentity).then((value){
        if(value['success']){
          EasyLoading.showSuccess(
            'update_user_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          homeController.updateWidgetHome(const GetUserList());
        }else{
          EasyLoading.showError('Error ${value['message']}', duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
      });
    }catch(e){
      EasyLoading.showError('Error', duration: const Duration(seconds: 2));
      EasyLoading.dismiss();
    }
  }

  Future<void> updateImageProfile({
    required String userId
})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try{
      await _userRepo.updateImageProfile(
          userId: userId,
          imagePath: profileImg
      ).then((value){
        if(value['success']){
          EasyLoading.showSuccess(
            'update_user_profile_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          getListUser();
          profileImg = null;
          imageDataX = null ;
          //homeController.updateWidgetHome(const GetUserList());
        }else{
          EasyLoading.showError('Error ${value['message']}', duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
      });
    }catch(e){
      EasyLoading.showError('Error', duration: const Duration(seconds: 2));
      EasyLoading.dismiss();
    }
  }

}