import 'package:esport_system/data/model/medal_model.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/data/repo/medal_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class MedalsPlayerController extends GetxController implements GetxService {
  final MedalsPlayerRepo _medalsPlayerRepo = MedalsPlayerRepo();

  final List<Map<String, String>> medalOptions = [
    {'value': 'gold_medal', 'text': 'Gold Medal'},
    {'value': 'silver_medal', 'text': 'Silver Medal'},
    {'value': 'bronze_medal', 'text': 'Bronze Medal'},
  ];

  TextEditingController qtyMedalsController = TextEditingController();
  TextEditingController winDateController = TextEditingController();

  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  bool isLoading = false;

  String? selectTeamId;
  String? selectPlayerCateId;
  String? selectPlayerId;
  String? selectCategoryPlayer ;
  PlayerModel? previousSelectPlayer;
  List<MedalModel> medalPlayerData = [];
  List<MedalDetailModel> medalPlayerDetailData = [];

  Future<void> getMedalPlayer({
    required String? playerCatId,
    required String? playerId
  }) async {
    isLoading = true;
    update();
    try {
      medalPlayerData = await _medalsPlayerRepo.getMedalPlayer(
          playerCatId: playerCatId ?? "",
          playerId: playerId ?? "");
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      update();
      print("Error get medal in con $e");
    }
  }

  Future<void> getMedalDetail({required String playerId}) async {
    isLoading = true;
    update();
    try {
      medalPlayerDetailData = await _medalsPlayerRepo.getDetailMedal(playerId: playerId);
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      update();
      print("Error get medal in con $e");
    }
  }

  Future<bool> createMedalPlayer({
    required String playerId,
    required String typeMedal,
    required String eventId,
    required String qtyMedal,
    required String dateWin,
    required String createdBy,
  }) async {
    isLoading = true;
    update();
    try {
      bool result = await _medalsPlayerRepo.createMedalPlayer(
          playerId: playerId,
          typeMedal: typeMedal,
          eventId: eventId,
          qtyMedal: qtyMedal,
          dateWin: dateWin,
          createdBy: createdBy);
      isLoading = false;
      update();
      return result;
    } catch (e) {
      print("Error create medal in con $e");
      return false;
    }
  }
}