import 'package:esport_system/data/model/subject_model.dart';
import 'package:esport_system/data/repo/subject_repo.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class SubjectController extends GetxController implements GetxService {
  final SubjectRepo _subjectRepo = SubjectRepo();
  bool isLoading = true;
  List<SubjectModel> subjectData = [];
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';

  TextEditingController titleController = TextEditingController();
  TextEditingController maxPointController = TextEditingController();
  TextEditingController desController = TextEditingController();

  void getOldValue(SubjectModel data) async {
    titleController.text = data.title.toString();
    maxPointController.text = data.maxPoint.toString();
    desController.text = data.description.toString();
  }

  void clearValue() async {
    titleController.text = "";
    maxPointController.text = "";
    desController.text = "";
  }

  Future<bool> createSubject({
    required String title,
    required String maxPoint,
    required String des, // Make sure this can be an empty string
  }) async {
    isLoading = true;
    update();
    try {
      bool result = await _subjectRepo.createSubject(
          title: title,
          maxPoint: maxPoint,
          des: des.isEmpty ? '' : des); // Ensure empty strings are handled
      isLoading = false;
      return result;
    } catch (e) {
      print("Error create subject in catch in con: $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<void> getListSubject() async {
    isLoading = true;
    update();
    try {
      subjectData = await _subjectRepo.getListSubject(
          search: searchQuery, page: currentPage, limit: limit);
      totalPages = subjectData.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      print("-------Error during get position : $e");
    }
  }

  Future<bool> updateSubject({
    required String subId,
    required String title,
    required String maxPoint,
    required String des,
    required String updatedBy,
  }) async {
    try {
      bool result = await _subjectRepo.updateSubject(
          subId: subId,
          title: title,
          maxPoint: maxPoint,
          des: des,
          updatedBy: updatedBy);
      return result;
    } catch (e) {
      print("Error in catch in the con $e");
      return false;
    }
  }

  Future<bool> deleteSubject({
    required String subId,
  }) async {
    try {
      bool result = await _subjectRepo.deleteSubject(
          subId: subId, deleteBy: userInfo!.id.toString());
      return result;
    } catch (e) {
      print("Error in catch in the con $e");
      return false;
    }
  }

  void setPage(int page) {
    currentPage = page;
    getListSubject();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getListSubject();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getListSubject();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getListSubject();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getListSubject();
    update();
  }
}
