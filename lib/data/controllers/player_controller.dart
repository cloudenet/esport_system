import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/category_player.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/data/repo/player_repo.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/player_section/screen/get_list_player.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'home_controller.dart';



class PlayerController extends GetxController implements GetxService {
  final PlayerRepo _playerRepo = PlayerRepo();
  var teamController = Get.find<TeamController>();
  var positionController = Get.find<PositionController>();
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();


  TextEditingController assignUserIdController = TextEditingController();
  TextEditingController userIdController = TextEditingController();
  TextEditingController teamIdController = TextEditingController();
  TextEditingController positionIdController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController joinDateController = TextEditingController();
  TextEditingController previousTeamController = TextEditingController();
  TextEditingController jerseyNumberController = TextEditingController();
  TextEditingController contractStartDateController = TextEditingController();
  TextEditingController contractEndDateController = TextEditingController();
  TextEditingController salaryController = TextEditingController();
  TextEditingController goalsScoredController = TextEditingController();
  TextEditingController assistsController = TextEditingController();
  TextEditingController matchesPlayedController = TextEditingController();
  TextEditingController yellowCardsController = TextEditingController();
  TextEditingController redCardsController = TextEditingController();
  TextEditingController injuriesController = TextEditingController();
  TextEditingController fileBioController = TextEditingController();

  List<CategoryPlayer> playerCategories = [];
  final List<String> playerSubCategories = ['1 x 1', '2 x 2', '3 x 3', 'shooting'];
  List<PlayerModel> dataPlayer = [];
  List<PlayerModel> dataPlayerForDropdownSearch = [];
  List<PlayerModel> filterPlayer = [];
  ListUserModel? previousSelectedUser ;
  PlayerModel? previousPlayer;
  String? previousPlayerID;
  bool isLoading = false;

  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  XFile? fileBioPlayer ;
  String? selectedUserId;
  String? selectTeamId;
  String? selectPositionId;
  String? selectedPlayerCate;

  void getOldValueUpdate(PlayerModel data){
    print("-------------------------1");
    if(data.playerCateId!.isNotEmpty){
      selectedPlayerCate = playerCategories.firstWhere(
            (category) => category.id == data.playerCateId,
      ).id;
    }
    if(data.teamId!.isNotEmpty){
      selectTeamId = teamController.listTeam.firstWhere(
            (v) => v.id == data.teamId,
      ).id;
    }
    if(data.positionId!.isNotEmpty){
      selectPositionId =positionController.positionData.firstWhere(
            (v) => v.id == data.position!.id,
      ).id;
    }
    if(data.id!.isNotEmpty){
      previousPlayer = dataPlayer.firstWhere((player) => player.id == data.id);
      previousPlayerID = dataPlayer.firstWhere((player) => player.id == data.id).id;
    }
    if(data.fileBioPlayer!.isNotEmpty){
      fileBioPlayer = XFile(data.fileBioPlayer.toString());
    }

    teamIdController.text = data.teamId?? "N/A";
    positionIdController.text = data.positionId?? "N/A";
    heightController.text = data.height?? "N/A";
    weightController.text = data.weight?? "N/A";
    joinDateController.text = data.joinDate?? "N/A";
    jerseyNumberController.text = data.jerseyNumber?? "N/A";
    previousTeamController.text = data.previousTeam?? "N/A";
    contractStartDateController.text = data.contractStartDate?? "N/A";
    contractEndDateController.text = data.contractEndDate?? "N/A";
    salaryController.text = data.salary?? "N/A";
    matchesPlayedController.text = data.matchesPlayed?? "N/A";
    fileBioController.text = data.fileBioPlayer.toString();
  }

  void clearValueTextField(){
    print("-------------------------2");
    teamIdController.text =  "";
    positionIdController.text = "";
    heightController.text = "";
    weightController.text = "";
    joinDateController.text = "";
    jerseyNumberController.text = "";
    previousTeamController.text = "";
    contractStartDateController.text = "";
    contractEndDateController.text = "";
    salaryController.text = "";
    goalsScoredController.text = "";//
    assistsController.text = "";//
    matchesPlayedController.text = "";
    selectPositionId = null;
    selectTeamId = null;
    selectedUserId = null;
    selectedPlayerCate = null;
    fileBioPlayer = null;
    fileBioController.text = "";
    previousSelectedUser = null;
    // selectedPlayerCate = "";
    // selectTeamId = "" ;
    // selectedUserId = "";
    // selectPositionId = "";
    // fileBioPlayer = null ;
    // fileBioController.text = "";
    // previousPlayerID = "" ;
    // previousPlayer = null ;
    update();
  }


  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getPlayer();
  }

  Future<void> getCategoryPlayer()async {
    try{
      await _playerRepo.getCategoryPlayer().then((v){
        if(v.isNotEmpty){
          playerCategories = v ;
          update();
        }else{
          playerCategories = [] ;
          update();
        }
      });
    }catch(e){
      print("Error During Get Category Player $e");
    }
  }

  Future<void> getPlayer() async {
    isLoading = true;
    update();
    try {
      dataPlayer = await _playerRepo.getPlayer(
        limit: limit,
        page: currentPage,
        search: searchQuery
      );
      filterPlayer = dataPlayer;
      totalPages = dataPlayer.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      print("Error During Get Player $e");
      isLoading = false;
      update();
    }
  }

  Future<void> getPlayerDropdownSearch({required String? searchCat}) async {
    isLoading = true;
    update();
    try {
      dataPlayerForDropdownSearch = await _playerRepo.getPlayer(
          limit: "",
          page: "",
          search: searchCat??""
      );
      isLoading = false;
      update();
    } catch (e) {
      print("Error During Get Player search in controller $e");
      isLoading = false;
      update();
    }
  }

  Future<void> createPlayerV2()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _playerRepo.createPlayerV2(
          assignUserId: selectedUserId.toString(),
          teamId: selectTeamId.toString(),
          positionId: selectPositionId.toString(),
          height: heightController.text,
          weight: weightController.text,
          joinDate: joinDateController.text,
          contractStartDate: contractStartDateController.text,
          contractEndDate: contractEndDateController.text,
          previousTeam: previousTeamController.text,
          salary: salaryController.text,
          matchesPlayed: matchesPlayedController.text,
          playerCategoryId: selectedPlayerCate.toString(),
          bioPlayer: fileBioPlayer
      ).then((v){
            if(v['success']){
              EasyLoading.showSuccess(
                'player_create_successfully'.tr,
                dismissOnTap: true,
                duration: const Duration(milliseconds: 1000),
              );
              clearValueTextField();
              homeController.updateWidgetHome(const ListPlayerScreen());
            }else{
              EasyLoading.showError("Failed to create player ${v['message']}",
                duration: const Duration(seconds: 2),
              );
            }
            EasyLoading.dismiss();
            update();
      }
      );
    }catch(e){
      EasyLoading.showError('Errors create in catch',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  Future<void> updatePlayerV2()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _playerRepo.updatePlayerV2(
          playerId: previousPlayerID.toString(),
          teamId: selectTeamId.toString(),
          positoinId: selectPositionId.toString(),
          height: heightController.text,
          weight: weightController.text,
          joinDate: joinDateController.text,
          previousTeam: previousTeamController.text,
          contractStartDate: contractStartDateController.text,
          contractEndDate: contractEndDateController.text,
          salary: salaryController.text,
          matchesPlay: matchesPlayedController.text,
          playerCatId: selectedPlayerCate.toString(),
          bioPlayer: fileBioPlayer
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'player_updated_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearValueTextField();
          homeController.updateWidgetHome(const ListPlayerScreen());
        }else{
          EasyLoading.showError("Failed to updated player ${v['message']}",
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      }
      );
    }catch(e){
      EasyLoading.showError('Errors update in catch',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  Future<Map<String, dynamic>> deletePlayer({required String playerId}) async {
    isLoading = true;
    update();
    var result = await _playerRepo.deletePlayer(playerID: playerId);
    isLoading = false;
    update();
    return result;
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getPlayer();
    update();
  }
  void setPage(int page) {
    currentPage = page;
    getPlayer();
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getPlayer();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getPlayer();
    }
  }

  Future<void> selectDate(
      BuildContext context, TextEditingController controller) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );

    if (picked != null) {
      controller.text = DateFormat('yyyy-MM-dd').format(picked);
    }
  }

  void pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf', 'doc', 'docx'], // Limit to specific file types if needed
    );
    if (result != null && result.files.isNotEmpty) {
      fileBioController.text = result.files.single.name.toString();
      fileBioPlayer = XFile(result.files.single.name);
      print("---------All : $result");
      print("---------single.name : ${result.files.single.name}");
    }
    update();
  }

  void launchDocument(Uri url) async {
    if (await canLaunchUrl(url)) {
      await launchUrl(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}
