
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/arbitrator_model.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/data/repo/arbitrator_repo.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../views/arbitrator_section/get_arbitrator_screen.dart';
import 'home_controller.dart';

class ArbitratorController extends GetxController implements GetxService {
  final ArbitratorRepo _arbitratorRepo = ArbitratorRepo();
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();

  List<ArbitratorModel> listArbitrator = [];
  TextEditingController experienceController = TextEditingController();
  TextEditingController trainingDateController = TextEditingController();
  TextEditingController userNameController = TextEditingController();

  String? selectedUserId;
  ListUserModel? selectedUser;
  String? selectedArbitratorRank;
  bool isLoading = true;
  bool isFirstFilePick = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  List<PlatformFile>? certificateFiles = [];


  TextEditingController fileBioArbitratorController = TextEditingController();
  TextEditingController certificateController = TextEditingController();
  XFile? fileBioArbitrator ;
  XFile? certificateArbitrator ;

  void pickFile({required bool bio}) async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.any,
    );
    if (result != null && result.files.isNotEmpty) {
      if(bio){
        fileBioArbitratorController.text = result.files.single.name.toString();
        fileBioArbitrator = XFile(result.files.single.name);
      }else{
        certificateController.text = result.files.single.name.toString();
        certificateArbitrator = XFile(result.files.single.name);
      }
    }
    update();
  }

  void clearData(){
    //selectedUserId = null ;
    selectedUser = null;
    selectedArbitratorRank = null ;
    experienceController.text = "" ;
    trainingDateController.text = "";
    fileBioArbitrator = null ;
    certificateFiles = null ;
  }

  void loadPreviousData(ArbitratorModel? data){

    if(data!.certificate != null){
      addPreviousFiles(data.certificate!);
    }
    if (data.bioFile != null && data.bioFile!.isNotEmpty) {
      fileBioArbitrator = XFile(data.bioFile!.split('/').last);
      fileBioArbitratorController.text = fileBioArbitrator!.path;
      print('File exists: ${fileBioArbitrator?.path}');
    }
    selectedUser = userController.listAllUser.firstWhere((user) =>
    user.id.toString() == data.userId!.id.toString(),);
    selectedUserId = data.userId!.id.toString();

    selectedArbitratorRank = data.rankModel?.id ?? "N/A";
    trainingDateController.text = data.trainingDate ?? "N/A";
    experienceController.text = data.exYear ?? "N/A";

  }

  Future<List<PlatformFile>?> pickCertificateFiles() async {
    if (isFirstFilePick) {
      certificateFiles?.clear();
      isFirstFilePick = false;
      update();
    }
    FilePickerResult? certificateFilesResult = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.any,
    );

    if (certificateFilesResult != null) {
      certificateFiles?.addAll(certificateFilesResult.files);
      update();
      return certificateFilesResult.files;
    } else {
      return null;
    }
  }
  void resetFilePickState() {
    isFirstFilePick = true;
    update();
  }

  void removeCertificateFile(String? file ,int index) {
    print("file index remove : $file -- $index");
    if (index >= 0 && index < certificateFiles!.length) {
      certificateFiles!.removeAt(index);
      update();
    }
  }

  void addPreviousFiles(List<String> serverFiles) {
    certificateFiles = [];
    for (String filePath in serverFiles) {
      String fileName = filePath.split('/').last;
      certificateFiles?.add(PlatformFile(
        name: fileName,
        path: filePath,
        size: 0,
      ));
      for (var file in certificateFiles!) {
        debugPrint('Picked file ctr: ${file.name} -- length ${certificateFiles!.length}');
      }
    }
    update(); // Notify listeners or update the UI
  }




  Future<void> getArbitrator() async {
    isLoading = true;
    update();
    try {
      listArbitrator = await _arbitratorRepo.getArbitrator(search: searchQuery, page: currentPage, limit: limit);
      totalPages = listArbitrator.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      print("-------Error during get position : $e");
    }
  }

  Future<void> createArbitrator() async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try {
      await _arbitratorRepo.createArbitrator(
          assignUserID: selectedUserId.toString(),
          rank: selectedArbitratorRank.toString(),
          expYear: experienceController.text,
          trainingDate: trainingDateController.text,
          bioFile: fileBioArbitrator,
          certificateFiles: certificateFiles!
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess('referee_created_successfully'.tr,dismissOnTap: true,duration: const Duration(seconds: 2),);
          clearData();
          homeController.updateWidgetHome(const GetArbitratorScreen());
        }else{
          EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
        update();
      });
    } catch (e) {
      EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
    }
  }

  Future<void> updateArbitrator({
    required String arbitratorsID,
  }) async {
    try {
      EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
      await _arbitratorRepo.updateArbitrator(
          arbitratorsID: arbitratorsID,
          experienceYear: experienceController.text,
          rankID: selectedArbitratorRank.toString(),
          trainingDate: trainingDateController.text,
          bioFile: fileBioArbitrator,
          certificateFiles: certificateFiles!
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess('referee_updated_successfully'.tr,dismissOnTap: true,duration: const Duration(seconds: 2),);
          clearData();
          homeController.updateWidgetHome(const GetArbitratorScreen());
        }else{
          EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
        update();
      });
    } catch (e) {
      EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
    }
  }

  Future<void> deleteArbitrator({
    required String arbitratorsID
}) async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
       await _arbitratorRepo.deleteArbitrator(arbitratorsID: arbitratorsID).then((v){
         if(v){
           EasyLoading.showSuccess('arbitrator_deleted_successfully'.tr,
           dismissOnTap: true,duration: const Duration(seconds: 2),);
           Get.back();
           getArbitrator();
           update();
         }else{
           EasyLoading.showError('referee_delete_false'.tr,
           duration: const Duration(seconds: 2));
         }
         EasyLoading.dismiss();
         update();
       });

    }catch(e){
      print("Arbitrator deleted false :$e");
      EasyLoading.showError('referee_delete_false'.tr,
      duration: const Duration(seconds: 2));
      EasyLoading.dismiss();
      update();
    }
  }

  void setPage(int page) {
    currentPage = page;
    getArbitrator();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getArbitrator();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getArbitrator();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getArbitrator();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getArbitrator();
    update();
  }
}
