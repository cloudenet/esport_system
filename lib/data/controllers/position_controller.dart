
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/model/position_model.dart';
import 'package:esport_system/data/repo/position_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../views/position_section/get_position_screen.dart';

class PositionController extends GetxController implements GetxService {
  final PositionRepo _positionRepo = PositionRepo();
  var homeController = Get.find<HomeController>();

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  List<PositionModel> positionData = [];

  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';

  void clearTextField(){
    titleController.text = "" ;
    descriptionController.text = "" ;
  }

  Future<void> createPosition() async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try{
      await _positionRepo.createPosition(
          title: titleController.text.toString(),
          description: descriptionController.text.toString()
      ).then((a){
        if(a['success']){
          EasyLoading.showSuccess('position_created_successfully'.tr,
            dismissOnTap: true, duration: const Duration(seconds: 2),
          );
          clearTextField();
          homeController.updateWidgetHome(const GetPositionScreen());
        }else{
          EasyLoading.showError(
            a['message'], duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });

    }catch(e){
      EasyLoading.showError('Please correct the errors in the form', duration: const Duration(seconds: 2),);
      EasyLoading.dismiss();
    }
  }

  Future<void> getPosition() async {
    isLoading = true;
    update();
    try {
      positionData = await _positionRepo.getPosition(
          search: searchQuery, page: currentPage, limit: limit);
      totalPages = positionData.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      print("-------Error during get position : $e");
    }
  }

  void setPage(int page) {
    currentPage = page;
    getPosition();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getPosition();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getPosition();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getPosition();
  }

  Future<void> updatePosition({
    required String positionId,

  }) async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try {
      await _positionRepo.updatePosition(
          positionId: positionId,
          title: titleController.text.toString(),
          description: descriptionController.text.toString()
      ).then((v){
        if(v){
          EasyLoading.showSuccess('position_updated_successfully'.tr,
            dismissOnTap: true, duration: const Duration(seconds: 2),
          );
          clearTextField();
          homeController.updateWidgetHome(const GetPositionScreen());
        }else{
          EasyLoading.showError(
            'failed_update_position'.tr, duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    } catch (error) {
      EasyLoading.showError('Please correct the errors in the form', duration: const Duration(seconds: 2),);
      EasyLoading.dismiss();
    }
  }

  Future<Map<String, dynamic>> deletePosition({required String positionId}) async {
    isLoading = true;
    update();
    var result = await _positionRepo.deletePosition(positionId: positionId);
    isLoading = false;
    update();
    return result;
  }
  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getPosition();
    update();
  }
}
