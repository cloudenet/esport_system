import 'package:esport_system/data/controllers/arbitrator_controller.dart';
import 'package:esport_system/data/model/coach_data_model.dart';
import 'package:esport_system/data/model/coach_model.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/data/repo/coach_repo.dart';
import 'package:esport_system/views/assistant/get_list_assistant.dart';
import 'package:esport_system/views/coach_section/get_coach.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_disposable.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:image_picker/image_picker.dart';

import 'home_controller.dart';

class CoachController extends GetxController implements GetxService {
  final CoachRepo _coachRepo = CoachRepo();
  var homeController = Get.find<HomeController>();

  TextEditingController experienceYearController = TextEditingController();
  TextEditingController teamIdController = TextEditingController();
  TextEditingController teamNameController = TextEditingController();
  TextEditingController coachNameController = TextEditingController();
  TextEditingController specialtyController = TextEditingController();
  TextEditingController hireDateController = TextEditingController();
  TextEditingController joinDateController = TextEditingController();
  TextEditingController contractStartDateController = TextEditingController();
  TextEditingController contractEndDateController = TextEditingController();
  TextEditingController salaryController = TextEditingController();
  TextEditingController championshipWonController = TextEditingController();
  TextEditingController matchesWonController = TextEditingController();
  TextEditingController matchesLostController = TextEditingController();
  TextEditingController matchesDrawnController = TextEditingController();
  TextEditingController fileBioController = TextEditingController();

  List<CoachModel> getCoachData = [];
  List<CoachDataModel> getListCoach = [];
  List<CoachDataModel> getListAssistant = [];
  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  XFile? fileBioCoach;
  ListUserModel? previousSelectedUser ;
  CoachDataModel? previousCoach;
  CoachDataModel? previousAssistant;
  String? previousCoachId;

  String? selectedUserId;
  String? selectedTeamId;
  String? selectRankId;
  String selectedCoachType = 'none';


  final List<Map< String,String>> coachType = [
    {
      'value': 'none', 'text': 'None'
    },
    {
      'value': 'provinces', 'text': 'Provinces'
    },
    {
      'value': 'international', 'text': 'International'
    },
    {
      'value': 'national', 'text': 'National'
    },
  ];
  void oldValueToUpdate(CoachDataModel data) async {

    if(data.id!.isNotEmpty && data.type == "coach"){
      previousCoach = getListCoach.firstWhere((coach) => coach.id == data.id);
      previousCoachId = getListCoach.firstWhere((coach) => coach.id == data.id).id;
    }else{
      previousAssistant = getListAssistant.firstWhere((assistant) => assistant.id == data.id);
    }
    coachNameController.text = data.user?.name ?? "N/A";
    selectRankId = data.rank!.id;
    selectedTeamId = data.team!.id ?? "N/A";
    experienceYearController.text = data.experienceYears ?? "N/A";
    specialtyController.text = data.specialty ?? "N/A";
    hireDateController.text = data.hireDate ?? "N/A";
    joinDateController.text = data.joinDate ?? "N/A";
    salaryController.text = data.salary ?? "N/A";
    championshipWonController.text = data.championshipsWon ?? "N/A";
    fileBioController.text = data.biographyFile!;
  }

  void clearValue() {
    coachNameController.text = "";
    selectedUserId = null;
    selectRankId = null;
    selectedTeamId = null;
    experienceYearController.text = "";
    specialtyController.text = "";
    hireDateController.text = "";
    contractStartDateController.text = "";
    contractEndDateController.text = "";
    salaryController.text = "";
    championshipWonController.text = "";
    matchesWonController.text = "";
    matchesLostController.text = "";
    fileBioCoach = null;
    fileBioController.text = "";
    joinDateController.text = "";
    selectedCoachType = 'none';
  }

  Future<Map<String, dynamic>> createCoach({
    required String assignUserId,
    required String teamId,
    required String experienceYear,
    required String specialty,
    required String hireDate,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String championshipWon,
    required String matchesWon,
    required String matchesLost,
    required String matchesDrawn,
    required String biography,
    required String userId,
  }) async {
    isLoading = true;
    update();
    var result = await _coachRepo.createCoach(
      assignUserId: assignUserId,
      teamId: teamId,
      experienceYear: experienceYear,
      specialty: specialty,
      hireDate: hireDate,
      contractStartDate: contractStartDate,
      contractEndDate: contractEndDate,
      salary: salary,
      championshipWon: championshipWon,
      matchesWon: matchesWon,
      matchesLost: matchesLost,
      matchesDrawn: matchesDrawn,
      biography: biography,
      userId: userId,
    );
    isLoading = false;
    update(); // Trigger UI update
    return result;
  }

  Future<void> createCoachV2() async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);

    try {
      final response = await _coachRepo.createCoachV2(
        assignUserId: selectedUserId.toString(),
        teamId: selectedTeamId.toString(),
        experienceYear: experienceYearController.text,
        specialty: specialtyController.text,
        hireDate: hireDateController.text,
        startDate: contractStartDateController.text,
        endDate: contractEndDateController.text,
        salary: salaryController.text,
        championshipsWon: championshipWonController.text,
        matchesWon: matchesWonController.text,
        matchesLost: matchesLostController.text,
        rankId: selectRankId.toString(),
        bioFile: fileBioCoach,
      );

      if (response['success']) {
        EasyLoading.showSuccess(
          'coach_created_successfully'.tr,
          dismissOnTap: true,
          duration: const Duration(milliseconds: 1000),
        );
        clearValue();
        homeController.updateWidgetHome(const GetCoachScreen());
      } else {
        EasyLoading.showError(
          "Failed to create coach: ${response['message']}",
          duration: const Duration(seconds: 2),
        );
      }
    } catch (error) {
      EasyLoading.showError(
        'Error creating coach: $error',
        duration: const Duration(seconds: 2),
      );
    } finally {
      EasyLoading.dismiss();
      update();
    }
  }

  Future<bool> updateCoachV2(
      {required String coachId,
      required String rankId,
      required String teamId,
      required String experienceYear,
      required String specialty,
      required String hireDate,
      required String startDate,
      required String endDate,
      required String salary,
      required String championshipWon,
      required String matchWon,
      required String matchLost,
      required XFile? bioFile}) async {
    isLoading = true;
    update();
    try {
      bool result = await _coachRepo.updateCoachV2(
          coachId: coachId,
          rankId: rankId,
          teamId: teamId,
          experienceYear: experienceYear,
          specialty: specialty,
          hireDate: hireDate,
          startDate: startDate,
          endDate: endDate,
          salary: salary,
          championshipWon: championshipWon,
          matchWon: matchWon,
          matchLost: matchLost,
          bioFile: bioFile);
      isLoading = false;
      return result;
    } catch (e) {
      print("Error update in con $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<void> getCoachX() async {
    isLoading = true;
    update();
    try {
      getCoachData = await _coachRepo.getCoach(page: currentPage, limit: limit, search: searchQuery);
      totalPages = getCoachData.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (error) {
      print("-------Error during get coach : $error");
    }
  }

  Future<void> fetchListCoach({required String typeCoach})async{
    isLoading = true;
    update();
    try{
      getListCoach = await _coachRepo.getListCoachAndAssistant(
        typeCoach: typeCoach,
        coachName: searchQuery,
        page: currentPage,
        limit: limit,
      );
      totalPages = getListCoach.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    }catch(e){
      print("Error $e");
    }finally{
      isLoading = false;
      update();
    }
  }

  Future<void> fetchListAssistant({required String typeCoach})async{
    isLoading = true;
    update();
    try{
      getListAssistant = await _coachRepo.getListCoachAndAssistant(
        typeCoach: typeCoach,
        coachName: searchQuery,
        page: currentPage,
        limit: limit,
      );
      totalPages = getListAssistant.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    }catch(e){
      print("Error $e");
    }finally{
      isLoading = false;
      update();
    }
  }



  Future<Map<String, dynamic>> updateCoach({
    required String coachId,
    required String teamId,
    required String experienceYear,
    required String specialty,
    required String hireDate,
    required String contractStartDate,
    required String contractEndDate,
    required String salary,
    required String championshipsWon,
    required String matchesWon,
    required String matchesLost,
    required String matchesDrawn,
    required String biography,
  })
  async {
    isLoading = true;
    update();
    var result = await _coachRepo.updateCoach(
        coachId: coachId,
        teamId: teamId,
        experinceYear: experienceYear,
        specialty: specialty,
        hireDate: hireDate,
        contractStartDate: contractStartDate,
        contractEndDate: contractEndDate,
        salary: salary,
        championhshipsWon: championshipsWon,
        matchesWon: matchesWon,
        matchesLost: matchesLost,
        matchesDrawn: matchesDrawn,
        biography: biography);
    isLoading = false;
    update();
    return result;
  }

  void setPage(int page) {
    currentPage = page;
    //getCoach();
    fetchListCoach(typeCoach: 'coach');
  }

  Future<void> deleteCoach({required coachId}) async {
    try {
      isLoading = true;
      update();
      await _coachRepo.deleteCoach(coachId: coachId);
      isLoading = false;
      update();
    } catch (error) {
      print("Error deleting coach $error");
      isLoading = false;
      update();
    }
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      fetchListCoach(typeCoach: 'coach');
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      fetchListCoach(typeCoach: 'coach');
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    fetchListCoach(typeCoach: 'coach');
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    fetchListCoach(typeCoach: 'coach');
    update();
  }

  void pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: [
        'pdf',
        'doc',
        'docx'
      ],
    );

    if (result != null && result.files.isNotEmpty) {
      fileBioController.text = result.files.single.name.toString();
      fileBioCoach = XFile(result.files.single.name);
      print("Coach file ${fileBioCoach.toString()}");
    }
    update();
  }
  
  
  Future<void> assignCoachAndAssistant({required String type})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _coachRepo.assignCoachAndAssistant(
          assignUserId: selectedUserId.toString(),
          teamId: selectedTeamId.toString(),
          rankId: selectRankId.toString(),
          experienceYears: experienceYearController.text,
          specialty: specialtyController.text,
          joinDate: joinDateController.text,
          salary: salaryController.text,
          championshipsWon: championshipWonController.text,
          coachesType: selectedCoachType,
          type: type,
          biographyCoaches: fileBioCoach
      ).then((v){
        if (v['success']) {
          EasyLoading.showSuccess(
            'coach_created_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearValue();
          if(type == "coach"){
            homeController.updateWidgetHome(const GetCoachScreen());
          }else{
            homeController.updateWidgetHome(const GetAssistantScreen());
          }
        } else {
          EasyLoading.showError(
            "Failed to create coach: ${v['message']}",
            duration: const Duration(seconds: 2),
          );
        }
      });
    }catch(e){
      EasyLoading.showError(
        'Error creating coach: $e',
        duration: const Duration(seconds: 2),
      );
    }finally{
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> updateAssignCoachAndAssistant({required String id,required String type})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _coachRepo.updateAssignCoachAndAssistant(
          id: id,
          assignUserId: selectedUserId.toString(),
          teamId: selectedTeamId.toString(),
          rankId: selectRankId.toString(),
          experienceYears: experienceYearController.text,
          specialty: specialtyController.text,
          joinDate: joinDateController.text,
          salary: salaryController.text,
          championshipsWon: championshipWonController.text,
          coachesType: coachNameController.text,
          type: type,
          biographyCoaches: fileBioCoach,
      ).then((v){
        if (v['success']) {
          EasyLoading.showSuccess(
            'coach_created_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearValue();
          if(type=='coach'){
            homeController.updateWidgetHome(const GetCoachScreen());
          }else{
            homeController.updateWidgetHome(const GetAssistantScreen());
          }
        } else {
          EasyLoading.showError(
            "Failed to create coach: ${v['message']}",
            duration: const Duration(seconds: 2),
          );
        }
      });
    }catch(e){
      EasyLoading.showError(
        'Error creating coach: $e',
        duration: const Duration(seconds: 2),
      );
    }finally{
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> deletedCoachAndAssistant({required String id,required String type})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _coachRepo.deleteAssignCoachAndAssistant(
          type: type,
          coachId: id
      ).then((v){
        if (v['success']) {
          if(type=='coach'){
            EasyLoading.showSuccess(
              'coach_deleted_successfully'.tr,
              dismissOnTap: true,
              duration: const Duration(milliseconds: 1000),
            );
            clearValue();
            homeController.updateWidgetHome(const GetCoachScreen());
            fetchListCoach(typeCoach: 'coach');
          }else{
            EasyLoading.showSuccess(
              'assistant_deleted_successfully'.tr,
              dismissOnTap: true,
              duration: const Duration(milliseconds: 1000),
            );
            clearValue();
            homeController.updateWidgetHome(const GetAssistantScreen());
            fetchListAssistant(typeCoach: 'assistant');
          }
        } else {
          EasyLoading.showError(
            "failed_deleted".tr,
            duration: const Duration(seconds: 2),
          );
        }
      });
    }catch(e){
      EasyLoading.showError(
        'failed_deleted'.tr,
        duration: const Duration(seconds: 2),
      );
    }finally{
      EasyLoading.dismiss();
      update();
    }
  }

}
