

import 'package:esport_system/data/repo/pitch_repo.dart';
import 'package:esport_system/data/repo/team_repo.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/pitch_section/pitch_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import '../model/address_model.dart';
import '../model/pitch_model.dart';
import '../model/team_model.dart';
import 'home_controller.dart';

class PitchController extends GetxController implements GetxService{

  var pitchRepo = PitchRepo();
  var teamRepository = TeamRepository();
  var homeController = Get.find<HomeController>();


  TextEditingController teamNameController = TextEditingController();
  TextEditingController amountPitchController = TextEditingController();
  TextEditingController amountTableController = TextEditingController();
  TextEditingController requesterNameController = TextEditingController();
  TextEditingController requesterPhoneController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController noteController = TextEditingController();
  TextEditingController matchDateController = TextEditingController();


  String? searchQuery = "" ;
  String? searchStatus = "" ;

  List<PitchModel> listReqPitch = [] ;
  List<TeamModel> listTeam = [];
  List<Address> provinces = [];
  Address? selectedProvince;
  int currentPage = 1;
  int totalPages = 1;
  int totalItems = 0;
  int limit = 6;
  bool isLoading = false ;
  bool isLoadingProvinces = false;


  String? selectedValue;
  final List<String> dropdownItems = ['approve'.tr, 'reject'.tr, 'pending'.tr];

  void selectDropdownValue(String value){
    selectedValue = value;
    update();
  }

  void isWait(value){
    isLoading = value;
    update();
  }

  void setPage(int page) {
    currentPage = page;
    getReqPitch();
    update();
  }
  void nextPage() {
    print("----------currentPage $currentPage");
    if (currentPage < totalPages) {
      currentPage++;
      getReqPitch();
    }
  }

  Future<void> getTeam() async {
    isWait(true);
    try{
      pitchRepo.getTeams().then((data){
        if(data.isNotEmpty){
          listTeam = data ;
          isWait(false);
        }
      });
    }catch(e){
      isWait(false);
      print("issue in pitch controller $e");
    }
  }

  void fetchProvinces() async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await teamRepository.getAddresses();
      provinces.assignAll(
          result.where((address) => address.type == 'province').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void previousPage() {
    print("----------currentPage $currentPage");
    if (currentPage > 1) {
      currentPage--;
      getReqPitch();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    print("----------newLimit $newLimit");
    update();
    getReqPitch();
  }

  String? selectedTeamId ;
  void getOldValue(PitchModel pitchData){
    //selectedTeamId = pitchData.teamId;
    selectedTeamId = pitchData.teamId ?? listTeam.first.id;
    teamNameController.text = pitchData.team?.teamName.toString() ?? "";
    amountPitchController.text = pitchData.numOfPitches.toString() ;
    amountTableController.text = pitchData.numOfTables.toString() ;
    requesterNameController.text = pitchData.requesterName.toString();
    requesterPhoneController.text = pitchData.requesterPhone.toString();
    locationController.text = pitchData.locationPitches.toString();
    matchDateController.text = pitchData.requestDate.toString();
    noteController.text  = pitchData.remark.toString();
    selectProvince(Address(
      code: pitchData.province!.code,
      name: pitchData.province!.name,
    ));
    update();
  }

  void clearAllTextController(){
    selectedTeamId = null ;
    selectedProvince = null;
    teamNameController.text =  "";
    amountPitchController.text =  "" ;
    amountTableController.text = "" ;
    requesterNameController.text =  "";
    requesterPhoneController.text =  "";
    locationController.text =  "";
    noteController.text  =  "";
    matchDateController.text = "";
    update();
  }

  void search(String query) {
    searchQuery = query;
    searchStatus = query;
    getReqPitch();
  }
  Future<void> getReqPitch()async {
    isWait(true);
    await pitchRepo.getListRequestPitch(
      searchTerm: searchQuery,
      r_status: searchStatus,
      limit: limit,
      page: currentPage,
    ).then((result){
      if(result.isNotEmpty){
        listReqPitch = result ;
        totalItems = result.length;
        totalPages = (totalItems / limit).ceil();
        isWait(false);
      }else{
        listReqPitch = [];
        isWait(false);
      }
    });
  }
  void selectProvince(Address? province) {
    selectedProvince = province;
    print("Select province ${selectedProvince!.name}---Code ${selectedProvince!.code}");
    update();
  }

  Future<void> requestPitch()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
    try{
      await pitchRepo.requestPitch(
          proId: selectedProvince!.code,
          teamId: selectedTeamId,
          amountPitch: amountPitchController.text,
          name: requesterNameController.text,
          phone: requesterPhoneController.text,
          amountTable: amountTableController.text,
          location: locationController.text,
          matchDate: matchDateController.text,
          note: noteController.text,
      ).then((status){
        if(status){
          EasyLoading.showSuccess(
            'request_pitch_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          clearAllTextController();
          homeController.updateWidgetHome(const ListPitch());
        }else{
          EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
          clearAllTextController();
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
      clearAllTextController();
      print("issue in pitch controller $e");
    }
  }

  Future<void> updateRequestPitch({required String rqPitchID})async{
    EasyLoading.show(status: 'Loading...'.tr, dismissOnTap: true);
    try{
      // print("------> ${selectedProvince!.name}----${selectedProvince!.code}");
      // print("------> ${selectedTeamId!}");
      // print("------> ${amountClubController.text}");
      // print("------> ${requesterNameController.text}");
      // print("------> ${requesterPhoneController.text}");
      // print("------> ${placeAmountController.text}");
      // print("------> ${placeController.text}");
      // print("------> ${remarkController.text}");
      await pitchRepo.updateRequestPitch(
          rqPitchID: rqPitchID,
          provinceID: selectedProvince!.code.toString(),
          teamID: selectedTeamId.toString(),
          amountPitch: amountPitchController.text,
          rqName: requesterNameController.text,
          rqPhone: requesterPhoneController.text,
          location: locationController.text,
          amountTable: amountTableController.text,
          matchDate: matchDateController.text,
          remark: noteController.text,
      ).then((status){
        if(status){
          EasyLoading.showSuccess(
            'update_request_pitch_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          clearAllTextController();
          homeController.updateWidgetHome(const ListPitch());
        }else{
          EasyLoading.showError('error_can_not_update'.tr, duration: const Duration(seconds: 2));
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
      clearAllTextController();
      print("issue in pitch controller $e");
    }
  }

  Future<void> deleteRequest({required String rqId})async{
    EasyLoading.show(status: 'Loading...'.tr, dismissOnTap: true);
    try{
      await pitchRepo.deleteRequest(rqId: rqId ).then((e){
        if(e){
          EasyLoading.showSuccess(
            'deleted_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
           getReqPitch();
        }else{
          EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
          print("issue deleted in pitch controller");
        }
      });
      update();
    }catch(e){
      EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
      print("issue in pitch controller $e");
    }
  }

  Future<void> approveRequestPitch({required String rqPitchID})async{
    EasyLoading.show(status: 'Loading...'.tr, dismissOnTap: true);
    try{
      await pitchRepo.approveRequestPitch(rqPitchID:rqPitchID ).then((value){
        if(value){
          EasyLoading.showSuccess(
            'approved_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(seconds: 2),
          );
          homeController.updateWidgetHome(const ListPitch());
        }else{
          EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
          print("issue deleted in pitch controller");
        }
      });
    }catch(e){
      EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
      print("issue in pitch controller $e");
    }
  }

}