


import 'package:esport_system/data/model/category_model.dart';
import 'package:esport_system/data/repo/categories_repo.dart';
import 'package:esport_system/views/categories/list_categories_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../views/home_section/screen/home.dart';
import 'home_controller.dart';

class CategoryController extends GetxController implements GetxService{

  final _cateRepo = CategoriesRepo();
  var homeController = Get.find<HomeController>();


  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController fromAgeController = TextEditingController();
  TextEditingController toAgeController = TextEditingController();

  bool isLoading = false;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  List<CategoryModel> lisCategory = [];

  void isWaiting(value){
    isLoading = value ;
    update();
  }

  void setPage(int page) {
    currentPage = page;
    getCategory();
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getCategory();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getCategory();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getCategory();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getCategory();
    update();
  }


  Future<void> getCategory()async{
    isWaiting(true);
    try{
      await _cateRepo.getCategory(limit: limit,page: currentPage,searchTitle: searchQuery).then((v){
        if(v.isNotEmpty){
          lisCategory = v ;
          totalPages = lisCategory.length == limit ? currentPage + 1 : currentPage;
          isWaiting(false);
        }else{
          lisCategory = [];
          isWaiting(false);
        }
      });
      update();
    }catch(e){
      isWaiting(false);
      print("Error get category in controller $e");
    }
  }

  Future<void> createCategory()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _cateRepo.createCategory(
          title: titleController.text,
          subTitle: descriptionController.text,
          fromAge: fromAgeController.text,
          toAge: toAgeController.text
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'category_create_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          getCategory();
          homeController.updateWidgetHome(const ListCategoriesPlayer());
        }else{
          EasyLoading.showError("failed_to_create_request".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("failed_to_create $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }
  Future<void> updateCategory({required String pcId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _cateRepo.updateCategory(
          pcID: pcId,
          title: titleController.text,
          subTitle: descriptionController.text,
          fromAge: fromAgeController.text,
          toAge: toAgeController.text,
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'category_update_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          getCategory();
          homeController.updateWidgetHome(const ListCategoriesPlayer());
        }else{
          EasyLoading.showError("failed_to_update_request".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("Failed to update $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> deletedCategory({required String pcId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _cateRepo.deletedCategory(
          pcId: pcId
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'deleted_category_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          homeController.updateWidgetHome(const ListCategoriesPlayer());
          getCategory();
        }else{
          EasyLoading.showError("failed_to_deleted_category".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("Failed to Deleted $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

}