import 'dart:io';

import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/scorekepper_model.dart';
import 'package:esport_system/data/repo/score_keeper_repo.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/score_keeper_section/get_list_scorekeeper.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import '../model/list_user_model.dart';
import 'home_controller.dart';

class ScoreKeeperController extends GetxController implements GetxService {
  final ScoreKeeperRepo _scoreKeeperRepo = ScoreKeeperRepo();
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();

  ListUserModel? previousSelectedUser;

  String? selectedUserId;
  String? selectRankId;
  TextEditingController experienceYearController = TextEditingController();
  TextEditingController trainingDateController = TextEditingController();
  TextEditingController bioFileController = TextEditingController();
  TextEditingController certificateController = TextEditingController();
  XFile? fileBio;
  List<PlatformFile>? certificateFiles = [];
  bool isLoading = false;
  bool isFirstFilePick = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  ScorekeeperModel? previousScorekeeper;
  String? previousScorekeeperId;
  List<ScorekeeperModel> listScorekeeper = [];


  void setPreviousValue(ScorekeeperModel? data)async{
    previousSelectedUser = userController.listAllUser.firstWhere((user) =>
    user.id.toString() == data!.userId!.id,);
    selectedUserId = data!.userId!.id;
    selectRankId = data.rankId!.id.toString();
    experienceYearController.text = data.exYear.toString();
    trainingDateController.text = data.trainingDate.toString();

    if(data.certificate != null ){
      addPreviousFiles(data.certificate!);
    }
    if (data.bioFile != null && data.bioFile!.isNotEmpty) {
      fileBio = XFile(data.bioFile!.split('/').last);
      bioFileController.text = fileBio!.path;
      print('File exists: ${fileBio?.path}');
    }
    update();
  }

  void addPreviousFiles(List<String> serverFiles) {
    certificateFiles = [];
    for (String filePath in serverFiles) {
      String fileName = filePath.split('/').last;
      certificateFiles?.add(PlatformFile(
        name: fileName,
        path: filePath,
        size: 0,
      ));
      for (var file in certificateFiles!) {
        debugPrint('Picked file ctr: ${file.name} -- length ${certificateFiles!.length}');
      }
    }
    update();
  }



  void cleaData(){
    selectedUserId = null;
    previousSelectedUser = null;
    selectRankId = null;
    experienceYearController.text = '';
    trainingDateController.text = '';
    fileBio = null;
    bioFileController.text = '';
    certificateFiles = [];
  }

  Future<void> createScoreKeeper() async {
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try {
      await _scoreKeeperRepo.createScoreKeeper(
          assignUserID: selectedUserId.toString(),
          rank: selectRankId.toString(),
          experienceYear: experienceYearController.text,
          trainingDate: trainingDateController.text,
          bioFile: fileBio,
          certificateFiles: certificateFiles!)
          .then((value) {
        if (value['success']) {
          EasyLoading.showSuccess(
            "scorekeeper_create_successfully".tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          cleaData();
          getListScorekeeper();
          homeController.updateWidgetHome(const GetListScorekeeper());
        } else {
          EasyLoading.showError(
            "failed_to_create_scorekeeper".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    } catch (e) {
      EasyLoading.showError(
        "Error create scorekeeper in con $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> updateScorekeeper({required String scorekeeperId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _scoreKeeperRepo.updateScorekeeper(
          scorekeeperId: scorekeeperId,
          rankId: selectRankId.toString(),
          exYears: experienceYearController.text,
          trainDate: trainingDateController.text,
          bioFile: fileBio,
          certificateFiles: certificateFiles!
      ).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'scorekeeper_updated_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          cleaData();
          getListScorekeeper();
          homeController.updateWidgetHome(const GetListScorekeeper());
        }else{
          EasyLoading.showError("failed_to_update_scorekeeper".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError("Failed to updated $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }

  Future<void> deleteScoreKeeper({required String scoreKeeperId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _scoreKeeperRepo.deleteScoreKeeper(scoreId: scoreKeeperId).then((v){
        if(v['success']){
          EasyLoading.showSuccess(
            'score_keeper_deleted_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          getListScorekeeper();
          homeController.updateWidgetHome(const GetListScorekeeper());
        }else{
          EasyLoading.showError("failed_to_deleted_score_keeper".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });

    }catch(e){
      EasyLoading.showError("Failed to deleted $e",
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
      update();
    }
  }


  Future<void> getListScorekeeper() async {
    isLoading = true;
    update();
    try {
      await _scoreKeeperRepo.getListScorekeeper(
          limit: limit, page: currentPage, search: searchQuery).then((value){
        if (value.isNotEmpty) {
          listScorekeeper = value;
          totalPages = listScorekeeper.length == limit ? currentPage + 1 : currentPage;
          isLoading = false;
          update();
        } else {
          listScorekeeper = [];
          isLoading = false;
          update();
        }
      });
    } catch (error) {
      print("Error get in catch in con $error");
      isLoading = false;
      update();
    }
  }

  // Future<List<PlatformFile>?> pickCertificateFiles() async {
  //   FilePickerResult? certificateFilesResult = await FilePicker.platform.pickFiles(
  //     allowMultiple: true,
  //     type: FileType.any,
  //   );
  //   if (certificateFilesResult != null) {
  //     certificateFiles?.addAll(certificateFilesResult.files);
  //     update();
  //     return certificateFilesResult.files;
  //   } else {
  //     return null;
  //   }
  // }


  Future<List<PlatformFile>?> pickCertificateFiles() async {
    if (isFirstFilePick) {
      certificateFiles?.clear();
      isFirstFilePick = false;
      update();
    }
    FilePickerResult? certificateFilesResult = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.any,
    );

    if (certificateFilesResult != null) {
      certificateFiles?.addAll(certificateFilesResult.files);
      update();
      return certificateFilesResult.files;
    } else {
      return null;
    }
  }
  void resetFilePickState() {
    isFirstFilePick = true;
    update();
  }

  void removeCertificateFile(String? file ,int index) {
    print("file index remove : $file -- $index");
    if (index >= 0 && index < certificateFiles!.length) {
      certificateFiles!.removeAt(index);
      update();
    }
  }

  void setPage(int page) {
    currentPage = page;
    getListScorekeeper();
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getListScorekeeper();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getListScorekeeper();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getListScorekeeper();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getListScorekeeper();
    update();
  }

  Future<PlatformFile?> getPlatformFileByName(String fileName) async {
    try {
      final directory = await getApplicationDocumentsDirectory();
      final filePath = '${directory.path}/$fileName';
      final file = File(filePath);
      if (await file.exists()) {
        return PlatformFile(
          path: filePath,
          name: fileName,
          size: await file.length(),
        );
      }
    } catch (e) {
      print("Error retrieving file: $e");
    }
    return null;
  }
}
