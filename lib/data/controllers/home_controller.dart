import 'dart:convert';
import 'package:esport_system/data/model/total_item_model.dart';
import 'package:esport_system/data/repo/home_repo.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/views/assistant/get_list_assistant.dart';
import 'package:esport_system/views/create_team_section/screen/get_team_screen.dart';
import 'package:esport_system/views/event_section/list_event.dart';
import 'package:esport_system/views/league_section/list_league.dart';
import 'package:esport_system/views/matches_section/screen/get_matches.dart';
import 'package:esport_system/views/medal_section/get_medal.dart';
import 'package:esport_system/views/pitch_section/pitch_list.dart';
import 'package:esport_system/views/player_section/screen/get_list_player.dart';
import 'package:esport_system/views/position_section/get_position_screen.dart';
import 'package:esport_system/views/report_section/medal_report.dart';
import 'package:esport_system/views/report_section/men_report.dart';
import 'package:esport_system/views/request_item_section/list_request_item.dart';
import 'package:esport_system/views/score_keeper_section/get_list_scorekeeper.dart';
import 'package:esport_system/views/subject_section/get_list_subject.dart';
import 'package:esport_system/views/user_section/screen/get_list_user.dart';
import 'package:esport_system/views/web_view_section/web_view_report_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:esport_system/helper/route_helper.dart';
import 'package:shared_value/shared_value.dart';

import '../../views/arbitrator_section/get_arbitrator_screen.dart';
import '../../views/categories/list_categories_player.dart';
import '../../views/coach_section/get_coach.dart';
import '../../views/home_section/screen/home.dart';
import '../../views/item_section/get_item.dart';
import '../../views/matches_section/screen/get_registration_matches.dart';
import '../../views/matches_section/screen/register_match.dart';
import '../../views/monthly_test/assign_monthly_test.dart';
import '../../views/rank_section/get_rank.dart';
import '../../views/setting_section/screen/setting.dart';
import '../../views/umpire_section/get_list_umpire.dart';

class HomeController extends GetxController implements GetxService {

  List<MenuItem> menuItems = [];

  List<MenuItem> bottomNavItems = [];

  @override
  void onInit() {
    initializeMenuItems();
    super.onInit();
  }
  void initializeMenuItems() {
    menuItems = [
      MenuItem(
          title: 'dashboard'.tr,
          widget: const Home(),
          svgMenu: 'assets/icons/dashboard.png',
          isSelected: true
      ),
      if(1==2)
      MenuItem(
          title: 'Register Match'.tr,
          widget: const GetRegistrationMatches(),
          svgMenu: 'assets/icons/dashboard.png',
      ),
      MenuItem(
        title: 'team'.tr,
        svgMenu: 'assets/icons/group.png',
        subMenuItems: [
          SubMenuItem(title: 'teams'.tr, svgSubMenu: 'assets/icons/group.png', widget: const ListTeamScreen()),
          SubMenuItem(title: 'league'.tr, svgSubMenu: 'assets/icons/league.png', widget: const ListLeague()),
          SubMenuItem(title: 'position'.tr, svgSubMenu: 'assets/icons/position.png', widget: const GetPositionScreen()),
          SubMenuItem(title: 'rank'.tr, svgSubMenu: 'assets/icons/rank.png', widget: const GetRank()),
        ],
      ),
      MenuItem(
        title: 'officials'.tr,
        svgMenu: 'assets/icons/user.png',
        subMenuItems: [
          SubMenuItem(title: 'referee'.tr, svgSubMenu: 'assets/icons/arbitrator.png', widget: const GetArbitratorScreen()),
          SubMenuItem(title: 'umpire'.tr, svgSubMenu: 'assets/icons/arbitrator.png', widget: const GetListUmpires()),
          SubMenuItem(title: 'scorekeepers'.tr, svgSubMenu: 'assets/icons/user.png', widget: const GetListScorekeeper()),
        ],
      ),
      MenuItem(
        title: 'event'.tr,
        svgMenu: 'assets/icons/calendar.png',
        subMenuItems: [
          SubMenuItem(title: 'events'.tr, svgSubMenu: 'assets/icons/calendar.png', widget: const ListEvent()),
          //SubMenuItem(title: 'match'.tr, svgSubMenu: 'assets/icons/avatar.png', widget: const MatchesScreen()),
          SubMenuItem(title: 'match_register'.tr, svgSubMenu: 'assets/icons/avatar.png', widget: const GetRegistrationMatches()),
        ],
      ),
      MenuItem(
        title: 'player'.tr,
        svgMenu: 'assets/icons/group.png',
        subMenuItems: [
          SubMenuItem(title: 'players'.tr, svgSubMenu: 'assets/icons/group.png', widget: const ListPlayerScreen()),
          SubMenuItem(title: 'coach'.tr, svgSubMenu: 'assets/icons/coach.png', widget: const GetCoachScreen()),
          SubMenuItem(title: 'assistant'.tr, svgSubMenu: 'assets/icons/coach.png', widget: const GetAssistantScreen()),
          SubMenuItem(title: 'category'.tr, svgSubMenu: 'assets/icons/group.png', widget: const ListCategoriesPlayer()),
        ],
      ),
      MenuItem(
        title: 'request'.tr,
        svgMenu: 'assets/icons/item.png',
        subMenuItems: [
          SubMenuItem(title: 'request_item'.tr, svgSubMenu: 'assets/icons/item.png', widget: const ListRequestItem()),
          SubMenuItem(title: 'request_pitch'.tr, svgSubMenu: 'assets/icons/icons8-stadium-100.png', widget: const ListPitch()),
        ],
      ),
      MenuItem(
        title: 'report'.tr,
        svgMenu: 'assets/icons/report.png',
        widget: const WebViewReportScreen(),
      ),

      if(1==2)
      MenuItem(
        title: 'monthly_test'.tr,
        svgMenu: 'assets/images/match.png',
        subMenuItems: [
          SubMenuItem(title: 'monthly_test'.tr, svgSubMenu: 'assets/images/match.png', widget: const AssignMonthlyTest()),
          SubMenuItem(title: 'subject'.tr, svgSubMenu: 'assets/icons/subjective.png', widget: const GetListSubject()),
        ],
      ),

      MenuItem(
          title: 'medals'.tr,
          svgMenu: 'assets/icons/medal.png',
          widget: const GetMedalScreen()
      ),
      MenuItem(
          title: 'items'.tr,
          svgMenu: 'assets/icons/bowl.png',
          widget: const GetItemScreen()
      ),

      MenuItem(
        title: 'users'.tr,
        svgMenu: 'assets/icons/user.png',
        widget: const GetUserList(),
      ),
      MenuItem(
        title: 'settings'.tr,
        svgMenu: 'assets/icons/settings.png',
        widget: const SettingScreen(),
      ),
    ];
    bottomNavItems = [
      MenuItem(
          title: 'dashboard'.tr,
          widget: Home(),
          svgMenu: 'assets/icons/dashboard.png',
          isSelected: true
      ),
      MenuItem(
          title: 'medals'.tr,
          svgMenu: 'assets/icons/medal.png',
          widget: const GetMedalScreen(),
          isSelected: false
      ),
      MenuItem(
          title: 'items'.tr,
          svgMenu: 'assets/icons/bowl.png',
          widget: const GetItemScreen(),
          isSelected: false
      ),

      MenuItem(
          title: 'users'.tr,
          svgMenu: 'assets/icons/user.png',
          widget: const GetUserList(),
          isSelected: false
      ),
      MenuItem(
          title: 'settings'.tr,
          svgMenu: 'assets/icons/settings.png',
          widget: const SettingScreen(),
          isSelected: false
      ),
    ];
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;
  final HomeRepo _homeRepo = HomeRepo();
  List<TotalItem> items = [];
  TotalItem? itemCounts = TotalItem();
  bool isLoading = false;



  void controlMenu() {
    if (_scaffoldKey.currentState != null &&
        !_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.openDrawer();
    }
    update();
  }

  //// phone,name, username, password, role_id
  int indexBottomNavbar = 0;
  var currentIndex = 0;
  var selectedSubmenuIndices = <int, int>{};

  void updateIndex(int index) {
    currentIndex = index;
    update();
  }

  var currentIndex1 = 0;
  void updateIndex1(int index) {
    currentIndex1 = index;
    update();
  }

  Widget? widgetHome;
  updateWidgetHome(Widget? widget) {
    widgetHome = widget ;
    update();
  }
  // updateIndexBottomNavbar(int index) {
  //   indexBottomNavbar = index;
  //   update();
  // }

  void updateIndexBottomNavbar(int index) {
    indexBottomNavbar = index;
    widgetHome = bottomNavItems[index].widget;
    //bottomNavItems[index].isSelected = true;
    for (var i = 0; i < bottomNavItems.length; i++) {
      bottomNavItems[i].isSelected = (i == index);  // Set true only for the selected index
    }
    update();

  }

  void selectMenuItem(MenuItem selectedItem) {
    for (var item in menuItems) {
      item.isSelected = item == selectedItem;
      if (item.subMenuItems != null) {
        for (var subItem in item.subMenuItems!) {
          subItem.isSelected = false;
        }
      }
    }
    update();
  }

  void selectSubMenuItem(MenuItem parentItem, SubMenuItem selectedSubItem) {
    for (var item in menuItems) {
      if (item == parentItem) {
        item.isSelected = true;
        for (var subItem in item.subMenuItems!) {
          subItem.isSelected = subItem == selectedSubItem;
        }
      } else {
        item.isSelected = false;
        if (item.subMenuItems != null) {
          for (var subItem in item.subMenuItems!) {
            subItem.isSelected = false;
          }
        }
      }
    }
    update();
  }






  UserModel? userModel;
  TextEditingController? phoneController = TextEditingController(text: "0987654321");
  TextEditingController? nameController = TextEditingController(text: "");
  TextEditingController? passwordController = TextEditingController(text: "123456");
  TextEditingController? passwordConfirmationController = TextEditingController(text: "");


  clearController() {
    phoneController!.text = "";
    nameController!.text = "";
    passwordController!.text = "";
    passwordConfirmationController!.text = "";
  }

  Future<bool> registerRepo() async {
    var headers = {'Content-Type': 'application/json'};
    var request =
        http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/register'));
    request.body = json.encode({
      "password": passwordController!.text,
      "passwordConfirmation": passwordConfirmationController!.text,
      "phone": phoneController!.text,
      "username": phoneController!.text,
      "name": nameController!.text,
    });
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      String source = (await response.stream.bytesToString());
      var data = jsonDecode(source);
      try {
        if (data['data'] != null &&
            data['data']['user'] != null &&
            data['data']['bcrypt'] != null) {
          userModel = UserModel.fromJson(data['data']['user']);
          userInfo = userModel;
          var bcrypt = data['data']['bcrypt'];
          accessToken.$ = bcrypt;
          user.$ = jsonEncode(data['data']['user']);
          accessToken.save();
          user.save();
          clearController();
          update();
          return true;
        }
      } catch (_) {}
    } else {
      if (kDebugMode) {
        print(response.reasonPhrase);
      }
    }
    return false;
  }

  Future<bool> loginRepo() async {
    var headers = {
      'Content-Type': 'application/json',
    };
    var request =
        http.Request('POST', Uri.parse('${AppConstants.baseUrl}/api/login'));
    request.body = json.encode({
      "password": passwordController!.text,
      "username": phoneController!.text
    });

    print(request.body);

    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      String source = (await response.stream.bytesToString());
      var data = jsonDecode(source);
      try {
        if (data['data'] != null &&
            data['data']['user'] != null &&
            data['data']['bcrypt'] != null) {
          userModel = UserModel.fromJson(data['data']['user']);
          userInfo = userModel;
          var bcrypt = data['data']['bcrypt'];
          accessToken.$ = bcrypt;
          user.$ = jsonEncode(data['data']['user']);
          accessToken.save();
          user.save();
          clearController();
          update();
          return true;
        }
      } catch (_) {}
    } else {
      if (kDebugMode) {
        print(response.reasonPhrase);
      }
    }

    return false;
  }

  int selectedIndex = -1; // -1 means no item is selected

  void updateSelectedIndex(int index) {
    selectedIndex = index;
    update();
  }

  final SharedValue<bool> isDarkMode = SharedValue(
    value: false,
    key: "isDarkMode",
    autosave: true,
  );
  Future<void> loadThemeDark() async {
    await isDarkMode.load();
    Get.changeThemeMode(isDarkMode.$ ? ThemeMode.dark : ThemeMode.light);
    update();
  }

  void toggleTheme() {
    isDarkMode.$ = !isDarkMode.$;
    isDarkMode.save();
    Get.changeThemeMode(isDarkMode.$ ? ThemeMode.dark : ThemeMode.light);
  }

  Future<void> fetchItemCounts() async {
    isLoading = true;

    try {
      itemCounts = await _homeRepo.fetchItemCounts();
      print("==========${itemCounts!.totalMatches}");
    } catch (e) {
      isLoading = false;
      print('Error fetching item counts: $e');
    }
    update();
  }
}

class MenuItem {
  final String title;
  final String svgMenu;
  final Widget? widget;
  final List<SubMenuItem>? subMenuItems;
  bool isSelected;

  MenuItem({
    required this.title,
    required this.svgMenu,
    this.widget,
    this.subMenuItems,
    this.isSelected = false, // Initialize to false
  });
}

class SubMenuItem {
  final String title;
  final String svgSubMenu;
  final Widget widget;
  bool isSelected;

  SubMenuItem({
    required this.title,
    required this.svgSubMenu,
    required this.widget,
    this.isSelected = false, // Initialize to false
  });
}
