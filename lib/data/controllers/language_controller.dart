import 'dart:ui';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/home_section/screen/new_home_dash.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'home_controller.dart';

class LanguageController extends GetxController implements GetxService {


  List<Map<String, dynamic>> languages = [
    {"name": "English", "icon": Icons.language, "code": "en", "countryCode": "US"},
    {"name": "Khmer", "icon": Icons.language, "code": "km", "countryCode": "KH"},
  ];
  // The currently selected locale
  Locale _locale = const Locale('en', 'US');
  int selectedIndexLang = 0;

  Locale get locale => _locale;

  @override
  void onInit() {
    super.onInit();
    loadLanguage();
  }
  loadLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? langCode = prefs.getString('languageCode');
    String? countryCode = prefs.getString('countryCode');


    if (langCode != null && countryCode != null) {
      _locale = Locale(langCode, countryCode);
      Get.updateLocale(_locale);
      selectedIndexLang = languages.indexWhere((lang) => lang['code'] == langCode && lang['countryCode'] == countryCode,
      );
      Get.find<HomeController>().initializeMenuItems();
      if (selectedIndexLang == -1) selectedIndexLang = 0;
    }
  }

  void changeLanguage(String langCode, String countryCode) async {
    _locale = Locale(langCode, countryCode);
    Get.updateLocale(_locale);
    Get.find<HomeController>().initializeMenuItems();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('languageCode', langCode);
    await prefs.setString('countryCode', countryCode);
    update();
  }

  // Method to get the current language selection index
  int getCurrentLanguageIndex(List<Map<String, dynamic>> languages) {
    return languages.indexWhere(
          (lang) => lang['code'] == _locale.languageCode && lang['countryCode'] == _locale.countryCode,
    );
  }
}
