import 'dart:typed_data';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/data/model/player_in_team_model.dart';
import 'package:esport_system/data/repo/team_repo.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/data/model/team_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class TeamController extends GetxController implements GetxService {
  final TeamRepository _teamRepository = TeamRepository();
  final ImagePicker _picker = ImagePicker();
  TextEditingController stadiumController = TextEditingController();
  TextEditingController teamNameController = TextEditingController();
  TextEditingController championshipsWonController = TextEditingController();
  TextEditingController provinceController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController communeController = TextEditingController();
  TextEditingController villageController = TextEditingController();
  final List<Map< String,String>> options = [
    {
      'value': 'province', 'text': 'Province'
    },
    {
      'value': 'district', 'text': 'District'
    },
    {
      'value': 'commune', 'text': 'Commune'
    },
    {
      'value': 'village', 'text': 'Village'
    },
    ];



  List<Address> provinces = [];
  List<Address> districts = [];
  List<Address> communes = [];
  List<Address> villages = [];
  List<TeamModel> listTeam = [];
  List<TeamModel> listTeamDropdown = [];
  List<PlayerInTeamModel> players = [];

  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  String? selectedLevel;

  Address? selectedProvince;
  Address? selectedDistrict;
  Address? selectedCommune;
  Address? selectedVillage;
  XFile? selectedImage;

  bool isLoadingProvinces = false;
  bool isLoadingDistricts = false;
  bool isLoadingCommunes = false;
  bool isLoadingVillages = false;


  Map<String, Uint8List?> imageDataMap = {};

  void pickImageForTeam(String teamId) async {
    final pickedFile =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      final bytes = await pickedFile.readAsBytes();
      imageDataMap[teamId] = bytes;
      update();
    } else {
      print('No image selected.');
    }
  }

  // Method to retrieve image data by team ID
  Uint8List? getImageDataForTeam(String teamId) {
    return imageDataMap[teamId];
  }
  void pickImage() async {
    XFile? imageFile = await _picker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      selectedImage = imageFile;
      update();
    } else {
      print('No image selected.');
    }
  }

  // Ensure that the selected image is cleared after creating a team
  void clearSelectedImage() {
    selectedImage = null;
    update();
  }

  void clearControlAddress(){
    print("---------- Test 1");
    selectProvince(Address(code: "", name: "",));
    selectDistrict(Address(code: "", name: "",));
    selectCommune(Address(code: "", name: "",));
    selectVillage(Address(code: "", name: "",));
    selectedLevel = null ;
    update();
  }

  void selectLevel({required String level}){
    selectedLevel = level ;
    validateLevel();
    update();
  }

  void clearAddress(){
    provinceController.text = "";
    districtController.text = "";
    communeController.text = "";
    villageController.text = "";
  }

  bool validateLevel(){
    if(selectedLevel == "province"){
      print("------------> Level check : ${provinceController.text} ");
      if(provinceController.text.isNotEmpty){
        return true;
      }else{
        return false;
      }
    }else if(selectedLevel == "district"){
      if(provinceController.text.isNotEmpty && districtController.text.isNotEmpty){
        print("------------> Level check : district ");
        return true;
      }else{
        return false;
      }
    }else if(selectedLevel == "commune"){
      if( provinceController.text.isNotEmpty &&
          districtController.text.isNotEmpty &&
          communeController.text.isNotEmpty){
        print("------------> Level check : commune ");
        return true;
      }else{
        return false;
      }
    }else{
      if( provinceController.text.isNotEmpty &&
          districtController.text.isNotEmpty &&
          communeController.text.isNotEmpty &&
          villageController.text.isNotEmpty){
        print("------------> Level check : village ");
        return true;
      }else{
        return false;
      }
    }
  }



  Future<bool> createTeam({
    required String leagueId,
    required String teamName,
    required String foundDate,
    required String stadium,
    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String district,
    required String commune,
    required String villages,
    required String championshipsWon,
    required XFile? logo,
  }) async {
    try {
      await _teamRepository.createTeam(
        teamName: teamName,
        foundDate: foundDate,
        stadium: stadium,
        provinceCode: provinceCode,
        districtCode: districtCode,
        villageCode: villageCode,
        communeCode: communeCode,
        province: province,
        district: district,
        commune: commune,
        villages: villages,
        championshipsWon: championshipsWon,
        leagueId: leagueId,
        logo: logo,
      );
      return true;
    } catch (e) {
      print("Error creating team: $e");
      return false;
    }
  }
  void fetchPlayersInTeam(String teamId) async {
    try {
      isLoading=true;
      var fetchedPlayers = await _teamRepository.fetchPlayersInTeam(teamId);
      players.assignAll(fetchedPlayers);
      print('==============user_player${players.length}');
      update();
    } catch (e) {
      print(e);
    } finally {
      isLoading=true;
    }
  }

  void fetchProvinces() async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _teamRepository.getAddresses();
      provinces.assignAll(
          result.where((address) => address.type == 'province').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchDistricts(String provinceCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _teamRepository.getDistricts(provinceCode);
      districts.assignAll(
          result.where((address) => address.type == 'district').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchCommunes(String districtCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _teamRepository.getCommunes(districtCode);
      communes.assignAll(
          result.where((address) => address.type == 'commune').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchVillages(String communeCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _teamRepository.getVillages(communeCode);
      villages.assignAll(
          result.where((address) => address.type == 'village').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void selectProvince(Address? province) {
    print("=====KKK${province!.name} ==== ${province.code}");
    selectedProvince = province;
    if (province != null) {
      // If the same province is selected again, clear district and commune selections
      if (selectedProvince!.code == province.code) {
        selectedDistrict = null;
        selectedCommune = null;
        selectedVillage = null;
      }
      fetchDistricts(province.code??"");
    } else {
      districts.clear();
      communes.clear();
      villages.clear();
      selectedDistrict = null;
      selectedCommune = null;
      selectedVillage = null;
    }
    update();
  }

  void selectDistrict(Address? district) {
    selectedDistrict = district;
    if (district != null) {
      fetchCommunes(district.code!);
      selectedCommune = null;
    } else {
      communes.clear();
      selectedCommune = null;
    }
    update();
  }

  void selectCommune(Address? commune) {
    selectedCommune = commune;
    if (commune != null) {
      fetchVillages(commune.code!);
      selectedVillage = null;
    } else {
      villages.clear();
      selectedVillage = null;
    }
    update();
  }

  void selectVillage(Address? village) {
    selectedVillage = village;
    // Add any additional logic here if needed
    update();
  }


  Future<void> getTeam() async {
    isLoading = true;
    update();
    try {
      listTeam = await _teamRepository.getTeam(
          userId: userInfo!.id.toString(),
          search: searchQuery,
          page: currentPage,
          limit: limit);

      isLoading = false;
      totalPages = listTeam.length == limit ? currentPage + 1 : currentPage;
      update();
    } catch (e) {
      print("============Error : $e");
      isLoading = false;
      update();
    }
  }

  Future<void> getTeamDropdown() async {
    isLoading = true;
    update();
    try {
      listTeamDropdown = await _teamRepository.getTeam(
          userId: userInfo!.id.toString(),
          search: searchQuery,
          page: "",
          limit: "");
      isLoading = false;
      update();
    } catch (e) {
      print("============Error get team dropdown in con : $e");
      isLoading = false;
      update();
    }
  }

  Future<bool> updateTeam(
      {required String teamId,
      required String teamName,
      required String foundedDate,
      required String stadium,
      required String provinceCode,
      required String districtCode,
      required String villageCode,
      required String communeCode,
      required String province,
      required String district,
      required String commune,
      required String village,
      required String leagueId,
      required String championshipsWon,
      required XFile? imageData}) async {
    try {
      isLoading = true;
      update();
      // XFile? imageFile;
      // if (imageData != null) {
      //   imageFile = XFile.fromData(imageData, mimeType: 'image/jpeg'); // assuming JPEG image type
      // }
      await _teamRepository.updateTeam(
          teamId: teamId,
          teamName: teamName,
          foundedDate: foundedDate,
          stadium: stadium,
          provinceCode: provinceCode,
          districtCode: districtCode,
          villageCode: villageCode,
          communeCode: communeCode,
          province: province,
          district: district,
          commune: commune,
          village: village,
          leagueId:leagueId ,
          championshipsWon: championshipsWon,
          logo: imageData);

      isLoading = false;
      getTeam();
      //update();
      return true;
    } catch (error) {
      print("--------Error : $error");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<Map<String, dynamic>> deleteTeam({required String teamID}) async {
    isLoading = true;
    update();
    var result = await _teamRepository.deleteTeam(teamID: teamID);
    isLoading = false;
    update();
    return result;
  }

  void setPage(int page) {
    currentPage = page;
    getTeam();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getTeam();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getTeam();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getTeam();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getTeam();
    update();
  }

}
