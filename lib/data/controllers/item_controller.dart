import 'package:esport_system/data/model/item_model.dart';
import 'package:esport_system/data/model/item_request_model.dart';
import 'package:esport_system/data/repo/item_repo.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class ItemController extends GetxController implements GetxService {
  final ItemRepo _itemRepo = ItemRepo();
  List<ItemModel> listItem = [];
  List<ItemsHistory> listItemHistory = [];
  List<ItemRequestModel> listItemReq = [];

  final List<Map<String, String>> type = [
    {'value': 'using', 'text': 'Using'},
    {'value': 'open', 'text': 'Open'},
  ];

  TextEditingController titleController = TextEditingController();
  TextEditingController subTitleController = TextEditingController();
  TextEditingController qtyItemsController = TextEditingController();
  TextEditingController notedController = TextEditingController();
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();

  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';

  Future<void> selectFormDate(BuildContext context, TextEditingController controller) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );
    if (picked != null) {
      controller.text = DateFormat('yyyy-MM-dd').format(picked);
      print("Debug month/Year  :${controller.text}");
    }
  }

  Future<void> selectToDate(BuildContext context, TextEditingController controller) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2100),
    );

    if (picked != null) {
      controller.text = DateFormat('yyyy-MM-dd').format(picked);
      print("Debug month/Year  :${controller.text}");
    }
  }

  Future<void> getItemDetailAndHistory({
    required String? itemID,
    required String? fromDate,
    required String? toDate,
    required String? trainType,
  }) async {
    isLoading = true;
    update();
    try {
      await _itemRepo.itemsHistory(
          itemId: itemID,
          toDate: toDate,
          fromDate: fromDate,
          trainType: trainType,
          limit: limit,
          page: currentPage
      ).then((v){
        if(v.isNotEmpty){
          listItemHistory = v ;
          totalPages = listItemHistory.length == limit ? currentPage + 1 : currentPage;
          isLoading = false ;
          update();
        }else{
          listItemHistory=[];
          isLoading = false ;
          update();
        }
      });
    } catch (e) {
      print('Error get item $e');
      isLoading = false;
      update();
    }
  }

  Future<void> getItem() async {
    isLoading = true;
    update();
    try {
      await _itemRepo.getItem(
          limit: limit,
          page: currentPage,
          search: searchQuery).then((v){
        if(v.isNotEmpty){
          totalPages = v.length == limit ? currentPage + 1 : currentPage;
        }
      });
      listItem = await _itemRepo.getItem(limit: limit, page: currentPage, search: searchQuery);
      totalPages = listItem.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      print('Error get item $e');
      isLoading = false;
      update();
    }
  }



  Future<bool> createItem({
    required String itemTitle,
    required String subTitle,
    required String status,
  }) async {
    isLoading = true;
    update();
    try {
      bool result = await _itemRepo.createItem(
          itemTitle: itemTitle, subTitle: subTitle, status: status);
      isLoading = false;
      update();
      return result;
    } catch (e) {
      print("Error Create item: $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<bool> addItemsStock({ required String itemID,required String qtyStock, required String noted}) async {
    isLoading = true;
    update();

    try {
      bool result = await _itemRepo.addQtyItem(itemID: itemID, qtyStock: qtyStock, noted: noted);
      isLoading = false;
      update();
      return result;
    } catch (e) {
      print("Error Add qty item to stock: $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<bool> updateItem({
    required String itemID,
    required String itemTitle,
    required String otherTitle,
    required String isStatus,
  }) async {
    isLoading = true;
    update();
    try {
      bool result = await _itemRepo.updateItem(
          itemID: itemID,
          itemTitle: itemTitle,
          otherTitle: otherTitle,
          isStatus: isStatus,
          updatedBy: userInfo!.id.toString());
      isLoading = false;
      return result;
    } catch (e) {
      print("Error update item $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<bool> deleteItem({required String itemID}) async {
    isLoading = true;
    update();
    try {
      var result = await _itemRepo.deleteItem(itemID: itemID);
      isLoading = false;
      return result;
    } catch (e) {
      print("Error delete item $e");
      isLoading = false;
      update();
      return false;
    }
  }

  void setPage(int page) {
    currentPage = page;
    getItem();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getItem();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getItem();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getItem();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getItem();
    update();
  }


  Future<void> getItemRequest()async{
    try{
      await _itemRepo.getListItemRequest().then((v){
        if(v.isNotEmpty){
          listItemReq = v ;
        }
        update();
      });
    }catch(e){
      print("Error get item in controller $e");
    }
  }
}
