

import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/cart_item_model.dart';
import 'package:esport_system/data/model/team_model.dart';
import 'package:esport_system/data/repo/item_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../helper/sr.dart';
import '../../views/home_section/screen/home.dart';
import '../../views/request_item_section/list_request_item.dart';
import '../model/address_model.dart';
import '../model/item_model.dart';
import '../model/item_request_model.dart';
import 'home_controller.dart';

class RequestItemController extends GetxController implements GetxService{

  final ItemRepo _itemRepo = ItemRepo();
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();



  TextEditingController refNumberController = TextEditingController();
  TextEditingController noteController = TextEditingController();
  TextEditingController subNoteController = TextEditingController();
  List<ItemRequestModel> listItemReq = [];

  bool isLoading = false;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';

  List<ItemModel> listItem = [];
  String? selectTeamId ;
  TeamModel? previousSelectedTeam ;
  String? selectLocationCode ;
  bool isInCart = false;
  int quantity = 1;
  List<CartItemModel> itemsCart = [];
  String? selectedValue;
  final List<String> dropdownItems = ['approve'.tr, 'pending'.tr,'reject'.tr];

  void selectValueDropdown(String value){
    selectedValue = value;
  }


  void setPage(int page) {
    currentPage = page;
    getItemRequest();
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getItemRequest();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getItemRequest();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getItemRequest();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getItemRequest();
    update();
  }


  Future<void> getItemRequest()async{
    isLoading = true;
    update();
    try{
      await _itemRepo.getListItemRequest(limit: limit, page: currentPage, search: searchQuery).then((v){
        if(v.isNotEmpty){
          listItemReq = v ;
          totalPages = listItemReq.length == limit ? currentPage + 1 : currentPage;
          isLoading = false ;
          update();
        }else{
          listItemReq=[];
          isLoading = false ;
          update();
        }
      });
    }catch(e){
      isLoading = false ;
      update();
      print("Error get item in controller $e");
    }
  }

  void clearValue(){
    refNumberController.text = "";
    noteController.text = "";
    teamController.selectProvince(Address(code: "", name: ""));
    selectTeamId = null;
    selectLocationCode = "";
    itemsCart = [] ;
  }


  Future<void> addToCart(ItemModel itemModel, int quantity,  String? subNote) async {
    var existing = itemsCart.indexWhere((item) => item.items!.id == itemModel.id);
    if(existing != -1){
      print("*****************This item existing in cart now ?");
      itemsCart[existing].quantity = (itemsCart[existing].quantity ?? 0) + 1;
    }else{
      print("+++++++++++++++++ Add item to cart! ");
      itemsCart.add(CartItemModel(items: itemModel ,quantity: quantity ,subNote: subNote));
      final index = itemsCart.indexWhere((cartItem) => cartItem.items?.id == itemModel.id);
      // Keep to do remember does item isInCart or not yet
      // if (index != -1) {
      //   itemsCart[index].items?.isInCart = true;
      // }
    }
    update();
  }

  void setPreviousValue(ItemRequestModel? itemRequest){
     itemsCart = convertToCartItems(itemRequest);
     selectLocationCode = itemRequest!.location!.code.toString();
     teamController.selectProvince(Address(code: itemRequest.location?.code, name: itemRequest.location!.name));
     refNumberController.text = itemRequest.refNumber.toString();
     noteController.text = itemRequest.note.toString();
     selectTeamId = itemRequest.team?.id;
  }


  List<CartItemModel> convertToCartItems(ItemRequestModel? itemRequest) {
    List<CartItemModel> cartItems = [];
    if (itemRequest != null && itemRequest.itemDetails != null) {
      for (var detail in itemRequest.itemDetails!) {
        CartItemModel cartItem = CartItemModel(
          items: ItemModel.fromJson(detail.item!.toJson()),
          quantity: SR.toInt(detail.qty),
          subNote: detail.note,
        );
        cartItems.add(cartItem);
      }
      for (var item in cartItems) {
        print('Item Title: ${item.items?.id} ,name: ${item.items!.itemTitle}, Quantity: ${item.quantity}, Note: ${item.subNote}');
      }
    } else {
      print("No items to update");
    }
    return cartItems;
  }


  Future<void> incrementQuantity(CartItemModel cartItem) async {
    cartItem.quantity = (cartItem.quantity ?? 0) + 1;
    update();
  }

  Future<void> decrementQuantity(CartItemModel cartItem) async {
    if (cartItem.quantity! > 1) {
      cartItem.quantity = (cartItem.quantity ?? 0) - 1;
    } else {
      itemsCart.remove(cartItem);
    }
    update();
  }

  Future<void> createRequest()async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      List<Map<String, dynamic>> items = itemsCart.map((cartItem) {
        return {
          "item_id": cartItem.items!.id.toString(),
          "qty": cartItem.quantity.toString(),
          "sub_note": "",
        };
      }).toList();
      await _itemRepo.createRequestItem(
          refNum: refNumberController.text,
          location: selectLocationCode,
          teamId: selectTeamId,
          note: noteController.text,
          items: items
      ).then((v){
        if(v){
          EasyLoading.showSuccess(
            'create_request_items_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearValue();
          homeController.updateWidgetHome(const ListRequestItem());
        }else{
          EasyLoading.showError("failed_to_create_request".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError('Error create request item in controller $e',
        duration: const Duration(seconds: 2),
      );
      clearValue();
      EasyLoading.dismiss();
    }
  }

  Future<void> updateRequest({required String? rqId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      List<Map<String, dynamic>> items = itemsCart.map((cartItem) {
        return {
          "item_id": cartItem.items!.id.toString(),
          "qty": cartItem.quantity.toString(),
          "sub_note": "",
        };
      }).toList();
      await _itemRepo.updateRequestItem(
          rqId: rqId,
          location: selectLocationCode,
          refNum: refNumberController.text,
          note: noteController.text,
          teamId: selectTeamId,
          items: items
      ).then((v){
        if(v){
          EasyLoading.showSuccess(
            'request_items_Update_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          clearValue();
          homeController.updateWidgetHome(const ListRequestItem());
        }else{
          EasyLoading.showError("failed_to_update_request".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      clearValue();
      EasyLoading.showError('Error update request item in controller $e',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  Future<void> rejectRequest({required String? rqId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      await _itemRepo.rejectedRequestItem(
          rqId: rqId
      ).then((v){
        if(v){
          EasyLoading.showSuccess(
            'rejected_request_items_successfully'.tr,
            dismissOnTap: true,
            duration: const Duration(milliseconds: 1000),
          );
          getItemRequest();
          homeController.updateWidgetHome(const ListRequestItem());
        }else{
          EasyLoading.showError("failed_to_rejected_request".tr,
            duration: const Duration(seconds: 2),
          );
        }
        EasyLoading.dismiss();
        update();
      });
    }catch(e){
      EasyLoading.showError('Error rejected request item in controller $e',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  Future<void> getItem() async {
    isLoading = true;
    update();
    try {
      listItem = await _itemRepo.getItem(limit: limit, page: currentPage, search: searchQuery);
      totalPages = listItem.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      print('Error get item $e');
      isLoading = false;
      update();
    }
  }

  Future<void> receivedItem({required String? rqId})async{
    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
    try{
      List<Map<String, dynamic>> items = itemsCart.map((cartItem) {
        return {
          "item_id": cartItem.items!.id.toString(),
          "qty": cartItem.quantity.toString(),
          "sub_note": "",
        };
      }).toList();
      // for(int i=0 ; i<itemsCart.length ; i++){
      //   var a = itemsCart[i];
      //   print(" name : ${a.items!.itemTitle} ---- qty : ${a.quantity}");
      // }
      print("rqId : $rqId");
      print("refNumberController : ${refNumberController.text}");
      print("selectLocationCode : ${selectLocationCode}");
      print("selectTeamId : ${selectTeamId}");
      print("noteController : ${noteController.text}");
      await _itemRepo.receivedItem(
          rqId: rqId,
          refNum: refNumberController.text,
          location: selectLocationCode,
          teamId: selectTeamId,
          note: noteController.text,
          items: items
      ).then((v){
            if(v['success']){
              EasyLoading.showSuccess(
                'received_items_successfully'.tr,
                dismissOnTap: true,
                duration: const Duration(milliseconds: 1000),
              );
              getItemRequest();
              homeController.updateWidgetHome(const ListRequestItem());
            }else{
              EasyLoading.showError("Failed to received ${v['success']}",
                duration: const Duration(seconds: 2),
              );
            }
            EasyLoading.dismiss();
            update();
      });
    }catch(e){
      EasyLoading.showError('Error rejected request item in controller $e',
        duration: const Duration(seconds: 2),
      );
      EasyLoading.dismiss();
    }
  }

  String _getKhmerDayOfWeek(String englishDay) {
    const dayOfWeekMap = {
      'Monday': 'ច័ន្ទ',
      'Tuesday': 'អង្គារ',
      'Wednesday': 'ពុធ',
      'Thursday': 'ព្រហស្បតិ៍',
      'Friday': 'សុក្រ',
      'Saturday': 'សៅរ៍',
      'Sunday': 'អាទិត្យ',
    };
    return dayOfWeekMap[englishDay] ?? englishDay;
  }

  String _getKhmerMonth(String englishMonth) {
    const monthMap = {
      'January': 'មករា',
      'February': 'កុម្ភៈ',
      'March': 'មីនា',
      'April': 'មេសា',
      'May': 'ឧសភា',
      'June': 'មិថុនា',
      'July': 'កក្កដា',
      'August': 'សីហា',
      'September': 'កញ្ញា',
      'October': 'តុលា',
      'November': 'វិច្ឆិកា',
      'December': 'ធ្នូ',
    };
    return monthMap[englishMonth] ?? englishMonth;
  }

  String _convertToKhmerNumber(int number) {
    const khmerDigits = '០១២៣៤៥៦៧៨៩';
    return number.toString().split('').map((digit) => khmerDigits[int.parse(digit)]).join('');
  }

  String? getKhmerDayOfWeek(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return null;
    }

    try {
      final dateTime = DateTime.parse(dateString);
      final dayOfWeek = DateFormat('EEEE').format(dateTime);
      return _getKhmerDayOfWeek(dayOfWeek);
    } catch (e) {
      return null;
    }
  }

  String? getKhmerDate(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return null;
    }

    try {
      final dateTime = DateTime.parse(dateString);
      final day = DateFormat('d').format(dateTime);
      final month = DateFormat('MMMM').format(dateTime);
      final year = DateFormat('y').format(dateTime);
      return '${_convertToKhmerNumber(int.parse(day))} ${_getKhmerMonth(month)} ${_convertToKhmerNumber(int.parse(year))}';
    } catch (e) {
      return null;
    }
  }

  String? getMonthKhmer(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return null;
    }

    try {
      final dateTime = DateTime.parse(dateString);
      final month = DateFormat('MMMM').format(dateTime);
      return _getKhmerMonth(month);
    } catch (e) {
      return null;
    }
  }

  String? getKhmerYear(String? dateString) {
    if (dateString == null || dateString.isEmpty) {
      return null;
    }

    try {
      final dateTime = DateTime.parse(dateString);
      final year = DateFormat('y').format(dateTime);
      return _convertToKhmerNumber(int.parse(year));
    } catch (e) {
      return null;
    }
  }





}