import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeController extends GetxController {
  var isDarkTheme = false;

  void loadTheme(bool isDark) {
    isDarkTheme = isDark;
  }

  void toggleTheme() async {
    isDarkTheme = !isDarkTheme;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('isDarkTheme', isDarkTheme);
  }
}
