import 'package:esport_system/data/model/rank_model.dart';
import 'package:esport_system/data/repo/rank_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class RankController extends GetxController implements GetxService {
  final RankRepo _rankRepo = RankRepo();
  TextEditingController titleController = TextEditingController();
  TextEditingController rankIdController = TextEditingController();
  List<RankModel> rank = [];
  List<RankModel> rankCoach = [];
  List<RankModel> rankArbitrator = [];
  bool isLoading = false;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';
  String? selectedValue;
  final List<Map<String, String>> dropdownItems = [
    {'value': 'coaches', 'text': 'Coaches'},
    {'value': 'arbitrator', 'text': 'Arbitrator'},
  ];

  void selectValueDropdown(String value){
    selectedValue = value;
  }

  Future<bool> createRank({required String type, required String title}) async {
    isLoading = true;
    update();
    try {
      bool result = await _rankRepo.createRank(type: type, title: title);
      isLoading = false;
      update();
      return result;
    } catch (e) {
      isLoading = false;
      update();
      print("----- Error create rank $e");
      return false;
    }
  }

  Future<void> getRank() async {
    isLoading = true;
    update();
    try {
      rank = await _rankRepo.getRank(search: searchQuery, page: currentPage, limit: limit);
      totalPages = rank.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      print("-------Error during get position : $e");
    }
  }

  Future<void> getRankCoach() async {
    isLoading = true;
    update();
    try {
      rankCoach = await _rankRepo.getRank(search: 'coaches', page: currentPage, limit: limit);
      totalPages = rank.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      print("-------Error during get position : $e");
    }
  }

  Future<void> getRankArbitrator() async {
    isLoading = true;
    update();
    try {
      rankArbitrator = await _rankRepo.getRank(
          search: 'arbitrator', page: currentPage, limit: limit);
      totalPages = rank.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
      update();
    } catch (e) {
      isLoading = false;
      print("-------Error during get position : $e");
    }
  }

  Future<bool> updateRank(
      {required String rankID,
      required String type,
      required String titleRank}) async {
    try {
      isLoading = true;
      bool result = await _rankRepo.updateRank(
          rankID: rankID, type: type, titleRank: titleRank);
      return result;
    } catch (e) {
      isLoading = false;
      update();
      print("===Error update $e");
      return false;
    }
  }

  Future<bool> deleteRank({required String rankID}) async {
    isLoading = true;
    update();
    try {
      bool result = await _rankRepo.deleteRank(rankID: rankID);
      isLoading = false;
      update();
      return result;
    } catch (e) {
      isLoading = false;
      update();
      print("Delter rank error $e");
      return false;
    }
  }

  void setPage(int page) {
    currentPage = page;
    getRank();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getRank();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getRank();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getRank();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getRank();
    update();
  }
}
