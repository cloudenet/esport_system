import 'dart:typed_data';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/data/repo/league_repo.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/data/model/league_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class LeagueController extends GetxController implements GetxService {

  final LeagueRepository _leagueRepository = LeagueRepository();

  TextEditingController codeController = TextEditingController();
  TextEditingController leagueNameController = TextEditingController();
  TextEditingController provinceController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController communeController = TextEditingController();
  TextEditingController villageController = TextEditingController();

  List<Address> provinces = [];
  List<Address> districts = [];
  List<Address> communes = [];
  List<Address> villages = [];
  Address? selectedProvince;
  Address? selectedDistrict;
  Address? selectedCommune;
  Address? selectedVillage;

  List<LeagueModel> listLeague = [];
  String address = '';

  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';

  XFile? selectedImage;

  bool isLoadingProvinces = false;
  bool isLoadingDistricts = false;
  bool isLoadingCommunes = false;
  bool isLoadingVillages = false;

  Uint8List? imageData;
  bool get isImageSelected => imageData != null;


  void clearTextField(){
    leagueNameController.text = "";
    selectedProvince = null;
    selectedDistrict = null;
    selectedCommune = null;
    selectedVillage = null;
  }
  Future<bool> createLeague({
    required String leagueName,
    required String createDate,
    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String address,
    required String district,
    required String commune,
    required String village,
  }) async {
    try {
      await _leagueRepository.createLeague(
          leagueName: leagueName,
          createDate: createDate,
          provinceCode: provinceCode,
          districtCode: districtCode,
          villageCode: villageCode,
          communeCode: communeCode,
          province: province,
          district: district,
          commune: commune,
          villages: village,
          address: address
      );
      return true;
    } catch (e) {
      print("Error creating league: $e");
      return false;
    }
  }

  Future<bool> updateLeague({
    required String id,
    required String leagueName,
    required String createDate,
    required String provinceCode,
    required String districtCode,
    required String villageCode,
    required String communeCode,
    required String province,
    required String district,
    required String commune,
    required String village,
    required String address,
  }) async {
    try {
      await _leagueRepository.updateLeague(
        leaugeId: id,
        leagueName: leagueName,
        createDate: createDate,
        provinceCode: provinceCode,
        districtCode: districtCode,
        villageCode: villageCode,
        communeCode: communeCode,
        province: province,
        district: district,
        commune: commune,
        villages: village,
      );
      return true;
    } catch (e) {
      print('==============update $e');
      return false;
    }
  }


  Future<void> getLeague() async {
    isLoading = true;
    update();
    try {
      listLeague =  await _leagueRepository.getleague(
          userId: userInfo!.id.toString(),
          search: searchQuery,
          page: currentPage,
          limit: limit
      );
      isLoading = false;
      totalPages = listLeague.length == limit ? currentPage + 1 : currentPage;
      update();
    } catch (e) {
      print("============Error : $e");
      isLoading = false;
      update();
    }
  }


  void setPage(int page) {
    currentPage = page;
    getLeague();
  }

  // Method to increase the page number
  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getLeague();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getLeague();
    }
  }

  void fetchProvinces() async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getAddresses();
      provinces.assignAll(result.where((address) => address.type == 'province').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchDistricts(String provinceCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getDistricts(provinceCode);
      districts.assignAll(
          result.where((address) => address.type == 'district').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchCommunes(String districtCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getCommunes(districtCode);
      communes.assignAll(
          result.where((address) => address.type == 'commune').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void fetchVillages(String communeCode) async {
    try {
      isLoadingProvinces = true;
      update();
      var result = await _leagueRepository.getVillages(communeCode);
      villages.assignAll(
          result.where((address) => address.type == 'village').toList());
    } catch (e) {
      // Handle error
    } finally {
      isLoadingProvinces = false;
      update();
    }
  }

  void selectProvince(Address? province) {
    selectedProvince = province;

    if (province != null) {

      if (selectedProvince!.code == province.code) {
        selectedDistrict = null;
        selectedCommune = null;
        selectedVillage = null;
      }
      fetchDistricts(province.code!);
    } else {
      districts.clear();
      communes.clear();
      villages.clear();
      selectedDistrict = null;
      selectedCommune = null;
      selectedVillage = null;
    }
    update();
  }

  void selectDistrict(Address? district) {
    selectedDistrict = district;
    if (district != null) {
      fetchCommunes(district.code!);
      selectedCommune = null;
    } else {
      communes.clear();
      selectedCommune = null;
    }
    update();
  }

  void selectCommune(Address? commune) {
    selectedCommune = commune;
    if (commune != null) {
      fetchVillages(commune.code!);
      selectedVillage = null;
    } else {
      villages.clear();
      selectedVillage = null;
    }
    update();
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getLeague();
  }


  void selectVillage(Address? village) {
    selectedVillage = village;
    // Add any additional logic here if needed
    update();
  }

  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getLeague();
    update();
  }
  Future<Map<String, dynamic>> deleteLeague({required String leagueID}) async {
    isLoading = true;
    update();
    var result = await _leagueRepository.deleteLeague(leagueID: leagueID);
    isLoading = false;
    update();
    return result;
  }
}