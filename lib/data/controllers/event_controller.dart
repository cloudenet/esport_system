import 'package:esport_system/data/model/event_model.dart';
import 'package:esport_system/data/repo/event_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class EventController extends GetxController implements GetxService {
  final EventRepo _eventRepo = EventRepo();

  TextEditingController titleController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController startDateController = TextEditingController();
  TextEditingController endDateController = TextEditingController();
  TextEditingController fromAgeController = TextEditingController();
  TextEditingController toAgeController = TextEditingController();

  List<EventModel> eventData = [];

  bool isLoading = true;
  int currentPage = 1;
  int limit = 6;
  int totalPages = 1;
  String searchQuery = '';


  Future<bool> createEvent(
      {required String title,
      required String address,
      required String description,
      required String startDate,
      required String endDate,
      required String createdBy}) async {
    isLoading = true;
    update();
    try {
      await _eventRepo.createEvent(
          title: title,
          address: address,
          description: description,
          startDate: startDate,
          endDate: endDate,
          createdBy: createdBy);
      isLoading = false;
      update();
      return true;
    } catch (e) {
      print("Error during create event $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<void> getEvent() async {
    isLoading = true;
    update();
    try {
      eventData = await _eventRepo.getEvent(
        search: searchQuery,page: currentPage,limit: limit
      );
      totalPages = eventData.length == limit ? currentPage + 1 : currentPage;
      isLoading = false;
    } catch (e) {
      print("Error During Get event $e");
      isLoading = false;
    }
    update();
  }

  Future<bool> updateEvent(
      {required String eventID,
      required String title,
      required String address,
      required String description,
      required String startDate,
      required String endDate,
      required String updatedBy}) async {
    isLoading = true;
    update();
    try {
      await _eventRepo.updateEvent(
          eventID: eventID,
          title: title,
          address: address,
          description: description,
          startDate: startDate,
          endDate: endDate,
          updatedBy: updatedBy);
      isLoading = false;
      update();
      return true;
    } catch (e) {
      print("=========Error : $e");
      isLoading = false;
      update();
      return false;
    }
  }

  Future<Map<String, dynamic>> deleteEvent({required String eventID}) async {
    isLoading = true;
    update();
    var result = await _eventRepo.deleteEven(eventID: eventID);
    isLoading = false;
    update();
    return result;
  }

  Future<void> getEventDetail({required String eventID}) async {
    isLoading = true;
    update();
    try {
      await _eventRepo.getEventDetail(eventID: eventID);
      isLoading = false;
      update();
    } catch (e) {
      print("Error During Get Detail Evnet $e");
      isLoading = false;
      update();
    }
  }

  void nextPage() {
    if (currentPage < totalPages) {
      currentPage++;
      getEvent();
    }
  }

  void previousPage() {
    if (currentPage > 1) {
      currentPage--;
      getEvent();
    }
  }

  void setLimit(int newLimit) {
    limit = newLimit;
    update();
    getEvent();
  }
  void search(String query) {
    searchQuery = query;
    currentPage = 1;
    getEvent();
    update();
  }
  void setPage(int page) {
    currentPage = page;
    getEvent();
  }
}
