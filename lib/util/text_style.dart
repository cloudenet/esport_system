// lib/text_styles.dart

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TextStylesHelper {
  static final TextStyle headline1 = GoogleFonts.roboto(
    fontSize: 32.0,
    fontWeight: FontWeight.bold,
    color: Colors.black,
  );

  static final TextStyle headline2 = GoogleFonts.roboto(
    fontSize: 28.0,
    fontWeight: FontWeight.w600,
    color: Colors.black,
  );

  static final TextStyle bodyText1 = GoogleFonts.lato(
    fontSize: 16.0,
    fontWeight: FontWeight.normal,
    color: Colors.black,
  );

  static final TextStyle bodyText2 = GoogleFonts.lato(
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    color: Colors.black54,
  );

  static final TextStyle caption = GoogleFonts.openSans(
    fontSize: 12.0,
    fontWeight: FontWeight.normal,
    color: Colors.black45,
  );
  static final TextStyle khmerH2 = GoogleFonts.moul(
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    color: Colors.white,
  );
  static final TextStyle khmerH1 = GoogleFonts.moul(
    fontSize: 16.0,
    fontWeight: FontWeight.normal,
    color: Colors.indigo,
  );
  static final TextStyle khmerH1Black = GoogleFonts.moul(
    fontSize: 16.0,
    fontWeight: FontWeight.normal,
    color: Colors.black,
  );
  static final TextStyle khmerH3 = GoogleFonts.moul(
    fontSize: 11.0,
    fontWeight: FontWeight.normal,
    color: Colors.indigo,
  );
  static final TextStyle khmerTextMoul = GoogleFonts.moul(
    fontSize: 9.0,
    fontWeight: FontWeight.bold,
    color: Colors.indigo,
  );
  static final TextStyle khmerTextNormal = GoogleFonts.battambang(
    fontSize: 13.0,
    fontWeight: FontWeight.normal,
    color: Colors.indigo,
  );

  static final TextStyle khmerText16 = GoogleFonts.battambang(
    fontSize: 16,
    fontWeight: FontWeight.normal,
  );
  static final TextStyle khmerTextBlack = GoogleFonts.battambang(
    fontSize: 16,
    fontWeight: FontWeight.normal,
    color: Colors.black
  );
  static final TextStyle khmerText18 = GoogleFonts.battambang(
    fontSize: 18,
    fontWeight: FontWeight.bold,
  );
}
