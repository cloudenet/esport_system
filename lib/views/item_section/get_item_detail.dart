import 'package:esport_system/data/controllers/item_controller.dart';
import 'package:esport_system/data/model/item_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class GetItemsDetailScreen extends StatefulWidget {
  final ItemModel? data;

  const GetItemsDetailScreen({super.key, this.data});

  @override
  State<GetItemsDetailScreen> createState() => _GetItemsDetailScreenState();
}

class _GetItemsDetailScreenState extends State<GetItemsDetailScreen> {

  String? selectType;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ItemController>(builder: (logic) {
        return SingleChildScrollView(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: .0, top: 20.0),
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.medical_information,
                        size: 40,
                      ),
                    ),
                    Text(
                      "item_detail".tr,
                      style: const TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                    vertical: 10, horizontal: 10),
                decoration: const BoxDecoration(
                  color: secondaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('player_name'.tr),
                              const SizedBox(height: 10,),
                              Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: bgColor,
                                ),
                                child: Center(
                                    child:
                                    Text(widget.data!.itemTitle.toString())
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('khmer_name'.tr),
                              const SizedBox(height: 10,),
                              Container(
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: bgColor,
                                ),
                                child: Center(
                                    child: Text(
                                        widget.data!.otherTitle.toString())
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('qty_items_stock'.tr),
                        const SizedBox(height: 10,),
                        Container(
                          height: Get.height * 0.15,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: bgColor,
                          ),
                          child: Center(
                            child: Text(
                              widget.data!.stockQty.toString(),
                              style: TextStyle(
                                fontSize: 32,
                                color: int.tryParse(
                                    widget.data!.stockQty.toString()) != null &&
                                    int.parse(
                                        widget.data!.stockQty.toString()) > 15
                                    ? Colors.green
                                    : int.tryParse(
                                    widget.data!.stockQty.toString()) != null &&
                                    int.parse(
                                        widget.data!.stockQty.toString()) > 0
                                    ? Colors.orange
                                    : Colors.red,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: .0, top: 20.0),
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(
                        Icons.history,
                        size: 40,
                      ),
                    ),
                    Text(
                      "item_history".tr,
                      style: const TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Responsive(
                  mobile: itemHistoryMobile(),
                  desktop: itemHistory(),
                  tablet: itemHistory(),
              ),
              const SizedBox(height: 10,),
              Container(
                  child: logic.isLoading
                      ? const Center(child: CircularProgressIndicator())
                      : (logic.listItemHistory.isEmpty
                      ? const EmptyData()
                      : LayoutBuilder(
                      builder: (context, constraints) {
                        return SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: ConstrainedBox(
                                  constraints: BoxConstraints(
                                    minWidth: constraints.maxWidth,
                                  ),
                                  child: DataTable(
                                      dataRowMaxHeight: Get.height * 0.1,
                                      headingRowColor:
                                      WidgetStateColor.resolveWith((
                                          states) => secondaryColor),
                                      columns: [
                                        DataColumn(
                                          label: Text(
                                            "no.".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "train_type".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "qty_items".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "date".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                      rows: logic.listItemHistory.isEmpty
                                          ? []
                                          : List<DataRow>.generate(
                                          logic.listItemHistory.length, (
                                          index) {
                                        var data = logic.listItemHistory[index];
                                        return DataRow(
                                            cells: [
                                              DataCell(
                                                Text(
                                                  _truncateText(
                                                      '${index + 1}', 5),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  _truncateText(
                                                      '${data.trainType}', 20),
                                                ),
                                              ),
                                              DataCell(
                                                Text('${data.qtyItems}',
                                                  style: TextStyle(
                                                    color: int.tryParse(
                                                        data.qtyItems!) !=
                                                        null && int.parse(
                                                        data.qtyItems!) > 0
                                                        ? Colors.green
                                                        : Colors.red,
                                                  ),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  _truncateText(
                                                      formatDate(data.trainDate
                                                          .toString()), 40
                                                  ),
                                                ),
                                              ),
                                            ]
                                        );
                                      }
                                      )
                                  )
                              )
                          ),
                        );
                      }
                  )
                  )
              )
            ],
          ),
        );
      }
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }

  Widget itemHistory() {
    return Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: FormBuilder(
          child: GetBuilder<ItemController>(builder: (logic) {
            return Column(
              children: [
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('train_type'.tr),
                          const SizedBox(height: 10,),
                          FormBuilderDropdown(
                            name: 'train_type'.tr,
                            initialValue: selectType,
                            borderRadius: BorderRadius.circular(10),
                            decoration: InputDecoration(
                              floatingLabelAlignment: FloatingLabelAlignment
                                  .center,
                              floatingLabelBehavior: FloatingLabelBehavior
                                  .never,
                              fillColor: bgColor,
                              labelText: 'please_select_train_type'.tr,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(10)
                              ),
                            ),
                            items: logic.type.map((option) =>
                                DropdownMenuItem(
                                  value: option['value'],
                                  child: Text(option['text']!.tr),
                                )).toList(),
                            onChanged: (value) {
                              setState(() {
                                selectType = value;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('from_date'.tr),
                          const SizedBox(height: 10,),
                          CustomFormBuilderTextField(
                            readOnly: true,
                            onTap: () =>
                                logic.selectFormDate(
                                    context, logic.fromDateController),
                            name: 'from_date'.tr,
                            controller: logic.fromDateController,
                            hintText: "from_date".tr,
                            icon: Icons.calendar_month_outlined,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('to_date'.tr),
                          const SizedBox(height: 10,),
                          CustomFormBuilderTextField(
                            readOnly: true,
                            onTap: () =>
                                logic.selectToDate(
                                    context, logic.toDateController),
                            name: 'to_date'.tr,
                            controller: logic.toDateController,
                            hintText: "to_date".tr,
                            icon: Icons.calendar_month_outlined,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 30
                        ),
                        child: OKButton(
                          btnName: 'search'.tr,
                          onPress: () async {
                            await logic.getItemDetailAndHistory(
                              itemID: widget.data!.id.toString(),
                              fromDate: logic.fromDateController.text
                                  .toString(),
                              toDate: logic.toDateController.text.toString(),
                              trainType: selectType.toString(),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ],
            );
          }),
        )
    );
  }

  Widget itemHistoryMobile(){
    return Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: FormBuilder(
          child: GetBuilder<ItemController>(builder: (logic) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start
              ,
              children: [
                Text('train_type'.tr),
                const SizedBox(height: 10,),
                FormBuilderDropdown(
                  name: 'train_type'.tr,
                  initialValue: selectType,
                  borderRadius: BorderRadius.circular(10),
                  decoration: InputDecoration(
                    floatingLabelAlignment: FloatingLabelAlignment
                        .center,
                    floatingLabelBehavior: FloatingLabelBehavior
                        .never,
                    fillColor: bgColor,
                    labelText: 'please_select_train_type'.tr,
                    filled: true,
                    border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(10)
                    ),
                  ),
                  items: logic.type.map((option) =>
                      DropdownMenuItem(
                        value: option['value'],
                        child: Text(option['text']!.tr),
                      )).toList(),
                  onChanged: (value) {
                    setState(() {
                      selectType = value;
                    });
                  },
                ),
                const SizedBox(height: 10),
                Text('from_date'.tr),
                const SizedBox(height: 10,),
                CustomFormBuilderTextField(
                  readOnly: true,
                  onTap: () =>
                      logic.selectFormDate(
                          context, logic.fromDateController),
                  name: 'from_date'.tr,
                  controller: logic.fromDateController,
                  hintText: "from_date".tr,
                  icon: Icons.calendar_month_outlined,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10),
                Text('to_date'.tr),
                const SizedBox(height: 10,),
                CustomFormBuilderTextField(
                  readOnly: true,
                  onTap: () =>
                      logic.selectToDate(
                          context, logic.toDateController),
                  name: 'to_date'.tr,
                  controller: logic.toDateController,
                  hintText: "to_date".tr,
                  icon: Icons.calendar_month_outlined,
                  fillColor: bgColor,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 30
                      ),
                      child: OKButton(
                        btnName: 'search'.tr,
                        onPress: () async {
                          await logic.getItemDetailAndHistory(
                            itemID: widget.data!.id.toString(),
                            fromDate: logic.fromDateController.text
                                .toString(),
                            toDate: logic.toDateController.text.toString(),
                            trainType: selectType.toString(),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ],
            );
          }),
        )
    );
  }
}
