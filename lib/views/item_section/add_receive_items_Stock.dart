import 'package:esport_system/data/controllers/item_controller.dart';
import 'package:esport_system/data/model/item_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/item_section/get_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class AddReceiveItemsStockScreen extends StatefulWidget {
  final ItemModel? data;
  const AddReceiveItemsStockScreen({super.key, this.data});

  @override
  State<AddReceiveItemsStockScreen> createState() => _AddReceiveItemsStockScreenState();
}

class _AddReceiveItemsStockScreenState extends State<AddReceiveItemsStockScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final itemController = Get.find<ItemController>();
  var homeController = Get.find<HomeController>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ItemController>(builder: (logic) {
        return Container(
          width: double.infinity,
          margin: const EdgeInsets.all(8.0),
          child: Responsive(
              mobile: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: .0, top: 20.0),
                      child: Row(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.medical_information,
                              size: 30,
                            ),
                          ),
                          Text(
                            "add_items_to_stock".tr,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    _buildItemsInfoSelection(),
                    Card(
                      color: secondaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FormBuilder(
                          key: _formKey,
                          child: Column(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'qty_items_to_stock'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 10.0),
                                  CustomFormBuilderTextField(
                                    name: 'qty_items'.tr,
                                    controller: logic.qtyItemsController,
                                    hintText: 'qty_items'.tr,
                                    errorText: 'qty_items_is_required'.tr,
                                    icon: Icons.sports_baseball,
                                    fillColor: bgColor,
                                  ),
                                ],
                              ),
                              const SizedBox(height: 10.0),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'note'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 10.0),
                                  CustomFormNote(
                                    name: 'note'.tr,
                                    controller: logic.notedController,
                                    minLine: 6,
                                    hintText: 'note'.tr,
                                    errorText: 'note_is_required'.tr,
                                    fillColor: bgColor,
                                  ),
                                ],
                              ),
                              const SizedBox(height: 10),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CancelButton(
                                    btnName: "cancel".tr,
                                    onPress: () {
                                      logic.qtyItemsController.text = '';
                                      logic.notedController.text = '';
                                      homeController.updateWidgetHome(const GetItemScreen());
                                    },
                                  ),
                                  const SizedBox(width: 16),
                                  OKButton(
                                    btnName: "save".tr,
                                    onPress: () async {
                                      print("object");
                                      if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                        logic.isLoading = true;
                                        logic.update();
                                        EasyLoading.show(
                                          status: 'loading...'.tr,
                                          dismissOnTap: true,
                                        );
                                        await logic.addItemsStock(
                                          itemID: widget.data!.id.toString(),
                                          qtyStock: logic.qtyItemsController.text,
                                          noted: logic.notedController.text,
                                        ).then((status) {
                                          if (status) {
                                            EasyLoading.showSuccess(
                                              'add_qty_to_stock_successfully'.tr,
                                              dismissOnTap: true,
                                              duration: const Duration(seconds: 2),
                                            );
                                            logic.qtyItemsController.text = '';
                                            logic.notedController.text = '';
                                            homeController.updateWidgetHome(const GetItemScreen());
                                          } else {
                                            EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
                                          }
                                          EasyLoading.dismiss();
                                        });
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                
                  ],
                ),
              ),
              tablet: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: .0, top: 20.0),
                      child: Row(
                        children: [
                          const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.medical_information,
                              size: 30,
                            ),
                          ),
                          Text(
                            "add_items_to_stock".tr,
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    _buildItemsInfoSelection(),
                    Card(
                      color: secondaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FormBuilder(
                          key: _formKey,
                          child: Column(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'qty_items_to_stock'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 10.0),
                                  CustomFormBuilderTextField(
                                    name: 'qty_items'.tr,
                                    controller: logic.qtyItemsController,
                                    hintText: 'qty_items'.tr,
                                    errorText: 'qty_items_is_required'.tr,
                                    icon: Icons.sports_baseball,
                                    fillColor: bgColor,
                                  ),
                                ],
                              ),
                              const SizedBox(height: 10.0),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'note'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 10.0),
                                  CustomFormNote(
                                    name: 'note'.tr,
                                    controller: logic.notedController,
                                    minLine: 5,
                                    hintText: 'note'.tr,
                                    errorText: 'note_is_required'.tr,
                                    fillColor: bgColor,
                                  ),
                                ],
                              ),
                              const SizedBox(height: 10),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CancelButton(
                                    btnName: "cancel".tr,
                                    onPress: () {
                                      logic.qtyItemsController.text = '';
                                      logic.notedController.text = '';
                                      homeController.updateWidgetHome(const GetItemScreen());
                                    },
                                  ),
                                  const SizedBox(width: 16),
                                  OKButton(
                                    btnName: "save".tr,
                                    onPress: () async {
                                      print("object");
                                      if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                        logic.isLoading = true;
                                        logic.update();
                                        EasyLoading.show(
                                          status: 'loading...'.tr,
                                          dismissOnTap: true,
                                        );
                                        await logic.addItemsStock(
                                          itemID: widget.data!.id.toString(),
                                          qtyStock: logic.qtyItemsController.text,
                                          noted: logic.notedController.text,
                                        ).then((status) {
                                          if (status) {
                                            EasyLoading.showSuccess(
                                              'add_qty_to_stock_successfully'.tr,
                                              dismissOnTap: true,
                                              duration: const Duration(seconds: 2),
                                            );
                                            logic.qtyItemsController.text = '';
                                            logic.notedController.text = '';
                                            homeController.updateWidgetHome(const GetItemScreen());
                                          } else {
                                            EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
                                          }
                                          EasyLoading.dismiss();
                                        });
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
              desktop: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: .0, top: 20.0),
                    child: Row(
                      children: [
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.medical_information,
                            size: 40,
                          ),
                        ),
                        Text(
                          "add_items_to_stock".tr,
                          style: const TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(child: _buildItemsInfoSelection()),
                      Expanded(
                        flex: 2,
                        child: Card(
                          color: secondaryColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: FormBuilder(
                              key: _formKey,
                              child: Column(
                                children:  [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'qty_items_to_stock'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      const SizedBox(height: 10.0),
                                      CustomFormBuilderTextField(
                                        name: 'qty_items'.tr,
                                        controller: logic.qtyItemsController,
                                        hintText: 'qty_items'.tr,
                                        errorText: 'qty_items_is_required'.tr,
                                        icon: Icons.sports_baseball,
                                        fillColor: bgColor,
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 10.0),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'note'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      const SizedBox(height: 10.0),
                                      CustomFormNote(
                                        name: 'note'.tr,
                                        controller: logic.notedController,
                                        minLine: 5,
                                        hintText: 'note'.tr,
                                        errorText: 'note_is_required'.tr,
                                        fillColor: bgColor,
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 15),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      CancelButton(
                                        btnName: "cancel".tr,
                                        onPress: () {
                                          logic.qtyItemsController.text = '';
                                          logic.notedController.text = '';
                                          homeController.updateWidgetHome(const GetItemScreen());
                                        },
                                      ),
                                      const SizedBox(width: 16),
                                      OKButton(
                                        btnName: "save".tr,
                                        onPress: () async {
                                          print("object");
                                          if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                            logic.isLoading = true;
                                            logic.update();
                                            EasyLoading.show(
                                              status: 'loading...'.tr,
                                              dismissOnTap: true,
                                            );
                                            await logic.addItemsStock(
                                              itemID: widget.data!.id.toString(),
                                              qtyStock: logic.qtyItemsController.text,
                                              noted: logic.notedController.text,
                                            ).then((status) {
                                              if (status) {
                                                EasyLoading.showSuccess(
                                                  'add_qty_to_stock_successfully'.tr,
                                                  dismissOnTap: true,
                                                  duration: const Duration(seconds: 2),
                                                );
                                                logic.qtyItemsController.text = '';
                                                logic.notedController.text = '';
                                                homeController.updateWidgetHome(const GetItemScreen());
                                              } else {
                                                EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
                                              }
                                              EasyLoading.dismiss();
                                            });
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 5),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              )
          ),
        );
      }),
    );
  }


  Widget _buildItemsInfoSelection() {
    return Card(
      color: secondaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:  Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildText(
                    title: "title".tr,
                    textData: "${widget.data!.itemTitle}",
                    height: 50,
                    alignment: Alignment.center
                ),
                const SizedBox(height: 10),
                buildText(
                    title: "other_title".tr,
                    textData: "${widget.data!.itemTitle}",
                    height: 50,
                    alignment: Alignment.center
                ),
                const SizedBox(height: 10),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('qty_items_stock'.tr),
                const SizedBox(height: 10,),
                Container(
                  height: Get.height * 0.15,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: bgColor,
                  ),
                  child: Center(
                    child: Text(
                      widget.data!.stockQty.toString(),
                      style: TextStyle(
                        fontSize: 32,
                        color: int.tryParse(widget.data!.stockQty.toString()) != null && int.parse(widget.data!.stockQty.toString()) > 15
                            ? Colors.green
                            : int.tryParse(widget.data!.stockQty.toString()) != null && int.parse(widget.data!.stockQty.toString()) > 0
                            ? Colors.orange
                            : Colors.red,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
