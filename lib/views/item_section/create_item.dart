import 'package:esport_system/data/controllers/item_controller.dart';
import 'package:esport_system/data/model/item_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/item_section/get_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CreateItemScreen extends StatefulWidget {
  final ItemModel? data;

  const CreateItemScreen({super.key, this.data});

  @override
  State<CreateItemScreen> createState() => _CreateItemScreenState();
}

class _CreateItemScreenState extends State<CreateItemScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final itemController = Get.find<ItemController>();
  var homeController = Get.find<HomeController>();
  final List<Map<String, String>> status = [
    {'value': 'active', 'text': 'Active'},
    {'value': 'inactive', 'text': 'Inactive'},
  ];

  String? selectStatus;

  @override
  void initState() {
    if (widget.data != null) {
      itemController.titleController.text = widget.data!.itemTitle.toString();
      itemController.subTitleController.text = widget.data!.otherTitle.toString();
      selectStatus = widget.data!.isStatus.toString();
    }else{
      itemController.titleController.text = "";
      itemController.subTitleController.text = "";
      selectStatus = status.first['value'];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ItemController>(builder: (logic) {
        return Container(
          width: Get.width,
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FormBuilder(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.data != null
                              ? 'update_item'.tr
                              : 'create_new_item'.tr,
                          style: const TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 20.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                             Text(
                              'item_title'.tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            const SizedBox(height: 5.0),
                            CustomFormBuilderTextField(
                              name: 'item_title'.tr,
                              controller: logic.titleController,
                              hintText: 'item_title'.tr,
                              errorText: 'item_title_is_required'.tr,
                              icon: Icons.description,
                              fillColor: secondaryColor,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                   Text(
                                    'other_title'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 5.0),
                                  CustomFormBuilderTextField(
                                    name: 'other_title'.tr,
                                    controller: logic.subTitleController,
                                    hintText: 'other_title'.tr,
                                    errorText: 'other_title_is_required'.tr,
                                    icon: Icons.description,
                                    fillColor: secondaryColor,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                   Text(
                                    'select_status'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 5.0),
                                  FormBuilderDropdown(
                                    name: 'status'.tr,
                                    initialValue: selectStatus,
                                    borderRadius:  BorderRadius.circular(10),
                                    decoration: InputDecoration(
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      floatingLabelBehavior: FloatingLabelBehavior.never,
                                      fillColor: secondaryColor,
                                      labelText: 'please_select_status'.tr,
                                      filled: true,
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(10)
                                      ),
                                    ),
                                    items: status.map((option) => DropdownMenuItem(
                                      value: option['value'],
                                      child: Text(option['text']!.tr),
                                    )).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        selectStatus = value;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            CancelButton(
                                btnName: 'cancel'.tr,
                                onPress: () {
                                  selectStatus = null;
                                  logic.titleController.text = '';
                                  logic.subTitleController.text = '';
                                  homeController.updateWidgetHome(
                                      const GetItemScreen());
                                }),
                            const SizedBox(
                              width: 16,
                            ),
                            OKButton(
                              btnName: widget.data != null
                                  ? 'update'.tr
                                  : 'create'.tr,
                              onPress: () async {
                                if (_formKey.currentState != null &&
                                    _formKey.currentState!
                                        .saveAndValidate()) {
                                  logic.isLoading = true;
                                  logic.update();
                                  EasyLoading.show(
                                    status: 'loading...'.tr,
                                    dismissOnTap: true,
                                  );
                                  if (widget.data != null) {
                                    await logic
                                        .updateItem(
                                            itemID: widget.data!.id
                                                .toString(),
                                            itemTitle: logic
                                                .titleController.text,
                                            otherTitle: logic
                                                .subTitleController.text,
                                            isStatus:
                                                selectStatus.toString())
                                        .then((value) {
                                      if (value) {
                                        EasyLoading.showSuccess(
                                          'item_update_successfully'.tr,
                                          dismissOnTap: true,
                                          duration:
                                              const Duration(seconds: 2),
                                        );
                                        homeController.updateWidgetHome(
                                            const GetItemScreen());
                                      } else {
                                        EasyLoading.showError(
                                            'something_went_wrong'.tr,
                                            duration: const Duration(
                                                seconds: 2));
                                      }
                                      EasyLoading.dismiss();
                                    });
                                  } else {
                                    await logic
                                        .createItem(
                                            itemTitle: logic
                                                .titleController.text,
                                            subTitle: logic
                                                .subTitleController.text,
                                            status:
                                                selectStatus.toString())
                                        .then((value) {
                                      if (value) {
                                        EasyLoading.showSuccess(
                                          'item_create_successfully'.tr,
                                          dismissOnTap: true,
                                          duration:
                                              const Duration(seconds: 2),
                                        );
                                        homeController.updateWidgetHome(
                                            const GetItemScreen());
                                      } else {
                                        EasyLoading.showError(
                                            'something_went_wrong'.tr,
                                            duration: const Duration(
                                                seconds: 2));
                                      }
                                      EasyLoading.dismiss();
                                    });
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
