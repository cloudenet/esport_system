import 'package:esport_system/data/model/position_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/position_section/get_position_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class PositionDetail extends StatefulWidget {
  final PositionModel positionModel;
  const PositionDetail({super.key, required this.positionModel});

  @override
  State<PositionDetail> createState() => _PositionDetailState();
}

class _PositionDetailState extends State<PositionDetail> {
  var homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Responsive(
      tablet: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                width: 150,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: Image.asset(
                  'assets/icons/position.png',
                  color: Colors.white,
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('position_detail'.tr,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 24),
                      buildText(
                        title: 'title'.tr,
                        textData: widget.positionModel.title != "null" ? widget.positionModel.title.toString() : "N/A",
                        height: 60,
                        padding: 10,
                        alignment: Alignment.centerLeft,
                      ),
                      const SizedBox(height: 16,),
                      buildText(
                        title: 'description'.tr,
                        textData: widget.positionModel.description != "null" ? widget.positionModel.description.toString() : "N/A",
                        height: 60,
                        padding: 10,
                        alignment: Alignment.centerLeft,
                      ),
                      const SizedBox(height: 24),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(const GetPositionScreen());
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      mobile: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: SingleChildScrollView(
          child: Container(
            decoration: const BoxDecoration(
              color: secondaryColor,
            ),
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('position_detail'.tr,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 24),
                buildText(
                  title: 'title'.tr,
                  textData: widget.positionModel.title != "null" ? widget.positionModel.title.toString() : "N/A",
                  height: 60,
                  padding: 10,
                  alignment: Alignment.centerLeft,
                ),
                const SizedBox(height: 16,),
                buildText(
                  title: 'description'.tr,
                  textData: widget.positionModel.description != "null" ? widget.positionModel.description.toString() : "N/A",
                  height: 60,
                  padding: 10,
                  alignment: Alignment.centerLeft,
                ),
                const SizedBox(height: 24),
                Align(
                  alignment: Alignment.bottomRight,
                  child: CancelButton(
                    btnName: 'close'.tr,
                    onPress: () {
                      homeController.updateWidgetHome(const GetPositionScreen());
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      desktop: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                width: 150,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: Image.asset(
                  'assets/icons/position.png',
                  color: Colors.white,
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('position_detail'.tr,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 24),
                      buildText(
                        title: 'title'.tr,
                        textData: widget.positionModel.title != "null" ? widget.positionModel.title.toString() : "N/A",
                        height: 60,
                        padding: 10,
                        alignment: Alignment.centerLeft,
                      ),
                      const SizedBox(height: 16,),
                      buildText(
                        title: 'description'.tr,
                        textData: widget.positionModel.description != "null" ? widget.positionModel.description.toString() : "N/A",
                        height: 60,
                        padding: 10,
                        alignment: Alignment.centerLeft,
                      ),
                      const SizedBox(height: 24),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(const GetPositionScreen());
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
