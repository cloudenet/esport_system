import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/model/position_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/position_section/get_position_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CreateAndUpdatePosition extends StatelessWidget {
  final PositionModel? data;

  CreateAndUpdatePosition({Key? key, this.data}) : super(key: key);

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    var homeController = Get.find<HomeController>();

    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<PositionController>(builder: (logic) {
        if (data != null) {
          logic.titleController.text = data!.title.toString();
          logic.descriptionController.text = data!.description.toString();
        } else {
          logic.titleController.clear();
          logic.descriptionController.clear();
        }
        return Container(
          width: double.infinity,
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    FormBuilder(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data == null
                                ? 'create_a_new_position'.tr
                                : 'update_position'.tr,
                            style: const TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 20.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'title'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              CustomFormBuilderTextField(
                                name: 'title'.tr,
                                controller: logic.titleController,
                                hintText: 'title'.tr,
                                errorText: 'title_is_required'.tr,
                                icon: Icons.text_fields,
                                fillColor: secondaryColor,
                              ),
                            ],
                          ),
                          const SizedBox(height: 20.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'description'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              CustomFormBuilderTextField(
                                name: 'description'.tr,
                                controller: logic.descriptionController,
                                hintText: 'description'.tr,
                                errorText: 'description_is_required'.tr,
                                icon: Icons.description,
                                fillColor: secondaryColor,
                              ),
                            ],
                          ),
                          const SizedBox(height: 24.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              CancelButton(
                                  btnName: 'cancel'.tr,
                                  onPress: () {
                                    if (data == null) {
                                      logic.clearTextField();
                                      homeController.updateWidgetHome(
                                          const GetPositionScreen());
                                    } else {
                                      homeController.updateWidgetHome(
                                          const GetPositionScreen());
                                    }
                                  }),
                              const SizedBox(
                                width: 16,
                              ),
                              OKButton(
                                  btnName: data == null
                                      ? 'create'.tr
                                      : 'update'.tr,
                                  onPress: () async {
                                    if (_formKey.currentState != null &&
                                        _formKey.currentState!
                                            .saveAndValidate()) {
                                      if(data==null){
                                        logic.createPosition();
                                      }else{
                                        logic.updatePosition(positionId: data!.id.toString(),);
                                      }
                                    }
                                  }),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
