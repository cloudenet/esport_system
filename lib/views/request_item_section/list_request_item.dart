

import 'package:esport_system/data/controllers/item_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/views/request_item_section/create_request_item.dart';
import 'package:esport_system/views/request_item_section/detail_request_item.dart';
import 'package:esport_system/views/request_item_section/recieve_or_completed_request.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';
import '../../data/controllers/request_item_controller.dart';
import '../../helper/add_new_button.dart';
import '../../helper/constants.dart';
import '../../helper/empty_data.dart';
import '../../helper/route_helper.dart';
import '../home_section/screen/home.dart';


class ListRequestItem extends StatefulWidget {
  const ListRequestItem({super.key});
  @override
  State<ListRequestItem> createState() => _ListRequestItemState();
}

class _ListRequestItemState extends State<ListRequestItem> {

  var requestItemController = Get.find<RequestItemController>();
  var itemController = Get.find<ItemController>();
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    teamController.getTeamDropdown();
    itemController.getItem();
    requestItemController.getItemRequest();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<RequestItemController>(
        builder: (logic) {
          return Container(
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            child: Column(
              children: [
                _buildDropdown(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 32,
                            height: 32,
                            child: Image.asset(
                              "assets/icons/item.png",
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(width: 8), // Add spacing between the icon and text
                          Text(
                            'request_item'.tr,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: AddNewButton(
                        btnName: "add_new".tr,
                        onPress: () {
                          print("------------>Do request ");
                          homeController.updateWidgetHome(const CreateRequestItem(updateItem: false,));
                          //homeController.updateWidgetHome(PreviewCardPdf());
                        },
                      ),
                    ),
                  ],
                ),

                const SizedBox(height: 22,),
                Expanded(
                  child: logic.isLoading
                      ? const Center(child: CircularProgressIndicator())
                      : LayoutBuilder(builder: (context, constraints) {
                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minWidth: constraints.maxWidth,
                              ),
                              child: DataTable(
                                dataRowMaxHeight: Get.height * 0.1,
                                headingRowColor: WidgetStateColor.resolveWith((states) => secondaryColor),
                                columns: [
                                  DataColumn(
                                    label: Text(
                                      "no.".tr,
                                      style:  const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "reference_no.".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "team".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "created_by".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "status".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "actions".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                                rows: logic.listItemReq.isEmpty
                                ?[]
                                :List<DataRow>.generate(
                                  logic.listItemReq.length,
                                      (index) {
                                    var list = logic.listItemReq[index];
                                    return DataRow(
                                      cells: [
                                        DataCell(
                                          Text(_truncateText('${index + 1}', 10)),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText(list.refNumber ?? "N/A", 20),
                                          ),
                                        ),

                                        DataCell(
                                          Text(
                                            _truncateText(list.team?.teamName ?? "N/A", 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            list.createdBy?.name ?? "N/A",
                                          ),
                                        ),
                                        DataCell(
                                          Container(
                                            width: MediaQuery.of(context).size.width* 0.05,
                                            height: MediaQuery.of(context).size.height* 0.032,
                                            decoration: BoxDecoration(
                                                border: Border.all(width: 1, color:
                                                list.approvedStatus! == AppConstants.pending ? Colors.amberAccent
                                                    : list.approvedStatus! == AppConstants.completed ?Colors.green
                                                    : list.approvedStatus! == AppConstants.received ?Colors.blue
                                                    : list.approvedStatus! == AppConstants.cancelled ?Colors.red
                                                    : Colors.amberAccent
                                                ),
                                                borderRadius: BorderRadius.circular(5),
                                                color: list.approvedStatus! == AppConstants.pending ? Colors.amberAccent.withOpacity(0.1)
                                                    : list.approvedStatus! == AppConstants.completed ?Colors.green.withOpacity(0.1)
                                                    : list.approvedStatus! == AppConstants.received ?Colors.blue.withOpacity(0.1)
                                                    : list.approvedStatus! == AppConstants.cancelled ?Colors.red.withOpacity(0.1)
                                                    : Colors.amberAccent.withOpacity(0.1)
                                            ),
                                            child: Center(
                                              child: Text(
                                                list.approvedStatus! == AppConstants.pending ? "Pending"
                                                    :list.approvedStatus! == AppConstants.completed ? "Approve"
                                                    :list.approvedStatus! == AppConstants.received ? "Received"
                                                    :list.approvedStatus! == AppConstants.cancelled ? "Reject":"",
                                                style: TextStyle(
                                                    color: list.approvedStatus! == AppConstants.pending ? Colors.amberAccent
                                                        : list.approvedStatus! == AppConstants.completed ?Colors.green
                                                        : list.approvedStatus! == AppConstants.received ?Colors.blue
                                                        : list.approvedStatus! == AppConstants.cancelled ?Colors.red
                                                        : Colors.amberAccent
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        DataCell(
                                          Row(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  homeController.updateWidgetHome(DetailRequestItem(itemRequestModel: list,));
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.amberAccent),
                                                      color: Colors.amberAccent.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.visibility_sharp,
                                                      size: 20,
                                                      color: Colors.amberAccent,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  if(list.isReceived == "0"){
                                                    requestItemController.setPreviousValue(list);
                                                    homeController.updateWidgetHome(CreateRequestItem(updateItem: true,itemRequestModel: list,));
                                                  }
                                                  else if(list.isReceived == "1"){
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                      context) {
                                                        return AlertDialog(
                                                          backgroundColor: bgColor,
                                                          content: Container(
                                                            width:Get.width * 0.3,
                                                            height:Get.height * 0.3,
                                                            padding: const EdgeInsets.all( 16),
                                                            child: Column(
                                                              children: [
                                                                Align(
                                                                  alignment: Alignment.topCenter,
                                                                  child: Image .asset("assets/images/warning.png", width:50, height:50,),
                                                                ),
                                                                const SizedBox(  height: 15),
                                                                Text("this_request_was_received".tr, style: Theme.of(context).textTheme.bodyMedium
                                                                ),
                                                                const SizedBox( height: 15),
                                                                const Spacer(),
                                                                Align(
                                                                  alignment:Alignment.bottomRight,
                                                                  child: CancelButton(
                                                                    btnName: 'cancel'.tr,
                                                                    onPress: () {
                                                                      Get.back();
                                                                    },),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  }
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.green),
                                                      color: Colors.green.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.edit,
                                                      size: 20,
                                                      color: Colors.green,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  if(list.approvedStatus == AppConstants.cancelled){
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                      context) {
                                                        return AlertDialog(
                                                          backgroundColor: bgColor,
                                                          content: Container(
                                                            width:Get.width * 0.3,
                                                            height:Get.height * 0.3,
                                                            padding: const EdgeInsets.all( 16),
                                                            child: Column(
                                                              children: [
                                                                Align(
                                                                  alignment: Alignment.topCenter,
                                                                  child: Image .asset("assets/images/warning.png", width:50, height:50,),
                                                                ),
                                                                const SizedBox(  height: 15),
                                                                Text("this_request_was_cancelled".tr, style: Theme.of(context).textTheme.bodyMedium
                                                                ),
                                                                const SizedBox( height: 15),
                                                                const Spacer(),
                                                                Align(
                                                                  alignment:Alignment.bottomRight,
                                                                  child: CancelButton(
                                                                    btnName: 'cancel'.tr,
                                                                    onPress: () {
                                                                      Get.back();
                                                                    },),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  }else{
                                                    requestItemController.setPreviousValue(list);
                                                    homeController.updateWidgetHome(RecieveOrCompletedRequest(approved: true, itemRequestModel: list));
                                                  }
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.blue),
                                                      color: Colors.blue.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.check,
                                                      size: 20,
                                                      color: Colors.blue,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  if(list.isReceived == "0"){
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                      context) {
                                                        return AlertDialog(
                                                          backgroundColor: bgColor,
                                                          content: Container(
                                                            width:Get.width * 0.3,
                                                            height:Get.height * 0.3,
                                                            padding: const EdgeInsets.all( 16),
                                                            child: Column(
                                                              children: [
                                                                Align(
                                                                  alignment: Alignment.topCenter,
                                                                  child: Image .asset("assets/images/warning.png", width:50, height:50,),
                                                                ),
                                                                const SizedBox(  height: 15),
                                                                Text("are_you_sure_you_want_to_rejected_this_request".tr, style: Theme.of(context).textTheme.bodyMedium
                                                                ),
                                                                const SizedBox( height: 15),
                                                                const Spacer(),
                                                                Align(
                                                                  alignment:Alignment.bottomRight,
                                                                  child: Row(
                                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                                    children: [
                                                                      CancelButton(
                                                                        btnName: 'cancel'.tr,
                                                                        onPress: () {
                                                                          Get.back();
                                                                        },),
                                                                      const SizedBox(width: 10),
                                                                      OKButton(
                                                                        btnName: 'confirm'.tr,
                                                                        onPress: (){
                                                                          logic.rejectRequest(rqId: list.id);
                                                                          Get.back();
                                                                        },
                                                                      )
                                                                    ],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  }
                                                  else{
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                      context) {
                                                        return AlertDialog(
                                                          backgroundColor: bgColor,
                                                          content: Container(
                                                            width:Get.width * 0.3,
                                                            height:Get.height * 0.3,
                                                            padding: const EdgeInsets.all( 16),
                                                            child: Column(
                                                              children: [
                                                                Align(
                                                                  alignment: Alignment.topCenter,
                                                                  child: Image .asset("assets/images/warning.png", width:50, height:50,),
                                                                ),
                                                                const SizedBox(  height: 15),
                                                                Text("this_request_was_received".tr, style: Theme.of(context).textTheme.bodyMedium
                                                                ),
                                                                const SizedBox( height: 15),
                                                                const Spacer(),
                                                                Align(
                                                                  alignment:Alignment.bottomRight,
                                                                  child: CancelButton(
                                                                    btnName: 'cancel'.tr,
                                                                    onPress: () {
                                                                      Get.back();
                                                                    },),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  }
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.red),
                                                      color: Colors.red.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.highlight_remove_sharp,
                                                      size: 20,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),

                              ),
                            ),
                          ),
                          if (logic.listItemReq.isEmpty)...[
                            const SizedBox(height: 50,),
                            const Center(child: EmptyData()),
                          ]
                        ],
                      ),
                    );
                  },
                  ),
                ),

                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 700,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            onPressed: ()=>logic.previousPage(),
                            icon: const Icon(Icons.arrow_back_ios),
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                for (int i = 1; i <= logic.totalPages; i++)
                                  Padding(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        TextButton(
                                          onPressed: () => logic.setPage(i),
                                          child: Text(
                                            i.toString(),
                                            style: TextStyle(
                                              color: logic.currentPage == i
                                                  ? Colors.blue
                                                  : Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: ()=>logic.nextPage(),
                            icon: const Icon(Icons.arrow_forward_ios),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildDropdown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: requestItemController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  requestItemController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: _searchField(onChange: (value) {
              requestItemController.search(value);
            },),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
  Widget _searchField({
    required Function(String) onChange,
  }) {
    final TextEditingController controller = TextEditingController();
    return Row(
      children: [
        Expanded(
          flex: 4,
          child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SizedBox(
                height: 48,
                child: TextField(
                  controller: controller,
                  onChanged: (value) {
                    onChange(value);
                  },
                  decoration: InputDecoration(
                    fillColor: bgColor,
                    hintStyle: const TextStyle(color: Colors.white),
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    contentPadding: const EdgeInsets.only(left: 12), // Set the horizontal padding
                    suffixIcon: DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(10),
                      value: requestItemController.selectedValue,
                      items: requestItemController.dropdownItems.map((item) {
                        return DropdownMenuItem<String>(
                          value: item,
                          child: Text(item),
                        );
                      }).toList(),
                      onChanged: (value) {
                        if(value != null){
                          setState(() {
                            requestItemController.selectValueDropdown(value);
                            controller.text = value;
                            onChange(value);
                          });
                        }
                      },
                      decoration: InputDecoration(
                        labelText: "select_status_to_search".tr,
                        floatingLabelAlignment: FloatingLabelAlignment.center,
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        fillColor: secondaryColor,
                        filled: true,
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        requestItemController.selectedValue != null ? const SizedBox(width: 10,):const SizedBox(),
        if (requestItemController.selectedValue != null) // Change the condition here
          GestureDetector(
            onTap: () {
              setState(() {
                requestItemController.selectedValue = null;
                controller.clear();
                onChange('');
              });
            },
            child: Container(
                height: 48,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor
                ),
                child: const Icon(Icons.clear,color: Colors.red,)
            ),
          ),
      ],
    );
  }
}

