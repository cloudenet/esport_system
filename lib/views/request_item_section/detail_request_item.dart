

import 'package:esport_system/data/controllers/request_item_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../data/model/item_request_model.dart';
import '../../util/text_style.dart';

class DetailRequestItem extends StatefulWidget {
  final ItemRequestModel? itemRequestModel;
  const DetailRequestItem({super.key,required this.itemRequestModel});

  @override
  State<DetailRequestItem> createState() => _DetailRequestItemState();
}

class _DetailRequestItemState extends State<DetailRequestItem> {
  static const double a4Width = 794.0;

  var requestController = Get.find<RequestItemController>();
  var userController = Get.find<UserController>();



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: bgColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("save_image".tr),
                  IconButton(
                    onPressed: () async {
                      await userController.renderWidgetsToImages([
                        userController.globalKeyReport,
                      ]);
                    },
                    icon: const Icon(Icons.save_alt),
                  ),

                ],
              ),
              Center(
                child:  RepaintBoundary(
                  key: userController.globalKeyReport,
                  child: Container(
                    color: Colors.white,
                    //color: secondaryColor,
                    width: a4Width,
                    // height: a4Height,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Column(
                              children: [
                                Image.asset('assets/images/Boules.png', height: 80,),
                                const SizedBox(height: 10.0),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        'សហព័ន្ធកីឡាប៊ុល និងប៉េតង់កម្ពុជា',
                                        style: TextStylesHelper.khmerH1Black,
                                        textAlign: TextAlign.center,
                                      ),
                                      const Text(
                                        'Federation of Boules and Pétanque of Cambodia',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 24),
                                Text('សូមគោរពជូន',
                                  style: TextStylesHelper.khmerH1Black,
                                  textAlign: TextAlign.center,),
                                Text('ឯកឧត្តមប្រធានសហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា',
                                  style: TextStylesHelper.khmerH1Black,
                                  textAlign: TextAlign.center,),
                                const SizedBox(height: 15),
                              ],
                            ),
                          ),
                          const SizedBox(height: 24),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('កម្មវត្ថុ៖      ............................................................................................................................................................',
                                style: TextStylesHelper.khmerTextBlack,
                                textAlign: TextAlign.start,),
                              const SizedBox(height: 5),
                              Text('យោង៖      ............................................................................................................................................................',
                                style: TextStylesHelper.khmerTextBlack,
                                textAlign: TextAlign.start,),
                              const SizedBox(height: 5),
                              Padding(
                                padding: const EdgeInsets.only(left: 77),
                                child: Text('សេចក្ដីដូចមានចែងក្នុងកម្មវត្ថុ និងយោងខាងលើ ខ្ញុំមានកិត្តិយសសូមគោរពជម្រាបជូន ឯកឧត្តមប្រធាន មេត្តា',
                                  style: TextStylesHelper.khmerTextBlack,
                                  textAlign: TextAlign.start,),
                              ),
                              const SizedBox(height: 5),
                              Text('ជ្រាបថា (ខ្លឹមសារលិខិត)................................................................................................................................................',
                                style: TextStylesHelper.khmerTextBlack,
                                textAlign: TextAlign.start,),
                              const SizedBox(height: 5),
                            ],
                          ),
                          const SizedBox(height: 24),

                          Row(
                            children: [
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: buildAddressColumn('request_by'.tr),
                                ),
                              ),
                              Expanded(
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: buildAddressColumnRight('team'.tr),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 24),
                          Text("Referral Number ${widget.itemRequestModel!.refNumber ?? "N/A"}",style: const TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
                          const SizedBox(height: 16),
                          Table(
                            columnWidths: const {
                              0: FlexColumnWidth(3),
                              1: FlexColumnWidth(3),
                              2: FlexColumnWidth(1),
                              3: FlexColumnWidth(2),
                              4: FlexColumnWidth(2),
                            },
                            children: [
                              TableRow(
                                decoration: const BoxDecoration(color: Colors.indigo ,),
                                children: [
                                  tableCell('Item Title', isHeader: true),
                                  tableCell('Sub title', isHeader: true),
                                  //tableCell('Note', isHeader: true),
                                  tableCell('Qty', isHeader: true),
                                  tableCell('Qty Received', isHeader: true),
                                  tableCell('Qty Remaining', isHeader: true),
                                ],
                              ),
                              // Data rows
                              for (var item in widget.itemRequestModel!.itemDetails!)
                              TableRow(
                              decoration: BoxDecoration(color: Colors.grey.shade100),
                              children: [
                                tableCell(item.item!.itemTitle.toString()),  // 'Item Title' column
                                tableCell(item.item!.otherTitle.toString()), // 'Sub title' column
                                //tableCell(item.note?.isNotEmpty == true ? item.note! : "-"),   // 'Status' column
                                tableCell(" ${item.qty.toString()}"),              // 'Qty' column
                                tableCell(" ${item.qtyReceived.toString()}"),              // 'Qty' column
                                tableCell(" ${item.qtyRemaining.toString()}"),              // 'Qty' column
                              ],
                              )
                            ],
                          ),
                          const SizedBox(height: 24),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('សេចក្ដីដូចបានជម្រាបជូនខាងលើ សូម ឯកឧត្តមប្រធាន មេត្តាពិនិត្យ  និងអញ្ជើញចូលរួមដោយក្ដីអនុគ្រោះ។',
                                  style: TextStylesHelper.khmerTextBlack,
                                  textAlign: TextAlign.start,),
                                Text('សូម ឯកឧត្តមប្រធានសហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា ទទួលនូវការគោរពពីខ្ញុំ៕',
                                  style: TextStylesHelper.khmerTextBlack,
                                  textAlign: TextAlign.start,),
                              ],
                            ),
                          ),
                          const SizedBox(height: 24),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('ថ្ងៃ ${requestController.getKhmerDayOfWeek(widget.itemRequestModel!.createdAt)} ខែ ${requestController.getMonthKhmer(widget.itemRequestModel!.createdAt)} ឆ្នាំ ${requestController.getKhmerYear(widget.itemRequestModel!.createdAt)} ព.ស.២៥៦.....',
                                  style: TextStylesHelper.khmerTextBlack,
                                  textAlign: TextAlign.start,),
                                const SizedBox(height: 5),
                                Text('រាជធានី-ខេត្ត.........ថ្ងៃទី.........ខែ.........ឆ្នាំ២០២...',
                                  style: TextStylesHelper.khmerTextBlack,
                                  textAlign: TextAlign.start,),
                                const SizedBox(height: 5),
                                Text('ប្រធានសម្ព័ន្ធ-ក្លិប..................',
                                  style: TextStylesHelper.khmerH1Black,
                                  textAlign: TextAlign.start,),
                              ],
                            ),
                          ),
                          const SizedBox(height: 15),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  Widget tableCell(String text, {bool isHeader = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 8),
      child: Text(
        text,
        style: TextStyle(
          color: isHeader ?Colors.white:Colors.black,
          fontWeight: isHeader ? FontWeight.bold : FontWeight.normal,
          fontSize: isHeader ? 14 : 14,
        ),
      ),
    );
  }
  TableRow tableRow(String name, String qty, String rate, String discount, String tax, String price) {
    return TableRow(
      children: [
        tableCell(name),
        tableCell(qty),
        tableCell(rate),
        tableCell(discount),
        tableCell(tax),
        tableCell(price),
      ],
    );
  }
  Column buildAddressColumn(String title) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.bold,
            color: Colors.black
          ),
        ),
        const SizedBox(height: 5),
        buildAddressLine('Card Number <${widget.itemRequestModel!.createdBy!.cardNumber ?? "N/A"}>'),
        buildAddressLine('Name <${widget.itemRequestModel!.createdBy!.name??"N/A"}>'),
        buildAddressLine('User Name <${widget.itemRequestModel!.createdBy!.username ?? "N/A"}>'),
        buildAddressLine('Phone <${widget.itemRequestModel!.createdBy!.phone ?? "N/A"}>'),
        buildAddressLine('Email <${widget.itemRequestModel!.createdBy!.email ?? "N/A"}>'),
      ],
    );
  }

  Column buildAddressColumnRight(String title) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.bold,
            color: Colors.black
          ),
        ),
        const SizedBox(height: 5),
        buildAddressLine('Team Name <${widget.itemRequestModel!.team!.teamName?? "N/A"}>'),
        buildAddressLine('Stadium <${widget.itemRequestModel!.team!.stadium?? "N/A"}>'),
        buildAddressLine('Founded Date <${widget.itemRequestModel!.team!.foundedDate?? "N/A"}>'),
        buildAddressLine('Province <${widget.itemRequestModel!.location!.name?? "N/A"}>,'),
        buildAddressLine('Province <${widget.itemRequestModel!.location!.description?? "N/A"}>'),
      ],
    );
  }

  Text buildAddressLine(String text) {
    return Text(
      text,
      style: const TextStyle(
        fontSize: 14,
        color: Colors.black
      ),
    );
  }
}


