import 'package:esport_system/data/controllers/request_item_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/dropdown_search_team.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/request_item_section/list_request_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import '../../data/controllers/home_controller.dart';
import '../../data/controllers/item_controller.dart';
import '../../data/model/item_request_model.dart';
import '../../helper/empty_data.dart';
import '../../helper/search_field.dart';
import '../../util/text_style.dart';


class CreateRequestItem extends StatefulWidget {
  final bool? updateItem;
  final ItemRequestModel? itemRequestModel;

  const CreateRequestItem(
      {super.key, required this.updateItem, this.itemRequestModel});

  @override
  State<CreateRequestItem> createState() => _CreateRequestItemState();
}

class _CreateRequestItemState extends State<CreateRequestItem> {
  var userController = Get.find<UserController>();
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();

  final _formKey = GlobalKey<FormBuilderState>();
  final requestItemController = Get.find<RequestItemController>();
  var itemController = Get.find<ItemController>();
  List<bool>_isPressedList = [];


  @override
  void initState() {
    teamController.fetchProvinces();
    _isPressedList = List<bool>.filled(itemController.listItem.length, false);
    if (!widget.updateItem!) {
      requestItemController.clearValue();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ItemController>(builder: (logic) {
        return GetBuilder<RequestItemController>(builder: (logic1) {
          return Container(
            margin: const EdgeInsets.all(10),
            width: double.infinity,
            height: double.infinity,
            child: Responsive(
                mobile: SingleChildScrollView(
                  child: Column(
                    children: [
                      _buildDropDown(),
                      logic.isLoading
                          ? const Center(child: CircularProgressIndicator())
                          : SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          dataRowMaxHeight: Get.height * 0.1,
                          headingRowColor: WidgetStateColor.resolveWith((
                              states) => secondaryColor),
                          columns: [
                            DataColumn(
                              label: Text(
                                "item_title".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "stock".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "status".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "add_item".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                          rows: logic.listItem.isEmpty
                              ? []
                              : List<DataRow>.generate(
                            logic.listItem.length, (index) {
                            var data = logic.listItem[index];
                            return DataRow(
                              cells: [
                                DataCell(
                                  Text(
                                    _truncateText('${data.itemTitle}', 20),
                                  ),
                                ),

                                DataCell(
                                  Text(
                                    _truncateText('${data.stockQty}', 20),
                                    style: TextStylesHelper.khmerText16
                                        .copyWith(
                                        fontSize: 14),
                                  ),
                                ),
                                DataCell(
                                  Text(
                                    '${data.isStatus}',
                                  ),
                                ),

                                DataCell(
                                  InkWell(
                                    onTap: () {
                                      requestItemController.addToCart(
                                          data, 1, "Not yet Sub note");
                                    },
                                    onHighlightChanged: (isPressed) {
                                      setState(() {
                                        _isPressedList[index] = isPressed;
                                      });
                                    },
                                    splashColor: Colors.white.withOpacity(
                                        0.3),
                                    highlightColor: Colors.transparent,
                                    // Optional: Keep it transparent if you want only the splash effect
                                    borderRadius: BorderRadius.circular(5),
                                    child: AnimatedContainer(
                                      duration: const Duration(
                                          milliseconds: 200),
                                      transform: Matrix4.identity()
                                        ..scale(
                                            _isPressedList[index]
                                                ? 0.98
                                                : 1.0),
                                      decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(
                                            5),
                                      ),
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        data.isInCart != null
                                            ? "update".tr
                                            : "+_add".tr,
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            );
                          },
                          ),
                        ),
                      ),
                      if (logic.listItem.isEmpty)...[
                        const SizedBox(height: 50,),
                        const Center(child: EmptyData()),
                      ],
                      const SizedBox(height: 20,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: FormBuilder(
                                  key: _formKey,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      Text('ref_number'.tr,
                                        style: const TextStyle(fontSize: 16),),
                                      const SizedBox(height: 5.0),
                                      CustomFormBuilderTextField(
                                        name: 'ref_number'.tr,
                                        controller: logic1.refNumberController,
                                        hintText: "ref_number".tr,
                                        icon: Icons.numbers,
                                        fillColor: secondaryColor,
                                        errorText: "",
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(width: 16),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment
                                      .start,
                                  children: [
                                    Text('province'.tr,
                                        style: const TextStyle(fontSize: 16)),
                                    const SizedBox(height: 5.0),
                                    _buildLocationSelector(
                                      icon: Icons.location_on,
                                      context,
                                      'province'.tr,
                                      teamController.provinces,
                                      teamController.selectedProvince?.name ??
                                          'select_province'.tr,
                                          (address) {
                                        teamController.selectProvince(
                                            address);
                                        setState(() {
                                          logic1.selectLocationCode =
                                              address.code;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16),
                          Text(
                            'select_team'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          TeamDropdownSearch(
                              selectedItem: logic1.previousSelectedTeam,
                              onChanged: (value) {
                                setState(() {
                                  logic1.selectTeamId = value!.id;
                                  logic1.previousSelectedTeam = value;
                                });
                              }),
                          const SizedBox(height: 16),
                          Text(
                            'note'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5),
                          CustomFormNote(
                            minLine: 2,
                            name: 'note'.tr,
                            controller: logic1.noteController,
                            hintText: "note".tr,
                            // icon: Icons.note,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      Container(
                        color: secondaryColor,
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("item_title".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "sub_title".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "quantity".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if(requestItemController.itemsCart.isNotEmpty)
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: requestItemController.itemsCart.length,
                          itemBuilder: (context, index) {
                            var data = requestItemController.itemsCart[index];
                            return Container(
                              margin: const EdgeInsets.symmetric(
                                  vertical: 4.0),
                              decoration: BoxDecoration(
                                border: Border.all(color: secondaryColor),
                                borderRadius: BorderRadius.circular(6),
                                color: bgColor.withOpacity(0.1),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          _truncateText(
                                              data.items?.itemTitle ?? "N/A",
                                              20),
                                          style: const TextStyle(
                                              fontSize: 14,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          _truncateText(
                                              '${data.items?.otherTitle}',
                                              20),
                                          style: TextStylesHelper.khmerText16
                                              .copyWith(fontSize: 14),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              print("Tap---------");
                                              logic1.decrementQuantity(data);
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(
                                                  6),
                                              decoration: BoxDecoration(
                                                color: bgColor.withOpacity(
                                                    0.1),
                                                border: Border.all(width: 1,
                                                    color: secondaryColor),
                                                borderRadius: BorderRadius
                                                    .circular(6),
                                              ),
                                              child: data.quantity! > 1
                                                  ? const Icon(
                                                CupertinoIcons.minus,
                                                size: 20,
                                                color: Colors.white,)
                                                  : const Icon(
                                                CupertinoIcons.delete,
                                                size: 20,
                                                color: Colors.red,),
                                            ),
                                          ),
                                          const SizedBox(width: 12),
                                          Padding(
                                            padding: const EdgeInsets.all(
                                                8.0),
                                            child: Text("${data.quantity}",
                                              style: const TextStyle(
                                                  fontSize: 16),
                                            ),
                                          ),
                                          const SizedBox(width: 12),
                                          GestureDetector(
                                            onTap: () {
                                              print("Tap++++++++++");
                                              logic1.incrementQuantity(data);
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(
                                                  6),
                                              decoration: BoxDecoration(
                                                color: bgColor.withOpacity(
                                                    0.1),
                                                border: Border.all(width: 1,
                                                    color: secondaryColor),
                                                borderRadius: BorderRadius
                                                    .circular(6),
                                              ),
                                              child: const Icon(
                                                CupertinoIcons.plus,
                                                size: 20,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      const SizedBox(height: 16),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                CancelButton(
                                    btnName: "cancel".tr,
                                    onPress: () {
                                      homeController.updateWidgetHome(
                                          const ListRequestItem());
                                    }),
                                const SizedBox(width: 16,),
                                OKButton(
                                  btnName: widget.updateItem!
                                      ? 'update'.tr
                                      : "create".tr,
                                  onPress: () async{
                                    if (_formKey.currentState != null &&
                                    _formKey.currentState!
                                        .saveAndValidate()) {
                                      if (widget.updateItem!) {
                                        logic1.updateRequest(
                                            rqId: widget.itemRequestModel!.id
                                                .toString());
                                      } else {
                                        if (logic1.refNumberController.text
                                            .isNotEmpty &&
                                            logic1.selectLocationCode!
                                                .isNotEmpty &&
                                            logic1.selectTeamId!.isNotEmpty &&
                                            logic1.noteController.text
                                                .isNotEmpty &&
                                            logic1.itemsCart.isNotEmpty) {
                                          // print("********* Test 2 ✅");
                                          // print("refNumberController ${logic1.refNumberController.text}");
                                          // print("selectLocationCode ${logic1.selectLocationCode}");
                                          // print("selectTeamId ${logic1.selectTeamId}");
                                          // print("noteController ${logic1.noteController.text}");
                                          // if(logic1.itemsCart.isNotEmpty){
                                          //   for(int i=0 ; i<logic1.itemsCart.length ; i++){
                                          //     var a = logic1.itemsCart[i];
                                          //     print("Item in cart ${a.items?.itemTitle} --- id: ${a.items?.id} ---${a.quantity}");
                                          //   }
                                          // }
                                          logic1.createRequest();
                                        }
                                      }
                                    }
                                  },
                                ),
                              ],
                            )
                        ),
                      ),
                    ],
                  ),
                ),
                tablet: SingleChildScrollView(
                  child: Column(
                    children: [
                      _buildDropDown(),
                      logic.isLoading
                          ? const Center(child: CircularProgressIndicator())
                          : SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          dataRowMaxHeight: Get.height * 0.1,
                          headingRowColor: WidgetStateColor.resolveWith((
                              states) => secondaryColor),
                          columns: [
                            DataColumn(
                              label: Text(
                                "item_title".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "stock".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "status".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "add_item".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                          rows: logic.listItem.isEmpty
                              ? []
                              : List<DataRow>.generate(
                            logic.listItem.length, (index) {
                            var data = logic.listItem[index];
                            return DataRow(
                              cells: [
                                DataCell(
                                  Text(
                                    _truncateText('${data.itemTitle}', 20),
                                  ),
                                ),

                                DataCell(
                                  Text(
                                    _truncateText('${data.stockQty}', 20),
                                    style: TextStylesHelper.khmerText16
                                        .copyWith(
                                        fontSize: 14),
                                  ),
                                ),
                                DataCell(
                                  Text(
                                    '${data.isStatus}',
                                  ),
                                ),

                                DataCell(
                                  InkWell(
                                    onTap: () {
                                      requestItemController.addToCart(
                                          data, 1, "Not yet Sub note");
                                    },
                                    onHighlightChanged: (isPressed) {
                                      setState(() {
                                        _isPressedList[index] = isPressed;
                                      });
                                    },
                                    splashColor: Colors.white.withOpacity(
                                        0.3),
                                    highlightColor: Colors.transparent,
                                    // Optional: Keep it transparent if you want only the splash effect
                                    borderRadius: BorderRadius.circular(5),
                                    child: AnimatedContainer(
                                      duration: const Duration(
                                          milliseconds: 200),
                                      transform: Matrix4.identity()
                                        ..scale(
                                            _isPressedList[index]
                                                ? 0.98
                                                : 1.0),
                                      decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(
                                            5),
                                      ),
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        data.isInCart != null
                                            ? "update".tr
                                            : "+_add".tr,
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            );
                          },
                          ),
                        ),
                      ),
                      if (logic.listItem.isEmpty)...[
                        const SizedBox(height: 50,),
                        const Center(child: EmptyData()),
                      ],
                      const SizedBox(height: 20,),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: FormBuilder(
                                  key: _formKey,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: [
                                      Text('ref_number'.tr,
                                        style: const TextStyle(fontSize: 16),),
                                      const SizedBox(height: 5.0),
                                      CustomFormBuilderTextField(
                                        name: 'ref_number'.tr,
                                        controller: logic1.refNumberController,
                                        hintText: "ref_number".tr,
                                        icon: Icons.numbers,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              const SizedBox(width: 16),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment
                                      .start,
                                  children: [
                                    Text('province'.tr,
                                        style: const TextStyle(fontSize: 16)),
                                    const SizedBox(height: 5.0),
                                    _buildLocationSelector(
                                      icon: Icons.location_on,
                                      context,
                                      'province'.tr,
                                      teamController.provinces,
                                      teamController.selectedProvince?.name ??
                                          'select_province'.tr,
                                          (address) {
                                        teamController.selectProvince(
                                            address);
                                        setState(() {
                                          logic1.selectLocationCode =
                                              address.code;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 16),
                          Text(
                            'select_team'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          TeamDropdownSearch(
                              selectedItem: logic1.previousSelectedTeam,
                              onChanged: (value) {
                                setState(() {
                                  logic1.selectTeamId = value!.id;
                                  logic1.previousSelectedTeam = value;
                                });
                              }),
                          const SizedBox(height: 16),
                          Text(
                            'note'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5),
                          CustomFormNote(
                            minLine: 2,
                            name: 'note'.tr,
                            controller: logic1.noteController,
                            hintText: "note".tr,
                            // icon: Icons.note,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      Container(
                        color: secondaryColor,
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("item_title".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "sub_title".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "quantity".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if(requestItemController.itemsCart.isNotEmpty)
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: requestItemController.itemsCart.length,
                          itemBuilder: (context, index) {
                            var data = requestItemController.itemsCart[index];
                            return Container(
                              margin: const EdgeInsets.symmetric(
                                  vertical: 4.0),
                              decoration: BoxDecoration(
                                border: Border.all(color: secondaryColor),
                                borderRadius: BorderRadius.circular(6),
                                color: bgColor.withOpacity(0.1),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          _truncateText(
                                              data.items?.itemTitle ?? "N/A",
                                              20),
                                          style: const TextStyle(
                                              fontSize: 14,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          _truncateText(
                                              '${data.items?.otherTitle}',
                                              20),
                                          style: TextStylesHelper.khmerText16
                                              .copyWith(fontSize: 14),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              print("Tap---------");
                                              logic1.decrementQuantity(data);
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(
                                                  6),
                                              decoration: BoxDecoration(
                                                color: bgColor.withOpacity(
                                                    0.1),
                                                border: Border.all(width: 1,
                                                    color: secondaryColor),
                                                borderRadius: BorderRadius
                                                    .circular(6),
                                              ),
                                              child: data.quantity! > 1
                                                  ? const Icon(
                                                CupertinoIcons.minus,
                                                size: 20,
                                                color: Colors.white,)
                                                  : const Icon(
                                                CupertinoIcons.delete,
                                                size: 20,
                                                color: Colors.red,),
                                            ),
                                          ),
                                          const SizedBox(width: 12),
                                          Padding(
                                            padding: const EdgeInsets.all(
                                                8.0),
                                            child: Text("${data.quantity}",
                                              style: const TextStyle(
                                                  fontSize: 16),
                                            ),
                                          ),
                                          const SizedBox(width: 12),
                                          GestureDetector(
                                            onTap: () {
                                              print("Tap++++++++++");
                                              logic1.incrementQuantity(data);
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(6),
                                              decoration: BoxDecoration(
                                                color: bgColor.withOpacity(0.1),
                                                border: Border.all(width: 1, color: secondaryColor),
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              child: const Icon(
                                                CupertinoIcons.plus,
                                                size: 20,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      const SizedBox(height: 16),
                      Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                CancelButton(
                                    btnName: "cancel".tr,
                                    onPress: () {homeController.updateWidgetHome(const ListRequestItem());}),
                                const SizedBox(width: 16,),
                                OKButton(
                                  btnName: widget.updateItem!
                                      ? 'update'.tr
                                      : "create".tr,
                                  onPress: () async{
                                    if (_formKey.currentState != null &&
                                    _formKey.currentState!
                                        .saveAndValidate()) {
                                      if (widget.updateItem!) {
                                        logic1.updateRequest(rqId: widget.itemRequestModel!.id.toString());
                                      } else {
                                        if (logic1.refNumberController.text.isNotEmpty &&
                                            logic1.selectLocationCode!.isNotEmpty &&
                                            logic1.selectTeamId!.isNotEmpty &&
                                            logic1.noteController.text.isNotEmpty &&
                                            logic1.itemsCart.isNotEmpty) {
                                          // print("********* Test 2 ✅");
                                          // print("refNumberController ${logic1.refNumberController.text}");
                                          // print("selectLocationCode ${logic1.selectLocationCode}");
                                          // print("selectTeamId ${logic1.selectTeamId}");
                                          // print("noteController ${logic1.noteController.text}");
                                          // if(logic1.itemsCart.isNotEmpty){
                                          //   for(int i=0 ; i<logic1.itemsCart.length ; i++){
                                          //     var a = logic1.itemsCart[i];
                                          //     print("Item in cart ${a.items?.itemTitle} --- id: ${a.items?.id} ---${a.quantity}");
                                          //   }
                                          // }
                                          logic1.createRequest();
                                        }
                                      }
                                    }
                                  },
                                ),
                              ],
                            )
                        ),
                      ),
                    ],
                  ),
                ),
                desktop: SingleChildScrollView(
                  child: SizedBox(
                    width: Get.width*7,
                    child: Column(
                      children: [
                        const SizedBox(height: 20),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("create_request_items".tr,style: TextStyle(fontSize: 20),),
                          ],
                        ),
                        const SizedBox(height: 20),
                        FormBuilder(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('ref_number'.tr,
                                          style: const TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          name: 'ref_number'.tr,
                                          controller: logic1.refNumberController,
                                          hintText: "ref_number".tr,
                                          icon: Icons.numbers,
                                          fillColor: secondaryColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        Text('province'.tr,
                                            style: const TextStyle(fontSize: 16)),
                                        const SizedBox(height: 5.0),
                                        _buildLocationSelector(
                                          icon: Icons.location_on,
                                          context,
                                          'province'.tr,
                                          teamController.provinces,
                                          teamController.selectedProvince?.name ??
                                              'select_province'.tr,
                                              (address) {
                                            teamController.selectProvince(address);
                                            setState(() {
                                              logic1.selectLocationCode = address.code;
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 16),
                              Text(
                                'select_team'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              const SizedBox(height: 5.0),
                              TeamDropdownSearch(
                                  selectedItem: logic1.previousSelectedTeam,
                                  onChanged: (value) {
                                    setState(() {
                                      logic1.selectTeamId = value!.id;
                                      logic1.previousSelectedTeam = value;
                                    });
                                  }),
                              const SizedBox(height: 16),
                              Text(
                                'note'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              const SizedBox(height: 5),
                              CustomFormNote(
                                minLine: 2,
                                name: 'note'.tr,
                                controller: logic1.noteController,
                                hintText: "note".tr,
                                // icon: Icons.note,
                                fillColor: secondaryColor,
                              ),
                            ],
                          ),
                        ),
                        _buildDropDown(),
                        const SizedBox(height: 20),
                        logic.isLoading
                            ? const Center(child: CircularProgressIndicator())
                            : SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SizedBox(
                            width: Get.width*0.9, // Adjust the width as needed, or use Get.width for full screen width.
                            child: DataTable(
                              dataRowMaxHeight: Get.height * 0.07,
                              headingRowColor: MaterialStateColor.resolveWith((states) => secondaryColor),
                              columns: [
                                DataColumn(
                                  label: Text(
                                    "item_title".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    "stock".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    "status".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                DataColumn(
                                  label: Text(
                                    "add_item".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                              rows: logic.listItem.isEmpty
                                  ? []
                                  : List<DataRow>.generate(
                                logic.listItem.length, (index) {
                                var data = logic.listItem[index];
                                return DataRow(
                                  cells: [
                                    DataCell(
                                      Text(
                                        _truncateText('${data.itemTitle}', 20),
                                      ),
                                    ),
                                    DataCell(
                                      Text(
                                        _truncateText('${data.stockQty}', 20),
                                        style: TextStylesHelper.khmerText16.copyWith(
                                          fontSize: 14,
                                        ),
                                      ),
                                    ),
                                    DataCell(
                                      Text('${data.isStatus}'),
                                    ),
                                    DataCell(
                                      InkWell(
                                        onTap: () {
                                          requestItemController.addToCart(data, 1, "Not yet Sub note");
                                        },
                                        onHighlightChanged: (isPressed) {
                                          setState(() {
                                            _isPressedList[index] = isPressed;
                                          });
                                        },
                                        splashColor: Colors.white.withOpacity(0.3),
                                        highlightColor: Colors.transparent,
                                        borderRadius: BorderRadius.circular(5),
                                        child: AnimatedContainer(
                                          duration: const Duration(milliseconds: 200),
                                          transform: Matrix4.identity()..scale(_isPressedList[index] ? 0.98 : 1.0),
                                          decoration: BoxDecoration(
                                            color: Colors.blue,
                                            borderRadius: BorderRadius.circular(5),
                                          ),
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            data.isInCart != null ? "update".tr : "+_add".tr,
                                            style: const TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              },
                              ),
                            ),
                          ),
                        ),
                        if (logic.listItem.isEmpty)...[
                          const SizedBox(height: 50,),
                          const Center(child: EmptyData()),
                        ],
                        const SizedBox(height: 20),
                        Container(
                          color: secondaryColor,
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Row(
                            children: [
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("item_title".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "sub_title".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "quantity".tr,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        if(requestItemController.itemsCart.isNotEmpty)
                          ListView.builder(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: requestItemController.itemsCart.length,
                            itemBuilder: (context, index) {
                              var data = requestItemController.itemsCart[index];
                              return Container(
                                margin: const EdgeInsets.symmetric(vertical: 4.0),
                                decoration: BoxDecoration(
                                  border: Border.all(color: secondaryColor),
                                  borderRadius: BorderRadius.circular(6),
                                  color: bgColor.withOpacity(0.1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            _truncateText(
                                                data.items?.itemTitle ?? "N/A",
                                                20),
                                            style: const TextStyle(
                                                fontSize: 14,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            _truncateText(
                                                '${data.items?.otherTitle}',
                                                20),
                                            style: TextStylesHelper.khmerText16
                                                .copyWith(fontSize: 14),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                print("Tap---------");
                                                logic1.decrementQuantity(data);
                                              },
                                              child: Container(
                                                padding: const EdgeInsets.all(
                                                    6),
                                                decoration: BoxDecoration(
                                                  color: bgColor.withOpacity(
                                                      0.1),
                                                  border: Border.all(width: 1,
                                                      color: secondaryColor),
                                                  borderRadius: BorderRadius
                                                      .circular(6),
                                                ),
                                                child: data.quantity! > 1
                                                    ? const Icon(
                                                  CupertinoIcons.minus,
                                                  size: 20,
                                                  color: Colors.white,)
                                                    : const Icon(
                                                  CupertinoIcons.delete,
                                                  size: 20,
                                                  color: Colors.red,),
                                              ),
                                            ),
                                            const SizedBox(width: 12),
                                            Padding(
                                              padding: const EdgeInsets.all(
                                                  8.0),
                                              child: Text("${data.quantity}",
                                                style: const TextStyle(
                                                    fontSize: 16),
                                              ),
                                            ),
                                            const SizedBox(width: 12),
                                            GestureDetector(
                                              onTap: () {
                                                print("Tap++++++++++");
                                                logic1.incrementQuantity(data);
                                              },
                                              child: Container(
                                                padding: const EdgeInsets.all(
                                                    6),
                                                decoration: BoxDecoration(
                                                  color: bgColor.withOpacity(
                                                      0.1),
                                                  border: Border.all(width: 1,
                                                      color: secondaryColor),
                                                  borderRadius: BorderRadius
                                                      .circular(6),
                                                ),
                                                child: const Icon(
                                                  CupertinoIcons.plus,
                                                  size: 20,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        const SizedBox(height: 16),
                        Align(
                          alignment: Alignment.topRight,
                          child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  CancelButton(
                                      btnName: "cancel".tr,
                                      onPress: () {
                                        homeController.updateWidgetHome(
                                            const ListRequestItem());
                                      }),
                                  const SizedBox(width: 16,),
                                  OKButton(
                                    btnName: widget.updateItem!
                                        ? 'update'.tr
                                        : "create".tr,
                                    onPress: () async{
                                      if (_formKey.currentState != null &&
                                      _formKey.currentState!
                                          .saveAndValidate()) {
                                        if (widget.updateItem!) {
                                          logic1.updateRequest(
                                              rqId: widget.itemRequestModel!.id
                                                  .toString());
                                        } else {
                                          if (logic1.refNumberController.text
                                              .isNotEmpty &&
                                              logic1.selectLocationCode!
                                                  .isNotEmpty &&
                                              logic1.selectTeamId!.isNotEmpty &&
                                              logic1.noteController.text
                                                  .isNotEmpty &&
                                              logic1.itemsCart.isNotEmpty) {
                                            // print("********* Test 2 ✅");
                                            // print("refNumberController ${logic1.refNumberController.text}");
                                            // print("selectLocationCode ${logic1.selectLocationCode}");
                                            // print("selectTeamId ${logic1.selectTeamId}");
                                            // print("noteController ${logic1.noteController.text}");
                                            // if(logic1.itemsCart.isNotEmpty){
                                            //   for(int i=0 ; i<logic1.itemsCart.length ; i++){
                                            //     var a = logic1.itemsCart[i];
                                            //     print("Item in cart ${a.items?.itemTitle} --- id: ${a.items?.id} ---${a.quantity}");
                                            //   }
                                            // }
                                            logic1.createRequest();
                                          }
                                        }
                                      }
                                    },
                                  ),
                                ],
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
            ),
          );
        });
      }),
    );
  }

  Widget _buildDropDown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: itemController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  itemController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              itemController.search(value);
            }, hintText: 'search_item_title'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }

  Widget _buildLocationSelector(BuildContext context,
      String label,
      List<Address> data,
      String currentValue,
      Function(Address) onSelect, {
        bool enabled = true,
        IconData? icon,
      }) {
    return SizedBox(
      width: double.infinity,
      height: 50,
      child: FormBuilderTextField(
        name: label,
        readOnly: true,
        onTap: enabled
            ? () =>
            _showSelectionDialog(
                context: context, title: label, data: data, onSelect: onSelect)
            : null,
        controller: TextEditingController(text: currentValue),
        decoration: InputDecoration(
          prefixIcon: Icon(icon),
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          filled: true,
          fillColor: secondaryColor,
          hintText: label,
          suffixIcon: const Icon(Icons.arrow_drop_down),
        ),
      ),
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
    kIsWeb ? MediaQuery
        .of(context)
        .size
        .width * 0.5 : double.maxFinite;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding:
          const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name ?? ''),
                  subtitle: Text(item.description ?? ''),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
