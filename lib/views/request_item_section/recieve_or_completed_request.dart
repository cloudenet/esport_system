

import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/request_item_controller.dart';
import '../../data/model/item_request_model.dart';
import '../../helper/add_new_button.dart';
import '../../helper/custom_textformfield.dart';
import '../../util/text_style.dart';
class RecieveOrCompletedRequest extends StatefulWidget {
  final bool? approved;
  final ItemRequestModel? itemRequestModel;
  const RecieveOrCompletedRequest({super.key,required this.approved,required this.itemRequestModel});

  @override
  State<RecieveOrCompletedRequest> createState() => _RecieveOrCompletedRequestState();
}

class _RecieveOrCompletedRequestState extends State<RecieveOrCompletedRequest> {

  static const double a4Width = 794.0;
  var teamController = Get.find<TeamController>();
  var requestItemController = Get.find<RequestItemController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<RequestItemController>(
          builder: (logic) {
            return Row(
              children: [
                Expanded(
                    flex: 4,
                    child: LayoutBuilder(
                      builder: (context, constraints) {
                        return Column(
                          children: [
                            Expanded(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: ConstrainedBox(
                                  constraints: BoxConstraints(
                                    minWidth: constraints.maxWidth,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 180,
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          'ref_number'.tr,
                                                          style: const TextStyle(fontSize: 16),
                                                        ),
                                                        const SizedBox(height: 5.0),
                                                        CustomFormBuilderTextField(
                                                          name: 'ref_number'.tr,
                                                          controller: TextEditingController(text: widget.itemRequestModel!.refNumber??"N/A"),
                                                          hintText: "ref_number".tr,
                                                          icon: Icons.numbers,
                                                          fillColor: secondaryColor,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(width: 16),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text('province'.tr,
                                                            style: const TextStyle(fontSize: 16)),
                                                        const SizedBox(height: 5.0),
                                                        CustomFormBuilderTextField(
                                                          name: 'province'.tr,
                                                          controller: TextEditingController(text: widget.itemRequestModel!.location!.name??"N/A"),
                                                          hintText: "province".tr,
                                                          icon: Icons.numbers,
                                                          fillColor: secondaryColor,
                                                        ),
                                                        // _buildLocationSelector(
                                                        //   icon: Icons.location_on,
                                                        //   context,
                                                        //   'Province'.tr,
                                                        //   teamController.provinces,
                                                        //   teamController.selectedProvince?.name ??
                                                        //       'Select Province'.tr,
                                                        //       (address) {
                                                        //     teamController.selectProvince(address);
                                                        //     setState(() {
                                                        //       logic1.selectLocationCode = address.code;
                                                        //     });
                                                        //   },
                                                        // ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(height: 16),
                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          'select_team'.tr,
                                                          style: const TextStyle(fontSize: 16),
                                                        ),
                                                        const SizedBox(height: 5.0),
                                                        CustomFormBuilderTextField(
                                                          name: 'team_name'.tr,
                                                          controller: TextEditingController(text: widget.itemRequestModel!.team!.teamName ?? "N/A"),
                                                          hintText: "team_name".tr,
                                                          icon: Icons.numbers,
                                                          fillColor: secondaryColor,
                                                        ),
                                                        // DropdownButtonFormField<String>(
                                                        //   borderRadius: BorderRadius.circular(10),
                                                        //   value: logic1.selectTeamId,
                                                        //   items: teamController.listTeam.map((team) {
                                                        //     return DropdownMenuItem<String>(
                                                        //       value: team.id,
                                                        //       child: Text(team.teamName!),
                                                        //     );
                                                        //   }).toList(),
                                                        //   onChanged: (value) {
                                                        //     setState(() {
                                                        //       logic1.selectTeamId = value;
                                                        //     });
                                                        //   },
                                                        //   decoration: const InputDecoration(
                                                        //     labelText: "Please select team",
                                                        //     floatingLabelAlignment:
                                                        //     FloatingLabelAlignment.center,
                                                        //     floatingLabelBehavior:
                                                        //     FloatingLabelBehavior.never,
                                                        //     fillColor: secondaryColor,
                                                        //     filled: true,
                                                        //     border: OutlineInputBorder(
                                                        //       borderSide: BorderSide.none,
                                                        //       borderRadius: BorderRadius.all(
                                                        //         Radius.circular(10),
                                                        //       ),
                                                        //     ),
                                                        //   ),
                                                        // ),
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(width: 16),
                                                  Expanded(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          'note'.tr,
                                                          style: const TextStyle(fontSize: 16),
                                                        ),
                                                        const SizedBox(height: 5),
                                                        CustomFormBuilderTextField(
                                                          name: 'note'.tr,
                                                          controller: TextEditingController(text: widget.itemRequestModel!.note??"N/A"),
                                                          hintText: "note".tr,
                                                          icon: Icons.note,
                                                          fillColor: secondaryColor,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 16),
                                        // Header Row
                                        Container(
                                          color: secondaryColor,
                                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    "item_title".tr,
                                                    style: const TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 16,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    "sub_title".tr,
                                                    style: const TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 16,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Padding(
                                                  padding: const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    "quantity".tr,
                                                    style: const TextStyle(
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 16,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        // ListView.builder
                                        if(logic.itemsCart.isNotEmpty)
                                          ListView.builder(
                                            shrinkWrap: true,
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemCount: logic.itemsCart.length,
                                            itemBuilder: (context, index) {
                                              var data = logic.itemsCart[index];
                                              return Container(
                                                margin: const EdgeInsets.symmetric(vertical: 4.0),
                                                decoration: BoxDecoration(
                                                  border: Border.all(color: secondaryColor),
                                                  borderRadius: BorderRadius.circular(6),
                                                  color: bgColor.withOpacity(0.1),
                                                ),
                                                child: Padding(
                                                  padding: const EdgeInsets.all(4.0),
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(8.0),
                                                          child: Text(
                                                            _truncateText(data.items?.itemTitle ?? "N/A", 20),
                                                            style: const TextStyle(fontSize: 14, color: Colors.white),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(8.0),
                                                          child: Text(
                                                            _truncateText('${data.items?.otherTitle}', 20),
                                                            style: TextStylesHelper.khmerText16.copyWith(fontSize: 14),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: Row(
                                                          children: [
                                                            GestureDetector(
                                                              onTap: () {
                                                                print("Tap---------");
                                                                logic.decrementQuantity(data);
                                                              },
                                                              child: Container(
                                                                padding: const EdgeInsets.all(6),
                                                                decoration: BoxDecoration(
                                                                  color: bgColor.withOpacity(0.1),
                                                                  border: Border.all(width: 1, color: secondaryColor),
                                                                  borderRadius: BorderRadius.circular(6),
                                                                ),
                                                                child: data.quantity! > 1
                                                                    ? const Icon( CupertinoIcons.minus, size: 20, color: Colors.white,)
                                                                    : const Icon( CupertinoIcons.delete, size: 20, color: Colors.red,),
                                                              ),
                                                            ),
                                                            const SizedBox(width: 12),
                                                            Padding(
                                                              padding: const EdgeInsets.all(8.0),
                                                              child: Text(
                                                                "${data.quantity}",
                                                                style: const TextStyle(fontSize: 16),
                                                              ),
                                                            ),
                                                            const SizedBox(width: 12),
                                                            GestureDetector(
                                                              onTap: () {
                                                                print("Tap++++++++++");
                                                                logic.incrementQuantity(data);
                                                              },
                                                              child: Container(
                                                                padding: const EdgeInsets.all(6),
                                                                decoration: BoxDecoration(
                                                                  color: bgColor.withOpacity(0.1),
                                                                  border: Border.all(width: 1, color: secondaryColor),
                                                                  borderRadius: BorderRadius.circular(6),
                                                                ),
                                                                child: const Icon(
                                                                  CupertinoIcons.plus,
                                                                  size: 20,
                                                                  color: Colors.white,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              );
                                            },
                                          ),
                                        const SizedBox(height: 16),
                                        SizedBox(
                                          height: 50,
                                          child: Align(
                                            alignment: Alignment.topRight,
                                            child: AddNewButton(
                                              btnName: "approved".tr,
                                              onPress: () {
                                                logic.receivedItem(rqId: widget.itemRequestModel!.id);
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        );
                      },
                    )
                ),
                Expanded(
                  flex: 5,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Center(
                        child:  Container(
                          color: Colors.white,
                          //color: secondaryColor,
                          width: a4Width,
                          // height: a4Height,
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      Image.asset('assets/images/Boules.png', height: 80,),
                                      const SizedBox(height: 10.0),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                                        child: Column(
                                          children: [
                                            Text(
                                              'សហព័ន្ធកីឡាប៊ុល និងប៉េតង់កម្ពុជា',
                                              style: TextStylesHelper.khmerH1Black,
                                              textAlign: TextAlign.center,
                                            ),
                                            const Text(
                                              'Federation of Boules and Pétanque of Cambodia',
                                              style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 24),
                                      Text('សូមគោរពជូន',
                                        style: TextStylesHelper.khmerH1Black,
                                        textAlign: TextAlign.center,),
                                      Text('ឯកឧត្តមប្រធានសហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា',
                                        style: TextStylesHelper.khmerH1Black,
                                        textAlign: TextAlign.center,),
                                      const SizedBox(height: 15),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 24),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('កម្មវត្ថុ៖      ............................................................................................................................................................',
                                      style: TextStylesHelper.khmerTextBlack,
                                      textAlign: TextAlign.start,),
                                    const SizedBox(height: 5),
                                    Text('យោង៖      ............................................................................................................................................................',
                                      style: TextStylesHelper.khmerTextBlack,
                                      textAlign: TextAlign.start,),
                                    const SizedBox(height: 5),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 77),
                                      child: Text('សេចក្ដីដូចមានចែងក្នុងកម្មវត្ថុ និងយោងខាងលើ ខ្ញុំមានកិត្តិយសសូមគោរពជម្រាបជូន ឯកឧត្តមប្រធាន មេត្តា',
                                        style: TextStylesHelper.khmerTextBlack,
                                        textAlign: TextAlign.start,),
                                    ),
                                    const SizedBox(height: 5),
                                    Text('ជ្រាបថា (ខ្លឹមសារលិខិត)................................................................................................................................................',
                                      style: TextStylesHelper.khmerTextBlack,
                                      textAlign: TextAlign.start,),
                                    const SizedBox(height: 5),
                                  ],
                                ),
                                const SizedBox(height: 24),
                                const SizedBox(height: 16),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: buildAddressColumn('request:'.tr),
                                      ),
                                    ),
                                    Expanded(
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: buildAddressColumnRight('request:'.tr),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 24),
                                Table(
                                  columnWidths: const {
                                    0: FlexColumnWidth(3),
                                    1: FlexColumnWidth(3),
                                    2: FlexColumnWidth(1),
                                    3: FlexColumnWidth(1),
                                  },
                                  children: [
                                    TableRow(
                                      decoration: const BoxDecoration(color: Colors.indigo ,),
                                      children: [
                                        tableCell('item_title'.tr, isHeader: true),
                                        tableCell('sub_title'.tr, isHeader: true),
                                        tableCell('note'.tr, isHeader: true),
                                        tableCell('qty'.tr, isHeader: true),
                                      ],
                                    ),
                                    // Data rows

                                    for (var item in logic.itemsCart) // Iterate over itemsCart list directly
                                      TableRow(
                                        decoration: BoxDecoration(color: Colors.grey.shade100),
                                        children: [
                                          tableCell(item.items?.itemTitle.toString() ?? "N/A"),  // 'Item Title' column
                                          tableCell(item.items?.otherTitle.toString() ?? "N/A"), // 'Sub title' column
                                          tableCell(item.subNote?.isNotEmpty == true ? item.subNote! : "-"), // 'Status' column
                                          tableCell("x ${item.quantity.toString()}"), // 'Qty' column
                                        ],
                                      ),
                                    // for (var item in widget.itemRequestModel!.itemDetails!)
                                    //   TableRow(
                                    //     decoration: BoxDecoration(color: Colors.grey.shade100),
                                    //     children: [
                                    //       tableCell(item.item!.itemTitle.toString()),  // 'Item Title' column
                                    //       tableCell(item.item!.otherTitle.toString()), // 'Sub title' column
                                    //       tableCell(item.note?.isNotEmpty == true ? item.note! : "N/A"),   // 'Status' column
                                    //       tableCell("x ${item.qty.toString()}"),              // 'Qty' column
                                    //     ],
                                    //   )
                                  ],
                                ),
                                const SizedBox(height: 24),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('សេចក្ដីដូចបានជម្រាបជូនខាងលើ សូម ឯកឧត្តមប្រធាន មេត្តាពិនិត្យ  និងអញ្ជើញចូលរួមដោយក្ដីអនុគ្រោះ។',
                                        style: TextStylesHelper.khmerTextBlack,
                                        textAlign: TextAlign.start,),
                                      Text('សូម ឯកឧត្តមប្រធានសហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា ទទួលនូវការគោរពពីខ្ញុំ៕',
                                        style: TextStylesHelper.khmerTextBlack,
                                        textAlign: TextAlign.start,),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 24),
                                Align(
                                  alignment: Alignment.centerRight,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('ថ្ងៃ.........ខែ.........ឆ្នាំ.........ព.ស.២៥៦.....',
                                        style: TextStylesHelper.khmerTextBlack,
                                        textAlign: TextAlign.start,),
                                      const SizedBox(height: 5),
                                      Text('រាជធានី-ខេត្ត.........ថ្ងៃទី.........ខែ.........ឆ្នាំ២០២...',
                                        style: TextStylesHelper.khmerTextBlack,
                                        textAlign: TextAlign.start,),
                                      const SizedBox(height: 5),
                                      Text('ប្រធានសម្ព័ន្ធ-ក្លិប..................',
                                        style: TextStylesHelper.khmerH1Black,
                                        textAlign: TextAlign.start,),
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 50),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            );
          },
      )
    );
  }
  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }

  Widget tableCell(String text, {bool isHeader = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 8),
      child: Text(
        text,
        style: TextStyle(
          color: isHeader ?Colors.white:Colors.black,
          fontWeight: isHeader ? FontWeight.bold : FontWeight.normal,
          fontSize: isHeader ? 14 : 14,
        ),
      ),
    );
  }
  TableRow tableRow(String name, String qty, String rate, String discount, String tax, String price) {
    return TableRow(
      children: [
        tableCell(name),
        tableCell(qty),
        tableCell(rate),
        tableCell(discount),
        tableCell(tax),
        tableCell(price),
      ],
    );
  }
  Column buildAddressColumn(String title) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 5),
        buildAddressLine('Card Number <${widget.itemRequestModel!.createdBy!.cardNumber ?? "N/A"}>'),
        buildAddressLine('Name <${widget.itemRequestModel!.createdBy!.name??"N/A"}>'),
        buildAddressLine('User Name <${widget.itemRequestModel!.createdBy!.username ?? "N/A"}>'),
        buildAddressLine('Phone <${widget.itemRequestModel!.createdBy!.phone ?? "N/A"}>'),
        buildAddressLine('Email <${widget.itemRequestModel!.createdBy!.email ?? "N/A"}>'),
      ],
    );
  }

  Column buildAddressColumnRight(String title) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          title,
          style: const TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 5),
        buildAddressLine('Team Name <${widget.itemRequestModel!.team!.teamName?? "N/A"}>'),
        buildAddressLine('Stadium <${widget.itemRequestModel!.team!.stadium?? "N/A"}>'),
        buildAddressLine('Founded Date <${widget.itemRequestModel!.team!.foundedDate?? "N/A"}>'),
        buildAddressLine('Province <${widget.itemRequestModel!.location!.name?? "N/A"}>,'),
        buildAddressLine('Province <${widget.itemRequestModel!.location!.description?? "N/A"}>'),
      ],
    );
  }

  Text buildAddressLine(String text) {
    return Text(
      text,
      style: const TextStyle(
          fontSize: 14,
          color: Colors.black
      ),
    );
  }
}
