import 'package:esport_system/data/controllers/pitch_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/pitch_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/pitch_section/pitch_list.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import '../../data/controllers/home_controller.dart';
import '../../data/model/address_model.dart';
import '../../helper/add_new_button.dart';
import '../../helper/constants.dart';
import '../../helper/custom_textformfield.dart';

class CreatePitch extends StatefulWidget {
  final PitchModel? pitchData;
  final bool update;
  const CreatePitch({Key? key, this.pitchData, required this.update})
      : super(key: key);

  @override
  State<CreatePitch> createState() => _CreatePitchState();
}

class _CreatePitchState extends State<CreatePitch> {
  final _formKey = GlobalKey<FormBuilderState>();
  var pitchController = Get.find<PitchController>();
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();
  @override
  void initState() {
    pitchController.clearAllTextController();
    if (widget.pitchData != null && widget.update) {
      pitchController.getOldValue(widget.pitchData!);
    }
    pitchController.fetchProvinces();
    get();
    super.initState();
  }

  void get() async {
    await pitchController.getTeam();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<PitchController>(
        builder: (logic) {
          return SingleChildScrollView(
            child: Container(
              width: Get.width,
              margin: const EdgeInsets.all(24.0),
              child: Column(
                children: [
                  FormBuilder(
                    key: _formKey,
                    child: Responsive(
                        mobile: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              widget.update
                                  ? 'update_request_pitch'.tr
                                  : 'create_request_pitch'.tr,
                              style: const TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('province'.tr,
                                    style: const TextStyle(fontSize: 16)),
                                _buildLocationSelector(
                                  icon: Icons.location_on,
                                  context, 'province'.tr,
                                  logic.provinces,
                                  logic.selectedProvince?.name ?? 'select_province'.tr,
                                  logic.selectProvince,
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Column(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: [
                                Text('team_name'.tr, style: const TextStyle(fontSize: 16)),
                                DropdownButtonFormField<String>(
                                  borderRadius: BorderRadius.circular(10),
                                  value: logic.selectedTeamId,
                                  items: logic.listTeam.map((team) {
                                    return DropdownMenuItem<String>(
                                      value: team.id,
                                      child: Text(
                                        (team.teamName?.length ?? 0) > 30
                                            ? '${team.teamName?.substring(0, 30)}...'
                                            : team.teamName ?? 'unknown'.tr,
                                        overflow: TextOverflow.ellipsis, // Truncate text
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      logic.selectedTeamId = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: "please_select_team".tr,
                                    floatingLabelAlignment: FloatingLabelAlignment.center,
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    fillColor: secondaryColor,
                                    filled: true,
                                    border: const OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(10),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('name'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_requester_name".tr,
                                        name: 'name'.tr,
                                        controller:
                                        logic.requesterNameController,
                                        hintText: 'enter_requester_name'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('phone'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_requester_phone".tr,
                                        name: 'Phone'.tr,
                                        controller:
                                        logic.requesterPhoneController,
                                        hintText: 'enter_requester_phone'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('amount_of_pitch'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_amount_of_pitch".tr,
                                        name: 'amount_of_pitch'.tr,
                                        controller: logic.amountPitchController,
                                        hintText: 'enter_amount_of_pitch'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('table_of_pitch'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_table_of_pitch".tr,
                                        name: 'table_of_pitch'.tr,
                                        controller: logic.amountTableController,
                                        hintText: 'enter_table_of_pitch'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('location'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_location".tr,
                                        name: 'location'.tr,
                                        controller: logic.locationController,
                                        hintText: 'enter_location'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'match_date'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                        name: 'match_date'.tr,
                                        controller: logic.matchDateController,
                                        hintText: 'match_date'.tr,
                                        errorText: 'match_date_is_required'.tr,
                                        icon: Icons.date_range_sharp,
                                        onTap: () {
                                          userController.selectDate(context, logic.matchDateController,
                                          );
                                          print("Match Date${logic.matchDateController.text}");
                                        },
                                        readOnly: true,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16),
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text('note'.tr, style: const TextStyle(fontSize: 16)),

                                TextField(
                                  controller: logic.noteController,
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(Icons.note_alt),
                                    hintText: "please_specify_the_purpose_of_your_request".tr,
                                    filled: true,
                                    fillColor: secondaryColor,
                                    contentPadding: const EdgeInsets.all(24.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CancelButton(
                                      btnName: "close".tr,
                                      onPress: () {
                                        homeController.updateWidgetHome(
                                          const ListPitch(),
                                        );
                                      }),
                                ),
                                OKButton(
                                  btnName: widget.update ? 'update'.tr : 'create'.tr,
                                  onPress: () async {
                                    if (widget.update && widget.pitchData != null) {
                                      // print(
                                      //     "------> ${logic.selectedProvince!.name}----${logic.selectedProvince!.code}");
                                      // print("------> ${logic.selectedTeamId!}");
                                      // print(
                                      //     "------> ${logic.amountClubController.text}");
                                      // print(
                                      //     "------> ${logic.requesterNameController.text}");
                                      // print(
                                      //     "------> ${logic.requesterPhoneController.text}");
                                      // print(
                                      //     "------> ${logic.placeAmountController.text}");
                                      // print(
                                      //     "------> ${logic.placeController.text}");
                                      // print(
                                      //     "------> ${logic.remarkController.text}");
                                      if (logic.selectedProvince != null &&
                                          logic.selectedTeamId!.isNotEmpty &&
                                          logic.amountPitchController.text.isNotEmpty &&
                                          logic.amountTableController.text.isNotEmpty &&
                                          logic.requesterNameController.text.isNotEmpty &&
                                          logic.requesterPhoneController.text.isNotEmpty &&
                                          logic.locationController.text.isNotEmpty &&
                                          logic.noteController.text.isNotEmpty) {
                                        logic.updateRequestPitch(
                                            rqPitchID: widget.pitchData!.id
                                                .toString());
                                      } else {
                                        EasyLoading.showError(
                                            'all_field_are_require'.tr,
                                            duration: const Duration(seconds: 2));
                                      }
                                    }
                                    else {
                                      if (logic.selectedProvince != null &&
                                          logic.selectedTeamId!.isNotEmpty &&
                                          logic.amountPitchController.text.isNotEmpty &&
                                          logic.amountTableController.text.isNotEmpty &&
                                          logic.requesterNameController.text.isNotEmpty &&
                                          logic.requesterPhoneController.text.isNotEmpty &&
                                          logic.locationController.text.isNotEmpty &&
                                          logic.noteController.text.isNotEmpty) {
                                        logic.requestPitch();
                                      } else {
                                        EasyLoading.showError(
                                            'all_field_are_require'.tr,
                                            duration: const Duration(seconds: 2));
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                        tablet: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              widget.update
                                  ? 'update_request_pitch'.tr
                                  : 'create_request_pitch'.tr,
                              style: const TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 20.0),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('province'.tr,
                                    style: const TextStyle(fontSize: 16)),
                                _buildLocationSelector(
                                  icon: Icons.location_on,
                                  context, 'province'.tr,
                                  logic.provinces,
                                  logic.selectedProvince?.name ?? 'select_province'.tr,
                                  logic.selectProvince,
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Column(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: [
                                Text('team_name'.tr, style: const TextStyle(fontSize: 16)),
                                DropdownButtonFormField<String>(
                                  borderRadius: BorderRadius.circular(10),
                                  value: logic.selectedTeamId,
                                  items: logic.listTeam.map((team) {
                                    return DropdownMenuItem<String>(
                                      value: team.id,
                                      child: Text(team.teamName.toString()),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      logic.selectedTeamId = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: "please_select_team".tr,
                                    floatingLabelAlignment: FloatingLabelAlignment.center,
                                    floatingLabelBehavior:FloatingLabelBehavior.never,
                                    fillColor: secondaryColor,
                                    filled: true,
                                    border: const OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(10),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('name'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_requester_name".tr,
                                        name: 'name'.tr,
                                        controller:
                                        logic.requesterNameController,
                                        hintText: 'enter_requester_name'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('phone'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_requester_phone".tr,
                                        name: 'Phone'.tr,
                                        controller:
                                        logic.requesterPhoneController,
                                        hintText: 'enter_requester_phone'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('amount_of_pitch'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_amount_of_pitch".tr,
                                        name: 'amount_of_pitch'.tr,
                                        controller: logic.amountPitchController,
                                        hintText: 'enter_amount_of_pitch'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('table_of_pitch'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_table_of_pitch".tr,
                                        name: 'table_of_pitch'.tr,
                                        controller: logic.amountTableController,
                                        hintText: 'enter_table_of_pitch'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('location'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_location".tr,
                                        name: 'location'.tr,
                                        controller: logic.locationController,
                                        hintText: 'enter_location'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'match_date'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                        name: 'match_date'.tr,
                                        controller: logic.matchDateController,
                                        hintText: 'match_date'.tr,
                                        errorText: 'match_date_is_required'.tr,
                                        icon: Icons.date_range_sharp,
                                        onTap: () {
                                          userController.selectDate(context, logic.matchDateController,
                                          );
                                          print("Match Date${logic.matchDateController.text}");
                                        },
                                        readOnly: true,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16),
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text('note'.tr, style: const TextStyle(fontSize: 16)),

                                TextField(
                                  controller: logic.noteController,
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(Icons.note_alt),
                                    hintText: "please_specify_the_purpose_of_your_request".tr,
                                    filled: true,
                                    fillColor: secondaryColor,
                                    contentPadding: const EdgeInsets.all(24.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CancelButton(
                                      btnName: "close".tr,
                                      onPress: () {
                                        homeController.updateWidgetHome(
                                          const ListPitch(),
                                        );
                                      }),
                                ),
                                OKButton(
                                  btnName: widget.update ? 'update'.tr : 'create'.tr,
                                  onPress: () async {
                                    if (widget.update && widget.pitchData != null) {
                                      // print(
                                      //     "------> ${logic.selectedProvince!.name}----${logic.selectedProvince!.code}");
                                      // print("------> ${logic.selectedTeamId!}");
                                      // print(
                                      //     "------> ${logic.amountClubController.text}");
                                      // print(
                                      //     "------> ${logic.requesterNameController.text}");
                                      // print(
                                      //     "------> ${logic.requesterPhoneController.text}");
                                      // print(
                                      //     "------> ${logic.placeAmountController.text}");
                                      // print(
                                      //     "------> ${logic.placeController.text}");
                                      // print(
                                      //     "------> ${logic.remarkController.text}");
                                      if (logic.selectedProvince != null &&
                                          logic.selectedTeamId!.isNotEmpty &&
                                          logic.amountPitchController.text.isNotEmpty &&
                                          logic.amountTableController.text.isNotEmpty &&
                                          logic.requesterNameController.text.isNotEmpty &&
                                          logic.requesterPhoneController.text.isNotEmpty &&
                                          logic.locationController.text.isNotEmpty &&
                                          logic.noteController.text.isNotEmpty) {
                                        logic.updateRequestPitch(
                                            rqPitchID: widget.pitchData!.id
                                                .toString());
                                      } else {
                                        EasyLoading.showError(
                                            'all_field_are_require'.tr,
                                            duration: const Duration(seconds: 2));
                                      }
                                    }
                                    else {
                                      if (logic.selectedProvince != null &&
                                          logic.selectedTeamId!.isNotEmpty &&
                                          logic.amountPitchController.text.isNotEmpty &&
                                          logic.amountTableController.text.isNotEmpty &&
                                          logic.requesterNameController.text.isNotEmpty &&
                                          logic.requesterPhoneController.text.isNotEmpty &&
                                          logic.locationController.text.isNotEmpty &&
                                          logic.noteController.text.isNotEmpty) {
                                        logic.requestPitch();
                                      } else {
                                        EasyLoading.showError(
                                            'all_field_are_require'.tr,
                                            duration: const Duration(seconds: 2));
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                        desktop: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              widget.update
                                  ? 'update_request_pitch'.tr
                                  : 'create_request_pitch'.tr,
                              style: const TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 20.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('province'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      _buildLocationSelector(
                                        icon: Icons.location_on,
                                        context, 'province'.tr,
                                        logic.provinces,
                                        logic.selectedProvince?.name ?? 'select_province'.tr,
                                        logic.selectProvince,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:CrossAxisAlignment.start,
                                    children: [
                                      Text('team_name'.tr, style: const TextStyle(fontSize: 16)),
                                      DropdownButtonFormField<String>(
                                        borderRadius: BorderRadius.circular(10),
                                        value: logic.selectedTeamId,
                                        items: logic.listTeam.map((team) {
                                          return DropdownMenuItem<String>(
                                            value: team.id,
                                            child: Text(team.teamName.toString()),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          setState(() {
                                            logic.selectedTeamId = value;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: "please_select_team".tr,
                                          floatingLabelAlignment: FloatingLabelAlignment.center,
                                          floatingLabelBehavior:FloatingLabelBehavior.never,
                                          fillColor: secondaryColor,
                                          filled: true,
                                          border: const OutlineInputBorder(
                                            borderSide: BorderSide.none,
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(10),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('name'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_requester_name".tr,
                                        name: 'name'.tr,
                                        controller:
                                        logic.requesterNameController,
                                        hintText: 'enter_requester_name'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('phone'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_requester_phone".tr,
                                        name: 'Phone'.tr,
                                        controller:
                                        logic.requesterPhoneController,
                                        hintText: 'enter_requester_phone'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('amount_of_pitch'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_amount_of_pitch".tr,
                                        name: 'amount_of_pitch'.tr,
                                        controller: logic.amountPitchController,
                                        hintText: 'enter_amount_of_pitch'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16), // Spacing between the two fields
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('table_of_pitch'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_table_of_pitch".tr,
                                        name: 'table_of_pitch'.tr,
                                        controller: logic.amountTableController,
                                        hintText: 'enter_table_of_pitch'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text('location'.tr,
                                          style: const TextStyle(fontSize: 16)),
                                      CustomFormBuilderTextField(
                                        errorText: "enter_location".tr,
                                        name: 'location'.tr,
                                        controller: logic.locationController,
                                        hintText: 'enter_location'.tr,
                                        icon: Icons.stadium,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'match_date'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                        name: 'match_date'.tr,
                                        controller: logic.matchDateController,
                                        hintText: 'match_date'.tr,
                                        errorText: 'match_date_is_required'.tr,
                                        icon: Icons.date_range_sharp,
                                        onTap: () {
                                          userController.selectDate(context, logic.matchDateController,
                                          );
                                          print("Match Date${logic.matchDateController.text}");
                                        },
                                        readOnly: true,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16),
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text('note'.tr, style: const TextStyle(fontSize: 16)),

                                TextField(
                                  controller: logic.noteController,
                                  decoration: InputDecoration(
                                    prefixIcon: const Icon(Icons.note_alt),
                                    hintText: "please_specify_the_purpose_of_your_request".tr,
                                    filled: true,
                                    fillColor: secondaryColor,
                                    contentPadding: const EdgeInsets.all(24.0),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      borderSide: BorderSide.none,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: CancelButton(
                                      btnName: "close".tr,
                                      onPress: () {
                                        homeController.updateWidgetHome(
                                          const ListPitch(),
                                        );
                                      }),
                                ),
                                widget.pitchData?.rStatus?.trim().toLowerCase() == AppConstants.approve.toLowerCase()
                                    ? OKButton(btnName: "update".tr, onPress: (){},colorRStatus: Colors.grey,)
                                    :OKButton(
                                  btnName: widget.update ? 'update'.tr : 'create'.tr,
                                  onPress: () async {
                                    if (widget.update && widget.pitchData != null) {
                                      // print(
                                      //     "------> ${logic.selectedProvince!.name}----${logic.selectedProvince!.code}");
                                      // print("------> ${logic.selectedTeamId!}");
                                      // print(
                                      //     "------> ${logic.amountClubController.text}");
                                      // print(
                                      //     "------> ${logic.requesterNameController.text}");
                                      // print(
                                      //     "------> ${logic.requesterPhoneController.text}");
                                      // print(
                                      //     "------> ${logic.placeAmountController.text}");
                                      // print(
                                      //     "------> ${logic.placeController.text}");
                                      // print(
                                      //     "------> ${logic.remarkController.text}");
                                      if (logic.selectedProvince != null &&
                                          logic.selectedTeamId!.isNotEmpty &&
                                          logic.amountPitchController.text.isNotEmpty &&
                                          logic.amountTableController.text.isNotEmpty &&
                                          logic.requesterNameController.text.isNotEmpty &&
                                          logic.requesterPhoneController.text.isNotEmpty &&
                                          logic.locationController.text.isNotEmpty &&
                                          logic.noteController.text.isNotEmpty) {
                                        logic.updateRequestPitch(
                                            rqPitchID: widget.pitchData!.id
                                                .toString());
                                      } else {
                                        EasyLoading.showError(
                                            'all_field_are_require'.tr,
                                            duration: const Duration(seconds: 2));
                                      }
                                    }
                                    else {
                                      if (logic.selectedProvince != null &&
                                          logic.selectedTeamId!.isNotEmpty &&
                                          logic.amountPitchController.text.isNotEmpty &&
                                          logic.amountTableController.text.isNotEmpty &&
                                          logic.requesterNameController.text.isNotEmpty &&
                                          logic.requesterPhoneController.text.isNotEmpty &&
                                          logic.locationController.text.isNotEmpty &&
                                          logic.noteController.text.isNotEmpty) {
                                        logic.requestPitch();
                                      } else {
                                        EasyLoading.showError(
                                            'all_field_are_require'.tr,
                                            duration: const Duration(seconds: 2));
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        )
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildLocationSelector(BuildContext context, String label,
      List<Address> data, String currentValue, Function(Address) onSelect,
      {bool enabled = true, IconData? icon}) {
    // if (widget.pitchData != null && label == 'Province') {
    //   currentValue = widget.pitchData!.province!.name.toString(); // Replace with actual property name from pitchData
    // }
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () => _showSelectionDialog(
              context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: secondaryColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
        kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
