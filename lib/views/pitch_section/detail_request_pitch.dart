import 'package:esport_system/data/controllers/pitch_controller.dart';
import 'package:esport_system/data/model/pitch_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/pitch_section/pitch_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class DetailRequestPitch extends StatefulWidget {
  final PitchModel pitchData;
  final bool isApprove = false;

  const DetailRequestPitch({super.key, required this.pitchData});

  @override
  State<DetailRequestPitch> createState() => _DetailRequestPitchState();
}

class _DetailRequestPitchState extends State<DetailRequestPitch> {

  var homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<PitchController>(builder: (logic) {
        return Responsive(
          tablet: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'detail_request_stadium'.tr,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 24),
                    buildText(
                        title: "team_name".tr,
                        textData: "${widget.pitchData.team!.teamName}",
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                              title: "requester_name".tr,
                              textData: "${widget.pitchData.requesterName}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: buildText(
                              title: "requester_phone".tr,
                              textData: "${widget.pitchData.requesterPhone}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                              title: "amount_of_pitch".tr,
                              textData: "${widget.pitchData.numOfPitches}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: buildText(
                              title: "amount_table".tr,
                              textData: "${widget.pitchData.numOfTables}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "location".tr,
                        textData: "${widget.pitchData.locationPitches}",
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "province".tr,
                        textData: "${widget.pitchData.province!.name}",
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "request_date".tr,
                        textData: formatDate("${widget.pitchData.requestDate}"),
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "remark".tr,
                        textData: "${widget.pitchData.remark}",
                        padding: 10,
                        height: 70),
                    const SizedBox(
                      height: 24,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(const ListPitch());
                          },
                        ),
                        widget.pitchData.rStatus?.trim().toLowerCase() ==
                                AppConstants.approve.toLowerCase()
                            ? const SizedBox()
                            : const SizedBox(
                                width: 16,
                              ),
                        widget.pitchData.rStatus?.trim().toLowerCase() ==
                                AppConstants.approve.toLowerCase()
                            ? const SizedBox()
                            : OKButton(
                                btnName: 'approve'.tr,
                                onPress: () {
                                  logic.approveRequestPitch(
                                      rqPitchID:
                                          widget.pitchData.id.toString());
                                },
                              )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          mobile: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'detail_request_stadium'.tr,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 24),
                    buildText(
                        title: "team_name".tr,
                        textData: "${widget.pitchData.team!.teamName}",
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                              title: "requester_name".tr,
                              textData: "${widget.pitchData.requesterName}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: buildText(
                              title: "requester_phone".tr,
                              textData: "${widget.pitchData.requesterPhone}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                              title: "amount_of_pitch".tr,
                              textData: "${widget.pitchData.numOfPitches}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: buildText(
                              title: "amount_table".tr,
                              textData: "${widget.pitchData.numOfTables}",
                              alignment: Alignment.centerLeft,
                              padding: 10,
                              height: 50),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "location".tr,
                        textData: "${widget.pitchData.locationPitches}",
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "province".tr,
                        textData: "${widget.pitchData.province!.name}",
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "request_date".tr,
                        textData: formatDate("${widget.pitchData.requestDate}"),
                        alignment: Alignment.centerLeft,
                        padding: 10,
                        height: 50),
                    const SizedBox(
                      height: 16,
                    ),
                    buildText(
                        title: "remark".tr,
                        textData: "${widget.pitchData.remark}",
                        padding: 10,
                        height: 70),
                    const SizedBox(
                      height: 24,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(const ListPitch());
                          },
                        ),
                        widget.pitchData.rStatus?.trim().toLowerCase() ==
                            AppConstants.approve.toLowerCase()
                            ? const SizedBox()
                            : const SizedBox(
                          width: 16,
                        ),
                        widget.pitchData.rStatus?.trim().toLowerCase() ==
                            AppConstants.approve.toLowerCase()
                            ? const SizedBox()
                            : OKButton(
                          btnName: 'approve'.tr,
                          onPress: () {
                            logic.approveRequestPitch(
                                rqPitchID:
                                widget.pitchData.id.toString());
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          desktop: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.all(16.0),
            child: SingleChildScrollView(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 150,
                      width: 150,
                      padding: const EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: secondaryColor,
                      ),
                      child: const Icon(
                        Icons.stadium_outlined,
                        size: 120,
                      )),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: secondaryColor,
                      ),
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'detail_request_stadium'.tr,
                            style: const TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 24),
                          Row(
                            children: [
                              Expanded(
                                child: buildText(
                                    title: "team_name".tr,
                                    textData:
                                        "${widget.pitchData.team!.teamName}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: buildText(
                                    title: "requester_name".tr,
                                    textData:
                                        "${widget.pitchData.requesterName}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: buildText(
                                    title: "requester_phone".tr,
                                    textData:
                                        "${widget.pitchData.requesterPhone}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: buildText(
                                    title: "amount_of_pitch".tr,
                                    textData:
                                        "${widget.pitchData.numOfPitches}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: buildText(
                                    title: "amount_table".tr,
                                    textData: "${widget.pitchData.numOfTables}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: buildText(
                                    title: "location".tr,
                                    textData:
                                        "${widget.pitchData.locationPitches}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: buildText(
                                    title: "province".tr,
                                    textData:
                                        "${widget.pitchData.province!.name}",
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: buildText(
                                    title: "request_date".tr,
                                    textData: formatDate(
                                        "${widget.pitchData.requestDate}"),
                                    alignment: Alignment.centerLeft,
                                    padding: 10,
                                    height: 50),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          buildText(
                              title: "remark".tr,
                              textData: "${widget.pitchData.remark}",
                              padding: 10,
                              height: 70),
                          const SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              CancelButton(
                                btnName: 'close'.tr,
                                onPress: () {
                                  homeController
                                      .updateWidgetHome(const ListPitch());
                                },
                              ),
                              widget.pitchData.rStatus?.trim().toLowerCase() ==
                                      AppConstants.approve.toLowerCase()
                                  ? const SizedBox()
                                  : const SizedBox(
                                      width: 16,
                                    ),
                              widget.pitchData.rStatus?.trim().toLowerCase() ==
                                      AppConstants.approve.toLowerCase()
                                  ? const SizedBox()
                                  : OKButton(
                                      btnName: 'approve'.tr,
                                      onPress: () {
                                        logic.approveRequestPitch(
                                            rqPitchID:
                                                widget.pitchData.id.toString());
                                      },
                                    )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
