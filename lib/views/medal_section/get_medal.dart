import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/medal_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/category_player.dart';
import 'package:esport_system/data/model/medal_model.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/dropdown_search_player_widget.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/medal_section/create_medal_screen.dart';
import 'package:esport_system/views/medal_section/get_medal_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../helper/images.dart';


class GetMedalScreen extends StatefulWidget {
  const GetMedalScreen({super.key});

  @override
  State<GetMedalScreen> createState() => _GetMedalScreenState();
}

class _GetMedalScreenState extends State<GetMedalScreen> {
  var teamController = Get.find<TeamController>();
  var playerController = Get.find<PlayerController>();
  var homeController = Get.find<HomeController>();
  var medalController = Get.find<MedalsPlayerController>();
  var eventController = Get.find<EventController>();

  @override
  void initState() {
    super.initState();
    fetchData();
    getPlayerCategory();
    playerDropDownSearch();
    loadDataEvent();
  }

  void fetchData() async {
    await medalController.getMedalPlayer(playerCatId: '', playerId: '');
  }

  void loadDataEvent() async {
    await eventController.getEvent();
  }

  void playerDropDownSearch() async {
    await playerController.getPlayerDropdownSearch(searchCat: "");
  }

  void getPlayerCategory() async {
    await playerController.getCategoryPlayer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<MedalsPlayerController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.all(8),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: logic.isLoading
              ? const Center(child: CircularProgressIndicator())
              : (logic.medalPlayerData.isEmpty
              ? const EmptyData()
              : LayoutBuilder(
              builder: (context, constraints) {
                return Responsive(
                    mobile: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Category and Player Selection
                        _buildCategoryAndPlayerSelection(logic, resMobile: true, tablet: false),
                        // Title Section
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          child: Center(
                            child: Text(
                              "player's_medals".tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                          ),
                        ),
                        const SizedBox(height: 11),
                        // Table Header
                        _buildTableHeaderMobile(),
                        const SizedBox(height: 11),
                        // Medals Data
                        Expanded(
                          child: ListView.builder(
                            itemCount: medalController.medalPlayerData.length,
                            itemBuilder: (context, index) {
                              final medalData = medalController.medalPlayerData[index];
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Team Name
                                  Text(medalData.title ?? 'unknown_team'.tr),
                                  const SizedBox(height: 11),
                                  Container(
                                    width: double.infinity,
                                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 4),
                                    decoration: const BoxDecoration(
                                      borderRadius: BorderRadius.all(Radius.circular(8)),
                                      color: secondaryColor,
                                    ),
                                    child: Column(
                                      children: medalData.players?.map<Widget>((player) {
                                        return Column(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.symmetric(vertical: 11.0),
                                              child: _buildPlayerRowMobile(player,),
                                            ),
                                            if(medalData.players!.length > 1)
                                              const Divider(color: Colors.white, height: 1, thickness: 0.5,)
                                          ],
                                        );
                                      }).toList() ?? [],
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    tablet: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Category and Player Selection
                        _buildCategoryAndPlayerSelection(logic, resMobile: false, tablet: true),
                        // Title Section
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          child: Center(
                            child: Text(
                              "player's_medals".tr,
                              style: const TextStyle(fontSize: 26),
                            ),
                          ),
                        ),
                        const SizedBox(height: 11),
                        // Table Header
                        _buildTableHeader(tablet: true, resMobile: false),
                        const SizedBox(height: 11),
                        // Medals Data
                        Expanded(
                          child: ListView.builder(
                            itemCount: medalController.medalPlayerData.length,
                            itemBuilder: (context, index) {
                              final medalData = medalController
                                  .medalPlayerData[index];
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // Team Name
                                    Padding(
                                      padding: const EdgeInsets.only(left: 120),
                                      child: Text(
                                          medalData.title ?? 'unknown_team'.tr),
                                    ),
                                    const SizedBox(height: 11),
                                    Container(
                                      width: double.infinity,
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 20, horizontal: 20),
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(8)),
                                        color: secondaryColor,
                                      ),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment
                                            .start,
                                        children: medalData.players?.map<
                                            Widget>((player) {
                                          return Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets
                                                    .symmetric(vertical: 11.0),
                                                child: _buildPlayerRow(player,
                                                    resMobile: false,
                                                    tablet: true),
                                              ),
                                              if(medalData.players!.length > 1)
                                                const Divider(color: Colors
                                                    .white,
                                                  height: 1,
                                                  thickness: 0.5,)
                                            ],
                                          );
                                        }).toList() ?? [],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                    desktop: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Category and Player Selection
                        _buildCategoryAndPlayerSelection(logic, resMobile: false, tablet: false),
                        // Title Section
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          child: Center(
                            child: Text(
                              "player's_medals".tr,
                              style: const TextStyle(fontSize: 26),
                            ),
                          ),
                        ),
                        const SizedBox(height: 11),

                        // Table Header
                        _buildTableHeader(resMobile: false,tablet: false),

                        const SizedBox(height: 11),

                        // Medals Data
                        Expanded(
                          child: ListView.builder(
                            itemCount: medalController.medalPlayerData.length,
                            itemBuilder: (context, index) {
                              final medalData = medalController.medalPlayerData[index];
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // Team Name
                                    Text(medalData.title ?? 'unknown_team'.tr),
                                    const SizedBox(height: 11),
                                    Container(
                                      width: double.infinity,
                                      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                      decoration: const BoxDecoration(
                                        borderRadius: BorderRadius.all(Radius.circular(8)),
                                        color: secondaryColor,
                                      ),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: medalData.players?.map<Widget>((player) {
                                          return Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.symmetric(vertical: 11.0),
                                                child: _buildPlayerRow(player, tablet: false, resMobile: false),
                                              ),
                                              if(medalData.players!.length > 1)
                                                const Divider(color: Colors
                                                    .white,
                                                  height: 1,
                                                  thickness: 0.5,)
                                            ],
                                          );
                                        }).toList() ?? [],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    )
                );
              }
          )
          ),
        );
      }),
    );
  }

  Widget _buildCategoryAndPlayerSelection(MedalsPlayerController logic,
      {required bool? resMobile, required bool? tablet}) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Column(
        children: [
          if(resMobile!)...[
            _buildPlayerCategoryDropdown(),
            const SizedBox(height: 10,),
            _buildPlayerDropdown(logic),
            const SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: [
                    if ((logic.selectPlayerId?.isNotEmpty ?? false) && (logic.selectCategoryPlayer?.isNotEmpty ?? false))
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                // Clear the selected category and player
                                logic.selectCategoryPlayer = null;
                                logic.selectPlayerId = null;

                                // Reset the dropdown selections by updating the GetBuilder state
                                playerController.getPlayerDropdownSearch(searchCat: ''); // Clear player dropdown search
                                logic.previousSelectPlayer = null; // Clear previous player selection

                                logic.getMedalPlayer(playerCatId: '', playerId: '');

                                // Trigger an update in the controller to reset the UI
                                logic.update();
                              });
                            },
                            child: Container(
                              height: 35,
                              width: 40,
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: bgColor,
                              ),
                              child: const Icon(
                                Icons.clear,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ],
                      ),
                    (logic.selectPlayerId?.isNotEmpty ?? false) && (logic.selectCategoryPlayer?.isNotEmpty ?? false)
                        ? const SizedBox(width: 10)
                        : const SizedBox(),
                    OKButton(
                      btnName: 'search'.tr,
                      onPress: () async {
                        await medalController.getMedalPlayer(playerCatId: logic.selectCategoryPlayer, playerId: logic.selectPlayerId);
                      },
                    ),
                  ],
                ),

              ],
            )
          ]
          else if(tablet!)...[
              Column(
                children: [
                  _buildPlayerCategoryDropdown(),
                  const SizedBox(height: 10,),
                  _buildPlayerDropdown(logic),
                  const SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      if ((logic.selectPlayerId?.isNotEmpty ?? false) && (logic.selectCategoryPlayer?.isNotEmpty ?? false))
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                // Clear the selected category and player
                                logic.selectCategoryPlayer = null;
                                logic.selectPlayerId = null;

                                // Reset the dropdown selections by updating the GetBuilder state
                                playerController.getPlayerDropdownSearch(searchCat: ''); // Clear player dropdown search
                                logic.previousSelectPlayer = null; // Clear previous player selection

                                logic.getMedalPlayer(playerCatId: '', playerId: '');

                                // Trigger an update in the controller to reset the UI
                                logic.update();
                              });
                            },
                            child: Container(
                              height: 40,
                              width: 45,
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: bgColor,
                              ),
                              child: const Icon(
                                Icons.clear,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ],
                      ),
                      (logic.selectPlayerId?.isNotEmpty ?? false) && (logic.selectCategoryPlayer?.isNotEmpty ?? false)
                          ? const SizedBox(width: 10)
                          : const SizedBox(),
                      OKButton(
                        btnName: 'search'.tr,
                        onPress: () async {
                          await medalController.getMedalPlayer(playerCatId: logic.selectCategoryPlayer, playerId: logic.selectPlayerId);
                        },
                      ),
                    ],
                  )
                ],
              )
            ]
          else ...[
                Row(
                  children: [
                    Expanded(
                      child: _buildPlayerCategoryDropdown(),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: _buildPlayerDropdown(logic),
                    ),
                    (logic.selectPlayerId?.isNotEmpty ?? false) && (logic.selectCategoryPlayer?.isNotEmpty ?? false)
                        ? const SizedBox(width: 10)
                        : const SizedBox(),
                    if ((logic.selectPlayerId?.isNotEmpty ?? false) && (logic.selectCategoryPlayer?.isNotEmpty ?? false))
                      Column(
                        children: [
                          const Text(""),
                          const SizedBox(height: 5,),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                // Clear the selected category and player
                                logic.selectCategoryPlayer = null;
                                logic.selectPlayerId = null;
                                // Reset the dropdown selections by updating the GetBuilder state
                                playerController.getPlayerDropdownSearch(searchCat: ''); // Clear player dropdown search
                                logic.previousSelectPlayer = null; // Clear previous player selection
                                logic.getMedalPlayer(playerCatId: '', playerId: '');
                                // Trigger an update in the controller to reset the UI
                                logic.update();
                              });
                            },
                            child: Container(
                              height: 48,
                              width: 60,
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: bgColor,
                              ),
                              child: const Icon(
                                Icons.clear,
                                color: Colors.red,
                              ),
                            ),
                          ),
                        ],
                      )

                  ],
                ),
                const SizedBox(height: 16.0),
                Center(
                  child: OKButton(
                    btnName: 'search'.tr,
                    onPress: () async {
                      await medalController.getMedalPlayer(
                        playerCatId: logic.selectCategoryPlayer,
                        playerId: logic.selectPlayerId,
                      );
                    },
                  ),
                ),
              ]
        ],
      ),
    );
  }

  Widget _buildPlayerCategoryDropdown() {
    return GetBuilder<MedalsPlayerController>(builder: (logic) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'select_player_category'.tr,
            style: const TextStyle(fontSize: 18),
          ),
          const SizedBox(height: 5.0),
          FormBuilderDropdown(
            name: 'select_player_category'.tr,
            borderRadius: BorderRadius.circular(10),
            decoration: InputDecoration(
              floatingLabelAlignment: FloatingLabelAlignment.center,
              floatingLabelBehavior: FloatingLabelBehavior.never,
              fillColor: bgColor,
              labelText: 'select_player_category'.tr,
              filled: true,
              border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(10)),
            ),
            //initialValue: logic.selectCategoryPlayer,
            items: playerController.playerCategories.map((option) =>
                DropdownMenuItem(
                  value: option,
                  child: Text(option.title.toString()),
                )).toList(),
            onChanged: (value) {
              setState(() {
                CategoryPlayer? a;
                a = value;
                logic.selectCategoryPlayer = a!.id.toString();
                playerController.getPlayerDropdownSearch(searchCat: a.title);
              });
            },
          ),
        ],
      );
    });
  }

  Widget _buildPlayerDropdown(MedalsPlayerController logic) {
    return GetBuilder<PlayerController>(builder: (a) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('select_player'.tr, style: const TextStyle(fontSize: 16)),
          const SizedBox(height: 5.0),
          PlayerDropdownSearch(
              selectedItem: logic.previousSelectPlayer,
              onChanged: (value) {
                setState(() {
                  logic.selectPlayerId = value!.id.toString();
                  logic.previousSelectPlayer = value;
                });
              }
          )
        ],
      );
    });
  }

  Widget _buildTableHeader({required bool? resMobile, required bool? tablet}) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: secondaryColor,
      ),
      child: Center(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if(tablet!)...[
              Expanded(
                flex: 3,
                child: Text(
                  'player_name'.tr,
                  textAlign: TextAlign.start, // Ensures player name starts from the left
                  style: const TextStyle(fontWeight: FontWeight.bold), // Optional for styling
                ),
              ),
            ]
            else ...[
              Expanded(
                flex: 3,
                child: Text(
                  'player_name'.tr,
                  textAlign: TextAlign.start, // Ensures player name starts from the left
                  style: const TextStyle(fontWeight: FontWeight.bold), // Optional for styling
                ),
              ),
            ],
            const SizedBox(width: 5),
            Expanded(
              flex: 2,
                child: Center(
                    child: Row(
                        children: [
                          if(tablet)...[
                              Image.asset(
                                Images.goldMedalImage, width: 24, height: 24,),
                              const SizedBox(width: 0,),
                              Text('gold'.tr),
                            ]
                          else ...[
                                Image.asset(Images.goldMedalImage, width: 30,
                                  height: 30,),
                                const SizedBox(width: 8,),
                                Text('gold_medal'.tr),
                            ]
                        ]
                    )
                )
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 2,
                child: Center(
                    child: Row(
                      children: [
                        if(tablet)...[
                            Image.asset(Images.silverMedalImage, width: 24, height: 24,),
                            const SizedBox(width: 0,),
                            Text('silver'.tr),
                          ]
                        else ...[
                              Image.asset(Images.silverMedalImage, width: 30, height: 30),
                              const SizedBox(width: 8,),
                              Text('silver_medal'.tr),
                            ]
                      ],
                    )
                )
            ),
            const SizedBox(width: 10),
            Expanded(flex: 2,
                child: Center(
                    child: Row(
                      children: [
                        if(tablet)...[
                            Image.asset(Images.brownCardImage, width: 24, height: 24,),
                            const SizedBox(width: 0,),
                            Text('bronze'.tr),
                          ]
                        else ...[
                              Image.asset(Images.brownCardImage, width: 30, height: 30,),
                              const SizedBox(width: 8,),
                              Text('bronze_medal'.tr),
                            ]
                      ],
                    )
                )
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 2,
              child: Center(
                child: SizedBox(
                  width: 100, // Define a width for the Stack
                  height: 30, // Define a height for the Stack
                  child: Stack(
                    clipBehavior: Clip.none,
                    // Allows the images to overlap outside the stack bounds
                    children: [
                      if(tablet)...[
                          Positioned(
                            left: 0,
                            child: Image.asset(
                              Images.goldMedalImage,
                              width: 24,
                              height: 24,
                            ),
                          ),
                          Positioned(
                            left: 10,
                            // Adjust this value to make the silver medal half overlap the gold medal
                            child: Image.asset(
                              Images.silverMedalImage,
                              width: 24,
                              height: 24,
                            ),
                          ),
                          Positioned(
                            left: 20,
                            // Adjust this value to make the brown medal half overlap the silver medal
                            child: Image.asset(
                              Images.brownCardImage,
                              width: 24,
                              height: 24,
                            ),
                          ),
                          Positioned(
                            left: 45,
                            top: 5,
                            // Adjust this value to position the text appropriately
                            child: Text('total'.tr),
                          ),
                        ]
                      else ...[
                            Positioned(
                              left: 0,
                              child: Image.asset(
                                Images.goldMedalImage,
                                width: 30,
                                height: 30,
                              ),
                            ),
                            Positioned(
                              left: 10,
                              // Adjust this value to make the silver medal half overlap the gold medal
                              child: Image.asset(
                                Images.silverMedalImage,
                                width: 30,
                                height: 30,
                              ),
                            ),
                            Positioned(
                              left: 20,
                              // Adjust this value to make the brown medal half overlap the silver medal
                              child: Image.asset(
                                Images.brownCardImage,
                                width: 30,
                                height: 30,
                              ),
                            ),
                            Positioned(
                              left: 60,
                              top: 5,
                              // Adjust this value to position the text appropriately
                              child: Text('total_medal'.tr),
                            ),
                          ]
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(width: 5),
            Expanded(
              flex: 2,
                child: Center(
                    child: Text('actions'.tr)
                )
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPlayerRow(Players player, {required bool? resMobile, required bool? tablet}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        if(tablet!)...[
          Expanded(
            flex: 3,
            child: Text(
              player.playerName!,
              textAlign: TextAlign.start, // Ensures player name starts from the left
              style: const TextStyle(fontWeight: FontWeight.bold), // Optional for styling
            ),
          ),
          ]
        else ...[
          Expanded(
            flex: 3,
            child: Text(
              player.playerName!,
              textAlign: TextAlign.start, // Ensures player name starts from the left
              style: const TextStyle(fontWeight: FontWeight.bold), // Optional for styling
            ),
          ),
        ],
        const SizedBox(width: 3),
        Expanded(
            flex: 2,
            child: Center(
                child: Text(player.medals?.goldMedals ?? '0')
            )
        ),
        const SizedBox(width: 3),
        Expanded(
            flex: 2,
            child: Center(
                child: Text(player.medals?.silverMedals ?? '0')
            )
        ),
        const SizedBox(width: 3),
        Expanded(
            flex: 2,
            child: Center(
                child: Text(player.medals?.bronzeMedals ?? '0')
            )
        ),
        const SizedBox(width: 3),
        Expanded(
            flex: 2,
            child: Center(
                child: Text(player.medals?.totalMedals ?? '0')
            )
        ),
        const SizedBox(width: 3),
        Expanded(
          flex: 2,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    PlayerModel playerModel = PlayerModel(
                      id: player.id,
                    );
                    homeController.updateWidgetHome(GetDetailMedalScreen(
                        playerModel: playerModel, player: player));
                  },
                  child: Container(
                    padding: const EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      color: Colors.amberAccent.withOpacity(0.1),
                      border: Border.all(width: 1, color: Colors.amberAccent),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: const Icon(
                      Icons.visibility,
                      size: 16,
                      color: Colors.amberAccent,
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                GestureDetector(
                  onTap: () {
                    PlayerModel playerModel = PlayerModel(
                      id: player.id,
                    );
                    print('ID: ${player.id}');
                    // Passing the PlayerModel to the CreateMedalScreen
                    homeController.updateWidgetHome(CreateMedalScreen(
                        playerModel: playerModel, player: player)
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.all(6),
                    decoration: BoxDecoration(
                      color: Colors.blue.withOpacity(0.1),
                      border: Border.all(width: 1, color: Colors.blue),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: const Icon(
                      Icons.add,
                      size: 20,
                      color: Colors.blue,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildTableHeaderMobile() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 8),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: secondaryColor,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // Player name aligned to the left
          Expanded(
            flex: 3,
            child: Text(
              'player_name'.tr,
              textAlign: TextAlign.start, // Ensures player name starts from the left
              style: const TextStyle(fontWeight: FontWeight.bold), // Optional for styling
            ),
          ),
          // Medal images and actions are centered
          Expanded(
            flex: 1,
            child: Center(
              child: Image.asset(Images.goldMedalImage, width: 24, height: 24),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Image.asset(Images.silverMedalImage, width: 24, height: 24),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Image.asset(Images.brownCardImage, width: 24, height: 24),
            ),
          ),
          Expanded(
            flex: 2,
            child: Center(
              child: Text('actions'.tr),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPlayerRowMobile(Players player) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 3,
            // Align the player name to the start
            child: Text(
              player.playerName!,
              textAlign: TextAlign.start, // Ensure it starts from the left
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(player.medals?.goldMedals ?? '0'),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(player.medals?.silverMedals ?? '0'),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(player.medals?.bronzeMedals ?? '0'),
            ),
          ),
          Expanded(
            flex: 2,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    PlayerModel playerModel = PlayerModel(id: player.id);
                    homeController.updateWidgetHome(GetDetailMedalScreen(
                        playerModel: playerModel, player: player));
                  },
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Colors.amberAccent.withOpacity(0.1),
                      border: Border.all(width: 1, color: Colors.amberAccent),
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: const Icon(
                      Icons.visibility,
                      size: 16,
                      color: Colors.amberAccent,
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                GestureDetector(
                  onTap: () {
                    PlayerModel playerModel = PlayerModel(id: player.id);
                    homeController.updateWidgetHome(CreateMedalScreen(
                        playerModel: playerModel, player: player));
                  },
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    decoration: BoxDecoration(
                      color: Colors.blue.withOpacity(0.1),
                      border: Border.all(width: 1, color: Colors.blue),
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: const Icon(
                      Icons.add,
                      size: 16,
                      color: Colors.blue,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }


}

