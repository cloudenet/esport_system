import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/medal_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/medal_model.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/medal_section/get_medal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CreateMedalScreen extends StatefulWidget {
  final PlayerModel playerModel;
  final Players? player;

  const CreateMedalScreen({super.key, required this.playerModel, this.player});

  @override
  State<CreateMedalScreen> createState() => _CreateMedalScreenState();
}

class _CreateMedalScreenState extends State<CreateMedalScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  var playerController = Get.find<PlayerController>();
  var homeController = Get.find<HomeController>();
  String? selectedPlayer;
  var eventController = Get.find<EventController>();
  var userController = Get.find<UserController>();
  String? selectedEventId;
  String? selectedMedal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<MedalsPlayerController>(
        builder: (logic) {
          return Container(
            margin: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                _buildCategoryAndPlayerSelection(widget.player),
                Card(
                  color: secondaryColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormBuilder(
                      key: _formKey,
                      child: Responsive(
                        mobile: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                'add_medal_to_player'.tr,
                                style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold,),
                              ),
                              const SizedBox(height: 16,),

                              Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'select_medal'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 10.0),
                                  FormBuilderDropdown(
                                    name: 'medal'.tr,
                                    borderRadius: BorderRadius.circular(10),
                                    decoration: InputDecoration(
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      floatingLabelBehavior: FloatingLabelBehavior.never,
                                      fillColor: bgColor,
                                      labelText: 'please_select_medal'.tr,
                                      filled: true,
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                    ),
                                    items: logic.medalOptions.map((option) {
                                      return DropdownMenuItem(
                                        value: option['value'],
                                        child: Text(option['text']!),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        selectedMedal = value;
                                      });
                                      print("Medal: $selectedMedal");
                                    },
                                    validator: (value) {
                                      if (value == null) {
                                        return 'please_select_medal'.tr;
                                      }
                                      return null; // No error if value is selected
                                    },
                                  )

                                ],
                              ),
                              const SizedBox(height: 16,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'select_event'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 10.0),
                                  DropdownButtonFormField<String>(
                                    borderRadius: BorderRadius.circular(10),
                                    value: selectedEventId,
                                    items: eventController.eventData.map((event) {
                                      return DropdownMenuItem<String>(
                                        value: event.id,
                                        child: Text(event.title!),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        selectedEventId = value;
                                        print("Event ID: $selectedEventId");
                                      });
                                    },
                                    validator: (value) {
                                      if (value == null) {
                                        return 'please_select_an_event'.tr;
                                      }
                                      return null; // No error if value is selected
                                    },
                                    decoration: InputDecoration(
                                      labelText: "please_select_event".tr,
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      floatingLabelBehavior: FloatingLabelBehavior.never,
                                      fillColor: bgColor,
                                      filled: true,
                                      border: const OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              const SizedBox(height: 16,),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'qty_madel'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 10.0),
                                        CustomFormBuilderTextField(
                                          name: 'qty_madel'.tr,
                                          controller:
                                          logic.qtyMedalsController,
                                          hintText: 'qty_madel'.tr,
                                          errorText:
                                          'qty_madel_is_required'.tr,
                                          icon: Icons.sports_baseball,
                                          fillColor: bgColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'win_date'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 10.0),
                                        CustomFormBuilderTextField(
                                          name: 'win_date'.tr,
                                          controller:
                                          logic.winDateController,
                                          hintText: 'win_date'.tr,
                                          errorText: 'win_date_is_required'.tr,
                                          icon: Icons.date_range_sharp,
                                          onTap: () {
                                            userController.selectDate(
                                              context,
                                              logic.winDateController,
                                            );
                                            print("Win date ${logic.winDateController.text}");
                                          },
                                          readOnly: true,
                                          fillColor: bgColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CancelButton(
                                      btnName: "cancel".tr,
                                      onPress: () {
                                        selectedMedal = null;
                                        selectedEventId = null;
                                        logic.qtyMedalsController.text = '';
                                        logic.winDateController.text = '';
                                        homeController.updateWidgetHome(const GetMedalScreen());
                                      }),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  OKButton(
                                    btnName: "save".tr,
                                    onPress: () async {
                                      if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                        logic.isLoading = true;
                                        logic.update();
                                        EasyLoading.show(
                                          status: 'loading...'.tr,
                                          dismissOnTap: true,
                                        );
                                        await logic.createMedalPlayer(
                                            playerId: widget.playerModel.id.toString(),
                                            typeMedal: selectedMedal.toString(),
                                            eventId: selectedEventId.toString(),
                                            qtyMedal: logic.qtyMedalsController.text,
                                            dateWin: logic.winDateController.text,
                                            createdBy: userInfo!.id.toString()).then((status) {
                                          if (status) {
                                            EasyLoading.showSuccess(
                                              'medal_created_successfully'.tr,
                                              dismissOnTap: true,
                                              duration: const Duration(seconds: 2),
                                            );
                                            selectedMedal = null;
                                            selectedEventId = null;
                                            logic.qtyMedalsController.text = '';
                                            logic.winDateController.text = '';
                                            homeController.updateWidgetHome(const GetMedalScreen());
                                          } else {
                                            EasyLoading.showError('something_went_wrong'.tr,duration: const Duration(seconds: 2));
                                          }
                                          EasyLoading.dismiss();
                                        },
                                        );
                                      }
                                    },
                                  )
                                ],
                              )
                            ]),
                        tablet: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            'add_medal_to_player'.tr,
                            style: const TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 24,),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'select_medal'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    const SizedBox(height: 10.0),
                                    FormBuilderDropdown(
                                      name: 'medal'.tr,
                                      borderRadius: BorderRadius.circular(10),
                                      decoration: InputDecoration(
                                        floatingLabelAlignment: FloatingLabelAlignment.center,
                                        floatingLabelBehavior: FloatingLabelBehavior.never,
                                        fillColor: bgColor,
                                        labelText: 'please_select_medal'.tr,
                                        filled: true,
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                      ),
                                      items: logic.medalOptions.map((option) {
                                        return DropdownMenuItem(
                                          value: option['value'],
                                          child: Text(option['text']!),
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        setState(() {
                                          selectedMedal = value;
                                        });
                                        print("Medal: $selectedMedal");
                                      },
                                      validator: (value) {
                                        if (value == null) {
                                          return 'please_select_medal'.tr;
                                        }
                                        return null; // No error if value is selected
                                      },
                                    )

                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 16,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'select_event'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    const SizedBox(height: 10.0),
                                    DropdownButtonFormField<String>(
                                      borderRadius: BorderRadius.circular(10),
                                      value: selectedEventId,
                                      items: eventController.eventData.map((event) {
                                        return DropdownMenuItem<String>(
                                          value: event.id,
                                          child: Text(event.title!),
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        setState(() {
                                          selectedEventId = value;
                                          print("Event ID: $selectedEventId");
                                        });
                                      },
                                      validator: (value) {
                                        if (value == null) {
                                          return 'please_select_an_event'.tr;
                                        }
                                        return null; // No error if value is selected
                                      },
                                      decoration: InputDecoration(
                                        labelText: "please_select_event".tr,
                                        floatingLabelAlignment: FloatingLabelAlignment.center,
                                        floatingLabelBehavior: FloatingLabelBehavior.never,
                                        fillColor: bgColor,
                                        filled: true,
                                        border: const OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          borderRadius: BorderRadius.all(Radius.circular(10)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                            ],
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'qty_madel'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    const SizedBox(height: 10.0),
                                    CustomFormBuilderTextField(
                                      name: 'qty_madel'.tr,
                                      controller:
                                          logic.qtyMedalsController,
                                      hintText: 'qty_madel'.tr,
                                      errorText:
                                          'qty_madel_is_required'.tr,
                                      icon: Icons.sports_baseball,
                                      fillColor: bgColor,
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 16,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'win_date'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    const SizedBox(height: 10.0),
                                    CustomFormBuilderTextField(
                                      name: 'win_date'.tr,
                                      controller:
                                          logic.winDateController,
                                      hintText: 'win_date'.tr,
                                      errorText: 'win_date_is_required'.tr,
                                      icon: Icons.date_range_sharp,
                                      onTap: () {
                                        userController.selectDate(
                                          context,
                                          logic.winDateController,
                                        );
                                        print("Win date ${logic.winDateController.text}");
                                      },
                                      readOnly: true,
                                      fillColor: bgColor,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              CancelButton(
                                btnName: "cancel".tr,
                                onPress: () {
                                  selectedMedal = null;
                                  selectedEventId = null;
                                  logic.qtyMedalsController.text = '';
                                  logic.winDateController.text = '';
                                  homeController.updateWidgetHome(const GetMedalScreen());
                                }),
                              const SizedBox(
                                width: 16,
                              ),
                              OKButton(
                                btnName: "save".tr,
                                onPress: () async {
                                  if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                    logic.isLoading = true;
                                    logic.update();
                                    EasyLoading.show(
                                      status: 'loading...'.tr,
                                      dismissOnTap: true,
                                    );
                                    await logic.createMedalPlayer(
                                      playerId: widget.playerModel.id.toString(),
                                      typeMedal: selectedMedal.toString(),
                                      eventId: selectedEventId.toString(),
                                      qtyMedal: logic.qtyMedalsController.text,
                                      dateWin: logic.winDateController.text,
                                      createdBy: userInfo!.id.toString()).then((status) {
                                        if (status) {
                                          EasyLoading.showSuccess(
                                            'medal_created_successfully'.tr,
                                            dismissOnTap: true,
                                            duration: const Duration(seconds: 2),
                                          );
                                          selectedMedal = null;
                                          selectedEventId = null;
                                          logic.qtyMedalsController.text = '';
                                          logic.winDateController.text = '';
                                          homeController.updateWidgetHome(const GetMedalScreen());
                                        } else {
                                          EasyLoading.showError('something_went_wrong'.tr,duration: const Duration(seconds: 2));
                                        }
                                        EasyLoading.dismiss();
                                      },
                                    );
                                  }
                                },
                              )
                            ],
                          )
                        ]),
                        desktop: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                'add_medal_to_player'.tr,
                                style: const TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              const SizedBox(height: 24,),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'select_medal'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 10.0),
                                        FormBuilderDropdown(
                                          name: 'medal'.tr,
                                          borderRadius: BorderRadius.circular(10),
                                          decoration: InputDecoration(
                                            floatingLabelAlignment: FloatingLabelAlignment.center,
                                            floatingLabelBehavior: FloatingLabelBehavior.never,
                                            fillColor: bgColor,
                                            labelText: 'please_select_medal'.tr,
                                            filled: true,
                                            border: OutlineInputBorder(
                                              borderSide: BorderSide.none,
                                              borderRadius: BorderRadius.circular(10),
                                            ),
                                          ),
                                          items: logic.medalOptions.map((option) {
                                            return DropdownMenuItem(
                                              value: option['value'],
                                              child: Text(option['text']!),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              selectedMedal = value;
                                            });
                                            print("Medal: $selectedMedal");
                                          },
                                          validator: (value) {
                                            if (value == null) {
                                              return 'please_select_medal'.tr;
                                            }
                                            return null; // No error if value is selected
                                          },
                                        )

                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'select_event'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 10.0),
                                        DropdownButtonFormField<String>(
                                          borderRadius: BorderRadius.circular(10),
                                          value: selectedEventId,
                                          items: eventController.eventData.map((event) {
                                            return DropdownMenuItem<String>(
                                              value: event.id,
                                              child: Text(event.title!),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              selectedEventId = value;
                                              print("Event ID: $selectedEventId");
                                            });
                                          },
                                          validator: (value) {
                                            if (value == null) {
                                              return 'please_select_an_event'.tr;
                                            }
                                            return null; // No error if value is selected
                                          },
                                          decoration: InputDecoration(
                                            labelText: "please_select_event".tr,
                                            floatingLabelAlignment: FloatingLabelAlignment.center,
                                            floatingLabelBehavior: FloatingLabelBehavior.never,
                                            fillColor: bgColor,
                                            filled: true,
                                            border: const OutlineInputBorder(
                                              borderSide: BorderSide.none,
                                              borderRadius: BorderRadius.all(Radius.circular(10)),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                                ],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'qty_madel'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 10.0),
                                        CustomFormBuilderTextField(
                                          name: 'qty_madel'.tr,
                                          controller:
                                          logic.qtyMedalsController,
                                          hintText: 'qty_madel'.tr,
                                          errorText:
                                          'qty_madel_is_required'.tr,
                                          icon: Icons.sports_baseball,
                                          fillColor: bgColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'win_date'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 10.0),
                                        CustomFormBuilderTextField(
                                          name: 'win_date'.tr,
                                          controller:
                                          logic.winDateController,
                                          hintText: 'win_date'.tr,
                                          errorText: 'win_date_is_required'.tr,
                                          icon: Icons.date_range_sharp,
                                          onTap: () {
                                            userController.selectDate(
                                              context,
                                              logic.winDateController,
                                            );
                                            print("Win date ${logic.winDateController.text}");
                                          },
                                          readOnly: true,
                                          fillColor: bgColor,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CancelButton(
                                      btnName: "cancel".tr,
                                      onPress: () {
                                        selectedMedal = null;
                                        selectedEventId = null;
                                        logic.qtyMedalsController.text = '';
                                        logic.winDateController.text = '';
                                        homeController.updateWidgetHome(const GetMedalScreen());
                                      }),
                                  const SizedBox(
                                    width: 16,
                                  ),
                                  OKButton(
                                    btnName: "save".tr,
                                    onPress: () async {
                                      if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                        logic.isLoading = true;
                                        logic.update();
                                        EasyLoading.show(
                                          status: 'loading...'.tr,
                                          dismissOnTap: true,
                                        );
                                        await logic.createMedalPlayer(
                                            playerId: widget.playerModel.id.toString(),
                                            typeMedal: selectedMedal.toString(),
                                            eventId: selectedEventId.toString(),
                                            qtyMedal: logic.qtyMedalsController.text,
                                            dateWin: logic.winDateController.text,
                                            createdBy: userInfo!.id.toString()).then((status) {
                                          if (status) {
                                            EasyLoading.showSuccess(
                                              'medal_created_successfully'.tr,
                                              dismissOnTap: true,
                                              duration: const Duration(seconds: 2),
                                            );
                                            selectedMedal = null;
                                            selectedEventId = null;
                                            logic.qtyMedalsController.text = '';
                                            logic.winDateController.text = '';
                                            homeController.updateWidgetHome(const GetMedalScreen());
                                          } else {
                                            EasyLoading.showError('something_went_wrong'.tr,duration: const Duration(seconds: 2));
                                          }
                                          EasyLoading.dismiss();
                                        },
                                        );
                                      }
                                    },
                                  )
                                ],
                              )
                            ]),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  Widget _buildCategoryAndPlayerSelection(Players? player) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.all(8.0),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("player_info".tr,style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
            const SizedBox(height: 24,),
            Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('player_name'.tr),
                      const SizedBox(height: 10,),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Center(child: Text(player?.playerName ?? 'Unknown Player')),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('khmer_name'.tr),
                      const SizedBox(height: 10,),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Center(child: Text(player?.playerNameKh ?? 'Unknown Player Khmer Name')),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
