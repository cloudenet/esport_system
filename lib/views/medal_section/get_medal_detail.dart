import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/medal_section/get_medal.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/medal_controller.dart';
import 'package:esport_system/data/model/medal_model.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';

import '../../data/controllers/home_controller.dart';

class GetDetailMedalScreen extends StatefulWidget {
  final PlayerModel playerModel;
  final Players? player;

  const GetDetailMedalScreen({super.key, required this.playerModel, this.player});

  @override
  State<GetDetailMedalScreen> createState() => _GetDetailMedalScreenState();
}

class _GetDetailMedalScreenState extends State<GetDetailMedalScreen> {
  var medalController = Get.find<MedalsPlayerController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    await medalController.getMedalDetail(playerId: widget.playerModel.id.toString());
    setState(() {}); // Trigger rebuild after data is loaded
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<MedalsPlayerController>(
        builder: (controller) {
          if (controller.isLoading) {
            return const Center(child: CircularProgressIndicator());
          }

          if (controller.medalPlayerDetailData.isEmpty ||
              controller.medalPlayerDetailData.every((medal) => medal.typeMedals == '0')) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildCategoryAndPlayerSelection(widget.player),
                const SizedBox(height: 16,),
                Text("medal_that_receive_from_event".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                const SizedBox(height: 200),
                const EmptyData(),
                const SizedBox(height: 24,),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: CancelButton(btnName: "close".tr, onPress: (){
                      homeController.updateWidgetHome(const GetMedalScreen());
                    }),
                  ),
                )
              ],
            );
          }

          return Responsive(
              mobile: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildCategoryAndPlayerSelection(widget.player),
                  const SizedBox(height: 10,),
                  Text("medal_that_receive_from_event".tr,style: const TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                  const SizedBox(height: 10,),
                  _buildTableHeader(resMobile: true,tablet: false),
                  const SizedBox(height: 10),
                  Expanded(
                    child: ListView.builder(
                      itemCount: controller.medalPlayerDetailData.length,
                      itemBuilder: (context, index) {
                        final medalData = controller.medalPlayerDetailData[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 50),
                                child: Text(formatMedalType(medalData.typeMedals.toString())),
                              ),
                              const SizedBox(height: 11),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    color: secondaryColor,
                                  ),
                                  child: medalData.event != null
                                      ? _buildPlayerRow(medalData.event!, medalData,tablet: false,resMobile: true)
                                      : const SizedBox.shrink(), // Handle the case where there is no event
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 24,),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: CancelButton(btnName: "close".tr, onPress: (){
                        homeController.updateWidgetHome(const GetMedalScreen());
                      }),
                    ),
                  )
                ],
              ),
              tablet: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildCategoryAndPlayerSelection(widget.player),
                  const SizedBox(height: 24,),
                  Text("medal_that_receive_from_event".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                  const SizedBox(height: 16,),
                  _buildTableHeader(tablet: true,resMobile: false),
                  const SizedBox(height: 11),
                  Expanded(
                    child: ListView.builder(
                      itemCount: controller.medalPlayerDetailData.length,
                      itemBuilder: (context, index) {
                        final medalData = controller.medalPlayerDetailData[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 50),
                                child: Text(formatMedalType(medalData.typeMedals.toString())),
                              ),
                              const SizedBox(height: 11),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    color: secondaryColor,
                                  ),
                                  child: medalData.event != null
                                      ? _buildPlayerRow(medalData.event!, medalData,resMobile: false,tablet: true)
                                      : const SizedBox.shrink(), // Handle the case where there is no event
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 24,),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: CancelButton(btnName: "close".tr, onPress: (){
                        homeController.updateWidgetHome(const GetMedalScreen());
                      }),
                    ),
                  )
                ],
              ),
              desktop: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _buildCategoryAndPlayerSelection(widget.player),
                  const SizedBox(height: 24,),
                  Text("medal_that_receive_from_event".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                  const SizedBox(height: 16,),
                  _buildTableHeader(resMobile: false,tablet: false),
                  const SizedBox(height: 11),
                  Expanded(
                    child: ListView.builder(
                      itemCount: controller.medalPlayerDetailData.length,
                      itemBuilder: (context, index) {
                        final medalData = controller.medalPlayerDetailData[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 50),
                                child: Text(formatMedalType(medalData.typeMedals.toString())),
                              ),
                              const SizedBox(height: 11),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: double.infinity,
                                  padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(8)),
                                    color: secondaryColor,
                                  ),
                                  child: medalData.event != null
                                      ? _buildPlayerRow(medalData.event!, medalData,tablet: false,resMobile: false)
                                      : const SizedBox.shrink(), // Handle the case where there is no event
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 24,),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: CancelButton(btnName: "close".tr, onPress: (){
                        homeController.updateWidgetHome(const GetMedalScreen());
                      }),
                    ),
                  )
                ],
              )
          );
        },
      ),
    );
  }

  Widget _buildTableHeader({required bool? resMobile , required bool? tablet}) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: double.infinity,
        padding: resMobile!
            ?const EdgeInsets.symmetric(vertical: 10, horizontal: 0)
            :const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: secondaryColor,
        ),
        child:  Row(
          children: [
            if(resMobile)...[
              Expanded(
                flex: 2,
                  child: Center(
                      child: Text('event_name'.tr)
                  )
              ),
              Expanded(child: Center(child: Text('qty_medal'.tr))),
              Expanded(
                flex: 2,
                  child: Center(
                      child: Text('win_date'.tr)
                  )
              ),
            ]else...[
              Expanded(child: Center(child: Text('event_name'.tr))),
              Expanded(child: Center(child: Text('qty_medal'.tr))),
              Expanded(child: Center(child: Text('win_date'.tr))),
              Expanded(child: Center(child: Text('event_start_date'.tr))),
              Expanded(child: Center(child: Text('event_end_date'.tr))),
            ]
          ],
        ),
      ),
    );
  }

  Widget _buildPlayerRow(Event event, MedalDetailModel medalDetailModel,{required bool? resMobile , required bool? tablet}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        if(resMobile!)...[
          Expanded(
            flex: 2,
              child: Center(
                  child: Text(event.title ?? 'unknown_event'.tr)
              )
          ),
          Expanded(child: Center(child: Text(medalDetailModel.qtyMedal ?? 'N/A'))),
          Expanded(
            flex: 2,
              child: Center(
                  child: Text(shortFormatDate(medalDetailModel.dateWin ?? 'N/A')
                  )
              )
          ),
        ]else if(tablet!)...[
          Expanded(child: Center(child: Text(event.title ?? 'unknown_event'.tr))),
          Expanded(child: Center(child: Text(medalDetailModel.qtyMedal ?? 'N/A'))),
          Expanded(child: Center(child: Text(shortFormatDate(medalDetailModel.dateWin ?? 'N/A')))),
          Expanded(child: Center(child: Text(shortFormatDate(event.startDate ?? 'N/A')))),
          Expanded(child: Center(child: Text(shortFormatDate(event.endDate ?? 'N/A')))),
        ]
        else...[
            Expanded(child: Center(child: Text(event.title ?? 'unknown_event'.tr))),
            Expanded(child: Center(child: Text(medalDetailModel.qtyMedal ?? 'N/A'))),
            Expanded(child: Center(child: Text(formatDate(medalDetailModel.dateWin ?? 'N/A')))),
            Expanded(child: Center(child: Text(formatDate(event.startDate ?? 'N/A')))),
            Expanded(child: Center(child: Text(formatDate(event.endDate ?? 'N/A')))),
        ]
      ],
    );
  }

  Widget _buildCategoryAndPlayerSelection(Players? player) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Column(

          children: [
            Text("player_info".tr,style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 20),),
            const SizedBox(height: 24,),
            Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('player_name'.tr),
                      const SizedBox(height: 10,),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Center(child: Text(player?.playerName ?? 'Unknown Player')),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('khmer_name'.tr),
                      const SizedBox(height: 10,),
                      Container(
                        height: 50,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Center(child: Text(player?.playerNameKh ?? 'Unknown Player Khmer Name')),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
  String formatMedalType(String type) {
    if (type.isEmpty) return 'unknown_medal_type'.tr;

    List<String> words = type.split('_');
    words = words.map((word) => word[0].toUpperCase() + word.substring(1)).toList();

    return words.join(' ');
  }

}
