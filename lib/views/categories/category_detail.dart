import 'package:esport_system/data/model/category_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/categories/list_categories_player.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CategoryDetail extends StatefulWidget {
  final CategoryModel data;
  const CategoryDetail({super.key, required this.data});

  @override
  State<CategoryDetail> createState() => _CategoryDetailState();
}

class _CategoryDetailState extends State<CategoryDetail> {
  var homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Responsive(
        tablet: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(16.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: secondaryColor,
            ),
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget> [
                Text('category_detail'.tr,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 24),
                buildText(
                    title: "title".tr,
                    textData: "${widget.data.title}",
                    alignment: Alignment.centerLeft,
                    padding: 10,
                    height: 50),
                const SizedBox(height: 10,),
                const SizedBox(height: 16,),
                buildText(
                    title: "description".tr,
                    textData: "${widget.data.otherTitle}",
                    alignment: Alignment.centerLeft,
                    padding: 10,
                    height: 50),
                const SizedBox(height: 24,),
                const Spacer(),
                Align(
                  alignment: Alignment.bottomRight,
                  child: CancelButton(
                    btnName: 'close'.tr,
                    onPress: () {
                      homeController.updateWidgetHome(const ListCategoriesPlayer());
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        mobile: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(16.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: secondaryColor,
            ),
            padding: const EdgeInsets.all(16),
            child: Column (
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('category_detail'.tr,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 24),
                buildText(
                    title: "title".tr,
                    textData: "${widget.data.title}",
                    alignment: Alignment.centerLeft,
                    padding: 10,
                    height: 50),
                const SizedBox(height: 10,),
                const SizedBox(height: 16,),
                buildText(
                    title: "description".tr,
                    textData: "${widget.data.otherTitle}",
                    alignment: Alignment.centerLeft,
                    padding: 10,
                    height: 50),
                const SizedBox(height: 24,),
                const Spacer(),
                Align(
                  alignment: Alignment.bottomRight,
                  child: CancelButton(
                    btnName: 'close'.tr,
                    onPress: () {
                      homeController.updateWidgetHome(const ListCategoriesPlayer());
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        desktop: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 150,
                  width: 150,
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  child: Image.asset(
                    'assets/icons/group.png',
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: secondaryColor,
                    ),
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('category_detail'.tr,
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(height: 24),
                        buildText(
                            title: "title".tr,
                            textData: "${widget.data.title}",
                            alignment: Alignment.centerLeft,
                            padding: 10,
                            height: 50),
                        const SizedBox(height: 10,),
                        const SizedBox(height: 16,),
                        buildText(
                            title: "description".tr,
                            textData: "${widget.data.otherTitle}",
                            alignment: Alignment.centerLeft,
                            padding: 10,
                            height: 50),
                        const SizedBox(height: 24,),
                        const SizedBox(height: 24),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: CancelButton(
                            btnName: 'close'.tr,
                            onPress: () {
                              homeController.updateWidgetHome(const ListCategoriesPlayer());
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      )
    );
  }
}
