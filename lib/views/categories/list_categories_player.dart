
import 'package:esport_system/data/controllers/category_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/views/categories/category_detail.dart';
import 'package:esport_system/views/categories/create_categories_player.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';
class ListCategoriesPlayer extends StatefulWidget {
  const ListCategoriesPlayer({super.key});

  @override
  State<ListCategoriesPlayer> createState() => _ListCategoriesPlayerState();
}

class _ListCategoriesPlayerState extends State<ListCategoriesPlayer> {

  var categoryController = Get.find<CategoryController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    categoryController.getCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<CategoryController>(
        builder: (logic) {
          return Container(
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            child: Column(
              children: [
                _buildDropdown(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 32,
                            height: 32,
                            child: Image.asset(
                              "assets/icons/group.png",
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(width: 8), // Add spacing between the icon and text
                          Text(
                            'category'.tr,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: AddNewButton(
                        btnName: "add_new".tr,
                        onPress: () {
                          print("------------>Do request ");
                          homeController.updateWidgetHome(const CreateCategoriesPlayer(data: null,));
                        },
                      ),
                    ),
                  ],
                ),

                const SizedBox(height: 22,),

                Expanded(
                  child: logic.isLoading
                      ? const Center(child: CircularProgressIndicator())
                      : LayoutBuilder(builder: (context, constraints) {
                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minWidth: constraints.maxWidth,
                              ),
                              child: DataTable(
                                dataRowMaxHeight: Get.height * 0.1,
                                headingRowColor: WidgetStateColor.resolveWith((states) => secondaryColor),
                                columns: [
                                  DataColumn(
                                    label: Text(
                                      "no.".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "title".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "other_title".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "created_by".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "created_date".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "actions".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                                rows: logic.lisCategory.isEmpty
                                  ?[]
                                  :List<DataRow>.generate(
                                  logic.lisCategory.length,
                                      (index) {
                                    var list = logic.lisCategory[index];
                                    return DataRow(
                                      cells: [
                                        DataCell(
                                          Text(_truncateText('${index + 1}', 10)),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText(list.title ?? "N/A", 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText(list.otherTitle ?? "N/A", 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            list.createdBy?.name ?? "N/A",
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText(formatDate(list.createdAt.toString()),20),
                                          ),
                                        ),

                                        DataCell(
                                          Row(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  homeController.updateWidgetHome(CategoryDetail(data: list,));
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.amberAccent),
                                                      color: Colors.amberAccent.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.visibility_sharp,
                                                      size: 20,
                                                      color: Colors.amberAccent,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  //requestItemController.setPreviousValue(list);
                                                  homeController.updateWidgetHome(CreateCategoriesPlayer(data: list));
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.green),
                                                      color: Colors.green.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.edit,
                                                      size: 20,
                                                      color: Colors.green,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  showDialog(
                                                    context: context,
                                                    builder: (BuildContext
                                                    context) {
                                                      return AlertDialog(
                                                        content:Container(
                                                          width:Get.width * 0.3,
                                                          height:Get.height * 0.3,
                                                          padding:const EdgeInsets.all(16),
                                                          child: Column(
                                                            children: [
                                                              Align(alignment:Alignment.topCenter,
                                                                child: Image.asset(
                                                                  "assets/images/warning.png",
                                                                  width:50,
                                                                  height:50,
                                                                ),
                                                              ),
                                                              const SizedBox(height:15),
                                                              Text(
                                                                  "are_you_sure_to_delete_this_category?".tr,
                                                                  style: Theme.of(context).textTheme.bodyMedium
                                                              ),
                                                              const SizedBox(height:15),
                                                              const Spacer(),
                                                              Row(
                                                                crossAxisAlignment:
                                                                CrossAxisAlignment.end,
                                                                mainAxisAlignment:
                                                                MainAxisAlignment.end,
                                                                children: [

                                                                  CancelButton(
                                                                      btnName: 'cancel'.tr,
                                                                      onPress: (){
                                                                        Get.back();
                                                                      }),
                                                                  const SizedBox(
                                                                      width: 10),
                                                                  OKButton(
                                                                      btnName: 'confirm',
                                                                      onPress: () async {
                                                                        logic.deletedCategory(pcId: list.id.toString());
                                                                        Get.back();
                                                                      })
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  );
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.all(6.0),
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(width: 1,color: Colors.red),
                                                      color: Colors.red.withOpacity(0.1),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.delete,
                                                      size: 20,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                          if (logic.lisCategory.isEmpty)...[
                            const SizedBox(height: 50,),
                            const Center(child: EmptyData()),
                          ]
                        ],
                      ),
                    );
                  },
                  )
                ),

                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 700,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            onPressed: logic.previousPage,
                            icon: const Icon(Icons.arrow_back_ios),
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                for (int i = 1; i <= logic.totalPages; i++)
                                  Padding(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        TextButton(
                                          onPressed: () => logic.setPage(i),
                                          child: Text(
                                            i.toString(),
                                            style: TextStyle(
                                              color: logic.currentPage == i
                                                  ? Colors.blue
                                                  : Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: categoryController.nextPage,
                            icon: const Icon(Icons.arrow_forward_ios),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),

    );
  }

  Widget _buildDropdown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: categoryController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  categoryController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              categoryController.search(value);
            }, hintText: 'search_category_name'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }

}
