

import 'package:esport_system/data/controllers/category_controller.dart';
import 'package:esport_system/data/model/category_model.dart';
import 'package:esport_system/views/categories/list_categories_player.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';
import '../../helper/add_new_button.dart';
import '../../helper/constants.dart';
import '../../helper/custom_textformfield.dart';

class CreateCategoriesPlayer extends StatefulWidget {
  final CategoryModel? data ;
  const CreateCategoriesPlayer({super.key,required this.data});

  @override
  State<CreateCategoriesPlayer> createState() => _CreateCategoriesPlayerState();
}

class _CreateCategoriesPlayerState extends State<CreateCategoriesPlayer> {

  var categoryController = Get.find<CategoryController>();
  var homeController = Get.find<HomeController>();
  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<CategoryController>(builder: (logic) {
        if (widget.data != null) {
          logic.titleController.text = widget.data?.title.toString()??"N/A";
          logic.descriptionController.text = widget.data?.otherTitle.toString()??"N/A";
          logic.fromAgeController.text = widget.data?.fromAge.toString()??"N/A";
          logic.toAgeController.text = widget.data?.endAge.toString()??"N/A";
        } else {
          logic.titleController.clear();
          logic.descriptionController.clear();
          logic.fromAgeController.clear();
          logic.toAgeController.clear();
        }
        return Container(
          width: Get.width,

          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FormBuilder(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.data == null
                              ? 'create_a_new_category'.tr
                              : 'update_category'.tr,
                          style: const TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 20.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'title'.tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            CustomFormBuilderTextField(
                              name: 'title'.tr,
                              controller: logic.titleController,
                              hintText: 'title'.tr,
                              errorText: 'title_is_required'.tr,
                              icon: Icons.text_fields,
                              fillColor: secondaryColor,
                            ),
                          ],
                        ),
                        const SizedBox(height: 16.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'description'.tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            CustomFormBuilderTextField(
                              name: 'description'.tr,
                              controller: logic.descriptionController,
                              hintText: 'description'.tr,
                              errorText: 'description_is_required'.tr,
                              icon: Icons.description,
                              fillColor: secondaryColor,
                            ),
                          ],
                        ),
                        const SizedBox(height: 16.0),
                        Row(
                          children: [
                            Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'from_age'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    CustomFormBuilderTextField(
                                      name: 'input_age'.tr,
                                      controller: logic.fromAgeController,
                                      hintText: 'please_input_age'.tr,
                                      errorText: 'age_is_required'.tr,
                                      icon: Icons.support_agent_sharp,
                                      fillColor: secondaryColor,
                                    ),
                                  ],
                                ),
                            ),
                            const SizedBox(width: 10,),
                            const Column(
                              children: [
                                SizedBox(height: 18,),
                                Icon(Icons.arrow_right_alt)
                              ],
                            ),
                            const SizedBox(width: 10,),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'to_age'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  CustomFormBuilderTextField(
                                    name: 'input_age'.tr,
                                    controller: logic.toAgeController,
                                    hintText: 'please_input_age'.tr,
                                    errorText: 'age_is_required'.tr,
                                    icon: Icons.support_agent_sharp,
                                    fillColor: secondaryColor,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            CancelButton(
                                btnName: 'cancel'.tr,
                                onPress: (){
                                  homeController.updateWidgetHome(const ListCategoriesPlayer());
                                }),
                            const SizedBox(width: 16,),
                            OKButton(
                                btnName: widget.data == null ? 'create'.tr : 'update'.tr,
                                onPress: () async {
                                  if (_formKey.currentState != null &&
                                      _formKey.currentState!
                                          .saveAndValidate()) {
                                    if(widget.data==null){
                                      logic.createCategory();
                                    }else{
                                      logic.updateCategory(pcId: widget.data!.id.toString());
                                    }
                                  }
                                }
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
