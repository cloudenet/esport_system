
import 'package:esport_system/data/controllers/coach_controller.dart';
import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/controllers/score_keeper_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/scorekepper_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/dropdown_search_user_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/score_keeper_section/get_list_scorekeeper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;

import '../../data/controllers/home_controller.dart';

class CreateScoreKeeper extends StatefulWidget {
  final ScorekeeperModel? data;

  const CreateScoreKeeper({super.key, this.data});

  @override
  State<CreateScoreKeeper> createState() => _CreateScoreKeeperState();
}

class _CreateScoreKeeperState extends State<CreateScoreKeeper> {
  final _formKey = GlobalKey<FormBuilderState>();
  var homeController = Get.find<HomeController>();
  var userController = Get.find<UserController>();
  var rankController = Get.find<RankController>();
  var coachController = Get.find<CoachController>();
  var scoreKeeperController = Get.find<ScoreKeeperController>();

  @override
  void initState() {
    super.initState();
    scoreKeeperController.certificateFiles=[];
    if(widget.data != null){
      scoreKeeperController.setPreviousValue(widget.data);
    }else{
      scoreKeeperController.cleaData();
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<ScoreKeeperController>(builder: (logic) {
        return GetBuilder<UserController>(builder: (_) {
          return Container(
            width: double.infinity,
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      FormBuilder(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.data != null
                                  ? "update_scoreKeeper".tr
                                  : 'create_ScoreKeeper'.tr,
                              style: const TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            const SizedBox(height: 20.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.data != null
                                            ? "scorekeeper_name".tr
                                            : 'select_user'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      const SizedBox(height: 5.0),
                                      if (widget.data != null) ...[
                                        UserDropdownSearch(
                                          checkUpdate: true,
                                          allUserList: userController.listAllUser,
                                          selectedItem: logic.previousSelectedUser,
                                          onChanged: (value) {
                                            setState(() {
                                              logic.selectedUserId = value!.id.toString();
                                            });
                                          },
                                        )
                                      ] else ...[
                                        UserDropdownSearch(
                                          checkUpdate: false,
                                          selectedItem: logic.previousSelectedUser,
                                          onChanged: (value) {
                                            setState(() {
                                              logic.selectedUserId = value!.id.toString();
                                            });
                                          },
                                        )
                                      ]
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'select_rank'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      const SizedBox(height: 5.0),

                                      DropdownButtonFormField<String>(
                                        borderRadius: BorderRadius.circular(10),
                                        value: logic.selectRankId,
                                        items: rankController.rankCoach.map((user) {
                                          return DropdownMenuItem<String>(
                                            value: user.id,
                                            child: Text(user.titleRank!),
                                          );
                                        }).toList(),
                                        onChanged: (value) {
                                          setState(() {
                                            logic.selectRankId = value;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: "please_select_rank".tr,
                                          floatingLabelAlignment: FloatingLabelAlignment.center,
                                          floatingLabelBehavior: FloatingLabelBehavior.never,
                                          fillColor: secondaryColor,
                                          filled: true,
                                          border: const OutlineInputBorder(
                                            borderSide: BorderSide.none,
                                            borderRadius: BorderRadius.all(Radius.circular(10),),
                                          ),
                                        ),
                                        validator: FormBuilderValidators.required(),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 20.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'experience_year'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                        name: 'experience_year'.tr,
                                        controller:
                                            logic.experienceYearController,
                                        hintText: 'experience_year'.tr,
                                        errorText:
                                            'experience_year_is_required'.tr,
                                        icon: Icons.description,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16,),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('training_date'.tr, style: const TextStyle(fontSize: 16),),
                                      CustomFormBuilderTextField(
                                        name: 'training_date'.tr,
                                        controller: logic.trainingDateController,
                                        hintText: 'training_date'.tr,
                                        errorText: 'training_date_is_required'.tr,
                                        icon: Icons.description,
                                        fillColor: secondaryColor,
                                        onTap: () {
                                          userController.selectDate(context, logic.trainingDateController);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 20,),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('biography'.tr, style: const TextStyle(fontSize: 16),),
                                      const SizedBox(height: 5.0),
                                      CustomFormBuilderTextField(
                                        name: 'upload_biography'.tr,
                                        controller: logic.bioFileController,
                                        hintText: 'upload_biography'.tr,
                                        errorText: 'upload_biography_is_required'.tr,
                                        icon: Icons.attachment_sharp,
                                        onTap: () async {
                                          pickFileGlobal().then((vFile) {
                                            if (vFile != null) {
                                              setState(() {
                                                logic.bioFileController.text = vFile.files.first.name.toString();
                                                logic.fileBio = XFile(vFile.files.single.name);
                                              });
                                            }
                                          });
                                        },
                                        readOnly: true,
                                        fillColor: secondaryColor,
                                      ),
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'certificate'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      const SizedBox(height: 5.0),
                                      logic.certificateFiles!.isEmpty
                                          ? CustomFormBuilderTextField(
                                        name: 'upload_certificate'.tr,
                                        hintText: 'upload_certificate'.tr,
                                        errorText: 'upload_certificate_is_required'.tr,
                                        icon: Icons.attachment_outlined,
                                        fillColor: secondaryColor,
                                        controller: TextEditingController(text: ""),
                                        onTap:(){
                                          logic.pickCertificateFiles();
                                        } ,
                                      ) : GestureDetector(
                                        onTap: () {
                                          logic.pickCertificateFiles();
                                        },
                                        child: Container(
                                          height: 47,
                                          width: double.infinity,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10),
                                            color: secondaryColor,
                                          ),
                                          child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: logic.certificateFiles!.length,
                                            itemBuilder: (context, index) {
                                              final file = logic.certificateFiles![index];
                                              final Map<String, Map<String, dynamic>> fileIconMap = {
                                                'pdf': {
                                                  'icon': Icons.picture_as_pdf,
                                                  'color': Colors.red,
                                                },
                                                'xlsx': {
                                                  'icon': Icons.description_outlined,
                                                  'color': Colors.green,
                                                },
                                                'doc': {
                                                  'icon': Icons.description,
                                                  'color': Colors.blue,
                                                },
                                                'docx': {
                                                  'icon': Icons.description,
                                                  'color': Colors.blue,
                                                },
                                                'jpg': {
                                                  'icon': Icons.image,
                                                  'color': Colors.green,
                                                },
                                                'jpeg': {
                                                  'icon': Icons.image,
                                                  'color': Colors.green,
                                                },
                                                'png': {
                                                  'icon': Icons.image,
                                                  'color': Colors.green,
                                                },
                                                'mp4': {
                                                  'icon': Icons.videocam,
                                                  'color': Colors.orange,
                                                },
                                                'mp3': {
                                                  'icon': Icons.audiotrack,
                                                  'color': Colors.purple,
                                                },
                                              };
                                              String fileExtension = p.extension(file.name).toLowerCase().replaceAll('.', ''); // Get extension without '.'
                                              var fileIconData = fileIconMap[fileExtension] ?? {
                                                'icon': Icons.insert_drive_file,
                                                'color': Colors.grey,
                                              };
                                              return Container(
                                                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                                child: Chip(
                                                  label: Text(file.name),
                                                  avatar: Icon(fileIconData['icon'], color: fileIconData['color'],),
                                                  deleteIcon: const Icon(Icons.delete, color: Colors.red),
                                                  onDeleted: () {
                                                    logic.removeCertificateFile(file.name,index); // Call method to remove file
                                                  },
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                CancelButton(
                                    btnName: 'cancel'.tr, onPress: () {
                                      homeController.updateWidgetHome(const GetListScorekeeper());
                                }),
                                const SizedBox(
                                  width: 16,
                                ),
                                OKButton(
                                    btnName: widget.data != null
                                        ? 'update'.tr
                                        : 'create'.tr,
                                    onPress: () async {
                                      if (_formKey.currentState != null &&
                                          _formKey.currentState!.saveAndValidate()) {
                                        if (widget.data != null &&
                                        logic.selectedUserId != null &&
                                        logic.selectRankId != null &&
                                        logic.experienceYearController.text.isNotEmpty &&
                                        logic.trainingDateController.text.isNotEmpty &&
                                        logic.fileBio != null &&
                                        logic.certificateFiles != null) {
                                          logic.updateScorekeeper(scorekeeperId: widget.data!.id.toString());
                                        }
                                        else {
                                          if(logic.selectedUserId != null &&
                                              logic.selectRankId != null &&
                                              logic.experienceYearController.text.isNotEmpty &&
                                              logic.trainingDateController.text.isNotEmpty &&
                                              logic.bioFileController.text.isNotEmpty &&
                                              logic.certificateFiles != null){
                                          }
                                          logic.createScoreKeeper();
                                        }
                                      }
                                    }),
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        });
      }),
    );
  }
}
