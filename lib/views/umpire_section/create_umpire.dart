import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/controllers/umpire_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/umpire_section/get_list_umpire.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as p;
import '../../data/controllers/home_controller.dart';
import '../../data/model/referees_model.dart';
import '../../helper/add_new_button.dart';
import '../../helper/custom_textformfield.dart';
import '../../helper/dropdown_search_user_widget.dart';
import '../../helper/format_date_widget.dart';

class CreateUmpire extends StatefulWidget {
  final UmpiresModel? dataUmpire;

  const CreateUmpire({super.key, this.dataUmpire});

  @override
  State<CreateUmpire> createState() => _CreateUmpireState();
}

class _CreateUmpireState extends State<CreateUmpire> {

  var userController = Get.find<UserController>();
  var umpireController = Get.find<RefereesController>();
  var rankController = Get.find<RankController>();
  var homeController = Get.find<HomeController>();

  final _formKey = GlobalKey<FormBuilderState>();


  @override
  void initState() {
    if (widget.dataUmpire == null) {
      umpireController.certificateFiles = [];
      //umpireController.certificateFileNames=[];
      umpireController.fileBioRefereeController.text = "";
      umpireController.selectedRefereesRank = null;
      umpireController.trainingDateController.text = "";
      umpireController.yearOfExperienceController.text = "";
      umpireController.selectedUser = null;
    }
    umpireController.getRankReferees();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(30),
        child: FormBuilder(
          child: GetBuilder<RefereesController>(builder: (logic) {
            return FormBuilder(
              key: _formKey,
              child: Responsive(
                  mobile: body(isMobile: true),
                  tablet: body(),
                  desktop: body()
              ),
            );
          }),
        ),
      ),
    );
  }

  Widget body({bool? isMobile}) {
    return GetBuilder<RefereesController>(builder: (logic) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'create_a_new_umpire'.tr,
            style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 20,),
          Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('select_user'.tr,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 5,),
                      if(widget.dataUmpire != null)...[
                        UserDropdownSearch(
                          checkUpdate: true,
                          allUserList: userController.listAllUser,
                          selectedItem: logic.selectedUser,
                          onChanged: (value) {
                            setState(() {
                              logic.selectedUserId = value!.id.toString();
                            });
                          },
                        )
                      ] else
                        ...[
                          UserDropdownSearch(
                            checkUpdate: false,
                            selectedItem: logic.selectedUser,
                            onChanged: (value) {
                              setState(() {
                                logic.selectedUserId = value!.id.toString();
                              });
                            },
                          )
                        ]
                    ],
                  )
              ),
              const SizedBox(
                width: 16,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text('training_date'.tr, style: const TextStyle(fontSize: 16)),
                    const SizedBox(height: 5,),
                    CustomFormBuilderTextField(
                      onTap: () {
                        selectDate(context, logic.trainingDateController,);
                      },
                      errorText: _formKey.currentState?.fields['training_date']
                          ?.errorText,
                      name: 'training_date',
                      controller: logic.trainingDateController,
                      hintText: 'enter_training_date'.tr,
                      icon: Icons.calendar_month,
                      fillColor: secondaryColor,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 15,),
          if(isMobile != null && isMobile)...[
            Text('biography'.tr,style: const TextStyle(fontSize: 16),),
            const SizedBox(height: 5.0),
            CustomFormBuilderTextField(
              name: 'upload_biography'.tr,
              controller: logic.fileBioRefereeController,
              hintText: 'upload_biography'.tr,
              errorText: 'upload_biography_is_required'.tr,
              icon: Icons.attachment_sharp,
              onTap: () async {
                pickFileGlobal().then((vFile) {
                  if (vFile != null) {
                    setState(() {
                      logic.fileBioRefereeController.text =
                          vFile.files.first.name.toString();
                      logic.fileBio =
                          XFile(vFile.files.single.name);
                    });
                  }
                });
              },
              readOnly: true,
              fillColor: secondaryColor,
            ),
            const SizedBox(height: 15,),
            Text(
              'certificate'.tr,
              style: const TextStyle(fontSize: 16),
            ),
            const SizedBox(height: 5.0),
            logic.certificateFiles!.isEmpty
                ? CustomFormBuilderTextField(
              name: 'upload_certificate'.tr,
              hintText: 'upload_certificate'.tr,
              errorText: 'upload_certificate_is_required'.tr,
              icon: Icons.attachment_outlined,
              fillColor: secondaryColor,
              controller: TextEditingController(text: ""),
              onTap: () {
                logic.pickCertificateFiles();
              },
            )
                : GestureDetector(
              onTap: () {
                logic.pickCertificateFiles();
              },
              child: Container(
                height: 47,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: logic.certificateFiles!.length,
                  itemBuilder: (context, index) {
                    final file = logic.certificateFiles![index];
                    final Map<String,
                        Map<String, dynamic>> fileIconMap = {
                      'pdf': {
                        'icon': Icons.picture_as_pdf,
                        'color': Colors.red,
                      },
                      'xlsx': {
                        'icon': Icons.description_outlined,
                        'color': Colors.green,
                      },
                      'doc': {
                        'icon': Icons.description,
                        'color': Colors.blue,
                      },
                      'docx': {
                        'icon': Icons.description,
                        'color': Colors.blue,
                      },
                      'jpg': {
                        'icon': Icons.image,
                        'color': Colors.green,
                      },
                      'jpeg': {
                        'icon': Icons.image,
                        'color': Colors.green,
                      },
                      'png': {
                        'icon': Icons.image,
                        'color': Colors.green,
                      },
                      'mp4': {
                        'icon': Icons.videocam,
                        'color': Colors.orange,
                      },
                      'mp3': {
                        'icon': Icons.audiotrack,
                        'color': Colors.purple,
                      },
                    };
                    String fileExtension = p
                        .extension(file.name)
                        .toLowerCase()
                        .replaceAll(
                        '.', ''); // Get extension without '.'
                    var fileIconData = fileIconMap[fileExtension] ??
                        {
                          'icon': Icons.insert_drive_file,
                          'color': Colors.grey,
                        };
                    return Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4.0),
                      child: Chip(
                        label: Text(file.name),
                        avatar: Icon(fileIconData['icon'],
                          color: fileIconData['color'],),
                        deleteIcon: const Icon(
                            Icons.delete, color: Colors.red),
                        onDeleted: () {
                          logic.removeCertificateFile(file.name,
                              index); // Call method to remove file
                        },
                      ),
                    );
                  },
                ),
              ),
            ),
          ]
          else...[
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      Text('biography'.tr, style: const TextStyle(fontSize: 16),),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'upload_biography'.tr,
                        controller: logic.fileBioRefereeController,
                        hintText: 'upload_biography'.tr,
                        errorText: 'upload_biography_is_required'.tr,
                        icon: Icons.attachment_sharp,
                        onTap: () async {
                          pickFileGlobal().then((vFile) {
                            if (vFile != null) {
                              setState(() {
                                logic.fileBioRefereeController.text =
                                    vFile.files.first.name.toString();
                                logic.fileBio = XFile(vFile.files.single.name);
                              });
                            }
                          });
                        },
                        readOnly: true,
                        fillColor: secondaryColor,
                      ),

                    ],
                  ),
                ),
                const SizedBox(width: 16,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'certificate'.tr,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      logic.certificateFiles!.isEmpty
                          ? CustomFormBuilderTextField(
                        name: 'upload_certificate'.tr,
                        hintText: 'upload_certificate'.tr,
                        errorText: 'upload_certificate_is_required'.tr,
                        icon: Icons.attachment_outlined,
                        fillColor: secondaryColor,
                        controller: TextEditingController(text: ""),
                        onTap: () {
                          logic.pickCertificateFiles();
                        },
                      )
                          : GestureDetector(
                        onTap: () {
                          logic.pickCertificateFiles();
                        },
                        child: Container(
                          height: 47,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: secondaryColor,
                          ),
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: logic.certificateFiles!.length,
                            itemBuilder: (context, index) {
                              final file = logic.certificateFiles![index];
                              final Map<String,
                                  Map<String, dynamic>> fileIconMap = {
                                'pdf': {
                                  'icon': Icons.picture_as_pdf,
                                  'color': Colors.red,
                                },
                                'xlsx': {
                                  'icon': Icons.description_outlined,
                                  'color': Colors.green,
                                },
                                'doc': {
                                  'icon': Icons.description,
                                  'color': Colors.blue,
                                },
                                'docx': {
                                  'icon': Icons.description,
                                  'color': Colors.blue,
                                },
                                'jpg': {
                                  'icon': Icons.image,
                                  'color': Colors.green,
                                },
                                'jpeg': {
                                  'icon': Icons.image,
                                  'color': Colors.green,
                                },
                                'png': {
                                  'icon': Icons.image,
                                  'color': Colors.green,
                                },
                                'mp4': {
                                  'icon': Icons.videocam,
                                  'color': Colors.orange,
                                },
                                'mp3': {
                                  'icon': Icons.audiotrack,
                                  'color': Colors.purple,
                                },
                              };
                              String fileExtension = p
                                  .extension(file.name)
                                  .toLowerCase()
                                  .replaceAll(
                                  '.', ''); // Get extension without '.'
                              var fileIconData = fileIconMap[fileExtension] ?? {
                                'icon': Icons.insert_drive_file,
                                'color': Colors.grey,
                              };
                              return Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 4.0),
                                child: Chip(
                                  label: Text(file.name),
                                  avatar: Icon(fileIconData['icon'],
                                    color: fileIconData['color'],),
                                  deleteIcon: const Icon(
                                      Icons.delete, color: Colors.red),
                                  onDeleted: () {
                                    logic.removeCertificateFile(file.name,
                                        index); // Call method to remove file
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
          const SizedBox(height: 15,),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text('experience_year'.tr,
                      style: const TextStyle(fontSize: 16),),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      name: 'experience'.tr,
                      controller: logic.yearOfExperienceController,
                      hintText: 'experience'.tr,
                      errorText: 'experience_is_required'.tr,
                      icon: Icons.note_add_rounded,
                      onTap: () async {},
                      fillColor: secondaryColor,
                    ),

                  ],
                ),
              ),
              const SizedBox(width: 16,),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'select_rank'.tr, style: const TextStyle(fontSize: 16),),
                    const SizedBox(height: 5.0),
                    DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(10),
                      value: logic.selectedRefereesRank,
                      items: logic.rankReferees.map((user) {
                        return DropdownMenuItem<String>(
                          value: user.id,
                          child: Text(user.titleRank!),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          logic.selectedRefereesRank = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "please_select_rank".tr,
                        floatingLabelAlignment: FloatingLabelAlignment.center,
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        fillColor: secondaryColor,
                        filled: true,
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(10),),
                        ),
                      ),
                      validator: FormBuilderValidators.required(),
                    ),
                  ],
                ),
              ),

            ],
          ),
          const SizedBox(height: 24.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CancelButton(
                  btnName: 'cancel'.tr,
                  onPress: () {
                    homeController.updateWidgetHome(const GetListUmpires());
                  }),
              const SizedBox(width: 16,),
              OKButton(
                  btnName: widget.dataUmpire != null ? 'update'.tr : "create"
                      .tr,
                  onPress: () async {
                    // print(" logic.selectedUserId ${logic.selectedUserId!}");
                    // print(" logic.trainingDateController ${logic.trainingDateController.text}");
                    // print(" logic.yearOfExperienceController ${logic.yearOfExperienceController.text}");
                    // print(" logic.fileBio ${logic.fileBio != null}");
                    // print(" logic.fileCertificate ${logic.certificateFiles != null}");
                    // print(" logic.selectedRefereesRank ${logic.selectedRefereesRank!.isNotEmpty}");

                    if (widget.dataUmpire != null) {
                      logic.updateUmpire(
                          umpireId: widget.dataUmpire!.id.toString());
                      // if(_formKey.currentState != null &&
                      //     _formKey.currentState!.saveAndValidate()) {
                      //   if( widget.dataUmpire != null &&
                      //       logic.trainingDateController.text.isNotEmpty &&
                      //       logic.fileBio != null &&
                      //       logic.certificateFiles != null &&
                      //       logic.yearOfExperienceController.text.isNotEmpty &&
                      //       logic.selectedRefereesRank!.isNotEmpty){
                      //     logic.updateUmpire(umpireId: widget.dataUmpire!.id.toString());
                      //   }
                      // }
                    } else {
                      if (_formKey.currentState != null &&
                          _formKey.currentState!.saveAndValidate()) {
                        if (logic.selectedUserId!.isNotEmpty &&
                            logic.trainingDateController.text.isNotEmpty &&
                            logic.fileBio != null &&
                            logic.certificateFiles != null &&
                            logic.yearOfExperienceController.text.isNotEmpty &&
                            logic.selectedRefereesRank!.isNotEmpty) {
                          logic.createUmpire();
                        }
                      }
                    }
                  }
              ),
            ],
          ),
        ],
      );
    });
  }
}
