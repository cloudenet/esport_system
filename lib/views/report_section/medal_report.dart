import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/util/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../../data/model/medal_report_model.dart';

class MedalReport extends StatefulWidget {
  const MedalReport({super.key});

  @override
  State<MedalReport> createState() => _MedalReportState();
}

class _MedalReportState extends State<MedalReport> {

  String? selectedEventTitle;
  var eventController = Get.find<EventController>();
  var userController = Get.find<UserController>();
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  List<FetchMedalReportModel> medalDataList = [
    FetchMedalReportModel(
      serialNumber: 1,
      event: "World Championships",
      goldMedals: 6,
      silverMedals: 3,
      bronzeMedals: 14,
      totalMedals: 23,
    ),
    FetchMedalReportModel(
      serialNumber: 2,
      event: "Single, Doubles, and Mixed double World Championships in Spain 2022",
      goldMedals: 0,
      silverMedals: 0,
      bronzeMedals: 1,
      totalMedals: 1,
    ),
    // Add more data as needed
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(vertical: 24, horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              decoration: const BoxDecoration(
                color: secondaryColor,
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: FormBuilder(
                child:  Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "select_event".tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              DropdownButtonFormField<String>(
                                value: selectedEventTitle,
                                items: eventController.eventData.map((event) {
                                  return DropdownMenuItem<String>(
                                    value: event.id,
                                    child: Text(event.title ?? 'unknown'.tr),
                                  );
                                }).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    selectedEventTitle = value ?? '';
                                  });
                                  print('=========eventID$selectedEventTitle');
                                },
                                decoration: InputDecoration(
                                  labelText: 'select_event'.tr,
                                  floatingLabelAlignment:
                                  FloatingLabelAlignment.center,
                                  floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                                  prefixIcon: const Icon(Icons.event),
                                  border: const OutlineInputBorder(
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                    borderSide: BorderSide.none,
                                  ),
                                  filled: true,
                                  fillColor: bgColor,
                                ),
                                validator: FormBuilderValidators.required(),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('from_date'.tr),
                              const SizedBox(height: 10,),
                              CustomFormBuilderTextField(
                                readOnly: true,
                                onTap: () => userController.selectDate(context,fromDateController ),
                                name: 'from_date'.tr,
                                controller: fromDateController,
                                hintText: "from_date".tr,
                                icon: Icons.calendar_month_outlined,
                                fillColor: bgColor,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('to_date'.tr),
                              const SizedBox(height: 10,),
                              CustomFormBuilderTextField(
                                readOnly: true,
                                onTap: () => userController.selectDate(context, toDateController),
                                name: 'to_date'.tr,
                                controller: toDateController,
                                hintText: "to_date".tr,
                                icon: Icons.calendar_month_outlined,
                                fillColor: bgColor,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 30
                            ),
                            child: OKButton(
                              btnName: 'search'.tr,
                              onPress: () async {
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 24,),
            Text(
              "តារាងបញ្ជីមេដាយសរុបគិតចាប់ពីឆ្នាំ១៩៩២​រហុតដល់ខែកញ្ញា ឆ្នាំ២០២៤",
              style: TextStylesHelper.khmerText16.copyWith(fontSize: 19, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 24,),
            // Make sure to use a `SingleChildScrollView` for vertical scrolling.
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  border: TableBorder.all(color: Colors.white),
                  headingTextStyle: TextStylesHelper.khmerText16.copyWith(fontWeight: FontWeight.bold),
                  columns: const [
                    DataColumn(label: Center(child: Text("ល.រ"))),
                    DataColumn(label: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 285),
                      child: Text("កម្មវិធីប្រកួត"),
                    )),
                    DataColumn(label: Center(child: Text("មេដាយមាស"))),
                    DataColumn(label: Center(child: Text("មេដាយប្រាក់"))),
                    DataColumn(label: Center(child: Text("មេដាយសំរិទ្ធ"))),
                    DataColumn(label: Center(child: Text("សរុប"))),
                  ],
                  rows: medalDataList.map((data) {
                    return DataRow(cells: [
                      DataCell(Center(child: Text(data.serialNumber.toString()))),
                      DataCell(Text(data.event)),
                      DataCell(Center(child: Text(data.goldMedals.toString()))),
                      DataCell(Center(child: Text(data.silverMedals.toString()))),
                      DataCell(Center(child: Text(data.bronzeMedals.toString()))),
                      DataCell(Center(child: Text(data.totalMedals.toString()))),
                    ]);
                  }).toList(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
