import 'package:esport_system/data/controllers/medal_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/men_report_model.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/dropdown_search_player_widget.dart';
import 'package:esport_system/util/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class MenReport extends StatefulWidget {
  const MenReport({super.key});

  @override
  State<MenReport> createState() => _MenReportState();
}

class _MenReportState extends State<MenReport> {
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  PlayerModel? selectedPlayer;
  String? selectPlayerId;
  final List<MenReportModel> dataRows = [
    MenReportModel(
      index: 1,
      name: "កីឡាករ សុខ ចាន់មាន",
      round1: 156.5,
      round2: 168,
      round3: 180.5,
      round4: 127,
      totalScore: 632,
      rank: 1,
    ),
    MenReportModel(
      index: 2,
      name: "កីឡាករ តិប នរ៉ា",
      round1: 129.5,
      round2: 155.5,
      round3: 187,
      round4: 133,
      totalScore: 605,
      rank: 2,
    ),
    MenReportModel(
      index: 3,
      name: "កីឡាករ យឹម សុភ័ណ្ឌ",
      round1: 150,
      round2: 149.5,
      round3: 144.5,
      round4: 148,
      totalScore: 592,
      rank: 3,
    ),
    // Add more data rows here
  ];

  final userController = Get.find<UserController>();
  final medalController = Get.find<MedalsPlayerController>();
  final playerController = Get.find<PlayerController>();

  @override
  void initState() {
    super.initState();
    playerDropDownSearch();
  }

  void playerDropDownSearch() async {
    await playerController.getPlayerDropdownSearch(searchCat: "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(vertical: 24, horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GetBuilder<PlayerController>(builder: (logic) {
              return Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                    vertical: 10, horizontal: 10),
                decoration: const BoxDecoration(
                  color: secondaryColor,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                ),
                child: FormBuilder(
                  child: Column(
                    children: [
                      _buildCategoryAndPlayerSelection(),
                      const SizedBox(height: 8,),
                      OKButton(
                        btnName: 'search'.tr,
                        onPress: () async {
                          // Implement search logic here
                        },
                      ),
                    ],
                  ),
                ),
              );
            }),
            const SizedBox(height: 24),
            Text(
              "លទ្ធផលធ្វើតេស្ដប្រចាំឆ្នាំ២០២៣",
              style: TextStylesHelper.khmerText16.copyWith(
                  fontSize: 19, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 6),
            Text(
              "ក្រុមបុរសឈុតធំ",
              style: TextStylesHelper.khmerText16.copyWith(
                  fontSize: 19, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 24),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: DataTable(
                  border: TableBorder.all(color: Colors.white),
                  headingTextStyle: TextStylesHelper.khmerText16.copyWith(
                      fontWeight: FontWeight.bold),
                  columns: const [
                    DataColumn(label: Center(child: Text("ល.រ"))),
                    DataColumn(label: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 100),
                      child: Text("នាម និងគោត្តនាម"),
                    )),
                    DataColumn(label: Center(child: Text("លើកទី១"))),
                    DataColumn(label: Center(child: Text("លើកទី២"))),
                    DataColumn(label: Center(child: Text("លើកទី៣"))),
                    DataColumn(label: Center(child: Text("លើកទី៤"))),
                    DataColumn(label: Center(child: Text("ពិន្ទុសរុប"))),
                    DataColumn(label: Center(child: Text("ចំណាត់ថ្នាក់"))),
                  ],
                  rows: dataRows.map((dataRow) {
                    return DataRow(cells: [
                      DataCell(Center(child: Text(dataRow.index.toString()))),
                      DataCell(Text(dataRow.name)),
                      DataCell(Center(child: Text(dataRow.round1.toString()))),
                      DataCell(Center(child: Text(dataRow.round2.toString()))),
                      DataCell(Center(child: Text(dataRow.round3.toString()))),
                      DataCell(Center(child: Text(dataRow.round4.toString()))),
                      DataCell(
                          Center(child: Text(dataRow.totalScore.toString()))),
                      DataCell(Center(child: Text(dataRow.rank.toString()))),
                    ]);
                  }).toList(),
                ),
              ),
            ),
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 110),
              child: Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  children: [
                    Text(
                        "ថ្ងៃព្រហស្បតិ៍ ៨រោច ខែមិគសិរ ឆ្នាំថោះ បញ្ចស័ក ព.ស.២៥៦៧",
                        style: TextStylesHelper.khmerText16),
                    const SizedBox(height: 8),
                    Text("រាជធានីភ្នំពេញ ថ្ងៃទី៤ ខែមករា ឆ្នាំ២០២៤",
                        style: TextStylesHelper.khmerText16),
                    const SizedBox(height: 8),
                    Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា",
                        style: TextStylesHelper.khmerText16.copyWith(
                            fontSize: 19, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildCategoryAndPlayerSelection() {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: _buildPlayerCategoryDropdown(),
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: _buildPlayerDropdown(),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('from_date'.tr),
                    const SizedBox(height: 10,),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () =>
                          userController.selectDate(
                              context, fromDateController),
                      name: 'from_date'.tr,
                      controller: fromDateController,
                      hintText: "from_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: bgColor,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('to_date'.tr),
                    const SizedBox(height: 10,),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () =>
                          userController.selectDate(context, toDateController),
                      name: 'to_date'.tr,
                      controller: toDateController,
                      hintText: "to_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: bgColor,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildPlayerCategoryDropdown() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('select_player_category'.tr, style: const TextStyle(fontSize: 16)),
        const SizedBox(height: 10.0),
        FormBuilderDropdown(
          name: 'player_category'.tr,
          borderRadius: BorderRadius.circular(10),
          decoration: InputDecoration(
            labelText: 'select_player_category'.tr,
            filled: true,
            fillColor: bgColor,
            border: OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          items: playerController.playerCategories.map((option) {
            return DropdownMenuItem(
              value: option.id,
              child: Text(option.title.toString()),
            );
          }).toList(),
          onChanged: (value) {
            medalController.selectPlayerCateId = value;
          },
        ),
      ],
    );
  }

  Widget _buildPlayerDropdown() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('select_player'.tr, style: const TextStyle(fontSize: 16)),
        const SizedBox(height: 5.0),
        PlayerDropdownSearch(
            selectedItem: selectedPlayer,
            onChanged: (value) {
              setState(() {
                selectPlayerId = value!.id.toString();
                selectedPlayer = value;
              });
            })
      ],
    );
  }
}
