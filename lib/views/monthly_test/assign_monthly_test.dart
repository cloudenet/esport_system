import 'package:esport_system/data/controllers/monthly_test_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/data/repo/monthly_test_repo.dart';
import 'package:esport_system/data/repo/subject_repo.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/dropdown_search_player_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/player_controller.dart';
import '../../data/controllers/subject_controller.dart';
import '../../data/model/category_player.dart';
import '../../helper/add_new_button.dart';
import '../../helper/custom_textformfield.dart';
import '../../helper/responsive_helper.dart';

class AssignMonthlyTest extends StatefulWidget {
  final String? typeEvent;

  const AssignMonthlyTest({super.key, this.typeEvent});

  @override
  State<AssignMonthlyTest> createState() => _AssignMonthlyTestState();
}

class _AssignMonthlyTestState extends State<AssignMonthlyTest> {
  var monthlyRepo = MonthLyTestRepo();
  var subjectRepo = SubjectRepo();
  var monthlyController = Get.find<MonthlyTestController>();
  var playerController = Get.find<PlayerController>();
  var monthlyTestController = Get.find<MonthlyTestController>();
  var subjectController = Get.find<SubjectController>();
  var userController = Get.find<UserController>();
  PlayerModel? selectedPlayerOne;
  PlayerModel? selectedPlayerTwo;
  String? selectedPlayerIdTwo;
  final double inputFieldWidth = 200;


  Map<String, Map<String, String>> scores = {};

  @override
  void initState() {
    playerDropDownSearch();
    userController.getListUser();
    playerController.getPlayer();
    monthlyController.getAllSubject();

    for (var player in playerController.dataPlayerForDropdownSearch) {
      scores[player.id!] = {};
      for (var subject in subjectController.subjectData) {
        scores[player.id!]![subject.id!] = ''; // Initialize with empty score
      }
    }
    super.initState();
  }

  void playerDropDownSearch() async {
    await playerController.getPlayerDropdownSearch(searchCat: "");
  }

  void getPlayerCategory() async {
    await playerController.getCategoryPlayer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor,
        body: GetBuilder<MonthlyTestController>(
          assignId: true,
          builder: (logic) {
            return Responsive(
                mobile: const SizedBox(),
                tablet: const SizedBox(),
                desktop: Padding(
                  padding: const EdgeInsets.all(16),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text("monthly_test".tr, style: const TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold)),
                        ),
                        const SizedBox(height: 15,),
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(24),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: secondaryColor,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('select_player_category'.tr,style: const TextStyle(fontSize: 18),),
                                        const SizedBox(height: 5.0),
                                        FormBuilderDropdown(
                                          name: 'select_player_category'.tr,
                                          borderRadius: BorderRadius.circular(10),
                                          decoration: InputDecoration(
                                            floatingLabelAlignment: FloatingLabelAlignment.center,
                                            floatingLabelBehavior: FloatingLabelBehavior.never,
                                            fillColor: bgColor,
                                            labelText: 'select_player_category'.tr,
                                            filled: true,
                                            border: OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                borderRadius: BorderRadius.circular(10)),
                                          ),
                                          //initialValue: logic.selectCategoryPlayer,
                                          items: playerController.playerCategories.map((option) =>
                                              DropdownMenuItem(
                                                value: option,
                                                child: Text(option.title.toString()),
                                              )).toList(),
                                          onChanged: (value) {
                                            setState(() {
                                              CategoryPlayer? a;
                                              a = value;
                                              logic.selectCategoryPlayer = a!.id.toString();
                                              playerController.getPlayerDropdownSearch(searchCat: a.title);
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                    child: GetBuilder<PlayerController>(
                                        builder: (logic1) {
                                          return Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text('select_player'.tr,style: const TextStyle(fontSize: 16)),
                                              const SizedBox(height: 5.0),
                                              PlayerDropdownSearch(
                                                  selectedItem: logic.selectPlayer,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      logic.selectedPlayerIdOne =value!.id.toString();
                                                      logic.selectPlayer = value;
                                                    });
                                                  })
                                            ],
                                          );
                                        }),
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'select_month/year'.tr,
                                          style: const TextStyle(fontSize: 16),
                                        ),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          readOnly: true,
                                          onTap: () =>
                                          logic.selectDateMonth(context,
                                          logic.monthYearTestController),
                                          name: 'join_date'.tr,
                                          controller: logic.monthYearTestController,
                                          hintText: "join_date".tr,
                                          icon: Icons.calendar_month_outlined,
                                          fillColor: bgColor,
                                          errorText: 'enter_join_date'.tr,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 15,),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  OKButton(
                                    btnName: 'generate'.tr,
                                    onPress: () {
                                      if (logic.selectCategoryPlayer!.isNotEmpty &&
                                          logic.selectedPlayerIdOne!.isNotEmpty &&
                                          logic.monthYearTestController.text.isNotEmpty) {

                                      } else {
                                        EasyLoading.showError(
                                          "please_select_all_field".tr,
                                          duration: const Duration(seconds: 2),
                                        );
                                      }
                                    },
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 15,),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                              minWidth: MediaQuery.of(context).size.width, // Force table to take full width
                            ),
                            child: _buildFullWidthDataTable(),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
            );
          },
        )
    );
  }

  Widget _buildFullWidthDataTable() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: DataTable(
        columnSpacing: .0,
        // Adds space between columns
        headingRowColor: WidgetStateProperty.all(secondaryColor),
        columns: _buildDataTableColumns(),
        rows: _buildDataTableRows(),
        dataRowHeight: 60.0, // Adds height to rows
      ),
    );
  }

  List<DataColumn> _buildDataTableColumns() {
    List<DataColumn> columns = [
      const DataColumn(
        label: SizedBox(
          width: 200,
          child: Text('Player Name',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
        ),
      ),
    ];

    //final double columnWidth = (MediaQuery.of(context).size.width-240) / (subjectController.subjectData.length); // Adjust for padding

    columns.addAll(monthlyController.subjectData.map((subject) =>
        DataColumn(
          label: SizedBox(
            width: 130,
            child: Text(subject.title ?? 'Subject',
                style: const TextStyle(fontWeight: FontWeight.bold)),
          ),
        )).toList());

    return columns;
  }

  List<DataRow> _buildDataTableRows() {
    return playerController.dataPlayerForDropdownSearch.map((player) {
      List<DataCell> cells = [
        DataCell(
            Text(player.name ?? 'Unknown Player')
        ),
      ];

      cells.addAll(monthlyController.subjectData.map((subject) {
        return DataCell(
          ConstrainedBox(
            constraints: BoxConstraints(maxWidth: inputFieldWidth),
            // Set the same width for all input fields
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
                onChanged: (value) {
                  //logic.updatePlayerScore(player.id!, subject.id!, value);
                },
              ),
            ),
          ),
        );
      }).toList());

      return DataRow(cells: cells);
    }).toList();
  }

}
