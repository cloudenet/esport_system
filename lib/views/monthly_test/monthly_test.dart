

import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/util/text_style.dart';
import 'package:esport_system/views/monthly_test/assign_monthly_test.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../data/controllers/home_controller.dart';
import '../../helper/add_new_button.dart';
import '../../helper/search_field.dart';
import '../home_section/screen/home.dart';

class MonthlyTest extends StatefulWidget {
  const MonthlyTest({super.key});

  @override
  State<MonthlyTest> createState() => _MonthlyTestState();
}

class _MonthlyTestState extends State<MonthlyTest> {

  // List<EventTest> listEventTest = [
  //   EventTest(no: '1', eventName: "Men Shooting ",eventKhmer: " បុរស វាយយកពិន្ទុ"),
  //   EventTest(no: '2', eventName: "Men Singles ",eventKhmer: " បុរស ១នាក់ ទល់ ១នាក់"),
  //   EventTest(no: '3', eventName: "Men Doubles ",eventKhmer: " បុរស ២នាក់ ទល់ ២នាក់"),
  //   EventTest(no: '4', eventName: "Men Triples ",eventKhmer: " បុរស ៣នាក់ ទល់ ៣នាក់"),
  //   EventTest(no: '5', eventName: "Women Shooting ",eventKhmer: " នារីវាយយកពិន្ទុ"),
  //   EventTest(no: '6', eventName: "Women Singles ",eventKhmer: " នារី ១នាក់ ទល់ ១នាក់"),
  //   EventTest(no: '7', eventName: "Women Doubles ",eventKhmer: " នារី២នាក់ ទល់ ២នាក់"),
  //   EventTest(no: '8', eventName: "Women Triples ",eventKhmer: " នារី៣នាក់ ទល់ ៣នាក់"),
  //   EventTest(no: '9', eventName: "Mixed Doubles (1M, 1W) ",eventKhmer: " ចម្រុះ បុរស១នាក់ នារី១នាក់"),
  //   EventTest(no: '10', eventName: "Mixed Triples (1M, 2W) ",eventKhmer: " ចម្រុះ បុរស១នាក់ នារី២នាក់"),
  //   EventTest(no: '11', eventName: "Mixed Triples (2M, 1W) ",eventKhmer: " ចម្រុះ បុរស២នាក់ នារី១នាក់"),
  // ];

  var playerController = Get.find<PlayerController>();
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();

  List<EventTest> listEventTest = [
    EventTest(no: '1', eventName: "Senior men pétanque player",eventKhmer: "ឈុតធំបុរស"),
    EventTest(no: '2', eventName: "Senior women pétanque player",eventKhmer: "ឈុតធំនារី"),
    EventTest(no: '3', eventName: "Man pétanque player",eventKhmer: "យុវជនផ្នែកបុរស"),
    EventTest(no: '4', eventName: "Women pétanque player",eventKhmer: "យុវជនផ្នែកនារី"),
    EventTest(no: '5', eventName: "Junior men pétanque player",eventKhmer: "ថ្នាលយុវជនផ្នែកបុរស"),
    EventTest(no: '6', eventName: "Junior women pétanque player",eventKhmer: "ថ្នាលយុវជនផ្នែកនារី"),
  ];

  @override
  void initState() {
    getPlayerDropdown();
    playerController.getCategoryPlayer();
    super.initState();
  }

  void getPlayerDropdown()async{
    await playerController.getPlayerDropdownSearch(searchCat: "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25), topRight: Radius.circular(25)),
        ),
        child: Column(
          children: [
            _buildDropdown(),
            Align(alignment: Alignment.topRight,child: AddNewButton(btnName:'Add New'.tr,onPress: (){
              homeController.updateWidgetHome(const AssignMonthlyTest());
            },),),
            const SizedBox(height: 22,),
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) {
                  return SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minWidth: constraints.maxWidth,
                        ),
                        child: DataTable(
                          dataRowMaxHeight: Get.height * 0.1,
                          headingRowColor:
                          WidgetStateColor.resolveWith((states) =>secondaryColor),
                          columns: [
                            DataColumn(
                              label: Text(
                                "No".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "Event Title".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "Description".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "Actions".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                          rows: List<DataRow>.generate(
                            listEventTest.length,
                                (index) {
                              var list = listEventTest[index];
                              return DataRow(
                                cells: [
                                  DataCell(
                                    Text(_truncateText(list.no, 20),),
                                  ),
                                  DataCell(
                                    Text(_truncateText(list.eventName, 100)),
                                  ),
                                  DataCell(
                                    Text(_truncateText(list.eventKhmer, 100),style: TextStylesHelper.khmerText16,),
                                  ),
                                  DataCell(
                                    Row(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            //buildViewEvent(context,list, eventController);
                                          },
                                          child: Container(
                                              padding: const EdgeInsets.all(6),
                                              decoration: BoxDecoration(
                                                color: Colors.amberAccent.withOpacity(0.1),
                                                border: Border.all(width: 1,color: Colors.amberAccent),
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              child: const Icon(
                                                Icons.visibility,
                                                size: 20,
                                                color: Colors.amberAccent,
                                              )),
                                        ),
                                        const SizedBox(width: 12,),
                                        GestureDetector(
                                          onTap: () { },
                                          child: Container(
                                              padding: const EdgeInsets.all(6),
                                              decoration: BoxDecoration(
                                                color: Colors.green.withOpacity(0.1),
                                                border: Border.all(width: 1,color: Colors.green),
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              child: const Icon(
                                                Icons.edit,
                                                size: 20,
                                                color: Colors.green,
                                              )),
                                        ),
                                        const SizedBox(width: 12,),
                                        GestureDetector(
                                          onTap: () {
                                            homeController.updateWidgetHome(AssignMonthlyTest(typeEvent: list.eventName));
                                          },
                                          child: Container(
                                              padding: const EdgeInsets.all(6),
                                              decoration: BoxDecoration(
                                                border: Border.all(width: 1, color: Colors.blue),
                                                color: Colors.blue.withOpacity(0.1),
                                                borderRadius: BorderRadius.circular(6),
                                              ),
                                              child: Image.asset(
                                                "assets/icons/control.png",
                                                width: 20,
                                                height: 20,
                                                color: Colors.blue,
                                              )),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),

            // Align(
            //   alignment: Alignment.centerRight,
            //   child: Padding(
            //     padding: const EdgeInsets.all(8.0),
            //     child: SizedBox(
            //       width: 700,
            //       child: Row(
            //         mainAxisAlignment: MainAxisAlignment.end,
            //         children: [
            //           IconButton(
            //             onPressed: logic.previousPage,
            //             icon: const Icon(Icons.arrow_back_ios),
            //           ),
            //           SingleChildScrollView(
            //             scrollDirection: Axis.horizontal,
            //             child: Row(
            //               children: [
            //                 for (int i = 1; i <= logic.totalPages; i++)
            //                   Padding(
            //                     padding: const EdgeInsets.all(0),
            //                     child: Row(
            //                       children: [
            //                         TextButton(
            //                           onPressed: () => logic.setPage(i),
            //                           child: Text(
            //                             i.toString(),
            //                             style: TextStyle(
            //                               color: logic.currentPage == i
            //                                   ? Colors.blue
            //                                   : Colors.white,
            //                             ),
            //                           ),
            //                         ),
            //                       ],
            //                     ),
            //                   ),
            //               ],
            //             ),
            //           ),
            //           IconButton(
            //             onPressed: logic.nextPage,
            //             icon: const Icon(Icons.arrow_forward_ios),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
  Widget _buildDropdown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              //value: eventController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  //eventController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              //eventController.search(value);
            },
                hintText: 'Search event title'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
class EventTest {
  final String no;
  final String eventName;
  final String eventKhmer;

  EventTest({
    required this.no,
    required this.eventName,
    required this.eventKhmer,
  });
}
