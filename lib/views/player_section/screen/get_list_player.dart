import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/views/player_section/screen/create_player_screen.dart';
import 'package:esport_system/views/player_section/screen/player_detail.dart';
import 'package:esport_system/views/player_section/screen/tracking_player_perfomance.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../helper/images.dart';

class ListPlayerScreen extends StatefulWidget {
  const ListPlayerScreen({super.key});

  @override
  State<ListPlayerScreen> createState() => _ListPlayerScreenState();
}

class _ListPlayerScreenState extends State<ListPlayerScreen> {
  var positionController = Get.find<PositionController>();
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();
  var playerController = Get.find<PlayerController>();

  @override
  void initState() {
    playerController.getPlayer();
    positionController.getPosition();
    teamController.getTeam();
    playerController.getCategoryPlayer();
    playerController.clearValueTextField();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<PlayerController>(
        builder: (logic) {
          return Container(
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
              Container(
              color: bgColor,
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: secondaryColor,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: DropdownButton<int>(
                      value: logic.limit,
                      items: [6, 12, 18, 24].map((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text(
                            '$value',
                            style: const TextStyle(color: Colors.white),
                          ),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        if (newValue != null) {
                          logic.setLimit(newValue);
                        }
                      },
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                      dropdownColor: secondaryColor,
                      underline: Container(),
                      elevation: 8,
                      borderRadius: BorderRadius.circular(8),
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      iconEnabledColor: Colors.grey,
                      iconDisabledColor: Colors.grey[400],
                      isDense: true,
                      menuMaxHeight: 200,
                      alignment: Alignment.center,
                    ),
                  ),
                  const Spacer(),
                  Expanded(
                    child: searchField(onChange: (value) {
                      logic.search(value);
                    }, hintText: 'search_player_name'.tr),
                  ),
                ],
              ),
            ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 32,
                            height: 32,
                            child: Image.asset(
                              "assets/icons/group.png",
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(width: 8), // Add spacing between the icon and text
                          Text(
                            'player'.tr,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: AddNewButton(
                        btnName:'add_new'.tr,
                        onPress: (){
                          homeController.updateWidgetHome(const CreatePlayerScreen(updatePlayer: false,playerModel: null,));
                        },),),
                  ],
                ),

                const SizedBox(height: 22,),
                Expanded(
                  child: logic.isLoading
                      ? const Center(child: CircularProgressIndicator())
                      : LayoutBuilder(
                              builder: (context, constraints) {
                                return SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                  child: Column(
                                    children: [
                                      SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                            minWidth: constraints.maxWidth,
                                          ),
                                          child: DataTable(
                                            dataRowMaxHeight: Get.height * 0.1,
                                            headingRowColor: WidgetStateColor.resolveWith(
                                                    (states) => secondaryColor),
                                            columns: [
                                              DataColumn(
                                                label: Text(
                                                  "no.".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "profile".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "name".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "name_khmer".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "team_name".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "position".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "matches_played".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                              DataColumn(
                                                label: Text(
                                                  "actions".tr,
                                                  style: const TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16,
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ],
                                            rows: logic.dataPlayer.isEmpty
                                            ?[]
                                            :List<DataRow>.generate(
                                              logic.dataPlayer.length, (index) {
                                                var data = logic.dataPlayer[index];
                                                return DataRow(
                                                  cells: [
                                                    DataCell(
                                                      Text(_truncateText('${index + 1}', 10),
                                                      ),
                                                    ),
                                                    DataCell(
                                                      data.photo != "null"
                                                          ? ClipOval(
                                                        child: Image.network(
                                                          "${AppConstants.baseUrl}${data.photo}",
                                                          width: 50,
                                                          height: 50,
                                                          fit: BoxFit.cover,
                                                          errorBuilder: (context, error, stackTrace) {
                                                            return ClipOval(
                                                              child: Image.asset(
                                                                Images.noImage,
                                                                height: 50,
                                                                width: 50,
                                                                fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                                              ),
                                                            );
                                                          },
                                                          loadingBuilder:
                                                              (context, child,
                                                              loadingProgress) {
                                                            if (loadingProgress ==
                                                                null) {
                                                              return child;
                                                            } else {
                                                              return Center(
                                                                child: CircularProgressIndicator(
                                                                  value: loadingProgress.expectedTotalBytes !=
                                                                      null
                                                                      ? loadingProgress.cumulativeBytesLoaded /
                                                                      (loadingProgress.expectedTotalBytes ?? 1)
                                                                      : null,
                                                                ),
                                                              );
                                                            }
                                                          },
                                                        ),
                                                      )
                                                          : Container(
                                                        width: 50,
                                                        height: 50,
                                                        decoration: BoxDecoration(
                                                            border: Border.all(color: Colors.grey, width: 2,),
                                                            shape: BoxShape.circle,
                                                            image: const DecorationImage(image: AssetImage("assets/images/no-image.webp"), fit: BoxFit.cover)),
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text(_truncateText('${data.name}', 20), // ID starts from 1
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text(_truncateText('${data.name_khmer}', 20), // ID starts from 1
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text(_truncateText('${data.teamName}', 20),),
                                                    ),
                                                    DataCell(
                                                      Text(_truncateText('${data.position!.title}', 20),
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text('${data.matchesPlayed}',),
                                                    ),
                                                    DataCell(
                                                      Row(
                                                        children: [
                                                          GestureDetector(
                                                            onTap: () {
                                                              //buildViewPlayer(data, context);
                                                              homeController.updateWidgetHome(PlayerDetail(data: data,));
                                                            },
                                                            child: Container(
                                                                padding: const EdgeInsets.all(6),
                                                                decoration: BoxDecoration(
                                                                  color: Colors.amberAccent.withOpacity(0.1),
                                                                  border: Border.all(width: 1, color: Colors.amberAccent),
                                                                  borderRadius: BorderRadius.circular(6),
                                                                ),
                                                                child: const Icon(
                                                                  Icons.visibility,
                                                                  size: 20,
                                                                  color: Colors.amberAccent,
                                                                )),
                                                          ),
                                                          const SizedBox(width: 12,),
                                                          GestureDetector(
                                                            onTap: () {
                                                              playerController.getOldValueUpdate(data);
                                                              homeController.updateWidgetHome(CreatePlayerScreen(updatePlayer: true,playerModel: data,));
                                                            },
                                                            child: Container(
                                                                padding: const EdgeInsets.all(6),
                                                                decoration: BoxDecoration(
                                                                  color: Colors.green.withOpacity(0.1),
                                                                  border: Border.all(width: 1,color: Colors.green),
                                                                  borderRadius: BorderRadius.circular(6),
                                                                ),
                                                                child: const Icon(Icons.edit, size: 20, color: Colors.green,)),
                                                          ),
                                                          const SizedBox(width: 12,),
                                                          GestureDetector(
                                                            onTap: () {
                                                              showDialog(
                                                                context: context,
                                                                builder: (BuildContext context) {
                                                                  return AlertDialog(
                                                                    content:Container(
                                                                      width:Get.width * 0.3,
                                                                      height:Get.height * 0.3,
                                                                      padding:const EdgeInsets.all(16),
                                                                      child: Column(
                                                                        children: [
                                                                          Align(alignment:Alignment.topCenter,
                                                                            child: Image.asset(
                                                                              "assets/images/warning.png",
                                                                              width:50,
                                                                              height:50,
                                                                            ),
                                                                          ),
                                                                          const SizedBox(height:15),
                                                                          Text("are_you_sure_you_want_to_delete_this_player?".tr,
                                                                              style: Theme.of(context).textTheme.bodyMedium
                                                                          ),
                                                                          const SizedBox(height:15),
                                                                          const Spacer(),
                                                                          Row(
                                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                                            mainAxisAlignment: MainAxisAlignment.end,
                                                                            children: [
                                                                              CancelButton(
                                                                                  btnName: 'cancel'.tr,
                                                                                  onPress: (){
                                                                                    Get.back();
                                                                                  }),
                                                                              const SizedBox(
                                                                                  width: 16),
                                                                              OKButton(
                                                                                  btnName: 'confirm'.tr,
                                                                                  onPress: () async {
                                                                                    logic.isLoading = true;
                                                                                    logic.update();
                                                                                    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
                                                                                    var result = await logic.deletePlayer(playerId: data.id.toString());
                                                                                    if (result['success']) {
                                                                                      EasyLoading.showSuccess('player_delete_successfully'.tr, dismissOnTap: true, duration: const Duration(seconds: 2),);
                                                                                      Get.back();
                                                                                      logic.getPlayer();
                                                                                    } else {
                                                                                      EasyLoading.showError(result['message'.tr], duration: const Duration(seconds: 2),);
                                                                                      Get.back();
                                                                                    }
                                                                                    EasyLoading.dismiss();
                                                                                    logic.isLoading = false;
                                                                                  }
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  );
                                                                },
                                                              );
                                                            },
                                                            child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.red.withOpacity(0.1),
                                                                border: Border.all( width: 1,color: Colors.red),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(Icons.delete,size: 20,color: Colors.red,),
                                                            ),
                                                          ),
                                                          const SizedBox(width: 12,),
                                                          if(1==2)
                                                          GestureDetector(
                                                            onTap: () {
                                                              homeController.updateWidgetHome(TrackingPlayerPerformance(playerData: data,));
                                                            },
                                                            child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration:BoxDecoration(
                                                                color: Colors.blue.withOpacity(0.1),
                                                                border: Border.all(width: 1,color: Colors.blue),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(Icons.person,size: 20, color: Colors.blue,),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              },
                                            ),
                                          ),
                                        ),
                                      ),
                                      if (logic.dataPlayer.isEmpty)...[
                                        const SizedBox(height: 50,),
                                        const Center(child: EmptyData()),
                                      ]
                                    ],
                                  ),
                                );
                              },
                            )
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 700,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            onPressed: logic.previousPage,
                            icon: const Icon(Icons.arrow_back_ios),
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                for (int i = 1; i <= logic.totalPages; i++)
                                  Padding(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        TextButton(
                                          onPressed: () => logic.setPage(i),
                                          child: Text(
                                            i.toString(),
                                            style: TextStyle(
                                              color: logic.currentPage == i
                                                  ? Colors.blue
                                                  : Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: logic.nextPage,
                            icon: const Icon(Icons.arrow_forward_ios),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
