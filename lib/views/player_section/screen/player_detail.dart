import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/views/player_section/screen/get_list_player.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/controllers/home_controller.dart';
import '../../../helper/file_type_extention.dart';
import '../../../helper/responsive_helper.dart';
import '../../home_section/screen/home.dart';


class PlayerDetail extends StatefulWidget {
  final PlayerModel? data;
  const PlayerDetail({super.key,required this.data});

  @override
  State<PlayerDetail> createState() => _PlayerDetailState();
}

class _PlayerDetailState extends State<PlayerDetail> {
  var homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
          mobile: bodyDetail(isMobile: true),
          tablet: bodyDetail(isTablet: true),
          desktop: bodyDetail()
      ),
    );
  }

  Widget bodyDetail({bool? isMobile,bool? isTablet}){
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data!.photo.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                    children: [
                      Text(
                        widget.data!.name != "null" ? widget.data!.name.toString() : "N/A",
                        style: const TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,  // Bold name
                        ),
                      ),
                      const SizedBox(height: 4.0),  // Space between name and rank
                      Text(
                        widget.data!.typePlayer != "null" ? widget.data!.typePlayer.toString() : "N/A",
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey[600],  // Rank text style
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("personal_information".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "full_name".tr,
                            textData: widget.data!.name != "null" ? widget.data!.name.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "khmer_name".tr,
                            textData: widget.data!.name_khmer != "null" ? widget.data!.name_khmer.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "height".tr,
                            textData: widget.data!.height != "null" ? widget.data!.height.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "weight".tr,
                            textData: widget.data!.weight != "null" ? widget.data!.weight.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("team_information".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if(isMobile != null && isMobile)...[
                    buildText(
                        title: "team_name".tr,
                        textData: widget.data!.teamName != "null" ? widget.data!.teamName.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ]else...[
                    buildText(
                        title: "team_name".tr,
                        textData: widget.data!.teamName != "null" ? widget.data!.teamName.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ],
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "jersey_number".tr,
                            textData: widget.data!.jerseyNumber != "null" ? widget.data!.jerseyNumber.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "position".tr,
                            textData: widget.data!.position!.title != "null" ? widget.data!.position!.title.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "matches_played".tr,
                      textData: widget.data!.matchesPlayed != "null" ? widget.data!.matchesPlayed.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "previous_team".tr,
                      textData: widget.data!.previousTeam != "null" ? widget.data!.previousTeam.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("contract_and_salary".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if(isMobile != null && isMobile || isTablet != null && isTablet)...[
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                              title: "contract_start_date".tr,
                              textData: widget.data!.contractStartDate != "null" ? formatDate(widget.data!.contractStartDate.toString()) : "N/A",
                              height: 50,
                              padding: 8,
                              alignment: Alignment.centerLeft
                          ),
                        ),
                        const SizedBox(width: 16,),
                        Expanded(
                          child: buildText(
                              title: "contract_end_date".tr,
                              textData: widget.data!.contractStartDate != "null" ? formatDate(widget.data!.contractStartDate.toString()) : "N/A",
                              height: 50,
                              padding: 8,
                              alignment: Alignment.centerLeft
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    buildText(
                        title: "salary".tr,
                        textData: widget.data!.salary != "null" ? widget.data!.salary.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ]else...[
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                              title: "contract_start_date".tr,
                              textData: widget.data!.contractEndDate != "null" ? formatDate(widget.data!.contractEndDate.toString()) : "N/A",
                              height: 50,
                              padding: 8,
                              alignment: Alignment.centerLeft
                          ),
                        ),
                        const SizedBox(width: 16,),
                        Expanded(
                          child: buildText(
                              title: "contract_end_date".tr,
                              textData: widget.data!.contractEndDate != "null" ? formatDate(widget.data!.contractEndDate.toString()) : "N/A",
                              height: 50,
                              padding: 8,
                              alignment: Alignment.centerLeft
                          ),
                        ),
                        const SizedBox(width: 16,),
                        Expanded(
                          child: buildText(
                              title: "salary".tr,
                              textData: widget.data!.salary != "null" ? widget.data!.salary.toString() : "N/A",
                              height: 50,
                              padding: 8,
                              alignment: Alignment.centerLeft
                          ),
                        ),
                      ],
                    ),
                  ]
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("documents".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                        height: 50,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.symmetric(horizontal: 4.0),
                              child: Chip(
                                label: Text(getFileNameFromUrl(widget.data!.fileBioPlayer.toString())),
                                avatar: Icon(
                                  getFileIcon(widget.data!.fileBioPlayer.toString()),
                                  color: getFileIconColor(widget.data!.fileBioPlayer.toString()),
                                ),
                                deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                                deleteButtonTooltipMessage: 'view_file'.tr,
                                onDeleted: (){},
                              ),
                            ),
                          ],
                        )
                    ),
                  ),

                ],
              ),
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const ListPlayerScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),

          ],
        ),
      ),
    );
  }
}

