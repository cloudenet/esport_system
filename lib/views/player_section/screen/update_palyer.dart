import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/player_section/screen/get_list_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../../../data/controllers/home_controller.dart';


class UpdatePlayerScreen extends StatefulWidget {
  final PlayerModel? data;
  const UpdatePlayerScreen({Key? key,  required this.data}) : super(key: key);

  @override
  State<UpdatePlayerScreen> createState() => _UpdatePlayerScreenState();
}

class _UpdatePlayerScreenState extends State<UpdatePlayerScreen> {

  var homeController = Get.find<HomeController>();
  final _formKey = GlobalKey<FormBuilderState>();
  var teamController = Get.find<TeamController>();
  var positionController = Get.find<PositionController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor,
        body: GetBuilder<PlayerController>(
          builder: (logic) {
            return SingleChildScrollView(
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(16.0),
                    child: Card(
                      color: secondaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: FormBuilder(
                        key: _formKey,
                        child: Padding(
                          padding: const EdgeInsets.all(24.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Update'.tr, style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),),
                              const SizedBox(height: 24.0),
                              Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Select Team'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          FormBuilderDropdown<String>(
                                            borderRadius: BorderRadius.circular(10),
                                            name: 'Team Name'.tr,
                                            initialValue: logic.teamIdController.text,
                                            decoration: InputDecoration(
                                              labelText: "Select Team".tr,
                                              floatingLabelAlignment: FloatingLabelAlignment.center,
                                              floatingLabelBehavior: FloatingLabelBehavior.never,
                                              fillColor: bgColor,
                                              filled: true,
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(10),
                                                ),
                                              ),
                                            ),
                                            items: teamController.listTeam.map((team) {
                                              return DropdownMenuItem<String>(
                                                value: team.id,
                                                child: Text(team.teamName ?? 'Unknown'.tr),
                                              );
                                            }).toList(),
                                            onChanged: (value) {
                                              logic.teamIdController.text = value!;
                                            },
                                          )
                                        ],
                                      )
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Select Position'.tr,
                                          style: TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        FormBuilderDropdown<String>(
                                          borderRadius: BorderRadius.circular(10),
                                          name: 'Position Name'.tr,
                                          initialValue: logic.positionIdController.text,
                                          decoration: InputDecoration(
                                            labelText: "Select Position".tr,
                                            floatingLabelAlignment: FloatingLabelAlignment.center,
                                            floatingLabelBehavior: FloatingLabelBehavior.never,
                                            fillColor: bgColor,
                                            filled: true,
                                            border: const  OutlineInputBorder(
                                              borderSide: BorderSide.none,
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(10),
                                              ),
                                            ),
                                          ),
                                          items: positionController.positionData
                                              .map((position) {
                                            return DropdownMenuItem<String>(
                                              value: position.id,
                                              child: Text(position.title ?? 'Unknown'.tr),
                                            );
                                          }).toList(),
                                          onChanged: (value) {
                                            logic.positionIdController.text = value!;
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 16.0),
                              Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Height'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'Height'.tr,
                                            controller: logic.heightController,
                                            hintText: "Height".tr,
                                            icon: Icons.title_outlined,
                                            fillColor: bgColor, errorText: 'Enter Height'.tr,
                                          ),
                                        ],
                                      )
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Weight'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'Weight'.tr,
                                            controller: logic.weightController,
                                            hintText: "Weight".tr,
                                            icon: Icons.title_outlined,
                                            fillColor: bgColor, errorText: 'Enter Weight'.tr,
                                          ),
                                        ],
                                      )
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Jersey Number'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'Jersey Number'.tr,
                                            controller: logic.jerseyNumberController,
                                            hintText: "Jersey Number".tr,
                                            icon: Icons.title_outlined,
                                            fillColor: bgColor, errorText: 'Enter jersey number'.tr,
                                          ),
                                        ],
                                      )
                                  ),
                                ],
                              ),
                              const SizedBox(height: 16.0),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Join Date'.tr,style: TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          readOnly: true,
                                          onTap: () => logic.selectDate(context, logic.joinDateController),
                                          name: 'Join Date'.tr,
                                          controller: logic.joinDateController,
                                          hintText: "Join Date".tr,
                                          icon: Icons.calendar_month_outlined,
                                          fillColor: bgColor, errorText: 'Select join date'.tr,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Start Date'.tr,style: TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          readOnly: true,
                                          onTap: () => logic.selectDate(context, logic.contractStartDateController),
                                          name: 'Start Date'.tr,
                                          controller: logic.contractStartDateController,
                                          hintText: "Select start date".tr,
                                          icon: Icons.calendar_month_outlined,
                                          fillColor: bgColor, errorText: '',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('End Date'.tr,style: TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          readOnly: true,
                                          onTap: () => logic.selectDate(context, logic.contractEndDateController),
                                          name: 'End Date'.tr,
                                          controller: logic.contractEndDateController,
                                          hintText: "End Date".tr,
                                          icon: Icons.calendar_month_outlined,
                                          fillColor: bgColor, errorText: 'Select end date'.tr,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 16.0),
                              Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Previous Team'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'Previous Team'.tr,
                                            controller: logic.previousTeamController,
                                            hintText: "Previous Team".tr,
                                            icon: CupertinoIcons.person_3,
                                            fillColor: bgColor,
                                            errorText: 'Select previous team'.tr,
                                          ),
                                        ],
                                      )
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Salary'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'Salary'.tr,
                                            controller: logic.salaryController,
                                            hintText: "Salary".tr,
                                            icon: CupertinoIcons.money_dollar_circle,
                                            fillColor: bgColor,
                                            errorText: 'Enter salary'.tr,
                                          ),
                                        ],
                                      )
                                  )
                                ],
                              ),
                              const SizedBox(height: 16.0),
                              Row(
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Goals Score'.tr,style: TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          name: 'Goals Score'.tr,
                                          controller: logic.goalsScoredController,
                                          hintText: "Goals Score".tr,
                                          icon: Icons.sports_basketball_rounded,
                                          fillColor: bgColor,
                                          errorText: 'Enter Goals Score'.tr,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(width: 16.0),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Assists'.tr,style: TextStyle(fontSize: 16),),
                                        const SizedBox(height: 5.0),
                                        CustomFormBuilderTextField(
                                          name: 'Assists'.tr,
                                          controller: logic.assistsController,
                                          hintText: "Assists".tr,
                                          icon: Icons.accessibility,
                                          fillColor: bgColor,
                                          errorText: 'Enter Assists'.tr,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 16.0),
                              Row(
                                children: [
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Matches Play'.tr,style: TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'Matches Play'.tr,
                                            controller: logic.matchesPlayedController,
                                            hintText: "Matches Play".tr,
                                            icon: CupertinoIcons.list_bullet,
                                            fillColor: bgColor,
                                            errorText: 'Enter Matches Play'.tr,
                                          ),
                                        ],
                                      )
                                  ),
                                  const SizedBox(width: 16.0),
                                  // Expanded(
                                  //     child: Column(
                                  //       crossAxisAlignment: CrossAxisAlignment.start,
                                  //       children: [
                                  //         const Text('Injeries',style: TextStyle(fontSize: 16),),
                                  //         const SizedBox(height: 5.0),
                                  //         CustomFormBuilderTextField(
                                  //           name: 'Injeries',
                                  //           controller: logic.injuriesController,
                                  //           hintText: "Injeries",
                                  //           icon: CupertinoIcons.list_bullet,
                                  //           fillColor: bgColor,
                                  //           errorText: 'Enter Injeries',
                                  //         ),
                                  //       ],
                                  //     )
                                  // ),
                                ],
                              ),
                              // const SizedBox(height: 16.0),
                              // Row(
                              //   children: [
                              //     Expanded(
                              //       child: Column(
                              //         crossAxisAlignment: CrossAxisAlignment.start,
                              //         children: [
                              //           const Text('Yellow Card',style: TextStyle(fontSize: 16),),
                              //           const SizedBox(height: 5.0),
                              //           FormBuilderTextField(
                              //             name: 'Yellow Card',
                              //             controller: logic.yellowCardsController,
                              //             validator: FormBuilderValidators.compose([FormBuilderValidators.required(),]),
                              //             keyboardType: TextInputType.number,
                              //             decoration: InputDecoration(
                              //               hintText: 'Yellow Card',
                              //               fillColor: bgColor,
                              //               filled: true,
                              //               border: const OutlineInputBorder(
                              //                 borderSide: BorderSide.none,
                              //                 borderRadius: BorderRadius.all(
                              //                   Radius.circular(10),
                              //                 ),
                              //               ),
                              //               suffixIcon: Column(
                              //                 children: [
                              //                   IconButton(
                              //                     icon: const Icon(Icons.arrow_drop_up),
                              //                     onPressed: () {
                              //                       int currentValue = int.tryParse(logic.yellowCardsController.text) ?? 0;
                              //                       logic.yellowCardsController.text = (currentValue + 1).toString();
                              //                     },
                              //                     iconSize: 15,
                              //                   ),
                              //                   IconButton(
                              //                       icon: const Icon(Icons.arrow_drop_down),
                              //                       onPressed: () {
                              //                         int currentValue = int.tryParse(logic.yellowCardsController.text) ?? 0;
                              //                         if (currentValue > 0) {
                              //                           logic.yellowCardsController.text = (currentValue - 1).toString();
                              //                         }
                              //                       },
                              //                       iconSize: 15
                              //                   ),
                              //                 ],
                              //               ),
                              //               contentPadding: const EdgeInsets.symmetric(vertical: .0, horizontal: 12.0),
                              //             ),
                              //           ),
                              //         ],
                              //       ),
                              //     ),
                              //     const SizedBox(width: 16.0),
                              //     Expanded(
                              //       child: Column(
                              //         crossAxisAlignment: CrossAxisAlignment.start,
                              //         children: [
                              //           const Text('Red Card',style: TextStyle(fontSize: 16),),
                              //           const SizedBox(height: 5.0),
                              //           FormBuilderTextField(
                              //             name: 'Red Card',
                              //             controller: logic.redCardsController,
                              //             validator:
                              //             FormBuilderValidators.compose([FormBuilderValidators.required(),]),
                              //             keyboardType: TextInputType.number,
                              //             decoration: InputDecoration(
                              //               hintText: 'Red Card',
                              //               fillColor: bgColor,
                              //               filled: true,
                              //               border: const OutlineInputBorder(
                              //                 borderSide: BorderSide.none,
                              //                 borderRadius: BorderRadius.all(
                              //                   Radius.circular(10),
                              //                 ),
                              //               ),
                              //               suffixIcon: Column(
                              //                 children: [
                              //                   IconButton(
                              //                     icon: const Icon(Icons.arrow_drop_up),
                              //                     onPressed: () {
                              //                       int currentValue = int.tryParse(logic.redCardsController.text) ?? 0;
                              //                       logic.redCardsController.text = (currentValue + 1) .toString();
                              //                     },
                              //                     iconSize:15,
                              //                     padding: EdgeInsets.zero,
                              //                   ),
                              //                   IconButton(
                              //                     icon: const Icon(Icons.arrow_drop_down),
                              //                     onPressed: () {
                              //                       int currentValue = int.tryParse(logic.redCardsController.text) ?? 0;
                              //                       if (currentValue > 0) {
                              //                         logic.redCardsController.text = (currentValue - 1).toString();
                              //                       }
                              //                     },
                              //                     iconSize: 15,
                              //                     padding: EdgeInsets.zero,
                              //                   ),
                              //                 ],
                              //               ),
                              //               contentPadding: const EdgeInsets.symmetric(vertical: .0, horizontal: 12.0),
                              //             ),
                              //           ),
                              //         ],
                              //       ),
                              //     ),
                              //   ],
                              // ),
                              const SizedBox(height: 24.0),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  CancelButton(btnName: 'Cancel'.tr, onPress: (){
                                    homeController.updateWidgetHome(const ListPlayerScreen());
                                  }),
                                  const SizedBox(width: 16,),
                                  OKButton(btnName: 'Update'.tr,
                                    onPress: () async {
                                      if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                        // logic.updatePlayer(
                                        //     playerId: widget.data!.id.toString(),
                                        //     teamId: logic.teamIdController.text,
                                        //     positoinId: logic.positionIdController.text,
                                        //     height: logic.heightController.text,
                                        //     weight: logic.weightController.text,
                                        //     joinDate: logic.joinDateController.text,
                                        //     jerseyNumber: logic.jerseyNumberController.text,
                                        //     previousTeam: logic.previousTeamController.text,
                                        //     contractStartDate: logic.contractStartDateController.text,
                                        //     contractEndDate: logic.contractEndDateController.text,
                                        //     salary: logic.salaryController.text,
                                        //     goalScore: logic.goalsScoredController.text,
                                        //     assists: logic.assistsController.text,
                                        //     matchesPlay: logic.matchesPlayedController.text,
                                        //     yellowCard: logic.yellowCardsController.text,
                                        //     redCard: logic.redCardsController.text,
                                        //     injeries: logic.injuriesController.text);
                                      }
                                    }
                                  )

                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
          },
));
  }
}

