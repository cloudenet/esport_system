import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/player_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/dropdown_search_user_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/player_section/screen/get_list_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/player_controller.dart';

import '../../../data/controllers/home_controller.dart';

class CreatePlayerScreen extends StatefulWidget {
  final bool updatePlayer;
  final PlayerModel? playerModel;

  const CreatePlayerScreen(
      {super.key, required this.updatePlayer, required this.playerModel});

  @override
  State<CreatePlayerScreen> createState() => _CreatePlayerScreenState();
}

class _CreatePlayerScreenState extends State<CreatePlayerScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  var userController = Get.find<UserController>();
  var teamController = Get.find<TeamController>();
  var positionController = Get.find<PositionController>();
  var playerController = Get.find<PlayerController>();
  var homeController = Get.find<HomeController>();



  @override
  void initState() {
    listUser();
    teamController.getTeam();
    positionController.getPosition();
    playerController.getPlayer();
    super.initState();
  }

  void listUser() async {
    await userController.getListUserDropdown();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<PlayerController>(
        builder: (logic) {
          return GetBuilder<UserController>(
            builder: (controller) {
              return SingleChildScrollView(
                child: Container(
                  width: Get.width,
                  padding: const EdgeInsets.all(24),
                  child: Column(
                    children: [
                      FormBuilder(
                        key: _formKey,
                        child: Responsive(
                            mobile: bodyMobile(),
                            tablet: body(),
                            desktop: body()
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }

  Widget body() {
    return GetBuilder<PlayerController>(builder: (logic) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.updatePlayer ? 'update_player'.tr : 'create_player'.tr,
            style: const TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      Text(
                        'select_player'.tr,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 10.0),
                      if(widget.updatePlayer)...[
                        Container(
                          padding: const EdgeInsets.all(12),
                          decoration: BoxDecoration(
                            color: secondaryColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Row(
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.blue,
                                radius: 12,
                                child: Text(
                                  (logic.previousPlayer?.name?.substring(
                                      0, 1) ?? 'N').toUpperCase(),
                                  style: const TextStyle(
                                      color: Colors.white, fontSize: 12),
                                ),
                              ),
                              const SizedBox(width: 8),
                              Expanded(
                                child: Text(
                                  logic.previousPlayer?.name?.toUpperCase() ??
                                      'N/A',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        )
                      ]
                      else
                        ...[
                          UserDropdownSearch(
                            checkUpdate: false,
                            selectedItem: logic.previousSelectedUser,
                            onChanged: (value) {
                              setState(() {
                                logic.selectedUserId = value!.id.toString();
                              });
                            },
                          )
                        ]
                    ],
                  )),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'select_player_category'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 10.0),
                    FormBuilderDropdown(
                      name: 'player_category'.tr,
                      borderRadius:
                      BorderRadius.circular(10),
                      decoration: InputDecoration(
                        floatingLabelAlignment:
                        FloatingLabelAlignment.center,
                        floatingLabelBehavior:
                        FloatingLabelBehavior.never,
                        fillColor: secondaryColor,
                        labelText: 'select_player_category'.tr,
                        filled: true,
                        border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius:
                            BorderRadius.circular(
                                10)),
                      ),
                      initialValue: logic.selectedPlayerCate,
                      items: logic.playerCategories.map((option) =>
                          DropdownMenuItem(
                            value: option.id,
                            child: Text(option.title.toString()),
                          ))
                          .toList(),
                      onChanged: (value) {
                        setState(() {
                          logic.selectedPlayerCate = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'height'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      name: 'height'.tr,
                      controller: logic.heightController,
                      hintText: "height".tr,
                      icon: Icons.title_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_height'.tr,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'weight'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      name: 'weight'.tr,
                      controller: logic.weightController,
                      hintText: "weight".tr,
                      icon: Icons.title_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_weight'.tr,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'select_position'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(10),
                      value: logic.selectPositionId,
                      items: positionController.positionData.map((user) {
                        return DropdownMenuItem<String>(
                          value: user.id,
                          child: Text("${user.title}"),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          logic.selectPositionId = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "select_position".tr,
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        floatingLabelAlignment: FloatingLabelAlignment.center,
                        fillColor: secondaryColor,
                        filled: true,
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 8.0),
              // Reduce the spacing
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'select_team'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(10),
                      value: logic.selectTeamId,
                      items: teamController.listTeam.map((user) {
                        return DropdownMenuItem<String>(
                          value: user.id,
                          child: Text(user.teamName!),
                        );
                      }).toList(),
                      onChanged: (value) {
                        setState(() {
                          logic.selectTeamId = value;
                        });
                      },
                      decoration: InputDecoration(
                        labelText: "select_team".tr,
                        floatingLabelAlignment: FloatingLabelAlignment.center,
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        fillColor: secondaryColor,
                        filled: true,
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // const SizedBox(width: 8.0), // Reduce the spacing
              // Expanded(
              //   flex: 2, // Reduce the flex to allow more space for other widgets
              //   child: Column(
              //     crossAxisAlignment: CrossAxisAlignment.start,
              //     children: [
              //       Text(
              //         'jersey_number'.tr,
              //         style: const TextStyle(fontSize: 16),
              //       ),
              //       const SizedBox(height: 5.0),
              //       CustomFormBuilderTextField(
              //         name: 'jersey_number'.tr,
              //         controller: logic.jerseyNumberController,
              //         hintText: "jersey_number".tr,
              //         icon: Icons.title_outlined,
              //         fillColor: secondaryColor,
              //         errorText: 'enter_jersey_number'.tr,
              //       ),
              //     ],
              //   ),
              // ),
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'join_date'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () => logic.selectDate(context, logic.joinDateController),
                      name: 'join_date'.tr,
                      controller:
                      logic.joinDateController,
                      hintText: "join_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_join_date',
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'contract_start_date'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () => logic.selectDate(context, logic.contractStartDateController),
                      name: 'start_date'.tr,
                      controller: logic.contractStartDateController,
                      hintText: "enter_start_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: secondaryColor,
                      errorText: '',
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'contract_end_date'.tr,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () => logic.selectDate(context, logic.contractEndDateController),
                      name: 'end_date'.tr,
                      controller: logic.contractEndDateController,
                      hintText: "end_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_end_date'.tr,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      Text(
                        'previous_team'.tr,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'previous_team'.tr,
                        controller:
                        logic.previousTeamController,
                        hintText: "previous_team".tr,
                        icon: CupertinoIcons.person_3,
                        fillColor: secondaryColor,
                        errorText: 'enter_previous_team'.tr,
                      ),
                    ],
                  )),
              const SizedBox(width: 16.0),
              Expanded(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      Text(
                        'benefit'.tr,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'benefit'.tr,
                        controller: logic.salaryController,
                        hintText: "benefit".tr,
                        icon: CupertinoIcons.money_dollar_circle,
                        fillColor: secondaryColor,
                        errorText: 'enter_benefit'.tr,
                      ),
                    ],
                  ))
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'matches_play'.tr,
                        style: const TextStyle(fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'matches_play'.tr,
                        controller:
                        logic.matchesPlayedController,
                        hintText: "matches_play".tr,
                        icon: CupertinoIcons.list_bullet,
                        fillColor: secondaryColor,
                        errorText: 'enter_matches_play'.tr,
                      ),
                    ],
                  )),
              const SizedBox(width: 16.0),
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "add_file".tr, style: const TextStyle(fontSize: 16),),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'add_file'.tr,
                        controller: logic.fileBioController,
                        hintText: "add_file".tr,
                        icon: Icons.attachment_sharp,
                        fillColor: secondaryColor,
                        errorText: 'enter_file'.tr,
                        onTap: () {
                          logic.pickFile();
                        },
                      ),
                    ],
                  )
              ),
            ],
          ),
          const SizedBox(height: 24.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CancelButton(
                  btnName: 'cancel'.tr,
                  onPress: () {
                    logic.clearValueTextField();
                    homeController.updateWidgetHome(const ListPlayerScreen());
                  }),
              const SizedBox(width: 16.0),
              OKButton(
                btnName: widget.updatePlayer ? 'update'.tr : 'create'.tr,
                onPress: () async {
                  if (widget.updatePlayer) {
                    // print("---------previousPlayerID   : ${logic.previousPlayerID}");
                    // print("---------selectedPlayerCate   : ${logic.selectedPlayerCate}");
                    // print("---------heightController   : ${logic.heightController.text}");
                    // print("---------weightController   : ${logic.weightController.text}");
                    // print("---------selectPositionId   : ${logic.selectPositionId}");
                    // print("---------selectTeamId   : ${logic.selectTeamId}");
                    // print("---------jerseyNumberController   : ${logic.jerseyNumberController.text}");
                    // print("---------joinDateController   : ${logic.joinDateController.text}");
                    // print("---------contractStartDateController   : ${logic.contractStartDateController.text}");
                    // print("---------contractEndDateController   : ${logic.contractEndDateController.text}");
                    // print("---------previousTeamController   : ${logic.previousTeamController.text}");
                    // print("---------salaryController   : ${logic.salaryController.text}");
                    // print("---------matchesPlayedController   : ${logic.matchesPlayedController.text}");
                    // print("---------fileBioPlayer   : ${logic.fileBioPlayer}");
                    if (widget.playerModel != null &&
                        logic.previousPlayerID!.isNotEmpty &&
                        logic.selectedPlayerCate!.isNotEmpty &&
                        logic.heightController.text.isNotEmpty &&
                        logic.weightController.text.isNotEmpty &&
                        logic.selectPositionId!.isNotEmpty &&
                        logic.selectTeamId!.isNotEmpty &&
                        logic.joinDateController.text.isNotEmpty &&
                        logic.contractStartDateController.text.isNotEmpty &&
                        logic.contractEndDateController.text.isNotEmpty &&
                        logic.previousTeamController.text.isNotEmpty &&
                        logic.salaryController.text.isNotEmpty &&
                        logic.fileBioPlayer != null) {
                      logic.updatePlayerV2();
                    }
                  } else {
                    if (logic.selectedUserId!.isNotEmpty &&
                        logic.selectedPlayerCate!.isNotEmpty &&
                        logic.heightController.text.isNotEmpty &&
                        logic.weightController.text.isNotEmpty &&
                        logic.selectPositionId!.isNotEmpty &&
                        logic.selectTeamId!.isNotEmpty &&
                        logic.joinDateController.text.isNotEmpty &&
                        logic.contractStartDateController.text.isNotEmpty &&
                        logic.contractEndDateController.text.isNotEmpty &&
                        logic.previousTeamController.text.isNotEmpty &&
                        logic.salaryController.text.isNotEmpty &&
                        logic.fileBioPlayer != null) {
                      logic.createPlayerV2();
                    } else {
                      EasyLoading.showError("please_input_all_field",
                        duration: const Duration(seconds: 2),
                      );
                    }
                  }
                },
              )
            ],
          ),
        ],
      );
    });
  }

  Widget bodyMobile() {
    return GetBuilder<PlayerController>(builder: (logic) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            widget.updatePlayer
                ? 'update_player'.tr
                : 'create_player'.tr,
            style: const TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 16.0),
          Text(
            'select_player'.tr,
            style: const TextStyle(fontSize: 16),
          ),
          const SizedBox(height: 10.0),
          if(widget.updatePlayer)...[
            Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                color: secondaryColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.blue,
                    radius: 12,
                    child: Text(
                      (logic.previousPlayer?.name
                          ?.substring(0, 1) ?? 'N')
                          .toUpperCase(),
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 12),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Text(
                      logic.previousPlayer?.name
                          ?.toUpperCase() ?? 'N/A',
                      style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            )
          ]
          else
            ...[
              UserDropdownSearch(
                checkUpdate: false,
                selectedItem: logic.previousSelectedUser,
                onChanged: (value) {
                  setState(() {
                    logic.selectedUserId =
                        value!.id.toString();
                  });
                },
              )
            ],
          const SizedBox(height: 16),
          Text(
            'select_player_category'.tr,
            style: const TextStyle(fontSize: 16),
          ),
          const SizedBox(height: 10.0),
          FormBuilderDropdown(
            name: 'player_category'.tr,
            borderRadius:
            BorderRadius.circular(10),
            decoration: InputDecoration(
              floatingLabelAlignment:
              FloatingLabelAlignment.center,
              floatingLabelBehavior:
              FloatingLabelBehavior.never,
              fillColor: secondaryColor,
              labelText: 'select_player_category'.tr,
              filled: true,
              border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius:
                  BorderRadius.circular(
                      10)),
            ),
            initialValue: logic.selectedPlayerCate,
            items: logic.playerCategories.map((option) =>
                DropdownMenuItem(
                  value: option.id,
                  child: Text(option.title.toString()),
                ))
                .toList(),
            onChanged: (value) {
              setState(() {
                logic.selectedPlayerCate = value;
              });
            },
          ),
          const SizedBox(height: 16),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'height'.tr,
                      style: const TextStyle(
                          fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      name: 'height'.tr,
                      controller: logic.heightController,
                      hintText: "height".tr,
                      icon: Icons.title_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_height'.tr,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment
                      .start,
                  children: [
                    Text(
                      'weight'.tr,
                      style: const TextStyle(
                          fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      name: 'weight'.tr,
                      controller: logic.weightController,
                      hintText: "weight".tr,
                      icon: Icons.title_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_weight'.tr,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'select_position'.tr,
                style: const TextStyle(fontSize: 16),
              ),
              const SizedBox(height: 5.0),
              DropdownButtonFormField<String>(
                borderRadius: BorderRadius.circular(10),
                value: logic.selectPositionId,
                items: positionController.positionData
                    .map((user) {
                  return DropdownMenuItem<String>(
                    value: user.id,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      // Add padding around the content
                      child: SizedBox(
                        width: 150,
                        // Adjust the width to your desired size
                        child: Text("${user.title}"),
                      ),
                    ),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    logic.selectPositionId = value;
                  });
                },
                decoration: InputDecoration(
                  labelText: "select_position".tr,
                  floatingLabelBehavior: FloatingLabelBehavior
                      .never,
                  floatingLabelAlignment: FloatingLabelAlignment
                      .center,
                  fillColor: secondaryColor,
                  filled: true,
                  border: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'select_team'.tr,
                style: const TextStyle(fontSize: 16),
              ),
              const SizedBox(height: 5.0),
              DropdownButtonFormField<String>(
                borderRadius: BorderRadius.circular(10),
                value: logic.selectTeamId,
                items: teamController.listTeam.map((user) {
                  return DropdownMenuItem<String>(
                    value: user.id,
                    child: Text(
                      (user.teamName?.length ?? 0) > 50
                          ? '${user.teamName?.substring(
                          0, 50)}...'
                          : user.teamName ?? 'unknown'.tr,
                      overflow: TextOverflow.ellipsis,),
                  );
                }).toList(),
                onChanged: (value) {
                  setState(() {
                    logic.selectTeamId = value;
                  });
                },
                decoration: InputDecoration(
                  labelText: "select_team".tr,
                  floatingLabelAlignment: FloatingLabelAlignment
                      .center,
                  floatingLabelBehavior: FloatingLabelBehavior
                      .never,
                  fillColor: secondaryColor,
                  filled: true,
                  border: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment
                      .start,
                  children: [
                    Text(
                      'join_date'.tr,
                      style: const TextStyle(
                          fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () =>
                          logic.selectDate(context,
                              logic.joinDateController),
                      name: 'join_date'.tr,
                      controller:
                      logic.joinDateController,
                      hintText: "join_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_join_date',
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment
                      .start,
                  children: [
                    Text(
                      'contract_start_date'.tr,
                      style: const TextStyle(
                          fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () =>
                          logic.selectDate(context, logic
                              .contractStartDateController),
                      name: 'start_date'.tr,
                      controller: logic
                          .contractStartDateController,
                      hintText: "enter_start_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_start_date'.tr,
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16.0),
              Expanded(
                child: Column(
                  crossAxisAlignment:
                  CrossAxisAlignment.start,
                  children: [
                    Text(
                      'contract_end_date'.tr,
                      style: const TextStyle(
                          fontSize: 16),
                    ),
                    const SizedBox(height: 5.0),
                    CustomFormBuilderTextField(
                      readOnly: true,
                      onTap: () =>
                          logic.selectDate(
                              context,
                              logic
                                  .contractEndDateController),
                      name: 'end_date'.tr,
                      controller:
                      logic.contractEndDateController,
                      hintText: "end_date".tr,
                      icon: Icons.calendar_month_outlined,
                      fillColor: secondaryColor,
                      errorText: 'enter_end_date'.tr,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      Text(
                        'previous_team'.tr,
                        style: const TextStyle(
                            fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'previous_team'.tr,
                        controller:
                        logic.previousTeamController,
                        hintText: "previous_team".tr,
                        icon: CupertinoIcons.person_3,
                        fillColor: secondaryColor,
                        errorText: 'enter_previous_team'
                            .tr,
                      ),
                    ],
                  )),
              const SizedBox(width: 16.0),
              Expanded(
                  child: Column(
                    crossAxisAlignment:
                    CrossAxisAlignment.start,
                    children: [
                      Text(
                        'salary'.tr,
                        style: const TextStyle(
                            fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'salary'.tr,
                        controller: logic
                            .salaryController,
                        hintText: "salary".tr,
                        icon: CupertinoIcons
                            .money_dollar_circle,
                        fillColor: secondaryColor,
                        errorText: 'enter_salary'.tr,
                      ),
                    ],
                  ))
            ],
          ),
          const SizedBox(height: 16.0),
          Row(
            children: [
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment
                        .start,
                    children: [
                      Text(
                        'matches_play'.tr,
                        style: const TextStyle(
                            fontSize: 16),
                      ),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'matches_play'.tr,
                        controller:
                        logic.matchesPlayedController,
                        hintText: "matches_play".tr,
                        icon: CupertinoIcons.list_bullet,
                        fillColor: secondaryColor,
                        errorText: 'enter_matches_play'
                            .tr,
                      ),
                    ],
                  )),
              const SizedBox(width: 16.0),
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment
                        .start,
                    children: [
                      Text("add_file".tr,
                        style: const TextStyle(
                            fontSize: 16),),
                      const SizedBox(height: 5.0),
                      CustomFormBuilderTextField(
                        name: 'add_file'.tr,
                        controller: logic
                            .fileBioController,
                        hintText: "add_file".tr,
                        icon: Icons.attachment_sharp,
                        fillColor: secondaryColor,
                        errorText: 'enter_file'.tr,
                        onTap: () {
                          logic.pickFile();
                        },
                      ),
                    ],
                  )
              ),
            ],
          ),
          const SizedBox(height: 24.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CancelButton(
                  btnName: 'cancel'.tr,
                  onPress: () {
                    logic.clearValueTextField();
                    homeController.updateWidgetHome(const ListPlayerScreen());
                  }),
              const SizedBox(width: 16.0),
              OKButton(
                btnName: widget.updatePlayer
                    ? 'update'.tr
                    : 'create'.tr,
                onPress: () async {
                  if (widget.updatePlayer) {
                    // print("---------previousPlayerID   : ${logic.previousPlayerID}");
                    // print("---------selectedPlayerCate   : ${logic.selectedPlayerCate}");
                    // print("---------heightController   : ${logic.heightController.text}");
                    // print("---------weightController   : ${logic.weightController.text}");
                    // print("---------selectPositionId   : ${logic.selectPositionId}");
                    // print("---------selectTeamId   : ${logic.selectTeamId}");
                    // print("---------jerseyNumberController   : ${logic.jerseyNumberController.text}");
                    // print("---------joinDateController   : ${logic.joinDateController.text}");
                    // print("---------contractStartDateController   : ${logic.contractStartDateController.text}");
                    // print("---------contractEndDateController   : ${logic.contractEndDateController.text}");
                    // print("---------previousTeamController   : ${logic.previousTeamController.text}");
                    // print("---------salaryController   : ${logic.salaryController.text}");
                    // print("---------matchesPlayedController   : ${logic.matchesPlayedController.text}");
                    // print("---------fileBioPlayer   : ${logic.fileBioPlayer}");
                    if (widget.playerModel != null &&
                        logic.previousPlayerID!.isNotEmpty &&
                        logic.selectedPlayerCate!.isNotEmpty &&
                        logic.heightController.text.isNotEmpty &&
                        logic.weightController.text.isNotEmpty &&
                        logic.selectPositionId!.isNotEmpty &&
                        logic.selectTeamId!.isNotEmpty &&
                        logic.joinDateController.text.isNotEmpty &&
                        logic.contractStartDateController.text.isNotEmpty &&
                        logic.contractEndDateController.text.isNotEmpty &&
                        logic.previousTeamController.text.isNotEmpty &&
                        logic.salaryController.text.isNotEmpty &&
                        logic.fileBioPlayer != null) {
                      logic.updatePlayerV2();
                    }
                  } else {
                    if (logic.selectedUserId!.isNotEmpty &&
                        logic.selectedPlayerCate!.isNotEmpty &&
                        logic.heightController.text.isNotEmpty &&
                        logic.weightController.text.isNotEmpty &&
                        logic.selectPositionId!.isNotEmpty &&
                        logic.selectTeamId!.isNotEmpty &&
                        logic.joinDateController.text.isNotEmpty &&
                        logic.contractStartDateController.text.isNotEmpty &&
                        logic.contractEndDateController.text.isNotEmpty &&
                        logic.previousTeamController.text.isNotEmpty &&
                        logic.salaryController.text.isNotEmpty &&
                        logic.fileBioPlayer != null) {
                      logic.createPlayerV2();
                    } else {
                      EasyLoading.showError(
                        "please_input_all_field",
                        duration: const Duration(
                            seconds: 2),
                      );
                    }
                  }
                },
              )
            ],
          ),
        ],
      );
    });
  }
}


