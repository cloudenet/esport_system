import 'package:esport_system/data/controllers/language_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../helper/confirmation_dialog.dart';

class LanguageSystemSetting extends StatefulWidget {
  final bool isLoginPage;
  final LanguageController languageController = Get.find();

  LanguageSystemSetting({super.key, required this.isLoginPage});

  @override
  State<LanguageSystemSetting> createState() => _LanguageSystemSettingState();
}

class _LanguageSystemSettingState extends State<LanguageSystemSetting> {
  var userController = Get.find<UserController>();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LanguageController>(
      builder: (controller) {
        return Padding(
          padding: const EdgeInsets.only(left: 0, right: 0),
          child: Container(
            padding: widget.isLoginPage ?  const EdgeInsets.all(0) : const EdgeInsets.all(defaultPadding),
            margin: const EdgeInsets.only(top: 10, bottom: 10),
            decoration:  BoxDecoration(
              color: widget.isLoginPage ? Colors.transparent :secondaryColor,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "choose_preferred_language".tr,
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w700,
                    color: widget.isLoginPage ? bgColor : Colors.white
                  ),
                ),
                SizedBox(
                  height: 150,
                  child: ListView.builder(
                    itemCount: widget.languageController.languages.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            widget.languageController.selectedIndexLang = index;
                          });
                          final selectedLanguage = widget.languageController.languages[index];
                          controller.changeLanguage(
                            selectedLanguage['code'],
                            selectedLanguage['countryCode'],
                          );
                        },
                        child: Container(
                          margin: const EdgeInsets.only(top: defaultPadding),
                          padding: const EdgeInsets.all(defaultPadding),
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 2,
                              color: widget.languageController.selectedIndexLang == index
                                  ? primaryColor.withOpacity(0.9)
                                  : bgColor.withOpacity(0.9),
                            ),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(defaultPadding),
                            ),
                          ),
                          child: Row(
                            children: [
                              SizedBox(
                                height: 20,
                                width: 20,
                                child: Icon(widget.languageController.languages[index]['icon'],color: widget.isLoginPage ? bgColor : Colors.white,),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.languageController.languages[index]['name'],
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: widget.isLoginPage ? bgColor : Colors.white
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              if (widget.languageController.selectedIndexLang == index)
                                SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: Icon(
                                    Icons.check,
                                    color: primaryColor.withOpacity(0.9),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 10.0),
                Row(
                  children: [
                    widget.isLoginPage
                    ? const SizedBox()
                    : ElevatedButton(
                      onPressed:() {
                        // Show logout confirmation dialog for the settings screen
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return ConfirmationDialog(
                              message: "are_you_sure_you_want_to_logout?".tr,
                              cancelText: 'cancel'.tr,
                              confirmText: 'confirm'.tr,
                              onCancel: () {
                                Get.back();
                              },
                              onConfirm: () {
                                userController.logOut();
                              },
                              backgroundColor: bgColor,
                            );
                          },
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                      child: Row(
                        children: [
                          const Icon(
                            Icons.logout,
                            color: Colors.white,
                          ),
                          const SizedBox(width: 20),
                          Text('logout'.tr,
                            style: const TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
