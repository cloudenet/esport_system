import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/setting_section/screen/setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';

class UpdateProfileScreen extends StatelessWidget {
  final UserModel? user;

  UpdateProfileScreen({super.key, this.user});

  final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey.withOpacity(0.2),
        appBar: AppBar(
          title: Text(
            "Profile".tr,
            style: const TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: GetBuilder<UserController>(builder: (logic) {
          logic.cardNameController.text = userInfo?.cardNumber ?? '';
          logic.nameController.text = userInfo?.name ?? '';
          logic.emailController.text = userInfo?.email ?? '';
          logic.nationalityController.text = userInfo?.nationality ?? '';
          logic.identityNoController.text = userInfo?.identityNo ?? '';
          logic.dayofbirthdayController.text = userInfo?.dob ?? '';
          logic.addressController.text = userInfo?.address ?? '';

          return Responsive(
            mobile: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 100,
                    color: Colors.green,
                  ),
                  Container(
                    width: double.infinity,
                    height: 100,
                    color: Colors.pink,
                  ),
                  Container(
                    width: double.infinity,
                    height: 100,
                    color: Colors.yellow,
                  )
                ],
              ),
            ),
            desktop: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Container(
                      width: double.infinity,
                      height: 350,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Expanded(
                                child: Container(
                                  color: Colors.white,
                              width: 300,
                              padding: const EdgeInsets.only(top: 16),
                              height: double.infinity,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Photo Profile".tr,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                  Text(
                                    "This image is show your profile".tr,
                                    style: const TextStyle(color: Colors.black54),
                                  )
                                ],
                              ),
                            )),
                          ),
                          Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.all(16),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.grey[100],
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Colors.black26,
                                        // Shadow color
                                        blurRadius: 6,
                                        offset: Offset(0, 0),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Align(
                                            alignment: Alignment.topLeft,
                                            child: userInfo!.photo ==
                                                    null // true
                                                ? Container(
                                                    width: 100,
                                                    height: 100,
                                                    decoration: BoxDecoration(
                                                      borderRadius: BorderRadius.circular(100),
                                                      color: Theme.of(context).primaryColor,
                                                    ),
                                                    child: Center(
                                                      child: Text(userInfo!.name![0].toString().toUpperCase()),
                                                    ),
                                                  )
                                                : logic.imageData != null
                                                    ? Container(
                                                        width: 100,
                                                        height: 100,
                                                        decoration: BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          image: DecorationImage(
                                                            image: MemoryImage(logic.imageData!),
                                                            // Corrected here
                                                            fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                      )
                                                    : Container(
                                                        width: 100,
                                                        height: 100,
                                                        decoration: BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          image: DecorationImage(
                                                            image: NetworkImage('${AppConstants.baseUrl}${userInfo?.photo}'),
                                                            fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                      ),
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 16),
                                            child: Container(
                                              height: 230,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () {
                                                      logic.pickImageFromFilePicker();
                                                    },
                                                    child: Container(
                                                      width: 70,
                                                      height: 70,
                                                      decoration: BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          border: Border.all(color: primaryColor, width: 2)),
                                                      child: const Icon(
                                                        Icons
                                                            .drive_folder_upload_outlined,
                                                        color: Colors.black54,
                                                        size: 50,
                                                      ),
                                                    ),
                                                  ),
                                                  Text(
                                                    "CLick to upload Image".tr,
                                                    style: const TextStyle(
                                                        color: Colors.black54,
                                                        fontSize: 20,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Padding(
                                          padding: const EdgeInsets.only(top: 25, right: 16),
                                          child: SizedBox(
                                            width: 700,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                ElevatedButton(
                                                    onPressed: () {
                                                      logic.imageData = null;
                                                      logic.update();
                                                    },
                                                    style: ElevatedButton.styleFrom(
                                                        shape: ContinuousRectangleBorder(borderRadius: BorderRadius.circular(5)),
                                                        backgroundColor: Colors.redAccent),
                                                    child: Text(
                                                      "Cancel".tr,
                                                      style: const TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                                const SizedBox(
                                                  width: 8,
                                                ),
                                                ElevatedButton(
                                                  onPressed: () async {
                                                    if (!logic.isImageSelected) {
                                                      EasyLoading.showInfo(
                                                        'Please select an image first'.tr,
                                                        duration: const Duration(seconds: 2),
                                                      );
                                                      return;
                                                    }

                                                    logic.isLoading = true;
                                                    logic.update();
                                                    EasyLoading.show(status: 'Loading...'.tr, dismissOnTap: true,);

                                                    bool success = await logic.updateProfileUser(
                                                      userId: userInfo!.id.toString(),
                                                      imageData: logic.imageData,
                                                    );
                                                    getMe();
                                                    EasyLoading.dismiss();
                                                    logic.isLoading = false;
                                                    logic.update();

                                                    if (success) {
                                                      EasyLoading.showSuccess('Profile updated successfully'.tr,
                                                        dismissOnTap: true,
                                                        duration: const Duration(seconds: 2),
                                                      );
                                                      Get.find<HomeController>().updateWidgetHome(UpdateProfileScreen());
                                                      await Future.delayed(const Duration(seconds: 2));
                                                      EasyLoading.dismiss();
                                                    } else {
                                                      EasyLoading.showError(
                                                        'Error'.tr,
                                                        duration: const Duration(seconds: 2),
                                                      );
                                                    }
                                                  },
                                                  style: ElevatedButton.styleFrom(
                                                    shape: ContinuousRectangleBorder(
                                                      borderRadius: BorderRadius.circular(5),
                                                    ),
                                                    backgroundColor: Colors.lightGreenAccent,
                                                  ),
                                                  child: Text(
                                                    "Update".tr,
                                                    style: const TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.bold,
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: Container(
                      width: double.infinity,
                      height: 300,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Expanded(
                                child: Container(
                              height: double.infinity,
                              padding: const EdgeInsets.only(top: 16),
                              width: 300,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Personal Information".tr,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                  Text(
                                    "Update your personal detail here".tr,
                                    style: const TextStyle(color: Colors.black54),
                                  )
                                ],
                              ),
                            )),
                          ),
                          Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 16, bottom: 16, right: 16),
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.grey[100],
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Colors.black26,
                                        // Shadow color
                                        blurRadius: 6,
                                        offset: Offset(0, 0),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(12),
                                        child: FormBuilder(
                                          key: _formKey,
                                          child: ResponsiveGridRow(
                                            children: [
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 8,
                                                          right: 4,
                                                          left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'Card number'.tr,
                                                    controller: logic
                                                        .cardNameController,
                                                    decoration: InputDecoration(
                                                        labelText:
                                                            'Card number'.tr),
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          bottom: 8,
                                                          right: 4,
                                                          left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'Name'.tr,
                                                    controller:
                                                        logic.nameController,
                                                    decoration: InputDecoration(
                                                        labelText: 'Name'.tr),
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding: const EdgeInsets.only(bottom: 8,right: 4,left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'Email'.tr,
                                                    controller: logic.emailController,
                                                    decoration: InputDecoration( labelText: 'Email'.tr),
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding: const EdgeInsets.only(bottom: 8,right: 4,left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'Address'.tr,
                                                    controller:logic.addressController,
                                                    decoration: InputDecoration( labelText: 'Address'.tr),
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: FormBuilderRadioGroup(
                                                  name: 'Gender'.tr,
                                                  initialValue: userInfo?.gender ??'male'.tr,
                                                  decoration: InputDecoration(labelText: 'Gender'.tr),
                                                  options: [
                                                    FormBuilderFieldOption(
                                                        value: 'male'.tr,
                                                        child: Text('Male'.tr)),
                                                    FormBuilderFieldOption(
                                                        value: 'female'.tr,
                                                        child: Text('Female'.tr)),
                                                  ],
                                                  onChanged: (selectedValue) {
                                                    logic.selectGender = selectedValue!;
                                                    logic.update();
                                                    print('Selected Gender: ${logic.selectGender}');
                                                  },
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding:const EdgeInsets.only(bottom: 8,right: 4,left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'Date of Birth'.tr,
                                                    controller: logic.dayofbirthdayController,
                                                    decoration: InputDecoration(labelText:'Date of Birth'.tr),
                                                    readOnly: true,
                                                    onTap: () {
                                                      logic.selectDate(context,logic.dayofbirthdayController);
                                                    },
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding:const EdgeInsets.only(bottom: 8,right: 4,left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'Nationality'.tr,
                                                    controller: logic.nationalityController,
                                                    decoration: InputDecoration(labelText:'Nationality'.tr),
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding:const EdgeInsets.only(bottom: 8,right: 4,left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'IdentityNo'.tr,
                                                    controller: logic.identityNoController,
                                                    decoration: InputDecoration(labelText: 'IdentityNo'.tr),
                                                  ),
                                                ),
                                              ),
                                              ResponsiveGridCol(
                                                md: 6,
                                                lg: 4,
                                                child: Padding(
                                                  padding: const EdgeInsets.only(bottom: 8, right: 4, left: 4),
                                                  child: FormBuilderTextField(
                                                    name: 'myIdentity'.tr,
                                                    readOnly: true,
                                                    onTap: () {
                                                      logic.pickImage();
                                                    },
                                                    controller: TextEditingController(text: logic.selectedImageId != null
                                                        ? logic.selectedImageId!.path
                                                        : 'No image selected'.tr,
                                                    ),
                                                    decoration: InputDecoration(labelText: 'Selected identity photo'.tr),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10, right: 16),
                                          child: SizedBox(
                                            width: 700,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                ElevatedButton(
                                                    onPressed: () {
                                                      Get.back();
                                                    },
                                                    style: ElevatedButton.styleFrom(
                                                        shape:
                                                            ContinuousRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5)),
                                                        backgroundColor:
                                                            Colors.redAccent),
                                                    child: Text(
                                                      "Cancel".tr,
                                                      style: const TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                                const SizedBox(
                                                  width: 8,
                                                ),
                                                ElevatedButton(
                                                    onPressed: () async {
                                                      logic.isLoading = true;
                                                      logic.update();
                                                      EasyLoading.show(
                                                        status: 'Loading...'.tr,
                                                        dismissOnTap: true,
                                                      );
                                                      bool success = await logic.updateUserInfo(
                                                          userId: userInfo!.id.toString(),
                                                          name: logic.nameController.text,
                                                          email: logic.emailController.text,
                                                          identityNo: logic.identityNoController.text,
                                                          nationality: logic.nationalityController.text,
                                                          gender: logic.selectGender.toString(),
                                                          dob: logic.dayofbirthdayController.text,
                                                          address: logic.addressController.text,
                                                          updatedBy: userInfo!.updatedBy.toString(),
                                                          cardNumber: logic.cardNameController.text,
                                                          identityImage: logic.selectedImageId);
                                                      getMe();
                                                      EasyLoading.dismiss();
                                                      logic.isLoading = false;
                                                      logic.update();
                                                      if (success) {
                                                        EasyLoading.showSuccess(
                                                          'Updated successfully'.tr,
                                                          dismissOnTap: true,
                                                          duration:
                                                              const Duration(
                                                                  seconds: 2),
                                                        );
                                                        Get.find<
                                                                HomeController>()
                                                            .updateWidgetHome(
                                                                const SettingScreen());
                                                        await Future.delayed(
                                                            const Duration(
                                                                seconds: 2));
                                                        EasyLoading.dismiss();
                                                      } else {
                                                        EasyLoading.showError(
                                                          'Error'.tr,
                                                          duration:
                                                              const Duration(
                                                                  seconds: 2),
                                                        );
                                                      }
                                                    },
                                                    style: ElevatedButton
                                                        .styleFrom(
                                                            shape:
                                                                ContinuousRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                            ),
                                                            backgroundColor: Colors
                                                                .lightGreenAccent),
                                                    child: Text(
                                                      "Update".tr,
                                                      style: const TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ))
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ))
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(right: 16, left: 16, bottom: 16),
                    child: Container(
                      width: double.infinity,
                      height: 180,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Expanded(
                                child: Container(
                              padding: const EdgeInsets.only(top: 10),
                              width: 200,
                              height: double.infinity,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Change Password".tr,
                                    style: const TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    "Enter your new password to update password".tr,
                                    style: const TextStyle(color: Colors.black54),
                                  ),
                                  const SizedBox(height: 10),
                                  Text(
                                    "New password should be more than 6 character".tr,
                                    style: TextStyle(
                                        color: Colors.red[200]),
                                  )
                                ],
                              ),
                            )),
                          ),
                          Expanded(
                              flex: 2,
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    top: 16, bottom: 16, right: 16),
                                child: Container(
                                  width: 200,
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.grey[100],
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Colors.black26,
                                        // Shadow color
                                        blurRadius: 6,
                                        offset: Offset(0, 0),
                                      ),
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(10),
                                        child: ResponsiveGridCol(
                                          md: 6,
                                          lg: 4,
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 8, right: 4, left: 4),
                                            child: FormBuilderTextField(
                                              name: 'New password'.tr,
                                              controller:
                                                  logic.newPasswordController,
                                              decoration: InputDecoration(
                                                labelText:
                                                    'New password should be have more than 6 character'.tr
                                                        .tr,
                                              ),
                                              validator: FormBuilderValidators
                                                  .compose([
                                                FormBuilderValidators
                                                    .required(),
                                                FormBuilderValidators.minLength(
                                                    3),
                                              ]),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 20, right: 16),
                                          child: SizedBox(
                                            width: 700,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                ElevatedButton(
                                                  onPressed: () {
                                                    logic.newPasswordController.clear();
                                                    logic.update();
                                                  },
                                                  style: ElevatedButton.styleFrom(
                                                    shape: ContinuousRectangleBorder(
                                                      borderRadius: BorderRadius.circular(5),
                                                    ),
                                                    backgroundColor: Colors.redAccent,
                                                  ),
                                                  child: Text(
                                                    "Cancel".tr,
                                                    style: const TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(width: 8),
                                                // ValueListenableBuilder<bool>(
                                                //   valueListenable: logic.isButtonEnabled,
                                                //   builder: (context, isEnabled,
                                                //       child) {
                                                //     return ElevatedButton(
                                                //       onPressed: isEnabled
                                                //           ? () async {
                                                //               if (_formKey.currentState !=
                                                //                       null &&
                                                //                   _formKey
                                                //                       .currentState!
                                                //                       .saveAndValidate()) {
                                                //                 logic.isLoading =
                                                //                     true;
                                                //                 logic.update();
                                                //                 EasyLoading
                                                //                     .show(
                                                //                   status:
                                                //                       'Loading...',
                                                //                   dismissOnTap:
                                                //                       true,
                                                //                 );
                                                //                 bool success =
                                                //                     await logic
                                                //                         .changeUserPassword(
                                                //                   userId: userInfo!
                                                //                       .id
                                                //                       .toString(),
                                                //                   newPassword: logic
                                                //                       .newPasswordController
                                                //                       .text,
                                                //                 );
                                                //                 EasyLoading
                                                //                     .dismiss();
                                                //                 logic.isLoading =
                                                //                     false;
                                                //                 logic.update();
                                                //                 if (success) {
                                                //                   logic
                                                //                       .newPasswordController
                                                //                       .clear();
                                                //
                                                //                   EasyLoading
                                                //                       .showSuccess(
                                                //                     'Password change successfully',
                                                //                     dismissOnTap:
                                                //                         true,
                                                //                     duration: const Duration(
                                                //                         seconds:
                                                //                             2),
                                                //                   );
                                                //                   Get.find<
                                                //                           HomeController>()
                                                //                       .updateWidgetHome(
                                                //                     UpdateProfileScreen(),
                                                //                   );
                                                //                   await Future
                                                //                       .delayed(
                                                //                     const Duration(
                                                //                         seconds:
                                                //                             2),
                                                //                   );
                                                //                   EasyLoading
                                                //                       .dismiss();
                                                //                 } else {
                                                //                   EasyLoading
                                                //                       .showError(
                                                //                     'Error',
                                                //                     duration: const Duration(
                                                //                         seconds:
                                                //                             2),
                                                //                   );
                                                //                 }
                                                //               }
                                                //             }
                                                //           : null,
                                                //       style: ElevatedButton
                                                //           .styleFrom(
                                                //         shape:
                                                //             ContinuousRectangleBorder(
                                                //           borderRadius:
                                                //               BorderRadius
                                                //                   .circular(5),
                                                //         ),
                                                //         backgroundColor: isEnabled
                                                //             ? Colors
                                                //                 .lightGreenAccent
                                                //             : Colors.grey,
                                                //       ),
                                                //       child: const Text(
                                                //         "Update",
                                                //         style: TextStyle(
                                                //           color: Colors.white,
                                                //           fontWeight:
                                                //               FontWeight.bold,
                                                //         ),
                                                //       ),
                                                //     );
                                                //   },
                                                // ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }));
  }
}
