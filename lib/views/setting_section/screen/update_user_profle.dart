import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/auth_section/app_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import '../../../helper/app_contrain.dart';
import '../../../helper/images.dart';


class UpdateUserProfile extends StatelessWidget {
  const UpdateUserProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: Column(
                children: [
                  _buildProfileHeader(),
                  const SizedBox(height: 24),
                  _buildUserInfo(),
                  const SizedBox(height: 34),
                ],
              )
          )
        ],
      ),
    );
  }

  Widget _buildProfileHeader() {
    return GetBuilder<UserController>(
      builder: (controller) {
        return Column(
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                ClipOval(
                  child: controller.imageDataX != null ?
                  Image.memory(
                    controller.imageDataX!,
                    width: 150,
                    height: 150,
                    fit: BoxFit.cover,
                  ):
                  Image.network(
                    "${AppConstants.baseUrl}${userInfo!.photo}",
                    width: 150,
                    height: 150,
                    fit: BoxFit.cover,
                    errorBuilder: (context, error, stackTrace) {
                      return ClipOval(
                        child: Image.asset(
                          Images.noImage,
                          height: 150,
                          width: 150,
                          fit: BoxFit.cover,
                        ),
                      );
                    },
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) {
                        return child;
                      } else {
                        return Center(
                          child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                (loadingProgress.expectedTotalBytes ?? 1)
                                : null,
                          ),
                        );
                      }
                    },
                  ),
                ),
                Positioned(
                    right: 0,
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.zero,
                      decoration: const BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle
                      ),
                      child: IconButton(
                          onPressed: (){
                            controller.pickImageX();
                          },
                          icon: const Icon(Icons.camera_alt_outlined,size: 25,)
                      ),
                    )
                )
              ],
            ),
            const SizedBox(height: 16),
            Text(
              userInfo!.name!.isNotEmpty ? userInfo!.name.toString() : "N/A",
              style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 4),
            Text(
              userInfo?.username.toString() ?? "N/A",
              style: TextStyle(fontSize: 16, color: Colors.grey.shade600),
            ),
            const SizedBox(height: 8),
            SizedBox(
              width: 150,
              child: TextButton(
                onPressed: () async {
                  if(controller.imageDataX !=null){
                    controller.changeProfile();
                  }else{
                    EasyLoading.showError("please_select_new_image".tr);
                  }
                },
                style: TextButton.styleFrom(
                  padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 16.0),
                  backgroundColor: AppColors.mainBlueColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Text(
                    'save_change'.tr,
                    style: ralewayStyle.copyWith(
                      fontWeight: FontWeight.w700,
                      color: AppColors.whiteColor,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ),
            ),

          ],
        );
      },
    );
  }

  Widget _buildUserInfo() {
    return Card(
      color: bgColor,
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            _buildInfoRow(Icons.email, 'email'.tr,userInfo?.email != "null" ? userInfo!.email.toString() : "N/A"),
            const Divider(),
            _buildInfoRow(
                Icons.location_city, 'address'.tr, userInfo?.address != "null" ? userInfo!.address.toString() : "N/A"),
            const Divider(),
            _buildInfoRow(Icons.account_balance_rounded, 'nationality'.tr,
                userInfo?.nationality != "null" ? userInfo!.nationality.toString() : "N/A"),
            const Divider(),
            _buildInfoRow(Icons.perm_identity_outlined, 'identity_no'.tr,
                userInfo?.identityNo != "null" ? userInfo!.identityNo.toString() : "N/A"),
            const Divider(),
            _buildInfoRow(
                Icons.phone, 'phone'.tr, '+855 ${userInfo?.phone != "null" ? userInfo!.phone.toString() : "N/A"}'),
            const Divider(),
            _buildInfoRow(Icons.credit_card_sharp, 'card_number'.tr,
                userInfo?.cardNumber != "null" ? userInfo!.cardNumber.toString() : "N/A"),
            const Divider(),
            _buildInfoRow(Icons.date_range_sharp, 'date_of_birth'.tr,
                userInfo?.dob != "null" ? userInfo!.dob.toString() : "N/A"),
            const Divider(),
            _buildInfoRow(
                CupertinoIcons.option, 'role'.tr, userInfo?.role != "null" ? userInfo!.role.toString() : "N/A"),
          ],
        ),
      ),
    );
  }

  Widget _buildInfoRow(IconData icon, String label, String value) {
    return Row(
      children: [
        Icon(icon, color: Colors.blue),
        const SizedBox(width: 16),
        Text(
          label,
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        const SizedBox(width: 8),
        Text(value),
      ],
    );
  }
}

