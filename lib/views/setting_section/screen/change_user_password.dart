import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/auth_section/app_icons.dart';
import 'package:esport_system/views/auth_section/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

class ChangeUserPassword extends StatefulWidget {
  const ChangeUserPassword({super.key});

  @override
  State<ChangeUserPassword> createState() => _ChangeUserPasswordState();
}

class _ChangeUserPasswordState extends State<ChangeUserPassword> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _obscureText = true;
  bool _obscurePText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _toggleP() {
    setState(() {
      _obscurePText = !_obscurePText;
    });
  }


  @override
  Widget build(BuildContext context) {
    var logic = Get.find<UserController>();
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    return FormBuilder(
      key: _formKey,
      child:Padding(
        padding: const EdgeInsets.only(
            left: 8, right: 8
        ),
        child: Container(
          padding: const EdgeInsets.all(defaultPadding),
          margin: const EdgeInsets.only(
              top: 10,
              bottom: 10
          ),
          decoration: const BoxDecoration(
            color: secondaryColor,
            borderRadius:  BorderRadius.all(
                Radius.circular(10)
            ),
          ),
          child:  Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "change_password".tr,
                style: const TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text("enter_your_new_password".tr),
              Text("new_password_should_be_then_6_character.".tr,
                style: const TextStyle(
                    color: Colors.red
                ),
              ),
              SizedBox(height: height * 0.014),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('password'.tr,
                  style: const TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

              const SizedBox(height: 6.0),
              FormBuilderTextField(
                name: "password".tr,
                obscureText: _obscureText,
                controller: logic.newPasswordController,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
                onChanged: (length){
                  if(length!.length > 5){
                    setState(() {
                      logic.isAllow = true ;
                    });
                  }else{
                    setState(() {
                      logic.isAllow = false ;
                    });
                  }
                },
                decoration: InputDecoration(
                  hintText: "enter_password".tr,
                  fillColor: bgColor,
                  filled: true,
                  border: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius:  BorderRadius.all(
                        Radius.circular(10)
                    ),
                  ),
                  suffixIcon: InkWell(
                    onTap: _toggle,
                    child: Container(
                      padding: const EdgeInsets.all(defaultPadding * 0.75),
                      margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
                      decoration: const BoxDecoration(
                        borderRadius:  BorderRadius.all(
                            Radius.circular(10)
                        ),
                      ),
                      // child: SvgPicture.asset("assets/icons/Search.svg"),
                      child: Image.asset(AppIcons.eyeIcon),
                    ),
                  ),
                ),
              ),
              SizedBox(height: height * 0.014),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('confirm_password'.tr,
                  style: const TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              const SizedBox(height: 6.0),
              FormBuilderTextField(
                controller: logic.cfmPasswordController,
                name: "confirm_password".tr,
                obscureText: _obscurePText,
                onChanged: (length){
                  if(length!.length > 5){
                    setState(() {
                      logic.isAllow = true ;
                    });
                  }else{
                    setState(() {
                      logic.isAllow = false ;
                    });
                  }
                },
                decoration: InputDecoration(
                  hintText: "enter_confirm_password".tr,
                  fillColor: bgColor,
                  filled: true,
                  border: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius:  BorderRadius.all(
                        Radius.circular(10)
                    ),
                  ),
                  suffixIcon: InkWell(
                    onTap: _toggleP,
                    child: Container(
                      padding: const EdgeInsets.all(defaultPadding * 0.75),
                      margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
                      decoration: const BoxDecoration(
                        borderRadius:  BorderRadius.all(
                            Radius.circular(10)
                        ),
                      ),
                      // child: SvgPicture.asset("assets/icons/Search.svg"),
                      child: Image.asset(AppIcons.eyeIcon),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 15),
                    child: TextButton(
                      onPressed: () {
                        logic.newPasswordController.clear();
                        logic.cfmPasswordController.clear();
                        logic.update();
                      },
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 16.0
                        ),
                        backgroundColor: AppColors.redColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      child: Text(
                        'cancel'.tr,
                        style: ralewayStyle.copyWith(
                          fontWeight: FontWeight.w700,
                          color: AppColors.whiteColor,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: logic.isAllow ? () async {
                      if(logic.newPasswordController.text == logic.cfmPasswordController.text ){
                        await logic.changeUserPassword(
                          userId: userInfo!.id.toString(),
                          newPassword: logic.newPasswordController.text,
                        );
                      }else{
                        EasyLoading.showError('the_password_not_match'.tr, duration: const Duration(seconds: 2));
                      }

                    } : null,
                    style: TextButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 35.0,
                          vertical: 16.0
                      ),
                      backgroundColor: logic.isAllow? AppColors.mainBlueColor : Colors.grey,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0),
                      ),
                    ),
                    child: Text(
                      'change'.tr,
                      style: ralewayStyle.copyWith(
                        fontWeight: FontWeight.w700,
                        color: AppColors.whiteColor,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }
}
