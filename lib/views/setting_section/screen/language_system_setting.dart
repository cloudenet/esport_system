import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LanguageSystemSetting extends StatefulWidget {
  const LanguageSystemSetting({super.key});

  @override
  State<LanguageSystemSetting> createState() => _LanguageSystemSettingState();
}

class _LanguageSystemSettingState extends State<LanguageSystemSetting> {
  List<Map<String, dynamic>> languages = [
    {"name": "English", "icon": Icons.language},
    {"name": "Khmer", "icon": Icons.language},
  ];

  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: 8, right: 8
      ),
      child: Container(
        padding: const EdgeInsets.all(
            defaultPadding
        ),
        margin: const EdgeInsets.only(
            top: 10,
            bottom: 10
        ),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius:  BorderRadius.all(
              Radius.circular(10)
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Choose Preferred Language".tr,
              style: const TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 150,
              child: ListView.builder(
                itemCount: languages.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        selectedIndex = index;
                        print("$selectedIndex");
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.only(top: defaultPadding),
                      padding: const EdgeInsets.all(defaultPadding),
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2,
                          color: selectedIndex == index ? primaryColor.withOpacity(0.9) : bgColor.withOpacity(0.9),
                        ),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(defaultPadding),
                        ),
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            height: 20,
                            width: 20,
                            child: Icon(languages[index]['icon']),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    languages[index]['name'],
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          if (selectedIndex == index)
                            SizedBox(
                              height: 20,
                              width: 20,
                              child: Icon(
                                Icons.check,
                                color: primaryColor.withOpacity(0.9)
                              ),
                            ),
                        ],
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
