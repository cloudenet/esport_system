import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/setting_section/screen/change_user_password.dart';
import 'package:esport_system/views/setting_section/screen/update_personal_info.dart';
import 'package:esport_system/views/setting_section/screen/update_user_profle.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';

class AccountSystemSettings extends StatelessWidget {
  const AccountSystemSettings({super.key});

  @override
  Widget build(BuildContext context) {

    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 15
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'account_system'.tr,
                style: const TextStyle(
                  fontWeight: FontWeight.w600,
                  color: AppColors.whiteColor,
                  fontSize: 20.0,
                ),
              ),
              const SizedBox(height: 10.0),
              ResponsiveGridRow(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ResponsiveGridCol(
                    md: 12,
                    lg: 12,
                    xl: 6,
                    child: const UpdateUserProfile(),
                  ),
                  ResponsiveGridCol(
                    md: 12,
                    lg: 12,
                    xl: 6,
                    child: const ChangeUserPassword(),
                  )
                ],
              ),
              const SizedBox(height: 2.0),
              const UpdatePersonalInfo(),
            ],
          ),
        ),
      ),
    );
  }
}
