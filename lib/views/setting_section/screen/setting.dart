import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/setting_section/screen/account_system_settings.dart';
import 'package:esport_system/views/setting_section/screen/general_system_setting.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../helper/image_picker_field.dart';

// class SettingScreen extends StatefulWidget {
//   const SettingScreen({super.key});
//
//   @override
//   State<SettingScreen> createState() => _SettingScreenState();
// }
//
// class _SettingScreenState extends State<SettingScreen> with SingleTickerProviderStateMixin{
//   late TabController _tabController;
//
//   @override
//   void initState() {
//     _tabController = TabController(length: 4, vsync: this);
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     _tabController.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Responsive(
//           desktop: SingleChildScrollView(
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Padding(
//                   padding: const EdgeInsets.symmetric(
//                       horizontal: 20,
//                       vertical: 15
//                   ),
//                   child: Text(
//                     "setting_system".tr,
//                     style: const TextStyle(
//                       fontWeight: FontWeight.w600,
//                       color: AppColors.whiteColor,
//                       fontSize: 20.0,
//                     ),
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.symmetric(
//                       horizontal: 20,
//                       vertical: 15
//                   ),
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       SizedBox(
//                         width: 800,
//                         child: Card(
//                           color: secondaryColor,
//                           child: Padding(
//                             padding: const EdgeInsets.all(8.0),
//                             child: TabBar(
//                               controller: _tabController,
//                               tabs: [
//                                 Tab(text: 'general_system'.tr),
//                                 Tab(text: 'account_system'.tr),
//                               ],
//                               indicator: BoxDecoration(
//                                 color: Colors.blue, // Highlight color
//                                 borderRadius: BorderRadius.circular(8),
//                               ),
//                               indicatorSize: TabBarIndicatorSize.tab,
//                               labelColor: Colors.white,
//                               unselectedLabelColor: Colors.white,
//                               dividerColor: Colors.transparent
//                             ),
//                           ),
//                         )
//                       ),
//                       SizedBox(
//                         height: MediaQuery.of(context).size.height,
//                         child: Card(
//                           color: secondaryColor,
//                           child: TabBarView(
//                             controller: _tabController,
//                             children:  [
//                               LanguageSystemSetting(),
//                               const AccountSystemSettings(),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//           mobile: SingleChildScrollView(
//             child: Container(
//               height: 100,
//               color: Colors.cyan,
//             ),
//           ),
//         ),
//       // ),
//     );
//   }
// }
//
// class ProfileMenuWidget extends StatelessWidget {
//   const ProfileMenuWidget({
//     super.key,
//     required this.title,
//     required this.icon,
//     required this.onPress,
//     this.endIcon = true,
//     this.textColor,
//   });
//
//   final String title;
//   final IconData icon;
//   final VoidCallback onPress;
//   final bool endIcon;
//   final Color? textColor;
//
//   @override
//   Widget build(BuildContext context) {
//     var iconColor = Colors.blue;
//
//     return ListTile(
//       onTap: onPress,
//       leading: Container(
//         width: 40,
//         height: 40,
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.circular(100),
//           color: iconColor.withOpacity(0.1),
//         ),
//         child: Icon(icon, color: iconColor),
//       ),
//       title: Text(title,style: TextStyle(color: textColor),),
//       trailing: endIcon
//           ? Container(
//           width: 30,
//           height: 30,
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(100),
//             color: Colors.grey.withOpacity(0.1),
//           ),
//           child: const Icon(Icons.arrow_forward_ios,
//               size: 18.0, color: Colors.grey))
//           : null,
//     );
//   }
// }

class SettingScreen extends StatefulWidget {
  const SettingScreen({super.key});

  @override
  State<SettingScreen> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth > 600) {
            return _buildDesktopView();
          } else {
            print("---------->Mobile");
            return _buildMobileView();
          }
        },
      ),
    );
  }

  Widget _buildDesktopView() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "setting_system".tr,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: AppColors.whiteColor,
                fontSize: 20.0,
              ),
            ),
            const SizedBox(height: 15),
            Card(
              color: secondaryColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TabBar(
                  controller: _tabController,
                  tabs: [
                    Tab(text: 'general_system'.tr),
                    Tab(text: 'account_system'.tr),
                  ],
                  indicator: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.white,
                  dividerColor: Colors.transparent,
                ),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: MediaQuery.of(context).size.height - 200,
              child: Card(
                color: secondaryColor,
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    LanguageSystemSetting(isLoginPage: false,),
                    const AccountSystemSettings(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMobileView() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "setting_system".tr,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: AppColors.whiteColor,
                fontSize: 18.0,
              ),
            ),
            const SizedBox(height: 10),
            Card(
              color: secondaryColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: TabBar(
                  controller: _tabController,
                  tabs: [
                    Tab(text: 'general_system'.tr),
                    Tab(text: 'account_system'.tr),
                  ],
                  indicator: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  indicatorSize: TabBarIndicatorSize.tab,
                  labelColor: Colors.white,
                  unselectedLabelColor: Colors.white,
                  dividerColor: Colors.transparent,
                ),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              height: MediaQuery.of(context).size.height - 150,
              child: Card(
                color: secondaryColor,
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    LanguageSystemSetting(isLoginPage: false,),
                    const AccountSystemSettings(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}