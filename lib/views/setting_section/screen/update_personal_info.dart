import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/auth_section/app_styles.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:esport_system/helper/constants.dart';

import '../../../data/model/address_model.dart';
import '../../../helper/custom_textformfield.dart';
import '../../../helper/location_selector.dart';

class UpdatePersonalInfo extends StatefulWidget {
  const UpdatePersonalInfo({super.key});

  @override
  State<UpdatePersonalInfo> createState() => _UpdatePersonalInfoState();
}

class _UpdatePersonalInfoState extends State<UpdatePersonalInfo> {
  String? selectedGender;
  var u = Get.find<UserController>();
  final _formKey = GlobalKey<FormBuilderState>();


  @override
  void initState() {
    u.fetchProvinces();
    u.clearTextField();
    setPreviousValue();
    super.initState();
  }
  void setPreviousValue(){
    UserModel? data = userInfo ;
    u.cardNameController.text = data?.cardNumber ?? "N/A";
    u.nameController.text = data?.name ?? "N/A";
    u.khmerNameController.text = data?.name ?? "N/A";
    u.emailController.text = data?.email ?? "N/A";
    u.identityNoController.text = data?.identityNo ?? "N/A";
    u.nationalityController.text = data?.nationality ?? "N/A";
    u.selectGender = data?.gender ?? "N/A";
    u.dayofbirthdayController.text = data?.dob ?? "N/A";
    u.addressController.text = data?.address ?? "N/A";
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(defaultPadding),
        decoration: const BoxDecoration(
          color: secondaryColor,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Responsive(
          mobile: bodyMobile(),
          tablet: bodyMobile(),
          desktop: bodyDesktop(),
        )
    );
  }

  Widget bodyDesktop(){
    double height = MediaQuery.of(context).size.height;
    return GetBuilder<UserController>(
        builder: (logic) {
          return FormBuilder(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("personal_information".tr,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text("update_your_personal_detail_here".tr),
                SizedBox(height: height * 0.014),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('card_number'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['Card Number']?.errorText,
                            name: 'card_number'.tr,
                            controller: logic.cardNameController,
                            hintText: 'enter_card_number'.tr,
                            icon: Icons.credit_card,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('name'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['name'.tr]?.errorText,
                            name: 'name'.tr,
                            controller: logic.nameController,
                            hintText: 'enter_name'.tr,
                            icon: Icons.person,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('khmer_name'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['khmer_name'.tr]?.errorText,
                            name: 'khmer_name',
                            controller: logic.khmerNameController,
                            hintText: 'enter_khmer_name'.tr,
                            icon: Icons.person,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('email'.tr, style: TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['email'.tr]?.errorText,
                            name: 'email'.tr,
                            controller: logic.emailController,
                            hintText: 'enter_email'.tr,
                            icon: Icons.email,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('identity_number'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields[' identity_number'.tr]?.errorText,
                            name: 'identity_number'.tr,
                            controller: logic.identityNoController,
                            hintText: 'enter_identity_number'.tr,
                            icon: Icons.credit_card,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('nationality'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['nationality'.tr]?.errorText,
                            name: 'nationality'.tr,
                            controller: logic.nationalityController,
                            hintText: 'enter_nationality'.tr,
                            icon: Icons.person,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('gender'.tr, style: const TextStyle(fontSize: 16)),
                          FormBuilderDropdown(
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10),
                            ),
                            name: 'gender'.tr,
                            initialValue: logic.selectGender, //// Set initial value here
                            decoration: InputDecoration(
                              hintText: 'gender'.tr,
                              fillColor: bgColor,
                              filled: true,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                            ),
                            validator: FormBuilderValidators.required(),
                            items: [
                              DropdownMenuItem(
                                value: 'male',
                                child: Text('male'.tr),
                              ),
                              DropdownMenuItem(
                                value: 'female',
                                child: Text('female'.tr),
                              ),

                            ],
                            onChanged: (selectedValue) {
                              if (selectedValue != null) {
                                logic.selectGender = selectedValue;// Ensure UI update if needed
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('date_of_birth'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            onTap: (){
                              logic.selectDate(context, logic.dayofbirthdayController,);
                            },
                            errorText: _formKey.currentState?.fields['date_of_birth'.tr]?.errorText,
                            name: 'date_of_birth'.tr,
                            controller: logic.dayofbirthdayController,
                            hintText: 'date_of_birth'.tr,
                            icon: Icons.credit_card,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('address'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['Address']?.errorText,
                            name: 'address'.tr,
                            controller: logic.addressController,
                            hintText: 'enter_address'.tr,
                            icon: Icons.person,
                            fillColor: bgColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('province'.tr,
                              style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'province'.tr,
                            logic.provinces,
                            logic.selectedProvince?.name ?? 'select_province'.tr,
                            logic.selectProvince,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10), // Spacing between the two fields
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('district'.tr,
                              style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'district'.tr,
                            logic.districts,
                            logic.selectedDistrict?.name ?? 'select_district'.tr,
                            logic.selectDistrict,
                            enabled: logic.selectedProvince != null,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('commune'.tr, style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'commune'.tr,
                            logic.communes,
                            logic.selectedCommune?.name ?? 'select_commune'.tr,
                            logic.selectCommune,
                            enabled: logic.selectedDistrict != null,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('village'.tr, style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'village'.tr,
                            logic.villages,
                            logic.selectedVillage?.name ?? 'select_village'.tr,
                            logic.selectVillage,
                            enabled: logic.selectedCommune != null,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if(1==2)
                    Container(
                      margin: const EdgeInsets.only(right: 15),
                      child: TextButton(
                        onPressed: () {

                        },
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 35.0,
                              vertical: 16.0
                          ),
                          backgroundColor: AppColors.redColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        child: Text(
                          'cancel'.tr,
                          style: ralewayStyle.copyWith(
                            fontWeight: FontWeight.w700,
                            color: AppColors.whiteColor,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        if(_formKey.currentState != null &&
                            _formKey.currentState!.saveAndValidate()){
                          logic.updateMySelf();
                        }
                      },
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 16.0
                        ),
                        backgroundColor: AppColors.mainBlueColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      child: Text(
                        'change'.tr,
                        style: ralewayStyle.copyWith(
                          fontWeight: FontWeight.w700,
                          color: AppColors.whiteColor,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),

              ],
            ),
          );
        },
    );
  }

  Widget bodyMobile(){
    double height = MediaQuery.of(context).size.height;
    return GetBuilder<UserController>(
        builder: (logic) {
          return FormBuilder(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("personal_information".tr,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text("update_your_personal_detail_here".tr),
                SizedBox(height: height * 0.014),
                Text('card_number'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields['Card Number']?.errorText,
                  name: 'card_number'.tr,
                  controller: logic.cardNameController,
                  hintText: 'enter_card_number'.tr,
                  icon: Icons.credit_card,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('name'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields['name'.tr]?.errorText,
                  name: 'name'.tr,
                  controller: logic.nameController,
                  hintText: 'enter_name'.tr,
                  icon: Icons.person,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('khmer_name'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields['khmer_name'.tr]?.errorText,
                  name: 'khmer_name',
                  controller: logic.khmerNameController,
                  hintText: 'enter_khmer_name'.tr,
                  icon: Icons.person,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('email'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields['email'.tr]?.errorText,
                  name: 'email'.tr,
                  controller: logic.emailController,
                  hintText: 'enter_email'.tr,
                  icon: Icons.email,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('identity_number'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields[' identity_number'.tr]?.errorText,
                  name: 'identity_number'.tr,
                  controller: logic.identityNoController,
                  hintText: 'enter_identity_number'.tr,
                  icon: Icons.credit_card,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('nationality'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields['nationality'.tr]?.errorText,
                  name: 'nationality'.tr,
                  controller: logic.nationalityController,
                  hintText: 'enter_nationality'.tr,
                  icon: Icons.person,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('gender'.tr, style: const TextStyle(fontSize: 16)),
                FormBuilderDropdown(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10),
                  ),
                  name: 'gender'.tr,
                  initialValue: logic.selectGender, //// Set initial value here
                  decoration: InputDecoration(
                    hintText: 'gender'.tr,
                    fillColor: bgColor,
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                  ),
                  validator: FormBuilderValidators.required(),
                  items: [
                    DropdownMenuItem(
                      value: 'male',
                      child: Text('male'.tr),
                    ),
                    DropdownMenuItem(
                      value: 'female',
                      child: Text('female'.tr),
                    ),

                  ],
                  onChanged: (selectedValue) {
                    if (selectedValue != null) {
                      logic.selectGender = selectedValue;// Ensure UI update if needed
                    }
                  },
                ),
                const SizedBox(height: 10.0),
                Text('date_of_birth'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  onTap: (){
                    logic.selectDate(context, logic.dayofbirthdayController,);
                  },
                  errorText: _formKey.currentState?.fields['date_of_birth'.tr]?.errorText,
                  name: 'date_of_birth'.tr,
                  controller: logic.dayofbirthdayController,
                  hintText: 'date_of_birth'.tr,
                  icon: Icons.credit_card,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('address'.tr, style: const TextStyle(fontSize: 16)),
                CustomFormBuilderTextField(
                  errorText: _formKey.currentState?.fields['Address']?.errorText,
                  name: 'address'.tr,
                  controller: logic.addressController,
                  hintText: 'enter_address'.tr,
                  icon: Icons.person,
                  fillColor: bgColor,
                ),
                const SizedBox(height: 10.0),
                Text('province'.tr, style: const TextStyle(fontSize: 16)),
                _buildLocationSelector(
                  icon: Icons.location_on, context,
                  'province'.tr,
                  logic.provinces,
                  logic.selectedProvince?.name ?? 'select_province'.tr,
                  logic.selectProvince,
                ),
                const SizedBox(height: 10.0),
                Text('district'.tr,
                    style: const TextStyle(fontSize: 16)),
                _buildLocationSelector(
                  icon: Icons.location_on,
                  context,
                  'district'.tr,
                  logic.districts,
                  logic.selectedDistrict?.name ?? 'select_district'.tr,
                  logic.selectDistrict,
                  enabled: logic.selectedProvince != null,
                ),
                const SizedBox(height: 10.0),
                Text('commune'.tr, style: const TextStyle(fontSize: 16)),
                _buildLocationSelector(
                  icon: Icons.location_on,
                  context,
                  'commune'.tr,
                  logic.communes,
                  logic.selectedCommune?.name ?? 'select_commune'.tr,
                  logic.selectCommune,
                  enabled: logic.selectedDistrict != null,
                ),
                const SizedBox(height: 10.0),
                Text('village'.tr, style: const TextStyle(fontSize: 16)),
                _buildLocationSelector(
                  icon: Icons.location_on,
                  context,
                  'village'.tr,
                  logic.villages,
                  logic.selectedVillage?.name ?? 'select_village'.tr,
                  logic.selectVillage,
                  enabled: logic.selectedCommune != null,
                ),
                const SizedBox(height: 20.0),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if(1==2)
                    Container(
                      margin: const EdgeInsets.only(right: 15),
                      child: TextButton(
                        onPressed: () {
                          Get.back();
                        },
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 35.0,
                              vertical: 16.0
                          ),
                          backgroundColor: AppColors.redColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        child: Text(
                          'cancel'.tr,
                          style: ralewayStyle.copyWith(
                            fontWeight: FontWeight.w700,
                            color: AppColors.whiteColor,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () {
                        if(_formKey.currentState != null &&
                            _formKey.currentState!.saveAndValidate()){
                          logic.updateMySelf();
                        }
                      },
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 35.0,
                            vertical: 16.0
                        ),
                        backgroundColor: AppColors.mainBlueColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      child: Text(
                        'change'.tr,
                        style: ralewayStyle.copyWith(
                          fontWeight: FontWeight.w700,
                          color: AppColors.whiteColor,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),

              ],
            ),
          );
        },
    );
  }

  Widget _buildLocationSelector
      (BuildContext context,
      String label,
      List<Address> data,
      String currentValue,
      Function(Address) onSelect,
      {bool enabled = true, IconData? icon}) {
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () => _showSelectionDialog(
          context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: bgColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
    kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding:
          const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
