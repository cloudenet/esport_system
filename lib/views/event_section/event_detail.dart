import 'package:esport_system/data/model/event_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/event_section/list_event.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class EventDetail extends StatefulWidget {
  final EventModel eventModel;
  const EventDetail({super.key, required this.eventModel});

  @override
  State<EventDetail> createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  var homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Responsive(
      tablet: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                width: 150,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: Image.asset(
                  'assets/icons/calendar.png',
                  color: Colors.white,
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('event_detail'.tr,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 24),
                      buildText(
                        title: 'title'.tr,
                        textData: widget.eventModel.title??"N/A",
                        height: 80,
                        padding: 10,
                      ),
                      const SizedBox(height: 16),
                      Row(
                        children: [
                          Expanded(
                            child: buildText(
                              title: 'start_date'.tr,
                              textData: formatDate(widget.eventModel.startDate??"N/A"),
                              height: 50,
                              padding: 10,
                              alignment: Alignment.center,
                            ),
                          ),
                          const SizedBox(width: 16,),
                          Expanded(
                            child: buildText(
                              title: 'end_date'.tr,
                              textData: formatDate(widget.eventModel.endDate??"N/A"),
                              height: 50,
                              padding: 10,
                              alignment: Alignment.center,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16,),
                      buildText(
                          title: 'description'.tr,
                          textData: widget.eventModel.description??"N/A",
                          padding: 10,
                          height: 80),
                      const SizedBox(height: 16,),
                      buildText(
                          title: 'address'.tr,
                          textData: widget.eventModel.address??"N/A",
                          padding: 10,
                          alignment: Alignment.centerLeft,
                          height: 50),
                      const SizedBox(
                        height: 24,
                      ),
                      const SizedBox(height: 24),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(const ListEvent());
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      mobile: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          color: bgColor,
        ),
        child: SingleChildScrollView(
          child: Container(
            decoration: const BoxDecoration(
              color: secondaryColor,
            ),
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('event_detail'.tr,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 24),
                buildText(
                  title: 'title'.tr,
                  textData: widget.eventModel.title??"N/A",
                  height: 80,
                  padding: 10,
                ),
                const SizedBox(height: 16),
                Row(
                  children: [
                    Expanded(
                      child: buildText(
                        title: 'start_date'.tr,
                        textData: formatDate(widget.eventModel.startDate??"N/A"),
                        height: 50,
                        padding: 10,
                        fontSizeText: 12,
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                    const SizedBox(width: 16,),
                    Expanded(
                      child: buildText(
                        title: 'end_date'.tr,
                        textData: formatDate(widget.eventModel.endDate??"N/A"),
                        height: 50,
                        padding: 10,
                        fontSizeText: 12,
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16,),
                buildText(
                    title: 'description'.tr,
                    textData: widget.eventModel.description??"N/A",
                    padding: 10,
                    height: 80),
                const SizedBox(height: 16,),
                buildText(
                    title: 'address'.tr,
                    textData: widget.eventModel.address??"N/A",
                    padding: 10,
                    height: 80),
                const SizedBox(
                  height: 24,
                ),
                const SizedBox(height: 24),
                Align(
                  alignment: Alignment.bottomRight,
                  child: CancelButton(
                    btnName: 'close'.tr,
                    onPress: () {
                      homeController.updateWidgetHome(const ListEvent());
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      desktop: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                width: 150,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: Image.asset(
                  'assets/icons/calendar.png',
                  color: Colors.white,
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('event_detail'.tr,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 24),
                      buildText(
                        title: 'title'.tr,
                        textData: widget.eventModel.title??"N/A",
                        height: 80,
                        padding: 10,
                      ),
                      const SizedBox(height: 16),
                      Row(
                        children: [
                          Expanded(
                            child: buildText(
                              title: 'start_date'.tr,
                              textData: formatDate(widget.eventModel.startDate??"N/A"),
                              height: 50,
                              padding: 10,
                              alignment: Alignment.center,
                            ),
                          ),
                          const SizedBox(width: 16,),
                          Expanded(
                            child: buildText(
                              title: 'end_date'.tr,
                              textData: formatDate(widget.eventModel.endDate??"N/A"),
                              height: 50,
                              padding: 10,
                              alignment: Alignment.center,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16,),
                      buildText(
                          title: 'description'.tr,
                          textData: widget.eventModel.description??"N/A",
                          padding: 10,
                          height: 80),
                      const SizedBox(height: 16,),
                      buildText(
                          title: 'address'.tr,
                          textData: widget.eventModel.address??"N/A",
                          padding: 10,
                          alignment: Alignment.centerLeft,
                          height: 50),
                      const SizedBox(
                        height: 24,
                      ),
                      const SizedBox(height: 24),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(const ListEvent());
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

