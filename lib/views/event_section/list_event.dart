import 'package:esport_system/data/controllers/coach_controller.dart';
import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/views/event_section/create_event.dart';
import 'package:esport_system/views/event_section/event_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';


class ListEvent extends StatefulWidget {
  const ListEvent({super.key});

  @override
  State<ListEvent> createState() => _ListEventState();
}

class _ListEventState extends State<ListEvent> {
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();
  var coachController =Get.find<CoachController>();

  var eventController = Get.find<EventController>();

  @override
  void initState() {
    eventController.getEvent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<EventController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: double.infinity,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25), topRight: Radius.circular(25)),
          ),
          child: Column(
            children: [
              _buildDropdown(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/calendar.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'events'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(alignment: Alignment.topRight,child: AddNewButton(btnName:'add_new'.tr,onPress: (){
                    homeController.updateWidgetHome(CreateEvent());
                  },),),

                ],
              ),
              const SizedBox(height: 22,),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : LayoutBuilder(
                            builder: (context, constraints) {
                              return SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  children: [
                                    SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minWidth: constraints.maxWidth,
                                        ),
                                        child: DataTable(
                                          dataRowMaxHeight: Get.height * 0.1,
                                          headingRowColor: WidgetStateColor.resolveWith((states) =>secondaryColor),
                                          columns:  [
                                            DataColumn(
                                              label: Text(
                                                "no.".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "title".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "address".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "description".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "start_date".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "end_date".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "actions".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                          rows: logic.eventData.isEmpty
                                          ?[]
                                          :List<DataRow>.generate(
                                            logic.eventData.length,
                                                (index) {
                                              var list = logic.eventData[index];
                                              return DataRow(
                                                cells: [
                                                  DataCell(
                                                    Text(
                                                      _truncateText('${index + 1}', 5),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${list.title}', 20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${list.address}', 20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${list.description}',
                                                          20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      formatDate(list.startDate.toString()),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      formatDate(list.endDate.toString()),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Row(
                                                      children: [
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(EventDetail(eventModel: list));
                                                          },
                                                          child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.amberAccent.withOpacity(0.1),
                                                                border: Border.all(width: 1,color: Colors.amberAccent),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(
                                                                Icons.visibility,
                                                                size: 20,
                                                                color: Colors.amberAccent,
                                                              )),
                                                        ),
                                                        const SizedBox(
                                                          width: 12,
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(CreateEvent(data: list,));
                                                          },
                                                          child: Container(
                                                              padding:
                                                              const EdgeInsets
                                                                  .all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.green.withOpacity(0.1),
                                                                border: Border.all(width: 1,color: Colors.green),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(
                                                                Icons.edit,
                                                                size: 20,
                                                                color: Colors.green,
                                                              )),
                                                        ),
                                                        const SizedBox(
                                                          width: 12,
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            showDialog(
                                                              context: context,
                                                              builder: (BuildContext
                                                              context) {
                                                                return AlertDialog(
                                                                  backgroundColor: bgColor,
                                                                  content:
                                                                  Container(
                                                                    width:Get.width * 0.3,
                                                                    height:Get.height * 0.3,
                                                                    padding:
                                                                    const EdgeInsets.all( 16),
                                                                    child: Column(
                                                                      children: [
                                                                        Align(
                                                                          alignment:
                                                                          Alignment
                                                                              .topCenter,
                                                                          child: Image .asset("assets/images/warning.png",
                                                                            width:50,
                                                                            height:50,
                                                                          ),
                                                                        ),
                                                                        const SizedBox(  height: 15),
                                                                        Text(
                                                                            "are_you_sure_you_want_to_delete_this_event?".tr,
                                                                            style: Theme.of(context).textTheme.bodyMedium
                                                                        ),
                                                                        const SizedBox( height: 15),
                                                                        const Spacer(),
                                                                        Align(
                                                                          alignment:Alignment.bottomRight,
                                                                          child: Row(
                                                                            crossAxisAlignment:
                                                                            CrossAxisAlignment.end,
                                                                            mainAxisAlignment:
                                                                            MainAxisAlignment.end,
                                                                            children: [
                                                                              CancelButton(
                                                                                btnName: 'cancel'.tr,
                                                                                onPress: () {
                                                                                  Get.back();
                                                                                },),
                                                                              const SizedBox(
                                                                                  width: 10),

                                                                              OKButton(
                                                                                  btnName: 'confirm'.tr,
                                                                                  onPress: () async{
                                                                                    logic.isLoading = true;
                                                                                    logic.update();
                                                                                    EasyLoading.show(
                                                                                      status: 'loading...'.tr,
                                                                                      dismissOnTap: true,
                                                                                    );
                                                                                    var result = await logic.deleteEvent(eventID: "${list.id}");
                                                                                    if (result['success']) {
                                                                                      EasyLoading
                                                                                          .showSuccess(
                                                                                        'event_delete_successfully'.tr,
                                                                                        dismissOnTap: true,
                                                                                        duration:
                                                                                        const Duration(
                                                                                            seconds: 2),
                                                                                      );
                                                                                      Get.back();
                                                                                      logic.eventData.removeAt(index);
                                                                                     // logic.getEvent();
                                                                                    } else {
                                                                                      EasyLoading.showError(
                                                                                        result['message'],
                                                                                        duration:
                                                                                        const Duration(
                                                                                            seconds: 2),
                                                                                      );
                                                                                      Get.back();
                                                                                    }
                                                                                    EasyLoading.dismiss();
                                                                                    logic.isLoading = false;
                                                                                  })
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                            );
                                                          },
                                                          child: Container(
                                                            padding:
                                                            const EdgeInsets
                                                                .all(6),
                                                            decoration:
                                                            BoxDecoration(
                                                              color: Colors.red.withOpacity(0.1),
                                                              border: Border.all(width: 1,color: Colors.red),
                                                              borderRadius:
                                                              BorderRadius
                                                                  .circular(6),
                                                            ),
                                                            child: const Icon(
                                                              Icons.delete,
                                                              size: 20,
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (logic.eventData.isEmpty)...[
                                      const SizedBox(height: 50,),
                                      const Center(child: EmptyData()),
                                    ]
                                  ],
                                ),
                              );
                            },
                          ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: logic.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= logic.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () => logic.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color: logic.currentPage == i
                                                ? Colors.blue
                                                : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: logic.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildDropdown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: eventController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  eventController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              eventController.search(value);
            }, hintText: 'search_event_title'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
