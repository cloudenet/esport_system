import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/event_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/event_section/list_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CreateEvent extends StatelessWidget {
  final EventModel? data;
  CreateEvent({super.key, this.data,});

  final _formKey = GlobalKey<FormBuilderState>();
  final userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor,
        body: GetBuilder<EventController>(builder: (logic) {
          if (data != null) {
            logic.titleController.text = data?.title.toString()??"N/A";
            logic.addressController.text = data?.address.toString()??"N/A";
            logic.descriptionController.text = data?.description.toString()??"N/A";
            logic.startDateController.text = data?.startDate.toString()??"N/A";
            logic.endDateController.text = data?.endDate.toString()??"N/A";
          } else {
            logic.titleController.clear();
            logic.addressController.clear();
            logic.descriptionController.clear();
            logic.startDateController.clear();
            logic.endDateController.clear();
          }
          return SizedBox(
            width: double.infinity,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        data == null
                            ? 'create_a_new_event'.tr
                            : 'update_event'.tr,
                        style: const TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 20.0),
                      FormBuilder(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'title'.tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            CustomFormBuilderTextField(
                                name: 'title'.tr,
                                controller: logic.titleController,
                                hintText: 'title'.tr,
                                errorText: 'title_is_required'.tr,
                                icon: Icons.text_fields,
                                fillColor: secondaryColor),
                            const SizedBox(height: 16.0),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'address'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                          name: 'address'.tr,
                                          controller:
                                              logic.addressController,
                                          hintText: 'address'.tr,
                                          errorText: 'address_is_required'.tr,
                                          icon: Icons.location_on_outlined,
                                          fillColor: secondaryColor),
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                       Text(
                                        'description'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                          name: 'description'.tr,
                                          controller:
                                              logic.descriptionController,
                                          hintText: 'description'.tr,
                                          errorText:
                                              'description_is_required'.tr,
                                          icon: Icons.description_outlined,
                                          fillColor: secondaryColor)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16.0),
                            Row(
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'start_date'.tr,
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                      CustomFormBuilderTextField(
                                          name: 'start_date'.tr,
                                          controller:
                                              logic.startDateController,
                                          hintText: 'start_date'.tr,
                                          errorText:
                                              'start_date_is_required'.tr,
                                          icon:
                                              Icons.calendar_month_outlined,
                                          readOnly: true,
                                          onTap: () {
                                            userController.selectDate(
                                                context,
                                                logic.startDateController);
                                            print('==============StartDart${logic.startDateController.text}');
                                          },
                                          fillColor: secondaryColor)
                                    ],
                                  ),
                                ),
                                const SizedBox(width: 16.0),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'end_date'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    CustomFormBuilderTextField(
                                        name: 'end_date'.tr,
                                        controller: logic.endDateController,
                                        hintText: 'end_date'.tr,
                                        errorText: 'end_date_is_required'.tr,
                                        icon: Icons.calendar_month_outlined,
                                        readOnly: true,
                                        onTap: () {
                                          userController.selectDate(context,
                                              logic.endDateController);
                                        },
                                        fillColor: secondaryColor)
                                  ],
                                )),
                              ],
                            ),
                            const SizedBox(height: 24.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                CancelButton(
                                    btnName: 'cancel'.tr,
                                    onPress: (){
                                      if(data == null){
                                        logic.titleController.clear();
                                        logic.addressController.clear();
                                        logic.endDateController.clear();
                                        logic.startDateController.clear();
                                        logic.descriptionController.clear();
                                        homeController.updateWidgetHome(const ListEvent());
                                      }else{
                                        homeController.updateWidgetHome(const ListEvent());
                                      }
                                    }),
                                const SizedBox(width: 16,),
                                OKButton(

                                  btnName:
                                      data == null ? 'create'.tr : 'update'.tr,
                                  onPress: () async {
                                    print('======================RR${logic.startDateController.text}');
                                    if (_formKey.currentState
                                            ?.saveAndValidate() ??
                                        false) {
                                      logic.isLoading = true;
                                      logic.update();
                                      EasyLoading.show(
                                        status: 'loading...'.tr,
                                        dismissOnTap: true,
                                      );

                                      if (data == null) {
                                        logic.isLoading = true;
                                        logic.update();
                                        EasyLoading.show(
                                          status: 'loading...'.tr,
                                          dismissOnTap: true,
                                        );
                                        bool success =
                                            await logic.createEvent(
                                                title: logic
                                                    .titleController.text,
                                                address: logic
                                                    .addressController.text,
                                                description: logic
                                                    .descriptionController
                                                    .text,
                                                startDate: logic
                                                    .startDateController
                                                    .text,
                                                endDate: logic
                                                    .endDateController.text,
                                                createdBy: userInfo!.id
                                                    .toString());
                                        EasyLoading.dismiss();
                                        logic.isLoading = false;
                                        logic.update();
                                        if (success) {
                                          EasyLoading.showSuccess(
                                            'event_created_successfully'.tr,
                                            dismissOnTap: true,
                                            duration:
                                                const Duration(seconds: 2),
                                          );
                                          homeController.updateWidgetHome(
                                              const ListEvent());
                                          EasyLoading.dismiss();
                                        } else {
                                          EasyLoading.showError(
                                            'failed_to_create'.tr,
                                            duration:
                                                const Duration(seconds: 2),
                                          );
                                        }
                                      } else {
                                        logic.isLoading = true;
                                        EasyLoading.show(
                                          status: 'loading...'.tr,
                                          dismissOnTap: true,
                                        );
                                        await logic
                                            .updateEvent(
                                          eventID: data!.id.toString(),
                                          title: logic.titleController.text,
                                          address:
                                              logic.addressController.text,
                                          description: logic
                                              .descriptionController.text,
                                          startDate: logic
                                              .startDateController.text,
                                          endDate:
                                              logic.endDateController.text,
                                          updatedBy: userInfo!.id
                                              .toString(), // Update this with actual user
                                        )
                                            .then((result) {
                                          if (result) {
                                            EasyLoading.showSuccess(
                                                'event_update_successfully!'.tr);
                                            homeController.updateWidgetHome(
                                                const ListEvent());
                                          } else {
                                            EasyLoading.showError(
                                                'failed_to_update'.tr);
                                          }
                                          EasyLoading.dismiss();
                                          logic.isLoading = false;
                                        });
                                        Get.back();
                                      }
                                    } else {
                                      EasyLoading.showError(
                                        'please_input_all_field'.tr,
                                        duration:
                                            const Duration(seconds: 2),
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }));
  }
}
