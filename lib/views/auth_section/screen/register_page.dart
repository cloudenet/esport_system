import 'package:animate_do/animate_do.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/auth_section/app_icons.dart';
import 'package:esport_system/views/auth_section/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:lottie/lottie.dart';
import 'package:responsive_grid/responsive_grid.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});

  // final _formKey = GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Row(
        children: [
          GestureDetector(
            onTap: () {
               FocusManager.instance.primaryFocus?.unfocus();
            },
             child: GetBuilder<HomeController>(builder: (logic) {
               return Responsive(
                 mobile: SingleChildScrollView(
                   child: SizedBox(
                     height: height,
                     width: width,
                     child: ResponsiveGridRow(
                       children: [
                         ResponsiveGridCol(
                             lg: 12,
                             md: 12,
                             child:  const RegisterForm(
                               padding: EdgeInsets.only(
                                   top: 35.0,
                                   bottom: 35.0,
                                   left: 45.0,
                                   right: 45.0
                               ),
                             )
                         )
                       ]
                     ),
                   ),
                 ),
                 tablet: SingleChildScrollView(
                   child: SizedBox(
                     height: height,
                     width: width,
                     child: ResponsiveGridRow(
                       children: [
                         ResponsiveGridCol(
                             lg: 6,
                             md: 6,
                             child: const RegisterView()
                         ),
                         ResponsiveGridCol(
                           lg: 6,
                           md: 6,
                           child: const RegisterForm(
                             padding:  EdgeInsets.only(
                                 top: 40.0,
                                 bottom: 40.0,
                                 left: 55.0,
                                 right: 55.0
                             ),
                           ),
                         )
                       ]
                     ),
                   ),
                 ),
                 desktop: SingleChildScrollView(
                   child: SizedBox(
                     height: height,
                     width: width,
                     child: ResponsiveGridRow(
                       children: [
                         ResponsiveGridCol(
                             lg: 6,
                             md: 6,
                             child: const RegisterView()
                         ),
                         ResponsiveGridCol(
                           lg: 6,
                           md: 6,
                           child: const RegisterForm(
                             padding:  EdgeInsets.only(
                                 top: 40.0,
                                 bottom: 40.0,
                                 left: 150.0,
                                 right: 150.0
                             ),
                           ),
                         )
                       ]
                     ),
                   ),
                 ),
               );
             }
            )
          ),
        ],
      ),
    );
  }
}

class RegisterForm extends StatefulWidget {
  final EdgeInsetsGeometry padding;
  const RegisterForm({
    super.key,
    required this.padding
  });

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _obscureText = true;
  bool _obscureCText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _ctoggle() {
    setState(() {
      _obscureCText = !_obscureCText;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    var logic = Get.find<HomeController>();

    return Container(
      height: height,
      color: AppColors.backColor,
      padding: widget.padding,
      child: SingleChildScrollView(
        padding: const EdgeInsets.only(bottom: 0.0),
        child: FormBuilder(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget> [
              SizedBox(height: height * 0.2),
              RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      alignment: PlaceholderAlignment.baseline,
                      baseline: TextBaseline.alphabetic,
                      child: FadeInUp(
                        duration: const Duration(milliseconds: 1000),
                        child: Text(
                          'Let’s'.tr,
                          style: ralewayStyle.copyWith(
                            fontSize: 25.0,
                            color: AppColors.blueDarkColor,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),

                    WidgetSpan(
                      alignment: PlaceholderAlignment.baseline,
                      baseline: TextBaseline.alphabetic,
                      child: FadeInUp(
                        duration: const Duration(milliseconds: 1000),
                        child: Text(
                          ' Sign Up 👇'.tr,
                          style: ralewayStyle.copyWith(
                            fontWeight: FontWeight.w800,
                            color: AppColors.blueDarkColor,
                            fontSize: 25.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: height * 0.01),
              FadeInUp(
                duration: const Duration(milliseconds: 1000),
                child: Text(
                  'Hey, Enter your details to register \na new account.'.tr,
                  style: ralewayStyle.copyWith(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                    color: AppColors.textColor,
                  ),
                ),
              ),

              SizedBox(height: height * 0.05),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('Username'.tr,
                  style: ralewayStyle.copyWith(
                    fontSize: 15.0,
                    color: AppColors.blueDarkColor,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

              const SizedBox(height: 6.0),
              TextFormField(
                controller: logic.nameController,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(3)
                ]),
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  color: AppColors.blueDarkColor,
                  fontSize: 14.0,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.whiteColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: AppColors.mainBlueColor
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  prefixIcon: IconButton(
                    onPressed: (){},
                    icon: Image.asset(AppIcons.emailIcon),
                  ),
                  contentPadding: const EdgeInsets.only(top: 16.0),
                  hintText: 'Enter Username'.tr,
                  hintStyle: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w400,
                    color: AppColors.blueDarkColor.withOpacity(0.5),
                    fontSize: 14.0,
                  ),
                ),
              ),

              SizedBox(height: height * 0.014),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('Phone Number'.tr,
                  style: ralewayStyle.copyWith(
                    fontSize: 15.0,
                    color: AppColors.blueDarkColor,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

              const SizedBox(height: 6.0),
              TextFormField(
                controller: logic.phoneController,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(9),
                  FormBuilderValidators.maxLength(15),
                ]),
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  color: AppColors.blueDarkColor,
                  fontSize: 14.0,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.whiteColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: AppColors.mainBlueColor
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  prefixIcon: IconButton(
                    onPressed: (){},
                    icon: Image.asset(AppIcons.emailIcon),
                  ),
                  contentPadding: const EdgeInsets.only(top: 16.0),
                  hintText: 'Enter Phone Number'.tr,
                  hintStyle: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w400,
                    color: AppColors.blueDarkColor.withOpacity(0.5),
                    fontSize: 14.0,
                  ),
                ),
              ),

              SizedBox(height: height * 0.014),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('Password'.tr,
                  style: ralewayStyle.copyWith(
                    fontSize: 15.0,
                    color: AppColors.blueDarkColor,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

              const SizedBox(height: 6.0),
              TextFormField(
                controller: logic.passwordController,
                obscureText: _obscureText,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  color: AppColors.blueDarkColor,
                  fontSize: 14.0,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.whiteColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.mainBlueColor),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  suffixIcon: IconButton(
                    onPressed: _toggle,
                    icon: _obscureText
                        ? Image.asset(AppIcons.eyeIcon)
                        : const Icon(Icons.visibility),
                  ),
                  prefixIcon: IconButton(
                    onPressed: (){},
                    icon: Image.asset(AppIcons.lockIcon),
                  ),
                  contentPadding: const EdgeInsets.only(top: 16.0),
                  hintText: 'Enter Password'.tr,
                  hintStyle: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w400,
                    color: AppColors.blueDarkColor.withOpacity(0.5),
                    fontSize: 14.0,
                  ),
                ),
              ),

              SizedBox(height: height * 0.014),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('Confirm Password'.tr,
                  style: ralewayStyle.copyWith(
                    fontSize: 15.0,
                    color: AppColors.blueDarkColor,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),

              const SizedBox(height: 6.0),
              TextFormField(
                controller: logic.passwordConfirmationController,
                obscureText: _obscureCText,
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  color: AppColors.blueDarkColor,
                  fontSize: 14.0,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.whiteColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.mainBlueColor),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  suffixIcon: IconButton(
                    onPressed: _ctoggle,
                    icon: _obscureCText
                        ? Image.asset(AppIcons.eyeIcon)
                        : const Icon(Icons.visibility),
                  ),
                  prefixIcon: IconButton(
                    onPressed: (){},
                    icon: Image.asset(AppIcons.lockIcon),
                  ),
                  contentPadding: const EdgeInsets.only(top: 16.0),
                  hintText: 'Enter Confirm Password'.tr,
                  hintStyle: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w400,
                    color: AppColors.blueDarkColor.withOpacity(0.5),
                    fontSize: 14.0,
                  ),
                ),
              ),

              SizedBox(height: height * 0.04),
              // SizedBox(height: height * 0.01),
              FadeInUp(
                duration: const Duration(milliseconds: 1000),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: ContinuousRectangleBorder(
                            borderRadius: BorderRadius.circular(10)
                        ),
                        backgroundColor: AppColors.mainBlueColor
                    ),
                    onPressed: (){
                      if(_formKey.currentState != null && _formKey.currentState!.saveAndValidate()){
                        EasyLoading.show(status: 'loading...',dismissOnTap: true);
                        logic.registerRepo().then((result) {
                          if(result){
                            EasyLoading.showSuccess('Great Success!');
                            Get.offAllNamed(RouteHelper.initial);
                          }else{
                            EasyLoading.showError('Failed with Error');
                          }
                          EasyLoading.dismiss();
                        });
                      }

                    },
                    child: Text('Sign Up'.tr,
                      style: ralewayStyle.copyWith(
                        fontWeight: FontWeight.w700,
                        color: AppColors.whiteColor,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ),
              ),

              FadeInUp(
                duration: const Duration(milliseconds: 1000),
                child:Column(
                  children: [
                    // TextButton(
                    //   onPressed: () {
                    //     //  Get.offNamed(RouteHelper.registerPage);
                    //   },
                    //   child: Text('Forgot Password'.tr),
                    // ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Already have an account?".tr,
                          style: ralewayStyle.copyWith(
                            fontSize: 14.0,
                            fontWeight: FontWeight.w600,
                            color: AppColors.textColor,
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            Get.offNamed(RouteHelper.loginPage);
                          },
                          child: Text(
                            'Sign in'.tr,
                            style: ralewayStyle.copyWith(
                              fontSize: 14.0,
                              color: AppColors.mainBlueColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}


class RegisterView extends StatelessWidget {
  const RegisterView({super.key});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;

    return Container(
      height: height,
      color: AppColors.whiteColor,
      padding: const EdgeInsets.all(15.0),
      // color: AppColors.mainBlueColor,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            Lottie.asset(
              'assets/images/login-dash.json',
              fit: BoxFit.fill,
            ),

            SizedBox(height: height * 0.03),
            FadeInUp(
              duration: const Duration(milliseconds: 1000),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Welcome back To'.tr,
                    style: ralewayStyle.copyWith(
                      fontWeight: FontWeight.w800,
                      color: AppColors.blueDarkColor,
                      fontSize: 22.0,
                    ),
                  ),
                  Text(
                    ' E-Sport System'.tr,
                    style: ralewayStyle.copyWith(
                      fontWeight: FontWeight.w800,
                      color: AppColors.mainBlueColor,
                      fontSize: 22.0,
                    ),
                  ),
                ],
              ),
            ),

            FadeInUp(
              duration: const Duration(milliseconds: 1000),
              child: Text(
                'Enjoy the time together'.tr,
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w800,
                  color: AppColors.blueDarkColor,
                  fontSize: 16.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

