import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/auth_section/app_colors.dart';
import 'package:esport_system/views/auth_section/app_icons.dart';
import 'package:esport_system/views/auth_section/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:animate_do/animate_do.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_grid/responsive_grid.dart';

import '../../../helper/images.dart';
import '../../setting_section/screen/general_system_setting.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: InkWell(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: GetBuilder<HomeController>(builder: (logic){
          return Responsive(
            mobile: SingleChildScrollView(
              child: SizedBox(
                height: height,
                width: width,
                child: ResponsiveGridRow(
                  children: [
                    ResponsiveGridCol(
                      lg: 12,
                      md: 12,
                      child:  const LogInFormMobile(
                        padding: EdgeInsets.only(
                          top: 70.0,
                          bottom: 70.0,
                          left: 45.0,
                          right: 45.0
                        ),
                      )
                    )
                  ]
                ),
              ),
            ),
            tablet: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: SizedBox(
                height: height,
                width: width,
                  child: ResponsiveGridRow(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ResponsiveGridCol(
                        lg: 12,
                        md: 12,
                        child: const LogInFormMobile(
                            padding: EdgeInsets.only(
                                    top: 45.0,
                                    bottom: 45.0,
                                    left: 85.0,
                                    right: 85.0
                            ),
                        )
                      ),
                      // ResponsiveGridCol(
                      //   lg: 7,
                      //   md: 7,
                      //   child: const LogInForm(
                      //     padding:  EdgeInsets.only(
                      //       top: 45.0,
                      //       bottom: 45.0,
                      //       left: 85.0,
                      //       right: 85.0
                      //     ),
                      //   ),
                      // )
                    ],
                  )
              ),
            ),
            desktop: SingleChildScrollView(
              child: SizedBox(
                height: height,
                width: width,
                child: ResponsiveGridRow(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ResponsiveGridCol(
                      lg: 6,
                      md: 6,
                      child: const LogInView()
                    ),
                    ResponsiveGridCol(
                      lg: 6,
                      md: 6,
                      child: const LogInForm(
                        padding:  EdgeInsets.only(
                          top: 75.0,
                          bottom: 75.0,
                          left: 150.0,
                          right: 150.0
                        ),
                      ),
                    )
                  ],
                )
              ),
            )
          );
        }),
      )
    );
  }
}

class LogInForm extends StatefulWidget {
  final EdgeInsetsGeometry padding;

  const LogInForm({
    super.key,
    required this.padding
  });

  @override
  State<LogInForm> createState() => _LogInFormState();
}

class _LogInFormState extends State<LogInForm> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width;
    var logic = Get.find<HomeController>();

    return Container(
      height: height,
      color: AppColors.backColor,
      padding: widget.padding,
      child: SingleChildScrollView(
        padding: const EdgeInsets.only(bottom: 0.0),
        child: FormBuilder(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget> [
              LanguageSystemSetting(isLoginPage: true,),
              RichText(
                text: TextSpan(
                  children: [
                    WidgetSpan(
                      alignment: PlaceholderAlignment.baseline,
                      baseline: TextBaseline.alphabetic,
                      child: FadeInUp(
                        duration: const Duration(milliseconds: 1000),
                        child: Text(
                          'let’s'.tr,
                          style: ralewayStyle.copyWith(
                            fontSize: 25.0,
                            color: AppColors.blueDarkColor,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),

                    WidgetSpan(
                      alignment: PlaceholderAlignment.baseline,
                      baseline: TextBaseline.alphabetic,
                      child: FadeInUp(
                        duration: const Duration(milliseconds: 1000),
                        child: Text(
                          'sign_in 👇'.tr,
                          style: ralewayStyle.copyWith(
                            fontWeight: FontWeight.w800,
                            color: AppColors.blueDarkColor,
                            fontSize: 25.0,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: height * 0.01),
              FadeInUp(
                duration: const Duration(milliseconds: 1000),
                child: Text(
                  'hey,_enter_your_details_to_get_sign_\ninto_your_account.'.tr,
                  style: ralewayStyle.copyWith(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600,
                    color: AppColors.textColor,
                  ),
                ),
              ),

              SizedBox(height: height * 0.05),
              Text('username_or_phone_number'.tr,
                style: ralewayStyle.copyWith(
                  fontSize: 15.0,
                  color: AppColors.blueDarkColor,
                  fontWeight: FontWeight.w700,
                ),
              ),

              const SizedBox(height: 6.0),
              TextFormField(
                controller: logic.phoneController,
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  color: AppColors.blueDarkColor,
                  fontSize: 14.0,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.whiteColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                        color: AppColors.mainBlueColor
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  prefixIcon: IconButton(
                    onPressed: (){},
                    icon: Image.asset(AppIcons.emailIcon),
                  ),
                  contentPadding: const EdgeInsets.only(top: 16.0),
                  hintText: 'enter_username_or_phone_number'.tr,
                  hintStyle: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w400,
                    color: AppColors.blueDarkColor.withOpacity(0.5),
                    fontSize: 14.0,
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),

              SizedBox(height: height * 0.014),
              Text('password'.tr,
                style: ralewayStyle.copyWith(
                  fontSize: 15.0,
                  color: AppColors.blueDarkColor,
                  fontWeight: FontWeight.w700,
                ),
              ),

              const SizedBox(height: 6.0),
              TextFormField(
                controller: logic.passwordController,
                obscureText: _obscureText,
                style: ralewayStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  color: AppColors.blueDarkColor,
                  fontSize: 14.0,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.whiteColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColors.mainBlueColor),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  suffixIcon: IconButton(
                    onPressed: _toggle,
                    icon: _obscureText
                        ? Image.asset(AppIcons.eyeIcon)
                        : const Icon(Icons.visibility),
                  ),
                  prefixIcon: IconButton(
                    onPressed: (){},
                    icon: Image.asset(AppIcons.lockIcon),
                  ),
                  contentPadding: const EdgeInsets.only(top: 16.0),
                  hintText: 'enter_password'.tr,
                  hintStyle: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w400,
                    color: AppColors.blueDarkColor.withOpacity(0.5),
                    fontSize: 14.0,
                  ),
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(),
                ]),
              ),

              SizedBox(height: height * 0.03),


              SizedBox(height: height * 0.01,),
              FadeInUp(
                duration: const Duration(milliseconds: 1000),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    height: Get.height * 0.045,
                    width: Get.width * 0.9,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                        shape: ContinuousRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                        ),
                        backgroundColor: AppColors.mainBlueColor
                      ),
                      onPressed: (){
                        if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                          EasyLoading.show( status: 'loading...'.tr, dismissOnTap: true);
                          logic.loginRepo().then((result) {
                            if (result) {
                              EasyLoading.showSuccess( 'great_success!'.tr);
                              Get.offAllNamed( RouteHelper.initial);
                            } else {
                              EasyLoading.showError( 'failed_with_error'.tr);
                            }
                            EasyLoading.dismiss();
                          });
                        }
                      },
                      child: Text('sign_in'.tr,
                        style: ralewayStyle.copyWith(
                          fontWeight: FontWeight.w700,
                          color: AppColors.whiteColor,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              // FadeInUp(
              //   duration: const Duration(milliseconds: 1000),
              //   child:Column(
              //     children: [
              //       // TextButton(
              //       //   onPressed: () {
              //       //     //  Get.offNamed(RouteHelper.registerPage);
              //       //   },
              //       //   child: Text('Forgot Password'.tr),
              //       // ),
              //       Row(
              //         crossAxisAlignment: CrossAxisAlignment.center,
              //         mainAxisAlignment: MainAxisAlignment.center,
              //         children: [
              //           Text(
              //             "Don't have an account?".tr,
              //             style: ralewayStyle.copyWith(
              //               fontSize: 14.0,
              //               fontWeight: FontWeight.w600,
              //               color: AppColors.textColor,
              //             ),
              //           ),
              //           TextButton(
              //             onPressed: () {
              //               Get.offNamed( RouteHelper.registerPage );
              //             },
              //             child: Text(
              //               'Sign up'.tr,
              //               style: ralewayStyle.copyWith(
              //                 fontSize: 14.0,
              //                 color: AppColors.mainBlueColor,
              //                 fontWeight: FontWeight.w600,
              //               ),
              //             ),
              //           ),
              //         ],
              //       ),
              //     ],
              //   ),
              // )
            ],
          ),
        )
      ),
    );
  }
}

class LogInView extends StatefulWidget {
  const LogInView({super.key});

  @override
  State<LogInView> createState() => _LogInViewState();
}

class _LogInViewState extends State<LogInView> with SingleTickerProviderStateMixin {
  // late AnimationController _controller;
  // late Animation<double> _animation;
  //
  // @override
  // void initState() {
  //   super.initState();
  //
  //   _controller = AnimationController(
  //     duration: const Duration(seconds: 2),
  //     vsync: this,
  //   )..repeat(reverse: true);
  //
  //   _animation = CurvedAnimation(
  //     parent: _controller,
  //     curve: Curves.decelerate,
  //   );
  // }
  //
  // @override
  // void dispose() {
  //   _controller.dispose();
  //   super.dispose();
  // }

  late AnimationController _controller;
  late Animation<double> fadeAnimation;
  late Animation<double> rotationAnimation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    )..repeat(reverse: true);

    fadeAnimation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    );

    rotationAnimation = Tween<double>(
      begin: -0.02,
      end: 0.01,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: height,
      color: AppColors.whiteColor,
      padding: const EdgeInsets.all(15.0),
      // color: AppColors.mainBlueColor,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget> [
            // Lottie.asset(
            //   'assets/images/login-dash.json',
            //   fit: BoxFit.fill,
            // ),
            // ScaleTransition(
            //     scale: _animation,
            //     child: Image.asset("assets/images/Boules.png",width: 500,height: 500,)
            // ),

            // FadeTransition(
            //   opacity: _fadeAnimation,
            //   child: RotationTransition(
            //     turns: _rotationAnimation,
            //     child: Image.asset(
            //       "assets/images/Boules.png",
            //       width: 500,
            //       height: 500,
            //     ),
            //   ),
            // ),
            Image.asset(
              Images.logoBoules,
              width: width * 0.6,
              height: height * 0.4,
            ),
            SizedBox(height: height * 0.03),
            FadeInUp(
              duration: const Duration(milliseconds: 1000),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា',
                    style: GoogleFonts.moul(
                      textStyle:  TextStyle(
                        color: AppColors.blueDarkColor,
                        fontWeight: FontWeight.w800,
                        fontSize: (width * 0.06)/3,
                        letterSpacing: 0.5,
                      ),
                    ),
                  ),
                  Text(
                    'Federation of Boules and Pétanque of Cambodia',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                      textStyle: const TextStyle(
                        fontWeight: FontWeight.w600,
                        color: AppColors.mainBlueColor,
                        fontSize: 26.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if(1==2)
            FadeInUp(
              duration: const Duration(milliseconds: 1000),
              child: Text(
                'Enjoy the time together'.tr,
                style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                    fontWeight: FontWeight.w600,
                    color: AppColors.blueDarkColor,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class LogInFormMobile extends StatefulWidget {
  final EdgeInsetsGeometry padding;

  const LogInFormMobile({
    super.key,
    required this.padding
  });

  @override
  State<LogInFormMobile> createState() => _LogInFormMobileState();
}

class _LogInFormMobileState extends State<LogInFormMobile> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    var logic = Get.find<HomeController>();

    return Container(
      height: height,
      width: width,
      color: AppColors.backColor,
      padding: widget.padding,
      child: SingleChildScrollView(
          padding: const EdgeInsets.only(bottom: 0.0),
          child: FormBuilder(
            key: _formKey,
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.center,
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget> [
                SizedBox(
                  width: width * 0.40,
                  height: height * 0.25,
                  child: Image.asset(
                    Images.logoBoules,

                  ),
                ),
                SizedBox(height: height * 0.01),
                FadeInUp(
                  duration: const Duration(milliseconds: 1000),
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        // 'Welcome back To'.tr,
                        'សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា',
                        style: GoogleFonts.moul(
                          textStyle: const TextStyle(
                            color: AppColors.blueDarkColor,
                            fontWeight: FontWeight.w800,
                            fontSize: 17.0,
                            letterSpacing: 0.5,
                          ),
                        ),
                      ),
                      Text(
                        'Federation of Boules and Pétanque of Cambodia',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                            fontWeight: FontWeight.w600,
                            color: AppColors.mainBlueColor,
                            fontSize: 17.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: height * 0.03),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text('username_or_phone_number'.tr,
                        style: ralewayStyle.copyWith(
                          fontSize: 15.0,
                          color: AppColors.blueDarkColor,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),

                    const SizedBox(height: 6.0),
                    TextFormField(
                      controller: logic.phoneController,
                      style: ralewayStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        color: AppColors.blueDarkColor,
                        fontSize: 14.0,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: AppColors.whiteColor,
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: AppColors.mainBlueColor
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        prefixIcon: IconButton(
                          onPressed: (){},
                          icon: Image.asset(AppIcons.emailIcon),
                        ),
                        contentPadding: const EdgeInsets.only(top: 16.0),
                        hintText: 'enter_username_or_phone_number'.tr,
                        hintStyle: ralewayStyle.copyWith(
                          fontWeight: FontWeight.w400,
                          color: AppColors.blueDarkColor.withOpacity(0.5),
                          fontSize: 14.0,
                        ),
                      ),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                      ]),
                    ),

                    SizedBox(height: height * 0.014),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text('password'.tr,
                        style: ralewayStyle.copyWith(
                          fontSize: 15.0,
                          color: AppColors.blueDarkColor,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),

                    const SizedBox(height: 6.0),
                    TextFormField(
                      controller: logic.passwordController,
                      obscureText: _obscureText,
                      style: ralewayStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        color: AppColors.blueDarkColor,
                        fontSize: 14.0,
                      ),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: AppColors.whiteColor,
                        focusedBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: AppColors.mainBlueColor),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        suffixIcon: IconButton(
                          onPressed: _toggle,
                          icon: _obscureText
                              ? Image.asset(AppIcons.eyeIcon)
                              : const Icon(Icons.visibility),
                        ),
                        prefixIcon: IconButton(
                          onPressed: (){},
                          icon: Image.asset(AppIcons.lockIcon),
                        ),
                        contentPadding: const EdgeInsets.only(top: 16.0),
                        hintText: 'enter_password'.tr,
                        hintStyle: ralewayStyle.copyWith(
                          fontWeight: FontWeight.w400,
                          color: AppColors.blueDarkColor.withOpacity(0.5),
                          fontSize: 14.0,
                        ),
                      ),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                      ]),
                    ),
                    SizedBox(height: height * 0.04),
                    FadeInUp(
                      duration: const Duration(milliseconds: 1000),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: SizedBox(
                          height: Get.height * 0.045,
                          width: Get.width * 0.9,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                                shape: ContinuousRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)
                                ),
                                backgroundColor: AppColors.mainBlueColor
                            ),
                            onPressed: (){
                              if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                EasyLoading.show( status: 'loading...'.tr, dismissOnTap: true);
                                logic.loginRepo().then((result) {
                                  if (result) {
                                    EasyLoading.showSuccess( 'great_success!');
                                    Get.offAllNamed( RouteHelper.initial);
                                  } else {
                                    EasyLoading.showError( 'failed_with_error');
                                  }
                                  EasyLoading.dismiss();
                                });
                              }
                            },
                            child: Text('sign_in'.tr,
                              style: ralewayStyle.copyWith(
                                fontWeight: FontWeight.w700,
                                color: AppColors.whiteColor,
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )
      ),
    );
  }
}
