import 'package:esport_system/data/controllers/league_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/data/model/team_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import '../../../data/controllers/home_controller.dart';
import 'get_team_screen.dart';

class CreateUpdateTeamScreen extends StatefulWidget {
  final TeamModel? teamData;

  const CreateUpdateTeamScreen({super.key, this.teamData});

  @override
  State<CreateUpdateTeamScreen> createState() => _CreateUpdateTeamScreenState();
}

class _CreateUpdateTeamScreenState extends State<CreateUpdateTeamScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final logic = Get.find<TeamController>();
  var leagueController = Get.find<LeagueController>();
  var homeController = Get.find<HomeController>();
  String? selectedLeagueId;

  @override
  void initState() {
    leagueController.getLeague();
    if (widget.teamData != null) {
      logic.teamNameController.text = widget.teamData!.teamName.toString();
      logic.stadiumController.text = widget.teamData!.stadium.toString();
      logic.championshipsWonController.text = widget.teamData!.championshipsWon.toString();
      logic.selectProvince(Address(
        code: widget.teamData!.provinceCode,
        name: widget.teamData!.province,
      ));
      logic.selectDistrict(Address(
        code: widget.teamData!.districtCode,
        name: widget.teamData!.district,
      ));
      logic.selectCommune(Address(
        code: widget.teamData!.communeCode,
        name: widget.teamData!.commune,
      ));
      logic.selectVillage(Address(
        code: widget.teamData!.villageCode,
        name: widget.teamData!.village,
      ));
      logic.selectedImage = XFile(widget.teamData!.logo!);
      selectedLeagueId = widget.teamData?.parentTeam?.id ?? null;
    } else {
      logic.selectedImage = null;
      logic.selectedVillage = null;
      logic.selectedProvince = null;
      logic.selectedDistrict = null;
      logic.selectedCommune = null;
      logic.championshipsWonController.clear();
      logic.stadiumController.clear();
      logic.stadiumController.clear();
      logic.teamNameController.clear();
      selectedLeagueId = null;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<TeamController>(builder: (logic) {
        return FormBuilder(
          key: _formKey,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(32),
            child: ListView(
              //   crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20),
                Text(
                  widget.teamData != null
                      ? 'update_team'.tr
                      : 'create_new_team'.tr,
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
                const SizedBox(height: 10),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('team_name'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['team_name'.tr]?.errorText,
                            name: 'team_name'.tr,
                            controller: logic.teamNameController,
                            hintText: 'enter_team_name'.tr,
                            icon: Icons.person,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('stadium'.tr, style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['stadium']?.errorText,
                            name: 'stadium'.tr,
                            controller: logic.stadiumController,
                            hintText: 'enter_stadium'.tr,
                            icon: Icons.stadium_outlined,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('province'.tr,
                              style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'province'.tr,
                            logic.provinces,
                            logic.selectedProvince?.name ?? 'select_province'.tr,
                            logic.selectProvince,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10), // Spacing between the two fields
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('district'.tr,
                              style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'district'.tr,
                            logic.districts,
                            logic.selectedDistrict?.name ?? 'select_district'.tr,
                            logic.selectDistrict,
                            enabled: logic.selectedProvince != null,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('commune'.tr, style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'commune'.tr,
                            logic.communes,
                            logic.selectedCommune?.name ?? 'select_commune'.tr,
                            logic.selectCommune,
                            enabled: logic.selectedDistrict != null,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10), // Spacing between the two fields
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('village'.tr, style: const TextStyle(fontSize: 16)),
                          _buildLocationSelector(
                            icon: Icons.location_on,
                            context,
                            'village'.tr,
                            logic.villages,
                            logic.selectedVillage?.name ?? 'select_village'.tr,
                            logic.selectVillage,
                            enabled: logic.selectedCommune != null,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('championships_won'.tr,
                              style: const TextStyle(fontSize: 16)),
                          CustomFormBuilderTextField(
                            errorText: _formKey.currentState?.fields['championships_won'.tr]?.errorText,
                            name: 'championships_won'.tr,
                            controller: logic.championshipsWonController,
                            hintText: 'enter_championships_won'.tr,
                            icon: Icons.code,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'select_league'.tr,
                            ),
                            DropdownButtonFormField<String>(
                              borderRadius: BorderRadius.circular(10),
                              value: selectedLeagueId,
                              items: leagueController.listLeague.map((user) {
                                return DropdownMenuItem<String>(
                                  value: user.id,
                                  child: Text(user.leagueName!),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  selectedLeagueId = value ?? '';
                                });
                              },
                              decoration: InputDecoration(
                                labelText: "please_select_league".tr,
                                floatingLabelAlignment:FloatingLabelAlignment.center,
                                floatingLabelBehavior: FloatingLabelBehavior.never,
                                fillColor: secondaryColor,
                                filled: true,
                                border: const OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                ),
                              ),
                            ),
                          ],
                    )
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('logo'.tr, style: const TextStyle(fontSize: 16)),
                          GestureDetector(
                            onTap: () {
                              logic.pickImage();
                            },
                            child: Container(
                              height: 150,
                              width: 150,
                              margin: const EdgeInsets.symmetric(vertical: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.grey[200],
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(10),
                                child: logic.selectedImage != null
                                    ? Image.network(
                                        logic.selectedImage!.path,
                                        fit: BoxFit.cover,
                                      )
                                    : Image.asset(
                                        'assets/images/no-images.png'),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CancelButton(
                        btnName: 'cancel'.tr,
                        onPress: () {
                          homeController
                              .updateWidgetHome(const ListTeamScreen());
                        }),
                    const SizedBox(
                      width: 12,
                    ),
                    OKButton(
                        btnName: widget.teamData == null ? 'create'.tr : "update".tr,
                        onPress: () async {
                          if (_formKey.currentState != null &&
                              _formKey.currentState!.saveAndValidate()) {
                            // All validations pass, proceed with form submission
                            String teamName = logic.teamNameController.text;
                            String stadium = logic.stadiumController.text;
                            String championshipsWon =
                                logic.championshipsWonController.text;
                            String foundDate =
                                DateFormat('yyyy-MM-dd').format(DateTime.now());
                            String provinceCode = logic.selectedProvince!.code!;
                            String districtCode = logic.selectedDistrict!.code!;
                            String communeCode = logic.selectedCommune!.code!;
                            String villageCode = logic.selectedVillage!.code!;
                            String province = logic.selectedProvince!.name!;
                            String district = logic.selectedDistrict!.name!;
                            String commune = logic.selectedCommune!.name!;
                            String villages = logic.selectedVillage!.name!;
                            if (widget.teamData != null) {
                              EasyLoading.show(status: 'loading...'.tr);
                              logic
                                  .updateTeam(
                                      teamId: widget.teamData!.id.toString(),
                                      teamName: teamName,
                                      foundedDate: foundDate,
                                      stadium: stadium,
                                      provinceCode: provinceCode,
                                      districtCode: districtCode,
                                      villageCode: villageCode,
                                      communeCode: communeCode,
                                      province: province,
                                      district: district,
                                      commune: commune,
                                      village: villages,
                                      leagueId: selectedLeagueId.toString(),
                                      championshipsWon: championshipsWon,
                                      imageData: logic.selectedImage)
                                  .then((result) {
                                if (result) {
                                  EasyLoading.showSuccess('team_update_successfully!'.tr);
                                  const Duration(seconds: 2);
                                  homeController
                                      .updateWidgetHome(const ListTeamScreen());
                                } else {
                                  EasyLoading.showError('failed_with_error'.tr);
                                  const Duration(seconds: 2);
                                }
                                EasyLoading.dismiss();
                              });
                            } else {
                              EasyLoading.show(status: 'loading...'.tr);
                              logic
                                  .createTeam(
                                teamName: teamName,
                                foundDate: foundDate,
                                stadium: stadium,
                                provinceCode: provinceCode,
                                districtCode: districtCode,
                                villageCode: villageCode,
                                communeCode: communeCode,
                                province: province,
                                district: district,
                                commune: commune,
                                villages: villages,
                                championshipsWon: championshipsWon,
                                leagueId: selectedLeagueId.toString(),
                                logo: logic.selectedImage!,
                              )
                                  .then((result) {
                                if (result) {
                                  EasyLoading.showSuccess(
                                    'team_create_successfully!'.tr,
                                    dismissOnTap: true,
                                    duration: const Duration(seconds: 2),
                                  );

                                  homeController
                                      .updateWidgetHome(const ListTeamScreen());
                                } else {
                                  EasyLoading.showError(
                                    'failed_with_error'.tr,
                                    duration: const Duration(seconds: 2),
                                  );
                                }
                                EasyLoading.dismiss();
                              });
                            }
                          }
                        }),
                  ],
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget _buildLocationSelector(BuildContext context, String label,
      List<Address> data, String currentValue, Function(Address) onSelect,
      {bool enabled = true, IconData? icon}) {
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () => _showSelectionDialog(
              context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: secondaryColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
        kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
