import 'package:esport_system/data/model/team_model.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/create_team_section/screen/get_team_screen.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/controllers/home_controller.dart';
import '../../../data/controllers/team_controller.dart';
import '../../../helper/add_new_button.dart';
import '../../../helper/app_contrain.dart';
import '../../../helper/constants.dart';

class TeamDetailScreen extends StatefulWidget {
  final TeamModel data;

  const TeamDetailScreen({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  State<TeamDetailScreen> createState() => _TeamDetailScreenState();
}

class _TeamDetailScreenState extends State<TeamDetailScreen> {
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();
  @override
  void initState() {
    teamController.fetchPlayersInTeam(widget.data.id.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
        mobile: bodyDetailMobile(),
        tablet: bodyDetailTablet(),
        desktop: bodyDetail(),
      ),
    );
  }


  Widget bodyDetail() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.logo!.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(  // This allows the Column to take up available space and wrap if necessary
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                      children: [
                        Text(
                          widget.data.teamName != "null" ? widget.data.teamName.toString() : "N/A",
                          style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,  // Allows the text to wrap to two lines
                          overflow: TextOverflow.ellipsis,  // Shows ellipsis when text overflows
                        ),
                        const SizedBox(height: 4.0),  // Space between name and stadium
                        Text(
                            widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey[600],  // Stadium text style
                          ),
                          overflow: TextOverflow.ellipsis,  // Safeguard against long stadium names
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("team_identity".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "team_name".tr,
                            textData: widget.data.teamName != "null" ? widget.data.teamName.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "founded_date".tr,
                            textData: widget.data.parentTeam!.foundedDate != "null" ? formatDate(widget.data.parentTeam!.foundedDate.toString()) : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("location_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "stadium".tr,
                      textData:widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "province".tr,
                            textData: widget.data.province != "null" ? widget.data.province.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "district".tr,
                            textData: widget.data.district != "null" ? widget.data.district.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "commune".tr,
                            textData: widget.data.commune != "null" ? widget.data.commune.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "village".tr,
                            textData: widget.data.village != "null" ? widget.data.village.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.address != "null" ? widget.data.address.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("performance".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "championships_won".tr,
                      textData: widget.data.championshipsWon != "null" ? widget.data.championshipsWon.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("parent_team_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),

            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "parents_team_name".tr,
                      textData: widget.data.parentTeam!.teamName != "null" ? widget.data.parentTeam!.teamName.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.parentTeam!.address != "null" ? widget.data.parentTeam!.address.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            GetBuilder<TeamController>(builder: (logic) {
              return Row(
                children: [
                  const SizedBox(width: 10),
                  ...List.generate(
                    logic.players.take(10).length,
                        (index) {
                      var player = logic.players[index];
                      return Align(
                        widthFactor: 0.5,
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 42,
                            backgroundImage:
                            NetworkImage(player.photo.toString()),
                          ),
                        ),
                      );
                    },
                  ),
                  logic.players.isNotEmpty
                      ? GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(10.0),
                            ),
                            backgroundColor: bgColor,
                            title: Align(
                                alignment: Alignment.topCenter,
                                child: Text('player_in_team'.tr)),
                            children: <Widget>[
                              Column(
                                children: [
                                  ...List.generate(
                                    logic.players.length,(index) {
                                      var dataList = logic.players[index];
                                      return ListTile(
                                        leading: Container(
                                          margin: const EdgeInsets.symmetric(horizontal: 8),
                                          child: dataList.photo != "null"
                                              ? CircleAvatar(
                                            foregroundImage:
                                            NetworkImage(
                                              "${AppConstants.baseUrl}${dataList.photo}",
                                            ),
                                          )
                                              : Container(
                                            width: 40,
                                            height: 40,
                                            decoration:
                                            BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius:
                                              BorderRadius
                                                  .circular(
                                                  20),
                                            ),
                                            child: Icon(
                                              Icons.person,
                                              color: Colors
                                                  .grey[400],
                                            ),
                                          ),
                                        ),
                                        title: Text('${dataList.name}'),
                                        onTap: () {
                                          // Handle action
                                          Navigator.pop(context);
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                        color: bgColor,
                        shape: BoxShape.circle,
                      ),
                      child: const Center(
                        child: Icon(Icons.more_horiz),
                      ),
                    ),
                  )
                      : const SizedBox(),
                ],
              );
            }),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const ListTeamScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
  Widget bodyDetailTablet() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.logo!.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(  // This allows the Column to take up available space and wrap if necessary
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                      children: [
                        Text(
                          widget.data.teamName != "null" ? widget.data.teamName.toString() : "N/A",
                          style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,  // Allows the text to wrap to two lines
                          overflow: TextOverflow.ellipsis,  // Shows ellipsis when text overflows
                        ),
                        const SizedBox(height: 4.0),  // Space between name and stadium
                        Text(
                          widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey[600],  // Stadium text style
                          ),
                          overflow: TextOverflow.ellipsis,  // Safeguard against long stadium names
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("team_identity".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "team_name".tr,
                            textData: widget.data.teamName != "null" ? widget.data.teamName.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "founded_date".tr,
                            textData: widget.data.parentTeam!.foundedDate != "null" ? formatDate(widget.data.parentTeam!.foundedDate.toString()) : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("location_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "stadium".tr,
                      textData: widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "province".tr,
                            textData: widget.data.province != "null" ? widget.data.province.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "district".tr,
                            textData: widget.data.district != "null" ? widget.data.district.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "commune".tr,
                            textData: widget.data.commune != "null" ? widget.data.commune.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "village".tr,
                            textData: widget.data.village != "null" ? widget.data.village.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.address != "null" ? widget.data.address.toString() : "N/A",
                      height: 70,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("performance".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "championships_won".tr,
                      textData: widget.data.championshipsWon != "null" ? widget.data.championshipsWon.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("parent_team_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),

            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "parents_team_name".tr,
                      textData: widget.data.parentTeam!.teamName != "null" ? widget.data.parentTeam!.teamName.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.parentTeam!.address != "null" ? widget.data.parentTeam!.address.toString() : "N/A",
                      height: 70,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            GetBuilder<TeamController>(builder: (logic) {
              return Row(
                children: [
                  const SizedBox(width: 10),
                  ...List.generate(
                    logic.players.take(10).length,
                        (index) {
                      var player = logic.players[index];
                      return Align(
                        widthFactor: 0.5,
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 42,
                            backgroundImage:
                            NetworkImage(player.photo.toString()),
                          ),
                        ),
                      );
                    },
                  ),
                  logic.players.isNotEmpty
                      ? GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(10.0),
                            ),
                            backgroundColor: bgColor,
                            title: Align(
                                alignment: Alignment.topCenter,
                                child: Text('player_in_team'.tr)),
                            children: <Widget>[
                              Column(
                                children: [
                                  ...List.generate(
                                    logic.players.length,(index) {
                                      var dataList = logic.players[index];
                                      return ListTile(
                                        leading: Container(
                                          margin: const EdgeInsets.symmetric(horizontal: 8),
                                          child: dataList.photo != "null"
                                              ? CircleAvatar(
                                            foregroundImage:
                                            NetworkImage(
                                              "${AppConstants.baseUrl}${dataList.photo}",
                                            ),
                                          )
                                              : Container(
                                            width: 40,
                                            height: 40,
                                            decoration:
                                            BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius:
                                              BorderRadius
                                                  .circular(
                                                  20),
                                            ),
                                            child: Icon(
                                              Icons.person,
                                              color: Colors
                                                  .grey[400],
                                            ),
                                          ),
                                        ),
                                        title: Text('${dataList.name}'),
                                        onTap: () {
                                          // Handle action
                                          Navigator.pop(context);
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                        color: bgColor,
                        shape: BoxShape.circle,
                      ),
                      child: const Center(
                        child: Icon(Icons.more_horiz),
                      ),
                    ),
                  )
                      : const SizedBox(),
                ],
              );
            }),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const ListTeamScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
  Widget bodyDetailMobile(){
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.logo!.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(  // This allows the Column to take up available space and wrap if necessary
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                      children: [
                        Text(
                          widget.data.teamName != "null" ? widget.data.teamName.toString() : "N/A",
                          style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,  // Allows the text to wrap to two lines
                          overflow: TextOverflow.ellipsis,  // Shows ellipsis when text overflows
                        ),
                        const SizedBox(height: 4.0),  // Space between name and stadium
                        Text(
                          widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey[600],  // Stadium text style
                          ),
                          overflow: TextOverflow.ellipsis,  // Safeguard against long stadium names
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("team_identity".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "team_name".tr,
                      textData: widget.data.teamName != "null" ? widget.data.teamName.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "founded_date".tr,
                      textData: widget.data.parentTeam!.foundedDate != "null" ? formatDate(widget.data.parentTeam!.foundedDate.toString()) : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("location_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "stadium".tr,
                      textData: widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "province".tr,
                      textData: widget.data.province != "null" ? widget.data.province.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "district".tr,
                      textData: widget.data.district != "null" ? widget.data.district.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "commune".tr,
                      textData: widget.data.commune != "null" ? widget.data.commune.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "village".tr,
                      textData: widget.data.village != "null" ? widget.data.village.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.address != "null" ? widget.data.address.toString() : "N/A",
                      height: 80,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("performance".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "championships_won".tr,
                      textData: widget.data.championshipsWon != "null" ? widget.data.championshipsWon.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("parent_team_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),

            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                    title: "parents_team_name".tr,
                    textData: widget.data.parentTeam!.teamName != "null" ? widget.data.parentTeam!.teamName.toString() : "N/A",
                    height: 50,
                    padding: 8,
                    alignment: Alignment.centerLeft,
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                    title: "address".tr,
                    textData: widget.data.parentTeam!.address != "null" ? widget.data.parentTeam!.address.toString() : "N/A",
                    height: 80,
                    padding: 8,
                    alignment: Alignment.centerLeft,
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            GetBuilder<TeamController>(builder: (logic) {
              return logic.players.isNotEmpty? Row(
                children: [
                  const SizedBox(width: 10),
                  ...List.generate(
                    logic.players.take(10).length,(index) {
                      var player = logic.players[index];
                      return Align(
                        widthFactor: 0.5,
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: Colors.white,
                          child: CircleAvatar(
                            radius: 42,
                            backgroundImage:
                            NetworkImage(player.photo.toString()),
                          ),
                        ),
                      );
                    },
                  ),
                  logic.players.isNotEmpty
                      ? GestureDetector(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(10.0),
                            ),
                            backgroundColor: bgColor,
                            title: Align(
                                alignment: Alignment.topCenter,
                                child: Text('player_in_team'.tr)),
                            children: <Widget>[
                              Column(
                                children: [
                                  ...List.generate(
                                    logic.players.length,
                                        (index) {
                                      var dataList =
                                      logic.players[index];
                                      return ListTile(
                                        leading: Container(
                                          margin: const EdgeInsets
                                              .symmetric(
                                              horizontal: 8),
                                          child: dataList.photo !=
                                              "null"
                                              ? CircleAvatar(
                                            foregroundImage:
                                            NetworkImage(
                                              "${AppConstants.baseUrl}${dataList.photo}",
                                            ),
                                          )
                                              : Container(
                                            width: 40,
                                            height: 40,
                                            decoration:
                                            BoxDecoration(
                                              color: Colors
                                                  .grey[200],
                                              borderRadius:
                                              BorderRadius
                                                  .circular(
                                                  20),
                                            ),
                                            child: Icon(
                                              Icons.person,
                                              color: Colors
                                                  .grey[400],
                                            ),
                                          ),
                                        ),
                                        title: Text(
                                            '${dataList.name}'),
                                        onTap: () {
                                          // Handle action
                                          Navigator.pop(context);
                                        },
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                        color: bgColor,
                        shape: BoxShape.circle,
                      ),
                      child: const Center(
                        child: Icon(Icons.more_horiz),
                      ),
                    ),
                  )
                      : const SizedBox(),
                ],
              ):const SizedBox();
            }),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const ListTeamScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}
