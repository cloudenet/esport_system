// import 'package:esport_system/data/controllers/team_controller.dart';
// import 'package:esport_system/data/model/address_model.dart';
// import 'package:esport_system/helper/app_contrain.dart';
// import 'package:esport_system/util/text_style.dart';
// import 'package:esport_system/data/model/team_model.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:flutter_form_builder/flutter_form_builder.dart';
// import 'package:form_builder_validators/form_builder_validators.dart';
// import 'package:get/get.dart';
// import 'package:responsive_grid/responsive_grid.dart';
//
// var teamController = Get.find<TeamController>();
// final _formKey = GlobalKey<FormBuilderState>();
//
// void buildUpdateTeam(TeamModel data, BuildContext context) async {
//   teamController.communeController.text = data.commune.toString();
//   teamController.provinceController.text = data.province.toString();
//   teamController.districtController.text = data.district.toString();
//   teamController.villageController.text = data.village.toString();
//   teamController.teamNameController.text = data.teamName.toString();
//   teamController.stadiumController.text = data.stadium.toString();
//   teamController.championshipsWonController.text = data.championshipsWon.toString();
//
//   return showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       return GetBuilder<TeamController>(builder: (logic) {
//         return AlertDialog(
//           content: Container(
//             width: Get.width * 0.5,
//             height: Get.height * 0.7,
//             padding: const EdgeInsets.all(16),
//             child: SingleChildScrollView(
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Text(
//                         'Update Team',
//                         style: TextStylesHelper.headline2,
//                       ),
//                       GestureDetector(
//                         onTap: () {
//                           Get.back();
//                         },
//                         child: Container(
//                           padding: const EdgeInsets.all(8),
//                           decoration: const BoxDecoration(
//                               shape: BoxShape.circle, color: Colors.red),
//                           child: const Text(
//                             'X',
//                             style: TextStyle(
//                                 color: Colors.white,
//                                 fontWeight: FontWeight.bold),
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                   Align(
//                     alignment: Alignment.topCenter,
//                     child: data.logo == null
//                         ? Container(
//                             width: 100,
//                             height: 100,
//                             decoration: BoxDecoration(
//                               borderRadius: BorderRadius.circular(100),
//                               color: Theme.of(context).primaryColor,
//                             ),
//                             child: Center(
//                               child: Text(
//                                   data.teamName![0].toString().toUpperCase()),
//                             ),
//                           )
//                         : logic.getImageDataForTeam(data.id.toString()) != null
//                             ? Container(
//                                 width: 100,
//                                 height: 100,
//                                 decoration: BoxDecoration(
//                                   shape: BoxShape.circle,
//                                   image: DecorationImage(
//                                     image: MemoryImage(
//                                         logic.getImageDataForTeam(
//                                             data.id.toString())!),
//                                     fit: BoxFit.cover,
//                                   ),
//                                 ),
//                               )
//                             : Container(
//                                 width: 100,
//                                 height: 100,
//                                 decoration: BoxDecoration(
//                                   shape: BoxShape.circle,
//                                   image: DecorationImage(
//                                     image: NetworkImage(
//                                         '${AppConstants.baseUrl}${data.logo}'),
//                                     fit: BoxFit.cover,
//                                   ),
//                                 ),
//                               ),
//                   ),
//                   const SizedBox(
//                     height: 15,
//                   ),
//                   ElevatedButton(
//                     onPressed: () {
//                       logic.pickImageForTeam(data.id.toString());
//                     },
//                     style: ElevatedButton.styleFrom(
//                       shape: ContinuousRectangleBorder(
//                           borderRadius: BorderRadius.circular(10)),
//                       backgroundColor: Colors.green,
//                     ),
//                     child: const Text(
//                       'Choose New Logo',
//                     ),
//                   ),
//                   const SizedBox(
//                     height: 15,
//                   ),
//                   FormBuilder(
//                     key: _formKey,
//                     child: ResponsiveGridRow(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         ResponsiveGridCol(
//                           md: 6,
//                           lg: 6,
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 8, vertical: 8),
//                             child: FormBuilderTextField(
//                               name: 'Team name',
//                               controller: logic.teamNameController,
//                               decoration: const InputDecoration(
//                                 labelText: 'Team name',
//                               ),
//                             ),
//                           ),
//                         ),
//                         ResponsiveGridCol(
//                           md: 6,
//                           lg: 6,
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 8, vertical: 8),
//                             child: FormBuilderTextField(
//                               name: 'Stadium',
//                               controller: logic.stadiumController,
//                               decoration: const InputDecoration(
//                                 labelText: 'Stadium',
//                               ),
//                             ),
//                           ),
//                         ),
//                         ResponsiveGridCol(
//                           md: 6,
//                           lg: 6,
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 8, vertical: 8),
//                             child: FormBuilderTextField(
//                               name: 'Select Province',
//                               readOnly: true,
//                               decoration: const InputDecoration(
//                                 labelText: 'Province',
//                                 suffixIcon: Icon(Icons.arrow_drop_down),
//                               ),
//                               onTap: () {
//                                 _showSelectionDialog(
//                                   context: context,
//                                   title: 'Select Province',
//                                   data: logic.provinces,
//                                   onSelect: (Address province) {
//                                     logic.selectProvince(province);
//                                     logic.provinceController.text =
//                                         province.name!;
//                                     logic.districtController.text = '';
//                                     logic.communeController.text = '';
//                                     logic.villageController.text = '';
//                                   },
//                                 );
//                               },
//                               validator: FormBuilderValidators.compose([
//                                 FormBuilderValidators.required(),
//                               ]),
//                               controller: TextEditingController(
//                                   text: logic.provinceController.text),
//                             ),
//                           ),
//                         ),
//                         ResponsiveGridCol(
//                           md: 6,
//                           lg: 6,
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 8, vertical: 8),
//                             child: FormBuilderTextField(
//                               name: 'Select District',
//                               readOnly: true,
//                               decoration: const InputDecoration(
//                                 labelText: 'District',
//                                 suffixIcon: Icon(Icons.arrow_drop_down),
//                               ),
//                               onTap: () {
//                                 _showSelectionDialog(
//                                   context: context,
//                                   title: 'Select District',
//                                   data: logic.districts,
//                                   onSelect: (Address district) {
//                                     logic.selectDistrict(district);
//                                     logic.districtController.text =
//                                         district.name!;
//                                     logic.communeController.text = '';
//                                     logic.villageController.text = '';
//                                   },
//                                 );
//                               },
//                               validator: FormBuilderValidators.compose([
//                                 FormBuilderValidators.required(),
//                               ]),
//                               controller: TextEditingController(
//                                   text: logic.districtController.text),
//                             ),
//                           ),
//                         ),
//                         ResponsiveGridCol(
//                           md: 6,
//                           lg: 6,
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 8, vertical: 8),
//                             child: FormBuilderTextField(
//                               name: 'Select Commune',
//                               readOnly: true,
//                               decoration: const InputDecoration(
//                                 labelText: 'Commune',
//                                 suffixIcon: Icon(Icons.arrow_drop_down),
//                               ),
//                               onTap: () {
//                                 _showSelectionDialog(
//                                   context: context,
//                                   title: 'Select Commune',
//                                   data: logic.communes,
//                                   onSelect: (Address commune) {
//                                     logic.selectCommune(commune);
//                                     logic.communeController.text = commune.name!;
//                                     logic.villageController.text = '';
//                                   },
//                                 );
//                               },
//                               validator: FormBuilderValidators.compose([
//                                 FormBuilderValidators.required(),
//                               ]),
//                               controller: TextEditingController(
//                                   text: logic.communeController.text),
//                             ),
//                           ),
//                         ),
//                         ResponsiveGridCol(
//                           md: 6,
//                           lg: 6,
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 8, vertical: 8),
//                             child: FormBuilderTextField(
//                               name: 'Select Village',
//                               readOnly: true,
//                               decoration: const InputDecoration(
//                                 labelText: 'Village',
//                                 suffixIcon: Icon(Icons.arrow_drop_down),
//                               ),
//                               onTap: () {
//                                 _showSelectionDialog(
//                                   context: context,
//                                   title: 'Select Village',
//                                   data: logic.villages,
//                                   onSelect: (Address village) {
//                                     logic.selectVillage(village);
//                                     logic.villageController.text = village.name!;
//                                   },
//                                 );
//                               },
//                               validator: FormBuilderValidators.compose([
//                                 FormBuilderValidators.required(),
//                               ]),
//                               controller: TextEditingController(
//                                   text: logic.villageController.text),
//                             ),
//                           ),
//                         ),
//                         ResponsiveGridCol(
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(vertical: 8.0),
//                             child: Center(
//                               child: ElevatedButton(
//                                 onPressed: () async {
//
//                                   try {
//                                     // Set loading state
//                                     logic.isLoading = true;
//                                     logic.update();
//                                     EasyLoading.show(
//                                       status: 'Loading...',
//                                       dismissOnTap: true,
//                                     );
//
//                                     // Call updateTeam method
//                                     var success = await logic.updateTeam(
//                                       teamId: data.id.toString(),
//                                       teamName: logic.teamNameController.text,
//                                       foundedDate: DateTime.now().toString(),
//                                       stadium: logic.stadiumController.text,
//                                       provinceCode: data.provinceCode.toString(),
//                                       districtCode: data.districtCode.toString(),
//                                       villageCode: data.villageCode.toString(),
//                                       communeCode: data.communeCode.toString(),
//                                       province: logic.provinceController.text,
//                                       district: logic.districtController.text,
//                                       commune: logic.communeController.text,
//                                       village: logic.villageController.text,
//                                       championshipsWon: logic.championshipsWonController.text,
//                                       imageData: logic.getImageDataForTeam(data.id.toString()),
//                                     );
//                                     EasyLoading.dismiss();
//                                     logic.isLoading = false;
//                                     // Show success or error message
//                                     if (success) {
//                                       Get.back();
//                                       logic.getImageDataForTeam(data.id.toString())!.clear();
//                                       EasyLoading.showSuccess('Profile updated successfully', dismissOnTap: true);
//                                       logic.getTeam();
//                                     } else {
//                                       EasyLoading.showError(
//                                         'Error updating profile',
//                                         duration: const Duration(seconds: 1),
//                                       );
//                                     }
//                                   } catch (e) {
//                                     EasyLoading.dismiss(); // Dismiss loading indicator on error
//                                   }
//                                   finally{
//                                     EasyLoading.dismiss();
//                                   }
//                                 },
//                                 child: const Text('Update Team'),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         );
//       });
//     },
//   );
// }
//
// void _showSelectionDialog({
//   required BuildContext context,
//   required String title,
//   required List<Address> data,
//   required Function(Address) onSelect,
// }) {
//   final double dialogWidth =
//       kIsWeb ? MediaQuery.of(context).size.width * 0.3 : double.maxFinite;
//   showDialog(
//     context: context,
//     builder: (BuildContext context) {
//       return AlertDialog(
//         title: Text(title),
//         contentPadding:
//             const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
//         content: SizedBox(
//           width: dialogWidth,
//           child: ListView(
//             children: data.map((item) {
//               return ListTile(
//                 title: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Text(item.name!),
//                     Text(item.description!),
//                     const Divider()
//                   ],
//                 ),
//                 onTap: () {
//                   onSelect(item);
//                   Navigator.pop(context);
//                 },
//               );
//             }).toList(),
//           ),
//         ),
//       );
//     },
//   );
// }
