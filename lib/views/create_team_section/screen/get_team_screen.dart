import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/views/create_team_section/screen/create_team_screen.dart';
import 'package:esport_system/views/create_team_section/screen/detail_team_widget.dart';
import 'package:esport_system/views/create_team_section/screen/dropdown_widget.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../data/controllers/home_controller.dart';
import '../../../helper/images.dart';
class ListTeamScreen extends StatefulWidget {
  const ListTeamScreen({super.key});

  @override
  State<ListTeamScreen> createState() => _ListTeamScreenState();
}

class _ListTeamScreenState extends State<ListTeamScreen> {
  final teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    teamController.getTeam();
    teamController.fetchProvinces();

    super.initState();
  }
  // Future<void> _refreshLocalGallery() async{
  //   print('refreshing stocks...');
  //
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<TeamController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: double.infinity,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
              topRight: Radius.circular(25),
            ),
          ),
          child: Column(
            children: [
              buildDropDown(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/group.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'teams'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: AddNewButton(
                      btnName: "add_new".tr,
                      onPress: () {
                        homeController.updateWidgetHome(const CreateUpdateTeamScreen());
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 22,),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : LayoutBuilder(
                            builder: (context, constraints) {
                              return SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  children: [
                                    SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minWidth: constraints.maxWidth,
                                        ),
                                        child: DataTable(
                                          dataRowMaxHeight: Get.height * 0.1,
                                          headingRowColor: WidgetStateColor.resolveWith((states) => secondaryColor),
                                          columns:  [
                                            DataColumn(
                                              label: Text(
                                                "logo".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "team_name".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "stadium".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "address".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "champion_won".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "actions".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                          rows: logic.listTeam.isEmpty
                                            ?[]
                                            :List<DataRow>.generate(
                                            logic.listTeam.length, (index) {
                                              var data = logic.listTeam[index];
                                              print("${data.logo}");
                                              return DataRow(
                                                cells: [
                                                  DataCell(
                                                    data.logo != "null" ?
                                                    ClipOval(
                                                      child: Image.network("${AppConstants.baseUrl}${data.logo}",
                                                        width: 50,
                                                        height: 50,
                                                        fit: BoxFit.cover,
                                                        errorBuilder: (context, error, stackTrace) {
                                                          return ClipOval(
                                                            child: Image.asset(
                                                              Images.noImage,
                                                              height: 50,
                                                              width: 50,
                                                              fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                                            ),
                                                          );
                                                        },
                                                        loadingBuilder: (context, child, loadingProgress) {
                                                          if (loadingProgress == null) {
                                                            return child;
                                                          } else {
                                                            return Center(
                                                              child: CircularProgressIndicator(
                                                                value: loadingProgress.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded /
                                                                    (loadingProgress.expectedTotalBytes ?? 1) : null,
                                                              ),
                                                            );
                                                          }
                                                        },
                                                      ),
                                                    ) :
                                                    Container(
                                                      width: 50,
                                                      height: 50,
                                                      decoration: BoxDecoration(
                                                          border: Border.all(color: primaryColor, width: 2,),
                                                          shape: BoxShape.circle),
                                                      child: const Icon(Icons.image_not_supported),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${data.teamName}', 20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${data.stadium}', 20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${data.address}', 20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      '${data.championshipsWon}',
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Row(
                                                      children: [
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(TeamDetailScreen(data: data,));
                                                          },
                                                          child: Container(
                                                              padding:const EdgeInsets.all(6),
                                                              decoration:
                                                              BoxDecoration(
                                                                color: Colors.amber.withOpacity(0.1),
                                                                border: Border.all(
                                                                    width: 1,
                                                                    color: Colors.amberAccent
                                                                ),
                                                                borderRadius:
                                                                BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(
                                                                Icons.visibility,
                                                                size: 20,
                                                                color: Colors.amberAccent,
                                                              )),
                                                        ),
                                                        const SizedBox(width: 12),
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(CreateUpdateTeamScreen(teamData: data,));
                                                          },
                                                          child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.green.withOpacity(0.1),
                                                                border: Border.all(width: 1, color: Colors.green
                                                                ),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(Icons.edit, size: 20, color: Colors.green,)
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          width: 12,
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            showDialog(
                                                              context: context,
                                                              builder: (BuildContext
                                                              context) {
                                                                return AlertDialog(
                                                                  content:Container(
                                                                    width:Get.width * 0.3,
                                                                    height:Get.height * 0.3,
                                                                    padding:const EdgeInsets.all(16),
                                                                    child: Column(
                                                                      children: [
                                                                        Align(alignment:Alignment.topCenter,
                                                                          child: Image.asset("assets/images/warning.png", width:50, height:50,),
                                                                        ),
                                                                        const SizedBox(height:15),
                                                                        Text("are_you_sure_you_want_to_delete_this_team?".tr,
                                                                            style: Theme.of(context).textTheme.bodyMedium
                                                                        ),
                                                                        const SizedBox(height:15),
                                                                        const Spacer(),
                                                                        Row(
                                                                          crossAxisAlignment: CrossAxisAlignment.end,
                                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                                          children: [
                                                                            CancelButton(btnName: 'cancel'.tr, onPress: (){
                                                                              Get.back();
                                                                            }),
                                                                            const SizedBox(width: 16),
                                                                            OKButton(
                                                                              btnName: 'confirm'.tr,
                                                                              onPress: ()async{
                                                                                  logic.isLoading = true;
                                                                                  logic.update();
                                                                                  EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
                                                                                  var result = await logic.deleteTeam(teamID: data.id.toString());
                                                                                  if (result['success']) {
                                                                                    EasyLoading.showSuccess('team_delete_successfully'.tr, dismissOnTap: true, duration: const Duration(seconds: 2),);
                                                                                    Get.back();
                                                                                    logic.listTeam.removeAt(index);
                                                                                  } else {
                                                                                    EasyLoading.showError(result['message'], duration: const Duration(seconds: 2),);
                                                                                    Get.back();
                                                                                  }
                                                                                  EasyLoading.dismiss();
                                                                                  logic.isLoading = false;
                                                                                },
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                            );
                                                          },
                                                          child: Container(
                                                            padding: const EdgeInsets.all(6),
                                                            decoration: BoxDecoration(
                                                              color: Colors.red.withOpacity(0.1),
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: Colors.red
                                                              ),
                                                              borderRadius: BorderRadius.circular(6),
                                                            ),
                                                            child: const Icon(
                                                              Icons.delete,
                                                              size: 20,
                                                              color: Colors.red,
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (logic.listTeam.isEmpty)...[
                                      const SizedBox(height: 50,),
                                      const Center(child: EmptyData()),
                                    ]
                                  ],
                                ),
                              );
                            },
                          ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: teamController.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1;
                                  i <= teamController.totalPages;
                                  i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () =>
                                            teamController.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color:
                                                teamController.currentPage == i
                                                    ? Colors.blue
                                                    : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: teamController.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
