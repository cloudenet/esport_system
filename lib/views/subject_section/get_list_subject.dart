import 'package:esport_system/data/controllers/subject_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/util/text_style.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/subject_section/create_subject.dart';
import 'package:esport_system/views/subject_section/subject_detail.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class GetListSubject extends StatefulWidget {
  const GetListSubject({super.key});

  @override
  State<GetListSubject> createState() => _GetListSubjectState();
}

class _GetListSubjectState extends State<GetListSubject> {
  var subjectController = Get.find<SubjectController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    subjectController.getListSubject();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<SubjectController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: double.infinity,

          child: Column(
            children: [
              _buildDropDown(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/subjective.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'subject'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(alignment: Alignment.topRight,child: AddNewButton(btnName:'add_new'.tr,onPress: (){
                    homeController.updateWidgetHome(const CreateSubject());
                  })),
                ],
              ),

              const SizedBox(height: 22,),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : (logic.subjectData.isEmpty
                    ? const EmptyData()
                    : LayoutBuilder(
                  builder: (context, constraints) {
                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                            minWidth: constraints.maxWidth,
                          ),
                          child: DataTable(
                            dataRowMaxHeight: Get.height* 0.1,
                            headingRowColor: WidgetStateColor.resolveWith((states) => secondaryColor),
                            columns: [
                              DataColumn(
                                label: Text(
                                  "no.".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "title".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "max_point".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "description".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "created_by".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  "actions".tr,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                            rows: List<DataRow>.generate(
                              logic.subjectData.length,(index) {
                                var data = logic.subjectData[index];
                                return DataRow(
                                  cells: [
                                    DataCell(
                                      Text(_truncateText('${index + 1}', 10)),
                                    ),
                                    DataCell(
                                      Text(
                                        _truncateText('${data.title}', 20),
                                        style: TextStylesHelper.khmerText16.copyWith(fontSize: 15),
                                      ),
                                    ),
                                    DataCell(
                                      Text(
                                        _truncateText('${data.maxPoint}', 20),
                                      ),
                                    ),
                                    DataCell(
                                      Text(
                                        _truncateText((data.description?.isNotEmpty ?? false) ? data.description! : "-", 20),
                                        style: TextStylesHelper.khmerText16.copyWith(fontSize: 15),
                                      ),
                                    ),
                                    DataCell(
                                      Text(
                                        _truncateText('${data.createdBy!.name}',20),
                                      ),
                                    ),

                                    DataCell(
                                      Row(
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              homeController.updateWidgetHome(SubjectDetail(data: data));
                                            },
                                            child: Container(
                                                padding:
                                                const EdgeInsets
                                                    .all(6),
                                                decoration:
                                                BoxDecoration(
                                                  color: Colors.amberAccent.withOpacity(0.1),
                                                  border: Border.all(width: 1,color: Colors.amberAccent),
                                                  borderRadius:
                                                  BorderRadius
                                                      .circular(
                                                      6),
                                                ),
                                                child: const Icon(
                                                  Icons.visibility,
                                                  size: 20,
                                                  color: Colors.amberAccent,
                                                )),
                                          ),
                                          const SizedBox(
                                            width: 12,
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              homeController.updateWidgetHome(CreateSubject(data: data,));

                                            },
                                            child: Container(
                                                padding:
                                                const EdgeInsets
                                                    .all(6),
                                                decoration:
                                                BoxDecoration(
                                                  color: Colors.green.withOpacity(0.1),
                                                  border: Border.all(width: 1,color: Colors.green),
                                                  borderRadius:
                                                  BorderRadius
                                                      .circular(
                                                      6),
                                                ),
                                                child: const Icon(
                                                  Icons.edit,
                                                  size: 20,
                                                  color: Colors.green,
                                                )),
                                          ),
                                          const SizedBox(
                                            width: 12,
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext
                                                context) {
                                                  return AlertDialog(
                                                    content:Container(
                                                      width:Get.width * 0.3,
                                                      height:Get.height * 0.3,
                                                      padding:const EdgeInsets.all(16),
                                                      child: Column(
                                                        children: [
                                                          Align(alignment:Alignment.topCenter,
                                                            child: Image.asset(
                                                              "assets/images/warning.png",
                                                              width:50,
                                                              height:50,
                                                            ),
                                                          ),
                                                          const SizedBox(height:15),
                                                          Text(
                                                              "are_you_sure_to_delete_this_subject?".tr,
                                                              style: Theme.of(context).textTheme.bodyMedium
                                                          ),
                                                          const SizedBox(height:15),
                                                          const Spacer(),
                                                          Row(
                                                            crossAxisAlignment:
                                                            CrossAxisAlignment.end,
                                                            mainAxisAlignment:
                                                            MainAxisAlignment.end,
                                                            children: [

                                                              CancelButton(
                                                                  btnName: 'cancel'.tr,
                                                                  onPress: (){
                                                                    Get.back();
                                                                  }),
                                                              const SizedBox(
                                                                  width: 10),

                                                              OKButton(
                                                                  btnName: 'confirm'.tr,
                                                                  onPress: () async{
                                                                    // EasyLoading.show(
                                                                    //   status: 'Loading...'.tr,
                                                                    //   dismissOnTap: true,
                                                                    // );
                                                                    // var result = await logic.deletePosition(positionId: data.id.toString());
                                                                    // if (result['success'.tr]) {
                                                                    //   EasyLoading.showSuccess(
                                                                    //     'Position delete successfully'.tr,
                                                                    //     dismissOnTap: true,
                                                                    //     duration: const Duration(seconds: 2),
                                                                    //   );
                                                                    //   Get.back();
                                                                    //   logic.getPosition();
                                                                    // } else {
                                                                    //   EasyLoading.showError(
                                                                    //     result['message'.tr],
                                                                    //     duration: const Duration(seconds: 2),
                                                                    //   );
                                                                    //   Get.back();
                                                                    // }
                                                                    // EasyLoading.dismiss();
                                                                  })
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                },
                                              );
                                            },
                                            child: Container(
                                              padding:
                                              const EdgeInsets
                                                  .all(6),
                                              decoration:
                                              BoxDecoration(
                                                color: Colors.red.withOpacity(0.1),
                                                border: Border.all(width: 1,color: Colors.red),
                                                borderRadius:
                                                BorderRadius
                                                    .circular(6),
                                              ),
                                              child: const Icon(
                                                Icons.delete,
                                                size: 20,
                                                color: Colors.red,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                )),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: logic.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= logic.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () => logic.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color: logic.currentPage == i
                                                ? Colors.blue
                                                : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: subjectController.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildDropDown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: subjectController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  subjectController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              subjectController.search(value);
            }, hintText: 'search_subject_title'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
