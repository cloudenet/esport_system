import 'package:esport_system/data/controllers/subject_controller.dart';
import 'package:esport_system/data/model/subject_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/subject_section/get_list_subject.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CreateSubject extends StatefulWidget {
  final SubjectModel? data;

  const CreateSubject({super.key, this.data});

  @override
  State<CreateSubject> createState() => _CreateSubjectState();
}

class _CreateSubjectState extends State<CreateSubject> {
  final _formKey = GlobalKey<FormBuilderState>();
  var subjectController = Get.find<SubjectController>();
  var homeController = Get.find<HomeController>();


  @override
  void initState() {
    if (widget.data != null) {
      subjectController.getOldValue(widget.data!);
    } else {
      subjectController.clearValue();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<SubjectController>(builder: (logic) {
        return Container(
          width: Get.width,
          padding: const EdgeInsets.all(24.0),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FormBuilder(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.data != null
                              ? "update_subject".tr
                              : 'create_a_new_subject'.tr,
                          style: const TextStyle(
                              fontSize: 18.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 20.0),
                        Row(
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'title'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  CustomFormBuilderTextField(
                                    name: 'title'.tr,
                                    controller: logic.titleController,
                                    hintText: 'title'.tr,
                                    errorText: 'title_is_required'.tr,
                                    icon: Icons.text_fields,
                                    fillColor: secondaryColor,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(
                              width: 16,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'max_point'.tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  CustomFormBuilderTextField(
                                    name: 'max_point'.tr,
                                    controller: logic.maxPointController,
                                    hintText: 'max_point'.tr,
                                    errorText: 'max_point_is_required'.tr,
                                    icon: Icons.text_fields,
                                    fillColor: secondaryColor,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'description'.tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            CustomFormNote(
                              name: 'description'.tr,
                              controller: logic.desController,
                              hintText: 'description_can_be_optional'.tr,
                              icon: Icons.description,
                              fillColor: secondaryColor,
                            ),
                          ],
                        ),

                        const SizedBox(height: 24.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            CancelButton(
                                btnName: 'cancel'.tr,
                                onPress: () {
                                  homeController.updateWidgetHome(
                                      const GetListSubject());
                                }),
                            const SizedBox(
                              width: 16,
                            ),
                            OKButton(
                                btnName: widget.data != null
                                    ? 'update'
                                    : 'save'.tr,
                                onPress: () async {
                                  if (_formKey.currentState != null &&
                                      _formKey.currentState!
                                          .saveAndValidate()) {
                                    EasyLoading.show(
                                      status: 'loading...'.tr,
                                      dismissOnTap: true,
                                    );
                                    final description = logic.desController.text.isEmpty
                                        ? '' // Send an empty string if no description
                                        : logic.desController.text;

                                    if (widget.data != null) {
                                      await logic.updateSubject(
                                          subId: widget.data!.id.toString(),
                                          title: logic.titleController.text,
                                          maxPoint: logic.maxPointController.text,
                                          des: description,
                                          updatedBy: userInfo!.id.toString())
                                          .then((value) {
                                        if (value) {
                                          EasyLoading.showSuccess(
                                            'subject_update_successfully'.tr,
                                            dismissOnTap: true,
                                            duration: const Duration(seconds: 2),
                                          );
                                          logic.clearValue();
                                          homeController.updateWidgetHome(
                                              const GetListSubject());
                                        } else {
                                          EasyLoading.showError(
                                              'something_went_wrong'.tr,
                                              duration: const Duration(seconds: 2));
                                        }
                                      });
                                    } else {
                                      await logic.createSubject(
                                          title: logic.titleController.text,
                                          maxPoint: logic.maxPointController.text,
                                          des: description)
                                          .then((val) {
                                        if (val) {
                                          EasyLoading.showSuccess(
                                            'subject_created_successfully'.tr,
                                            dismissOnTap: true,
                                            duration: const Duration(seconds: 2),
                                          );
                                          logic.clearValue();
                                          homeController.updateWidgetHome(
                                              const GetListSubject());
                                        } else {
                                          EasyLoading.showError(
                                              'something_went_wrong'.tr,
                                              duration: const Duration(seconds: 2));
                                        }
                                      });
                                    }
                                  }
                                }),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
class CustomFormNote extends StatelessWidget {
  final String name;
  final TextEditingController controller;
  final String hintText;
  final String? errorText;
  final IconData? icon;
  final int? minLine;
  final Color fillColor;
  final VoidCallback? onTap;
  final bool? readOnly;

  const CustomFormNote({
    Key? key,
    required this.name,
    required this.controller,
    required this.hintText,
    this.errorText,
    this.icon,
    this.minLine,
    required this.fillColor,
    this.onTap,
    this.readOnly,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FormBuilderTextField(
      onTap: onTap,
      name: name,
      controller: controller,
      readOnly: readOnly ?? false,
      maxLines: null,
      decoration: InputDecoration(
        prefixIcon: Icon(icon),
        hintText: hintText,
        fillColor: fillColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
      ),
      validator: FormBuilderValidators.compose([
        if (errorText != null) FormBuilderValidators.required(errorText: errorText),
      ]),
    );
  }
}

