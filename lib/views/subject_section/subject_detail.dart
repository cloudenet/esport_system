import 'package:esport_system/data/model/subject_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/subject_section/get_list_subject.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class SubjectDetail extends StatefulWidget {
  final SubjectModel data;
  const SubjectDetail({super.key, required this.data});

  @override
  State<SubjectDetail> createState() => _SubjectDetailState();
}

class _SubjectDetailState extends State<SubjectDetail> {
  var homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Responsive(
      tablet: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: secondaryColor,
          ),
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'subject_detail'.tr,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 24),
              buildText(
                title: 'subject_title'.tr,
                textData: widget.data.title.toString(),
                height: 50,
                padding: 10,
                alignment: Alignment.centerLeft,
              ),
              const SizedBox(height: 16),
              buildText(
                title: 'max_point'.tr,
                textData: widget.data.maxPoint.toString(),
                height: 50,
                padding: 10,
                alignment: Alignment.centerLeft,
              ),
              const SizedBox(height: 16),
              if (widget.data.description?.isNotEmpty == true)
                buildText(
                  title: "description",
                  textData: widget.data.description!,
                  height: 70,
                  padding: 10,
                ),
              const Spacer(),// This replaces Spacer()
              Align(
                alignment: Alignment.bottomRight,
                child: CancelButton(
                  btnName: 'close'.tr,
                  onPress: () {
                    homeController.updateWidgetHome(
                        const GetListSubject());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      mobile: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: secondaryColor,
          ),
          padding: const EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'subject_detail'.tr,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 24),
              buildText(
                title: 'subject_title'.tr,
                textData: widget.data.title.toString(),
                height: 50,
                padding: 10,
                alignment: Alignment.centerLeft,
              ),
              const SizedBox(height: 16),
              buildText(
                title: 'max_point'.tr,
                textData: widget.data.maxPoint.toString(),
                height: 50,
                padding: 10,
                alignment: Alignment.centerLeft,
              ),
              const SizedBox(height: 16),
              if (widget.data.description?.isNotEmpty == true)
                buildText(
                  title: "description",
                  textData: widget.data.description!,
                  height: 70,
                  padding: 10,
                ),
              const Spacer(),// This replaces Spacer()
              Align(
                alignment: Alignment.bottomRight,
                child: CancelButton(
                  btnName: 'close'.tr,
                  onPress: () {
                    homeController.updateWidgetHome(
                        const GetListSubject());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      desktop: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 150,
                width: 150,
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: Image.asset(
                  'assets/icons/subjective.png',
                  color: Colors.white,
                ),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'subject_detail'.tr,
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(height: 24),
                      Row(
                        children: [
                          Expanded(
                            child: buildText(
                              title: 'subject_title'.tr,
                              textData: widget.data.title.toString(),
                              height: 50,
                              padding: 10,
                              alignment: Alignment.centerLeft,
                            ),
                          ),
                          const SizedBox(width: 16),
                          Expanded(
                            child: buildText(
                              title: 'max_point'.tr,
                              textData: widget.data.maxPoint.toString(),
                              height: 50,
                              padding: 10,
                              alignment: Alignment.centerLeft,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      if (widget.data.description?.isNotEmpty == true)
                        buildText(
                          title: "description",
                          textData: widget.data.description!,
                          height: 70,
                          padding: 10,
                        ),
                      const SizedBox(height: 16), // This replaces Spacer()
                      Align(
                        alignment: Alignment.bottomRight,
                        child: CancelButton(
                          btnName: 'close'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(
                                const GetListSubject());
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
