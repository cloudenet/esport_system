import 'package:cached_network_image/cached_network_image.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/util/text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/controllers/team_controller.dart';
import '../../../data/model/list_user_model.dart';
import '../../../helper/app_contrain.dart';
import '../../../helper/constants.dart';
import '../../../helper/detail_container.dart';

import '../../../helper/images.dart';
import '../../../helper/khmer_calender.dart';

class UserCardDetail extends StatefulWidget {
  final ListUserModel? list;

  const UserCardDetail({super.key, required this.list});

  @override
  State<UserCardDetail> createState() => _UserCardDetailState();
}

class _UserCardDetailState extends State<UserCardDetail> {
  var teamController = Get.find<TeamController>();
  var userController = Get.find<UserController>();
  String? dobKhmer;

  String? expireKhmer;

  @override
  void initState() {
    convertKhmer();
    super.initState();
  }

  void convertKhmer() async {
    if (widget.list != null) {
      DateTime dateTime = DateTime.parse(widget.list!.dob.toString());
      dateTime = DateTime(dateTime.year, dateTime.month, dateTime.day);
      dobKhmer = convertToKhmerDate(dateTime);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                    //flex: 5,
                    child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    color: secondaryColor,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(
                          height: 100,
                        ),
                        Row(
                          children: [
                            Align(
                              alignment: Alignment.topLeft,
                              child: SizedBox(
                                height: 90,
                                child: widget.list?.photo != "null"
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(100),
                                        child: CachedNetworkImage(
                                          height: 90,
                                          width: 90,
                                          fit: BoxFit.cover,
                                          imageUrl: "${AppConstants.baseUrl}${widget.list?.photo ?? ""}",
                                        ),
                                      )
                                    : SizedBox(
                                        height: 90,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(100),
                                          child: Image.asset(
                                            "assets/images/no-image.png",
                                            fit: BoxFit.cover,
                                            width: 90,
                                            height: 90,
                                          ),
                                        ),
                                      ),
                              ),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(widget.list!.name.toString(),
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineLarge
                                        ?.copyWith(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold)),
                                const SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  widget.list!.role.toString(),
                                  style: Theme.of(context).textTheme.bodyLarge,
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  width: 100,
                                  height: 30,
                                  decoration: BoxDecoration(
                                      color: bgColor,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        width: 10,
                                        height: 10,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: widget.list?.userStatus == 'active'
                                              ? Colors.green
                                              : Colors.red,
                                        ),
                                      ),
                                      Text(widget.list!.userStatus.toString())
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Divider(
                          height: 3,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "User Information",
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineLarge
                                  ?.copyWith(fontSize: 18),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: buildContainer(
                                        title: "UserName:",
                                        data: widget.list!.username.toString(),
                                        icon: Icons.person,
                                        context: context)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: buildContainer(
                                        title: "Nationality:",
                                        data: widget.list!.nationality.toString(),
                                        icon: Icons.flag,
                                        context: context)),
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: buildContainer(
                                        title: "Phone:",
                                        data: widget.list!.phone.toString(),
                                        icon: Icons.phone,
                                        context: context)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: buildContainer(
                                        title: "Card Number:",
                                        data: widget.list!.cardNumber.toString(),
                                        icon: Icons.credit_card_outlined,
                                        context: context))
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: buildContainer(
                                        title: "Identity No:",
                                        data: widget.list!.identityNo.toString(),
                                        icon: Icons.credit_card_outlined,
                                        context: context)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                    child: buildContainer(
                                        title: "Email:",
                                        data: widget.list!.email.toString(),
                                        icon: Icons.mail,
                                        context: context))
                              ],
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: buildContainer(
                                        title: "Gender:",
                                        data: widget.list!.gender.toString(),
                                        icon: Icons.male_sharp,
                                        context: context)),
                                const SizedBox(
                                  width: 10,
                                ),
                                Expanded(
                                  child: buildContainer(
                                      title: "DOB:",
                                      data: widget.list!.dob.toString(),
                                      icon: Icons.cake_sharp,
                                      context: context),
                                )
                              ],
                            ),
                            // const SizedBox(
                            //   height: 10,
                            // ),
                            // buildContainer(
                            //     title: "Address",
                            //     data: list.address,
                            //     icon: Icons.location_on,
                            //     context: context),
                            const SizedBox(
                              height: 16,
                            ),
                            Text(
                              "User Level Control",
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineLarge
                                  ?.copyWith(fontSize: 18),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            if (widget.list!.level == "village") ...[
                              Row(
                                children: [
                                  Expanded(
                                      child: buildContainer(
                                          title: "Province:",
                                          data: widget
                                                  .list?.addressControlOf?.province ??
                                              "N/A",
                                          icon: Icons.location_on,
                                          context: context)),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: buildContainer(
                                        title: "District:",
                                        data:
                                            widget.list?.addressControlOf?.district ??
                                                "N/A",
                                        icon: Icons.location_on,
                                        context: context),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: buildContainer(
                                          title: "Commune:",
                                          data: widget
                                                  .list?.addressControlOf?.commune ??
                                              "N/A",
                                          icon: Icons.location_on,
                                          context: context)),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: buildContainer(
                                        title: "Village:",
                                        data:
                                            widget.list?.addressControlOf?.village ??
                                                "N/A",
                                        icon: Icons.location_on,
                                        context: context),
                                  )
                                ],
                              ),
                            ]
                            else if (widget.list!.level == "commune") ...[
                              Row(
                                children: [
                                  Expanded(
                                      child: buildContainer(
                                          title: "Province:",
                                          data: widget
                                                  .list?.addressControlOf?.province ??
                                              "N/A",
                                          icon: Icons.location_on,
                                          context: context)),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: buildContainer(
                                        title: "District:",
                                        data:
                                            widget.list?.addressControlOf?.district ??
                                                "N/A",
                                        icon: Icons.location_on,
                                        context: context),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              Center(
                                child: Expanded(
                                    child: buildContainer(
                                        title: "Commune:",
                                        data:
                                            widget.list?.addressControlOf?.commune ??
                                                "N/A",
                                        icon: Icons.location_on,
                                        context: context)),
                              ),
                            ]
                            else if (widget.list!.level == "district") ...[
                              Row(
                                children: [
                                  Expanded(
                                      child: buildContainer(
                                          title: "Province:",
                                          data: widget
                                                  .list?.addressControlOf?.province ??
                                              "N/A",
                                          icon: Icons.location_on,
                                          context: context)),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: buildContainer(
                                        title: "District:",
                                        data:
                                            widget.list?.addressControlOf?.district ??
                                                "N/A",
                                        icon: Icons.location_on,
                                        context: context),
                                  )
                                ],
                              ),
                            ]
                            else ...[
                              Center(
                                child: Expanded(
                                    child: buildContainer(
                                        title: "Province:",
                                        data:
                                            widget.list?.addressControlOf?.province ??
                                                "N/A",
                                        icon: Icons.location_on,
                                        context: context)),
                              ),
                            ],
                          ],
                        ),
                        if(Responsive.isMobile(context))
                        _buildCard()
                      ],
                    ),
                  ),
                )),
                if(Responsive.isDesktop(context))
                  _buildCard()

              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget contactInfoRow(IconData icon, String text,
      {required bool? twoIcon, IconData? icon2}) {
    return Row(
      children: [
        twoIcon!
            ? Row(
                children: [
                  Icon(icon, color: Colors.indigo),
                  const SizedBox(
                    width: 2,
                  ),
                  Icon(icon2, color: Colors.indigo)
                ],
              )
            : Icon(icon, color: Colors.indigo),
        const SizedBox(width: 8.0),
        Text(
          text,
          style: const TextStyle(color: Colors.indigo, fontSize: 12),
        ),
      ],
    );
  }
Widget _buildCard(){
    return Container(

      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text("Save Image"),
              IconButton(
                onPressed: () async {
                  await userController.renderWidgetsToImages([
                    userController.globalKey,
                    userController.globalKeyBack
                  ]);
                },
                icon: const Icon(Icons.save_alt),
              ),

            ],
          ),
          const SizedBox(
            height: 10,
          ),
          RepaintBoundary(
            key: userController.globalKey,
            child: Container(
           width: !Responsive.isDesktop(context)?MediaQuery.of(context).size.width * 0.9:MediaQuery.of(context).size.width * 0.3,
             // height: !Responsive.isDesktop(context)?MediaQuery.of(context).size.width * 0.5:MediaQuery.of(context).size.width * 0.3,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 5,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Container(
                      decoration: const BoxDecoration(
                          color: Colors.indigo,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                          )),
                      // height: MediaQuery.of(context).size.height * 0.07,
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("ប័ណ្ណសម្គាល់ខ្លួន",
                                      style: TextStylesHelper.khmerH2),
                                  Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា",
                                      style: TextStylesHelper.khmerH2)
                                ],
                              )),
                          const Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Federation of Boules and Pétanque of Cambodia",
                                    style: TextStyle(
                                        fontSize: 9,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text("Identity Card",
                                      style: TextStyle(
                                          fontSize: 9,
                                          fontWeight: FontWeight.bold))
                                ],
                              )),
                        ],
                      )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'No: ${widget.list?.cardNumber.toString()}',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.red,
                              fontSize: 12),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Expanded(
                              flex: 5,
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  khmerText(
                                      sureName: 'គោត្តនាម និងនាម',
                                      name: widget.list?.nameKhmer!.isNotEmpty ==true ? widget.list!.nameKhmer.toString():"N/A"  ),
                                  const SizedBox(
                                    height: 3,
                                  ),
                                  englishText(
                                      sureName: 'Surename & Name ',
                                      name: widget.list?.name!.isNotEmpty ==true ? widget.list!.name.toString():"N/A"  ),
                                  const SizedBox(
                                    height: 3,
                                  ),
                                  khmerText(
                                      sureName: 'ថ្ងៃខែឆ្នាំកំណើត',
                                      name: dobKhmer ?? "N/A"),
                                  const SizedBox(
                                    height: 3,
                                  ),
                                  englishText(
                                      sureName: 'Date of Birth',
                                      name:
                                      '${widget.list?.dob.toString()}'),
                                  const SizedBox(
                                    height: 3,
                                  ),
                                  khmerText(
                                      sureName: 'តួនាទី',
                                      name: 'មន្ត្រី'),
                                  const SizedBox(
                                    height: 3,
                                  ),
                                  englishText(
                                      sureName: 'Position',
                                      name: 'Officer'),
                                ],
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 5,
                                    child: Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        khmerText(
                                            sureName: 'ភេទ',
                                            name: 'ប្រុស'),
                                        const SizedBox(
                                          height: 3,
                                        ),
                                        englishText(
                                            sureName: 'Sex',
                                            name:
                                            '${widget.list?.gender.toString()}'),
                                        const SizedBox(
                                          height: 3,
                                        ),
                                        khmerText(
                                            sureName: 'សញ្ជាតិ',
                                            name: 'ខ្មែរ'),
                                        const SizedBox(
                                          height: 3,
                                        ),
                                        englishText(
                                            sureName: 'National',
                                            name:
                                            '${widget.list?.nationality != "null" ? widget.list?.nationality : "N/A" }'),
                                        const SizedBox(
                                          height: 3,
                                        ),
                                        khmerText(
                                            sureName: 'សុពលភាព',
                                            name: '២៥/មេសា/២០២៥'),
                                        const SizedBox(
                                          height: 3,
                                        ),
                                        englishText(
                                            sureName: 'Expire Date',
                                            name: '25/04/2025'),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Image.asset(
                                      Images.userCardImage,
                                      width: 110,
                                      height: 120,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 180),
                          child: Align(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា",
                                    style: TextStylesHelper.khmerH3),
                                Text("ប្រធាន",
                                    style: TextStylesHelper.khmerH3),
                                Image.network(
                                  'https://static.vecteezy.com/system/resources/previews/000/538/077/original/manual-signature-for-documents-on-white-background-hand-drawn-calligraphy-lettering-vector-illustration.jpg',
                                  width: 200,
                                  height: 50,
                                ),
                                Text("សុខ សូកាន",
                                    style: TextStylesHelper.khmerH3),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          RepaintBoundary(
            key: userController.globalKeyBack,
            child: Container(
              width: !Responsive.isDesktop(context)?MediaQuery.of(context).size.width * 0.9:MediaQuery.of(context).size.width * 0.3,
              height: 322,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 5,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            const SizedBox(
                              height: 10,
                            ),
                            Image.asset(
                              'assets/images/Boules.png',
                              height: 100,
                            ),
                            const SizedBox(height: 10.0),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0),
                              child: Column(
                                children: [
                                  Text(
                                    'សហព័ន្ធកីឡាប៊ុល និងប៉េតង់កម្ពុជា',
                                    style: TextStylesHelper.khmerH1,
                                    textAlign: TextAlign.center,
                                  ),
                                  const Text(
                                    'Federation of Boules and Pétanque of Cambodia',
                                    style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.indigo,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 16.0),
                            Container(
                              padding:
                              const EdgeInsets.symmetric(horizontal: 24),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      contactInfoRow(Icons.email,
                                          'petanquecambodia@gmail.com',
                                          twoIcon: false),
                                      contactInfoRow(
                                          Icons.phone, '(+855) 78 239 177',
                                          twoIcon: true,
                                          icon2: Icons.telegram),
                                    ],
                                  ),
                                  const SizedBox(height: 5.0),
                                  contactInfoRow(Icons.facebook,
                                      'www.facebook.com/PetanqueCambodia',
                                      twoIcon: false),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )),
                  Container(
                    height: 60,
                    padding: const EdgeInsets.all(.0),
                    decoration: const BoxDecoration(
                        color: Colors.indigo,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Icon(Icons.location_on,
                            color: Colors.white),
                        const SizedBox(width: 8.0),
                        Flexible(
                          child: Text(
                            'ពហុកីឡដ្ឋានជាតិ សង្កាត់វាលវង់ ខណ្ឌ៧មករា រាជធានីភ្នំពេញ',
                            textAlign: TextAlign.center,
                            style: TextStylesHelper.khmerH2,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
}
  Widget khmerText({required String sureName, required String name}) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Text(sureName, style: TextStylesHelper.khmerTextNormal),
        ),
        Expanded(
          flex: 4,
          child: Text(name, style: TextStylesHelper.khmerTextMoul),
        ),
      ],
    );
  }

  Widget englishText({required String sureName, required String name}) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Text(
            sureName,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.indigo, fontSize: 9),
          ),
        ),
        Expanded(
          flex: 4,
          child: Text(
            name,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.indigo,
              fontSize: 9,
            ),
          ),
        ),
      ],
    );
  }
}
