import 'package:esport_system/data/controllers/league_controller.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/helper/image_picker_field.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/user_section/screen/get_list_user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import '../../../data/controllers/home_controller.dart';
import '../../../data/controllers/user_controller.dart';
import '../../../data/model/address_model.dart';
import '../../../helper/add_new_button.dart';
import '../../../helper/constants.dart';
class CreateUser extends StatefulWidget {
  final ListUserModel? userModel;
  final bool updateScreen;

  const CreateUser({super.key, this.userModel, required this.updateScreen});

  @override
  State<CreateUser> createState() => _CreateUpdateUserScreenState();
}

class _CreateUpdateUserScreenState extends State<CreateUser> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool isVisible = true;
  UserController userController = Get.find<UserController>();
  LeagueController leagueController = Get.find<LeagueController>();
  HomeController homeController = Get.find<HomeController>();


  // Define FocusNodes
  final FocusNode cardNumberFocusNode = FocusNode();
  final FocusNode userNameFocusNode = FocusNode();
  final FocusNode phoneFocusNode = FocusNode();
  final FocusNode nameFocusNode = FocusNode();
  final FocusNode khNameFocusNode = FocusNode();
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode passwordFocusNode = FocusNode();
  final FocusNode dobFocusNode = FocusNode();
  final FocusNode addressFocusNode = FocusNode();
  final FocusNode nationalFocusNode = FocusNode();
  final FocusNode identityFocusNode = FocusNode();

  @override
  void initState() {
    userController.clearTextField();
    userController.fetchProvinces();
    if (widget.userModel != null) {
      userController.setOldValue(widget.userModel!);
    }
    super.initState();
  }

  @override
  void dispose() {
    // Dispose FocusNodes
    cardNumberFocusNode.dispose();
    userNameFocusNode.dispose();
    nameFocusNode.dispose();
    phoneFocusNode.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();
    dobFocusNode.dispose();
    addressFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<UserController>(builder: (logic) {
        return Container(
          width: double.infinity,
          padding: const EdgeInsets.all(24.0),
          child: Responsive(
            mobile: bodyMobile(),
            tablet: body(tablet: true),
            desktop: body(),
          ),
        );
      }),
    );
  }

  Widget body({bool tablet = false}) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: GetBuilder<UserController>(builder: (logic) {
        return FormBuilder(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 20,),
                  Text(
                    'create_user'.tr,
                    style: const TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(height: 20,),
                  Row(
                    children: [
                      Expanded(
                        child: _buildTextField(
                          title: 'card_number'.tr,
                          hintext: 'card_number'.tr,
                          controller: logic.cardNameController,
                          icon: Icons.sd_card,
                          focusNode: cardNumberFocusNode,
                          nextFocusNode: nameFocusNode,
                        ),
                      ),
                      Expanded(
                        child: _buildTextField(
                          title: 'name'.tr,
                          hintext: 'name'.tr,
                          controller: logic.nameController,
                          icon: Icons.people_rounded,
                          focusNode: nameFocusNode,
                          nextFocusNode: userNameFocusNode,
                        ),
                      ),
                      Expanded(
                        child: _buildTextField(
                          title: 'username'.tr,
                          hintext: 'username'.tr,
                          controller: logic.userNameController,
                          icon: Icons.people_rounded,
                          focusNode: userNameFocusNode,
                          nextFocusNode: khNameFocusNode,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: _buildTextField(
                          title: 'khmer_name'.tr,
                          hintext: 'khmer_name'.tr,
                          controller: logic.khmerNameController,
                          icon: Icons.people_rounded,
                          focusNode: khNameFocusNode,
                          nextFocusNode: phoneFocusNode,
                        ),
                      ),
                      Expanded(
                        child: _buildTextField(
                          title: 'phone'.tr,
                          hintext: 'phone'.tr,
                          controller: logic.phoneController,
                          icon: Icons.phone,
                          focusNode: phoneFocusNode,
                          nextFocusNode: emailFocusNode,
                        ),
                      ),
                      Expanded(
                        child: _buildTextField(
                          title: 'email'.tr,
                          hintext: 'email'.tr,
                          controller: logic.emailController,
                          icon: Icons.email,
                          focusNode: emailFocusNode,
                          nextFocusNode: passwordFocusNode,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: _buildPasswordField(
                          logic: logic,
                          isVisible: isVisible,
                          focusNode: passwordFocusNode,
                          nextFocusNode: dobFocusNode,
                          onVisibilityChanged: () {
                            setState(() {
                              isVisible = !isVisible;
                            });
                          },
                        ),
                      ),
                      Expanded(
                        child: _buildDateField(
                          title: 'date_of_birth'.tr,
                          context: context,
                          logic: logic,
                          controller: logic.dayofbirthdayController,
                          focusNode: dobFocusNode,
                          nextFocusNode: addressFocusNode,
                        ),
                      ),
                      Expanded(
                        child: _buildTextField(
                          title: 'address'.tr,
                          hintext: 'address'.tr,
                          controller: logic.addressController,
                          icon: CupertinoIcons.map_pin_ellipse,
                          focusNode: addressFocusNode,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('gender'.tr),
                              FormBuilderDropdown(
                                name: 'type'.tr,
                                initialValue: logic.selectGender,
                                borderRadius: BorderRadius.circular(10),
                                decoration: InputDecoration(
                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                  fillColor: secondaryColor,
                                  labelText: 'please_select_gender'.tr,
                                  filled: true,
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                ),
                                items: logic.typeGender.map((option) =>
                                    DropdownMenuItem(
                                      value: option['value'],
                                      child: Text(option['text']!),
                                    )).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    logic.selectGender = value!;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 4, vertical: 8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('province'.tr),
                              _buildLocationSelector(
                                context,
                                'province'.tr,
                                userController.provinces,
                                userController.selectedProvince?.name ??
                                    'please_select_province'.tr,
                                userController.selectProvince,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('district'.tr,),
                              _buildLocationSelector(
                                context,
                                'district'.tr,
                                userController.districts,
                                userController.selectedDistrict?.name ??
                                    'please_select_district'.tr,
                                userController.selectDistrict,
                                enabled: userController.selectedProvince !=
                                    null,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        // Spacing between the two fields
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('commune'.tr),
                              _buildLocationSelector(
                                context,
                                'commune'.tr,
                                userController.communes,
                                userController.selectedCommune?.name ??
                                    'please_select_commune'.tr,
                                userController.selectCommune,
                                enabled: userController.selectedDistrict !=
                                    null,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('village'.tr),
                              _buildLocationSelector(
                                context,
                                'village',
                                userController.villages,
                                userController.selectedVillage?.name ??
                                    'please_select_village'.tr,
                                userController.selectVillage,
                                enabled: userController.selectedCommune != null,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 10,),
                  if(tablet)...[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 310,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: ImagePickerField(
                              imageUrls: "",
                              title: 'select_profile'.tr,
                              hintText: logic.selectedImageUser != null ? logic.selectedImageUser!.path : "no_image_selected".tr,
                              imageFile: logic.selectedImageUser,
                              onImageSelected: () {
                                return logic.pickImage().then((v) {
                                  if (v != null) {
                                    logic.selectedImageUser = v;
                                    logic.update();
                                  }
                                });
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 20,),
                        SizedBox(
                          width: 310,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: ImagePickerField(
                              imageUrls: "",
                              title: 'select_front_identity'.tr,
                              hintText: logic.selectedFrontId != null ? logic.selectedFrontId!.path : "no_image_selected".tr,
                              imageFile: logic.selectedFrontId,
                              onImageSelected: () {
                                return logic.pickImage().then((v) {
                                  logic.selectedFrontId = v;
                                  logic.update();
                                });
                              },
                            ),
                          ),
                        ),
                        const SizedBox(width: 20,),
                        SizedBox(
                          width: 310,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 4),
                            child: ImagePickerField(
                              imageUrls: "",
                              title: 'select_back_identity'.tr,
                              hintText: logic.selectedBackID != null ? logic.selectedBackID!.path : "no_image_selected".tr,
                              imageFile: logic.selectedBackID,
                              onImageSelected: () {
                                return logic.pickImage().then((v) {
                                  logic.selectedBackID = v;
                                  logic.update();
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ]
                  else...[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ImagePickerWidget(
                          title: "upload_profile_picture".tr,
                          previousImg: "",
                          height: 150,
                          width: 310,
                          onImagePicked: (image) {
                            // Handle picked image for Profile Section
                            if (image != null) {
                              logic.selectedImageUser = image ;
                              print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                            } else {
                              logic.selectedImageUser = null;
                              print("Profile Picture Cleared");
                            }
                          },
                        ),
                        const SizedBox(width: 20,),
                        ImagePickerWidget(
                          title: "select_front_identity".tr,
                          previousImg: "",
                          height: 150,
                          width: 310,
                          onImagePicked: (image) {
                            // Handle picked image for Profile Section
                            if (image != null) {
                              if(logic.selectedFrontId!=null){
                                logic.listIdentity.remove(logic.selectedFrontId);
                              }
                              logic.selectedFrontId = image ;
                              logic.listIdentity.add(image);
                              print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                            } else {
                              if(logic.selectedFrontId!=null){
                                logic.listIdentity.remove(logic.selectedFrontId);
                              }
                              print("Profile Picture Cleared");
                            }
                          },
                        ),
                        const SizedBox(width: 20,),
                        ImagePickerWidget(
                          title: "select_back_identity".tr,
                          previousImg: "",
                          height: 150,
                          width: 310,
                          onImagePicked: (image) {
                            // Handle picked image for Profile Section
                            if (image != null) {
                              if(logic.selectedBackID!=null){
                                logic.listIdentity.remove(logic.selectedBackID);
                              }
                              logic.selectedBackID = image ;
                              logic.listIdentity.add(image);
                              print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                            } else {
                              if(logic.selectedBackID!=null){
                                logic.listIdentity.remove(logic.selectedBackID);
                              }
                              print("Profile Picture Cleared");
                            }
                          },
                        ),
                      ],
                    ),
                  ],
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            CancelButton(
                                btnName: "cancel".tr,
                                onPress: () {
                                  for(int i=0 ; i<logic.listIdentity.length ; i++){
                                    print('------Image$i ${logic.listIdentity[i]?.name}');
                                  }
                                  logic.clearTextField();
                                  homeController.updateWidgetHome(const GetUserList());
                                }
                            ),
                            const SizedBox(width: 16,),
                            OKButton(
                                btnName: "create".tr,
                                onPress: () async {
                                  if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                    if (logic.cardNameController.text.isNotEmpty &&
                                        logic.userNameController.text.isNotEmpty &&
                                        logic.khmerNameController.text.isNotEmpty &&
                                        logic.phoneController.text.isNotEmpty &&
                                        logic.emailController.text.isNotEmpty &&
                                        logic.passwordController.text.isNotEmpty &&
                                        logic.selectGender.isNotEmpty &&
                                        logic.dayofbirthdayController.text.isNotEmpty &&
                                        logic.addressController.text.isNotEmpty &&
                                        logic.nameController.text.isNotEmpty &&
                                        logic.selectedProvince != null &&
                                        logic.selectedDistrict != null &&
                                        logic.selectedCommune != null &&
                                        logic.selectedVillage != null &&
                                        logic.selectedImageUser != null
                                    ) {
                                      await logic.createUser();
                                    } else {
                                      EasyLoading.showError(
                                        "please_input_all_field".tr,
                                        duration: const Duration(seconds: 2),);
                                    }
                                  }
                                }
                            ),
                          ],
                        )
                    ),
                  ),

                ],
              ),
            )
        );
      }),
    );
  }
  Widget bodyMobile() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: GetBuilder<UserController>(builder: (logic) {
        return FormBuilder(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  _buildTextField(
                    title: 'card_number'.tr,
                    hintext: 'card_number'.tr,
                    controller: logic.cardNameController,
                    icon: Icons.sd_card,
                    focusNode: cardNumberFocusNode,
                    nextFocusNode: nameFocusNode,
                  ),
                  _buildTextField(
                    title: 'name'.tr,
                    hintext: 'name'.tr,
                    controller: logic.nameController,
                    icon: Icons.people_rounded,
                    focusNode: nameFocusNode,
                    nextFocusNode: userNameFocusNode,
                  ),
                  _buildTextField(
                    title: 'username'.tr,
                    hintext: 'username'.tr,
                    controller: logic.userNameController,
                    icon: Icons.people_rounded,
                    focusNode: userNameFocusNode,
                    nextFocusNode: khNameFocusNode,
                  ),
                  _buildTextField(
                    title: 'khmer_name'.tr,
                    hintext: 'khmer_name'.tr,
                    controller: logic.khmerNameController,
                    icon: Icons.people_rounded,
                    focusNode: khNameFocusNode,
                    nextFocusNode: phoneFocusNode,
                  ),
                  _buildTextField(
                    title: 'phone'.tr,
                    hintext: 'phone'.tr,
                    controller: logic.phoneController,
                    icon: Icons.phone,
                    focusNode: phoneFocusNode,
                    nextFocusNode: emailFocusNode,
                  ),
                  _buildTextField(
                    title: 'email'.tr,
                    hintext: 'email'.tr,
                    controller: logic.emailController,
                    icon: Icons.email,
                    focusNode: emailFocusNode,
                    nextFocusNode: passwordFocusNode,
                  ),
                  _buildPasswordField(
                    logic: logic,
                    isVisible: isVisible,
                    focusNode: passwordFocusNode,
                    nextFocusNode: dobFocusNode,
                    onVisibilityChanged: () {
                      setState(() {
                        isVisible = !isVisible;
                      });
                    },
                  ),
                  _buildDateField(
                    title: 'date_of_birth'.tr,
                    context: context,
                    logic: logic,
                    controller: logic.dayofbirthdayController,
                    focusNode: dobFocusNode,
                    nextFocusNode: addressFocusNode,
                  ),
                  _buildTextField(
                    title: 'address'.tr,
                    hintext: 'address'.tr,
                    controller: logic.addressController,
                    icon: CupertinoIcons.map_pin_ellipse,
                    focusNode: addressFocusNode,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('gender'.tr),
                        FormBuilderDropdown(
                          name: 'type'.tr,
                          initialValue: logic.selectGender,
                          borderRadius: BorderRadius.circular(10),
                          decoration: InputDecoration(
                            floatingLabelAlignment: FloatingLabelAlignment.center,
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            fillColor: secondaryColor,
                            labelText: 'please_select_gender'.tr,
                            filled: true,
                            border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10)
                            ),
                          ),
                          items: logic.typeGender.map((option) =>
                              DropdownMenuItem(
                                value: option['value'],
                                child: Text(option['text']!),
                              ))
                              .toList(),
                          onChanged: (value) {
                            setState(() {
                              logic.selectGender = value!;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('province'.tr),
                        _buildLocationSelector(
                          context,
                          'province'.tr,
                          userController.provinces,
                          userController.selectedProvince?.name ??
                              'please_select_province'.tr,
                          userController.selectProvince,
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('district'.tr,),
                      _buildLocationSelector(
                        context,
                        'district'.tr,
                        userController.districts,
                        userController.selectedDistrict?.name ??
                            'please_select_district'.tr,
                        userController.selectDistrict,
                        enabled: userController.selectedProvince !=
                            null,
                      ),
                    ],
                  ),
                  const SizedBox(height: 15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('commune'.tr),
                      _buildLocationSelector(
                        context,
                        'commune'.tr,
                        userController.communes,
                        userController.selectedCommune?.name ??
                            'please_select_commune'.tr,
                        userController.selectCommune,
                        enabled: userController.selectedDistrict !=
                            null,
                      ),
                    ],
                  ),
                  const SizedBox(height: 15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('village'.tr),
                      _buildLocationSelector(
                        context,
                        'village',
                        userController.villages,
                        userController.selectedVillage?.name ??
                            'please_select_village'.tr,
                        userController.selectVillage,
                        enabled: userController.selectedCommune != null,
                      ),
                    ],
                  ),
                  const SizedBox(height: 15,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 310,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: ImagePickerField(
                            imageUrls: "",
                            title: 'select_profile'.tr,
                            hintText: logic.selectedImageUser != null ? logic.selectedImageUser!.path : "no_image_selected".tr,
                            imageFile: logic.selectedImageUser,
                            onImageSelected: () {
                              return logic.pickImage().then((v) {
                                if (v != null) {
                                  logic.selectedImageUser = v;
                                  logic.update();
                                }
                              });
                            },
                          ),
                        ),
                      ),
                      const SizedBox(width: 20,),
                      SizedBox(
                        width: 310,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: ImagePickerField(
                            imageUrls: "",
                            title: 'select_front_identity'.tr,
                            hintText: logic.selectedFrontId != null ? logic.selectedFrontId!.path : "no_image_selected".tr,
                            imageFile: logic.selectedFrontId,
                            onImageSelected: () {
                              return logic.pickImage().then((v) {
                                logic.selectedFrontId = v;
                                logic.update();
                              });
                            },
                          ),
                        ),
                      ),
                      const SizedBox(width: 20,),
                      SizedBox(
                        width: 310,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          child: ImagePickerField(
                            imageUrls: "",
                            title: 'select_back_identity'.tr,
                            hintText: logic.selectedBackID != null ? logic.selectedBackID!.path : "no_image_selected".tr,
                            imageFile: logic.selectedBackID,
                            onImageSelected: () {
                              return logic.pickImage().then((v) {
                                logic.selectedBackID = v;
                                logic.update();
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            CancelButton(
                                btnName: "cancel".tr,
                                onPress: () {
                                  logic.clearTextField();
                                  homeController.updateWidgetHome(
                                      const GetUserList());
                                }),
                            const SizedBox(width: 16,),
                            OKButton(
                                btnName: "create".tr,
                                onPress: () async {
                                  if (_formKey.currentState != null &&
                                      _formKey.currentState!
                                          .saveAndValidate()) {
                                    if (logic.cardNameController.text
                                        .isNotEmpty &&
                                        logic.userNameController.text
                                            .isNotEmpty &&
                                        logic.khmerNameController.text
                                            .isNotEmpty &&
                                        logic.phoneController.text.isNotEmpty &&
                                        logic.emailController.text.isNotEmpty &&
                                        logic.passwordController.text
                                            .isNotEmpty &&
                                        logic.selectGender.isNotEmpty &&
                                        logic.dayofbirthdayController.text
                                            .isNotEmpty &&
                                        logic.addressController.text
                                            .isNotEmpty &&
                                        logic.nameController.text.isNotEmpty &&
                                        logic.selectedProvince != null &&
                                        logic.selectedDistrict != null &&
                                        logic.selectedCommune != null &&
                                        logic.selectedVillage != null &&
                                        logic.selectedImageUser != null
                                    ) {
                                      await logic.createUser();
                                    } else {
                                      EasyLoading.showError(
                                        "please_input_all_field".tr,
                                        duration: const Duration(seconds: 2),);
                                    }
                                  }
                                }
                            ),
                          ],
                        )
                    ),
                  ),

                ],
              ),
            )
        );
      }),
    );
  }

  Widget _buildTextField({
    required String title,
    required String hintext,
    required TextEditingController controller,
    IconData? icon,
    required FocusNode focusNode,
    FocusNode? nextFocusNode,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(color: Colors.white)),
          FormBuilderTextField(
            name: title,
            controller: controller,
            focusNode: focusNode,
            decoration: InputDecoration(
              fillColor: secondaryColor,
              filled: true,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              hintText: hintext,
              prefixIcon: icon != null
                  ? Icon(icon, color: Colors.white)
                  : null,
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
            onSubmitted: (_) {
              if (nextFocusNode != null) {
                FocusScope.of(context).requestFocus(nextFocusNode);
              } else {
                focusNode.unfocus();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildPasswordField({
    required UserController logic,
    required bool isVisible,
    required FocusNode focusNode,
    FocusNode? nextFocusNode,
    required VoidCallback onVisibilityChanged,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('password'.tr, style: const TextStyle(color: Colors.white)),
          FormBuilderTextField(
            name: 'password'.tr,
            obscureText: isVisible,
            controller: logic.passwordController,
            focusNode: focusNode,
            decoration: InputDecoration(
              hintText: 'password'.tr,
              prefixIcon: const Icon(Icons.lock),
              suffixIcon: IconButton(
                onPressed: onVisibilityChanged,
                icon: !isVisible
                    ? const Icon(Icons.visibility)
                    : const Icon(Icons.visibility_off),
              ),
              fillColor: secondaryColor,
              filled: true,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
            onSubmitted: (_) {
              if (nextFocusNode != null) {
                FocusScope.of(context).requestFocus(nextFocusNode);
              } else {
                focusNode.unfocus();
              }
            },
          ),
        ],
      ),
    );
  }

  Widget _buildDateField({
    required String title,
    required BuildContext context,
    required UserController logic,
    required TextEditingController controller,
    required FocusNode focusNode,
    FocusNode? nextFocusNode,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(color: Colors.white)),
          FormBuilderTextField(
            name: 'yy/mm/dd',
            controller: controller,
            focusNode: focusNode,
            decoration: InputDecoration(
              hintText: 'date_of_birth'.tr,
              prefixIcon: const Icon(Icons.date_range),

              fillColor: secondaryColor,
              filled: true,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
            readOnly: true,
            onTap: () {
              logic.selectDate(context, controller);
            },
            onSubmitted: (_) {
              if (nextFocusNode != null) {
                FocusScope.of(context).requestFocus(nextFocusNode);
              } else {
                focusNode.unfocus();
              }
            },
          ),
        ],
      ),
    );
  }


  Widget _buildLocationSelector(BuildContext context, String label,
      List<Address> data, String currentValue, Function(Address) onSelect,
      {bool enabled = true}) {
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () =>
          _showSelectionDialog(
              context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: secondaryColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
    kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding: const EdgeInsets.symmetric(
              horizontal: 4, vertical: 8),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }

}