

import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:printing/printing.dart';
import '../../../helper/images.dart';
import 'generate_card_pdf.dart';

class PreviewCardPdf extends StatelessWidget {
  const PreviewCardPdf({super.key});

  @override
  Widget build(BuildContext context) {

    // Future<Uint8List> getAssetImage(String assetName) async {
    //   final byteData = await rootBundle.load(assetName);
    //   return byteData.buffer.asUint8List();
    // }
    //
    // Future<Map<String, Uint8List>> loadImages() async {
    //   final logoBoules = await getAssetImage(Images.logoBoules);
    //   // final iconLocation = await getAssetImage(Images.locationIcon);
    //   return {
    //     'logoBoules': logoBoules,
    //     // 'iconLocation': iconLocation,
    //   };
    // }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: bgColor,
        title: const Text('Card Preview'),
        centerTitle: true,
      ),
      body: FutureBuilder<Uint8List>(
        future: GenerateCardPdf.generateCardPDF(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return const Center(child: Text('Error generating PDF'));
          } else if (snapshot.hasData) {
            return PdfPreview(
              // pdfPreviewPageDecoration: const BoxDecoration(
              //     color: bgColor,
              //     backgroundBlendMode: BlendMode.lighten,
              //     boxShadow: [
              //       BoxShadow(
              //           offset: Offset.zero,
              //           color: bgColor
              //       )
              //     ]
              // ),
              build: (format) => snapshot.data!,
              allowPrinting: true,
              allowSharing: true,
              canChangePageFormat: true,
              canChangeOrientation: true,
              enableScrollToPage: true,
              dynamicLayout: true,

            );
          } else {
            return const Center(child: Text('No data available'));
          }
        },
      )
    );
  }
}
