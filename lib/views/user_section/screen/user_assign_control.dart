  import 'package:esport_system/data/controllers/team_controller.dart';
  import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/address_assign.dart';
  import 'package:esport_system/data/model/address_model.dart';
  import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/data/repo/user_repo.dart';
  import 'package:esport_system/helper/add_new_button.dart';
  import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/user_section/screen/get_list_user.dart';
  import 'package:flutter/foundation.dart';
  import 'package:flutter/material.dart';
  import 'package:flutter_easyloading/flutter_easyloading.dart';
  import 'package:flutter_form_builder/flutter_form_builder.dart';
  import 'package:form_builder_validators/form_builder_validators.dart';
  import 'package:get/get.dart';

import '../../../data/controllers/home_controller.dart';

  class UserAssignControl extends StatefulWidget {
    final ListUserModel? userModel;

    const UserAssignControl({super.key, required this.userModel});

    @override
    State<UserAssignControl> createState() => _UserAssignControlState();
  }

  class _UserAssignControlState extends State<UserAssignControl> {
    final _formKey = GlobalKey<FormBuilderState>();
    final teamController = Get.find<TeamController>();
    HomeController homeController = Get.find<HomeController>();

    String? provinceCode;
    String? districtCode;
    String? communeCode;
    String? villageCode;
    List<AddressAssign> addressControl = [] ;

    @override
    void initState() {
      teamController.fetchProvinces();
      if(widget.userModel!.controlOf.toString() != "null" ){
        UserRepo().addressAfterAssign(controlOf: widget.userModel!.controlOf.toString()).then((addresses){
          for(var i=0 ;i<addresses.length ; i++){
            var a=addresses[i];
            teamController.selectLevel(level: a.level.toString());
            if (a.provinceCode != null) {
              teamController.provinceController.text = a.provinceCode!.name.toString();
              teamController.selectProvince(Address(
                code: a.provinceCode!.code,
                name: a.provinceCode!.name,
              ));
            }
            if (a.districtCode != null) {
              teamController.districtController.text = a.districtCode!.name.toString();
              teamController.selectDistrict(Address(
                code: a.districtCode!.code,
                name: a.districtCode!.name,
              ));
            }
            if (a.communeCode != null) {
              teamController.communeController.text = a.communeCode!.name.toString();
              teamController.selectCommune(Address(
                code: a.communeCode!.code,
                name: a.communeCode!.name,
              ));
            }
            if (a.villageCode != null) {
              teamController.villageController.text = a.villageCode!.name.toString();
              teamController.selectVillage(Address(
                code: a.villageCode!.code,
                name: a.villageCode!.name,
              ));
            }

            // print("-------- Level control : ${a.level}");
            // print("-------- provinceCode: ${a.provinceCode!.name}");
            // print("-------- districtCode: ${a.districtCode!.name}");
            // print("-------- communeCode: ${a.communeCode!.name}");
            // print("-------- villageCode: ${a.villageCode!.name}");
          }
        });
      }else{
        teamController.clearControlAddress();
      }
      if (provinceCode != null) {
        teamController.fetchDistricts(provinceCode!);
      }
      if (districtCode != null) {
        teamController.fetchCommunes(districtCode!);
      }
      if (communeCode != null) {
        teamController.fetchVillages(communeCode!);
      }
      super.initState();
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        backgroundColor: bgColor,
        body: GetBuilder<UserController>(builder: (logic) {
          return Container(
            width: double.infinity,
            padding: const EdgeInsets.all(24.0),
            child: GetBuilder<TeamController>(builder: (logic1) {
              return Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text('assign_user_to_control'.tr,
                        style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(height: 20.0),
                      FormBuilder(
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                 Text('select_level'.tr, style: const TextStyle(fontSize: 16),
                                ),
                                const SizedBox(height: 10.0),

                                DropdownButtonFormField<String>(
                                  borderRadius: BorderRadius.circular(5),
                                  value: logic1.selectedLevel,
                                  decoration: InputDecoration(
                                    floatingLabelAlignment: FloatingLabelAlignment.center,
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    fillColor: secondaryColor,
                                    labelText: 'select_level'.tr,
                                    filled: true,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide.none,
                                        borderRadius: BorderRadius.circular(5)),
                                  ),
                                  items: logic1.options.map((option) => DropdownMenuItem(
                                            value: option['value'],
                                            child: Text(option['text']!),
                                          ))
                                      .toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      logic1.selectedLevel = value;
                                    });
                                    print("Level: $logic1.selectedLevel ");
                                  },
                                ),
                              ],
                            ),
                            const SizedBox(height: 16),
                            if (logic1.selectedLevel != null)
                              Row(
                                children: [
                                  if (logic1.selectedLevel == 'province' ||
                                      logic1.selectedLevel == 'district' ||
                                      logic1.selectedLevel == 'commune' ||
                                      logic1.selectedLevel == 'village')
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('province'.tr, style: const TextStyle(fontSize: 16)),
                                          const SizedBox(height: 10.0),
                                          _buildLocationSelector(
                                            icon: Icons.location_on,
                                            context,
                                            'province'.tr,
                                            logic1.provinces,
                                            logic1.selectedProvince?.name ??'please_select_province'.tr,
                                            (address) {
                                              logic1.selectProvince(address);
                                              setState(() {
                                                logic1.provinceController.text = address.code.toString();
                                              });
                                              print("Province Code : $provinceCode}");
                                            },
                                          ),
                                        ],
                                      ),
                                    ),

                                  if (logic1.selectedLevel == 'district' ||
                                      logic1.selectedLevel == 'commune' ||
                                      logic1.selectedLevel == 'village')

                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 20),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('district'.tr, style: const TextStyle(fontSize: 16)),
                                            const SizedBox(height: 10.0),
                                            _buildLocationSelector(
                                              icon: Icons.location_on,
                                              context,
                                              'district'.tr,
                                              logic1.districts,
                                              logic1.selectedDistrict?.name ?? 'please_select_district'.tr,
                                              (address) {
                                                logic1.selectDistrict(address);
                                                setState(() {
                                                  logic1.districtController.text = address.code.toString();
                                                });
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                ],
                              ),
                            const SizedBox(height: 16),
                            Row(
                              children: [
                                if (logic1.selectedLevel == 'commune' ||
                                    logic1.selectedLevel == 'village')
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text('commune'.tr,
                                            style: const TextStyle(fontSize: 16)),
                                        const SizedBox(height: 10.0),
                                        _buildLocationSelector(
                                          icon: Icons.location_on,
                                          context,
                                          'commune'.tr,
                                          logic1.communes,
                                          logic1.selectedCommune?.name ?? 'please_select_commune'.tr,
                                          (address) {
                                            logic1.selectCommune(address);
                                            setState(() {
                                              logic1.communeController.text = address.code.toString();
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                if (logic1.selectedLevel == 'village')
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 20),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('village'.tr,
                                              style: const TextStyle(fontSize: 16)),
                                          const SizedBox(height: 10.0),
                                          _buildLocationSelector(
                                            icon: Icons.location_on,
                                            context,
                                            'village'.tr,
                                            logic1.villages,
                                            logic1.selectedVillage?.name ??
                                                'please_select_village'.tr,
                                            (address) {
                                              logic1.selectVillage(address);
                                              setState(() {
                                                logic1.villageController.text = address.code.toString();
                                              });
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                              ],
                            ),

                            const SizedBox(
                              height: 24,
                            ),

                            Row(mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                CancelButton(
                                    btnName: 'cancel'.tr,
                                    onPress: (){
                                      homeController.updateWidgetHome(const GetUserList());
                                    }),
                                const SizedBox(width: 16,),
                                OKButton(
                                  btnName: widget.userModel?.controlOf != "null" ? "update".tr : "save" .tr,
                                  onPress: () async {
                                    print("-------selectedLevel ${logic1.selectedLevel}");
                                    print("-------id ${widget.userModel!.id.toString()}");
                                    print("-------provinceCode ${provinceCode}");
                                    print("-------districtCode ${districtCode}");
                                    print("-------communeCode ${communeCode}");
                                    print("-------villageCode ${villageCode}");

                                      if(widget.userModel!.id != null &&
                                          logic1.validateLevel()
                                      ){
                                        await logic.assignControlUser(
                                          level: logic1.selectedLevel.toString(),
                                          userID: widget.userModel!.id.toString(),
                                          province: logic1.provinceController.text,
                                          district: logic1.districtController.text,
                                          commune: logic1.communeController.text,
                                          village: logic1.villageController.text,
                                        );
                                      }else{
                                        EasyLoading.showError('please_input_all_field'.tr,
                                          duration: const Duration(seconds: 2),
                                        );
                                      }

                                  },
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              );
            }),
          );
        }),
      );
    }

    Widget _buildLocationSelector(
      BuildContext context,
      String label,
      List<Address> data,
      String currentValue,
      Function(Address) onSelect, {
      bool enabled = true,
      IconData? icon,
    }) {
      return SizedBox(
        width: double.infinity,
        child: FormBuilderTextField(
          name: label,
          readOnly: true,
          onTap: enabled
              ? () => _showSelectionDialog(
                  context: context, title: label, data: data, onSelect: onSelect)
              : null,
          controller: TextEditingController(text: currentValue),
          decoration: InputDecoration(
            prefixIcon: Icon(icon),
            border: const OutlineInputBorder(
              borderSide: BorderSide.none,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            filled: true,
            fillColor: secondaryColor,
            hintText: label,
            suffixIcon: const Icon(Icons.arrow_drop_down),
          ),
          validator: enabled ? FormBuilderValidators.required() : null,
        ),
      );
    }

    void _showSelectionDialog({
      required BuildContext context,
      required String title,
      required List<Address> data,
      required Function(Address) onSelect,
    }) {
      final double dialogWidth =
          kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: bgColor,
            title: Text(title),
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            content: SizedBox(
              width: dialogWidth,
              child: ListView(
                children: data.map((item) {
                  return ListTile(
                    title: Text(item.name!),
                    subtitle: Text(item.description!),
                    onTap: () {
                      onSelect(item);
                      Navigator.pop(context);
                    },
                  );
                }).toList(),
              ),
            ),
          );
        },
      );
    }
  }
