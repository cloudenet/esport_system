import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/user_section/screen/get_list_user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';
import '../../../data/controllers/team_controller.dart';
import '../../../data/model/address_model.dart';
import '../../../data/model/list_user_model.dart';
import '../../../helper/add_new_button.dart';
import '../../../helper/app_contrain.dart';
import '../../../helper/constants.dart';
import '../../../helper/image_picker_field.dart';
import '../../../helper/images.dart';
import '../../auth_section/app_colors.dart';
import '../../auth_section/app_styles.dart';



class UpdateUserScreen extends StatefulWidget {
  final ListUserModel? userModel;

  const UpdateUserScreen({super.key,required this.userModel});

  @override
  State<UpdateUserScreen> createState() => _UpdateUserScreenState();
}

class _UpdateUserScreenState extends State<UpdateUserScreen> {
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();
  var teamController = Get.find<TeamController>();
  final _formKey = GlobalKey<FormBuilderState>();
  final FocusNode cardNumberFocusNode = FocusNode();
  final FocusNode nameFocusNode = FocusNode();
  final FocusNode khNameFocusNode = FocusNode();
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode dobFocusNode = FocusNode();
  final FocusNode addressFocusNode = FocusNode();
  final FocusNode nationalFocusNode = FocusNode();
  final FocusNode identityFocusNode = FocusNode();
  List<String?> previousImg = [] ;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userController.clearTextField();
    userController.fetchProvinces();
    if(widget.userModel!=null){
      userController.setOldValue(widget.userModel!);
      String cleanedData = widget.userModel!.identityPhoto.toString().replaceAll(RegExp(r'[\[\]]'), '');
      previousImg = cleanedData.split(", ");
      while (previousImg.length < 2) {
        previousImg.add("");
      }
      for (var a in previousImg) {
        print("ID-Photo:  $a");
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Responsive(
          mobile: bodyMobileAndTablet(),
          tablet: bodyMobileAndTablet(),
          desktop: body(),
        ),
      ),
    );
  }

  Widget body() {
    return GetBuilder<UserController>(builder: (logic) {
      return FormBuilder(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20,),
                Text('update_user_profile'.tr, style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),),
                const SizedBox(height: 20,),
                _buildProfileHeader(),
                const SizedBox(height: 30,),
                Text('update_personal_information'.tr, style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),),
                const SizedBox(height: 15,),
                Container(
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: secondaryColor,
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: _buildTextField(
                              title: 'card_number'.tr,
                              hintext: 'card_number'.tr,
                              controller: logic.cardNameController,
                              icon: Icons.sd_card,
                              focusNode: cardNumberFocusNode,
                              nextFocusNode: nameFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'name'.tr,
                              hintext: 'name'.tr,
                              controller: logic.nameController,
                              icon: Icons.people_rounded,
                              focusNode: nameFocusNode,
                              nextFocusNode: khNameFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'khmer_name'.tr,
                              hintext: 'khmer_name'.tr,
                              controller: logic.khmerNameController,
                              icon: Icons.people_rounded,
                              focusNode: khNameFocusNode,
                              nextFocusNode: emailFocusNode,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: _buildTextField(
                              title: 'email'.tr,
                              hintext: 'email'.tr,
                              controller: logic.emailController,
                              icon: Icons.email,
                              focusNode: emailFocusNode,
                              nextFocusNode: identityFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'identity'.tr,
                              hintext: 'identity'.tr,
                              controller: logic.identityNoController,
                              icon: Icons.credit_card_sharp,
                              focusNode: identityFocusNode,
                              nextFocusNode: nationalFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'nationality'.tr,
                              hintext: 'nationality'.tr,
                              controller: logic.nationalityController,
                              icon: Icons.person,
                              focusNode: nationalFocusNode,
                              nextFocusNode: null,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('gender'.tr),
                                  FormBuilderDropdown(
                                    name: 'type'.tr,
                                    borderRadius: BorderRadius.circular(10),
                                    decoration: InputDecoration(
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      floatingLabelBehavior: FloatingLabelBehavior.never,
                                      fillColor: bgColor,
                                      labelText: 'please_select_gender'.tr,
                                      filled: true,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          borderRadius: BorderRadius.circular(10)
                                      ),
                                    ),
                                    initialValue: logic.selectGender,
                                    items: logic.typeGender.map((option) =>
                                        DropdownMenuItem(
                                          value: option['value'],
                                          child: Text(option['text']!),
                                        )).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        logic.selectGender = value!;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: _buildDateField(
                              title: 'date_of_birth'.tr,
                              context: context,
                              logic: logic,
                              controller: logic.dayofbirthdayController,
                              focusNode: dobFocusNode,
                              nextFocusNode: addressFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'address'.tr,
                              hintext: 'address'.tr,
                              controller: logic.addressController,
                              icon: CupertinoIcons.map_pin_ellipse,
                              focusNode: addressFocusNode,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('province'.tr),
                                  _buildLocationSelector(
                                    context,
                                    'province'.tr,
                                    userController.provinces,
                                    userController.selectedProvince?.name ??
                                        'please_select_province'.tr,
                                    userController.selectProvince,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('district'.tr,),
                                _buildLocationSelector(
                                  context,
                                  'district'.tr,
                                  userController.districts,
                                  userController.selectedDistrict?.name ??
                                      'please_select_district'.tr,
                                  userController.selectDistrict,
                                  enabled: userController.selectedProvince !=
                                      null,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('commune'.tr),
                                _buildLocationSelector(
                                  context,
                                  'commune'.tr,
                                  userController.communes,
                                  userController.selectedCommune?.name ??
                                      'please_select_commune'.tr,
                                  userController.selectCommune,
                                  enabled: userController.selectedDistrict !=
                                      null,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('village'.tr),
                                _buildLocationSelector(
                                  context,
                                  'village',
                                  userController.villages,
                                  userController.selectedVillage?.name ??
                                      'please_select_village'.tr,
                                  userController.selectVillage,
                                  enabled: userController.selectedCommune != null,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ImagePickerWidget(
                            title: "select_front_identity".tr,
                            previousImg: previousImg[0]??"",
                            height: 150,
                            width: 310,
                            onImagePicked: (image) {
                              // Handle picked image for Profile Section
                              if (image != null) {
                                if (logic.selectedFrontId != null) {
                                  logic.listIdentity.remove(logic.selectedFrontId); // Remove old file if exists
                                }
                                logic.selectedFrontId = image;
                                logic.listIdentity.add(image);
                                print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                              } else {
                                if (logic.selectedFrontId != null) {
                                  logic.listIdentity.remove(logic.selectedFrontId); // Remove old file if exists
                                }
                                print("Profile Picture Cleared");
                              }
                            },
                          ),
                          const SizedBox(width: 20,),
                          ImagePickerWidget(
                            title: "select_back_identity".tr,
                            previousImg: previousImg[1]??"",
                            height: 150,
                            width: 310,
                            onImagePicked: (image) {
                              // Handle picked image for Profile Section
                              if (image != null) {
                                if (logic.selectedBackID != null) {
                                  logic.listIdentity.remove(logic.selectedBackID); // Remove old file if exists
                                }
                                logic.selectedBackID = image;
                                logic.listIdentity.add(image);
                                print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                              } else {
                                if (logic.selectedBackID != null) {
                                  logic.listIdentity.remove(logic.selectedBackID); // Remove old file if exists
                                }
                                print("Profile Picture Cleared");
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),

                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          CancelButton(
                              btnName: "cancel".tr,
                              onPress: () {
                                homeController.updateWidgetHome(const GetUserList());
                              }),
                          const SizedBox(width: 16,),
                          OKButton(
                              btnName: "create".tr,
                              onPress: () async {
                                if (_formKey.currentState != null &&
                                    _formKey.currentState!.saveAndValidate()) {
                                  logic.updateUsers(userId: widget.userModel!.id.toString());
                                }
                              }
                          ),
                        ],
                      )
                  ),
                ),

              ],
            ),
          )
      );
    });
  }

  Widget bodyMobileAndTablet() {
    return GetBuilder<UserController>(builder: (logic) {
      return FormBuilder(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20,),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('update_user_profile'.tr, style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),),
                      const SizedBox(height: 20,),
                      _buildProfileHeader(),
                    ],
                  ),
                ),
                const SizedBox(height: 30,),
                Text('update_personal_information'.tr, style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),),
                const SizedBox(height: 15,),
                Container(
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                      color: secondaryColor,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: _buildTextField(
                              title: 'card_number'.tr,
                              hintext: 'card_number'.tr,
                              controller: logic.cardNameController,
                              icon: Icons.sd_card,
                              focusNode: cardNumberFocusNode,
                              nextFocusNode: nameFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'name'.tr,
                              hintext: 'name'.tr,
                              controller: logic.nameController,
                              icon: Icons.people_rounded,
                              focusNode: nameFocusNode,
                              nextFocusNode: khNameFocusNode,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: _buildTextField(
                              title: 'khmer_name'.tr,
                              hintext: 'khmer_name'.tr,
                              controller: logic.khmerNameController,
                              icon: Icons.people_rounded,
                              focusNode: khNameFocusNode,
                              nextFocusNode: emailFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'email'.tr,
                              hintext: 'email'.tr,
                              controller: logic.emailController,
                              icon: Icons.email,
                              focusNode: emailFocusNode,
                              nextFocusNode: identityFocusNode,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: _buildTextField(
                              title: 'identity'.tr,
                              hintext: 'identity'.tr,
                              controller: logic.identityNoController,
                              icon: Icons.credit_card_sharp,
                              focusNode: identityFocusNode,
                              nextFocusNode: nationalFocusNode,
                            ),
                          ),
                          Expanded(
                            child: _buildTextField(
                              title: 'nationality'.tr,
                              hintext: 'nationality'.tr,
                              controller: logic.nationalityController,
                              icon: Icons.person,
                              focusNode: nationalFocusNode,
                              nextFocusNode: null,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('gender'.tr),
                                  FormBuilderDropdown(
                                    name: 'type'.tr,
                                    borderRadius: BorderRadius.circular(10),
                                    decoration: InputDecoration(
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      floatingLabelBehavior: FloatingLabelBehavior.never,
                                      fillColor: bgColor,
                                      labelText: 'please_select_gender'.tr,
                                      filled: true,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide.none,
                                          borderRadius: BorderRadius.circular(10)
                                      ),
                                    ),
                                    initialValue: logic.selectGender,
                                    items: logic.typeGender.map((option) =>
                                        DropdownMenuItem(
                                          value: option['value'],
                                          child: Text(option['text']!),
                                        )).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        logic.selectGender = value!;
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: _buildDateField(
                              title: 'date_of_birth'.tr,
                              context: context,
                              logic: logic,
                              controller: logic.dayofbirthdayController,
                              focusNode: dobFocusNode,
                              nextFocusNode: addressFocusNode,
                            ),
                          ),
                        ],
                      ),
                      _buildTextField(
                        title: 'address'.tr,
                        hintext: 'address'.tr,
                        controller: logic.addressController,
                        icon: CupertinoIcons.map_pin_ellipse,
                        focusNode: addressFocusNode,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('province'.tr),
                                  _buildLocationSelector(
                                    context,
                                    'province'.tr,
                                    userController.provinces,
                                    userController.selectedProvince?.name ??
                                        'please_select_province'.tr,
                                    userController.selectProvince,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('district'.tr,),
                                _buildLocationSelector(
                                  context,
                                  'district'.tr,
                                  userController.districts,
                                  userController.selectedDistrict?.name ??
                                      'please_select_district'.tr,
                                  userController.selectDistrict,
                                  enabled: userController.selectedProvince !=
                                      null,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('commune'.tr),
                                _buildLocationSelector(
                                  context,
                                  'commune'.tr,
                                  userController.communes,
                                  userController.selectedCommune?.name ??
                                      'please_select_commune'.tr,
                                  userController.selectCommune,
                                  enabled: userController.selectedDistrict !=
                                      null,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('village'.tr),
                                _buildLocationSelector(
                                  context,
                                  'village',
                                  userController.villages,
                                  userController.selectedVillage?.name ??
                                      'please_select_village'.tr,
                                  userController.selectVillage,
                                  enabled: userController.selectedCommune != null,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10,),
                      ImagePickerWidget(
                        title: "select_front_identity".tr,
                        previousImg: previousImg[0]??"",
                        height: 150,
                        width: 310,
                        onImagePicked: (image) {
                          // Handle picked image for Profile Section
                          if (image != null) {
                            if (logic.selectedFrontId != null) {
                              logic.listIdentity.remove(logic.selectedFrontId); // Remove old file if exists
                            }
                            logic.selectedFrontId = image;
                            logic.listIdentity.add(image);
                            print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                          } else {
                            if (logic.selectedFrontId != null) {
                              logic.listIdentity.remove(logic.selectedFrontId); // Remove old file if exists
                            }
                            print("Profile Picture Cleared");
                          }
                        },
                      ),
                      const SizedBox(height: 10,),
                      ImagePickerWidget(
                        title: "select_back_identity".tr,
                        previousImg: previousImg[1]??"",
                        height: 150,
                        width: 310,
                        onImagePicked: (image) {
                          // Handle picked image for Profile Section
                          if (image != null) {
                            if (logic.selectedBackID != null) {
                              logic.listIdentity.remove(logic.selectedBackID); // Remove old file if exists
                            }
                            logic.selectedBackID = image;
                            logic.listIdentity.add(image);
                            print("Profile Picture Selected: name ${logic.selectedImageUser!.name}");
                          } else {
                            if (logic.selectedBackID != null) {
                              logic.listIdentity.remove(logic.selectedBackID); // Remove old file if exists
                            }
                            print("Profile Picture Cleared");
                          }
                        },
                      ),
                    ],
                  ),
                ),

                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          CancelButton(
                              btnName: "cancel".tr,
                              onPress: () {
                                homeController.updateWidgetHome(const GetUserList());
                              }),
                          const SizedBox(width: 16,),
                          OKButton(
                              btnName: "create".tr,
                              onPress: () async {
                                if (_formKey.currentState != null &&
                                    _formKey.currentState!.saveAndValidate()) {
                                  logic.updateUsers(userId: widget.userModel!.id.toString());
                                }
                              }
                          ),
                        ],
                      )
                  ),
                ),

              ],
            ),
          )
      );
    });
  }

  Widget _buildProfileHeader() {
    return GetBuilder<UserController>(
      builder: (controller) {
        return Column(
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                ClipOval(
                  child: controller.imageDataX != null ?
                  Image.memory(
                    controller.imageDataX!,
                    width: 150,
                    height: 150,
                    fit: BoxFit.cover,
                    errorBuilder: (context, error, stackTrace) {
                      return ClipOval(
                        child: Image.asset(
                          Images.noImage,
                          height: 150,
                          width: 150,
                          fit: BoxFit.cover,
                        ),
                      );
                    },
                  ):
                  Image.network(
                    "${AppConstants.baseUrl}${widget.userModel!.photo}",
                    width: 150,
                    height: 150,
                    fit: BoxFit.cover,
                    errorBuilder: (context, error, stackTrace) {
                      return ClipOval(
                        child: Image.asset(
                          Images.noImage,
                          height: 150,
                          width: 150,
                          fit: BoxFit.cover,
                        ),
                      );
                    },
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) {
                        return child;
                      } else {
                        return Center(
                          child: CircularProgressIndicator(
                            value: loadingProgress.expectedTotalBytes != null
                                ? loadingProgress.cumulativeBytesLoaded /
                                (loadingProgress.expectedTotalBytes ?? 1)
                                : null,
                          ),
                        );
                      }
                    },
                  ),
                ),
                Positioned(
                    right: 0,
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.zero,
                      decoration: const BoxDecoration(
                          color: Colors.blue,
                          shape: BoxShape.circle
                      ),
                      child: IconButton(
                          onPressed: (){
                            controller.pickImageX();
                          },
                          icon: const Icon(Icons.camera_alt_outlined,size: 20,)
                      ),
                    )
                )
              ],
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: 130,
              child: TextButton(
                onPressed: () async {
                  if(controller.imageDataX !=null){
                    controller.updateImageProfile(
                      userId: widget.userModel!.id.toString(),
                    );
                  }else{
                    EasyLoading.showError("please_select_new_image".tr);
                  }
                },
                style: TextButton.styleFrom(
                  backgroundColor: AppColors.mainBlueColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
                child: Text(
                  'save_change'.tr,
                  style: ralewayStyle.copyWith(
                    fontWeight: FontWeight.w700,
                    color: AppColors.whiteColor,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),

          ],
        );
      },
    );
  }

  Widget _buildTextField({
    required String title,
    required String hintext,
    required TextEditingController controller,
    IconData? icon,
    required FocusNode focusNode,
    FocusNode? nextFocusNode,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(color: Colors.white)),
          FormBuilderTextField(
            name: title,
            controller: controller,
            focusNode: focusNode,
            decoration: InputDecoration(
              fillColor: bgColor,
              filled: true,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              hintText: hintext,
              prefixIcon: icon != null
                  ? Icon(icon, color: Colors.white)
                  : null,
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
            onSubmitted: (_) {
              if (nextFocusNode != null) {
                FocusScope.of(context).requestFocus(nextFocusNode);
              } else {
                focusNode.unfocus();
              }
            },
          ),
        ],
      ),
    );
  }
  Widget _buildDateField({
    required String title,
    required BuildContext context,
    required UserController logic,
    required TextEditingController controller,
    required FocusNode focusNode,
    FocusNode? nextFocusNode,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(title, style: const TextStyle(color: Colors.white)),
          FormBuilderTextField(
            name: 'yy/mm/dd',
            controller: controller,
            focusNode: focusNode,
            decoration: InputDecoration(
              hintText: 'Date of Birth'.tr,
              prefixIcon: const Icon(Icons.date_range),

              fillColor: bgColor,
              filled: true,
              border: const OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
            validator: FormBuilderValidators.compose([
              FormBuilderValidators.required(),
            ]),
            readOnly: true,
            onTap: () {
              logic.selectDate(context, controller);
            },
            onSubmitted: (_) {
              if (nextFocusNode != null) {
                FocusScope.of(context).requestFocus(nextFocusNode);
              } else {
                focusNode.unfocus();
              }
            },
          ),
        ],
      ),
    );
  }
  Widget _buildLocationSelector(BuildContext context, String label,
      List<Address> data, String currentValue, Function(Address) onSelect,
      {bool enabled = true}) {
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () =>
          _showSelectionDialog(
              context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: bgColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }
  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
    kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding: const EdgeInsets.symmetric(
              horizontal: 4, vertical: 8),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}

