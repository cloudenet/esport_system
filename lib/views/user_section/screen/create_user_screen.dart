import 'package:esport_system/data/controllers/league_controller.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/user_section/screen/get_list_user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:responsive_grid/responsive_grid.dart';
import '../../../data/controllers/user_controller.dart';
import '../../../data/model/address_model.dart';
import '../../../helper/add_new_button.dart';
import '../../../helper/constants.dart';


// class CreateUpdateUserScreen extends StatefulWidget {
//   final ListUserModel? userModel;
//   final bool updateScreen ;
//   const CreateUpdateUserScreen({super.key, this.userModel , required this.updateScreen});
//
//   @override
//   State<CreateUpdateUserScreen> createState() => _CreateUpdateUserScreenState();
// }
//
// class _CreateUpdateUserScreenState extends State<CreateUpdateUserScreen> {
//   final _formKey = GlobalKey<FormBuilderState>();
//   bool isVisible = true;
//   UserController userController = Get.find<UserController>();
//   LeagueController leagueController = Get.find<LeagueController>();
//
//   // Define FocusNodes
//   final FocusNode cardNumberFocusNode = FocusNode();
//   final FocusNode userNameFocusNode = FocusNode();
//   final FocusNode phoneFocusNode = FocusNode();
//   final FocusNode nameFocusNode = FocusNode();
//   final FocusNode khNameFocusNode = FocusNode();
//   final FocusNode emailFocusNode = FocusNode();
//   final FocusNode passwordFocusNode = FocusNode();
//   final FocusNode dobFocusNode = FocusNode();
//   final FocusNode addressFocusNode = FocusNode();
//   final FocusNode nationalFocusNode = FocusNode();
//   final FocusNode identityFocusNode = FocusNode();
//
//   @override
//   void initState() {
//     userController.clearTextField();
//     userController.fetchProvinces();
//     if(widget.userModel!=null){
//       userController.setOldValue(widget.userModel!);
//     }
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     // Dispose FocusNodes
//     cardNumberFocusNode.dispose();
//     userNameFocusNode.dispose();
//     nameFocusNode.dispose();
//     phoneFocusNode.dispose();
//     emailFocusNode.dispose();
//     passwordFocusNode.dispose();
//     dobFocusNode.dispose();
//     addressFocusNode.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: bgColor,
//       body: GetBuilder<UserController>(builder: (logic) {
//         return Container(
//           width: double.infinity,
//           padding: const EdgeInsets.all(24.0),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Responsive(
//                   mobile: Row(
//                     children: [
//                       const Padding(
//                         padding: EdgeInsets.all(8.0),
//                         child: Icon(Icons.people, size: 40),
//                       ),
//                       Text( widget.userModel != null ? "update_user_information".tr : 'create_user_information'.tr,
//                           style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
//                     ],
//                   ),
//                   desktop: Row(
//                     children: [
//                       const Padding(
//                         padding: EdgeInsets.all(8.0),
//                         child: Icon(Icons.people, size: 40),
//                       ),
//                       Text( widget.userModel != null ? "update_user_information".tr : 'create_user_information'.tr,
//                           style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
//                     ],
//                   ),
//                   tablet: Row(
//                   children: [
//                     const Padding(
//                       padding: EdgeInsets.all(8.0),
//                       child: Icon(Icons.people, size: 40),
//                     ),
//                     Text( widget.userModel != null ? "update_user_information".tr : 'create_user_information'.tr,
//                         style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
//                   ],
//                 ),
//               ),
//               widget.updateScreen
//                   ?Expanded(
//                   child: Container(
//                       decoration: const BoxDecoration(
//                         borderRadius: BorderRadius.only(
//                           topLeft: Radius.circular(20),
//                           topRight: Radius.circular(20),
//                         ),
//                       ),
//                       child: SingleChildScrollView(
//                         child: Padding(
//                           padding: const EdgeInsets.all(16),
//                           child: FormBuilder(
//                             key: _formKey,
//                             child: ResponsiveGridRow(
//                               children: [
//                                 _buildTextField(
//                                   title: 'card_number'.tr,
//                                   hintext: 'card_number'.tr,
//                                   controller: logic.cardNameController,
//                                   icon: Icons.sd_card,
//                                   focusNode: cardNumberFocusNode,
//                                   nextFocusNode: nameFocusNode,
//                                 ),
//                                 _buildTextField(
//                                   title: 'name'.tr,
//                                   hintext: 'name'.tr,
//                                   controller: logic.nameController,
//                                   icon: Icons.people_rounded,
//                                   focusNode: nameFocusNode,
//                                   nextFocusNode: khNameFocusNode,
//                                 ),
//                                 _buildTextField(
//                                   title: 'khmer_name'.tr,
//                                   hintext: 'khmer_name'.tr,
//                                   controller: logic.khmerNameController,
//                                   icon: Icons.people_rounded,
//                                   focusNode: khNameFocusNode,
//                                   nextFocusNode: emailFocusNode,
//                                 ),
//                                 _buildTextField(
//                                   title: 'email'.tr,
//                                   hintext: 'email'.tr,
//                                   controller: logic.emailController,
//                                   icon: Icons.email,
//                                   focusNode: emailFocusNode,
//                                   nextFocusNode: dobFocusNode,
//                                 ),
//                                 _buildDateField(
//                                   title: 'date_of_birth'.tr,
//                                   context: context,
//                                   logic: logic,
//                                   controller: logic.dayofbirthdayController,
//                                   focusNode: dobFocusNode,
//                                   nextFocusNode: addressFocusNode,
//                                 ),
//                                 _buildTextField(
//                                   title: 'address'.tr,
//                                   hintext: 'address'.tr,
//                                   controller: logic.addressController,
//                                   icon: CupertinoIcons.map_pin_ellipse,
//                                   focusNode: addressFocusNode,
//                                   nextFocusNode: nationalFocusNode
//                                 ),
//                                 _buildTextField(
//                                   title: 'nationality'.tr,
//                                   hintext: 'nationality'.tr,
//                                   controller: logic.nationalityController,
//                                   icon: CupertinoIcons.creditcard_fill,
//                                   focusNode: nationalFocusNode,
//                                   nextFocusNode: identityFocusNode
//                                 ),
//                                 _buildTextField(
//                                   lgIdNo: 4,
//                                   title: 'identity_no'.tr,
//                                   hintext: 'identity_no'.tr,
//                                   controller: logic.identityNoController,
//                                   icon: CupertinoIcons.number_square_fill,
//                                   focusNode: identityFocusNode,
//                                 ),
//                                 _buildImagePickerField(
//                                   lgSelectImg: 4,
//                                   title: 'selected_identity_image'.tr,
//                                   logic: logic,
//                                   onTap: logic.pickID,
//                                   hinText: logic.selectedImageId != null
//                                       ? logic.selectedImageId!.path
//                                       : 'no_image_selected'.tr, text: '',
//                                 ),
//                                 ResponsiveGridCol(
//                                   md: 6,
//                                   lg: 6,
//                                   child: Padding(
//                                     padding: const EdgeInsets.symmetric(horizontal: 4,vertical: 8),
//                                     child: Column(
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: [
//                                         Text('gender'.tr),
//                                         FormBuilderDropdown(
//                                           borderRadius: const BorderRadius.all(
//                                             Radius.circular(10),
//                                           ),
//                                           name: 'gender'.tr,
//                                           initialValue: logic.selectGender, //// Set initial value here
//                                           decoration: InputDecoration(
//                                             hintText: 'gender'.tr,
//                                             fillColor: secondaryColor,
//                                             filled: true,
//                                             border: const OutlineInputBorder(
//                                               borderSide: BorderSide.none,
//                                               borderRadius: BorderRadius.all(
//                                                 Radius.circular(10),
//                                               ),
//                                             ),
//                                           ),
//                                           validator: FormBuilderValidators.compose([
//                                             FormBuilderValidators.required(),
//                                           ]),
//                                           items: [
//                                             DropdownMenuItem(
//                                               value: 'male',
//                                               child: Text('male'.tr),
//                                             ),
//                                             DropdownMenuItem(
//                                               value: 'female',
//                                               child: Text('female'.tr),
//                                             ),
//
//                                           ],
//                                           onChanged: (selectedValue) {
//                                             if (selectedValue != null) {
//                                               logic.selectGender = selectedValue;
//                                               logic.update(); // Ensure UI update if needed
//                                             }
//                                           },
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                                 ResponsiveGridCol(
//                                   md: 12,
//                                   lg: 6,
//                                   child: Padding(
//                                     padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
//                                     child: Column(
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: [
//                                         Text('province'.tr),
//                                         _buildLocationSelector(
//                                           context,
//                                           'province'.tr,
//                                           userController.provinces,
//                                           userController.selectedProvince?.name ?? 'select_province'.tr,
//                                           userController.selectProvince,
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//
//                                 ResponsiveGridCol(
//                                   child: Padding(
//                                     padding: const EdgeInsets.symmetric(horizontal: 4),
//                                     child: Row(
//                                       children: [
//                                         Expanded(
//                                           child: Column(
//                                             crossAxisAlignment: CrossAxisAlignment.start,
//                                             children: [
//                                               Text('district'.tr),
//                                               _buildLocationSelector(
//                                                 context,
//                                                 'district',
//                                                 userController.districts,
//                                                 userController.selectedDistrict?.name ??
//                                                     'select_district'.tr,
//                                                 userController.selectDistrict,
//                                                 enabled: userController.selectedProvince != null,
//                                               ),
//                                             ],
//                                           ),
//                                         ),
//                                         const SizedBox(width: 10),
//                                         // Spacing between the two fields
//                                         Expanded(
//                                           child: Column(
//                                             crossAxisAlignment: CrossAxisAlignment.start,
//                                             children: [
//                                               Text('commune'.tr),
//                                               _buildLocationSelector(
//                                                 context,
//                                                 'commune'.tr,
//                                                 userController.communes,
//                                                 userController.selectedCommune?.name ?? 'select_commune'.tr,
//                                                 userController.selectCommune,
//                                                 enabled: userController.selectedDistrict != null,
//                                               ),
//                                             ],
//                                           ),
//                                         ),
//                                         const SizedBox(width: 10),
//                                         Expanded(
//                                           child: Column(
//                                             crossAxisAlignment: CrossAxisAlignment.start,
//                                             children: [
//                                               Text('village'.tr),
//                                               _buildLocationSelector(
//                                                 context,'village'.tr,
//                                                 userController.villages,
//                                                 userController.selectedVillage?.name ?? 'select_village'.tr,
//                                                 userController.selectVillage,
//                                                 enabled: userController.selectedCommune != null,
//                                               ),
//                                             ],
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//                                 ResponsiveGridCol(
//                                   child: Padding(
//                                     padding: const EdgeInsets.symmetric(vertical: 24),
//                                     child: Row(
//                                       mainAxisAlignment: MainAxisAlignment.end,
//                                       crossAxisAlignment: CrossAxisAlignment.end,
//                                       children: [
//                                         CancelButton(
//                                             btnName: "cancel".tr,
//                                             onPress: (){
//                                               logic.clearTextField();
//                                               homeController.updateWidgetHome(const GetUserList());
//                                             }),
//                                         const SizedBox(width: 16,),
//                                         OKButton(
//                                           btnName:"update".tr,
//                                           onPress: () async {
//                                             if(widget.userModel != null){
//                                                 if(logic.cardNameController.text.isNotEmpty &&
//                                                     logic.nameController.text.isNotEmpty &&
//                                                     logic.emailController.text.isNotEmpty &&
//                                                     logic.dayofbirthdayController.text.isNotEmpty &&
//                                                     logic.addressController.text.isNotEmpty &&
//                                                     logic.nationalityController.text.isNotEmpty &&
//                                                     logic.identityNoController.text.isNotEmpty &&
//                                                     logic.selectGender.isNotEmpty &&
//                                                     logic.selectedProvince != null &&
//                                                     logic.selectedDistrict != null &&
//                                                     logic.selectedCommune != null &&
//                                                     logic.selectedVillage != null &&
//                                                     logic.selectedImageId != null
//                                                 ){
//                                                   logic.updateUser(
//                                                       userId: widget.userModel!.id.toString(),
//                                                       identityImage: logic.selectedImageId
//                                                   );
//                                                 }else{
//                                                   EasyLoading.showError("please_input_all _field".tr,duration: const Duration(seconds: 2),);
//                                                 }
//
//                                             }
//                                           },
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ),
//
//                               ],
//                             ),
//                           ),
//                         ),
//                       ),
//                   )
//               )
//                   :Expanded(
//                 child: Container(
//                     decoration: const BoxDecoration(
//                       borderRadius: BorderRadius.only(
//                         topLeft: Radius.circular(20),
//                         topRight: Radius.circular(20),
//                       ),
//                     ),
//                     child: SingleChildScrollView(
//                       child: Padding(
//                         padding: const EdgeInsets.all(16),
//                         child: FormBuilder(
//                           key: _formKey,
//                           child: ResponsiveGridRow(
//                             children: [
//                               _buildTextField(
//                                 title: 'card_number'.tr,
//                                 hintext: 'card_number'.tr,
//                                 controller: logic.cardNameController,
//                                 icon: Icons.sd_card,
//                                 focusNode: cardNumberFocusNode,
//                                 nextFocusNode: nameFocusNode,
//                               ),
//                               _buildTextField(
//                                 title: 'name'.tr,
//                                 hintext: 'name'.tr,
//                                 controller: logic.nameController,
//                                 icon: Icons.people_rounded,
//                                 focusNode: nameFocusNode,
//                                 nextFocusNode: userNameFocusNode,
//                               ),
//                               _buildTextField(
//                                 title: 'username'.tr,
//                                 hintext: 'username'.tr,
//                                 controller: logic.userNameController,
//                                 icon: Icons.people_rounded,
//                                 focusNode: userNameFocusNode,
//                                 nextFocusNode: khNameFocusNode,
//                               ),
//                               _buildTextField(
//                                 title: 'khmer_name'.tr,
//                                 hintext: 'khmer_name'.tr,
//                                 controller: logic.khmerNameController,
//                                 icon: Icons.people_rounded,
//                                 focusNode: khNameFocusNode,
//                                 nextFocusNode: phoneFocusNode,
//                               ),
//                               _buildTextField(
//                                 title: 'phone'.tr,
//                                 hintext: 'phone'.tr,
//                                 controller: logic.phoneController,
//                                 icon: Icons.phone,
//                                 focusNode: phoneFocusNode,
//                                 nextFocusNode: emailFocusNode,
//                               ),
//                               _buildTextField(
//                                 title: 'email'.tr,
//                                 hintext: 'email'.tr,
//                                 controller: logic.emailController,
//                                 icon: Icons.email,
//                                 focusNode: emailFocusNode,
//                                 nextFocusNode: passwordFocusNode,
//                               ),
//                               _buildPasswordField(
//                                 logic: logic,
//                                 isVisible: isVisible,
//                                 focusNode: passwordFocusNode,
//                                 nextFocusNode: dobFocusNode,
//                                 onVisibilityChanged: () {
//                                   setState(() {
//                                     isVisible = !isVisible;
//                                   });
//                                 },
//                               ),
//                               _buildDateField(
//                                 title: 'date_of_birth'.tr,
//                                 context: context,
//                                 logic: logic,
//                                 controller: logic.dayofbirthdayController,
//                                 focusNode: dobFocusNode,
//                                 nextFocusNode: addressFocusNode,
//                               ),
//                               _buildTextField(
//                                 title: 'address'.tr,
//                                 hintext: 'address'.tr,
//                                 controller: logic.addressController,
//                                 icon: CupertinoIcons.map_pin_ellipse,
//                                 focusNode: addressFocusNode,
//                               ),
//                               _buildImagePickerField(
//                                 title: 'select_image'.tr,
//                                 logic: logic,
//                                 onTap: logic.pickImage,
//                                 hinText: logic.selectedImageUser != null
//                                     ? logic.selectedImageUser!.path
//                                     : "no_image_selected".tr,
//                                 text: '',
//                               ),
//                               ResponsiveGridCol(
//                                 md: 6,
//                                 lg: 4,
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(horizontal: 4,vertical: 8),
//                                   child: Column(
//                                     crossAxisAlignment: CrossAxisAlignment.start,
//                                     children: [
//                                       Text('gender'.tr),
//                                       FormBuilderDropdown(
//                                         name: 'type'.tr,
//                                         initialValue: logic.selectGender,
//                                         borderRadius: BorderRadius.circular(10),
//                                         decoration: InputDecoration(
//                                           floatingLabelAlignment: FloatingLabelAlignment.center,
//                                           floatingLabelBehavior: FloatingLabelBehavior.never,
//                                           fillColor: secondaryColor,
//                                           labelText: 'please_select_gender'.tr,
//                                           filled: true,
//                                           border: OutlineInputBorder(
//                                               borderSide: BorderSide.none,
//                                               borderRadius:BorderRadius.circular(10)
//                                           ),
//                                         ),
//                                         items: logic.typeGender.map((option) => DropdownMenuItem(
//                                           value: option['value'],
//                                           child: Text(option['text']!),
//                                         ))
//                                             .toList(),
//                                         onChanged: (value) {
//                                           setState(() {
//                                             logic.selectGender = value!;
//                                           });
//                                         },
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                               ResponsiveGridCol(
//                                 md: 6,
//                                 lg: 4,
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
//                                   child: Column(
//                                     crossAxisAlignment: CrossAxisAlignment.start,
//                                     children: [
//                                       Text('province'.tr),
//                                       _buildLocationSelector(
//                                         context,
//                                         'province'.tr,
//                                         userController.provinces,
//                                         userController.selectedProvince?.name ?? 'please_select_province'.tr,
//                                         userController.selectProvince,
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//
//                               ResponsiveGridCol(
//                                 child: Padding(
//                                   padding: const EdgeInsets.symmetric(horizontal: 4),
//                                   child: Row(
//                                     children: [
//                                       Expanded(
//                                         child: Column(
//                                           crossAxisAlignment: CrossAxisAlignment.start,
//                                           children: [
//                                             Text('district'.tr,),
//                                             _buildLocationSelector(
//                                               context,
//                                               'district'.tr,
//                                               userController.districts,
//                                               userController.selectedDistrict?.name ?? 'please_select_district'.tr,
//                                               userController.selectDistrict,
//                                               enabled: userController.selectedProvince != null,
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                       const SizedBox(width: 10),
//                                       // Spacing between the two fields
//                                       Expanded(
//                                         child: Column(
//                                           crossAxisAlignment: CrossAxisAlignment.start,
//                                           children: [
//                                             Text('commune'.tr),
//                                             _buildLocationSelector(
//                                               context,
//                                               'commune'.tr,
//                                               userController.communes,
//                                               userController.selectedCommune?.name ?? 'please_select_commune'.tr,
//                                               userController.selectCommune,
//                                               enabled: userController.selectedDistrict != null,
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                       const SizedBox(width: 10),
//                                       Expanded(
//                                         child: Column(
//                                           crossAxisAlignment: CrossAxisAlignment.start,
//                                           children: [
//                                             Text('village'.tr),
//                                             _buildLocationSelector(
//                                               context,
//                                               'village',
//                                               userController.villages,
//                                               userController.selectedVillage?.name ?? 'please_select_village'.tr,
//                                               userController.selectVillage,
//                                               enabled: userController.selectedCommune != null,
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                               ),
//
//                               ResponsiveGridCol(
//                                 child: Align(
//                                   alignment: Alignment.topRight,
//                                   child: Padding(
//                                       padding: const EdgeInsets.all(20.0),
//                                       child:  Row(
//                                         mainAxisAlignment: MainAxisAlignment.end,
//                                         crossAxisAlignment: CrossAxisAlignment.end,
//                                         children: [
//                                           CancelButton(
//                                               btnName: "cancel".tr,
//                                               onPress: (){
//                                                 logic.clearTextField();
//                                                 homeController.updateWidgetHome(const GetUserList());
//                                               }),
//                                           const SizedBox(width: 16,),
//                                           OKButton(
//                                               btnName:"create".tr,
//                                               onPress: () async {
//                                                 if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
//
//                                                   if(logic.cardNameController.text.isNotEmpty &&
//                                                       logic.userNameController.text.isNotEmpty &&
//                                                       logic.khmerNameController.text.isNotEmpty &&
//                                                       logic.phoneController.text.isNotEmpty &&
//                                                       logic.emailController.text.isNotEmpty &&
//                                                       logic.passwordController.text.isNotEmpty &&
//                                                       logic.selectGender.isNotEmpty &&
//                                                       logic.dayofbirthdayController.text.isNotEmpty &&
//                                                       logic.addressController.text.isNotEmpty &&
//                                                       logic.nameController.text.isNotEmpty &&
//                                                       logic.selectedProvince != null &&
//                                                       logic.selectedDistrict != null &&
//                                                       logic.selectedCommune != null &&
//                                                       logic.selectedVillage != null &&
//                                                       logic.selectedImageUser != null
//                                                   ){
//                                                     await logic.createUser();
//                                                   }else{
//                                                     EasyLoading.showError("please_input_all_field".tr,duration: const Duration(seconds: 2),);
//                                                   }
//                                                 }
//                                               }
//                                           ),
//                                         ],
//                                       )
//                                   ),
//                                 ),
//                               ),
//
//                             ],
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//               ),
//             ],
//           ),
//         );
//       }),
//     );
//   }
//
//   ResponsiveGridCol _buildTextField({
//     int? lgIdNo ,
//     required String title,
//     required String hintext,
//     required TextEditingController controller,
//     IconData? icon,
//     required FocusNode focusNode,
//     FocusNode? nextFocusNode,
//   }) {
//     return ResponsiveGridCol(
//       md: 6,
//       lg: lgIdNo ?? 4 ,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(title, style: const TextStyle(color: Colors.white)),
//             FormBuilderTextField(
//               name: title,
//               controller: controller,
//               focusNode: focusNode,
//               decoration: InputDecoration(
//                 fillColor: secondaryColor,
//                 filled: true,
//                 border: const OutlineInputBorder(
//                   borderSide: BorderSide.none,
//                   borderRadius: BorderRadius.all(
//                     Radius.circular(10),
//                   ),
//                 ),
//                 hintText: hintext,
//                 prefixIcon: icon != null
//                     ? Icon(icon, color: Colors.white)
//                     : null,
//               ),
//               validator: FormBuilderValidators.compose([
//                 FormBuilderValidators.required(),
//               ]),
//               onSubmitted: (_) {
//                 if (nextFocusNode != null) {
//                   FocusScope.of(context).requestFocus(nextFocusNode);
//                 } else {
//                   focusNode.unfocus();
//                 }
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   ResponsiveGridCol _buildPasswordField({
//     required UserController logic,
//     required bool isVisible,
//     required FocusNode focusNode,
//     FocusNode? nextFocusNode,
//     required VoidCallback onVisibilityChanged,
//   }) {
//     return ResponsiveGridCol(
//       md: 6,
//       lg: 4,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text('Password'.tr, style: const TextStyle(color: Colors.white)),
//             FormBuilderTextField(
//               name: 'Password'.tr,
//               obscureText: isVisible,
//               controller: logic.passwordController,
//               focusNode: focusNode,
//               decoration: InputDecoration(
//                 hintText: 'Password'.tr,
//                 prefixIcon: const Icon(Icons.lock),
//                 suffixIcon: IconButton(
//                   onPressed: onVisibilityChanged,
//                   icon: !isVisible
//                       ? const Icon(Icons.visibility)
//                       : const Icon(Icons.visibility_off),
//                 ),
//                 fillColor: secondaryColor,
//                 filled: true,
//                 border: const OutlineInputBorder(
//                   borderSide: BorderSide.none,
//                   borderRadius: BorderRadius.all(
//                     Radius.circular(10),
//                   ),
//                 ),
//               ),
//               validator: FormBuilderValidators.compose([
//                 FormBuilderValidators.required(),
//               ]),
//               onSubmitted: (_) {
//                 if (nextFocusNode != null) {
//                   FocusScope.of(context).requestFocus(nextFocusNode);
//                 } else {
//                   focusNode.unfocus();
//                 }
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   ResponsiveGridCol _buildDateField({
//     required String title,
//     required BuildContext context,
//     required UserController logic,
//     required TextEditingController controller,
//     required FocusNode focusNode,
//     FocusNode? nextFocusNode,
//   }) {
//     return ResponsiveGridCol(
//       md: 6,
//       lg: 4,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(title, style: const TextStyle(color: Colors.white)),
//             FormBuilderTextField(
//               name: 'yy/mm/dd',
//               controller: controller,
//               focusNode: focusNode,
//               decoration: InputDecoration(
//                 hintText: 'Date of Birth'.tr,
//                 prefixIcon: const Icon(Icons.date_range),
//
//                 fillColor: secondaryColor,
//                 filled: true,
//                 border: const OutlineInputBorder(
//                   borderSide: BorderSide.none,
//                   borderRadius: BorderRadius.all(
//                     Radius.circular(10),
//                   ),
//                 ),
//               ),
//               validator: FormBuilderValidators.compose([
//                 FormBuilderValidators.required(),
//               ]),
//               readOnly: true,
//               onTap: () {
//                 logic.selectDate(context, controller);
//               },
//               onSubmitted: (_) {
//                 if (nextFocusNode != null) {
//                   FocusScope.of(context).requestFocus(nextFocusNode);
//                 } else {
//                   focusNode.unfocus();
//                 }
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   ResponsiveGridCol _buildImagePickerField({
//     int? lgSelectImg ,
//     required String title,
//     required String hinText,
//     required UserController logic,
//     required Function onTap,
//     required String text,
//   }) {
//     return ResponsiveGridCol(
//       md: 6,
//       lg: lgSelectImg ?? 4,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 4),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(title, style: const TextStyle(color: Colors.white)),
//             FormBuilderTextField(
//               name: text,
//               readOnly: true,
//               onTap: () => onTap(),
//               controller: TextEditingController(text: text),
//               decoration: InputDecoration(
//                 hintText: hinText,
//                 prefixIcon: const Icon(Icons.image),
//
//                 fillColor: secondaryColor,
//                 filled: true,
//                 border: const OutlineInputBorder(
//                   borderSide: BorderSide.none,
//                   borderRadius: BorderRadius.all(
//                     Radius.circular(10),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _buildLocationSelector(BuildContext context, String label,
//       List<Address> data, String currentValue, Function(Address) onSelect,
//       {bool enabled = true}) {
//     return FormBuilderTextField(
//       name: label,
//       readOnly: true,
//       onTap: enabled
//           ? () => _showSelectionDialog(
//           context: context, title: label, data: data, onSelect: onSelect)
//           : null,
//       controller: TextEditingController(text: currentValue),
//       decoration: InputDecoration(
//         border: const OutlineInputBorder(
//           borderSide: BorderSide.none,
//           borderRadius: BorderRadius.all(
//             Radius.circular(10),
//           ),
//         ),
//         filled: true,
//         fillColor: secondaryColor,
//         hintText: label,
//         suffixIcon: const Icon(Icons.arrow_drop_down),
//       ),
//       validator: enabled ? FormBuilderValidators.required() : null,
//     );
//   }
//
//   void _showSelectionDialog({
//     required BuildContext context,
//     required String title,
//     required List<Address> data,
//     required Function(Address) onSelect,
//   }) {
//     final double dialogWidth =
//     kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;
//
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return AlertDialog(
//           backgroundColor: bgColor,
//           title: Text(title),
//           contentPadding: const EdgeInsets.symmetric(horizontal: 4, vertical: 8),
//           content: SizedBox(
//             width: dialogWidth,
//             child: ListView(
//               children: data.map((item) {
//                 return ListTile(
//                   title: Text(item.name!),
//                   subtitle: Text(item.description!),
//                   onTap: () {
//                     onSelect(item);
//                     Navigator.pop(context);
//                   },
//                 );
//               }).toList(),
//             ),
//           ),
//         );
//       },
//     );
//   }
//
// }
