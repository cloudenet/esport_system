import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/khmer_calender.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../data/controllers/user_controller.dart';
import '../../../data/model/list_user_model.dart';
import '../../../helper/cotainer_widget.dart';
import '../../../helper/images.dart';
import '../../../util/text_style.dart';


class DetailUserScreen extends StatefulWidget {
  final ListUserModel? data;
  const DetailUserScreen({super.key,required this.data});

  @override
  State<DetailUserScreen> createState() => _DetailUserScreenState();
}

class _DetailUserScreenState extends State<DetailUserScreen> {

  var teamController = Get.find<TeamController>();
  var userController = Get.find<UserController>();
  String? dobKhmer;
  String? expireKhmer;
  List<String> imagePaths = [] ;

  @override
  void initState() {
    String cleanedData = widget.data!.identityPhoto.toString().replaceAll(RegExp(r'[\[\]]'), '');
    imagePaths = cleanedData.split(", ");
    for(var a in imagePaths){
      print("ID-Photo:  $a");
    }
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    print("kkkkk ${widget.data!.photo}");
    // final Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
        mobile: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                userInfo(),
                const SizedBox(height: 20,),
                _buildCard(isMobile: true, isTablet: false),
                const SizedBox(height: 20,),
                userIdentity(isTablet: false,isMobile: true)
              ],
            ),
          ),
        ),
        tablet: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                userInfo(),
                const SizedBox(height: 20,),
                _buildCard(isMobile: false, isTablet: true),
                const SizedBox(height: 20,),
                userIdentity(isTablet: true,isMobile: false)
              ],
            ),
          ),
        ),
        desktop: Padding(
          padding: const EdgeInsets.all(16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                userInfo(),
                const SizedBox(height: 10,),
                _buildCard(isMobile: false, isTablet: false),
                const SizedBox(height: 20,),
                userIdentity(isTablet: false,isMobile: false)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCard({required bool? isMobile, required bool? isTablet}){
    return LayoutBuilder(
      builder: (context, constraints) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10,),
            Text("user_card".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            const SizedBox(height: 10,),
            Container(
              decoration: BoxDecoration(
                color: secondaryColor,
                borderRadius: BorderRadius.circular(10)
              ),
              padding: const EdgeInsets.all(24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if(isMobile! || isTablet!)...[
                    RepaintBoundary(
                      key: userController.globalKey,
                      child: Container(
                        width: 540,
                        height: 340,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 5,
                              offset: Offset(0, 2),
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Container(
                                decoration: const BoxDecoration(
                                    color: Colors.indigo,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10),
                                    )),
                                // height: MediaQuery.of(context).size.height * 0.07,
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text("ប័ណ្ណសម្គាល់ខ្លួន",
                                                style: TextStylesHelper.khmerH2),
                                            Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា",
                                                style: TextStylesHelper.khmerH2)
                                          ],
                                        )),
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            const Text(
                                              "Federation of Boules and Pétanque of Cambodia",
                                              style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),
                                            ),
                                            Text("identity_no".tr,
                                                style: const TextStyle(fontSize: 9, fontWeight: FontWeight.bold))
                                          ],
                                        )),
                                  ],
                                )),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'No: ${widget.data?.cardNumber.toString()}',
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red,
                                        fontSize: 12),
                                  ),
                                  const SizedBox(height: 5,),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 5,
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            khmerText(sureName: 'គោត្តនាម និងនាម', name: widget.data?.nameKhmer!.isNotEmpty ==true ? widget.data!.nameKhmer.toString():"N/A"  ),
                                            const SizedBox(height: 3,),
                                            englishText(sureName: 'Surename & Name ', name: widget.data?.name!.isNotEmpty ==true ? widget.data!.name!.toUpperCase():"N/A"  ),
                                            const SizedBox(height: 3,),
                                            khmerText(sureName: 'ថ្ងៃខែឆ្នាំកំណើត', name: convertToKhmerDate(DateTime.parse(widget.data!.dob.toString())) ?? ""),
                                            const SizedBox(height: 3,),
                                            englishText(sureName: 'Date of Birth', name: convertToEnglishDateFormatted(DateTime.parse(widget.data!.dob.toString()))),
                                            const SizedBox(height: 3,),
                                            khmerText(sureName: 'តួនាទី', name: 'មន្ត្រី'),
                                            const SizedBox(height: 3,),
                                            englishText(sureName: 'Position', name: 'Officer'),

                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 5,
                                        child: Row(
                                          children: [
                                            Expanded(
                                              flex: 5,
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  khmerText(sureName: 'ភេទ', name: 'ប្រុស'),
                                                  const SizedBox(height: 3,),
                                                  englishText(sureName: 'Sex', name: toBeginningOfSentenceCase(widget.data?.gender?.toLowerCase() ?? '')),
                                                  const SizedBox(height: 3,),
                                                  khmerText(sureName: 'សញ្ជាតិ', name: 'ខ្មែរ'),
                                                  const SizedBox(height: 3,),
                                                  englishText(sureName: 'National', name: '${widget.data?.nationality != "null" ? toBeginningOfSentenceCase(widget.data?.nationality?.toLowerCase()) : "N/A" }'),
                                                  const SizedBox(height: 3,),
                                                  khmerText(sureName: 'សុពលភាព', name: convertToKhmerDate(DateTime.parse(widget.data!.dob.toString()))),
                                                  const SizedBox(height: 3,),
                                                  englishText(sureName: 'Expire Date', name: convertToEnglishDateFormatted(DateTime.parse(widget.data!.dob.toString()))),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                                flex: 2,
                                                child: widget.data!.photo != null ?
                                                Image.network('${AppConstants.baseUrl}${widget.data?.photo}',
                                                  width: 110,
                                                  height: 120,
                                                )
                                                    :Image.asset('assets/images/no-image.png', width: 110,height: 120,)
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex: 5,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់ខេត្តកំពត", style: TextStylesHelper.khmerH3),
                                            Text("ប្រធាន", style: TextStylesHelper.khmerH3),
                                            // Image.network(
                                            //   'https://static.vecteezy.com/system/resources/previews/000/538/077/original/manual-signature-for-documents-on-white-background-hand-drawn-calligraphy-lettering-vector-illustration.jpg',
                                            //   width: 200,
                                            //   height: 50,
                                            // ),
                                            Image.asset(Images.sign,height: 50,width: 200,),
                                            Text("សុខ សូកាន", style: TextStylesHelper.khmerH3),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        flex: 5,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា", style: TextStylesHelper.khmerH3),
                                            Text("ប្រធាន", style: TextStylesHelper.khmerH3),
                                            // Image.network(
                                            //   'https://static.vecteezy.com/system/resources/previews/000/538/077/original/manual-signature-for-documents-on-white-background-hand-drawn-calligraphy-lettering-vector-illustration.jpg',
                                            //   width: 200,
                                            //   height: 50,
                                            // ),
                                            Image.asset(Images.sign,height: 50,width: 200,),
                                            Text("សុខ សូកាន", style: TextStylesHelper.khmerH3),
                                          ],
                                        ),
                                      ),
                                      const Expanded(flex: 1,child: SizedBox())
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 10,),
                    RepaintBoundary(
                      key: userController.globalKeyBack,
                      child: Container(
                        width: 540,
                        height: 340,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 5,
                              offset: Offset(0, 2),
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    children: [
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      Image.asset(
                                        'assets/images/Boules.png',
                                        height: 100,
                                      ),
                                      const SizedBox(height: 10.0),
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 16.0),
                                        child: Column(
                                          children: [
                                            Text(
                                              'សហព័ន្ធកីឡាប៊ុល និងប៉េតង់កម្ពុជា',
                                              style: TextStylesHelper.khmerH1,
                                              textAlign: TextAlign.center,
                                            ),
                                            const Text(
                                              'Federation of Boules and Pétanque of Cambodia',
                                              style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.indigo,
                                              ),
                                              textAlign: TextAlign.center,
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 16.0),
                                      Container(
                                        padding: const EdgeInsets.symmetric(horizontal: 24),
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                contactInfoRow(Icons.email,
                                                    'petanquecambodia@gmail.com',
                                                    twoIcon: false),
                                                const SizedBox(width: 5.0),
                                                contactInfoRow(
                                                    Icons.phone, '(+855) 78 239 177',
                                                    twoIcon: true,
                                                    icon2: Icons.telegram),
                                              ],
                                            ),
                                            const SizedBox(height: 5.0),
                                            contactInfoRow(Icons.facebook,
                                                'www.facebook.com/PetanqueCambodia',
                                                twoIcon: false),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                            Container(
                              height: 60,
                              padding: const EdgeInsets.all(.0),
                              decoration: const BoxDecoration(
                                  color: Colors.indigo,
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  )),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(Icons.location_on,
                                      color: Colors.white),
                                  const SizedBox(width: 8.0),
                                  Flexible(
                                    child: Text(
                                      'ពហុកីឡដ្ឋានជាតិ សង្កាត់វាលវង់ ខណ្ឌ៧មករា រាជធានីភ្នំពេញ',
                                      textAlign: TextAlign.center,
                                      style: TextStylesHelper.khmerH2,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]else...[
                    Row(
                      children: [
                        RepaintBoundary(
                          key: userController.globalKey,
                          child: Container(
                            width: 540,
                            height: 340,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Container(
                                    decoration: const BoxDecoration(
                                        color: Colors.indigo,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                        )),
                                    // height: MediaQuery.of(context).size.height * 0.07,
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text("ប័ណ្ណសម្គាល់ខ្លួន",
                                                    style: TextStylesHelper.khmerH2),
                                                Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា",
                                                    style: TextStylesHelper.khmerH2)
                                              ],
                                            )),
                                        Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                const Text(
                                                  "Federation of Boules and Pétanque of Cambodia",
                                                  style: TextStyle(fontSize: 9, fontWeight: FontWeight.bold),
                                                ),
                                                Text("identity_no".tr,
                                                    style: const TextStyle(fontSize: 9, fontWeight: FontWeight.bold))
                                              ],
                                            )),
                                      ],
                                    )),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'No: ${widget.data?.cardNumber.toString()}',
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.red,
                                            fontSize: 12),
                                      ),
                                      const SizedBox(height: 5,),
                                      Row(
                                        children: [
                                          Expanded(
                                            flex: 5,
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: [
                                                khmerText(sureName: 'គោត្តនាម និងនាម', name: widget.data?.nameKhmer!.isNotEmpty ==true ? widget.data!.nameKhmer.toString():"N/A"  ),
                                                const SizedBox(height: 3,),
                                                englishText(sureName: 'Surename & Name ', name: widget.data?.name!.isNotEmpty ==true ? widget.data!.name!.toUpperCase():"N/A"  ),
                                                const SizedBox(height: 3,),
                                                khmerText(sureName: 'ថ្ងៃខែឆ្នាំកំណើត', name: convertToKhmerDate(DateTime.parse(widget.data!.dob.toString())) ?? ""),
                                                const SizedBox(height: 3,),
                                                englishText(sureName: 'Date of Birth', name: convertToEnglishDateFormatted(DateTime.parse(widget.data!.dob.toString()))),
                                                const SizedBox(height: 3,),
                                                khmerText(sureName: 'តួនាទី', name: 'មន្ត្រី'),
                                                const SizedBox(height: 3,),
                                                englishText(sureName: 'Position', name: 'Officer'),

                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            flex: 5,
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 5,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    children: [
                                                      khmerText(sureName: 'ភេទ', name: 'ប្រុស'),
                                                      const SizedBox(height: 3,),
                                                      englishText(sureName: 'Sex', name: toBeginningOfSentenceCase(widget.data?.gender?.toLowerCase() ?? '')),
                                                      const SizedBox(height: 3,),
                                                      khmerText(sureName: 'សញ្ជាតិ', name: 'ខ្មែរ'),
                                                      const SizedBox(height: 3,),
                                                      englishText(sureName: 'National', name: '${widget.data?.nationality != "null" ? toBeginningOfSentenceCase(widget.data?.nationality?.toLowerCase()) : "N/A" }'),
                                                      const SizedBox(height: 3,),
                                                      khmerText(sureName: 'សុពលភាព', name: convertToKhmerDate(DateTime.parse(widget.data!.dob.toString()))),
                                                      const SizedBox(height: 3,),
                                                      englishText(sureName: 'Expire Date', name: convertToEnglishDateFormatted(DateTime.parse(widget.data!.dob.toString()))),
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                    flex: 2,
                                                    child: widget.data!.photo != null ?
                                                    Image.network('${AppConstants.baseUrl}${widget.data?.photo}',
                                                      width: 110,
                                                      height: 120,
                                                    )
                                                        :Image.asset('assets/images/no-image.png', width: 110,height: 120,)
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 5,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់ខេត្តកំពត", style: TextStylesHelper.khmerH3),
                                                Text("ប្រធាន", style: TextStylesHelper.khmerH3),
                                                // Image.network(
                                                //   'https://static.vecteezy.com/system/resources/previews/000/538/077/original/manual-signature-for-documents-on-white-background-hand-drawn-calligraphy-lettering-vector-illustration.jpg',
                                                //   width: 200,
                                                //   height: 50,
                                                // ),
                                                Image.asset(Images.sign,height: 50,width: 200,),
                                                Text("សុខ សូកាន", style: TextStylesHelper.khmerH3),
                                              ],
                                            ),
                                          ),
                                          Expanded(
                                            flex: 5,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Text("សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា", style: TextStylesHelper.khmerH3),
                                                Text("ប្រធាន", style: TextStylesHelper.khmerH3),
                                                // Image.network(
                                                //   'https://static.vecteezy.com/system/resources/previews/000/538/077/original/manual-signature-for-documents-on-white-background-hand-drawn-calligraphy-lettering-vector-illustration.jpg',
                                                //   width: 200,
                                                //   height: 50,
                                                // ),
                                                Image.asset(Images.sign,height: 50,width: 200,),
                                                Text("សុខ សូកាន", style: TextStylesHelper.khmerH3),
                                              ],
                                            ),
                                          ),
                                          const Expanded(flex: 1,child: SizedBox())
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(width: 10,),
                        RepaintBoundary(
                          key: userController.globalKeyBack,
                          child: Container(
                            width: 540,
                            height: 340,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 5,
                                  offset: Offset(0, 2),
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        children: [
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          Image.asset(
                                            'assets/images/Boules.png',
                                            height: 100,
                                          ),
                                          const SizedBox(height: 10.0),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 16.0),
                                            child: Column(
                                              children: [
                                                Text(
                                                  'សហព័ន្ធកីឡាប៊ុល និងប៉េតង់កម្ពុជា',
                                                  style: TextStylesHelper.khmerH1,
                                                  textAlign: TextAlign.center,
                                                ),
                                                const Text(
                                                  'Federation of Boules and Pétanque of Cambodia',
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.indigo,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                          ),
                                          const SizedBox(height: 16.0),
                                          Container(
                                            padding: const EdgeInsets.symmetric(horizontal: 24),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    contactInfoRow(Icons.email,
                                                        'petanquecambodia@gmail.com',
                                                        twoIcon: false),
                                                    const SizedBox(width: 5.0),
                                                    contactInfoRow(
                                                        Icons.phone, '(+855) 78 239 177',
                                                        twoIcon: true,
                                                        icon2: Icons.telegram),
                                                  ],
                                                ),
                                                const SizedBox(height: 5.0),
                                                contactInfoRow(Icons.facebook,
                                                    'www.facebook.com/PetanqueCambodia',
                                                    twoIcon: false),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    )),
                                Container(
                                  height: 60,
                                  padding: const EdgeInsets.all(.0),
                                  decoration: const BoxDecoration(
                                      color: Colors.indigo,
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(10),
                                        bottomRight: Radius.circular(10),
                                      )),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(Icons.location_on,
                                          color: Colors.white),
                                      const SizedBox(width: 8.0),
                                      Flexible(
                                        child: Text(
                                          'ពហុកីឡដ្ឋានជាតិ សង្កាត់វាលវង់ ខណ្ឌ៧មករា រាជធានីភ្នំពេញ',
                                          textAlign: TextAlign.center,
                                          style: TextStylesHelper.khmerH2,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                  const SizedBox(height: 10,),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 150,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.blueAccent
                      ),
                      child: Center(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("save_image".tr),
                            IconButton(
                              onPressed: () async {
                                await userController.renderWidgetsToImages([
                                  userController.globalKey,
                                  userController.globalKeyBack
                                ]);
                              },
                              icon: const Icon(Icons.save_alt),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }
  Widget khmerText({required String sureName, required String name}) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Text(sureName, style: TextStylesHelper.khmerTextNormal),
        ),
        Expanded(
          flex: 4,
          child: Row(
            children: [
              Text(": ", style: TextStylesHelper.khmerTextMoul),
              Text(name, style: TextStylesHelper.khmerTextMoul),
            ],
          ),
        ),
      ],
    );
  }

  Widget englishText({required String sureName, required String name}) {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Text(
            sureName,
            style: const TextStyle(
                fontWeight: FontWeight.bold, color: Colors.indigo, fontSize: 9),
          ),
        ),
        Expanded(
          flex: 4,
          child: Row(
            children: [
              Text(": ", style: TextStylesHelper.khmerTextMoul),
              Text(name, style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.indigo, fontSize: 9,),
              ),
            ],
          ),
        ),
      ],
    );
  }
  Widget contactInfoRow(IconData icon, String text,
      {required bool? twoIcon, IconData? icon2}) {
    return Row(
      children: [
        twoIcon!
            ? Row(
          children: [
            Icon(icon, color: Colors.indigo),
            const SizedBox(
              width: 2,
            ),
            Icon(icon2, color: Colors.indigo)
          ],
        )
            : Icon(icon, color: Colors.indigo),
        const SizedBox(width: 8.0),
        Text(
          text,
          style: const TextStyle(color: Colors.indigo, fontSize: 12),
        ),
      ],
    );
  }
  Widget userInfo(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 50,),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            children: [
              Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.blueAccent, width: 2.0),
                ),
                child: ClipOval(
                  child: Image.network(
                    widget.data!.photo.toString(),
                    fit: BoxFit.cover,
                    errorBuilder: (context, error, stackTrace) {
                      return const Icon(Icons.person, size: 50, color: Colors.grey);
                    },
                    loadingBuilder: (context, child, loadingProgress) {
                      if (loadingProgress == null) return child;
                      return const Center(child: CircularProgressIndicator());
                    },
                  ),
                ),
              ),
              const SizedBox(width: 16.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                children: [
                  Text(
                    widget.data!.name.toString(),
                    style: const TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,  // Bold name
                    ),
                  ),
                  const SizedBox(height: 4.0),
                  Container(
                    width: 100,
                    height: 30,
                    decoration: BoxDecoration(
                        color: secondaryColor,
                        borderRadius: BorderRadius.circular(5)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: 10,
                          height: 10,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: widget.data?.userStatus == 'active'
                                ? Colors.green
                                : Colors.red,
                          ),
                        ),
                        Text(widget.data!.userStatus.toString())
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        const SizedBox(height: 20,),
        Text("personal_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.all(24),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: secondaryColor,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "full_name".tr,
                        textData: widget.data!.name != "null" ? widget.data!.name.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "khmer_name".tr,
                        textData: widget.data!.nameKhmer != "null" ? widget.data!.nameKhmer.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "gender".tr,
                        textData: widget.data!.gender != "null" ? widget.data!.gender.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "date_of_birth".tr,
                        textData: widget.data!.dob != "null" ? formatDate(widget.data!.dob.toString()) : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "nationality".tr,
                        textData: widget.data!.nationality != "null" ? widget.data!.nationality.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "identity_number".tr,
                        textData: widget.data!.identityNo != "null" ? widget.data!.identityNo.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        const SizedBox(height: 20,),
        Text("contact_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.all(24),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: secondaryColor,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "phone_number".tr,
                        textData: widget.data!.phone != "null" ? widget.data!.phone.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "email".tr,
                        textData: widget.data!.email != "null" ? widget.data!.email.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "province".tr,
                        textData: widget.data!.provinceName != "null" ? widget.data!.provinceName.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "district".tr,
                        textData: widget.data!.districtName != "null" ? widget.data!.districtName.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "commune".tr,
                        textData: widget.data!.communeName != "null" ? widget.data!.communeName.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "village".tr,
                        textData: widget.data!.villageName != "null" ? widget.data!.villageName.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        const SizedBox(height: 20,),
        Text("account_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
        Container(
          width: double.infinity,
          padding: const EdgeInsets.all(24),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: secondaryColor,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "card_number".tr,
                        textData: widget.data!.cardNumber != "null" ? widget.data!.cardNumber.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "role".tr,
                        textData: widget.data!.role.toString(),
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),

                ],
              ),
              const SizedBox(height: 10,),
              Row(
                children: [
                  Expanded(
                    child: buildText(
                        title: "user_status".tr,
                        textData: widget.data!.userStatus != "null" ? widget.data!.userStatus.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                  const SizedBox(width: 16,),
                  Expanded(
                    child: buildText(
                        title: "last_login".tr,
                        textData: widget.data!.lastLoginAt != "null" ? widget.data!.lastLoginAt.toString() : "N/A",
                        height: 50,
                        padding: 8,
                        alignment: Alignment.centerLeft
                    ),
                  ),
                ],
              ),

            ],
          ),
        ),
      ],
    );
  }

  Widget userIdentity({required bool isMobile, required bool isTablet}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("user_identity".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
        const SizedBox(height: 10,),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: secondaryColor,
            borderRadius: BorderRadius.circular(10)
          ),
          padding: const EdgeInsets.all(24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if(isMobile || isTablet)...[
                imgIdentity(imagePaths[0],isMobile: isMobile,isTablet: isMobile),
                const SizedBox(height: 10,),
                imgIdentity(imagePaths[1],isMobile: isMobile,isTablet: isMobile),
              ]else...[
                Row(
                  children: [
                    imgIdentity(imagePaths[0],isMobile: isMobile,isTablet: isMobile),
                    const SizedBox(width: 10,),
                    imgIdentity(imagePaths[1],isMobile: isMobile,isTablet: isMobile),
                  ],
                )
              ]
            ],
          ),
        ),
      ],
    );
  }

  Widget imgIdentity (String imgId,{required bool isMobile, required bool isTablet}){
    return Container(
      width: isMobile ? 440 : isTablet ? 540 : 540,
      height: isMobile ? 200 : isTablet ? 300 : 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.blueAccent, width: 2.0),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.network(
          imgId,
          fit: BoxFit.cover,
          errorBuilder: (context, error, stackTrace) {
            return Image.asset(
              Images.noImage,
              fit: BoxFit.cover,
            );
          },
          loadingBuilder: (context, child, loadingProgress) {
            if (loadingProgress == null) return child;
            return const Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}



