
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import '../../../helper/images.dart';

class GenerateCardPdf {

  static Future<Uint8List> generateCardPDF() async {
    final pdf = pw.Document();
    final fontKhmerMoul = pw.Font.ttf(await rootBundle.load('assets/fonts/khmer_moul_regular.ttf'));
    //final fontKhmerMoul = pw.Font.ttf(await rootBundle.load('assets/fonts/KhmerOSMoul.ttf'));
    final fontKhmerBattambong = pw.Font.ttf(await rootBundle.load('assets/fonts/khmer_os_battambang_regular.ttf'));
    final telegramIcon = (await rootBundle.load(Images.telegramIcon)).buffer.asUint8List();
    final noImage = (await rootBundle.load(Images.noImage)).buffer.asUint8List();
    final logoBoules = (await rootBundle.load(Images.logoBoules)).buffer.asUint8List();
    final locationIcon = (await rootBundle.load(Images.locationIcon)).buffer.asUint8List();
    final phoneIcon = (await rootBundle.load(Images.phoneIcon)).buffer.asUint8List();
    final facebookIcon = (await rootBundle.load(Images.facebookIcon)).buffer.asUint8List();
    final emailIcon = (await rootBundle.load(Images.mailIcon)).buffer.asUint8List();
    const bgColor = PdfColor.fromInt(0xFF212332);


    pdf.addPage(
      pw.MultiPage(
        //margin: pw.EdgeInsets.zero,
        mainAxisAlignment: pw.MainAxisAlignment.center,
        crossAxisAlignment: pw.CrossAxisAlignment.center,
        pageTheme: pw.PageTheme(
          pageFormat: PdfPageFormat(450 * PdfPageFormat.mm, PdfPageFormat.a4.height),
          theme: pw.ThemeData.withFont(
            base: fontKhmerMoul,
          ),
          buildBackground: (pw.Context context) => pw.FullPage(
            ignoreMargins: true,
            child: pw.Container(
              color: bgColor, // Set your desired background color here
            ),
          ),
        ),
        build: (pw.Context context) => [
          pw.Container(
            width: 500,
            height: 322,
            decoration: pw.BoxDecoration(
              color: PdfColors.white,
              borderRadius: pw.BorderRadius.circular(10),

            ),
            child: pw.Column(
              children: [
                pw.Container(
                  decoration: const pw.BoxDecoration(
                    color: PdfColors.indigo,
                    borderRadius: pw.BorderRadius.only(
                      topLeft: pw.Radius.circular(10),
                      topRight: pw.Radius.circular(10),
                    ),
                  ),
                  height: 65,
                  padding: const pw.EdgeInsets.all(8.0),
                  child: pw.Row(
                    children: [
                      pw.Expanded(
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.start,
                          mainAxisAlignment: pw.MainAxisAlignment.center,
                          children: [
                            pw.Text(
                              "ប័ណ្ណសម្គាល់ខ្លួន",
                              style: pw.TextStyle(
                                color: PdfColors.white,
                                font: fontKhmerMoul,
                                fontSize: 14,
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                            pw.Text(
                              "សហព័ន្ធកីឡាប៊ូល និងប៉ាតង់កម្ពុជា",
                              style: pw.TextStyle(
                                color: PdfColors.white,
                                font: fontKhmerMoul,
                                fontSize: 14,
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      pw.Expanded(
                        child: pw.Column(
                          crossAxisAlignment: pw.CrossAxisAlignment.end,
                          mainAxisAlignment: pw.MainAxisAlignment.center,
                          children: [
                            pw.Text(
                              "Federation of Boules and Pétanque of Cambodia",
                              style: pw.TextStyle(
                                color: PdfColors.white,
                                fontSize: 9,
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                            pw.Text(
                              "Identity Card",
                              style: pw.TextStyle(
                                color: PdfColors.white,
                                fontSize: 9,
                                fontWeight: pw.FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                pw.Padding(
                  padding: const pw.EdgeInsets.all(8.0),
                  child: pw.Column(
                    crossAxisAlignment: pw.CrossAxisAlignment.start,
                    children: [
                      pw.Text(
                        'No: 12345678', // Replace with dynamic value
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.bold,
                          color: PdfColors.red,
                          fontSize: 12,
                        ),
                      ),
                      pw.SizedBox(height: 5),
                      pw.Row(
                        children: [
                          pw.Expanded(
                            flex: 5,
                            child: pw.Column(
                              crossAxisAlignment: pw.CrossAxisAlignment.start,
                              children: [
                                khmerTextPdf('គោត្តនាម និងនាម', 'John Doe',fontKhmerBattambong), // Replace with dynamic value
                                pw.SizedBox(height: 3),
                                englishTextPdf('Surename & Name', 'John Doe',), // Replace with dynamic value
                                pw.SizedBox(height: 3),
                                khmerTextPdf('ថ្ងៃខែឆ្នាំកំណើត', '01-01-2000',fontKhmerBattambong), // Replace with dynamic value
                                pw.SizedBox(height: 3),
                                englishTextPdf('Date of Birth', '01-01-2000'), // Replace with dynamic value
                                pw.SizedBox(height: 3),
                                khmerTextPdf('តួនាទី', 'Member',fontKhmerBattambong), // Replace with dynamic value
                                pw.SizedBox(height: 3),
                                englishTextPdf('Position', 'Deputy Secretary General'), // Replace with dynamic value
                              ],
                            ),
                          ),
                          pw.Expanded(
                            flex: 5,
                            child: pw.Row(
                              children: [
                                pw.Expanded(
                                  flex: 5,
                                  child: pw.Column(
                                    crossAxisAlignment: pw.CrossAxisAlignment.start,
                                    children: [
                                      khmerTextPdf('ភេទ', 'Male',fontKhmerBattambong), // Replace with dynamic value
                                      pw.SizedBox(height: 3),
                                      englishTextPdf('Sex', 'Male'), // Replace with dynamic value
                                      pw.SizedBox(height: 3),
                                      khmerTextPdf('សញ្ជាតិ', 'Cambodian',fontKhmerBattambong), // Replace with dynamic value
                                      pw.SizedBox(height: 3),
                                      englishTextPdf('Nationality', 'Cambodian'), // Replace with dynamic value
                                      pw.SizedBox(height: 3),
                                      khmerTextPdf('សុពលភាព', '២៥/មេសា/២០២៥',fontKhmerBattambong), // Replace with dynamic value
                                      pw.SizedBox(height: 3),
                                      englishTextPdf('Expire Date', '25/04/2025'), // Replace with dynamic value
                                    ],
                                  ),
                                ),
                                if(1==2)
                                  pw.Expanded(
                                    flex: 2,
                                    child: pw.Image(pw.MemoryImage(noImage), height: 80,width: 80, fit: pw.BoxFit.contain),
                                    //child: pw.SizedBox(width: 110, height: 120,)
                                  ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      pw.Row(
                        children: [
                          pw.Expanded(
                            flex: 5,
                            child: pw.Container(),
                          ),
                          pw.Expanded(
                            flex: 6,
                            child: pw.Column(
                              crossAxisAlignment: pw.CrossAxisAlignment.center,
                              children: [
                                pw.Text(
                                  "សហព័ន្ធកីឡាប៊ូល និងប៉េតង់កម្ពុជា",
                                  style: pw.TextStyle(
                                    font: fontKhmerMoul,
                                    fontSize: 11,
                                    fontWeight: pw.FontWeight.bold,
                                    color: PdfColors.indigo,
                                  ),
                                ),
                                pw.Text(
                                  "ប្រធាន",
                                  style: pw.TextStyle(
                                    font: fontKhmerMoul,
                                    fontSize: 11,
                                    fontWeight: pw.FontWeight.bold,
                                    color: PdfColors.indigo,
                                  ),
                                ),
                                //if(1==2)
                                pw.Image(pw.MemoryImage(noImage), height: 50,width: 200, fit: pw.BoxFit.contain),
                                //pw.SizedBox(width: 200, height: 50,),
                                pw.Text(
                                  "សុខ សូកាន",
                                  style: pw.TextStyle(
                                    font: fontKhmerMoul,
                                    fontSize: 11,
                                    fontWeight: pw.FontWeight.bold,
                                    color: PdfColors.indigo,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          pw.SizedBox(height: 20,),
          pw.Container(
            width: 500,
            height: 322,
            decoration: pw.BoxDecoration(
              color: PdfColors.white,
              borderRadius: pw.BorderRadius.circular(10),
              // boxShadow: const [
              //   pw.BoxShadow(
              //     color: PdfColors.black,
              //     blurRadius: 5,
              //     offset: PdfPoint(0, 2),
              //   ),
              // ],
            ),
            child: pw.Column(
              mainAxisSize: pw.MainAxisSize.min,
              children: [
                pw.Expanded(
                  child: pw.Padding(
                    padding: const pw.EdgeInsets.all(8.0),
                    child: pw.Column(
                      children: [
                        pw.SizedBox(height: 10),
                        //if(1==2)
                        pw.Padding(
                          padding: const pw.EdgeInsets.all(8.0),
                          child: pw.Image(pw.MemoryImage(logoBoules), height: 100, fit: pw.BoxFit.contain),
                        ),
                        pw.SizedBox(height: 10),
                        pw.Padding(
                          padding: const pw.EdgeInsets.symmetric(horizontal: 16.0),
                          child: pw.Column(
                            children: [
                              pw.Text(
                                'សហព័ន្ធកីឡាប៊ុល និងប៉េតង់កម្ពុជា',
                                style: pw.TextStyle(
                                  font: fontKhmerMoul,
                                  fontSize: 20,
                                  fontWeight: pw.FontWeight.bold,
                                  color: PdfColors.indigo,
                                ),
                                textAlign: pw.TextAlign.center,
                              ),
                              pw.Text(
                                'Federation of Boules and Pétanque of Cambodia',
                                style: pw.TextStyle(
                                  fontSize: 16,
                                  fontWeight: pw.FontWeight.bold,
                                  color: PdfColors.indigo,
                                ),
                                textAlign: pw.TextAlign.center,
                              ),
                            ],
                          ),
                        ),
                        pw.SizedBox(height: 16),
                        pw.Container(
                          padding: const pw.EdgeInsets.symmetric(horizontal: 24),
                          child: pw.Column(
                            children: [
                              pw.Row(
                                mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                                children: [
                                  contactInfoRowPdf(emailIcon, 'petanquecambodia@gmail.com', false),
                                  contactInfoRowPdf(telegramIcon, '(+855) 78 239 177', true, image2: phoneIcon),
                                ],
                              ),
                              pw.SizedBox(height: 5),
                              contactInfoRowPdf(facebookIcon, 'www.facebook.com/PetanqueCambodia', false),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                pw.Container(
                  height: 60,
                  padding: const pw.EdgeInsets.all(.0),
                  decoration: const pw.BoxDecoration(
                      color: PdfColors.indigo,
                      borderRadius: pw.BorderRadius.only(
                        bottomLeft: pw.Radius.circular(10),
                        bottomRight: pw.Radius.circular(10),
                      )
                  ),
                  child: pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.center,
                    children: [
                      pw.Image(pw.MemoryImage(locationIcon), height: 20,),
                      pw.SizedBox(width: 8.0),
                      pw.Flexible(
                        child: pw.Text(
                          'ពហុកីឡដ្ឋានជាតិ សង្កាត់វាលវង់ ខណ្ឌ៧មករា រាជធានីភ្នំពេញ',
                          textAlign: pw.TextAlign.center,
                          style: pw.TextStyle(
                            font: fontKhmerMoul,
                            fontSize: 14,
                            color: PdfColors.white
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    // Save the PDF document and return its bytes
    return pdf.save();
  }

  static pw.Widget khmerTextPdf(String label, String value,pw.Font fontBattambong) {
    return pw.RichText(
      text: pw.TextSpan(
        text: '$label : ',
        style: pw.TextStyle(
          fontWeight: pw.FontWeight.bold,
          fontSize: 12,
          font: fontBattambong,
          color: PdfColors.indigo
        ),
        children: [
          pw.TextSpan(
            text: value,
            style: pw.TextStyle(
              fontWeight: pw.FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }

  static pw.Widget englishTextPdf(String label, String value) {
    return pw.RichText(
      text: pw.TextSpan(
        text: '$label : ',
        style: const pw.TextStyle(
          fontSize: 10,
          color: PdfColors.indigo
        ),
        children: [
          pw.TextSpan(
            text: value,
            style: pw.TextStyle(
              fontWeight: pw.FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }

  static pw.Widget contactInfoRowPdf(Uint8List image, String text, bool isPhone, {Uint8List? image2}) {
    return pw.Row(
      children: [
        pw.Image(pw.MemoryImage(image), height: 15,),
        pw.SizedBox(width: 5),
        if (isPhone) ...[
          pw.Image(pw.MemoryImage(image2!), height: 15,),
        ],
        pw.Text(
          text,
          style: const pw.TextStyle(
            color: PdfColors.indigo,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}