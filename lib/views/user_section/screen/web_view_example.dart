import 'dart:io' as io;
import 'dart:html';
import 'dart:ui_web';

import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class WebViewExample extends StatefulWidget {
  const WebViewExample({super.key});

  @override
  _WebViewExampleState createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample> {
  late final WebViewController controller;
  var homeController=Get.find<HomeController>();
  final PlatformViewRegistry platformViewRegistry = PlatformViewRegistry();

  @override
  void initState() {
    super.initState();

    if (kIsWeb) {
      platformViewRegistry.registerViewFactory(
        'iframeElement',(int viewId) => IFrameElement()
          // ..src = 'http://localhost:1110/api/v1/exports/list-users/excel?user_id=1'
          // ..src = 'http://localhost:1110/api/v1/exports/main-templates'
          ..src = '${AppConstants.baseUrl}/api/v1/exports/main-templates'
          ..style.border = 'none',
      );
    } else {
      if (io.Platform.isAndroid) {
        WebViewPlatform.instance = AndroidWebViewPlatform();
      } else if (io.Platform.isIOS) {
        WebViewPlatform.instance = WebKitWebViewPlatform();
      }

      controller = WebViewController()
        ..setJavaScriptMode(JavaScriptMode.unrestricted)
        // ..loadRequest(Uri.parse('http://localhost:1110/api/v1/exports/list-users/excel?user_id=1'));
        ..loadRequest(Uri.parse('${AppConstants.baseUrl}/api/v1/exports/main-templates'));

    }
  }

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      return const Scaffold(
        body: Center(
          child: HtmlElementView(
            viewType: 'iframeElement',
          ),
        ),
      );
    }

    // Mobile platform behavior
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Flutter WebView Example'),
      // ),
      body: WebViewWidget(controller: controller),
    );
  }
}