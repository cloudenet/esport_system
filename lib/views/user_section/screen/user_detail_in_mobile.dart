import 'package:cached_network_image/cached_network_image.dart';
import 'package:esport_system/data/model/list_user_model.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/detail_container.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserDetailInMobile extends StatelessWidget {
  final ListUserModel list; // Make list a final property

  const UserDetailInMobile({Key? key, required this.list}) : super(key: key); // Modify constructor to accept ListUserModel instance

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryColor,
      appBar: AppBar(
        title: Text("User Detail".tr),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: SizedBox(
                    height: 90,
                    child: list.photo != "null"
                        ? ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: CachedNetworkImage(
                        height: 90,
                        width: 90,
                        fit: BoxFit.cover,
                        imageUrl:
                        "${AppConstants.baseUrl}${list.photo}",
                      ),
                    )
                        : SizedBox(
                      height: 90,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Image.asset(
                          "assets/images/no-image.png",
                          fit: BoxFit.cover,
                          width: 90,
                          height: 90,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("${list.name}",
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge
                            ?.copyWith(
                            fontSize: 18, fontWeight: FontWeight.bold)),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      "${list.role}",
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Container(
                      width: 100,
                      height: 30,
                      decoration: BoxDecoration(
                          color: bgColor,
                          borderRadius: BorderRadius.circular(5)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: 10,
                            height: 10,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: list.userStatus == 'active'.tr
                                  ? Colors.green
                                  : Colors.red,
                            ),
                          ),
                          Text("${list.userStatus}")
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
            const SizedBox(
              height: 15,
            ),
            const Divider(
              height: 3,
            ),
            const SizedBox(
              height: 15,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "User Information".tr,
                  style: Theme.of(context)
                      .textTheme
                      .headlineLarge
                      ?.copyWith(fontSize: 18),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        child: buildContainer(
                            title: "Username:".tr,
                            data: "${list.username}",
                            icon: Icons.person,
                            context: context)),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: buildContainer(
                            title: "Nationality:".tr,
                            data: "${list.nationality}",
                            icon: Icons.flag,
                            context: context)),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        child: buildContainer(
                            title: "Phone:".tr,
                            data: "${list.phone}",
                            icon: Icons.phone,
                            context: context)),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: buildContainer(
                            title: "Card Number:".tr,
                            data: "${list.cardNumber}",
                            icon: Icons.credit_card_outlined,
                            context: context))
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        child: buildContainer(
                            title: "Identity No:".tr,
                            data: "${list.identityNo}",
                            icon: Icons.credit_card_outlined,
                            context: context)),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: buildContainer(
                            title: "Email:".tr,
                            data: "${list.email}",
                            icon: Icons.mail,
                            context: context))
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                        child: buildContainer(
                            title: "Gender:".tr,
                            data: "${list.gender}",
                            icon: Icons.male_sharp,
                            context: context)),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: buildContainer(
                          title: "DOB:".tr,
                          data: "${list.dob}",
                          icon: Icons.cake_sharp,
                          context: context),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                buildContainer(
                    title: "Address".tr,
                    data: "${list.address}",
                    icon: Icons.location_on,
                    context: context),

              ],
            )
          ],
        ),
      ),
    );
  }
}
