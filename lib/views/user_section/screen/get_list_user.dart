import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/util/text_style.dart';
import 'package:esport_system/views/user_section/screen/create_user.dart';
import 'package:esport_system/views/user_section/screen/detail_user_screen.dart';
import 'package:esport_system/views/user_section/screen/update_user_screen.dart';
import 'package:esport_system/views/user_section/screen/user_assign_control.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../helper/empty_data.dart';
import '../../../helper/images.dart';

class GetUserList extends StatefulWidget {
  const GetUserList({super.key});

  @override
  State<GetUserList> createState() => _GetUserListState();
}

class _GetUserListState extends State<GetUserList> {
  var userController = Get.find<UserController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    listUser();
    //userController.getUserDetail();
    super.initState();
  }

  void listUser()async{
    await userController.getListUser();
    userController.listUser.sort((a,b) => b.id!.compareTo(a.id!));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<UserController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25)
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Container(
            color: bgColor,
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: secondaryColor,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: DropdownButton<int>(
                    value: logic.limit,
                    items: [6, 12, 18, 24].map((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(
                          '$value',
                          style: const TextStyle(color: Colors.white),
                        ),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      if (newValue != null) {
                        logic.setLimit(newValue);
                      }
                    },
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                    dropdownColor: secondaryColor,
                    underline: Container(),
                    elevation: 8,
                    borderRadius: BorderRadius.circular(8),
                    icon: const Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    iconEnabledColor: Colors.grey,
                    iconDisabledColor: Colors.grey[400],
                    isDense: true,
                    menuMaxHeight: 200,
                    alignment: Alignment.center,
                  ),
                ),
                const Spacer(),
                Expanded(
                  child: searchField(onChange: (value) {
                    logic.search(value);
                  }, hintText: 'search_user_name'.tr),
                ),
              ],
            ),
          ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/user.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'users'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Row(
                      children: [
                        // DownloadButton(
                        //   btnName: 'PDF',
                        //   onPress: () {
                        //     print('Download button pressed!');
                        //   },
                        //   colors: Colors.red,
                        //   textColor: Colors.white,
                        //   icons: Icons.picture_as_pdf,
                        //   iconColor: Colors.white,
                        // ),
                        // const SizedBox(
                        //   width: 10,
                        // ),
                        // DownloadButton(
                        //   btnName: 'Excel',
                        //   onPress: () {
                        //     print('Download button pressed!');
                        //   },
                        //   colors: Colors.green,
                        //   textColor: Colors.white,
                        //   icons: Icons.expand_circle_down,
                        //   iconColor: Colors.white,
                        // ),
                        // const SizedBox(
                        //   width: 10,
                        // ),
                        AddNewButton(
                          btnName: 'add_new'.tr,
                          onPress: () {
                            //homeController.updateWidgetHome(const CreateUpdateUserScreen(updateScreen: false,));
                            homeController.updateWidgetHome(const CreateUser(updateScreen: false,));
                          },
                        )
                      ],
                    ),
                  ),

                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : LayoutBuilder(
                            builder: (context, constraints) {
                              return SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  children: [
                                    SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minWidth: constraints.maxWidth,
                                        ),
                                        child: DataTable(
                                          dataRowMaxHeight: Get.height * 0.1,
                                          headingRowColor: WidgetStateColor.resolveWith(
                                          (states) => secondaryColor),
                                          columns: [
                                            DataColumn(
                                              label: Text(
                                                "profile".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "name".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "khmer_name".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "phone".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "gender".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "assign_control".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "actions".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                          rows: logic.listUser.isEmpty
                                          ?[]
                                          :List<DataRow>.generate(
                                            logic.listUser.length,
                                                (index) {
                                              var list = logic.listUser[index];
                                              print("vvvvUser${AppConstants.baseUrl}${list.photo}");
                                              return DataRow(
                                                cells: [
                                                  DataCell(
                                                    SizedBox(
                                                      height: 50,
                                                      child: list.photo != "null" ?
                                                      ClipOval(
                                                        child: Image.network(
                                                          "${AppConstants.baseUrl}${list.photo}",
                                                          width: 50,
                                                          height: 50,
                                                          fit: BoxFit.cover,
                                                          errorBuilder: (context, error, stackTrace) {
                                                            return ClipOval(
                                                              child: Image.asset(
                                                                Images.noImage,
                                                                height: 50,
                                                                width: 50,
                                                                fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                                              ),
                                                            );
                                                          },
                                                          loadingBuilder: (context, child, loadingProgress) {
                                                            if (loadingProgress == null) {
                                                              return child;
                                                            } else {
                                                              return Center(
                                                                child:
                                                                CircularProgressIndicator(
                                                                  value: loadingProgress.expectedTotalBytes != null ?
                                                                  loadingProgress.cumulativeBytesLoaded / (loadingProgress.expectedTotalBytes ?? 1)
                                                                      : null,
                                                                ),
                                                              );
                                                            }
                                                          },
                                                        ),
                                                      ) :
                                                      Container(
                                                        width: 50,
                                                        height: 50,
                                                        decoration:
                                                        BoxDecoration(
                                                            border: Border.all(
                                                              color: primaryColor,
                                                              width: 2,
                                                            ),
                                                            shape: BoxShape.circle),
                                                        child: const Icon(Icons.image_not_supported),
                                                      ),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${list.name}', 20),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      _truncateText(
                                                          '${list.nameKhmer! != "null" ? list.nameKhmer : "N/A"}', 20) ,style: TextStylesHelper.khmerText16.copyWith(fontSize: 14),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      '${list.phone}',
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      capitalizeFirstLetter(list.gender.toString()),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                        list.controlOf != "null"
                                                            ? capitalizeFirstLetter(list.level.toString())
                                                            : "not_yet".tr
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Row(
                                                      children: [
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome( DetailUserScreen(data: list));
                                                          },
                                                          child: Container(
                                                            padding: const EdgeInsets.all(6),
                                                            decoration: BoxDecoration(
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: Colors.amberAccent
                                                              ),
                                                              color: Colors.amberAccent.withOpacity(0.1),
                                                              borderRadius: BorderRadius.circular(6),
                                                            ),
                                                            child: const Icon(
                                                              Icons.visibility,
                                                              size: 20,
                                                              color: Colors.amberAccent,
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(width: 12),
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(UpdateUserScreen(userModel: list,));
                                                          },
                                                          child: Container(
                                                            padding: const EdgeInsets.all(6),
                                                            decoration:
                                                            BoxDecoration(
                                                              border: Border.all(
                                                                  width: 1,
                                                                  color: Colors.green
                                                              ),
                                                              color: Colors.green.withOpacity(0.1),
                                                              borderRadius: BorderRadius.circular(6),
                                                            ),
                                                            child: const Icon(
                                                              Icons.edit,
                                                              size: 20,
                                                              color: Colors.green,
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(width: 12),
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(UserAssignControl(userModel: list,));
                                                          },
                                                          child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                border: Border.all(
                                                                    width: 1,
                                                                    color: Colors.blue
                                                                ),
                                                                color: Colors.blue.withOpacity(0.1),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: Image.asset("assets/icons/control.png",
                                                                width: 20,
                                                                height: 20,
                                                                color: Colors.blue,
                                                              )),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            showDialog(
                                                                context: context,
                                                                builder: (BuildContext context) {
                                                                  return AlertDialog(
                                                                    content:Container(
                                                                      width:Get.width * 0.3,
                                                                      height:Get.height * 0.3,
                                                                      padding:const EdgeInsets.all(16),
                                                                      child: Column(
                                                                        children: [
                                                                          Align(alignment:Alignment.topCenter,
                                                                            child: Image.asset(
                                                                              "assets/images/warning.png",
                                                                              width:50,
                                                                              height:50,
                                                                            ),
                                                                          ),
                                                                          const SizedBox(height:15),
                                                                          Text(
                                                                              "are_you_sure_you_want_to_delete_this_user?".tr,
                                                                              style: Theme.of(context).textTheme.bodyMedium
                                                                          ),
                                                                          const SizedBox(height:15),
                                                                          const Spacer(),
                                                                          Row(
                                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                                            mainAxisAlignment: MainAxisAlignment.end,
                                                                            children: [
                                                                              CancelButton(
                                                                                  btnName: 'cancel'.tr,
                                                                                  onPress: (){
                                                                                    Get.back();
                                                                                  }),
                                                                              const SizedBox(width: 16),
                                                                              OKButton(
                                                                                  btnName: 'save'.tr,
                                                                                  onPress: () async {
                                                                                    EasyLoading.show( status: 'loading...'.tr, dismissOnTap: true,);
                                                                                    var result = await logic.deleteUser(userId: list.id.toString());
                                                                                    if (result['success']) {
                                                                                      EasyLoading.showSuccess('user_delete_successfully'.tr,
                                                                                        dismissOnTap: true,
                                                                                        duration: const Duration(seconds: 2),
                                                                                      );
                                                                                      logic.getListUser();
                                                                                      Get.back();
                                                                                    } else {
                                                                                      EasyLoading.showError(result['message'], duration: const Duration(seconds: 2),
                                                                                      );
                                                                                      Get.back();
                                                                                    }
                                                                                    EasyLoading.dismiss();
                                                                                  }
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  );
                                                                });
                                                          },
                                                          child: Padding(
                                                            padding: const EdgeInsets.all(10.0),
                                                            child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.red.withOpacity(0.1),
                                                                border: Border.all(
                                                                    width: 1,
                                                                    color: Colors.red
                                                                ),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(
                                                                Icons.delete,
                                                                size: 20,
                                                                color: Colors.red,
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (logic.listUser.isEmpty)...[
                                      const SizedBox(height: 50,),
                                      const Center(child: EmptyData()),
                                    ]
                                  ],
                                ),
                              );
                            },
                          )

              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: logic.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= logic.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () => logic.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color: logic.currentPage == i
                                                ? Colors.blue
                                                : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: logic.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
  String capitalizeFirstLetter(String value) {
    return value.isNotEmpty ? '${value[0].toUpperCase()}${value.substring(1).toLowerCase()}' : value;
  }

}
