import 'package:esport_system/data/controllers/league_controller.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/league_section/league_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import '../../data/controllers/home_controller.dart';
import '../../helper/add_new_button.dart';
import '../../helper/search_field.dart';
import 'create_league.dart';
import '../../../helper/constants.dart';

class ListLeague extends StatefulWidget {
  const ListLeague({super.key});

  @override
  State<ListLeague> createState() => _ListLeagueState();
}

class _ListLeagueState extends State<ListLeague> {
  // const defaultPadding = 16.0;
  final LeagueController logic = Get.find<LeagueController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    logic.getLeague();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: GetBuilder<LeagueController>(
        builder: (logic) {
          return Container(
            margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
            child: Column(
              children: [
              Container(
              color: bgColor,
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: secondaryColor,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: DropdownButton<int>(
                      value: logic.limit,
                      items: [6, 12, 18, 24].map((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: Text(
                            '$value',
                            style: const TextStyle(color: Colors.white),
                          ),
                        );
                      }).toList(),
                      onChanged: (newValue) {
                        if (newValue != null) {
                          logic.setLimit(newValue);
                        }
                      },
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                      ),
                      dropdownColor: secondaryColor,
                      underline: Container(),
                      elevation: 8,
                      borderRadius: BorderRadius.circular(8),
                      icon: const Icon(Icons.arrow_drop_down),
                      iconSize: 24,
                      iconEnabledColor: Colors.grey,
                      iconDisabledColor: Colors.grey[400],
                      isDense: true,
                      menuMaxHeight: 200,
                      alignment: Alignment.center,
                    ),
                  ),
                  const Spacer(),
                  Expanded(
                    child: searchField(onChange: (value) {
                      logic.search(value);
                    }, hintText: 'search_league_name'.tr),
                  ),
                ],
              ),
            ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 32,
                            height: 32,
                            child: Image.asset(
                              "assets/icons/league.png",
                              color: Colors.white,
                            ),
                          ),
                          const SizedBox(width: 8), // Add spacing between the icon and text
                          Text(
                            'league'.tr,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 32,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Align(
                        alignment: Alignment.topRight,
                        child: AddNewButton(btnName: 'add_new'.tr,onPress: (){
                          homeController.updateWidgetHome(const CreateUpdateLeague());
                        },)
                    ),
                  ],
                ),
                const SizedBox(height: 22,),
                Expanded(
                  child: logic.isLoading
                      ? const Center(child: CircularProgressIndicator())
                  : LayoutBuilder(
                    builder: (context, constraints) {
                      return SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          children: [
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: ConstrainedBox(
                                constraints: BoxConstraints(
                                  minWidth: constraints.maxWidth,
                                ),
                                child: DataTable(
                                  dataRowMaxHeight: Get.height * 0.1,
                                  headingRowColor:
                                  WidgetStateColor.resolveWith(
                                          (states) =>
                                         secondaryColor),
                                  columns: [
                                    DataColumn(
                                      label: Text(
                                        "no.".tr,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        "league_name".tr,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),

                                    ),
                                    DataColumn(
                                      label: Text(
                                        "address".tr,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        "created_date".tr,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    DataColumn(
                                      label: Text(
                                        "actions".tr,
                                        style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                  rows: logic.listLeague.isEmpty
                                    ?[]
                                    :List<DataRow>.generate(
                                    logic.listLeague.length,
                                        (index) {
                                      var data = logic.listLeague[index];
                                      return DataRow(
                                        cells: [
                                          DataCell(
                                            Text(_truncateText('${index + 1}', 10)),
                                          ),
                                          DataCell(
                                            Text(
                                              _truncateText(
                                                  '${data.leagueName}', 50),

                                            ),
                                          ),

                                          DataCell(
                                            Text(
                                              _truncateText(
                                                  '${data.address}', 20),

                                            ),
                                          ),  DataCell(
                                            Text(
                                              _truncateText(formatDate(data.foundedDate.toString()),
                                                  20),

                                            ),
                                          ),

                                          DataCell(
                                            Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    homeController.updateWidgetHome(LeagueDetail(data: data));
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                        color: Colors.amberAccent.withOpacity(0.1),
                                                        borderRadius: BorderRadius.circular(6),
                                                        border: Border.all(width: 1,color: Colors.amberAccent)
                                                    ),
                                                    child: const Icon(
                                                      Icons.visibility,
                                                      size: 20,
                                                      color: Colors.amberAccent,
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(width: 12,),
                                                GestureDetector(
                                                  onTap: () {
                                                    homeController.updateWidgetHome(CreateUpdateLeague(data: data,));
                                                  },
                                                  child: Container(
                                                      padding: const EdgeInsets.all(6),
                                                      decoration:
                                                      BoxDecoration(
                                                          color: Colors.green.withOpacity(0.1),
                                                          borderRadius: BorderRadius.circular(6),
                                                          border: Border.all(width: 1,color: Colors.green)
                                                      ),
                                                      child: const Icon(
                                                        Icons.edit,
                                                        size: 20,
                                                        color: Colors.green,
                                                      )),
                                                ),
                                                const SizedBox(width: 12),
                                                GestureDetector(
                                                  onTap: () {
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                      context) {
                                                        return AlertDialog(
                                                          content:Container(
                                                            width:Get.width * 0.3,
                                                            height:Get.height * 0.3,
                                                            padding:const EdgeInsets.all(16),
                                                            child: Column(
                                                              children: [
                                                                Align(alignment:Alignment.topCenter,
                                                                  child: Image.asset("assets/images/warning.png", width:50, height:50,),
                                                                ),
                                                                const SizedBox(height:15),
                                                                Text("are_you_sure_you_want_to_delete_this_league?".tr,
                                                                    style: Theme.of(context).textTheme.bodyMedium
                                                                ),
                                                                const SizedBox(height:15),
                                                                const Spacer(),
                                                                Row(
                                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: [
                                                                    CancelButton(btnName: 'cancel'.tr, onPress: (){
                                                                      Get.back();
                                                                    }),
                                                                    const SizedBox(width: 16),
                                                                    OKButton(
                                                                      btnName: 'confirm'.tr,
                                                                      onPress: ()async{
                                                                        logic.isLoading = true;
                                                                        EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,);
                                                                        var result = await logic.deleteLeague(leagueID: data.id.toString());
                                                                        if (result['success']) {
                                                                          EasyLoading.showSuccess('league_deleted_successfully'.tr, dismissOnTap: true,
                                                                          duration: const Duration(seconds: 2),);
                                                                          Get.back();
                                                                          logic.listLeague.removeAt(index);
                                                                        } else {
                                                                          EasyLoading.showError(result['message'], duration: const Duration(seconds: 2),);
                                                                          Get.back();
                                                                        }
                                                                        EasyLoading.dismiss();
                                                                        logic.isLoading = false;
                                                                      },
                                                                    )
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  },
                                                  child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                        color: Colors.red.withOpacity(0.1),
                                                        borderRadius: BorderRadius.circular(6),
                                                        border: Border.all(width: 1,color: Colors.red)
                                                    ),
                                                    child: const Icon(
                                                      Icons.delete,
                                                      size: 20,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),

                                              ],
                                            ),
                                          ),
                                        ],
                                      );
                                    },
                                  ),
                                ),
                              ),
                            ),
                            if (logic.listLeague.isEmpty)...[
                              const SizedBox(height: 50,),
                              const Center(child: EmptyData()),
                            ]
                          ],
                        ),
                      );
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      width: 700,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          IconButton(
                            onPressed: logic.previousPage,
                            icon: const Icon(Icons.arrow_back_ios),
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                for (int i = 1; i <= logic.totalPages; i++)
                                  Padding(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        TextButton(
                                          onPressed: () => logic.setPage(i),
                                          child: Text(
                                            i.toString(),
                                            style: TextStyle(
                                              color: logic.currentPage == i
                                                  ? Colors.blue
                                                  : Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: logic.nextPage,
                            icon: const Icon(Icons.arrow_forward_ios),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}