import 'package:esport_system/data/controllers/league_controller.dart';
import 'package:esport_system/data/model/address_model.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../data/controllers/home_controller.dart';
import '../../data/model/league_model.dart';
import '../../helper/add_new_button.dart';
import '../../helper/constants.dart';
import '../../helper/custom_textformfield.dart';
import 'list_league.dart';

class CreateUpdateLeague extends StatefulWidget {
  final LeagueModel? data;
  const CreateUpdateLeague({Key? key,this.data}) : super(key: key);

  @override
  State<CreateUpdateLeague> createState() => _CreateUpdateLeagueState();
}

class _CreateUpdateLeagueState extends State<CreateUpdateLeague> {
  final LeagueController logic = Get.find<LeagueController>();
  final _formKey = GlobalKey<FormBuilderState>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    logic.fetchProvinces();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(widget.data == null){
      logic.clearTextField();
    }
    if (widget.data!=null) {
      logic.leagueNameController.text = widget.data!.leagueName.toString();

      logic.selectProvince(Address(
        code: widget.data!.provinceCode,
        name: widget.data!.province,
      ));
      logic.selectDistrict(Address(
        code: widget.data!.districtCode,
        name: widget.data!.district,
      ));
      logic.selectCommune(Address(
        code: widget.data!.communeCode,
        name: widget.data!.commune,
      ));
      logic.selectVillage(Address(
        code: widget.data!.villageCode,
        name: widget.data!.village,
      ));

    }

    return Scaffold(
      backgroundColor: secondaryColor,
      body: GetBuilder<LeagueController>(
        builder: (logic) {
          // Reset address string before appending
          String address = '';
          if (logic.selectedProvince != null) {
            address += '${logic.selectedProvince?.name}, ';
          }
          if (logic.selectedDistrict != null) {
            address += '${logic.selectedDistrict?.name}, ';
          }
          if (logic.selectedCommune != null) {
            address += '${logic.selectedCommune?.name}, ';
          }
          if (logic.selectedVillage != null) {
            address += '${logic.selectedVillage?.name}';
          }
          return Container(
             width: Get.width,
             height: Get.height,
            decoration: const BoxDecoration(
              color: bgColor,
            ),
            child: Padding(
              padding: const EdgeInsets.all(30),
              child: FormBuilder(
                key: _formKey,
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    const SizedBox(height: 20),
                    Text(
                     widget.data!=null?'update_league'.tr: 'create_league'.tr,
                      style: Theme.of(context).textTheme.headlineSmall,
                    ),
                    const SizedBox(height: 10),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('title'.tr,
                                  style: const TextStyle(fontSize: 16)),
                              CustomFormBuilderTextField(
                                errorText: 'enter_league_name'.tr,
                                name: 'league_name'.tr,
                                controller: logic.leagueNameController,
                                hintText: 'enter_league_name'.tr,
                                icon: Icons.location_on,
                                fillColor: secondaryColor,
                              ),
                            ],
                          ),
                        ),

                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('province'.tr,
                                  style: const TextStyle(fontSize: 16)),
                              _buildLocationSelector(
                                context,
                                'province'.tr,
                                logic.provinces,
                               logic.selectedProvince?.name ??
                                    'select_province'.tr,
                                logic.selectProvince,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        // Spacing between the two fields
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('district'.tr,
                                  style: const TextStyle(fontSize: 16)),
                              _buildLocationSelector(
                                context,
                                'district'.tr,
                                logic.districts,
                                logic.selectedDistrict?.name ??
                                    'select_district'.tr,
                                logic.selectDistrict,
                                enabled: logic.selectedProvince != null,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('commune'.tr,
                                  style: const TextStyle(fontSize: 16)),
                              _buildLocationSelector(
                                context,
                                'commune'.tr,
                                logic.communes,
                                logic.selectedCommune?.name ?? 'select_commune'.tr,
                                logic.selectCommune,
                                enabled: logic.selectedDistrict != null,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(width: 10),
                        // Spacing between the two fields
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('village'.tr,
                                  style: const TextStyle(fontSize: 16)),
                              _buildLocationSelector(
                                context, 'village'.tr,
                                logic.villages,
                                logic.selectedVillage?.name ?? 'select_village'.tr,
                                logic.selectVillage,
                                enabled: logic.selectedCommune != null,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('address'.tr,
                                  style: const TextStyle(fontSize: 16)),
                              FormBuilderTextField(
                                name: 'address'.tr,
                                controller:
                                    TextEditingController(text: address),
                                readOnly: true,
                                decoration: InputDecoration(
                                  prefixIcon: const Icon(Icons.location_on),
                                  hintText: 'enter_address'.tr,
                                  fillColor: secondaryColor,
                                  filled: true,
                                  border: const OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CancelButton(
                              btnName: 'close'.tr,
                              onPress: () {
                                logic.clearTextField();
                                homeController.updateWidgetHome(const ListLeague());
                              }),
                          const SizedBox(
                            width: 12,
                          ),
                          OKButton(
                            btnName:widget.data!=null?'update'.tr: 'create'.tr,
                            onPress: () async {
                              if (_formKey.currentState != null &&
                                  _formKey.currentState!.saveAndValidate()) {
                                EasyLoading.show(
                                    status: 'loading...'.tr, dismissOnTap: true);
                                try {
                                  String leagueName = logic.leagueNameController.text;
                                  String? provinceCode = logic.selectedProvince?.code;
                                  String? districtCode = logic.selectedDistrict?.code;
                                  String? communeCode = logic.selectedCommune?.code;
                                  String? villageCode = logic.selectedVillage?.code;
                                  String? province = logic.selectedProvince?.name;
                                  String? district = logic.selectedDistrict?.name;
                                  String? commune = logic.selectedCommune?.name;
                                  String? village = logic.selectedVillage?.name;
                                  String foundDate =
                                  DateFormat('yyyy-MM-dd')
                                      .format(DateTime.now());

                                  if ([provinceCode, districtCode, communeCode, villageCode, province, district, commune, village].contains(null)) {
                                    EasyLoading.showError('please_select_all_administrative_divisions'.tr);
                                    return;
                                  }

                                  if (widget.data != null) {
                                    await logic.updateLeague(
                                      id: widget.data!.id!,
                                      leagueName: leagueName,
                                      createDate: foundDate,
                                      provinceCode: provinceCode!,
                                      districtCode: districtCode!,
                                      villageCode: villageCode!,
                                      communeCode: communeCode!,
                                      province: province!,
                                      district: district!,
                                      commune: commune!,
                                      village: village!,
                                      address: address,
                                    );
                                    logic.clearTextField();
                                    EasyLoading.showSuccess('league_updated_successfully!'.tr);
                                  } else {
                                    await logic.createLeague(
                                      leagueName: leagueName,
                                      createDate: foundDate,
                                      provinceCode: provinceCode!,
                                      districtCode: districtCode!,
                                      villageCode: villageCode!,
                                      communeCode: communeCode!,
                                      province: province!,
                                      district: district!,
                                      commune: commune!,
                                      village: village!,
                                      address: address,
                                    );
                                    logic.clearTextField();
                                    EasyLoading.showSuccess('league_created_successfully!'.tr);
                                  }

                                  homeController.updateWidgetHome(const ListLeague());
                                } catch (e) {
                                  EasyLoading.showError('an_error_occurred: $e'.tr);
                                } finally {
                                  EasyLoading.dismiss();
                                }
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  Widget _buildLocationSelector(BuildContext context, String label,
      List<Address> data, String currentValue, Function(Address) onSelect,
      {bool enabled = true}) {
    return FormBuilderTextField(
      name: label,
      readOnly: true,
      onTap: enabled
          ? () => _showSelectionDialog(
              context: context, title: label, data: data, onSelect: onSelect)
          : null,
      controller: TextEditingController(text: currentValue),
      decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        filled: true,
        fillColor: secondaryColor,
        hintText: label,
        suffixIcon: const Icon(Icons.arrow_drop_down),
      ),
      validator: enabled ? FormBuilderValidators.required() : null,
    );
  }

  void _showSelectionDialog({
    required BuildContext context,
    required String title,
    required List<Address> data,
    required Function(Address) onSelect,
  }) {
    final double dialogWidth =
        kIsWeb ? MediaQuery.of(context).size.width * 0.5 : double.maxFinite;

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: bgColor,
          title: Text(title),
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          content: SizedBox(
            width: dialogWidth,
            child: ListView(
              children: data.map((item) {
                return ListTile(
                  title: Text(item.name!),
                  subtitle: Text(item.description!),
                  onTap: () {
                    onSelect(item);
                    Navigator.pop(context);
                  },
                );
              }).toList(),
            ),
          ),
        );
      },
    );
  }
}
