import 'package:esport_system/data/model/league_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/league_section/list_league.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class LeagueDetail extends StatefulWidget {
  final LeagueModel data;
  const LeagueDetail({super.key, required this.data});

  @override
  State<LeagueDetail> createState() => _LeagueDetailState();
}

class _LeagueDetailState extends State<LeagueDetail> {
  var homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
        mobile: bodyDetailMobile(),
        tablet: bodyDetailMobile(),
        desktop: bodyDetailDesktop(),
      ),
    );
  }

  Widget bodyDetailDesktop() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.logo!.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(  // This allows the Column to take up available space and wrap if necessary
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                      children: [
                        Text(
                          widget.data.leagueName != "null" ? widget.data.leagueName.toString() : "N/A",
                          style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,  // Allows the text to wrap to two lines
                          overflow: TextOverflow.ellipsis,  // Shows ellipsis when text overflows
                        ),
                        const SizedBox(height: 4.0),  // Space between name and stadium
                        Text(
                          widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey[600],  // Stadium text style
                          ),
                          overflow: TextOverflow.ellipsis,  // Safeguard against long stadium names
                        )

                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("league_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "league_name".tr,
                            textData: widget.data.leagueName != "null" ? widget.data.leagueName.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "founded_date".tr,
                            textData: widget.data.foundedDate != "null" ? formatDate(widget.data.foundedDate.toString()) : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("location_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "stadium".tr,
                      textData: widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "province".tr,
                            textData: widget.data.province != "null" ? widget.data.province.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "district".tr,
                            textData: widget.data.district != "null" ? widget.data.district.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "commune".tr,
                            textData: widget.data.commune != "null" ? widget.data.commune.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "village".tr,
                            textData: widget.data.village != "null" ? widget.data.village.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.address!= "null" ? widget.data.address.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("performance".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "championships_won".tr,
                      textData: widget.data.championshipsWon != "null" ? widget.data.championshipsWon.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const ListLeague());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
  Widget bodyDetailMobile() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.logo!.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Expanded(  // This allows the Column to take up available space and wrap if necessary
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                      children: [
                        Text(
                          widget.data.leagueName != "null" ? widget.data.leagueName.toString() : "N/A",
                          style: const TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 2,  // Allows the text to wrap to two lines
                          overflow: TextOverflow.ellipsis,  // Shows ellipsis when text overflows
                        ),
                        const SizedBox(height: 4.0),  // Space between name and stadium
                        Text(
                          widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.grey[600],  // Stadium text style
                          ),
                          overflow: TextOverflow.ellipsis,  // Safeguard against long stadium names
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("league_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "league_name".tr,
                            textData: widget.data.leagueName != "null" ? widget.data.leagueName.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "founded_date".tr,
                            textData: widget.data.foundedDate != "null" ? formatDate(widget.data.foundedDate.toString()) : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("location_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "stadium".tr,
                      textData: widget.data.stadium != "null" ? widget.data.stadium.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "province".tr,
                      textData: widget.data.province != "null" ? widget.data.province.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "district".tr,
                      textData: widget.data.district != "null" ? widget.data.district.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "commune".tr,
                      textData: widget.data.commune != "null" ? widget.data.commune.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "village".tr,
                      textData: widget.data.village != "null" ? widget.data.village.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.address != "null" ? widget.data.address.toString() : "N/A",
                      height: 80,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("performance".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "championships_won".tr,
                      textData: widget.data.championshipsWon != "null" ? widget.data.championshipsWon.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const ListLeague());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }


}

