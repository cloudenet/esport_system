import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/model/recent_files.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class EventOnDashboard extends StatefulWidget {
  const EventOnDashboard({
    Key? key,
  }) : super(key: key);

  @override
  State<EventOnDashboard> createState() => _EventOnDashboardState();
}

class _EventOnDashboardState extends State<EventOnDashboard> {
  var eventController = Get.find<EventController>();

  @override
  void initState() {
    eventController.getEvent();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "events".tr,
            style: Theme.of(context).textTheme.titleMedium,
          ),
          GetBuilder<EventController>(builder: (logic) {
            if (logic.isLoading) {
              return const Center(child: CircularProgressIndicator());
            } else if (logic.eventData.isEmpty) {
              return const EmptyData();
            } else {
              return SizedBox(
                width: double.infinity,
                child: DataTable(
                  columnSpacing: defaultPadding,
                  headingTextStyle: const TextStyle(
                    color: Colors.white,
                  ),
                  columns:  [
                    DataColumn(
                      label: Text("event_name".tr),
                    ),
                    DataColumn(
                      label: Text("start_date".tr),
                    ),
                    DataColumn(
                      label: Text("end_date".tr),
                    ),
                  ],

                  rows: List<DataRow>.generate(logic.eventData.take(5).length, (index) {
                    var list = logic.eventData[index];
                    return DataRow(cells: [
                      DataCell(
                        Text(list.title!),
                      ),
                      DataCell(
                        Text(formatDate(list.startDate.toString()),),  // Ensure you use the correct data
                      ),
                      DataCell(
                        Text(formatDate(list.endDate.toString())),  // Ensure you use the correct data
                      ),
                    ]);
                  }),
                ),
              );
            }
          }),
        ],
      ),
    );
  }
}

DataRow recentFileDataRow(RecentFile fileInfo) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            SvgPicture.asset(
              fileInfo.icon!,
              height: 30,
              width: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Text(fileInfo.title!),
            ),
          ],
        ),
      ),
      DataCell(Text(fileInfo.date!)),
      DataCell(Text(fileInfo.size!)),
    ],
  );
}
