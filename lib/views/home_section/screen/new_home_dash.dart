import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/helper/hover_widget.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../data/controllers/home_controller.dart';
import '../../../data/controllers/language_controller.dart';
import '../../../helper/constants.dart';
import '../../../helper/responsive_helper.dart';


class HomePage extends StatelessWidget {


  final homeController = Get.find<HomeController>();
  final playerController = Get.find<PlayerController>();
  final LanguageController languageController = Get.find();

  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    Size s = MediaQuery.of(context).size;
    const mobileSize = 600;
    playerController.getCategoryPlayer();

    return GetBuilder<HomeController>(
        builder: (logic) {
          return Scaffold(
            key: logic.scaffoldKey,
            backgroundColor: secondaryColor,
            drawer: const MyDrawer(),
            body: Responsive(
                mobile: Expanded(
                  child: Container(child: logic.widgetHome ?? const Home()),
                ),
                tablet: Row(
                  children: [
                    const Expanded(
                        child: MyDrawer()
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(child: logic.widgetHome ?? const Home()),
                    ),
                  ],
                ),
                desktop: Row(
                  children: [
                    const Expanded(
                        child: MyDrawer()
                    ),
                    Expanded(
                      flex: 5,
                      child: Container(child: logic.widgetHome ?? const Home()),
                    ),
                  ],
                )
            ),
            bottomNavigationBar: s.width > mobileSize ? null :
            BottomNavigationBar(
              currentIndex: logic.indexBottomNavbar,
              onTap: (index) {
                logic.updateIndexBottomNavbar(index); // Update navigation and widget
              },
              items: logic.bottomNavItems.map((item) {
                return BottomNavigationBarItem(
                  icon: Image.asset(
                    item.svgMenu,
                    width: 24,
                    height: 24,
                    color: item.isSelected ? Colors.blue : Colors.grey, // Highlight selected item
                  ),
                  label: item.title.tr,
                );
              }).toList(),
              selectedItemColor: Colors.blue,
              unselectedItemColor: Colors.black,
            ),
          );
        });
  }
}
class MyDrawer extends StatefulWidget {
  const MyDrawer({super.key});

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  final HomeController homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: secondaryColor,
      child: Column(
        children: [
          SizedBox(
            child: DrawerHeader(
              child: Image.asset("assets/images/Boules.png"),
            ),
          ),
          Expanded(
            child: GetBuilder<HomeController>(
              builder: (logic) {
                return Container(
                  margin: const EdgeInsets.symmetric(horizontal: 8),
                  child: ListView.builder(
                    itemCount: logic.menuItems.length,
                    itemBuilder: (context, index) {
                      final menuItem = logic.menuItems[index];
                      final isSelected = menuItem.isSelected;
                      if (menuItem.subMenuItems != null &&
                          menuItem.subMenuItems!.isNotEmpty) {
                        return Container(
                          padding: const EdgeInsets.symmetric(horizontal: 0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: isSelected ? secondaryColor : Colors.transparent, // Apply borderRadius and background color here
                          ),
                          child: ExpansionTile(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)
                            ),
                            leading: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                color: isSelected ? secondaryColor : Colors.transparent, // Background color for selected icon
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ColorFiltered(
                                  colorFilter: const ColorFilter.mode(
                                    Colors.white,
                                    BlendMode.srcIn,
                                  ),
                                  child: Image.asset(menuItem.svgMenu, width: 20, height: 20,),
                                ),
                              ),
                            ),
                            title: Text(
                              menuItem.title.tr,
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: isSelected ? Colors.white : Colors.grey,
                                // Text color for selected item
                                fontSize: 16,
                              ),
                            ),
                            children: menuItem.subMenuItems!.map((subMenuItem) {
                              final isSubMenuSelected = subMenuItem.isSelected;
                              return HoverInSizebar(
                                child: Container(
                                  margin: const EdgeInsets.symmetric(horizontal: 16),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: isSubMenuSelected ? bgColor : Colors.transparent, // Apply borderRadius and background color here
                                  ),
                                  child: GestureDetector(
                                    onTap: () {
                                      logic.selectSubMenuItem(menuItem, subMenuItem); // Update selected state
                                      logic.updateWidgetHome(subMenuItem.widget);
                                    },
                                    child: ListTile(
                                      leading: Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              8),
                                          color: isSubMenuSelected
                                              ? secondaryColor
                                              : Colors
                                              .transparent, // Background color for selected submenu icon
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: ColorFiltered(
                                            colorFilter: const ColorFilter.mode(
                                              Colors.white,
                                              BlendMode.srcIn,
                                            ),
                                            child: Image.asset(
                                              subMenuItem.svgSubMenu, width: 20,
                                              height: 20,),
                                          ),
                                        ),
                                      ),
                                      title: Text(
                                        subMenuItem.title.tr,
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          color: isSubMenuSelected ? Colors.white : Colors.grey,
                                          // Text color for selected item
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        );
                      } else {
                        return HoverInSizebar(
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: isSelected ? bgColor : Colors.transparent, // Apply borderRadius and background color here
                            ),
                            child: GestureDetector(
                              onTap: () {
                                logic.selectMenuItem(menuItem); // Update selected state
                                logic.updateWidgetHome(menuItem.widget);
                              },
                              child: ListTile(
                                leading: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: isSelected ? secondaryColor : Colors.transparent, // Background color for selected submenu icon
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: ColorFiltered(
                                      colorFilter: const ColorFilter.mode(
                                        Colors.white,
                                        BlendMode.srcIn,
                                      ),
                                      child: Image.asset(menuItem.svgMenu, width: 20, height: 20,),
                                    ),
                                  ),
                                ),
                                title: Text(
                                  menuItem.title.tr,
                                  style: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: isSelected ? Colors.white : Colors.grey, // Text color for selected item
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}


