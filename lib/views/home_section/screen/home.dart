import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/event_on_dashboard.dart';
import 'package:esport_system/views/home_section/screen/my_file.dart';
import 'package:esport_system/views/home_section/screen/matches_on_dasboard.dart';
import 'package:esport_system/views/home_section/screen/starage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'header.dart';

const mobileSize = 600;
class Home extends StatefulWidget {
  const Home({super.key,});
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  MatchesController matchesController = Get.find<MatchesController>();
  HomeController homeController = Get.find<HomeController>();

  @override
  void initState() {
    matchesController.getMatches();
    homeController.fetchItemCounts();
    super.initState();
  }

  var list = [
    "https://scontent.fpnh12-1.fna.fbcdn.net/v/t1.6435-9/141616946_429725968373780_8902903990400508156_n.jpg?_nc_cat=107&ccb=1-7&_nc_sid=cc71e4&_nc_eui2=AeFdONHTZtZup8rJEMKVlR8dAZudc1VenS0Bm51zVV6dLTQ-_1Ke7-RBMKDHWW-WnZDurTCGwNat77rJLTp5rmJT&_nc_ohc=3viP9j973p8Q7kNvgEziX10&_nc_zt=23&_nc_ht=scontent.fpnh12-1.fna&_nc_gid=AQXOIEmt0weF0dpu6zjBvLL&oh=00_AYCLRab7_-9qKLya3O7zpbyGjKRy0g35fg4gOJxDvfOg_g&oe=673E82B9",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GetBuilder<HomeController>(builder: (logic) {
          return SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            primary: false,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Header(),
                if (Get.size.width <= mobileSize)
                CarouselSlider.builder(
                      options: CarouselOptions(
                        // height: 400,
                        aspectRatio: 16 / 9,
                        viewportFraction: 0.8,
                        initialPage: 0,
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: const Duration(seconds: 3),
                        autoPlayAnimationDuration: const Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        enlargeCenterPage: true,
                        enlargeFactor: 0.2,
                        scrollDirection: Axis.horizontal,
                      ),
                      itemCount: list.length,
                      itemBuilder: (BuildContext context, int itemIndex,
                          int pageViewIndex) =>
                          CachedNetworkImage(
                            imageUrl: list[itemIndex],
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(10)),
                                image: DecorationImage(image: imageProvider,fit: BoxFit.cover),
                              ),
                            ),
                          )),
                const SizedBox(height: defaultPadding),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 5,
                      child: Column(
                        children: [
                          const CountCardOnDashboard(),
                          const SizedBox(height: defaultPadding),
                          if (!Responsive.isMobile(context))
                            const EventOnDashboard(),
                            const SizedBox(height: defaultPadding),
                            const MatchesOnDashboard(),
                          if (Responsive.isMobile(context))
                            const SizedBox(height: defaultPadding),
                          if (Responsive.isMobile(context)) const CalendarDetails(),
                        ],
                      ),
                    ),
                    if (!Responsive.isMobile(context))
                      const SizedBox(width: defaultPadding),
                    // On Mobile means if the screen is less than 850 we don't want to show it
                    if (Responsive.isDesktop(context))
                      const Expanded(
                        flex: 2,
                        child: CalendarDetails(),
                      ),
                  ],
                )
              ],
            ),
          );
        }),
      ),
    );
  }

//
}
