import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/model/recent_files.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/responsive_helper.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../helper/images.dart';

class MatchesOnDashboard extends StatelessWidget {
  const MatchesOnDashboard({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Responsive(
        mobile: body(context,isMobile: true),
        tablet: body(context,isMobile: false),
        desktop: body(context,isMobile: false)
    );
  }
  Widget body(context,{required bool isMobile}){
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Matches".tr,
            style: Theme.of(context).textTheme.titleMedium,
          ),
          GetBuilder<MatchesController>(builder: (logic) {
            return logic.isLoading
                ? const Center(child: CircularProgressIndicator())
                : logic.matchesList.isEmpty
                ? const EmptyData()
                : SizedBox(
              width: double.infinity,
              child: DataTable(
                columnSpacing: defaultPadding,
                // minWidth: 600,
                // headingRowColor:,
                headingTextStyle: const TextStyle(color: Colors.white),
                columns:  [
                  DataColumn(
                    label: Text("versus".tr),
                  ),
                  DataColumn(
                    label: Text("event_name".tr),
                  ),
                  if(!isMobile)
                    DataColumn(
                      label: Text("start_date".tr),
                    ),
                  if(!isMobile)
                  DataColumn(
                    label: Text("status".tr),
                  ),
                ],
                rows: List<DataRow>.generate(logic.matchesList.length,
                        (index) {
                      var list = logic.matchesList[index];
                      print("vvvv${AppConstants.baseUrl}${list.homeTeam!.logo}");
                      return DataRow(cells: [
                        DataCell(
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ClipOval(
                                child: Image.network("${AppConstants.baseUrl}${list.homeTeam!.logo}",
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return ClipOval(
                                      child: Image.asset(
                                        Images.noImage,
                                        height: 40,
                                        width: 40,
                                        fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                      ),
                                    );
                                  },
                                  loadingBuilder: (context, child, loadingProgress) {
                                    if (loadingProgress == null) {
                                      return child;
                                    } else {
                                      return Center(
                                        child: CircularProgressIndicator(
                                          value: loadingProgress.expectedTotalBytes != null
                                              ? loadingProgress.cumulativeBytesLoaded / (loadingProgress.expectedTotalBytes ?? 1)
                                              : null,
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ),
                              //  const SizedBox(width: 8),
                              Container(
                                padding: const EdgeInsets.all(6.0),
                                child: const Center(
                                  child: Text(
                                    'VS',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                              // const SizedBox(width: 8),
                              ClipOval(
                                child: Image.network("${AppConstants.baseUrl}${list.awayTeam!.logo}",
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) {
                                    return ClipOval(
                                      child: Image.asset(
                                        Images.noImage,
                                        height: 40,
                                        width: 40,
                                        fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                      ),
                                    );
                                  },
                                  loadingBuilder: (context, child, loadingProgress) {
                                    if (loadingProgress == null) {
                                      return child;
                                    } else {
                                      return Center(
                                        child: CircularProgressIndicator(
                                          value: loadingProgress.expectedTotalBytes != null
                                              ? loadingProgress.cumulativeBytesLoaded / (loadingProgress.expectedTotalBytes ?? 1)
                                              : null,
                                        ),
                                      );
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        DataCell(
                          Text(
                            '${list.event!.title}',
                          ),
                        ),
                        if(!isMobile)
                        DataCell(
                            Text(
                              '${list.event!.startDate}',
                            ),
                          ),
                        if(!isMobile)
                        DataCell(
                          Container(
                            width: 100,
                            height: 30,
                            padding: const EdgeInsets.all(3),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              border: Border.all(
                                  width: 1,
                                  color: list.matchStatus == "Pending".tr
                                      ? pendingColor
                                      : list.matchStatus == 'Completed'.tr
                                      ? Colors.green
                                      : Colors.grey),
                            ),
                            child: Center(
                              child: Text(
                                '${list.matchStatus}',
                                style: TextStyle(
                                    color: list.matchStatus == "Pending".tr
                                        ? pendingColor
                                        : list.matchStatus == 'Completed'.tr
                                        ? Colors.green
                                        : Colors.grey),
                              ),
                            ),
                          ),
                        ),
                      ]);
                    }),
              ),
            );
          })
        ],
      ),
    );
  }
}

DataRow recentFileDataRow(RecentFile fileInfo) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            SvgPicture.asset(
              fileInfo.icon!,
              height: 30,
              width: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: defaultPadding),
              child: Text(fileInfo.title!),
            ),
          ],
        ),
      ),
      DataCell(Text(fileInfo.date!)),
      DataCell(Text(fileInfo.size!)),
    ],
  );
}
