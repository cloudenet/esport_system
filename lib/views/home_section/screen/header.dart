import 'package:cached_network_image/cached_network_image.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import '../../../helper/constants.dart';
import '../../../helper/images.dart';
import '../../../helper/responsive_helper.dart';

class Header extends StatelessWidget {
   Header({
    Key? key,
  }) : super(key: key);
  final controller = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (!Responsive.isDesktop(context))
          IconButton(
            icon: const Icon(Icons.menu),
            onPressed:controller.controlMenu
          ),
        if (!Responsive.isMobile(context))
          Text("Dashboard".tr,style: Theme.of(context).textTheme.titleLarge,
          ),
        if (!Responsive.isMobile(context))
          Spacer(flex: Responsive.isDesktop(context) ? 2 : 1),
       // Expanded(child: SearchField()),
        if (!Responsive.isMobile(context))
        const ProfileCard()
      ],
    );
  }
}


class ProfileCard extends StatelessWidget {
  const ProfileCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: defaultPadding),
      padding: const EdgeInsets.symmetric(
        horizontal: defaultPadding,
        vertical: defaultPadding / 2,
      ),
      decoration: BoxDecoration(
        color: secondaryColor,
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        border: Border.all(color: Colors.white10),
      ),
      child: Row(
        children: [
          CachedNetworkImage(
            imageUrl: "${AppConstants.baseUrl}/${userInfo!.photo??""}",
            width: 50,
            height: 50,
            fit: BoxFit.cover,
            placeholder: (context,
                url) =>
                CircularProgressIndicator(
                  color: Theme.of(context).primaryColor,
                ),
            errorWidget: (context,
                url, error) =>
                ClipOval(
                  child: Image.asset(
                    Images.noImage,
                    fit: BoxFit.cover, // Ensures the image fits well inside the circle
                  ),
                )//
          ),
          if (!Responsive.isMobile(context))
            Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
              child: Text(userInfo?.name??"N/A"),
            ),
          const Icon(Icons.keyboard_arrow_down),
        ],
      ),
    );
  }
}

class SearchField extends StatelessWidget {
  const SearchField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        hintText: "Search",
        fillColor: secondaryColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        suffixIcon: InkWell(
          onTap: () {},
          child: Container(
            padding: const EdgeInsets.all(defaultPadding * 0.75),
            margin: const EdgeInsets.symmetric(horizontal: defaultPadding / 2),
            decoration: const BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            child: SvgPicture.asset("assets/icons/Search.svg"),
          ),
        ),
      ),
    );
  }
}
