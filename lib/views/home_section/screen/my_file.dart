import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/my_filde_model.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/home_section/screen/my_flied.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CountCardOnDashboard extends StatefulWidget {
  const CountCardOnDashboard({
    Key? key,
  }) : super(key: key);

  @override
  State<CountCardOnDashboard> createState() => _CountCardOnDashboardState();
}

class _CountCardOnDashboardState extends State<CountCardOnDashboard> {

  var homeController = Get.find<HomeController>();
  @override
  void initState() {
    loadTotalItem();
    super.initState();
  }

  void loadTotalItem ()async {
    await homeController.fetchItemCounts();
  }
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        const SizedBox(height: defaultPadding),
        Responsive(
          mobile: FileInfoCardGridView(
            crossAxisCount: size.width < 650 ? 2 : 4,
            childAspectRatio: size.width < 650 && size.width > 350 ? 1.3 : 1,
          ),
          tablet: FileInfoCardGridView(),
          desktop: FileInfoCardGridView(
            childAspectRatio: size.width < 1400 ? 1.1 : 1.4,
          ),
        ),
      ],
    );
  }
}

class FileInfoCardGridView extends StatelessWidget {
  FileInfoCardGridView({
    Key? key,
    this.crossAxisCount = 4,
    this.childAspectRatio = 1,
  }) : super(key: key);

  final int crossAxisCount;
  final double childAspectRatio;
  final userController = Get.find<UserController>();
  final eventController = Get.find<EventController>();
  final playerController = Get.find<PlayerController>();
  final homeController = Get.find<HomeController>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MatchesController>(builder: (logic) {
      List cardModel = [
        CloudStorageInfo(
          title: "matches".tr,
          numOfFiles: 1328,
          svgSrc: "assets/icons/avatar.png",
          totalStorage: homeController.itemCounts?.totalMatches ?? "",
          color: primaryColor,
          percentage: int.tryParse(homeController.itemCounts?.totalMatches ?? "") ?? 0,
        ),
        CloudStorageInfo(
          title: "events".tr,
          numOfFiles: 1328,
          svgSrc: "assets/icons/calendar.png",
          totalStorage: homeController.itemCounts?.totalEvents??"",
          color: const Color(0xFFFFA113),
          percentage: int.tryParse(homeController.itemCounts?.totalEvents??"") ?? 0 ,
        ),
        CloudStorageInfo(
          title: "coaches".tr,
          numOfFiles: 1328,
          svgSrc: "assets/icons/user.png",
          totalStorage: homeController.itemCounts?.totalCoaches??"",
          color: const Color(0xFFA4CDFF),
          percentage: int.tryParse(homeController.itemCounts?.totalCoaches??"")?? 0 ,
        ),
        CloudStorageInfo(
          title: "players".tr,
          numOfFiles: 5328,
          svgSrc: "assets/icons/group.png",
          totalStorage: homeController.itemCounts?.totalPlayers??"",
          color: const Color(0xFF007EE5),
          percentage: int.tryParse(homeController.itemCounts?.totalPlayers??"") ?? 0 ,
        ),
      ];
      return GridView.builder(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: cardModel.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          crossAxisSpacing: defaultPadding,
          mainAxisSpacing: defaultPadding,
          childAspectRatio: childAspectRatio,
        ),
        itemBuilder: (context, index) => FileInfoCard(info: cardModel[index]),
      );
    });
  }
}

