import 'package:esport_system/helper/constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarDetails extends StatefulWidget {
  //MatchesModel? matchesModel;
  const CalendarDetails({
    Key? key,
  }) : super(key: key);

  @override
  State<CalendarDetails> createState() => _CalendarDetailsState();
}

class _CalendarDetailsState extends State<CalendarDetails> {
  final CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  final Map<DateTime, List<Event>> _events = {};
  // var matchController = Get.find<MatchesController>();

  // @override
  // void initState() {
  //     matchController.getMatches();
  //   super.initState();
  //   _loadEvents();
  // }

  // void _loadEvents() {
  //   if (matchController.matchesList.isNotEmpty) {
  //     for (var match in matchController.matchesList) {
  //       DateTime matchDate = DateTime.parse(match.event!.startDate!); // Assuming startDate is a String
  //       if (_events[matchDate] == null) {
  //         _events[matchDate] = [];
  //       }
  //       _events[matchDate]!.add(Event(title: match.event!.title!)); // Assuming match has a title
  //     }
  //   }
  // }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(defaultPadding),
      margin: const EdgeInsets.only(top: 16),
      decoration: const BoxDecoration(
        color: secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('event_date'.tr),
          TableCalendar<Event>(
            firstDay: DateTime(2000),
            lastDay: DateTime(2100),
            focusedDay: _focusedDay,
            calendarFormat: _calendarFormat,
            selectedDayPredicate: (day) {
              return isSameDay(_selectedDay, day);
            },
            onDaySelected: (selectedDay, focusedDay) {
              setState(() {
                _selectedDay = selectedDay;
                _focusedDay = focusedDay;
              });
            },
            eventLoader: (day) {
              return _events[day] ?? [];
            },
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () => _addEvent(),
      //   child: Icon(Icons.add),
      // ),
    );
  }
  //
  // void _addEvent() {
  //   if (_selectedDay == null) return;
  //
  //   showDialog(
  //     context: context,
  //     builder: (context) => AlertDialog(
  //       title: const Text('add_event'),
  //       content: TextField(
  //         onSubmitted: (value) {
  //           if (value.isEmpty) return;
  //
  //           setState(() {
  //             if (_events[_selectedDay!] == null) {
  //               _events[_selectedDay!] = [];
  //             }
  //             _events[_selectedDay]!.add(Event(title: value));
  //           });
  //
  //           Navigator.pop(context);
  //         },
  //         decoration: const InputDecoration(
  //           hintText: 'even_title',
  //         ),
  //       ),
  //     ),
  //   );
  // }
}

class Event {
  final String title;

  Event({required this.title});
}
      // child: const Column(
      //   crossAxisAlignment: CrossAxisAlignment.start,
      //   children: [
      //     Text(
      //       "Storage Details",
      //       style: TextStyle(
      //         fontSize: 18,
      //         fontWeight: FontWeight.w500,
      //       ),
      //     ),
      //     SizedBox(height: defaultPadding),
      //     Chart(),
      //     StorageInfoCard(
      //       svgSrc: "assets/icons/Documents.svg",
      //       title: "Documents Files",
      //       amountOfFiles: "1.3GB",
      //       numOfFiles: 1328,
      //     ),
      //     StorageInfoCard(
      //       svgSrc: "assets/icons/media.svg",
      //       title: "Media Files",
      //       amountOfFiles: "15.3GB",
      //       numOfFiles: 1328,
      //     ),
      //     StorageInfoCard(
      //       svgSrc: "assets/icons/folder.svg",
      //       title: "Other Files",
      //       amountOfFiles: "1.3GB",
      //       numOfFiles: 1328,
      //     ),
      //     StorageInfoCard(
      //       svgSrc: "assets/icons/unknown.svg",
      //       title: "Unknown",
      //       amountOfFiles: "1.3GB",
      //       numOfFiles: 140,
      //     ),
      //   ],
//       // ),
//     );
//   }
// }
