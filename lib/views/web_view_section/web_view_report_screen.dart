import 'dart:io' as io;
import 'dart:html';
import 'dart:ui_web';

import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:webview_flutter_wkwebview/webview_flutter_wkwebview.dart';

class WebViewReportScreen extends StatefulWidget {
  const WebViewReportScreen({super.key});

  @override
  _WebViewReportScreenState createState() => _WebViewReportScreenState();
}


class _WebViewReportScreenState extends State<WebViewReportScreen> {
  late final WebViewController controller;
  var homeController=Get.find<HomeController>();
  final PlatformViewRegistry platformViewRegistry = PlatformViewRegistry();

  @override
  void initState() {
    super.initState();

    if (kIsWeb) {
      platformViewRegistry.registerViewFactory(
        'iframeElement', (int viewId) => IFrameElement()
        ..src = '${AppConstants.baseUrl}/api/v1/exports/main-templates'
        ..style.border = 'none',
      );
    } else {
      if (io.Platform.isAndroid) {
        WebViewPlatform.instance = AndroidWebViewPlatform();
      } else if (io.Platform.isIOS) {
        WebViewPlatform.instance = WebKitWebViewPlatform();
      }

      controller = WebViewController()
        ..setJavaScriptMode(JavaScriptMode.unrestricted)
        ..loadRequest(Uri.parse('${AppConstants.baseUrl}/api/v1/exports/main-templates'));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (kIsWeb) {
      return  Scaffold(
        appBar: AppBar(
          title: Row(
            children: [
              const Icon(
                Icons.library_books_sharp,
                size: 32,
                color: Color(0xFF123456),
              ),
              const SizedBox(width: 8),
              Text(
                'របាយការណ៍'.tr,
                style: const TextStyle(
                  fontSize: 20, // Set font size
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
            ],

          ),
          backgroundColor: Colors.white,
          shadowColor: null,
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(4.0),
            child: Container(
              color: Colors.white,
              height: 4.0,
            )
          ),
        ),
        body: const Center(
          child: HtmlElementView(
            viewType: 'iframeElement',
          ),
        ),
      );
    }

    return Scaffold(
      body: WebViewWidget(controller: controller),
    );
  }
}