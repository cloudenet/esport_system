import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/model/registration_model.dart';
import 'package:esport_system/views/matches_section/screen/get_registration_matches.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../../helper/add_new_button.dart';
import '../../../helper/constants.dart';
import '../../../helper/cotainer_widget.dart';
import '../../../helper/custom_textformfield.dart';
import '../../../helper/responsive_helper.dart';

class DetailRegistrationMatch extends StatefulWidget {
  final RegistrationMatchesModel? data;

  final bool ? authorize;

  const DetailRegistrationMatch(
      {super.key, this.data, required this.authorize});

  @override
  State<DetailRegistrationMatch> createState() =>
      _DetailRegistrationMatchState();
}

class _DetailRegistrationMatchState extends State<DetailRegistrationMatch> {

  var homeController = Get.find<HomeController>();
  var m = Get.find<MatchesController>();
  String? _selectedOption;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
          mobile: bodyDetail(),
          tablet: bodyDetail(),
          desktop: bodyDetail()
      ),
    );
  }

  Widget bodyDetail() {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            if(widget.authorize!)...[
              Text("authorize_registration_matches".tr, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
              const SizedBox(height: 5,),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: GetBuilder<MatchesController>(builder: (logic) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedOption = "approved";
                              });
                            },
                            child: Container(
                              width: 120,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 8),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: _selectedOption == "approved" ? Colors.blue : Colors.grey,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Row(
                                children: [
                                  Radio<String>(
                                    focusColor: Colors.blue,
                                    activeColor: Colors.blue,
                                    value: "approved",
                                    groupValue: _selectedOption,
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedOption = value;
                                      });
                                    },
                                  ),
                                  Text("approve".tr),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(width: 10,),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                _selectedOption = "rejected";
                              });
                            },
                            child: Container(
                              width: 120,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 12, vertical: 8),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: _selectedOption == "rejected"
                                      ? Colors.blue
                                      : Colors.grey,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Row(
                                children: [
                                  Radio<String>(
                                    value: "rejected",
                                    activeColor: Colors.blue,
                                    groupValue: _selectedOption,
                                    onChanged: (value) {
                                      setState(() {
                                        _selectedOption = value;
                                      });
                                    },
                                  ),
                                  Text("reject".tr),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10,),
                      if (_selectedOption == "rejected") ...[
                        Text('note'.tr, style: const TextStyle(fontSize: 16)),
                        CustomFormBuilderTextField(
                          name: 'note'.tr,
                          controller: logic.approvedNoteController,
                          hintText: 'note'.tr,
                          errorText: 'note'.tr,
                          icon: Icons.note_alt_rounded,
                          fillColor: bgColor,
                        ),
                      ],
                    ],
                  );
                }),
              ),
              const SizedBox(height: 20,),
              Align(
                alignment: Alignment.topRight,
                child: OKButton(
                  btnName: 'save'.tr,
                  onPress: () {
                    if(_selectedOption == 'approved' &&
                        widget.data?.approvedBy == null){
                        m.authorizeRegistration(id: widget.data!.id.toString(), status: _selectedOption!);
                    }else{
                      if(_selectedOption!.isNotEmpty &&
                          m.approvedNoteController.text.isNotEmpty &&
                          widget.data?.approvedBy == null){
                        m.authorizeRegistration(id: widget.data!.id.toString(), status: _selectedOption!);
                      }else{
                        EasyLoading.showError(
                          "please_input_all_field".tr,
                          duration: const Duration(seconds: 2),
                        );
                      }
                    }
                  },
                ),
              ),
              const SizedBox(height: 10,),
            ],
            Text("event_registration_overview".tr, style: const TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold),),
            const SizedBox(height: 5,),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "registration_date".tr,
                            textData: widget.data?.registrationDate != null
                                ? widget.data!.registrationDate.toString()
                                : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "description".tr,
                            textData: widget.data?.description != null ? widget
                                .data!.description.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "match_type".tr,
                            textData: widget.data?.matchType != null ? widget
                                .data!.matchType.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "venue".tr,
                            textData: widget.data?.venue != null ? widget.data!
                                .venue.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "registration_status".tr,
                            textData: widget.data?.registrationStatus != null
                                ? widget.data!.registrationStatus.toString()
                                : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "created_at".tr,
                            textData: widget.data?.createdAt != null ? widget
                                .data!.createdAt.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("creator_details".tr, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const SizedBox(height: 5,),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "name".tr,
                            textData: widget.data?.createdBy!.name != null
                                ? widget.data!.createdBy!.name.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "name_khmer".tr,
                            textData: widget.data?.createdBy!.nameKhmer != null
                                ? widget.data!.createdBy!.nameKhmer.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "username".tr,
                            textData: widget.data?.createdBy!.username != null
                                ? widget.data!.createdBy!.username.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "phone_number".tr,
                            textData: widget.data?.createdBy!.phone != null
                                ? widget.data!.createdBy!.phone.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "email".tr,
                            textData: widget.data?.createdBy!.email != null
                                ? widget.data!.createdBy!.email.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            if(widget.data?.approvedBy != null)...[
              const SizedBox(height: 20,),
              Text("approval_details".tr, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
              const SizedBox(height: 5,),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.all(24),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: secondaryColor,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: buildText(
                            title: "approved_by".tr,
                            textData: widget.data?.approvedBy?.name?.toString() ?? "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft,
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          child: buildText(
                            title: "approved_date".tr,
                            textData: (widget.data?.approvedDate != "null" && widget.data!.approvedDate.toString().isNotEmpty)
                                ? widget.data!.approvedDate.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft,
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          child: buildText(
                            title: "approved_note".tr,
                            textData: (widget.data?.approvedNote != "null" && widget.data!.approvedNote.toString().isNotEmpty)
                                ? widget.data!.approvedNote.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
            const SizedBox(height: 20,),
            Text("team_details".tr, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const SizedBox(height: 5,),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "team_name".tr,
                            textData: widget.data?.team!.teamName != null
                                ? widget.data!.team!.teamName.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "address".tr,
                            textData: widget.data?.team!.address != null
                                ? widget.data!.team!.address.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "province".tr,
                            textData: widget.data?.team?.province != null
                                ? widget.data!.team!.province.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("event_details".tr, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const SizedBox(height: 5,),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "title".tr,
                            textData: widget.data?.event?.title != null ? widget
                                .data!.event!.title.toString() : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "address".tr,
                            textData: widget.data?.event!.address != null
                                ? widget.data!.event!.address.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "event_type".tr,
                            textData: widget.data?.event?.eventType != null
                                ? widget.data!.event!.eventType.toString()
                                : "None",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("match_details".tr, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            const SizedBox(height: 5,),
            LayoutBuilder(
              builder: (context, constraints) {
                return ConstrainedBox(
                  constraints: BoxConstraints(
                    minWidth: constraints.maxWidth,
                  ),
                  child: DataTable(
                    dataRowMaxHeight: Get.height * 0.1,
                    headingRowColor: WidgetStateColor.resolveWith((states) => secondaryColor,),
                    columns: [
                      DataColumn(
                        label: Text(
                          "player_name".tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "player_name_khmer".tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "subject_title".tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "max_point".tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          "subject_description".tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                    rows: widget.data!.matchDetails!.isEmpty
                        ? []
                        : List<DataRow>.generate(
                      widget.data!.matchDetails!.length, (index) {
                      var list = widget.data!.matchDetails![index];
                      return DataRow(
                        cells: [
                          DataCell(
                            Text(
                              list.player?.playerName != null ? list.player!
                                  .playerName.toString() : "N/A",
                            ),
                          ),
                          DataCell(
                            Text(
                                list.player?.playerNameKh != null ? list.player!
                                    .playerNameKh.toString() : "N/A"
                            ),
                          ),
                          DataCell(
                            Text(
                              list.subject != null ? list.subject!.title
                                  .toString() : "N/A",
                            ),
                          ),
                          DataCell(
                            Text(
                              list.subject != null ? list.subject!.maxPoint
                                  .toString() : "N/A",
                            ),
                          ),
                          DataCell(
                            Text(
                              list.subject != null ? list.subject!.description
                                  .toString() : "N/A",
                            ),
                          ),
                        ],
                      );
                    },
                    ),
                  ),
                );
              },
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(
                      const GetRegistrationMatches());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}
