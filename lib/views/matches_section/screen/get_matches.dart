import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/views/matches_section/screen/create_matches_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/controllers/home_controller.dart';
import '../../../data/model/matches_model.dart';
import '../../../helper/images.dart';
import 'matches_detail_screen.dart';

class MatchesScreen extends StatefulWidget {
  final MatchesModel? data;

  const MatchesScreen({super.key, this.data});

  @override
  State<MatchesScreen> createState() => _MatchesScreenState();
}

class _MatchesScreenState extends State<MatchesScreen> {
  var matchesController = Get.find<MatchesController>();
  var eventController = Get.find<EventController>();
  var teamController = Get.find<TeamController>();
  var homeController = Get.find<HomeController>();

  String? selectedEventTitle;
  String? selectedTeamId;
  String? selectAwayId;

  @override
  void initState() {
    matchesController.getMatches();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<MatchesController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
            Container(
            color: bgColor,
            padding: const EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: secondaryColor,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: DropdownButton<int>(
                    value: logic.limit,
                    items: [6, 12, 18, 24].map((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: Text(
                          '$value',
                          style: const TextStyle(color: Colors.white),
                        ),
                      );
                    }).toList(),
                    onChanged: (newValue) {
                      if (newValue != null) {
                        logic.setLimit(newValue);
                      }
                    },
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                    dropdownColor: secondaryColor,
                    underline: Container(),
                    elevation: 8,
                    borderRadius: BorderRadius.circular(8),
                    icon: const Icon(Icons.arrow_drop_down),
                    iconSize: 24,
                    iconEnabledColor: Colors.grey,
                    iconDisabledColor: Colors.grey[400],
                    isDense: true,
                    menuMaxHeight: 200,
                    alignment: Alignment.center,
                  ),
                ),
                const Spacer(),
                Expanded(
                  child: _searchField(
                    onChange: (value) {
                      logic.search(value);
                    },
                  ),
                ),
              ],
            ),
          ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/avatar.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'match'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: AddNewButton(
                      btnName: 'add_new'.tr,
                      onPress: () {
                        homeController.updateWidgetHome(const CreateMatchesScreen());
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : LayoutBuilder(
                            builder: (context, constraints) {
                              return SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  children: [
                                    SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minWidth: constraints.maxWidth,
                                        ),
                                        child: DataTable(
                                          dataRowMaxHeight: Get.height * 0.1,
                                          headingRowColor: WidgetStateColor.resolveWith((states) => secondaryColor,),
                                          columns: [
                                            DataColumn(
                                              label: Text(
                                                "versus".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "event_name".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "start_date".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "end_date".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "start_time".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "end_time".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "status".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            DataColumn(
                                              label: Text(
                                                "actions".tr,
                                                style: const TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                          rows: logic.matchesList.isEmpty
                                          ?[]
                                          :List<DataRow>.generate(
                                            logic.matchesList.length, (index) {
                                              var list = logic.matchesList[index];
                                              return DataRow(
                                                cells: [
                                                  DataCell(
                                                    GestureDetector(
                                                      onTap: () {},
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                        children: [
                                                          ClipOval(
                                                            child: Image.network(
                                                              "${AppConstants.baseUrl}${list.homeTeam!.logo}",
                                                              width: 50,
                                                              height: 50,
                                                              fit: BoxFit.cover,
                                                              errorBuilder: (context, error, stackTrace) {
                                                                return ClipOval(
                                                                  child: Image.asset(
                                                                    Images.noImage,
                                                                    height: 50,
                                                                    width: 50,
                                                                    fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                                                  ),
                                                                );
                                                              },
                                                              loadingBuilder: (context,
                                                                  child,
                                                                  loadingProgress) {
                                                                if (loadingProgress ==
                                                                    null) {
                                                                  return child;
                                                                } else {
                                                                  return Center(
                                                                    child:
                                                                    CircularProgressIndicator(
                                                                      value: loadingProgress
                                                                          .expectedTotalBytes !=
                                                                          null
                                                                          ? loadingProgress
                                                                          .cumulativeBytesLoaded /
                                                                          (loadingProgress.expectedTotalBytes ??
                                                                              1)
                                                                          : null,
                                                                    ),
                                                                  );
                                                                }
                                                              },
                                                            ),
                                                          ),
                                                          const SizedBox(width: 8),
                                                          Container(
                                                            padding: const EdgeInsets.all(8),
                                                            child: const Center(
                                                              child: Text(
                                                                'VS',
                                                                style: TextStyle(
                                                                  fontWeight: FontWeight.bold,
                                                                  fontSize: 18,
                                                                  color: Colors.white,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(width: 8),
                                                          ClipOval(
                                                            child: Image.network(
                                                              "${AppConstants.baseUrl}${list.awayTeam!.logo}",
                                                              width: 50,
                                                              height: 50,
                                                              fit: BoxFit.cover,
                                                              errorBuilder: (context, error, stackTrace) {
                                                                return ClipOval(
                                                                  child: Image.asset(
                                                                    Images.noImage,
                                                                    height: 50,
                                                                    width: 50,
                                                                    fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                                                  ),
                                                                );
                                                              },
                                                              loadingBuilder: (context, child, loadingProgress) {
                                                                if (loadingProgress == null) {
                                                                  return child;
                                                                } else {
                                                                  return Center(
                                                                    child:
                                                                    CircularProgressIndicator(
                                                                      value: loadingProgress.expectedTotalBytes != null
                                                                          ? loadingProgress.cumulativeBytesLoaded /
                                                                          (loadingProgress.expectedTotalBytes ?? 1)
                                                                          : null,
                                                                    ),
                                                                  );
                                                                }
                                                              },
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      '${list.event!.title}',
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      formatDate(
                                                        list.event!.startDate
                                                            .toString(),
                                                      ),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      formatDate(list.event!.endDate
                                                          .toString()),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    GestureDetector(
                                                      onTap: () {},
                                                      child: Text(
                                                        '${list.startTime}',
                                                      ),
                                                    ),
                                                  ),
                                                  DataCell(
                                                    Text(
                                                      '${list.endTime}',
                                                    ),
                                                  ),
                                                  DataCell(StatusContainerX(
                                                    status:
                                                    list.matchStatus.toString(),
                                                  )),
                                                  DataCell(
                                                    Row(
                                                      children: [
                                                        MouseRegion(
                                                          cursor: SystemMouseCursors.click,
                                                          child: GestureDetector(
                                                            onTap: () {
                                                              homeController.updateWidgetHome(MatchesDetailScreen(data: list));
                                                            },
                                                            child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.amberAccent.withOpacity(0.1),
                                                                borderRadius: BorderRadius.circular(6),
                                                                border: Border.all(width: 1, color: Colors.amberAccent),
                                                              ),
                                                              child: const Icon(
                                                                Icons.visibility,
                                                                size: 20,
                                                                color: Colors.amberAccent,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(width: 12),
                                                        GestureDetector(
                                                          onTap: () {
                                                            homeController.updateWidgetHome(CreateMatchesScreen(matchesModel: list,),);
                                                          },
                                                          child: Container(
                                                            padding: const EdgeInsets.all(6),
                                                            decoration: BoxDecoration(
                                                              color: Colors.green.withOpacity(0.1),
                                                              borderRadius: BorderRadius.circular(6),
                                                              border: Border.all(width: 1, color: Colors.green),
                                                            ),
                                                            child: const Icon(
                                                              Icons.edit,
                                                              size: 20,
                                                              color: Colors.green,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    if (logic.matchesList.isEmpty)...[
                                      const SizedBox(height: 50,),
                                      const Center(child: EmptyData()),
                                    ]
                                  ],
                                ),
                              );
                            },
                          ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: logic.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= logic.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () => logic.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color: logic.currentPage == i
                                                ? Colors.blue
                                                : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: logic.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _searchField({
    required Function(String) onChange,
  }) {
    final TextEditingController controller = TextEditingController(text: "select_status_to_search");
    return Row(
      children: [
        Expanded(
          flex: 4,
          child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SizedBox(
                height: 48, // Set the desired height
                child: TextField(
                  controller: controller,
                  onChanged: (value) {
                    onChange(value);
                  },
                  decoration: InputDecoration(
                    fillColor: bgColor,
                    hintStyle: const TextStyle(color: Colors.white),
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    contentPadding: const EdgeInsets.only(left: 12),
                    // Set the horizontal padding
                    suffixIcon: DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(10),
                      value: matchesController.selectedValue,
                      items: matchesController.dropdownItems.map((item) {
                        return DropdownMenuItem<String>(
                          value: item,
                          child: Text(item),
                        );
                      }).toList(),
                      onChanged: (value) {
                        if (value != null) {
                          setState(() {
                            matchesController.selectDropdownValue(value);
                            controller.text = value;
                            onChange(value);
                          });
                        }
                      },
                      decoration: InputDecoration(
                        labelText: "select_status_to_search".tr,
                        floatingLabelAlignment: FloatingLabelAlignment.center,
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        fillColor: secondaryColor,
                        filled: true,
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        matchesController.selectedValue != null ? const SizedBox(width: 10,):const SizedBox(),
        if (matchesController.selectedValue != null) // Change the condition here
          GestureDetector(
            onTap: () {
              setState(() {
                matchesController.selectedValue = null;
                controller.clear();
                onChange('');
              });
            },
            child: Container(
                height: 48,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor
                ),
                child: const Icon(Icons.clear,color: Colors.red,)
            ),
          ),
      ],
    );
  }
}


class StatusContainerX extends StatelessWidget {
  final String status;

  const StatusContainerX({
    Key? key,
    required this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Normalize the status string
    final normalizedStatus = status.trim().toLowerCase();

    // Determine colors based on status
    final backgroundColor = normalizedStatus == "pending"
        ? Colors.amberAccent.withOpacity(0.1)
        : normalizedStatus == "cancelled"
        ? Colors.grey.withOpacity(0.1)
        : normalizedStatus == "completed"
        ? Colors.green.withOpacity(0.1)
        : Colors.grey.withOpacity(0.1); // Default color for unexpected statuses

    final borderColor = normalizedStatus == "pending"
        ? Colors.amberAccent
        : normalizedStatus == "cancelled"
        ? Colors.grey
        : normalizedStatus == "completed"
        ? Colors.green
        : Colors.grey; // Default color for unexpected statuses

    final textColor = borderColor;

    return Container(
      width: 100,
      height: 30,
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: backgroundColor,
        border: Border.all(width: 1, color: borderColor),
      ),
      child: Center(
        child: Text(
          status[0].toUpperCase() + status.substring(1), // Capitalize the first letter
          style: TextStyle(color: textColor),
        ),
      ),
    );
  }
}

