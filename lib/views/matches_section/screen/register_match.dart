import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/model/event_model.dart';
import 'package:esport_system/data/model/team_model.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/matches_section/screen/get_registration_matches.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import '../../../data/controllers/player_controller.dart';
import '../../../data/controllers/subject_controller.dart';
import '../../../data/controllers/team_controller.dart';
import '../../../data/model/player_model.dart';
import '../../../data/model/registration_model.dart';
import '../../../data/model/subject_model.dart';
import '../../../helper/add_new_button.dart';
import '../../../helper/custom_textformfield.dart';
import '../../../helper/dropdown_search_player_widget.dart';
import '../../../helper/empty_data.dart';
import '../../../helper/sr.dart';

class RegisterMatch extends StatefulWidget {
  final RegistrationMatchesModel? data;

  const RegisterMatch({super.key, this.data});

  @override
  State<RegisterMatch> createState() => _RegisterMatchState();
}

class _RegisterMatchState extends State<RegisterMatch> {

  var m = Get.find<MatchesController>();
  var teamController = Get.find<TeamController>();
  var eventController = Get.find<EventController>();
  var playerController = Get.find<PlayerController>();
  var subjectController = Get.find<SubjectController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    m.clearData();
    if (widget.data != null) {
      m.setOldValue(widget.data);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Responsive(
            mobile: body(mobile: true,tablet: false,desktop: false),
            tablet: body(mobile: false,tablet: true,desktop: false),
            desktop: body(mobile: false,tablet: false,desktop: true),
          ),
        ),
      ),
    );
  }


  Widget body({
    required bool mobile ,
    required bool tablet ,
    required bool desktop ,
  }) {
    return GetBuilder<MatchesController>(builder: (logic) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 40),
          Text(
            widget.data != null ? 'update_register_match'.tr : 'register_match'.tr,
            style: const TextStyle(
                fontSize: 24, fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 10),
          if(mobile)...[
            Text('select_team'.tr, style: const TextStyle(fontSize: 16)),
            DropdownButtonFormField<TeamModel>(
              borderRadius: BorderRadius.circular(10),
              value: logic.selectTeam,
              items: teamController.listTeam.map((team) {
                return DropdownMenuItem<TeamModel>(
                  value: team,
                  child: Text(team.teamName.toString()),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  logic.selectedTeamId = value!.id.toString();
                  logic.selectTeam = value;
                });
              },
              decoration: InputDecoration(
                labelText: "select_team".tr,
                floatingLabelAlignment: FloatingLabelAlignment.center,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                fillColor: secondaryColor,
                filled: true,
                border: const OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(
                      Radius.circular(10)),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Text("select_event".tr,
                style: const TextStyle(fontSize: 16)),
            DropdownButtonFormField<EventModel>(
              borderRadius: BorderRadius.circular(10),
              value: eventController.eventData.contains(
                  logic.selectEvent)
                  ? logic.selectEvent
                  : null,
              items: eventController.eventData.map((event) {
                return DropdownMenuItem<EventModel>(
                  value: event,
                  child: Text(event.title ?? 'unknown'.tr),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  logic.selectedEventId = value!.id.toString();
                  logic.selectEvent = value;
                });
              },
              decoration: InputDecoration(
                labelText: 'select_event'.tr,
                floatingLabelAlignment: FloatingLabelAlignment.center,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                prefixIcon: const Icon(Icons.event),
                border: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                      Radius.circular(10)
                  ),
                  borderSide: BorderSide.none,
                ),
                filled: true,
                fillColor: secondaryColor,
              ),
              validator: FormBuilderValidators.required(),
              hint: Text('select_event'.tr),
            ),
            const SizedBox(height: 10),
            Text('match_type'.tr, style: const TextStyle(fontSize: 16)),
            DropdownButtonFormField<String>(
              borderRadius: BorderRadius.circular(10),
              value: logic.selectedMatchType,
              items: logic.listMatchType.map((type) {
                return DropdownMenuItem<String>(
                  value: type,
                  child: Text(
                      type[0].toUpperCase() + type.substring(1)),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  logic.selectedMatchType = value;
                });
              },
              decoration: InputDecoration(
                labelText: "match_type".tr,
                floatingLabelAlignment: FloatingLabelAlignment.center,
                floatingLabelBehavior: FloatingLabelBehavior.never,
                fillColor: secondaryColor,
                filled: true,
                border: const OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.all(
                      Radius.circular(10)
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Text('description'.tr, style: const TextStyle(fontSize: 16)),
            CustomFormBuilderTextField(
              name: 'description'.tr,
              controller: logic.descriptionController,
              hintText: 'description'.tr,
              errorText: 'description'.tr,
              icon: Icons.description_outlined,
              onTap: () {},
              fillColor: secondaryColor,
            ),
            const SizedBox(height: 10),
            Text('venue'.tr, style: const TextStyle(fontSize: 16)),
            CustomFormBuilderTextField(
              name: 'venue'.tr,
              controller: logic.venueController,
              hintText: 'venue'.tr,
              errorText: 'venue'.tr,
              icon: Icons.location_on,
              onTap: () {},
              fillColor: secondaryColor,
            ),
          ]
          else if(tablet || desktop)...[
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('select_team'.tr, style: const TextStyle(fontSize: 16)),
                      DropdownButtonFormField<TeamModel>(
                        borderRadius: BorderRadius.circular(10),
                        value: logic.selectTeam,
                        items: teamController.listTeam.map((team) {
                          return DropdownMenuItem<TeamModel>(
                            value: team,
                            child: Text(team.teamName.toString()),
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            logic.selectedTeamId = value!.id.toString();
                            logic.selectTeam = value;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: "select_team".tr,
                          floatingLabelAlignment: FloatingLabelAlignment.center,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          fillColor: secondaryColor,
                          filled: true,
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("select_event".tr,
                          style: const TextStyle(fontSize: 16)),
                      DropdownButtonFormField<EventModel>(
                        borderRadius: BorderRadius.circular(10),
                        value: eventController.eventData.contains(
                            logic.selectEvent)
                            ? logic.selectEvent
                            : null,
                        items: eventController.eventData.map((event) {
                          return DropdownMenuItem<EventModel>(
                            value: event,
                            child: Text(event.title ?? 'unknown'.tr),
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            logic.selectedEventId = value!.id.toString();
                            logic.selectEvent = value;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: 'select_event'.tr,
                          floatingLabelAlignment: FloatingLabelAlignment.center,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          prefixIcon: const Icon(Icons.event),
                          border: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)
                            ),
                            borderSide: BorderSide.none,
                          ),
                          filled: true,
                          fillColor: secondaryColor,
                        ),
                        validator: FormBuilderValidators.required(),
                        hint: Text('select_event'.tr),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('match_type'.tr, style: const TextStyle(fontSize: 16)),
                      DropdownButtonFormField<String>(
                        borderRadius: BorderRadius.circular(10),
                        value: logic.selectedMatchType,
                        items: logic.listMatchType.map((type) {
                          return DropdownMenuItem<String>(
                            value: type,
                            child: Text(
                                type[0].toUpperCase() + type.substring(1)),
                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            logic.selectedMatchType = value;
                          });
                        },
                        decoration: InputDecoration(
                          labelText: "match_type".tr,
                          floatingLabelAlignment: FloatingLabelAlignment.center,
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          fillColor: secondaryColor,
                          filled: true,
                          border: const OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('description'.tr, style: const TextStyle(fontSize: 16)),
                      CustomFormBuilderTextField(
                        name: 'description'.tr,
                        controller: logic.descriptionController,
                        hintText: 'description'.tr,
                        errorText: 'description'.tr,
                        icon: Icons.description_outlined,
                        onTap: () {},
                        fillColor: secondaryColor,
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('venue'.tr, style: const TextStyle(fontSize: 16)),
                      CustomFormBuilderTextField(
                        name: 'venue'.tr,
                        controller: logic.venueController,
                        hintText: 'venue'.tr,
                        errorText: 'venue'.tr,
                        icon: Icons.location_on,
                        onTap: () {},
                        fillColor: secondaryColor,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
          const SizedBox(height: 10),
          Align(
              alignment: Alignment.topRight,
              child: AddNewButton(
                  btnName: 'add_new'.tr,
                  onPress: () {
                    if (logic.selectTeam != null &&
                        logic.selectEvent != null &&
                        logic.descriptionController.text.isNotEmpty &&
                        logic.selectedMatchType!.isNotEmpty &&
                        logic.venueController.text.isNotEmpty) {
                      logic.addMatchDetail(MatchDetail(
                          playerId: 0, subjectId: 0, description: ""));
                    } else {
                      EasyLoading.showError(
                        "please_input_all_field".tr,
                        duration: const Duration(seconds: 2),
                      );
                    }
                  }
              )
          ),
          const SizedBox(height: 10),
          LayoutBuilder(
            builder: (context, constraints) {
              return SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: [
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minWidth: constraints.maxWidth,
                        ),
                        child: DataTable(
                          dataRowMaxHeight: Get.height * 0.1,
                          headingRowColor:
                          WidgetStateColor.resolveWith((
                              states) => secondaryColor),
                          columns: [
                            DataColumn(
                              label: Text(
                                "select_player".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "select_subject".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "team_name".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "event".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "match_type".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                "actions".tr,
                                style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                          rows: logic.matchDetail.isEmpty
                              ? []
                              : List<DataRow>.generate(
                            logic.matchDetail.length, (index) {
                            return DataRow(
                              cells: [
                                DataCell(
                                  PlayerDropDownInMatch(
                                    //selectedItem: logic.selectedPlayers[index],
                                    selectedItem: logic.selectedPlayers.length >
                                        index
                                        ? logic.selectedPlayers[index]
                                        : null,
                                    onChanged: (PlayerModel? player) {
                                      if (player != null) {
                                        if (logic.selectedPlayers.length >
                                            index) {
                                          logic.selectedPlayers[index] = player;
                                        } else {
                                          logic.selectedPlayers.add(player);
                                        }
                                        logic.updateMatchDetail(
                                          index,
                                          MatchDetail(
                                            playerId: SR.toInt(player.id),
                                            // Assuming `id` exists in PlayerModel
                                            subjectId: logic.matchDetail
                                                .length > index
                                                ? logic.matchDetail[index]
                                                .subjectId
                                                : 0,
                                            description: logic.matchDetail
                                                .length > index
                                                ? logic.matchDetail[index]
                                                .description
                                                : "",
                                          ),
                                        );
                                        logic.update();
                                      }
                                    },
                                  ),
                                ),
                                DataCell(
                                  dropdownSubject(
                                    onChanged: (SubjectModel? subject) {
                                      if (subject != null) {
                                        if (logic.selectSubjects.length >
                                            index) {
                                          logic.selectSubjects[index] = subject;
                                        } else {
                                          logic.selectSubjects.add(subject);
                                        }
                                        logic.updateMatchDetail(
                                          index,
                                          MatchDetail(
                                            playerId: logic.matchDetail.length >
                                                index
                                                ? logic.matchDetail[index]
                                                .playerId
                                                : 0,
                                            subjectId: SR.toInt(subject.id),
                                            // Assuming `id` exists in SubjectModel
                                            description: logic.matchDetail
                                                .length > index
                                                ? logic.matchDetail[index]
                                                .description
                                                : "",
                                          ),
                                        );
                                      }
                                    },
                                    selectSub: logic.selectSubjects.length >
                                        index
                                        ? logic.selectSubjects[index]
                                        : null,
                                  ),
                                ),
                                DataCell(
                                  Text(
                                    _truncateText(
                                        logic.selectTeam!.teamName.toString(),
                                        20),
                                  ),
                                ),
                                DataCell(
                                  Text(
                                    _truncateText(
                                        logic.selectEvent != null
                                            ? logic.selectEvent!.title
                                            .toString()
                                            : "N/A", 20),
                                  ),
                                ),
                                DataCell(
                                  Text(
                                    _truncateText(
                                        logic.selectedMatchType.toString(), 20),
                                  ),
                                ),

                                DataCell(
                                  Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          logic.removeMatchDetail(index);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.all(6),
                                          decoration: BoxDecoration(
                                            color: Colors.red.withOpacity(0.1),
                                            border: Border.all(
                                                width: 1, color: Colors.red),
                                            borderRadius: BorderRadius.circular(
                                                6),
                                          ),
                                          child: const Icon(
                                            Icons.delete,
                                            size: 20,
                                            color: Colors.red,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            );
                          },
                          ),

                        ),
                      ),
                    ),
                    if (logic.matchDetail.isEmpty)...[
                      const SizedBox(height: 50,),
                      const Center(child: EmptyData()),
                    ],
                    if (logic.matchDetail.isNotEmpty)...[
                      Row(
                        children: [
                          const Spacer(),
                          Row(
                            children: [
                              CancelButton(btnName: "cancel".tr, onPress: (){
                                homeController.updateWidgetHome(const GetRegistrationMatches());
                              }),
                              const SizedBox(width: 10,),
                              OKButton(
                                  btnName: widget.data != null?"update".tr:"save".tr,
                                  onPress: (){
                                    for (int i = 0; i < logic.matchDetail.length; i++) {
                                      var a = logic.matchDetail[i];
                                      print(" playerId: ${a.playerId}----subjectId:${a.subjectId}");
                                    }
                                    if (widget.data != null) {
                                      logic.updateRegisterMatch(id: widget.data!.id.toString());
                                    } else {
                                      logic.registerMatch();
                                    }
                                  }
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ],
                ),
              );
            },
          )
        ],
      );
    });
  }

  Widget dropdownSubject({required ValueChanged<SubjectModel?> onChanged,
    required SubjectModel? selectSub}) {
    return DropdownButtonFormField<SubjectModel>(
      borderRadius: BorderRadius.circular(10),
      value: selectSub,
      items: subjectController.subjectData.map((v) {
        return DropdownMenuItem<SubjectModel>(
          value: v,
          child: Text(v.title.toString()),
        );
      }).toList(),
      onChanged: onChanged,
      decoration: InputDecoration(
        labelText: "select_subject".tr,
        floatingLabelAlignment: FloatingLabelAlignment.center,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        fillColor: secondaryColor,
        filled: true,
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}

class MatchDetail {
  final int playerId;
  final int subjectId;
  final String description;

  MatchDetail({
    required this.playerId,
    required this.subjectId,
    required this.description,
  });

  Map<String, dynamic> toJson() {
    return {
      "player_id": playerId,
      "subject_id": subjectId,
      "description": description,
    };
  }
}








