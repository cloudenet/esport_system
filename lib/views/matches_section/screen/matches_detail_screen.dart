import 'package:cached_network_image/cached_network_image.dart';
import 'package:esport_system/data/model/matches_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/matches_section/screen/get_matches.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../../data/controllers/home_controller.dart';

class MatchesDetailScreen extends StatefulWidget {
  final MatchesModel data;
  const MatchesDetailScreen({super.key, required this.data});

  @override
  State<MatchesDetailScreen> createState() => _MaState();
}

class _MaState extends State<MatchesDetailScreen> {

  var homeController = Get.find<HomeController>();
  String formatDate(String dateString) {
    DateTime date = DateTime.parse(dateString);
    final DateFormat formatter = DateFormat('EEEE d, MMM yyyy');
    return formatter.format(date);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Responsive(
        tablet: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: bgColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'match_detail'.tr,
                        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                            width: 1,
                            color: widget.data.matchStatus == "Pending".tr
                                ? pendingColor
                                : widget.data.matchStatus == 'Completed'.tr
                                ? Colors.green
                                : Colors.grey,
                          ),
                        ),
                        child: Text(
                          '${widget.data.matchStatus}',
                          style: TextStyle(
                            color: widget.data.matchStatus == "Pending".tr
                                ? pendingColor
                                : widget.data.matchStatus == 'Completed'.tr
                                ? Colors.green
                                : Colors.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 16.0),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    widget.data.event!.title.toString(),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(fontSize: 16),
                  ),
                ),
                const SizedBox(height: 24.0),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Center(
                          child: CircleAvatar(
                            radius: 35,
                            child: Image.network(
                              "${AppConstants.baseUrl}${widget.data.homeTeam!.logo.toString()}",
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                        Text(
                          widget.data.homeTeam!.teamName.toString(),
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        "VS",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(height: 10,),
                    Column(
                      children: [
                        Center(
                          child: CircleAvatar(
                            radius: 35,
                            child: Image.network(
                              "${AppConstants.baseUrl}/${widget.data.awayTeam!.logo.toString()}",
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                        Text(
                          widget.data.awayTeam!.teamName.toString(),
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        formatDate(
                          widget.data.event!.startDate.toString(),
                        ),
                        style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                      ),
                      const Text(" / "),
                      Text(
                        widget.data.startTime.toString(),
                        style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                      ),
                      const Text(" - "),
                      Text(
                        widget.data.endTime.toString(),
                        style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Icon(
                      Icons.location_on,
                      color: Colors.red,
                      size: 34,
                    ),
                    Text(
                      widget.data.event!.address.toString(),
                      style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                    ),
                  ],
                ),
                const SizedBox(height: 24),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: CancelButton(
                      btnName: 'close'.tr,
                      onPress: () {
                        homeController.updateWidgetHome(const MatchesScreen());
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        mobile: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: bgColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'match_detail'.tr,
                        style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                      Container(
                        padding: const EdgeInsets.all(3),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          border: Border.all(
                            width: 1,
                            color: widget.data.matchStatus == "Pending".tr
                                ? pendingColor
                                : widget.data.matchStatus == 'Completed'.tr
                                ? Colors.green
                                : Colors.grey,
                          ),
                        ),
                        child: Text(
                          '${widget.data.matchStatus}',
                          style: TextStyle(
                            color: widget.data.matchStatus == "Pending".tr
                                ? pendingColor
                                : widget.data.matchStatus == 'Completed'.tr
                                ? Colors.green
                                : Colors.grey,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 16.0),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    widget.data.event!.title.toString(),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(fontSize: 16),
                  ),
                ),
                const SizedBox(height: 24.0),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Center(
                          child: CircleAvatar(
                            radius: 35,
                            child: Image.network(
                              "${AppConstants.baseUrl}${widget.data.homeTeam!.logo.toString()}",
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                        Text(
                          widget.data.homeTeam!.teamName.toString(),
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10,),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        "VS",
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(height: 10,),
                    Column(
                      children: [
                        Center(
                          child: CircleAvatar(
                            radius: 35,
                            child: Image.network(
                              "${AppConstants.baseUrl}/${widget.data.awayTeam!.logo.toString()}",
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                        Text(
                          widget.data.awayTeam!.teamName.toString(),
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        formatDate(
                          widget.data.event!.startDate.toString(),
                        ),
                        style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                      ),
                      const Text(" / "),
                      Text(
                        widget.data.startTime.toString(),
                        style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                      ),
                      const Text(" - "),
                      Text(
                        widget.data.endTime.toString(),
                        style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Icon(
                      Icons.location_on,
                      color: Colors.red,
                      size: 34,
                    ),
                    Text(
                      widget.data.event!.address.toString(),
                      style: Theme.of(context).textTheme.headlineMedium?.copyWith(fontSize: 16),
                    ),
                  ],
                ),
                const SizedBox(height: 24),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: CancelButton(
                      btnName: 'close'.tr,
                      onPress: () {
                        homeController.updateWidgetHome(const MatchesScreen());
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        desktop: Column(
          children: [
            const SizedBox(height: 50,),

            Card(
              color: secondaryColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
              margin: const EdgeInsets.all(10),
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.data.event?.title ?? "N/A",
                      style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: Colors.blueAccent),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        const Icon(Icons.location_on, color: Colors.redAccent),
                        const SizedBox(width: 5),
                        Text(widget.data.event?.address ?? "N/A", style: const TextStyle(fontSize: 16)),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        const Icon(Icons.calendar_today, color: Colors.green),
                        const SizedBox(width: 5),
                        Text(
                          "${widget.data.event?.startDate ?? "N/A"} - ${widget.data.event?.endDate ?? "N/A"}",
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),

            Card(
              color: secondaryColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
              margin: const EdgeInsets.all(10),
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        const Icon(Icons.timer, color: Colors.orangeAccent),
                        const SizedBox(width: 5),
                        Text(
                          "Match Time: ${widget.data.startTime ?? "N/A"} - ${widget.data.endTime ?? "N/A"}",
                          style: const TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children: [
                        const Icon(Icons.sports_soccer, color: Colors.orange),
                        const SizedBox(width: 5),
                        Text(
                          "Status: ${widget.data.matchStatus ?? "N/A"}",
                          style: const TextStyle(fontSize: 16, color: Colors.orangeAccent),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),

            Card(
              color: secondaryColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
              margin: const EdgeInsets.all(10),
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: widget.data.homeTeam?.logo != null && widget.data.homeTeam!.logo!.isNotEmpty
                              ? CachedNetworkImage(
                            imageUrl: "${AppConstants.baseUrl}${widget.data.homeTeam!.logo!}",
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Container(
                              width: 80,
                              height: 80,
                              color: Colors.grey[300],
                              child: const Icon(Icons.image, size: 40, color: Colors.grey),
                            ),
                            errorWidget: (context, url, error) => Container(
                              width: 80,
                              height: 80,
                              color: Colors.grey[300],
                              child: const Icon(Icons.image_not_supported, size: 40, color: Colors.redAccent),
                            ),
                          )
                              : Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: const Icon(Icons.image_not_supported, size: 40, color: Colors.redAccent),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          widget.data.homeTeam?.teamName ?? "N/A",
                          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 5),
                        Text(
                          "Score: ${widget.data.homeTeamScore ?? "N/A"}",
                          style: const TextStyle(fontSize: 16, color: Colors.blueAccent),
                        ),
                      ],
                    ),
                    const Column(
                      children: [
                        Text("VS", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
                      ],
                    ),
                    Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: widget.data.awayTeam?.logo != null && widget.data.awayTeam!.logo!.isNotEmpty
                              ? CachedNetworkImage(
                            imageUrl: "${AppConstants.baseUrl}${widget.data.homeTeam!.logo!}",
                            width: 80,
                            height: 80,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Container(
                              width: 80,
                              height: 80,
                              color: Colors.grey[300],
                              child: const Icon(Icons.image, size: 40, color: Colors.grey),
                            ),
                            errorWidget: (context, url, error) => Container(
                              width: 80,
                              height: 80,
                              color: Colors.grey[300],
                              child: const Icon(Icons.image_not_supported, size: 40, color: Colors.redAccent),
                            ),
                          )
                              : Container(
                            width: 80,
                            height: 80,
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: const Icon(Icons.image_not_supported, size: 40, color: Colors.redAccent),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          widget.data.awayTeam?.teamName ?? "N/A",
                          style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 5),
                        Text(
                          "Score: ${widget.data.awayTeamScore ?? "N/A"}",
                          style: const TextStyle(fontSize: 16, color: Colors.redAccent),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),

            if (widget.data.winningTeam != null && widget.data.losingTeam != null)
              Card(
                color: secondaryColor,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                margin: const EdgeInsets.all(10),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const Icon(Icons.emoji_events, color: Colors.green),
                          const SizedBox(width: 5),
                          Text(
                            "Winning Team: ${widget.data.homeTeamScore}",
                            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.green),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      Row(
                        children: [
                          const Icon(Icons.sentiment_dissatisfied, color: Colors.red),
                          const SizedBox(width: 5),
                          Text(
                            "Losing Team: ${widget.data.awayTeamScore}",
                            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.red),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
          ],
        )
      )
    );
  }
}
