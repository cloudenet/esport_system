import 'package:esport_system/data/controllers/event_controller.dart';
import 'package:esport_system/data/controllers/matches_controller.dart';
import 'package:esport_system/data/model/matches_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/helper/route_helper.dart';
import 'package:esport_system/views/matches_section/screen/get_matches.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import '../../../data/controllers/home_controller.dart';
import '../../../data/controllers/team_controller.dart';

class CreateMatchesScreen extends StatefulWidget {
  final MatchesModel? matchesModel;

  const CreateMatchesScreen({super.key, this.matchesModel});

  @override
  State<CreateMatchesScreen> createState() => _CreateMatchesScreenState();
}

class _CreateMatchesScreenState extends State<CreateMatchesScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final eventController = Get.find<EventController>();
  final teamController = Get.find<TeamController>();
  final matchesController = Get.find<MatchesController>();

  String? selectedEventTitle;
  String? selectedTeamId;
  String? selectedAwayId;
  String? selectedLoseTeamId;
  String?selectedWinTeamId;


  @override
  void initState() {
    loadEvent();
    loadTeam();
    if (widget.matchesModel != null) {
      matchesController.endTimeController.text = widget.matchesModel!.endTime.toString();
      matchesController.startTimeController.text = widget.matchesModel!.startTime.toString();

      //selectedEventTitle = widget.matchesModel!.event!.id.toString();
      selectedEventTitle = eventController.eventData.any((event) => event.id == widget.matchesModel!.event!.id.toString())
          ? widget.matchesModel!.event!.id.toString()
          : null;

      selectedTeamId = widget.matchesModel!.homeTeam!.id.toString();
      selectedAwayId = widget.matchesModel!.awayTeam!.id.toString();
      selectedWinTeamId = widget.matchesModel!.homeTeam!.id.toString();
      selectedLoseTeamId = widget.matchesModel!.awayTeam!.id.toString();
      matchesController.selectedStatus = widget.matchesModel!.matchStatus.toString();
    } else {
      matchesController.endTimeController.clear();
      matchesController.startTimeController.clear();
      selectedEventTitle = null;
      selectedTeamId = null;
      selectedAwayId = null;
      selectedWinTeamId=null;
      selectedLoseTeamId=null;
    }

    super.initState();
  }

  Future<void> loadEvent() async {
    await eventController.getEvent();
    setState(() {});
  }

  Future<void> loadTeam() async {
    await teamController.getTeam();
    setState(() {});
  }

  Future<void> _selectTime(
      BuildContext context, TextEditingController controller) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (picked != null) {
      final formattedTime =
          "${picked.hour.toString().padLeft(2, '0')}:${picked.minute.toString().padLeft(2, '0')}";
      controller.text = formattedTime;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<MatchesController>(
        builder: (logic) {
          return Container(
            margin: const EdgeInsets.all(16),
            child: FormBuilder(
              key: _formKey,
              child: SingleChildScrollView(
                child: Responsive(
                    tablet: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            color: bgColor,
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.matchesModel != null
                                        ? "update".tr
                                        : "create".tr,
                                    style:
                                    Theme.of(context).textTheme.headlineSmall,
                                  ),
                                  const SizedBox(height: 16),
                                  Text(
                                    "select_event".tr,
                                    style: const TextStyle(fontSize: 16),
                                  ),
                                  const SizedBox(height: 5,),
                                  DropdownButtonFormField<String>(
                                    value: selectedEventTitle,
                                    items: eventController.eventData.map((event) {
                                      print('Selected Event ID: $selectedEventTitle');
                                      return DropdownMenuItem<String>(
                                        value: event.id,
                                        child: Text(event.title ?? 'unknown'.tr),
                                      );
                                    }).toList(),
                                    onChanged: (value) {
                                      setState(() {
                                        selectedEventTitle = value;
                                      });
                                      print('Selected Event ID: $selectedEventTitle');
                                    },
                                    decoration: InputDecoration(
                                      labelText: 'select_event'.tr,
                                      floatingLabelAlignment: FloatingLabelAlignment.center,
                                      floatingLabelBehavior: FloatingLabelBehavior.never,
                                      prefixIcon: const Icon(Icons.event),
                                      border: const OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        borderSide: BorderSide.none,
                                      ),
                                      filled: true,
                                      fillColor: secondaryColor,
                                    ),
                                    validator: FormBuilderValidators.required(),
                                    hint: Text('select_event'.tr),
                                  ),
                                  const SizedBox(height: 16),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "select_home_team".tr,
                                              style: const TextStyle(fontSize: 16),
                                            ),
                                            const SizedBox(height: 5,),
                                            DropdownButtonFormField<String>(
                                              value: selectedTeamId,
                                              items: teamController.listTeam
                                                  .map((team) {
                                                return DropdownMenuItem<String>(
                                                  value: team.id,
                                                  child: Text(
                                                      team.teamName ?? 'unknown'.tr),
                                                );
                                              }).toList(),
                                              onChanged: (value) {
                                                setState(() {
                                                  selectedTeamId = value ?? "";
                                                });
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'select_team'.tr,
                                                floatingLabelAlignment:
                                                FloatingLabelAlignment.center,
                                                floatingLabelBehavior:
                                                FloatingLabelBehavior.never,
                                                prefixIcon:
                                                const Icon(Icons.groups),
                                                border: const OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(10)),
                                                  borderSide: BorderSide.none,
                                                ),
                                                filled: true,
                                                fillColor: secondaryColor,
                                              ),
                                              validator:
                                              FormBuilderValidators.required(),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(width: 16),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "select_away_team".tr,
                                              style: const TextStyle(fontSize: 16),
                                            ),
                                            const SizedBox(height: 5,),
                                            DropdownButtonFormField<String>(
                                              value: selectedAwayId,
                                              items: teamController.listTeam.map((team) {
                                                return DropdownMenuItem<String>(
                                                  value: team.id,
                                                  child: Text(
                                                    team.teamName ?? 'unknown'.tr,
                                                    overflow: TextOverflow.ellipsis, // Prevents overflow by truncating text
                                                  ),
                                                );
                                              }).toList(),
                                              onChanged: (value) {
                                                setState(() {
                                                  selectedAwayId = value ?? '';
                                                });
                                              },
                                              decoration: InputDecoration(
                                                labelText: 'select_away_team'.tr,
                                                floatingLabelAlignment: FloatingLabelAlignment.center,
                                                floatingLabelBehavior: FloatingLabelBehavior.never,
                                                prefixIcon: const Icon(Icons.groups),
                                                border: const OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                                  borderSide: BorderSide.none,
                                                ),
                                                filled: true,
                                                fillColor: secondaryColor,
                                              ),
                                              validator: FormBuilderValidators.required(),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 16),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "start_time".tr,
                                              style: const TextStyle(fontSize: 16),
                                            ),
                                            const SizedBox(height: 5,),
                                            FormBuilderTextField(
                                              name: 'start_time'.tr,
                                              controller: logic.startTimeController,
                                              decoration: InputDecoration(
                                                hintText: 'start_time'.tr,
                                                prefixIcon:
                                                const Icon(Icons.watch_later),
                                                border: const OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(10)),
                                                  borderSide: BorderSide.none,
                                                ),
                                                filled: true,
                                                fillColor: secondaryColor,
                                              ),
                                              validator:
                                              FormBuilderValidators.required(),
                                              readOnly: true,
                                              onTap: () {
                                                _selectTime(context,
                                                    logic.startTimeController);
                                                print('========startDate${logic.startTimeController.text}');
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(width: 16),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "end_time".tr,
                                              style: const TextStyle(fontSize: 16),
                                            ),
                                            const SizedBox(height: 5,),
                                            FormBuilderTextField(
                                              name: 'end_time'.tr,
                                              controller: logic.endTimeController,
                                              decoration: InputDecoration(
                                                hintText: 'end_time'.tr,
                                                prefixIcon:
                                                const Icon(Icons.watch_later),
                                                border: const OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(10)),
                                                  borderSide: BorderSide.none,
                                                ),
                                                filled: true,
                                                fillColor: secondaryColor,
                                              ),
                                              validator:
                                              FormBuilderValidators.required(),
                                              readOnly: true,
                                              onTap: () {
                                                _selectTime(context,
                                                    logic.endTimeController);
                                                print('=====endDate${logic.endTimeController.text}');
                                              },
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(height: 16),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CancelButton(
                                          btnName: 'cancel'.tr,
                                          onPress: () {
                                            Get.find<HomeController>()
                                                .updateWidgetHome(
                                                const MatchesScreen());
                                          },
                                        ),
                                      ),
                                      const SizedBox(height: 16),
                                      OKButton(
                                        btnName: widget.matchesModel != null
                                            ? "update".tr
                                            : "create".tr,
                                        onPress: () async {
                                          String awayTeamId =
                                          selectedAwayId.toString();
                                          String eventId =
                                          selectedEventTitle.toString();
                                          String homeTeamId =
                                          selectedTeamId.toString();
                                          String startTime =
                                              logic.startTimeController.text;
                                          String endTime =
                                              logic.endTimeController.text;
                                          String? matchId =
                                          widget.matchesModel?.id.toString();
                                          String updatedBy =
                                          userInfo!.id.toString();

                                          try {
                                            EasyLoading.show(
                                                status: 'loading...'.tr,
                                                dismissOnTap: true);

                                            if (widget.matchesModel != null) {
                                              // Update operation
                                              bool result =
                                              await logic.updateMatches(
                                                eventId: eventId,
                                                homeTeamId: homeTeamId,
                                                awayTeamId: awayTeamId,
                                                startTime: startTime,
                                                endTime: endTime,
                                                matchId: matchId!,
                                                updatedBy: updatedBy,
                                              );

                                              if (result) {
                                                EasyLoading.showSuccess(
                                                    'match_updated_successfully!'.tr);
                                                Get.find<HomeController>()
                                                    .updateWidgetHome(
                                                    const MatchesScreen());
                                              } else {
                                                EasyLoading.showError(
                                                    'failed_to_update_match'.tr);
                                              }
                                            } else {
                                              // Create operation
                                              if (_formKey.currentState != null &&
                                                  _formKey.currentState!
                                                      .saveAndValidate()) {
                                                bool result =
                                                await logic.createMatches(
                                                  eventId: eventId,
                                                  homeTeamId: homeTeamId,
                                                  awayTeamId: awayTeamId,
                                                  startTime: startTime,
                                                  endTime: endTime,
                                                );

                                                if (result) {
                                                  EasyLoading.showSuccess(
                                                      'match_created_successfully!'.tr);
                                                  Get.find<HomeController>()
                                                      .updateWidgetHome(
                                                      const MatchesScreen());
                                                } else {
                                                  EasyLoading.showError(
                                                      'failed_to_create_match'.tr);
                                                }
                                              }
                                            }
                                          } catch (e) {
                                            EasyLoading.showError(
                                                'an_error_occurred: $e'.tr);
                                          } finally {
                                            EasyLoading.dismiss();
                                          }
                                        },
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        widget.matchesModel == null
                            ? const SizedBox()
                            : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "update_status_matches".tr,
                                    style: Theme.of(context).textTheme.headlineSmall,
                                  ),
                                  const SizedBox(height: 16),
                                  Container(
                                    width: Get.width * 0.2,
                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: ['Pending'.tr, 'Cancelled'.tr
                                      ].map((status) {
                                        bool isSelected = logic.selectedStatus == status;
                                        return GestureDetector(
                                          onTap: () => logic.selectStatus(status),
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                            decoration: BoxDecoration(
                                              color: isSelected
                                                  ? Colors.blue
                                                  : Colors.grey[300],
                                              borderRadius: BorderRadius.circular(10),
                                              border: Border.all(color: isSelected ? Colors.blue : Colors.grey,),
                                            ),
                                            child: Text(
                                              status,
                                              style: TextStyle(
                                                color: isSelected
                                                    ? Colors.white
                                                    : Colors.black,
                                              ),
                                            ),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  const SizedBox(height: 10,),
                                  SizedBox(
                                    width: Get.width * 0.3,
                                    child: FormBuilderTextField(
                                      name: 'status'.tr,
                                      controller: TextEditingController(text: logic.selectedStatus),
                                      decoration: const InputDecoration(
                                          prefixIcon: Icon(Icons.watch_later),
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            borderSide: BorderSide.none,
                                          ),
                                          filled: true,
                                          fillColor: bgColor
                                        // Adjust the bgColor as needed
                                      ),
                                      validator:
                                      FormBuilderValidators.required(),
                                      readOnly: true,
                                      onTap: () {},
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  const SizedBox(height: 16),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CancelButton(
                                          btnName: 'cancel'.tr,
                                          onPress: () {
                                            Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                          },
                                        ),
                                      ),
                                      const SizedBox(height: 16),
                                      OKButton(
                                        btnName: "confirm".tr,
                                        onPress: () async {
                                          logic.isLoading = true;
                                          EasyLoading.show(
                                              status: 'loading...'.tr,
                                              dismissOnTap: true);
                                          try {
                                            logic.updateStatusMatche(
                                              matcheId: widget.matchesModel!.id.toString(),
                                              statusMatche: logic.selectedStatus.toString(),
                                            ).then((result) {
                                              if (result) {
                                                EasyLoading.showSuccess('status_updated_successfully!'.tr);
                                                Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                              } else {
                                                EasyLoading.showError('failed_to_update_status'.tr);
                                              }
                                              logic.isLoading = false;
                                              EasyLoading.dismiss();
                                            });
                                          } catch (e) {
                                            print('error_updating_status: $e'.tr);
                                            logic.isLoading = false;
                                            EasyLoading.dismiss();
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // if(widget.matchesModel==null)

                        widget.matchesModel?.matchStatus == 'Completed'.tr
                            ? const SizedBox()
                            : widget.matchesModel == null
                            ? const SizedBox()
                            : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "update_modify_finish_match".tr,
                                      style: Theme.of(context).textTheme.headlineSmall,
                                    ),
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "select_winning_team".tr,
                                                style: const TextStyle(fontSize: 16),
                                              ),
                                              DropdownButtonFormField<String>(
                                                value: selectedWinTeamId,
                                                items: teamController.listTeam.map((team) {
                                                  return DropdownMenuItem<String>(
                                                    value: team.id,
                                                    child: Text(team.teamName ?? 'unknown'.tr),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedWinTeamId = value ?? '';
                                                  });
                                                  print("Team ID : $selectedWinTeamId");
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'select_winning_team'.tr,
                                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                                  prefixIcon: const Icon(Icons.groups),
                                                  border: const OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  filled: true,
                                                  fillColor: bgColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 10),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "select_losing_team".tr,
                                                style: const TextStyle(fontSize: 16),
                                              ),
                                              DropdownButtonFormField<String>(
                                                value: selectedLoseTeamId,
                                                items: teamController.listTeam.map((team) {
                                                  return DropdownMenuItem<String>(
                                                    value: team.id,
                                                    child: Text(team.teamName ?? 'unknown'.tr),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedLoseTeamId = value ?? '';
                                                  });
                                                  print("Team ID : $selectedLoseTeamId");
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'select_losing_team'.tr,
                                                  floatingLabelAlignment:
                                                  FloatingLabelAlignment.center,
                                                  floatingLabelBehavior:
                                                  FloatingLabelBehavior.never,
                                                  prefixIcon: const Icon(Icons.groups),
                                                  border: const OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  filled: true,
                                                  fillColor: bgColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                  'score_winning'.tr,
                                                  style: const TextStyle(fontSize: 16)),
                                              CustomFormBuilderTextField(
                                                errorText: _formKey.currentState?.fields['score_winning'.tr]?.errorText,
                                                name: 'score_winning'.tr,
                                                controller: logic.losingController,
                                                hintText: 'enter_score_winning'.tr,
                                                icon: Icons.code,
                                                fillColor: bgColor,
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 10,),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                  'score_losing'.tr,
                                                  style: const TextStyle(fontSize: 16)),
                                              CustomFormBuilderTextField(
                                                errorText: _formKey.currentState?.fields['score_losing'.tr]?.errorText,
                                                name: 'score_losing'.tr,
                                                controller: logic.winningController,
                                                hintText: 'enter_score_losing'.tr,
                                                icon: Icons.code,
                                                fillColor: bgColor,
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 10,),
                                      ],
                                    ),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: CancelButton(
                                            btnName: 'cancel'.tr,
                                            onPress: () {
                                              logic.winningController.text = '';
                                              logic.losingController.text = '';
                                              Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                            },
                                          ),
                                        ),
                                        const SizedBox(height: 16),
                                        OKButton(
                                          btnName: "confirm".tr,
                                          onPress: () async {
                                            EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
                                            try {
                                              logic.updateModifyMatch(
                                                matcheId: widget.matchesModel!.id.toString(),
                                                winningId: selectedWinTeamId.toString(),
                                                losingId: selectedLoseTeamId.toString(),
                                                losingScore: logic.losingController.text,
                                                winningScore: logic.winningController.text,
                                              ).then((result) {
                                                if (result) {
                                                  EasyLoading.showSuccess('updated_modify_finish_successfully!'.tr);
                                                  logic.winningController.text = '';
                                                  logic.losingController.text = '';
                                                  Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                                } else {
                                                  EasyLoading.showError('failed_to_update_modify_finish.'.tr);
                                                }
                                                EasyLoading.dismiss();
                                              });
                                            } catch (e) {
                                              logic.isLoading = false;
                                              EasyLoading.dismiss();
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ),
                        )
                      ],
                    ),
                    mobile: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          color: bgColor,
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.matchesModel != null
                                      ? "update".tr
                                      : "create".tr,
                                  style: Theme.of(context).textTheme.headlineSmall,
                                ),
                                const SizedBox(height: 16),
                                Text("select_event".tr,style: const TextStyle(fontSize: 16),),
                                const SizedBox(height: 5,),
                                DropdownButtonFormField<String>(
                                  value: selectedEventTitle,
                                  items: eventController.eventData.map((event) {
                                    print('Selected Event ID: $selectedEventTitle');
                                    return DropdownMenuItem<String>(
                                      value: event.id,
                                      child: Text(event.title ?? 'unknown'.tr),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedEventTitle = value;
                                    });
                                    print('Selected Event ID: $selectedEventTitle');
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'select_event'.tr,
                                    floatingLabelAlignment: FloatingLabelAlignment.center,
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    prefixIcon: const Icon(Icons.event),
                                    border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide.none,
                                    ),
                                    filled: true,
                                    fillColor: secondaryColor,
                                  ),
                                  validator: FormBuilderValidators.required(),
                                  hint: Text('select_event'.tr),
                                ),
                                const SizedBox(height: 16),
                                Text("select_home_team".tr,),
                                const SizedBox(height: 5,),
                                DropdownButtonFormField<String>(
                                  value: selectedTeamId,
                                  items: teamController.listTeam.map((team) {
                                    return DropdownMenuItem<String>(
                                      value: team.id,
                                      child: Text(
                                        (team.teamName?.length ?? 0) > 50
                                            ? '${team.teamName?.substring(0, 50)}...'
                                            : team.teamName ?? 'unknown'.tr,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedTeamId = value ?? "";
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'select_team'.tr,
                                    floatingLabelAlignment:FloatingLabelAlignment.center,
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide.none,
                                    ),
                                    filled: true,
                                    fillColor: secondaryColor,
                                  ),
                                  validator:FormBuilderValidators.required(),
                                ),
                                const SizedBox(height: 16),
                                Text( "select_away_team".tr,),
                                const SizedBox(height: 5,),
                                DropdownButtonFormField<String>(
                                  value: selectedAwayId,
                                  items: teamController.listTeam.map((team) {
                                    return DropdownMenuItem<String>(
                                      value: team.id,
                                      child: Text(
                                        (team.teamName?.length ?? 0) > 50
                                            ? '${team.teamName?.substring(0, 50)}...'
                                            : team.teamName ?? 'unknown'.tr,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedAwayId = value ?? '';
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'select_away_team'.tr,
                                    floatingLabelAlignment: FloatingLabelAlignment.center,
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide.none,
                                    ),
                                    filled: true,
                                    fillColor: secondaryColor,
                                  ),
                                  validator: FormBuilderValidators.required(),
                                ),
                                const SizedBox(height: 16),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "start_time".tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5,),
                                          FormBuilderTextField(
                                            name: 'start_time'.tr,
                                            controller: logic.startTimeController,
                                            decoration: InputDecoration(
                                              hintText: 'start_time'.tr,
                                              prefixIcon: const Icon(Icons.watch_later),
                                              border: const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                borderSide: BorderSide.none,
                                              ),
                                              filled: true,
                                              fillColor: secondaryColor,
                                            ),
                                            validator: FormBuilderValidators.required(),
                                            readOnly: true,
                                            onTap: () {
                                              _selectTime(context, logic.startTimeController);
                                              print('========startDate${logic.startTimeController.text}');
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(width: 16),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "end_time".tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5,),
                                          FormBuilderTextField(
                                            name: 'end_time'.tr,
                                            controller: logic.endTimeController,
                                            decoration: InputDecoration(
                                              hintText: 'end_time'.tr,
                                              prefixIcon:
                                              const Icon(Icons.watch_later),
                                              border: const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: BorderSide.none,
                                              ),
                                              filled: true,
                                              fillColor: secondaryColor,
                                            ),
                                            validator:
                                            FormBuilderValidators.required(),
                                            readOnly: true,
                                            onTap: () {
                                              _selectTime(context,
                                                  logic.endTimeController);
                                              print('=====endDate${logic.endTimeController.text}');
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CancelButton(
                                        btnName: 'cancel'.tr,
                                        onPress: () {
                                          Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                        },
                                      ),
                                    ),
                                    const SizedBox(height: 16),
                                    OKButton(
                                      btnName: widget.matchesModel != null
                                          ? "update".tr
                                          : "create".tr,
                                      onPress: () async {
                                        String awayTeamId = selectedAwayId.toString();
                                        String eventId = selectedEventTitle.toString();
                                        String homeTeamId = selectedTeamId.toString();
                                        String startTime = logic.startTimeController.text;
                                        String endTime = logic.endTimeController.text;
                                        String? matchId = widget.matchesModel?.id.toString();
                                        String updatedBy = userInfo!.id.toString();

                                        try {
                                          EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);

                                          if (widget.matchesModel != null) {
                                            bool result = await logic.updateMatches(
                                              eventId: eventId,
                                              homeTeamId: homeTeamId,
                                              awayTeamId: awayTeamId,
                                              startTime: startTime,
                                              endTime: endTime,
                                              matchId: matchId!,
                                              updatedBy: updatedBy,
                                            );

                                            if (result) {
                                              EasyLoading.showSuccess('match_updated_successfully!'.tr);
                                              Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                            } else {
                                              EasyLoading.showError('failed_to_update_match'.tr);
                                            }
                                          } else {
                                            // Create operation
                                            if (_formKey.currentState != null &&
                                                _formKey.currentState!.saveAndValidate()) {
                                              bool result = await logic.createMatches(
                                                eventId: eventId,
                                                homeTeamId: homeTeamId,
                                                awayTeamId: awayTeamId,
                                                startTime: startTime,
                                                endTime: endTime,
                                              );

                                              if (result) {
                                                EasyLoading.showSuccess('match_created_successfully!'.tr);
                                                Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                              } else {
                                                EasyLoading.showError('failed_to_create_match'.tr);
                                              }
                                            }
                                          }
                                        } catch (e) {
                                          EasyLoading.showError('an_error_occurred: $e'.tr);
                                        } finally {
                                          EasyLoading.dismiss();
                                        }
                                      },
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        widget.matchesModel == null
                            ? const SizedBox()
                            : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "update_status_matches".tr,
                                    style: Theme.of(context).textTheme.headlineSmall,
                                  ),
                                  const SizedBox(height: 16),
                                  Container(
                                    width: Get.width * 0.2,
                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: ['Pending'.tr, 'Cancelled'.tr
                                      ].map((status) {
                                        bool isSelected = logic.selectedStatus == status;
                                        return GestureDetector(
                                          onTap: () => logic.selectStatus(status),
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                            decoration: BoxDecoration(
                                              color: isSelected
                                                  ? Colors.blue
                                                  : Colors.grey[300],
                                              borderRadius:
                                              BorderRadius.circular(10),
                                              border: Border.all(
                                                color: isSelected
                                                    ? Colors.blue
                                                    : Colors.grey,
                                              ),
                                            ),
                                            child: Text(
                                              status,
                                              style: TextStyle(
                                                color: isSelected
                                                    ? Colors.white
                                                    : Colors.black,
                                              ),
                                            ),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  const SizedBox(height: 10,),
                                  SizedBox(
                                    width: Get.width * 0.3,
                                    child: FormBuilderTextField(
                                      name: 'status'.tr,
                                      controller: TextEditingController(text: logic.selectedStatus),
                                      decoration: const InputDecoration(
                                          prefixIcon: Icon(Icons.watch_later),
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            borderSide: BorderSide.none,
                                          ),
                                          filled: true,
                                          fillColor: bgColor
                                        // Adjust the bgColor as needed
                                      ),
                                      validator: FormBuilderValidators.required(),
                                      readOnly: true,
                                      onTap: () {},
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  const SizedBox(height: 16),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CancelButton(
                                          btnName: 'cancel'.tr,
                                          onPress: () {
                                            Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                          },
                                        ),
                                      ),
                                      const SizedBox(height: 16),
                                      OKButton(
                                        btnName: "confirm".tr,
                                        onPress: () async {
                                          logic.isLoading = true;
                                          EasyLoading.show(
                                              status: 'loading...'.tr,
                                              dismissOnTap: true);
                                          try {
                                            logic.updateStatusMatche(
                                              matcheId: widget.matchesModel!.id.toString(),
                                              statusMatche: logic.selectedStatus.toString(),
                                            ).then((result) {
                                              if (result) {
                                                // Handle success
                                                EasyLoading.showSuccess('status_updated_successfully!'.tr);
                                                Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                              } else {
                                                // Handle failure
                                                EasyLoading.showError('failed_to_update_status'.tr);
                                              }
                                              logic.isLoading = false;
                                              EasyLoading.dismiss();
                                            });
                                          } catch (e) {
                                            // Handle any additional errors that might not be caught by updateStatusMatche
                                            print('error_updating_status: $e'.tr);
                                            logic.isLoading = false;
                                            EasyLoading.dismiss();
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // if(widget.matchesModel==null)

                        widget.matchesModel?.matchStatus == 'Completed'.tr
                            ? const SizedBox()
                            : widget.matchesModel == null
                            ? const SizedBox()
                            : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "update_modify_finish_match".tr,
                                      style: Theme.of(context).textTheme.headlineSmall,
                                    ),
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "select_winning_team".tr,
                                                style: const TextStyle(fontSize: 16),
                                              ),
                                              DropdownButtonFormField<String>(
                                                value: selectedWinTeamId,
                                                items: teamController.listTeam.map((team) {
                                                  return DropdownMenuItem<String>(
                                                    value: team.id,
                                                    child: Text(team.teamName ?? 'unknown'.tr),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedWinTeamId = value ?? '';
                                                  });
                                                  print("Team ID : $selectedWinTeamId");
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'select_winning_team'.tr,
                                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                                  prefixIcon: const Icon(Icons.groups),
                                                  border: const OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  filled: true,
                                                  fillColor: bgColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 10),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "select_losing_team".tr,
                                                style: const TextStyle(fontSize: 16),
                                              ),
                                              DropdownButtonFormField<String>(
                                                value: selectedLoseTeamId,
                                                items: teamController.listTeam.map((team) {
                                                  return DropdownMenuItem<String>(
                                                    value: team.id,
                                                    child: Text(team.teamName ?? 'unknown'.tr),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedLoseTeamId = value ?? '';
                                                  });
                                                  print("Team ID : $selectedLoseTeamId");
                                                },
                                                decoration:
                                                InputDecoration(
                                                  labelText: 'select_losing_team'.tr,
                                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                                  prefixIcon: const Icon(Icons.groups),
                                                  border: const OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  filled: true,
                                                  fillColor: bgColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text('score_winning'.tr, style: const TextStyle(fontSize: 16)),
                                              CustomFormBuilderTextField(
                                                errorText: _formKey.currentState?.fields['score_winning'.tr]?.errorText,
                                                name: 'score_winning'.tr,
                                                controller: logic.losingController,
                                                hintText: 'enter_score_winning'.tr,
                                                icon: Icons.code,
                                                fillColor: bgColor,
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 10,),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                  'score_losing'.tr,
                                                  style: const TextStyle(fontSize: 16)),
                                              CustomFormBuilderTextField(
                                                errorText: _formKey.currentState?.fields['score_losing'.tr]?.errorText,
                                                name: 'score_losing'.tr,
                                                controller: logic.winningController,
                                                hintText: 'enter_score_losing'.tr,
                                                icon: Icons.code,
                                                fillColor: bgColor,
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 26,),
                                      ],
                                    ),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: CancelButton(
                                            btnName: 'cancel'.tr,
                                            onPress: () {
                                              logic.winningController.text = '';
                                              logic.losingController.text = '';
                                              Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                            },
                                          ),
                                        ),
                                        const SizedBox(height: 16),
                                        OKButton(
                                          btnName: "confirm".tr,
                                          onPress: () async {
                                            EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
                                            try {
                                              logic.updateModifyMatch(
                                                matcheId: widget.matchesModel!.id.toString(),
                                                winningId: selectedWinTeamId.toString(),
                                                losingId: selectedLoseTeamId.toString(),
                                                losingScore: logic.losingController.text,
                                                winningScore: logic.winningController.text,
                                              ).then((result) {
                                                if (result) {
                                                  EasyLoading.showSuccess('updated_modify_finish_successfully!'.tr);
                                                  logic.winningController.text = '';
                                                  logic.losingController.text = '';
                                                  Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                                } else {
                                                  EasyLoading.showError('failed_to_update_modify_finish.'.tr);
                                                }
                                                EasyLoading.dismiss();
                                              });
                                            } catch (e) {
                                              logic.isLoading = false;
                                              EasyLoading.dismiss();
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ),
                        )
                      ],
                    ),
                    desktop: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          color: bgColor,
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.matchesModel != null
                                      ? "update".tr
                                      : "create".tr,
                                  style: Theme.of(context).textTheme.headlineSmall,
                                ),
                                const SizedBox(height: 16),
                                Text(
                                  "select_event".tr,
                                  style: const TextStyle(fontSize: 16),
                                ),
                                const SizedBox(height: 5,),
                                DropdownButtonFormField<String>(
                                  value: selectedEventTitle,
                                  items: eventController.eventData.map((event) {
                                    return DropdownMenuItem<String>(
                                      value: event.id ?? "",
                                      child: Text(event.title ?? 'unknown'.tr),
                                    );
                                  }).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedEventTitle = value;
                                    });
                                    print('Selected Event ID: $selectedEventTitle');
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'select_event'.tr,
                                    floatingLabelAlignment: FloatingLabelAlignment.center,
                                    floatingLabelBehavior: FloatingLabelBehavior.never,
                                    prefixIcon: const Icon(Icons.event),
                                    border: const OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide.none,
                                    ),
                                    filled: true,
                                    fillColor: secondaryColor,
                                  ),
                                  validator: FormBuilderValidators.required(),
                                  hint: Text('select_event'.tr),
                                ),
                                const SizedBox(height: 16),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "select_home_team".tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5,),
                                          DropdownButtonFormField<String>(
                                            value: selectedTeamId,
                                            items: teamController.listTeam
                                                .map((team) {
                                              return DropdownMenuItem<String>(
                                                value: team.id,
                                                child: Text(
                                                    team.teamName ?? 'unknown'.tr),
                                              );
                                            }).toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                selectedTeamId = value ?? "";
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText: 'select_team'.tr,
                                              floatingLabelAlignment:
                                              FloatingLabelAlignment.center,
                                              floatingLabelBehavior:
                                              FloatingLabelBehavior.never,
                                              prefixIcon:
                                              const Icon(Icons.groups),
                                              border: const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10)),
                                                borderSide: BorderSide.none,
                                              ),
                                              filled: true,
                                              fillColor: secondaryColor,
                                            ),
                                            validator:
                                            FormBuilderValidators.required(),
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(width: 16),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "select_away_team".tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5,),
                                          DropdownButtonFormField<String>(
                                            value: selectedAwayId,
                                            items: teamController.listTeam.map((team) {
                                              return DropdownMenuItem<String>(
                                                value: team.id,
                                                child: Text(
                                                  team.teamName ?? 'unknown'.tr,
                                                  overflow: TextOverflow.ellipsis, // Prevents overflow by truncating text
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                selectedAwayId = value ?? '';
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText: 'select_away_team'.tr,
                                              floatingLabelAlignment: FloatingLabelAlignment.center,
                                              floatingLabelBehavior: FloatingLabelBehavior.never,
                                              prefixIcon: const Icon(Icons.groups),
                                              border: const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                borderSide: BorderSide.none,
                                              ),
                                              filled: true,
                                              fillColor: secondaryColor,
                                            ),
                                            validator: FormBuilderValidators.required(),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "start_time".tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5,),
                                          FormBuilderTextField(
                                            name: 'start_time'.tr,
                                            controller: logic.startTimeController,
                                            decoration: InputDecoration(
                                              hintText: 'start_time'.tr,
                                              prefixIcon: const Icon(Icons.watch_later),
                                              border: const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                borderSide: BorderSide.none,
                                              ),
                                              filled: true,
                                              fillColor: secondaryColor,
                                            ),
                                            validator: FormBuilderValidators.required(),
                                            readOnly: true,
                                            onTap: () {
                                              _selectTime(context, logic.startTimeController);
                                              print('========startDate${logic.startTimeController.text}');
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(width: 16),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "end_time".tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5,),
                                          FormBuilderTextField(
                                            name: 'end_time'.tr,
                                            controller: logic.endTimeController,
                                            decoration: InputDecoration(
                                              hintText: 'end_time'.tr,
                                              prefixIcon: const Icon(Icons.watch_later),
                                              border: const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                                borderSide: BorderSide.none,
                                              ),
                                              filled: true,
                                              fillColor: secondaryColor,
                                            ),
                                            validator: FormBuilderValidators.required(),
                                            readOnly: true,
                                            onTap: () {
                                              _selectTime(context, logic.endTimeController);
                                              print('=====endDate${logic.endTimeController.text}');
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: CancelButton(
                                        btnName: 'cancel'.tr,
                                        onPress: () {
                                          Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                        },
                                      ),
                                    ),
                                    const SizedBox(height: 16),
                                    OKButton(
                                      btnName: widget.matchesModel != null
                                          ? "update".tr
                                          : "create".tr,
                                      onPress: () async {
                                        String awayTeamId = selectedAwayId.toString();
                                        String eventId = selectedEventTitle.toString();
                                        String homeTeamId = selectedTeamId.toString();
                                        String startTime = logic.startTimeController.text;
                                        String endTime = logic.endTimeController.text;
                                        String? matchId = widget.matchesModel?.id.toString();
                                        String updatedBy = userInfo!.id.toString();

                                        try {
                                          EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);

                                          if (widget.matchesModel != null) {
                                            // Update operation
                                            bool result = await logic.updateMatches(
                                              eventId: eventId,
                                              homeTeamId: homeTeamId,
                                              awayTeamId: awayTeamId,
                                              startTime: startTime,
                                              endTime: endTime,
                                              matchId: matchId!,
                                              updatedBy: updatedBy,
                                            );

                                            if (result) {
                                              EasyLoading.showSuccess('match_updated_successfully!'.tr);
                                              Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                            } else {
                                              EasyLoading.showError('failed_to_update_match'.tr);
                                            }
                                          } else {
                                            // Create operation
                                            if (_formKey.currentState != null &&
                                                _formKey.currentState!.saveAndValidate()) {
                                              bool result = await logic.createMatches(
                                                eventId: eventId,
                                                homeTeamId: homeTeamId,
                                                awayTeamId: awayTeamId,
                                                startTime: startTime,
                                                endTime: endTime,
                                              );

                                              if (result) {
                                                EasyLoading.showSuccess('match_created_successfully!'.tr);
                                                Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                              } else {
                                                EasyLoading.showError('failed_to_create_match'.tr);
                                              }
                                            }
                                          }
                                        } catch (e) {
                                          EasyLoading.showError(
                                              'an_error_occurred: $e'.tr);
                                        } finally {
                                          EasyLoading.dismiss();
                                        }
                                      },
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        widget.matchesModel == null
                            ? const SizedBox()
                            : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "update_status_matches".tr,
                                    style: Theme.of(context).textTheme.headlineSmall,
                                  ),
                                  const SizedBox(height: 16),
                                  Container(
                                    width: Get.width * 0.2,
                                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: ['Pending'.tr, 'Cancelled'.tr
                                      ].map((status) {
                                        bool isSelected = logic.selectedStatus == status;
                                        return GestureDetector(
                                          onTap: () => logic.selectStatus(status),
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                            decoration: BoxDecoration(
                                              color: isSelected
                                                  ? Colors.blue
                                                  : Colors.grey[300],
                                              borderRadius:
                                              BorderRadius.circular(10),
                                              border: Border.all(
                                                color: isSelected
                                                    ? Colors.blue
                                                    : Colors.grey,
                                              ),
                                            ),
                                            child: Text(
                                              status,
                                              style: TextStyle(
                                                color: isSelected
                                                    ? Colors.white
                                                    : Colors.black,
                                              ),
                                            ),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  const SizedBox(height: 10,),
                                  SizedBox(
                                    width: Get.width * 0.3,
                                    child: FormBuilderTextField(
                                      name: 'status'.tr,
                                      controller: TextEditingController(text: logic.selectedStatus),
                                      decoration: const InputDecoration(
                                          prefixIcon: Icon(Icons.watch_later),
                                          border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(Radius.circular(10)),
                                            borderSide: BorderSide.none,
                                          ),
                                          filled: true,
                                          fillColor: bgColor
                                        // Adjust the bgColor as needed
                                      ),
                                      validator:
                                      FormBuilderValidators.required(),
                                      readOnly: true,
                                      onTap: () {},
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                  const SizedBox(height: 16),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CancelButton(
                                          btnName: 'cancel'.tr,
                                          onPress: () {
                                            Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                          },
                                        ),
                                      ),
                                      const SizedBox(height: 16),
                                      OKButton(
                                        btnName: "confirm".tr,
                                        onPress: () async {
                                          logic.isLoading = true;
                                          EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
                                          try {
                                            logic.updateStatusMatche(
                                              matcheId: widget.matchesModel!.id.toString(),
                                              statusMatche: logic.selectedStatus.toString(),
                                            ).then((result) {
                                              if (result) {
                                                // Handle success
                                                EasyLoading.showSuccess('status_updated_successfully!'.tr);
                                                Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                              } else {
                                                // Handle failure
                                                EasyLoading.showError('failed_to_update_status'.tr);
                                              }
                                              logic.isLoading = false;
                                              EasyLoading.dismiss();
                                            });
                                          } catch (e) {
                                            // Handle any additional errors that might not be caught by updateStatusMatche
                                            print('error_updating_status: $e'.tr);
                                            logic.isLoading = false;
                                            EasyLoading.dismiss();
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // if(widget.matchesModel==null)

                        widget.matchesModel?.matchStatus == 'Completed'.tr
                            ? const SizedBox()
                            : widget.matchesModel == null
                            ? const SizedBox()
                            : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              color: secondaryColor,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                ),
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "update_modify_finish_match".tr,
                                      style: Theme.of(context).textTheme.headlineSmall,
                                    ),
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "select_winning_team".tr,
                                                style: const TextStyle(fontSize: 16),
                                              ),
                                              DropdownButtonFormField<String>(
                                                value: selectedWinTeamId,
                                                items: teamController.listTeam
                                                    .map((team) {
                                                  return DropdownMenuItem<String>(
                                                    value: team.id,
                                                    child: Text(
                                                        team.teamName ?? 'unknown'.tr),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedWinTeamId = value ?? '';
                                                  });
                                                  print("Team ID : $selectedWinTeamId");
                                                },
                                                decoration: InputDecoration(
                                                  labelText: 'select_winning_team'.tr,
                                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                                  prefixIcon: const Icon(Icons.groups),
                                                  border: const OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  filled: true,
                                                  fillColor: bgColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 10),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "select_losing_team".tr,
                                                style: const TextStyle(fontSize: 16),
                                              ),
                                              DropdownButtonFormField<String>(
                                                value: selectedLoseTeamId,
                                                items: teamController.listTeam.map((team) {
                                                  return DropdownMenuItem<String>(
                                                    value: team.id,
                                                    child: Text(team.teamName ?? 'unknown'.tr),
                                                  );
                                                }).toList(),
                                                onChanged: (value) {
                                                  setState(() {
                                                    selectedLoseTeamId = value ?? '';
                                                  });
                                                  print("Team ID : $selectedLoseTeamId");
                                                },
                                                decoration:
                                                InputDecoration(
                                                  labelText: 'select_losing_team'.tr,
                                                  floatingLabelAlignment:
                                                  FloatingLabelAlignment.center,
                                                  floatingLabelBehavior:
                                                  FloatingLabelBehavior.never,
                                                  prefixIcon: const Icon(Icons.groups),
                                                  border: const OutlineInputBorder(
                                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                                    borderSide: BorderSide.none,
                                                  ),
                                                  filled: true,
                                                  fillColor: bgColor,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 16),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                  'score_winning'.tr,
                                                  style: const TextStyle(fontSize: 16)),
                                              CustomFormBuilderTextField(
                                                errorText: _formKey.currentState?.fields['score_winning'.tr]?.errorText,
                                                name: 'score_winning'.tr,
                                                controller: logic.losingController,
                                                hintText: 'enter_score_winning'.tr,
                                                icon: Icons.code,
                                                fillColor: bgColor,
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 10,),
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                  'score_losing'.tr,
                                                  style: const TextStyle(fontSize: 16)),
                                              CustomFormBuilderTextField(
                                                errorText: _formKey.currentState?.fields['score_losing'.tr]?.errorText,
                                                name: 'score_losing'.tr,
                                                controller: logic.winningController,
                                                hintText: 'enter_score_losing'.tr,
                                                icon: Icons.code,
                                                fillColor: bgColor,
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(height: 26,),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: CancelButton(
                                            btnName: 'cancel'.tr,
                                            onPress: () {
                                              logic.winningController.text = '';
                                              logic.losingController.text = '';
                                              Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                            },
                                          ),
                                        ),
                                        const SizedBox(height: 16),
                                        OKButton(
                                          btnName: "confirm".tr,
                                          onPress: () async {
                                            EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true);
                                            try {
                                              logic.updateModifyMatch(
                                                matcheId: widget.matchesModel!.id.toString(),
                                                winningId: selectedWinTeamId.toString(),
                                                losingId: selectedLoseTeamId.toString(),
                                                losingScore: logic.losingController.text,
                                                winningScore: logic.winningController.text,
                                              ).then((result) {
                                                if (result) {
                                                  EasyLoading.showSuccess('updated_modify_finish_successfully!'.tr);
                                                  logic.winningController.text = '';
                                                  logic.losingController.text = '';
                                                  Get.find<HomeController>().updateWidgetHome(const MatchesScreen());
                                                } else {
                                                  EasyLoading.showError('failed_to_update_modify_finish.'.tr);
                                                }
                                                EasyLoading.dismiss();
                                              });
                                            } catch (e) {
                                              logic.isLoading = false;
                                              EasyLoading.dismiss();
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ]),
                            ),
                          ),
                        )
                      ],
                    ),
                )
              ),
            ),
          );
        },
      ),
    );
  }
}
