import 'package:esport_system/data/controllers/player_controller.dart';
import 'package:esport_system/views/matches_section/screen/detail_registration_match.dart';
import 'package:esport_system/views/matches_section/screen/register_match.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/controllers/event_controller.dart';
import '../../../data/controllers/home_controller.dart';
import '../../../data/controllers/matches_controller.dart';
import '../../../data/controllers/subject_controller.dart';
import '../../../helper/add_new_button.dart';
import '../../../helper/constants.dart';
import '../../../helper/empty_data.dart';
import '../../../status_widget.dart';

class GetRegistrationMatches extends StatefulWidget {
  const GetRegistrationMatches({
    super.key,
  });

  @override
  State<GetRegistrationMatches> createState() => _GetRegistrationMatchesState();
}

class _GetRegistrationMatchesState extends State<GetRegistrationMatches> {
  var m = Get.find<MatchesController>();
  var playerController = Get.find<PlayerController>();
  var subjectController = Get.find<SubjectController>();
  var eventController = Get.find<EventController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    super.initState();
    playerController.getPlayerDropdownSearch(searchCat: "");
    eventController.getEvent();
    subjectController.getListSubject();
    m.getRegistrationMatch();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<MatchesController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                color: bgColor,
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Row(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: secondaryColor,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: DropdownButton<int>(
                        value: logic.limit,
                        items: [6, 12, 18, 24].map((int value) {
                          return DropdownMenuItem<int>(
                            value: value,
                            child: Text(
                              '$value',
                              style: const TextStyle(color: Colors.white),
                            ),
                          );
                        }).toList(),
                        onChanged: (newValue) {
                          if (newValue != null) {
                            logic.setLimit(newValue);
                          }
                        },
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                        dropdownColor: secondaryColor,
                        underline: Container(),
                        elevation: 8,
                        borderRadius: BorderRadius.circular(8),
                        icon: const Icon(Icons.arrow_drop_down),
                        iconSize: 24,
                        iconEnabledColor: Colors.grey,
                        iconDisabledColor: Colors.grey[400],
                        isDense: true,
                        menuMaxHeight: 200,
                        alignment: Alignment.center,
                      ),
                    ),
                    const Spacer(),
                    Expanded(child: filterStatus()),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/avatar.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8),
                        // Add spacing between the icon and text
                        Text(
                          'match'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: AddNewButton(
                      btnName: 'add_new'.tr,
                      onPress: () {
                        homeController.updateWidgetHome(const RegisterMatch());
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : LayoutBuilder(
                        builder: (context, constraints) {
                          return SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Column(
                              children: [
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                      minWidth: constraints.maxWidth,
                                    ),
                                    child: DataTable(
                                      dataRowMaxHeight: Get.height * 0.1,
                                      headingRowColor: WidgetStateColor.resolveWith(
                                        (states) => secondaryColor,
                                      ),
                                      columns: [
                                        DataColumn(
                                          label: Text(
                                            "registration_date".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "team_name".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "event_name".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "match_type".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "status".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "actions".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                      rows: logic.listRegistrationMatches.isEmpty
                                          ? []
                                          : List<DataRow>.generate(
                                              logic.listRegistrationMatches.length, (index) {
                                                var list = logic.listRegistrationMatches[index];
                                                return DataRow(
                                                  cells: [
                                                    DataCell(
                                                      Text('${list.registrationDate}',),
                                                    ),
                                                    DataCell(
                                                      GestureDetector(
                                                        onTap: () {},
                                                        child: Text(list.team?.teamName !=null ? list.team!.teamName.toString(): "N/A"),
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text(
                                                        list.event?.title != null ? list.event!.title.toString(): "N/A",
                                                      ),
                                                    ),
                                                    DataCell(
                                                      Text(
                                                        list.matchType != "" ? list.matchType![0].toUpperCase() + list.matchType!.substring(1): "N/A",
                                                      ),
                                                    ),
                                                    DataCell(StatusContainer(
                                                      status: list.registrationStatus.toString(),
                                                    )),
                                                    DataCell(
                                                      Row(
                                                        children: [
                                                          MouseRegion(
                                                            cursor: SystemMouseCursors.click,
                                                            child:
                                                                GestureDetector(
                                                              onTap: () {
                                                                homeController.updateWidgetHome(DetailRegistrationMatch(
                                                                  data: list,
                                                                  authorize: false,
                                                                ));
                                                              },
                                                              child: Container(
                                                                padding: const EdgeInsets.all(6),
                                                                decoration: BoxDecoration(
                                                                  color: Colors.amberAccent.withOpacity(0.1),
                                                                  borderRadius: BorderRadius.circular(6),
                                                                  border: Border.all(
                                                                      width: 1,
                                                                      color: Colors.amberAccent
                                                                  ),
                                                                ),
                                                                child: const Icon(
                                                                  Icons.visibility,
                                                                  size: 20,
                                                                  color: Colors.amberAccent,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(width: 12),
                                                          GestureDetector(
                                                            onTap: () {
                                                              if (list.registrationStatus == "approved") {
                                                                showDialog(
                                                                  context: context,
                                                                  builder: (BuildContext context) {
                                                                    return AlertDialog(
                                                                      backgroundColor: bgColor,
                                                                      content: Container(
                                                                        width: Get.width * 0.3,
                                                                        height: Get.height * 0.3,
                                                                        padding: const EdgeInsets.all(16),
                                                                        child: Column(
                                                                          children: [
                                                                            Align(
                                                                              alignment: Alignment.topCenter,
                                                                              child: Image.asset("assets/images/warning.png", width: 50, height: 50,),
                                                                            ),
                                                                            const SizedBox(height: 15),
                                                                            Text("this_request_was_approved".tr, style: Theme.of(context).textTheme.bodyMedium),
                                                                            const SizedBox(height: 15),
                                                                            const Spacer(),
                                                                            Align(
                                                                              alignment: Alignment.bottomRight,
                                                                              child: OKButton(
                                                                                btnName: 'ok'.tr,
                                                                                onPress: () {
                                                                                  Get.back();
                                                                                },
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    );
                                                                  },
                                                                );
                                                              } else {
                                                                homeController.updateWidgetHome(RegisterMatch(data: list,));
                                                              }
                                                            },
                                                            child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                color: Colors.green.withOpacity(0.1),
                                                                borderRadius: BorderRadius.circular(6),
                                                                border: Border.all(
                                                                    width: 1,
                                                                    color: Colors.green),
                                                              ),
                                                              child: const Icon(
                                                                Icons.edit,
                                                                size: 20,
                                                                color: Colors.green,
                                                              ),
                                                            ),
                                                          ),
                                                          const SizedBox(width: 12,),
                                                          GestureDetector(
                                                            onTap: () {
                                                              if (list.registrationStatus == "approved" ||list.registrationStatus == "rejected") {
                                                                showDialog(
                                                                  context: context,
                                                                  builder: (BuildContext context) {
                                                                    return AlertDialog(
                                                                      backgroundColor: bgColor,
                                                                      content: Container(
                                                                        width: Get.width * 0.3,
                                                                        height: Get.height * 0.3,
                                                                        padding: const EdgeInsets.all(16),
                                                                        child: Column(
                                                                          children: [
                                                                            Align(
                                                                              alignment: Alignment.topCenter,
                                                                              child: Image.asset("assets/images/warning.png", width: 50, height: 50,),
                                                                            ),
                                                                            const SizedBox(height: 15),
                                                                            Text(list.registrationStatus == "approved"
                                                                                ? "this_request_was_approved".tr
                                                                                : list.registrationStatus == "rejected"
                                                                                ? "this_request_was_rejected".tr
                                                                                : "",
                                                                                style: Theme.of(context).textTheme.bodyMedium),
                                                                            const SizedBox(height: 15),
                                                                            const Spacer(),
                                                                            Align(
                                                                              alignment: Alignment.bottomRight,
                                                                              child: OKButton(
                                                                                btnName: 'ok'.tr,
                                                                                onPress: () {
                                                                                  Get.back();
                                                                                },
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    );
                                                                  },
                                                                );
                                                              } else {
                                                                homeController.updateWidgetHome(DetailRegistrationMatch(authorize: true, data: list,));
                                                              }
                                                            },
                                                            child: Container(
                                                              padding: const EdgeInsets.all(6),
                                                              decoration: BoxDecoration(
                                                                border: Border.all(
                                                                    width: 1,
                                                                    color: Colors.blue
                                                                ),
                                                                color: Colors.blue.withOpacity(0.1),
                                                                borderRadius: BorderRadius.circular(6),
                                                              ),
                                                              child: const Icon(
                                                                Icons.check,
                                                                size: 20,
                                                                color: Colors.blue,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                );
                                              },
                                            ),
                                    ),
                                  ),
                                ),
                                if (logic.listRegistrationMatches.isEmpty) ...[
                                  const SizedBox(
                                    height: 50,
                                  ),
                                  const Center(child: EmptyData()),
                                ]
                              ],
                            ),
                          );
                        },
                      ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: logic.previousPageRegisterMatch,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= logic.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () => logic.setPageRegisterMatch(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color: logic.currentPage == i
                                                ? Colors.blue
                                                : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: logic.nextPageRegisterMatch,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget filterStatus() {
    return GetBuilder<MatchesController>(builder: (logic) {
      return Row(
        children: [
          Expanded(
            child: Container(
              height: 48,
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
              decoration: BoxDecoration(
                color: secondaryColor, // Background color
                borderRadius: BorderRadius.circular(10),
              ),
              child: DropdownButton<String>(
                value: logic.filterStatus,
                hint: Text("select_status_to_search".tr),
                icon: const Icon(Icons.arrow_drop_down_sharp),
                iconSize: 24,
                elevation: 16,
                isExpanded: true,
                underline: const SizedBox(),
                onChanged: (String? newValue) {
                  setState(() {
                    logic.filterStatus = newValue;
                    logic.getRegistrationMatch();
                  });
                },
                items: logic.listStatus
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(
                      _capitalize(value),
                      style: const TextStyle(fontSize: 16), // Custom text style
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
          if (logic.filterStatus != null) ...[
            const SizedBox(width: 10),
            Container(
              height: 48,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor // Background color for close button container
              ),
              child: GestureDetector(
                onTap: () {
                  logic.filterStatus = null;
                  logic.getRegistrationMatch();
                },
                child: const Icon(
                  Icons.clear,
                  color: Colors.red,
                ),
              ),
            ),
          ]
        ],
      );
    });
  }

  String _capitalize(String input) {
    if (input.isEmpty) return input;
    return input[0].toUpperCase() + input.substring(1).toLowerCase();
  }
}
