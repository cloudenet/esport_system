import 'package:esport_system/data/controllers/arbitrator_controller.dart';
import 'package:esport_system/data/controllers/position_controller.dart';
import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/app_contrain.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/views/arbitrator_section/create_arbitrator.dart';
import 'package:esport_system/views/arbitrator_section/detail_arbitrator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../data/controllers/home_controller.dart';
import '../../helper/images.dart';

class GetArbitratorScreen extends StatefulWidget {
  const GetArbitratorScreen({super.key});

  @override
  State<GetArbitratorScreen> createState() => _GetArbitratorScreenState();
}

class _GetArbitratorScreenState extends State<GetArbitratorScreen> {
   var arbitratorController = Get.find<ArbitratorController>();
   var userController = Get.find<UserController>();
   var rankController = Get.find<RankController>();
   var homeController = Get.find<HomeController>();
   var positionController = Get.find<PositionController>();
  @override
  void initState() {
    arbitratorController.getArbitrator();
    listAllUser();
    listUserDropDown();
    listRank();
    super.initState();
  }
   void listUserDropDown() async {
     await userController.getListUserDropdown();
   }
   void listAllUser() async {
     await userController.getAllUser();
   }

   void listRank() async {
     await rankController.getRankArbitrator();
   }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<ArbitratorController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: double.infinity,

          child: Column(
            children: [
              _buildDropDown(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/arbitrator.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'referee'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(alignment: Alignment.topRight,child: AddNewButton(btnName:'add_new'.tr,onPress: (){
                    arbitratorController.resetFilePickState();
                    homeController.updateWidgetHome(const CreateUpdateArbitratorScreen());
                  })),
                ],
              ),
              const SizedBox(height: 22,),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : LayoutBuilder(
                  builder: (context, constraints) {
                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minWidth: constraints.maxWidth,
                              ),
                              child: DataTable(
                                dataRowMaxHeight: Get.height* 0.1,
                                headingRowColor:
                                WidgetStateColor.resolveWith((states) =>secondaryColor),
                                columns:  [
                                  DataColumn(
                                    label: Text(
                                      "profile".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "name".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "phone".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "rank".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "experience".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "training_date".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "actions".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                                rows: logic.listArbitrator.isEmpty
                                  ?[]
                                  :List<DataRow>.generate(
                                  logic.listArbitrator.length,
                                      (index) {
                                    var data = logic.listArbitrator[index];
                                    return DataRow(
                                      cells: [
                                        DataCell(
                                          data.userId!.photo != "null" ?
                                          ClipOval(
                                          child: Image.network("${AppConstants.baseUrl}${data.userId!.photo ?? ""}",
                                            width: 50,
                                            height: 50,
                                            fit: BoxFit.cover,
                                            errorBuilder: (context, error, stackTrace) {
                                              return ClipOval(
                                                child: Image.asset(
                                                  Images.noImage,
                                                  height: 50,
                                                  width: 50,
                                                  fit: BoxFit.cover, // Ensures the image fits well inside the circle
                                                ),
                                              );
                                            },
                                            loadingBuilder: (context, child, loadingProgress) {
                                              if (loadingProgress == null) {
                                                return child;
                                              } else {
                                                return Center(
                                                  child: CircularProgressIndicator(
                                                    value: loadingProgress.expectedTotalBytes != null ? loadingProgress.cumulativeBytesLoaded /
                                                        (loadingProgress.expectedTotalBytes ?? 1) : null,
                                                  ),
                                                );
                                              }
                                            },
                                          ),) :
                                          Container(
                                          width: 50,
                                          height: 50,
                                          decoration: BoxDecoration(
                                          border: Border.all(color: primaryColor, width: 2,),
                                          shape: BoxShape.circle),
                                          child: const Icon(Icons.image_not_supported),
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText('${data.name}', 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText('${data.userId?.phone}', 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(data.rankModel?.titleRank??"N/A",
                                          ),
                                        ),
                                        DataCell(
                                          Text('${data.exYear}',
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            formatDate(data.trainingDate.toString()),
                                          ),
                                        ),
                                        DataCell(
                                          Row(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  homeController.updateWidgetHome(DetailArbitrator(data: data));
                                                  // buildViewPosition(context, data,positionController);
                                                },
                                                child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration:
                                                    BoxDecoration(
                                                      color: Colors.amberAccent.withOpacity(0.1),
                                                      border: Border.all(width: 1,color: Colors.amberAccent),
                                                      borderRadius:
                                                      BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.visibility,
                                                      size: 20,
                                                      color: Colors.amberAccent,
                                                    )),
                                              ),
                                              const SizedBox(width: 12,),
                                              GestureDetector(
                                                onTap: () {
                                                  arbitratorController.resetFilePickState();
                                                  homeController.updateWidgetHome(CreateUpdateArbitratorScreen(data: data,));
                                                },
                                                child: Container(
                                                    padding:
                                                    const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      color: Colors.green.withOpacity(0.1),
                                                      border: Border.all(width: 1,color: Colors.green),
                                                      borderRadius:
                                                      BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.edit,
                                                      size: 20,
                                                      color: Colors.green,
                                                    )),
                                              ),
                                              const SizedBox(
                                                width: 12,
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  showDialog(
                                                    context: context,
                                                    builder: (BuildContext
                                                    context) {
                                                      return AlertDialog(
                                                        content:Container(
                                                          width:Get.width * 0.3,
                                                          height:Get.height * 0.3,
                                                          padding:const EdgeInsets.all(16),
                                                          child: Column(
                                                            children: [
                                                              Align(alignment:Alignment.topCenter,
                                                                child: Image.asset(
                                                                  "assets/images/warning.png",
                                                                  width:50,
                                                                  height:50,
                                                                ),
                                                              ),
                                                              const SizedBox(height:15),
                                                              Text("are_you_sure_you_want_to_delete_this_referee?".tr,
                                                                  style: Theme.of(context).textTheme.bodyMedium
                                                              ),
                                                              const SizedBox(height:15),
                                                              const Spacer(),
                                                              Row(
                                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                children: [
                                                                  CancelButton(
                                                                      btnName: 'cancel'.tr,
                                                                      onPress: (){
                                                                        Get.back();
                                                                      }),
                                                                  const SizedBox(width: 16),
                                                                  OKButton(
                                                                      btnName: 'confirm'.tr,
                                                                      onPress: () async {
                                                                        logic.deleteArbitrator(arbitratorsID: data.id.toString());
                                                                        Get.back();
                                                                      }
                                                                  )
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  );
                                                },
                                                child: Container(
                                                  padding:
                                                  const EdgeInsets
                                                      .all(6),
                                                  decoration:
                                                  BoxDecoration(
                                                    color: Colors.red.withOpacity(0.1),
                                                    border: Border.all(width: 1,color: Colors.red),
                                                    borderRadius:
                                                    BorderRadius
                                                        .circular(6),
                                                  ),
                                                  child: const Icon(
                                                    Icons.delete,
                                                    size: 20,
                                                    color: Colors.red,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                          if (logic.listArbitrator.isEmpty)...[
                            const SizedBox(height: 50,),
                            const Center(child: EmptyData()),
                          ]
                        ],
                      ),
                    );
                  },
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: logic.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= logic.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () => logic.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color: logic.currentPage == i
                                                ? Colors.blue
                                                : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: positionController.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildDropDown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: positionController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  positionController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              positionController.search(value);
            }, hintText: 'search_referee_name'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
