
import 'package:esport_system/data/controllers/arbitrator_controller.dart';
import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/data/model/arbitrator_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/helper/dropdown_search_user_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/arbitrator_section/get_arbitrator_screen.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;

import '../../data/controllers/home_controller.dart';

class CreateUpdateArbitratorScreen extends StatefulWidget {
  final ArbitratorModel? data;

  const CreateUpdateArbitratorScreen({super.key, this.data});

  @override
  State<CreateUpdateArbitratorScreen> createState() =>
      _CreateUpdateArbitratorScreenState();
}

class _CreateUpdateArbitratorScreenState
    extends State<CreateUpdateArbitratorScreen> {
  final _formKey = GlobalKey<FormBuilderState>();

  var arbitratorController = Get.find<ArbitratorController>();
  var userController = Get.find<UserController>();
  var rankController = Get.find<RankController>();
  var homeController = Get.find<HomeController>();


  @override
  void initState() {
    super.initState();
    if(widget.data != null){
      arbitratorController.loadPreviousData(widget.data);
    }
    if(widget.data == null){
      arbitratorController.certificateFiles = [];
      arbitratorController.fileBioArbitratorController.text = "";
      arbitratorController.selectedArbitratorRank = null;
      arbitratorController.trainingDateController.text = "";
      arbitratorController.experienceController.text = "";
      arbitratorController.selectedUser = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor,
        body: GetBuilder<ArbitratorController>(builder: (logic) {
          return Container(
            width: double.infinity,
            margin: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      FormBuilder(
                        key: _formKey,
                        child: Responsive(
                            mobile: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.data == null
                                      ? 'create_a_new_referee'.tr
                                      : 'update_referee'.tr,
                                  style: const TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('select_user'.tr,style:const TextStyle(fontSize: 16),),
                                            const SizedBox(height: 5.0),
                                            if(widget.data != null)...[
                                              UserDropdownSearch(
                                                checkUpdate: true,
                                                allUserList: userController.listAllUser,
                                                selectedItem: logic.selectedUser,
                                                onChanged: (value) {
                                                  setState(() {
                                                    logic.selectedUserId = value!.id.toString();
                                                  });
                                                },
                                              )
                                            ]else...[
                                              UserDropdownSearch(
                                                checkUpdate: false,
                                                selectedItem: logic.selectedUser,
                                                onChanged: (value) {
                                                  setState(() {
                                                    logic.selectedUserId = value!.id.toString();
                                                  });
                                                },
                                              )
                                            ]
                                          ],
                                        )),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('select_rank'.tr, style: const TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),

                                          DropdownButtonFormField<String>(
                                            borderRadius: BorderRadius.circular(10),
                                            value: arbitratorController.selectedArbitratorRank,
                                            items: rankController.rankArbitrator.map((user) {
                                              return DropdownMenuItem<String>(
                                                value: user.id,
                                                child: Text(user.titleRank!),
                                              );
                                            }).toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                arbitratorController.selectedArbitratorRank = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText: "please_select_rank".tr,
                                              labelStyle: const TextStyle(
                                                  overflow: TextOverflow.clip
                                              ),
                                              floatingLabelAlignment: FloatingLabelAlignment.center,
                                              floatingLabelBehavior: FloatingLabelBehavior.never,
                                              fillColor: secondaryColor,
                                              filled: true,
                                              //contentPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0), // Adjust padding here
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                              ),
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment:CrossAxisAlignment.start,
                                          children: [
                                            Text('experience_year'.tr,style: const TextStyle(fontSize: 16),),
                                            const SizedBox(height: 5,),
                                            CustomFormBuilderTextField(
                                              name: 'experience_year'.tr,
                                              controller: logic.experienceController,
                                              hintText: "enter_experience_year".tr,
                                              icon: Icons.perm_contact_cal_outlined,
                                              fillColor: secondaryColor,
                                              errorText: 'experience_year_is_required'.tr,
                                            )
                                          ],
                                        )),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'training_date'.tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'training_date'.tr,
                                            controller:logic.trainingDateController,
                                            hintText: 'training_date'.tr,
                                            errorText:'training_date_is_required'.tr,
                                            icon: Icons.date_range_sharp,
                                            onTap: () {
                                              userController.selectDate(context,logic.trainingDateController,);
                                            },
                                            readOnly: true,
                                            fillColor: secondaryColor,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('biography'.tr, style: const TextStyle(fontSize: 16),),
                                    const SizedBox(height: 5.0),
                                    CustomFormBuilderTextField(
                                      name: 'upload_biography'.tr,
                                      controller: logic.fileBioArbitratorController,
                                      hintText: 'upload_biography'.tr,
                                      errorText: 'upload_biography_is_required'.tr,
                                      icon: Icons.attachment_sharp,
                                      onTap: () async {
                                        logic.pickFile(bio: true);
                                      },
                                      readOnly: true,
                                      fillColor: secondaryColor,
                                    )
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'certificate'.tr,
                                      style: const TextStyle(fontSize: 16),
                                    ),
                                    const SizedBox(height: 5.0),
                                    logic.certificateFiles!.isEmpty
                                        ? CustomFormBuilderTextField(
                                      name: 'upload_certificate'.tr,
                                      hintText: 'upload_certificate'.tr,
                                      errorText: 'upload_certificate_is_required'.tr,
                                      icon: Icons.attachment_outlined,
                                      fillColor: secondaryColor,
                                      controller: TextEditingController(text: ""),
                                      onTap:(){
                                        logic.pickCertificateFiles();
                                      } ,
                                    ): GestureDetector(
                                      onTap: () {
                                        logic.pickCertificateFiles();
                                      },
                                      child: Container(
                                        height: 47,
                                        width: double.infinity,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                          color: secondaryColor,
                                        ),
                                        child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: logic.certificateFiles!.length,
                                          itemBuilder: (context, index) {
                                            final file = logic.certificateFiles![index];
                                            final Map<String, Map<String, dynamic>> fileIconMap = {
                                              'pdf': {
                                                'icon': Icons.picture_as_pdf,
                                                'color': Colors.red,
                                              },
                                              'xlsx': {
                                                'icon': Icons.description_outlined,
                                                'color': Colors.green,
                                              },
                                              'doc': {
                                                'icon': Icons.description,
                                                'color': Colors.blue,
                                              },
                                              'docx': {
                                                'icon': Icons.description,
                                                'color': Colors.blue,
                                              },
                                              'jpg': {
                                                'icon': Icons.image,
                                                'color': Colors.green,
                                              },
                                              'jpeg': {
                                                'icon': Icons.image,
                                                'color': Colors.green,
                                              },
                                              'png': {
                                                'icon': Icons.image,
                                                'color': Colors.green,
                                              },
                                              'mp4': {
                                                'icon': Icons.videocam,
                                                'color': Colors.orange,
                                              },
                                              'mp3': {
                                                'icon': Icons.audiotrack,
                                                'color': Colors.purple,
                                              },
                                            };
                                            String fileExtension = p.extension(file.name).toLowerCase().replaceAll('.', ''); // Get extension without '.'
                                            var fileIconData = fileIconMap[fileExtension] ?? {
                                              'icon': Icons.insert_drive_file,
                                              'color': Colors.grey,
                                            };
                                            return Container(
                                              margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                              child: Chip(
                                                label: Text(file.name),
                                                avatar: Icon(fileIconData['icon'], color: fileIconData['color'],),
                                                deleteIcon: const Icon(Icons.delete, color: Colors.red),
                                                onDeleted: () {
                                                  logic.removeCertificateFile(file.name,index); // Call method to remove file
                                                },
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    )
                                  ],
                                ),

                                const SizedBox(height: 24),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    CancelButton(
                                        btnName: 'cancel'.tr,
                                        onPress: () {
                                          homeController.updateWidgetHome(const GetArbitratorScreen());
                                        }),
                                    const SizedBox(width: 16,),
                                    OKButton(
                                      btnName: widget.data == null
                                          ? 'create'.tr
                                          : 'update'.tr,
                                      onPress: () async {
                                        if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                          if (widget.data != null &&
                                              logic.selectedUserId!.isNotEmpty &&
                                              logic.selectedArbitratorRank!.isNotEmpty &&
                                              logic.experienceController.text.isNotEmpty &&
                                              logic.trainingDateController.text.isNotEmpty) {
                                            logic.updateArbitrator(arbitratorsID: widget.data!.id.toString());
                                          }else{
                                            if(logic.selectedUserId!.isNotEmpty &&
                                                logic.selectedArbitratorRank!.isNotEmpty &&
                                                logic.experienceController.text.isNotEmpty &&
                                                logic.trainingDateController.text.isNotEmpty){
                                              logic.createArbitrator();
                                            }
                                          }
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            tablet: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.data == null
                                      ? 'create_a_new_referee'.tr
                                      : 'update_referee'.tr,
                                  style: const TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('select_user'.tr,style:const TextStyle(fontSize: 16),),
                                            const SizedBox(height: 5.0),
                                            if(widget.data != null)...[
                                              UserDropdownSearch(
                                                checkUpdate: true,
                                                allUserList: userController.listAllUser,
                                                selectedItem: logic.selectedUser,
                                                onChanged: (value) {
                                                  setState(() {
                                                    logic.selectedUserId = value!.id.toString();
                                                  });
                                                },
                                              )
                                            ]else...[
                                              UserDropdownSearch(
                                                checkUpdate: false,
                                                selectedItem: logic.selectedUser,
                                                onChanged: (value) {
                                                  setState(() {
                                                    logic.selectedUserId = value!.id.toString();
                                                  });
                                                },
                                              )
                                            ]
                                          ],
                                        )),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('select_rank'.tr, style: const TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),

                                          DropdownButtonFormField<String>(
                                            borderRadius: BorderRadius.circular(10),
                                            value: arbitratorController.selectedArbitratorRank,
                                            items: rankController.rankArbitrator.map((user) {
                                              return DropdownMenuItem<String>(
                                                value: user.id,
                                                child: Text(user.titleRank!),
                                              );
                                            }).toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                arbitratorController.selectedArbitratorRank = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText: "please_select_rank".tr,
                                              labelStyle: const TextStyle(
                                                  overflow: TextOverflow.clip
                                              ),
                                              floatingLabelAlignment: FloatingLabelAlignment.center,
                                              floatingLabelBehavior: FloatingLabelBehavior.never,
                                              fillColor: secondaryColor,
                                              filled: true,
                                              //contentPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0), // Adjust padding here
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                              ),
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment:CrossAxisAlignment.start,
                                          children: [
                                            Text('experience_year'.tr,style: const TextStyle(fontSize: 16),),
                                            const SizedBox(height: 5,),
                                            CustomFormBuilderTextField(
                                              name: 'experience_year'.tr,
                                              controller: logic.experienceController,
                                              hintText: "enter_experience_year".tr,
                                              icon: Icons.perm_contact_cal_outlined,
                                              fillColor: secondaryColor,
                                              errorText: 'experience_year_is_required'.tr,
                                            )
                                          ],
                                        )),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'training_date'.tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'training_date'.tr,
                                            controller:logic.trainingDateController,
                                            hintText: 'training_date'.tr,
                                            errorText:'training_date_is_required'.tr,
                                            icon: Icons.date_range_sharp,
                                            onTap: () {
                                              userController.selectDate(context,logic.trainingDateController,);
                                            },
                                            readOnly: true,
                                            fillColor: secondaryColor,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('biography'.tr, style: const TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'upload_biography'.tr,
                                            controller: logic.fileBioArbitratorController,
                                            hintText: 'upload_biography'.tr,
                                            errorText: 'upload_biography_is_required'.tr,
                                            icon: Icons.attachment_sharp,
                                            onTap: () async {
                                              logic.pickFile(bio: true);
                                            },
                                            readOnly: true,
                                            fillColor: secondaryColor,
                                          )
                                        ],
                                      ),
                                    ),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'certificate'.tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5.0),
                                          logic.certificateFiles!.isEmpty
                                              ? CustomFormBuilderTextField(
                                            name: 'upload_certificate'.tr,
                                            hintText: 'upload_certificate'.tr,
                                            errorText: 'upload_certificate_is_required'.tr,
                                            icon: Icons.attachment_outlined,
                                            fillColor: secondaryColor,
                                            controller: TextEditingController(text: ""),
                                            onTap:(){
                                              logic.pickCertificateFiles();
                                            } ,
                                          ): GestureDetector(
                                            onTap: () {
                                              logic.pickCertificateFiles();
                                            },
                                            child: Container(
                                              height: 47,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(10),
                                                color: secondaryColor,
                                              ),
                                              child: ListView.builder(
                                                scrollDirection: Axis.horizontal,
                                                itemCount: logic.certificateFiles!.length,
                                                itemBuilder: (context, index) {
                                                  final file = logic.certificateFiles![index];
                                                  final Map<String, Map<String, dynamic>> fileIconMap = {
                                                    'pdf': {
                                                      'icon': Icons.picture_as_pdf,
                                                      'color': Colors.red,
                                                    },
                                                    'xlsx': {
                                                      'icon': Icons.description_outlined,
                                                      'color': Colors.green,
                                                    },
                                                    'doc': {
                                                      'icon': Icons.description,
                                                      'color': Colors.blue,
                                                    },
                                                    'docx': {
                                                      'icon': Icons.description,
                                                      'color': Colors.blue,
                                                    },
                                                    'jpg': {
                                                      'icon': Icons.image,
                                                      'color': Colors.green,
                                                    },
                                                    'jpeg': {
                                                      'icon': Icons.image,
                                                      'color': Colors.green,
                                                    },
                                                    'png': {
                                                      'icon': Icons.image,
                                                      'color': Colors.green,
                                                    },
                                                    'mp4': {
                                                      'icon': Icons.videocam,
                                                      'color': Colors.orange,
                                                    },
                                                    'mp3': {
                                                      'icon': Icons.audiotrack,
                                                      'color': Colors.purple,
                                                    },
                                                  };
                                                  String fileExtension = p.extension(file.name).toLowerCase().replaceAll('.', ''); // Get extension without '.'
                                                  var fileIconData = fileIconMap[fileExtension] ?? {
                                                    'icon': Icons.insert_drive_file,
                                                    'color': Colors.grey,
                                                  };
                                                  return Container(
                                                    margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                                    child: Chip(
                                                      label: Text(file.name),
                                                      avatar: Icon(fileIconData['icon'], color: fileIconData['color'],),
                                                      deleteIcon: const Icon(Icons.delete, color: Colors.red),
                                                      onDeleted: () {
                                                        logic.removeCertificateFile(file.name,index); // Call method to remove file
                                                      },
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),

                                  ],
                                ),
                                const SizedBox(height: 24),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    CancelButton(
                                        btnName: 'cancel'.tr,
                                        onPress: () {
                                          homeController.updateWidgetHome(const GetArbitratorScreen());
                                        }),
                                    const SizedBox(width: 16,),
                                    OKButton(
                                      btnName: widget.data == null
                                          ? 'create'.tr
                                          : 'update'.tr,
                                      onPress: () async {
                                        if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                          if (widget.data != null &&
                                              logic.selectedUserId!.isNotEmpty &&
                                              logic.selectedArbitratorRank!.isNotEmpty &&
                                              logic.experienceController.text.isNotEmpty &&
                                              logic.trainingDateController.text.isNotEmpty) {
                                            logic.updateArbitrator(arbitratorsID: widget.data!.id.toString());
                                          }else{
                                            if(logic.selectedUserId!.isNotEmpty &&
                                                logic.selectedArbitratorRank!.isNotEmpty &&
                                                logic.experienceController.text.isNotEmpty &&
                                                logic.trainingDateController.text.isNotEmpty){
                                              logic.createArbitrator();
                                            }
                                          }
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            desktop: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.data == null
                                      ? 'create_a_new_referee'.tr
                                      : 'update_referee'.tr,
                                  style: const TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text('select_user'.tr,style:const TextStyle(fontSize: 16),),
                                            const SizedBox(height: 5.0),
                                            if(widget.data != null)...[
                                              UserDropdownSearch(
                                                checkUpdate: true,
                                                allUserList: userController.listAllUser,
                                                selectedItem: logic.selectedUser,
                                                onChanged: (value) {
                                                  setState(() {
                                                    logic.selectedUserId = value!.id.toString();
                                                  });
                                                },
                                              )
                                            ]else...[
                                              UserDropdownSearch(
                                                checkUpdate: false,
                                                selectedItem: logic.selectedUser,
                                                onChanged: (value) {
                                                  setState(() {
                                                    logic.selectedUserId = value!.id.toString();
                                                  });
                                                },
                                              )
                                            ]
                                          ],
                                        )),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('select_rank'.tr, style: const TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),

                                          DropdownButtonFormField<String>(
                                            borderRadius: BorderRadius.circular(10),
                                            value: arbitratorController.selectedArbitratorRank,
                                            items: rankController.rankArbitrator.map((user) {
                                              return DropdownMenuItem<String>(
                                                value: user.id,
                                                child: Text(user.titleRank!),
                                              );
                                            }).toList(),
                                            onChanged: (value) {
                                              setState(() {
                                                arbitratorController.selectedArbitratorRank = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              labelText: "please_select_rank".tr,
                                              labelStyle: const TextStyle(
                                                  overflow: TextOverflow.clip
                                              ),
                                              floatingLabelAlignment: FloatingLabelAlignment.center,
                                              floatingLabelBehavior: FloatingLabelBehavior.never,
                                              fillColor: secondaryColor,
                                              filled: true,
                                              //contentPadding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0), // Adjust padding here
                                              border: const OutlineInputBorder(
                                                borderSide: BorderSide.none,
                                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                              ),
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Column(
                                          crossAxisAlignment:CrossAxisAlignment.start,
                                          children: [
                                            Text('experience_year'.tr,style: const TextStyle(fontSize: 16),),
                                            const SizedBox(height: 5,),
                                            CustomFormBuilderTextField(
                                              name: 'experience_year'.tr,
                                              controller: logic.experienceController,
                                              hintText: "enter_experience_year".tr,
                                              icon: Icons.perm_contact_cal_outlined,
                                              fillColor: secondaryColor,
                                              errorText: 'experience_year_is_required'.tr,
                                            )
                                          ],
                                        )),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'training_date'.tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'training_date'.tr,
                                            controller:logic.trainingDateController,
                                            hintText: 'training_date'.tr,
                                            errorText:'training_date_is_required'.tr,
                                            icon: Icons.date_range_sharp,
                                            onTap: () {
                                              userController.selectDate(context,logic.trainingDateController,);
                                            },
                                            readOnly: true,
                                            fillColor: secondaryColor,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 16,),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('biography'.tr, style: const TextStyle(fontSize: 16),),
                                          const SizedBox(height: 5.0),
                                          CustomFormBuilderTextField(
                                            name: 'upload_biography'.tr,
                                            controller: logic.fileBioArbitratorController,
                                            hintText: 'upload_biography'.tr,
                                            errorText: 'upload_biography_is_required'.tr,
                                            icon: Icons.attachment_sharp,
                                            onTap: () async {
                                              logic.pickFile(bio: true);
                                            },
                                            readOnly: true,
                                            fillColor: secondaryColor,
                                          )
                                        ],
                                      ),
                                    ),
                                    const SizedBox(width: 16,),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'certificate'.tr,
                                            style: const TextStyle(fontSize: 16),
                                          ),
                                          const SizedBox(height: 5.0),
                                          logic.certificateFiles!.isEmpty
                                              ? CustomFormBuilderTextField(
                                            name: 'upload_certificate'.tr,
                                            hintText: 'upload_certificate'.tr,
                                            errorText: 'upload_certificate_is_required'.tr,
                                            icon: Icons.attachment_outlined,
                                            fillColor: secondaryColor,
                                            controller: TextEditingController(text: ""),
                                            onTap:(){
                                              logic.pickCertificateFiles();
                                            } ,
                                          ): GestureDetector(
                                            onTap: () {
                                              logic.pickCertificateFiles();
                                            },
                                            child: Container(
                                              height: 47,
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(10),
                                                color: secondaryColor,
                                              ),
                                              child: ListView.builder(
                                                scrollDirection: Axis.horizontal,
                                                itemCount: logic.certificateFiles!.length,
                                                itemBuilder: (context, index) {
                                                  final file = logic.certificateFiles![index];
                                                  final Map<String, Map<String, dynamic>> fileIconMap = {
                                                    'pdf': {
                                                      'icon': Icons.picture_as_pdf,
                                                      'color': Colors.red,
                                                    },
                                                    'xlsx': {
                                                      'icon': Icons.description_outlined,
                                                      'color': Colors.green,
                                                    },
                                                    'doc': {
                                                      'icon': Icons.description,
                                                      'color': Colors.blue,
                                                    },
                                                    'docx': {
                                                      'icon': Icons.description,
                                                      'color': Colors.blue,
                                                    },
                                                    'jpg': {
                                                      'icon': Icons.image,
                                                      'color': Colors.green,
                                                    },
                                                    'jpeg': {
                                                      'icon': Icons.image,
                                                      'color': Colors.green,
                                                    },
                                                    'png': {
                                                      'icon': Icons.image,
                                                      'color': Colors.green,
                                                    },
                                                    'mp4': {
                                                      'icon': Icons.videocam,
                                                      'color': Colors.orange,
                                                    },
                                                    'mp3': {
                                                      'icon': Icons.audiotrack,
                                                      'color': Colors.purple,
                                                    },
                                                  };
                                                  String fileExtension = p.extension(file.name).toLowerCase().replaceAll('.', ''); // Get extension without '.'
                                                  var fileIconData = fileIconMap[fileExtension] ?? {
                                                    'icon': Icons.insert_drive_file,
                                                    'color': Colors.grey,
                                                  };
                                                  return Container(
                                                    margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                                    child: Chip(
                                                      label: Text(file.name),
                                                      avatar: Icon(fileIconData['icon'], color: fileIconData['color'],),
                                                      deleteIcon: const Icon(Icons.delete, color: Colors.red),
                                                      onDeleted: () {
                                                        logic.removeCertificateFile(file.name,index); // Call method to remove file
                                                      },
                                                    ),
                                                  );
                                                },
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),

                                  ],
                                ),
                                const SizedBox(height: 24),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    CancelButton(
                                        btnName: 'cancel'.tr,
                                        onPress: () {
                                          homeController.updateWidgetHome(const GetArbitratorScreen());
                                        }),
                                    const SizedBox(width: 16,),
                                    OKButton(
                                      btnName: widget.data == null
                                          ? 'create'.tr
                                          : 'update'.tr,
                                      onPress: () async {
                                        if (_formKey.currentState != null && _formKey.currentState!.saveAndValidate()) {
                                          if (widget.data != null &&
                                              logic.selectedUserId!.isNotEmpty &&
                                              logic.selectedArbitratorRank!.isNotEmpty &&
                                              logic.experienceController.text.isNotEmpty &&
                                              logic.trainingDateController.text.isNotEmpty) {
                                            logic.updateArbitrator(arbitratorsID: widget.data!.id.toString());
                                          }else{
                                            if(logic.selectedUserId!.isNotEmpty &&
                                                logic.selectedArbitratorRank!.isNotEmpty &&
                                                logic.experienceController.text.isNotEmpty &&
                                                logic.trainingDateController.text.isNotEmpty){
                                              logic.createArbitrator();
                                            }
                                          }
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            )
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          );
        }));
  }
}
