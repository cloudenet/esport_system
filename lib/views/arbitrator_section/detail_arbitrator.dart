import 'package:esport_system/data/model/arbitrator_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/arbitrator_section/get_arbitrator_screen.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';
import '../../helper/file_type_extention.dart';

class DetailArbitrator extends StatefulWidget {
  final ArbitratorModel data;
  const DetailArbitrator({super.key, required this.data});

  @override
  State<DetailArbitrator> createState() => _DetailArbitratorState();
}

class _DetailArbitratorState extends State<DetailArbitrator> {
  String? filePath;
  String? fileNameWithExtension;
  var homeController = Get.find<HomeController>();


  @override
  void initState() {
    super.initState();
    filePath = widget.data.bioFile;
    fileNameWithExtension = filePath?.split('/').last;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
          mobile: bodyDetail(isMobile: true),
          tablet: bodyDetail(isTablet: true),
          desktop: bodyDetail()
      ),
    );
  }
  Widget bodyDetail({bool? isMobile,bool? isTablet}){
    print("=======sssLL $isMobile");
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.userId!.photo.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                    children: [
                      Text(
                        widget.data.name != "null" ? widget.data.name.toString() : "N/A",
                        style: const TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,  // Bold name
                        ),
                      ),
                      const SizedBox(height: 4.0),  // Space between name and rank
                      Text(
                        widget.data.rankModel!.typeRank != "null" ? widget.data.rankModel!.typeRank.toString() : "N/A",
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey[600],  // Rank text style
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("personal_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "full_name".tr,
                            textData: widget.data.name != "null" ? widget.data.name.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "user_name".tr,
                            textData: widget.data.username != "null" ? widget.data.username.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "phone".tr,
                            textData: widget.data.userId!.phone != "null" ? widget.data.userId!.phone.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "email".tr,
                            textData: widget.data.userId!.email != "null" ? widget.data.userId!.email.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "gender".tr,
                            textData: widget.data.userId!.gender != "null" ? widget.data.userId!.gender.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "date_of_birth".tr,
                            textData: formatDate(widget.data.userId!.dob != "null" ? widget.data.userId!.dob.toString() : "N/A",),
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("professional_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "official_type".tr,
                            textData: widget.data.officialType != "null" ? widget.data.officialType.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "rank".tr,
                            textData: widget.data.rankModel!.typeRank != "null" ? widget.data.rankModel!.typeRank.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "title_rank".tr,
                            textData: widget.data.rankModel!.titleRank != "null" ? widget.data.rankModel!.titleRank.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "experience".tr,
                            textData: widget.data.exYear != "null" ? widget.data.exYear.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("documents".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if(isMobile != null && isMobile || isTablet != null && isTablet)...[
                    GestureDetector(
                      onTap: () {

                      },
                      child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: bgColor,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                child: Chip(
                                  label: Text(getFileNameFromUrl(widget.data.bioFile.toString())),
                                  avatar: Icon(
                                    getFileIcon(widget.data.bioFile.toString()),
                                    color: getFileIconColor(widget.data.bioFile.toString()),
                                  ),
                                  deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                                  deleteButtonTooltipMessage: 'view_file'.tr,
                                  onDeleted: (){},
                                ),
                              ),
                            ],
                          )
                      ),
                    ),
                    const SizedBox(height: 10,),
                    GestureDetector(
                      onTap: () {

                      },
                      child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: bgColor,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                child: Chip(
                                  label: Text(getFileNameFromUrl(widget.data.bioFile.toString())),
                                  avatar: Icon(
                                    getFileIcon(widget.data.bioFile.toString()),
                                    color: getFileIconColor(widget.data.bioFile.toString()),
                                  ),
                                  deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                                  deleteButtonTooltipMessage: 'view_file'.tr,
                                  onDeleted: (){},
                                ),
                              ),
                            ],
                          )
                      ),
                    )
                  ]else...[
                    Row(
                      children: [
                        Expanded(
                          child: GestureDetector(
                            onTap: () {

                            },
                            child: Container(
                                height: 50,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: bgColor,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                      child: Chip(
                                        label: Text(getFileNameFromUrl(widget.data.bioFile.toString())),
                                        avatar: Icon(
                                          getFileIcon(widget.data.bioFile.toString()),
                                          color: getFileIconColor(widget.data.bioFile.toString()),
                                        ),
                                        deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                                        deleteButtonTooltipMessage: 'view_file'.tr,
                                        onDeleted: (){},
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ),
                        ),
                        const SizedBox(width: 16,),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {

                            },
                            child: Container(
                                height: 50,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: bgColor,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.symmetric(horizontal: 4.0),
                                      child: Chip(
                                        label: Text(getFileNameFromUrl(widget.data.bioFile.toString())),
                                        avatar: Icon(
                                          getFileIcon(widget.data.bioFile.toString()),
                                          color: getFileIconColor(widget.data.bioFile.toString()),
                                        ),
                                        deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                                        deleteButtonTooltipMessage: 'view_file'.tr,
                                        onDeleted: (){},
                                      ),
                                    ),
                                  ],
                                )
                            ),
                          ),
                        ),
                      ],
                    )
                  ]
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("training_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "training_date".tr,
                      textData: widget.data.trainingDate != "null" ? formatDate(widget.data.trainingDate.toString()) : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const GetArbitratorScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}
