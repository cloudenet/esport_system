import 'package:esport_system/data/model/coach_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/views/coach_section/get_coach.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';
import '../../data/model/coach_data_model.dart';
import '../../helper/file_type_extention.dart';
import '../../helper/format_date_widget.dart';
import '../../helper/responsive_helper.dart';

class CoachDetail extends StatefulWidget {
  final CoachDataModel data;
  const CoachDetail({super.key, required this.data});

  @override
  State<CoachDetail> createState() => _CoachDetailState();
}

class _CoachDetailState extends State<CoachDetail> {
  String? filePath;
  String? fileNameWithExtension;
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    super.initState();
    filePath = widget.data.biographyFile;
    fileNameWithExtension = filePath?.split('/').last;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: Responsive(
        mobile: bodyDetailMobile(),
        tablet: bodyDetail(),
        desktop: bodyDetail(),
      ),
    );
  }

  Widget bodyDetail(){
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.user.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                    children: [
                      Text(
                        widget.data.user?.name != "null" ? widget.data.user!.name.toString() : "N/A",
                        style: const TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,  // Bold name
                        ),
                      ),
                      const SizedBox(height: 4.0),  // Space between name and rank
                      Text(
                        widget.data.rank!.titleRank != "null" ? widget.data.rank!.titleRank.toString() : "N/A",
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey[600],  // Rank text style
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("coach_profile".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "full_name".tr,
                            textData: widget.data.user?.name != "null" ? widget.data.user!.name.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "khmer_name".tr,
                            textData: widget.data.user?.nameKhmer != "null" ? widget.data.user!.nameKhmer.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "code".tr,
                            textData: widget.data.code != "null" ? widget.data.code.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "type".tr,
                            textData: widget.data.type != "null" ? widget.data.type.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "specialty".tr,
                            textData: widget.data.specialty != "null" ? widget.data.specialty.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "experience".tr,
                            textData: widget.data.experienceYears != "null" ? widget.data.experienceYears.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "join_date".tr,
                            textData: widget.data.joinDate != "null" ? widget.data.joinDate.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "salary".tr,
                            textData: widget.data.salary != "null" ? widget.data.salary.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("personal_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "full_name".tr,
                            textData: widget.data.user?.name != "null" ? widget.data.user!.name.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "card_number".tr,
                            textData: widget.data.user?.cardNumber != "null" ? widget.data.user!.cardNumber.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "phone".tr,
                            textData: widget.data.user?.phone != "null" ? widget.data.user!.phone.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "email".tr,
                            textData: widget.data.user?.email != "null" ? widget.data.user!.email.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("team_and_rank_information".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "team_name".tr,
                            textData: widget.data.team?.teamName != "null" ? widget.data.team!.teamName.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "rank".tr,
                            textData: widget.data.rank!.titleRank != "null" ? widget.data.rank!.titleRank.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5,),
                  Row(
                    children: [
                      Expanded(
                        child: buildText(
                            title: "founded".tr,
                            textData: widget.data.team?.foundedDate != "null" ? widget.data.team!.foundedDate.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                      const SizedBox(width: 16,),
                      Expanded(
                        child: buildText(
                            title: "address".tr,
                            textData: widget.data.team?.address != "null" ? widget.data.team!.address.toString() : "N/A",
                            height: 50,
                            padding: 8,
                            alignment: Alignment.centerLeft
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("documents".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                        height: 50,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.symmetric(horizontal: 4.0),
                              child: Chip(
                                label: Text(getFileNameFromUrl(widget.data.biographyFile.toString())),
                                avatar: Icon(
                                  getFileIcon(widget.data.biographyFile.toString()),
                                  color: getFileIconColor(widget.data.biographyFile.toString()),
                                ),
                                deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                                deleteButtonTooltipMessage: 'view_file'.tr,
                                onDeleted: (){},
                              ),
                            ),
                          ],
                        )
                    ),
                  ),

                ],
              ),
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const GetCoachScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }

  Widget bodyDetailMobile(){
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 50,),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.blueAccent, width: 2.0),
                    ),
                    child: ClipOval(
                      child: Image.network(
                        widget.data.user.toString(),
                        fit: BoxFit.cover,
                        errorBuilder: (context, error, stackTrace) {
                          return const Icon(Icons.person, size: 50, color: Colors.grey);
                        },
                        loadingBuilder: (context, child, loadingProgress) {
                          if (loadingProgress == null) return child;
                          return const Center(child: CircularProgressIndicator());
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 16.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,  // Align text to the start (left)
                    children: [
                      Text(
                        widget.data.user?.name != "null" ? widget.data.user!.name.toString() : "N/A",
                        style: const TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,  // Bold name
                        ),
                      ),
                      const SizedBox(height: 4.0),  // Space between name and rank
                      Text(
                        widget.data.rank!.titleRank != "null" ? widget.data.rank!.titleRank.toString() : "N/A",
                        style: TextStyle(
                          fontSize: 16.0,
                          color: Colors.grey[600],  // Rank text style
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("coach_profile".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "full_name".tr,
                      textData: widget.data.user?.name != "null" ? widget.data.user!.name.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "khmer_name".tr,
                      textData: widget.data.user?.nameKhmer != "null" ? widget.data.user!.nameKhmer.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "code".tr,
                      textData: widget.data.code != "null" ? widget.data.code.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "type".tr,
                      textData: widget.data.type != "null" ? widget.data.type.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "specialty".tr,
                      textData: widget.data.specialty != "null" ? widget.data.specialty.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "experience".tr,
                      textData: widget.data.experienceYears != "null" ? widget.data.experienceYears.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "join_date".tr,
                      textData: widget.data.joinDate != "null" ? widget.data.joinDate.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "salary".tr,
                      textData: widget.data.salary != "null" ? widget.data.salary.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("personal_information".tr,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "full_name".tr,
                      textData: widget.data.user?.name != "null" ? widget.data.user!.name.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "card_number".tr,
                      textData: widget.data.user?.cardNumber != "null" ? widget.data.user!.cardNumber.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "phone".tr,
                      textData: widget.data.user?.phone != "null" ? widget.data.user!.phone.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "email".tr,
                      textData: widget.data.user?.email != "null" ? widget.data.user!.email.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("team_and_rank_information".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  buildText(
                      title: "team_name".tr,
                      textData: widget.data.team?.teamName != "null" ? widget.data.team!.teamName.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "rank".tr,
                      textData: widget.data.rank!.titleRank != "null" ? widget.data.rank!.titleRank.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "founded".tr,
                      textData: widget.data.team?.foundedDate != "null" ? widget.data.team!.foundedDate.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                  const SizedBox(height: 10,),
                  buildText(
                      title: "address".tr,
                      textData: widget.data.team?.address != "null" ? widget.data.team!.address.toString() : "N/A",
                      height: 50,
                      padding: 8,
                      alignment: Alignment.centerLeft
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20,),
            Text("documents".tr ,style: const TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(24),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: secondaryColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: () {

                    },
                    child: Container(
                        height: 50,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: bgColor,
                        ),
                        child: Container(
                          margin: const EdgeInsets.symmetric(horizontal: 4.0),
                          child: Chip(
                            label: Text(getFileNameFromUrl(widget.data.biographyFile.toString())),
                            avatar: Icon(
                              getFileIcon(widget.data.biographyFile.toString()),
                              color: getFileIconColor(widget.data.biographyFile.toString()),
                            ),
                            deleteIcon: const Icon(Icons.visibility, color: Colors.grey),
                            deleteButtonTooltipMessage: 'view_file'.tr,
                            onDeleted: (){},
                          ),
                        )
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Align(
              alignment: Alignment.topRight,
              child: CancelButton(
                btnName: 'close'.tr,
                onPress: () {
                  homeController.updateWidgetHome(const GetCoachScreen());
                },
              ),
            ),
            const SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }
}
