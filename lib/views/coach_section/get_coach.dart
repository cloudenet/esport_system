import 'package:esport_system/data/controllers/coach_controller.dart';
import 'package:esport_system/data/controllers/home_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/search_field.dart';
import 'package:esport_system/views/coach_section/create_coach_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import 'coach_detail.dart';


class GetCoachScreen extends StatefulWidget {
  const GetCoachScreen({super.key});

  @override
  State<GetCoachScreen> createState() => _GetCoachScreenState();
}

class _GetCoachScreenState extends State<GetCoachScreen> {
  final TeamController teamController = Get.find<TeamController>();
  final CoachController coachController = Get.find<CoachController>();
  final HomeController homeController = Get.find<HomeController>();

  @override
  void initState() {
    coachController.fetchListCoach(typeCoach: 'coach');
    listTeam();
    super.initState();
  }

  void listTeam()async{
    await teamController.getTeam();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<CoachController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: double.infinity,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25),
              topRight: Radius.circular(25),
            ),
          ),
          child: Column(
            children: [
              _buildDropdown(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/coach.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'coach'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: AddNewButton(
                      btnName: "add_new".tr,
                      onPress: () {
                        homeController.updateWidgetHome(const CreateCoachScreen());
                      },
                    ),
                  ),
                ],
              ),

              const SizedBox(height: 22,),
              Expanded(
                child: logic.isLoading
                ? const Center(child: CircularProgressIndicator())
                : LayoutBuilder(
                  builder: (context, constraints) {
                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        children: [
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: ConstrainedBox(
                              constraints: BoxConstraints(
                                minWidth: constraints.maxWidth,
                              ),
                              child: DataTable(
                                dataRowMaxHeight: Get.height * 0.1,
                                headingRowColor: WidgetStateColor.resolveWith(
                                (states) => secondaryColor),
                                columns: [
                                  DataColumn(
                                    label: Text(
                                      "card_number".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "name".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "phone".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "type".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "team_name".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "rank".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  DataColumn(
                                    label: Text(
                                      "actions".tr,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                                rows: logic.getListCoach.isEmpty
                                ?[]
                                :List<DataRow>.generate(
                                  logic.getListCoach.length,
                                      (index) {
                                    var data = logic.getListCoach[index];
                                    return DataRow(
                                      cells: [
                                        DataCell(
                                          Text(
                                            _truncateText('${data.user!.cardNumber}', 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(
                                            _truncateText('${data.user!.name}', 20),
                                          ),
                                        ),
                                        DataCell(
                                          Text(_truncateText('${data.user!.phone}', 20),),
                                        ),
                                        DataCell(
                                          Text(_truncateText('${data.type}', 20),),
                                        ),
                                        DataCell(
                                          Text(_truncateText('${data.team!.teamName}', 20),),
                                        ),
                                        DataCell(
                                          Text(_truncateText('${data.rank!.titleRank}', 20),),
                                        ),
                                        DataCell(
                                          Row(
                                            children: [
                                              GestureDetector(
                                                onTap: () {
                                                  homeController.updateWidgetHome(CoachDetail(data: data));
                                                },
                                                child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      color: Colors.amberAccent.withOpacity(0.1),
                                                      border: Border.all(width: 1,color: Colors.amberAccent),
                                                      borderRadius:
                                                      BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.visibility,
                                                      size: 20,
                                                      color: Colors.amberAccent,
                                                    )),
                                              ),
                                              const SizedBox(width: 12,),
                                              GestureDetector(
                                                onTap: () {
                                                  homeController.updateWidgetHome(CreateCoachScreen(data: data,));
                                                },
                                                child: Container(
                                                    padding: const EdgeInsets.all(6),
                                                    decoration: BoxDecoration(
                                                      color: Colors.green.withOpacity(0.1),
                                                      border: Border.all(width: 1,color: Colors.green),
                                                      borderRadius: BorderRadius.circular(6),
                                                    ),
                                                    child: const Icon(
                                                      Icons.edit,
                                                      size: 20,
                                                      color: Colors.green,
                                                    )),
                                              ),
                                              const SizedBox(width: 12,),
                                              GestureDetector(
                                                onTap: () {
                                                  showDialog(
                                                    context: context,
                                                    builder: (BuildContext
                                                    context) {
                                                      return AlertDialog(
                                                        content: Container(
                                                          width: Get.width * 0.3,
                                                          height: Get.height * 0.3,
                                                          padding: const EdgeInsets.all(16),
                                                          child: Column(
                                                            children: [
                                                              Align(
                                                                alignment: Alignment.topCenter,
                                                                child: Image.asset("assets/images/warning.png",
                                                                  width: 50,
                                                                  height: 50,
                                                                ),
                                                              ),
                                                              const SizedBox(height: 15),
                                                              Text("are_you_sure_to_delete_this_coach?".tr,),
                                                              const SizedBox(height: 15),
                                                              const Spacer(),
                                                              // This will push the buttons to the bottom
                                                              Align(
                                                                alignment: Alignment.bottomRight,
                                                                child: Row(
                                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: [
                                                                    CancelButton(btnName: 'cancel'.tr, onPress: (){
                                                                      Get.back();
                                                                    }),
                                                                    const SizedBox(width: 10),
                                                                    OKButton(
                                                                      btnName: 'confirm'.tr,
                                                                      onPress: () async {
                                                                        Get.back();
                                                                        logic.deletedCoachAndAssistant(id: data.id.toString(), type: 'coach');
                                                                      },
                                                                    )
                                                                  ],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  );
                                                },
                                                child: Container(
                                                  padding:
                                                  const EdgeInsets
                                                      .all(6),
                                                  decoration:
                                                  BoxDecoration(
                                                    color: Colors.red.withOpacity(0.1),
                                                    border: Border.all(width: 1,color: Colors.red),
                                                    borderRadius: BorderRadius.circular(6),
                                                  ),
                                                  child: const Icon(
                                                    Icons.delete,
                                                    size: 20,
                                                    color: Colors.red,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                ),
                              ),
                            ),
                          ),
                          if (logic.getListCoach.isEmpty)...[
                            const SizedBox(height: 50,),
                            const Center(child: EmptyData()),
                          ]
                        ],
                      ),
                    );
                  },
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: coachController.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1;
                                  i <= coachController.totalPages;
                                  i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () =>
                                            coachController.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(
                                            color:
                                                coachController.currentPage == i
                                                    ? Colors.blue
                                                    : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: coachController.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildDropdown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: coachController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  coachController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: searchField(onChange: (value) {
              coachController.search(value);
            }, hintText: 'search_coach_name'.tr),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
}
