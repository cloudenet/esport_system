import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/model/coach_model.dart';
import 'package:esport_system/helper/dropdown_search_user_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:esport_system/data/controllers/coach_controller.dart';
import 'package:esport_system/data/controllers/team_controller.dart';
import 'package:esport_system/data/controllers/user_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/views/coach_section/get_coach.dart';
import 'package:esport_system/views/home_section/screen/home.dart';

import '../../data/controllers/home_controller.dart';
import '../../data/model/coach_data_model.dart';

class CreateCoachScreen extends StatefulWidget {
  final CoachDataModel? data;

  const CreateCoachScreen({super.key, this.data});

  @override
  State<CreateCoachScreen> createState() => _CreateCoachScreenState();
}

class _CreateCoachScreenState extends State<CreateCoachScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final userController = Get.find<UserController>();
  final teamController = Get.find<TeamController>();
  final coachController = Get.find<CoachController>();
  final rankController = Get.find<RankController>();
  var homeController = Get.find<HomeController>();

  @override
  void initState() {
    coachController.clearValue();
    listUserDropdown();
    listCoachRank();
    if (widget.data != null) {
      coachController.oldValueToUpdate(widget.data!);
    }
    super.initState();
  }


  void listUserDropdown() async {
    await userController.getListUserDropdown();
  }

  void listCoachRank() async {
    await rankController.getRankCoach();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<CoachController>(
        builder: (logic) {
          return GetBuilder<UserController>(
            builder: (controller) {
              return SingleChildScrollView(
                child: Container(
                    width: Get.width,
                    padding: const EdgeInsets.all(24),
                    child: Responsive(
                      mobile: bodyMobile(),
                      desktop: body(),
                      tablet: body(),
                    )
                ),
              );
            },
          );
        },
      ),
    );
  }

  Widget body() {
    return GetBuilder<CoachController>(builder: (logic) {
      return Column(
        children: [
          FormBuilder(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  widget.data == null
                      ? 'create_a_new_coach'.tr
                      : 'update_coach'.tr,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16.0),
                Row(
                  children: [
                    Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'select_user'.tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            const SizedBox(height: 5.0),
                            if(widget.data != null)...[
                              Container(
                                padding: const EdgeInsets.all(12),
                                decoration: BoxDecoration(
                                  color: secondaryColor,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.blue,
                                      radius: 12,
                                      child: Text(
                                        (logic.previousCoach!.user!.name?.substring(0, 1) ?? 'N').toUpperCase(),
                                        style: const TextStyle(color: Colors.white, fontSize: 12),
                                      ),
                                    ),
                                    const SizedBox(width: 8),
                                    Expanded(
                                      child: Text(
                                        logic.previousCoach!.user!.name?.toUpperCase() ?? 'N/A',
                                        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ] else
                              ...[
                                UserDropdownSearch(
                                  checkUpdate: false,
                                  selectedItem: logic.previousSelectedUser,
                                  onChanged: (value) {
                                    setState(() {
                                      logic.selectedUserId = value!.id.toString();
                                    });
                                  },
                                )
                              ]
                          ],
                        )),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'select_rank'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          DropdownButtonFormField<String>(
                            validator: (value) {
                              if(value != null){
                                return null;
                              }
                              return "please_select_rank".tr;
                            },
                            borderRadius: BorderRadius.circular(10),
                            value: logic.selectRankId,
                            items: rankController.rankCoach.map((ranked) {
                              return DropdownMenuItem<String>(
                                value: ranked.id,
                                child: Text(ranked.titleRank ?? 'unknown'.tr),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                logic.selectRankId = value.toString();
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "please_select_rank".tr,
                              floatingLabelAlignment: FloatingLabelAlignment.center,
                              floatingLabelBehavior: FloatingLabelBehavior.never,
                              labelStyle: const TextStyle(color: Colors.white70),
                              fillColor: secondaryColor,
                              filled: true,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'select_team'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          DropdownButtonFormField<String>(
                            validator: (value) {
                              if(value != null){
                                return null;
                              }
                              return "select_team".tr;
                            },
                            borderRadius: BorderRadius.circular(10),
                            value: logic.selectedTeamId,
                            items: teamController.listTeam.map((user) {
                              return DropdownMenuItem<String>(
                                value: user.id,
                                child: Text(user.teamName!),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                logic.selectedTeamId = value;
                                print("Selected Team ID: $value");
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "select_team".tr,
                              labelStyle: const TextStyle(color: Colors.white70),
                              floatingLabelAlignment: FloatingLabelAlignment.center,
                              floatingLabelBehavior: FloatingLabelBehavior.never,
                              fillColor: secondaryColor,
                              filled: true,
                              border: const OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'select_coach_type'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          DropdownButtonFormField<String>(
                            value: logic.selectedCoachType,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return "select_coach_type".tr;
                              }
                              return null;
                            },
                            borderRadius: BorderRadius.circular(10),
                            items: logic.coachType.map((type) {
                              return DropdownMenuItem<String>(
                                value: type['value'],
                                child: Text(type['text']!),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                logic.selectedCoachType = value!;
                                print("logic.selectedCoachType : ${logic.selectedCoachType}");
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "select_coach_type".tr,
                              labelStyle: const TextStyle(color: Colors.white70),
                              floatingLabelAlignment: FloatingLabelAlignment.center,
                              floatingLabelBehavior: FloatingLabelBehavior.never,
                              fillColor: secondaryColor,
                              filled: true,
                              border: OutlineInputBorder(
                                borderSide: BorderSide.none,
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(
                            'experience_year'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          CustomFormBuilderTextField(
                            name: 'experience_year'.tr,
                            controller: logic.experienceYearController,
                            hintText: 'experience_year'.tr,
                            errorText: 'experience_year_is_required'.tr,
                            icon: Icons.perm_contact_cal_outlined,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(
                            'specialty'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          CustomFormBuilderTextField(
                            name: 'specialty'.tr,
                            controller: logic.specialtyController,
                            hintText: 'specialty'.tr,
                            errorText: 'specialty_is_required'.tr,
                            icon: Icons.type_specimen_rounded,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16.0),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(
                            'join_date'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          CustomFormBuilderTextField(
                            name: 'join_date'.tr,
                            controller: logic.joinDateController,
                            hintText: 'join_date'.tr,
                            errorText: 'join_date_is_required'.tr,
                            icon: Icons.date_range_sharp,
                            onTap: () {
                              userController.selectDate(context, logic.joinDateController,);
                            },
                            readOnly: true,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 16.0),
                    Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "biography".tr,
                              style: const TextStyle(fontSize: 16),
                            ),
                            const SizedBox(height: 5.0),
                            CustomFormBuilderTextField(
                              name: 'biography'.tr,
                              controller: logic.fileBioController,
                              hintText: "biography".tr,
                              icon: Icons.attachment_sharp,
                              fillColor: secondaryColor,
                              errorText: 'enter_file'.tr,
                              onTap: () {
                                logic.pickFile();
                                print("Bio file ${logic.fileBioController.text}");
                              },
                            ),
                          ],
                        ),
                    )
                  ],
                ),
                const SizedBox(height: 16),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(
                            'salary'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          CustomFormBuilderTextField(
                            name: 'salary'.tr,
                            controller: logic.salaryController,
                            hintText: 'salary'.tr,
                            errorText: 'salary_is_required'.tr,
                            icon: CupertinoIcons.money_dollar_circle,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 20),
                    Expanded(
                      child: Column(
                        crossAxisAlignment:
                        CrossAxisAlignment.start,
                        children: [
                          Text(
                            'champion_won'.tr,
                            style: const TextStyle(fontSize: 16),
                          ),
                          const SizedBox(height: 5.0),
                          CustomFormBuilderTextField(
                            name: 'champion_won'.tr,
                            controller: logic.championshipWonController,
                            hintText: 'champion_won'.tr,
                            errorText: 'champion_won_is_required'.tr,
                            icon: Icons.wine_bar_outlined,
                            fillColor: secondaryColor,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),

                const SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CancelButton(
                        btnName: 'cancel'.tr,
                        onPress: () {
                          logic.clearValue();
                          homeController.updateWidgetHome(const GetCoachScreen());
                        }),
                    const SizedBox(width: 16,),
                    OKButton(
                      btnName: widget.data == null
                          ? 'create'.tr
                          : 'update'.tr,
                      onPress: () async {
                        if (_formKey.currentState != null &&
                            _formKey.currentState!.saveAndValidate()) {
                          if (widget.data != null) {
                            logic.updateAssignCoachAndAssistant(id: widget.data!.id.toString(),type: 'coach');
                          }
                          else {
                            logic.assignCoachAndAssistant(type: 'coach');
                          }
                        } else {
                          EasyLoading.showError(
                            "please_input_all_field".tr,
                            duration:
                            const Duration(seconds: 2),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    });
  }
  Widget bodyMobile() {
    return GetBuilder<CoachController>(builder: (logic) {
      return Column(
        children: [
          FormBuilder(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  widget.data == null
                      ? 'create_a_new_coach'.tr
                      : 'update_coach'.tr,
                  style: const TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'select_user'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                if(widget.data != null)...[
                  Container(
                    padding: const EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      color: secondaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.blue,
                          radius: 12,
                          child: Text(
                            (logic.previousCoach!.user!.name?.substring(0, 1) ?? 'N').toUpperCase(),
                            style: const TextStyle(color: Colors.white, fontSize: 12),
                          ),
                        ),
                        const SizedBox(width: 8),
                        Expanded(
                          child: Text(
                            logic.previousCoach!.user!.name?.toUpperCase() ?? 'N/A',
                            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  )
                ] else ...[
                    UserDropdownSearch(
                      checkUpdate: false,
                      selectedItem: logic.previousSelectedUser,
                      onChanged: (value) {
                        setState(() {
                          logic.selectedUserId = value!.id.toString();
                        });
                      },
                    )
                  ],
                const SizedBox(height: 16.0),
                Text(
                  'select_rank'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                DropdownButtonFormField<String>(
                  validator: (value) {
                    if(value != null){
                      return null;
                    }
                    return "please_select_rank".tr;
                  },
                  borderRadius: BorderRadius.circular(10),
                  value: logic.selectRankId,
                  items: rankController.rankCoach.map((ranked) {
                    return DropdownMenuItem<String>(
                      value: ranked.id,
                      child: Text(ranked.titleRank ?? 'unknown'.tr),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      logic.selectRankId = value.toString();
                    });
                  },
                  decoration: InputDecoration(
                    labelText: "please_select_rank".tr,
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    labelStyle: const TextStyle(color: Colors.white70),
                    fillColor: secondaryColor,
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'select_team'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                DropdownButtonFormField<String>(
                  validator: (value) {
                    if(value != null){
                      return null;
                    }
                    return "select_team".tr;
                  },
                  borderRadius: BorderRadius.circular(10),
                  value: logic.selectedTeamId,
                  items: teamController.listTeam.map((user) {
                    return DropdownMenuItem<String>(
                      value: user.id,
                      child: Text(user.teamName!),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      logic.selectedTeamId = value;
                      print("Selected Team ID: $value");
                    });
                  },
                  decoration: InputDecoration(
                    labelText: "select_team".tr,
                    labelStyle: const TextStyle(color: Colors.white70),
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    fillColor: secondaryColor,
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'select_coach_type'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                DropdownButtonFormField<String>(
                  value: logic.selectedCoachType,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return "select_coach_type".tr;
                    }
                    return null;
                  },
                  borderRadius: BorderRadius.circular(10),
                  items: logic.coachType.map((type) {
                    return DropdownMenuItem<String>(
                      value: type['value'],
                      child: Text(type['text']!),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      logic.selectedCoachType = value!;
                      print("logic.selectedCoachType : ${logic.selectedCoachType}");
                    });
                  },
                  decoration: InputDecoration(
                    labelText: "select_coach_type".tr,
                    labelStyle: const TextStyle(color: Colors.white70),
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                    floatingLabelBehavior: FloatingLabelBehavior.never,
                    fillColor: secondaryColor,
                    filled: true,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
                const SizedBox(height: 16.0),
                Text(
                  'experience_year'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                CustomFormBuilderTextField(
                  name: 'experience_year'.tr,
                  controller: logic.experienceYearController,
                  hintText: 'experience_year'.tr,
                  errorText: 'experience_year_is_required'.tr,
                  icon: Icons.perm_contact_cal_outlined,
                  fillColor: secondaryColor,
                ),
                const SizedBox(height: 16.0),
                Text(
                  'specialty'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                CustomFormBuilderTextField(
                  name: 'specialty'.tr,
                  controller: logic.specialtyController,
                  hintText: 'specialty'.tr,
                  errorText: 'specialty_is_required'.tr,
                  icon: Icons.type_specimen_rounded,
                  fillColor: secondaryColor,
                ),
                const SizedBox(height: 16.0),
                Text(
                  'join_date'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                CustomFormBuilderTextField(
                  name: 'join_date'.tr,
                  controller: logic.joinDateController,
                  hintText: 'join_date'.tr,
                  errorText: 'join_date_is_required'.tr,
                  icon: Icons.date_range_sharp,
                  onTap: () {
                    userController.selectDate(context, logic.joinDateController,);
                  },
                  readOnly: true,
                  fillColor: secondaryColor,
                ),
                const SizedBox(height: 16.0),
                Text(
                  "biography".tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                CustomFormBuilderTextField(
                  name: 'biography'.tr,
                  controller: logic.fileBioController,
                  hintText: "biography".tr,
                  icon: Icons.attachment_sharp,
                  fillColor: secondaryColor,
                  errorText: 'enter_file'.tr,
                  onTap: () {
                    logic.pickFile();
                    print("Bio file ${logic.fileBioController.text}");
                  },
                ),
                const SizedBox(height: 16),
                Text(
                  'salary'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                CustomFormBuilderTextField(
                  name: 'salary'.tr,
                  controller: logic.salaryController,
                  hintText: 'salary'.tr,
                  errorText: 'salary_is_required'.tr,
                  icon: CupertinoIcons.money_dollar_circle,
                  fillColor: secondaryColor,
                ),
                const SizedBox(height: 16),
                Text(
                  'champion_won'.tr,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 5.0),
                CustomFormBuilderTextField(
                  name: 'champion_won'.tr,
                  controller: logic.championshipWonController,
                  hintText: 'champion_won'.tr,
                  errorText: 'champion_won_is_required'.tr,
                  icon: Icons.wine_bar_outlined,
                  fillColor: secondaryColor,
                ),
                const SizedBox(height: 24),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CancelButton(
                        btnName: 'cancel'.tr,
                        onPress: () {
                          logic.clearValue();
                          homeController.updateWidgetHome(const GetCoachScreen());
                        }),
                    const SizedBox(width: 16,),
                    OKButton(
                      btnName: widget.data == null
                          ? 'create'.tr
                          : 'update'.tr,
                      onPress: () async {
                        if (_formKey.currentState != null &&
                            _formKey.currentState!.saveAndValidate()) {
                          if (widget.data != null) {
                            logic.updateAssignCoachAndAssistant(id: widget.data!.id.toString(),type: 'coach');
                          }
                          else {
                            logic.assignCoachAndAssistant(type: 'coach');
                          }
                        } else {
                          EasyLoading.showError(
                            "please_input_all_field".tr,
                            duration:
                            const Duration(seconds: 2),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    });
  }
}
