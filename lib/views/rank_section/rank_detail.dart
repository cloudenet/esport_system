import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/cotainer_widget.dart';
import 'package:esport_system/helper/responsive_helper.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/rank_section/get_rank.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';
import '../../data/model/rank_model.dart';

class RankDetail extends StatefulWidget {
  final RankModel data;
  const RankDetail({super.key, required this.data});

  @override
  State<RankDetail> createState() => _RankDetailState();
}

class _RankDetailState extends State<RankDetail> {
  @override
  Widget build(BuildContext context) {
    var homeController = Get.find<HomeController>();

    return Scaffold(
      body: Responsive(
        tablet: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Left image container
                Container(
                  height: 150,
                  width: 150,
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  child: Image.asset(
                    'assets/icons/rank.png',
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 16),
                // Right details container
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: secondaryColor,
                    ),
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Rank Detail Title
                        Text(
                          'rank_detail'.tr,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 24),
                        // Rank type
                        buildText(
                          title: 'type_of_rank'.tr,
                          textData: widget.data.typeRank != "null" ? widget.data.typeRank.toString() : "N/A",
                          height: 50,
                          padding: 10,
                          alignment: Alignment.centerLeft,
                        ),
                        const SizedBox(height: 16),
                        // Rank title
                        buildText(
                          title: 'title'.tr,
                          textData: widget.data.titleRank != "null" ? widget.data.titleRank.toString() : "N/A",
                          height: 50,
                          padding: 10,
                          alignment: Alignment.centerLeft,
                        ),
                        const SizedBox(height: 24),
                        // Close button aligned at the bottom right
                        Align(
                          alignment: Alignment.bottomRight,
                          child: CancelButton(
                            btnName: 'close'.tr,
                            onPress: () {
                              // Ensure homeController is initialized properly
                              homeController.updateWidgetHome(const GetRank());
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        mobile: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: const BoxDecoration(
            color: secondaryColor,
          ),
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Rank Detail Title
                Text(
                  'rank_detail'.tr,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 24),
                // Rank type
                buildText(
                  title: 'type_of_rank'.tr,
                  textData: widget.data.typeRank != "null" ? widget.data.typeRank.toString() : "N/A",
                  height: 50,
                  padding: 10,
                  alignment: Alignment.centerLeft,
                ),
                const SizedBox(height: 16),
                // Rank title
                buildText(
                  title: 'title'.tr,
                  textData: widget.data.titleRank != "null" ? widget.data.titleRank.toString() : "N/A",
                  height: 50,
                  padding: 10,
                  alignment: Alignment.centerLeft,
                ),
                const SizedBox(height: 24),
                // Close button aligned at the bottom right
                Align(
                  alignment: Alignment.bottomRight,
                  child: CancelButton(
                    btnName: 'close'.tr,
                    onPress: () {
                      // Ensure homeController is initialized properly
                      homeController.updateWidgetHome(const GetRank());
                    },
                  ),
                )
              ],
            ),
          ),
        ),
        desktop: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: bgColor,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Left image container
                Container(
                  height: 150,
                  width: 150,
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor,
                  ),
                  child: Image.asset(
                    'assets/icons/rank.png',
                    color: Colors.white,
                  ),
                ),
                const SizedBox(width: 16),
                // Right details container
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: secondaryColor,
                    ),
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Rank Detail Title
                        Text(
                          'rank_detail'.tr,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(height: 24),
                        // Rank type
                        buildText(
                          title: 'type_of_rank'.tr,
                          textData: widget.data.typeRank != "null" ? widget.data.typeRank.toString() : "N/A",
                          height: 50,
                          padding: 10,
                          alignment: Alignment.centerLeft,
                        ),
                        const SizedBox(height: 16),
                        // Rank title
                        buildText(
                          title: 'title'.tr,
                          textData: widget.data.titleRank != "null" ? widget.data.titleRank.toString() : "N/A",
                          height: 50,
                          padding: 10,
                          alignment: Alignment.centerLeft,
                        ),
                        const SizedBox(height: 24),
                        // Close button aligned at the bottom right
                        Align(
                          alignment: Alignment.bottomRight,
                          child: CancelButton(
                            btnName: 'close'.tr,
                            onPress: () {
                              // Ensure homeController is initialized properly
                              homeController.updateWidgetHome(const GetRank());
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
