import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/data/model/rank_model.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/custom_textformfield.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/rank_section/get_rank.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class CreateArbitratorRankScreen extends StatefulWidget {
  final RankModel? data; // Add a data parameter to hold the existing rank data

  const CreateArbitratorRankScreen({Key? key, this.data}) : super(key: key);

  @override
  State<CreateArbitratorRankScreen> createState() =>
      _CreateArbitratorRankScreenState();
}

class _CreateArbitratorRankScreenState extends State<CreateArbitratorRankScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final rankController = Get.find<RankController>();
  var homeController = Get.find<HomeController>();


  final List<Map<String, String>> type = [
    {'value': 'coaches', 'text': 'Coaches'},
    {'value': 'arbitrator', 'text': 'Arbitrator'},
  ];
  String? selectedType;

  @override
  void initState() {
    super.initState();
    if (widget.data != null) {
      selectedType = widget.data?.typeRank ?? "N/A";
      rankController.titleController.text =
          widget.data?.titleRank.toString() ?? "N/A";
    }else{
      rankController.titleController.text = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: GetBuilder<RankController>(builder: (logic) {
        return Container(
          width: double.infinity,
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    FormBuilder(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.data != null
                                ? 'update_rank'.tr
                                : 'create_rank'.tr,
                            style: const TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(height: 20.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'select_type'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              const SizedBox(height: 10.0),
                              FormBuilderDropdown(
                                name: 'type'.tr,
                                initialValue: selectedType,
                                borderRadius: BorderRadius.circular(10),
                                decoration: InputDecoration(
                                  floatingLabelAlignment: FloatingLabelAlignment.center,
                                  floatingLabelBehavior: FloatingLabelBehavior.never,
                                  fillColor: secondaryColor,
                                  labelText: 'please_select_type'.tr,
                                  filled: true,
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide.none,
                                      borderRadius:BorderRadius.circular(10)
                                  ),
                                ),
                                items: type
                                    .map((option) => DropdownMenuItem(
                                          value: option['value'],
                                          child: Text(option['text']!),
                                        ))
                                    .toList(),
                                onChanged: (value) {
                                  setState(() {
                                    selectedType = value;
                                  });
                                },
                              ),
                            ],
                          ),
                          const SizedBox(height: 20.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'title'.tr,
                                style: const TextStyle(fontSize: 16),
                              ),
                              CustomFormBuilderTextField(
                                name: 'title',
                                controller: logic.titleController,
                                hintText: 'title'.tr,
                                errorText: 'title_is_required'.tr,
                                icon: Icons.description,
                                fillColor: secondaryColor,
                              ),
                            ],
                          ),
                          const SizedBox(height: 24.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              CancelButton(
                                  btnName: 'cancel'.tr, onPress: () {
                                    selectedType == null;
                                    logic.titleController.text = '';
                                    homeController.updateWidgetHome(const GetRank());
                              }),
                              const SizedBox(width: 16,),
                              OKButton(
                                btnName: widget.data == null
                                    ? 'create'.tr
                                    : 'update'.tr,
                                onPress: () async {
                                  if (_formKey.currentState != null &&
                                      _formKey.currentState!.saveAndValidate()) {
                                    EasyLoading.show(status: 'loading...'.tr, dismissOnTap: true,
                                    );
                                    if (widget.data == null) {
                                      await logic.createRank(
                                              type: selectedType.toString(),
                                              title: logic.titleController.text)
                                          .then((status) {
                                            if (status) {
                                              EasyLoading.showSuccess('rank_create_successfully'.tr, dismissOnTap: true, duration: const Duration(seconds: 2),);
                                              homeController.updateWidgetHome(const GetRank());
                                            } else {
                                              EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
                                            }
                                            EasyLoading.dismiss();
                                          }
                                          );
                                    } else {
                                      await logic.updateRank(
                                              rankID: widget.data!.id.toString(),
                                              type: selectedType.toString(),
                                              titleRank: logic.titleController.text)
                                          .then((status) {
                                            if (status) {
                                              EasyLoading.showSuccess('rank_updated_successfully'.tr, dismissOnTap: true, duration: const Duration(seconds: 2),);
                                              homeController.updateWidgetHome(const GetRank());
                                        } else {
                                          EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
                                        }
                                        EasyLoading.dismiss();
                                      });
                                    }
                                  }
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
