import 'package:esport_system/data/controllers/rank_controller.dart';
import 'package:esport_system/helper/add_new_button.dart';
import 'package:esport_system/helper/constants.dart';
import 'package:esport_system/helper/empty_data.dart';
import 'package:esport_system/helper/format_date_widget.dart';
import 'package:esport_system/views/home_section/screen/home.dart';
import 'package:esport_system/views/rank_section/create_abitrator_rank_screen.dart';
import 'package:esport_system/views/rank_section/rank_detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';

import '../../data/controllers/home_controller.dart';

class GetRank extends StatefulWidget {
  const GetRank({super.key});

  @override
  State<GetRank> createState() => _GetRankState();
}

class _GetRankState extends State<GetRank> {
  final rankController = Get.find<RankController>();
  var homeController = Get.find<HomeController>();


  @override
  void initState() {
    rankController.getRank();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<RankController>(builder: (logic) {
        return Container(
          margin: const EdgeInsets.only(left: 10, right: 10, top: 10),
          width: double.infinity,
          height: double.infinity,
          child: Column(
            children: [
              _buildDropDown(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 12),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 32,
                          height: 32,
                          child: Image.asset(
                            "assets/icons/rank.png",
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8), // Add spacing between the icon and text
                        Text(
                          'rank'.tr,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 32,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                      alignment: Alignment.topRight,
                      child: AddNewButton(
                          btnName: 'add_new'.tr,
                          onPress: () {
                            homeController.updateWidgetHome(
                                const CreateArbitratorRankScreen());
                          })),
                ],
              ),
              const SizedBox(
                height: 22,
              ),
              Expanded(
                child: logic.isLoading
                    ? const Center(child: CircularProgressIndicator())
                    : (logic.rank.isEmpty
                        ? const EmptyData()
                        : LayoutBuilder(
                            builder: (context, constraints) {
                              return SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                      minWidth: constraints.maxWidth,
                                    ),
                                    child: DataTable(
                                      dataRowMaxHeight: Get.height * 0.1,
                                      headingRowColor:
                                          WidgetStateColor.resolveWith(
                                              (states) => secondaryColor),
                                      columns:  [
                                        DataColumn(
                                          label: Text(
                                            "no.".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "title".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "type".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "created_by".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "created_date".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                        DataColumn(
                                          label: Text(
                                            "actions".tr,
                                            style: const TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                      rows: List<DataRow>.generate(
                                        logic.rank.length,
                                        (index) {
                                          var data = logic.rank[index];
                                          return DataRow(
                                            cells: [
                                              DataCell(
                                                Text(_truncateText('${index + 1}', 10)),
                                              ),
                                              DataCell(
                                                Text(
                                                  _truncateText(
                                                      '${data.titleRank}', 20),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  _capitalizeFirst(data.typeRank!.toLowerCase()),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  _truncateText(
                                                      '${data.createdBy!.name}',
                                                      20),
                                                ),
                                              ),
                                              DataCell(
                                                Text(
                                                  _truncateText(formatDate(data.createdAt.toString()), 20),
                                                ),
                                              ),
                                              DataCell(
                                                Row(
                                                  children: [
                                                    GestureDetector(
                                                      onTap: () {
                                                        homeController.updateWidgetHome( RankDetail(data: data));
                                                      },
                                                      child: Container(
                                                          padding: const EdgeInsets.all(6),
                                                          decoration: BoxDecoration(
                                                            color: Colors.amberAccent.withOpacity(0.1),
                                                            border: Border.all(
                                                                width: 1,
                                                                color: Colors.amberAccent),
                                                            borderRadius: BorderRadius.circular(6),
                                                          ),
                                                          child: const Icon(
                                                            Icons.visibility,
                                                            size: 20,
                                                            color: Colors.amberAccent,
                                                          )),
                                                    ),
                                                    const SizedBox(width: 12,),
                                                    GestureDetector(
                                                      onTap: () {
                                                        homeController.updateWidgetHome(CreateArbitratorRankScreen(data: data));
                                                      },
                                                      child: Container(
                                                          padding: const EdgeInsets.all(6),
                                                          decoration: BoxDecoration(
                                                            color: Colors.green.withOpacity(0.1),
                                                            border: Border.all(width: 1, color: Colors.green),
                                                            borderRadius:
                                                                BorderRadius.circular(6),
                                                          ),
                                                          child: const Icon(
                                                            Icons.edit,
                                                            size: 20,
                                                            color: Colors.green,
                                                          )),
                                                    ),
                                                    const SizedBox(width: 12,),
                                                    GestureDetector(
                                                      onTap: () {
                                                        showDialog(
                                                          context: context,
                                                          builder: (BuildContext context) {
                                                            return AlertDialog(
                                                              content: Container(
                                                                width: Get.width * 0.3,
                                                                height: Get.height * 0.3,
                                                                padding: const EdgeInsets.all(16),
                                                                child: Column(
                                                                  children: [
                                                                    Align(
                                                                      alignment: Alignment.topCenter,
                                                                      child: Image.asset("assets/images/warning.png",
                                                                        width: 50,
                                                                        height: 50,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(height: 15),
                                                                    Text("are_you_sure_you_want_to_delete_this_rank?".tr,
                                                                        style: Theme.of(context).textTheme.bodyMedium),
                                                                    const SizedBox(height: 15),
                                                                    const Spacer(),
                                                                    Row(
                                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                                      children: [
                                                                        CancelButton(
                                                                            btnName: 'cancel'.tr,
                                                                            onPress: () {
                                                                              Get.back();
                                                                            }),
                                                                        const SizedBox(width: 16),
                                                                        OKButton(
                                                                            btnName: 'confirm'.tr,
                                                                            onPress: () async {
                                                                              logic.isLoading = true;
                                                                              logic.update();
                                                                              EasyLoading.show(
                                                                                status: 'loading...'.tr,
                                                                                dismissOnTap: true,
                                                                              );
                                                                              await logic.deleteRank(rankID: data.id.toString()).then((status) {
                                                                                if (status) {
                                                                                  EasyLoading.showSuccess(
                                                                                    'rank_delete_successfully'.tr,
                                                                                    dismissOnTap: true,
                                                                                    duration: const Duration(seconds: 2),
                                                                                  );
                                                                                  Get.back();
                                                                                  logic.getRank();
                                                                                } else {
                                                                                  EasyLoading.showError('something_went_wrong'.tr, duration: const Duration(seconds: 2));
                                                                                  Get.back();
                                                                                }
                                                                                EasyLoading.dismiss();
                                                                              });
                                                                            })
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            );
                                                          },
                                                        );
                                                      },
                                                      child: Container(
                                                        padding: const EdgeInsets.all(6),
                                                        decoration: BoxDecoration(
                                                          color: Colors.red.withOpacity(0.1),
                                                          border: Border.all(width: 1, color: Colors.red),
                                                          borderRadius: BorderRadius.circular(6),
                                                        ),
                                                        child: const Icon(Icons.delete,
                                                          size: 20,
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          )),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SizedBox(
                    width: 700,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                          onPressed: rankController.previousPage,
                          icon: const Icon(Icons.arrow_back_ios),
                        ),
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              for (int i = 1; i <= rankController.totalPages; i++)
                                Padding(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      TextButton(
                                        onPressed: () =>
                                            rankController.setPage(i),
                                        child: Text(
                                          i.toString(),
                                          style: TextStyle(color: rankController.currentPage == i ? Colors.blue : Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ),
                        IconButton(
                          onPressed: rankController.nextPage,
                          icon: const Icon(Icons.arrow_forward_ios),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }

  Widget _buildDropDown() {
    return Container(
      color: bgColor,
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(6),
            ),
            child: DropdownButton<int>(
              value: rankController.limit,
              items: [6, 12, 18, 24].map((int value) {
                return DropdownMenuItem<int>(
                  value: value,
                  child: Text(
                    '$value',
                    style: const TextStyle(color: Colors.white),
                  ),
                );
              }).toList(),
              onChanged: (newValue) {
                if (newValue != null) {
                  rankController.setLimit(newValue);
                }
              },
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
              dropdownColor: secondaryColor,
              underline: Container(),
              elevation: 8,
              borderRadius: BorderRadius.circular(8),
              icon: const Icon(Icons.arrow_drop_down),
              iconSize: 24,
              iconEnabledColor: Colors.grey,
              iconDisabledColor: Colors.grey[400],
              isDense: true,
              menuMaxHeight: 200,
              alignment: Alignment.center,
            ),
          ),
          const Spacer(),
          Expanded(
            child: _searchField(
                onChange: (value) {
                  rankController.search(value);
                }
            ),
          ),
        ],
      ),
    );
  }

  String _truncateText(String text, int length) {
    return text.length > length ? '${text.substring(0, length)}...' : text;
  }
  Widget _searchField({
    required Function(String) onChange,
  }) {
    final TextEditingController controller = TextEditingController();
    return Row(
      children: [
        Expanded(
          child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SizedBox(
                height: 48, // Set the desired height
                child: TextField(
                  controller: controller,
                  onChanged: (value) {
                    onChange(value);
                  },
                  decoration: InputDecoration(
                    fillColor: bgColor,
                    hintStyle: const TextStyle(color: Colors.white),
                    filled: true,
                    border: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    focusedBorder: const OutlineInputBorder(
                      borderSide: BorderSide.none,
                    ),
                    contentPadding: const EdgeInsets.only(left: 12), // Set the horizontal padding
                    suffixIcon: DropdownButtonFormField<String>(
                      borderRadius: BorderRadius.circular(10),
                      value: rankController.selectedValue,
                      items: rankController.dropdownItems.map((item) {
                        return DropdownMenuItem<String>(
                          value: item['value'],
                          child: Text(item['text']!),
                        );
                      }).toList(),
                      onChanged: (value) {
                        if(value != null){
                          setState(() {
                            rankController.selectValueDropdown(value);
                            controller.text = value;
                            onChange(value);
                          });
                        }
                      },
                      decoration: InputDecoration(
                        labelText: "select_type_to_search".tr,
                        floatingLabelAlignment: FloatingLabelAlignment.center,
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        fillColor: secondaryColor,
                        filled: true,
                        border: const OutlineInputBorder(
                          borderSide: BorderSide.none,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        rankController.selectedValue != null ? const SizedBox(width: 10,):const SizedBox(),
        if (rankController.selectedValue != null) // Change the condition here
          GestureDetector(
            onTap: () {
              setState(() {
                rankController.selectedValue = null;
                controller.clear();
                onChange('');
              });
            },
            child: Container(
                height: 48,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor
                ),
                child: const Icon(Icons.clear,color: Colors.red,)
            ),
          ),
      ],
    );
  }
  String _capitalizeFirst(String type) {
    if (type.isEmpty) return type;
    return type.substring(0, 1).toUpperCase() + type.substring(1).toLowerCase();
  }
}
