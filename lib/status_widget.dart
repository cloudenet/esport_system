import 'package:flutter/material.dart';

class StatusContainer extends StatelessWidget {
  final String status;

  const StatusContainer({
    Key? key,
    required this.status,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Normalize the status string
    final normalizedStatus = status.trim().toLowerCase();

    // Determine colors based on status
    final backgroundColor = normalizedStatus == "pending"
        ? Colors.amberAccent.withOpacity(0.1)
        : normalizedStatus == "rejected"
        ? Colors.redAccent.withOpacity(0.1)
        : normalizedStatus == "approve"
        ? Colors.green.withOpacity(0.1)
        : Colors.grey.withOpacity(0.1); // Default color for unexpected statuses

    final borderColor = normalizedStatus == "pending"
        ? Colors.amberAccent
        : normalizedStatus == "rejected"
        ? Colors.redAccent
        : normalizedStatus == "rejected"
        ? Colors.green
        : Colors.green; // Default color for unexpected statuses

    final textColor = borderColor;

    return Container(
      width: 100,
      height: 30,
      padding: const EdgeInsets.all(3),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: backgroundColor,
        border: Border.all(width: 1, color: borderColor),
      ),
      child: Center(
        child: Text(
          status[0].toUpperCase() + status.substring(1), // Capitalize the first letter
          style: TextStyle(color: textColor),
        ),
      ),
    );
  }
}
