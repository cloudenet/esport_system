import 'package:get/get.dart';
import 'en.dart';
import 'km.dart';

class MyTranslations extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
    'en_US': langEn,
    'km_KH': langKm,
  };
}